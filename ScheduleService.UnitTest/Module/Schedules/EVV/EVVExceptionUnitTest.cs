
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Services.Schedules.Service;
using System;
using System.Collections.Generic;
using Xunit;
using Moq;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;

namespace ScheduleService.UnitTest
{
    public class EVVExceptionUnitTest
    {
        //[Fact]
        //public async void RaiseEVVEventsForConfiguredHHA_VerifyExceptionValling_returnTrue()
        //{
        //    //MentionName_Scneario_returns
        //    //---------------------------------------Arrange  
        //    var Expected = true;

        //    ScheduleEVVExceptionsQuery ExceptionsPayLoad = new ScheduleEVVExceptionsQuery();
        //   // ExceptionsPayLoad.HHA = 208;


        //    var service_1 = new Mock<IDatabaselistRepository>();
        //   // var service_2 = new Mock<IEvvDefaultExceptionEventsRepository>();
        //    var service_3 = new Mock<IAgencyMasterListRepository>();
        //    var service_4 = new Mock<IAutoGenerateException>();
        //    var service_5 = new Mock<IHHAScheduleAuthSettingsRepository>();

        //    service_1.Setup(x => x.GetAllDataBaseList()).ReturnsAsync(GetDBList()).Verifiable();
        //   // service_2.Setup(x => x.GetDBEVVDefaultEventDetails(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(GetHHAEVVEventist()).Verifiable();

        //    //service_4.Setup(x => x.AutoGenerateNoShowException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<int>())).ReturnsAsync(true).Verifiable();
        //    //service_4.Setup(x => x.AutoGenerateMissedCheckoutException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<int>())).ReturnsAsync(true).Verifiable();
        //    //service_4.Setup(x => x.AutoGenerateUnScheduledVisitException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<int>())).ReturnsAsync(true).Verifiable();

        //   // service_5.Setup(x => x.GetHHAScheduleConfigDetails(It.IsAny<int>())).ReturnsAsync(GetHHAConfigDetails()).Verifiable();

        //    //----------------------------------------Act       
        //    // Invoke the method to test
        //   // var class_EVVException = new EVVException(service_1.Object, service_2.Object,service_3.Object,service_4.Object,service_5.Object);
        //   // var actualValue = await class_EVVException.AutogenerateEVVEventExceptions(ExceptionsPayLoad);

        //    //---------------------------------------Assert  
        //    Assert.Equal(Expected, actualValue);
        //    //verify the mock            
        //    service_1.Verify();
        //    service_2.Verify();
        //    //service_3.Verify();
        //    service_4.Verify();
        //    service_5.Verify();

        //} 

        //[Fact]
        //public async void AutoGenerateNoShowException_GetExceptionsAndSaveExceptions_returnTrue()
        //{

        //    //---------------------------------------Arrange  
        //    var Expected = true;

        //    var service_1 = new Mock<ICaregiverTaskEVVExceptionsRepository>();
        //    var service_2 = new Mock<ICaregivertasksRepository>();

        //    service_1.Setup(h => h.AddEVVException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
        //    service_2.Setup(h => h.GetEVVExceptionEligibleSchedules(It.IsAny<ScheduleFilters>(), It.IsAny<string>())).ReturnsAsync(EVvExceptionElgibleSchedules()).Verifiable();


        //    //----------------------------------------Act    
        //    //Invoke the method to test
        //    var class_AutoGenerateException = new AutoGenerateException(service_1.Object, service_2.Object); 
        //    var actualValue = await class_AutoGenerateException.AutoGenerateNoShowException(HHASystemCodeDetails_NoShow(), 20);
                      

        //    //---------------------------------------Assert  
        //    Assert.Equal(Expected, actualValue);
        //    //verify the mock
        //    service_1.Verify();
        //    service_2.Verify();
        //}

        //[Fact]
        //public async void AutoGenerateMissedCheckoutException_GetExceptionsAndSaveExceptions_returnTrue()
        //{

        //    //---------------------------------------Arrange  
        //    var Expected = true;

        //    var service_1 = new Mock<ICaregiverTaskEVVExceptionsRepository>();
        //    var service_2 = new Mock<ICaregivertasksRepository>();

        //    service_1.Setup(h => h.AddEVVException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
        //    service_2.Setup(h => h.GetEVVExceptionEligibleSchedules(It.IsAny<ScheduleFilters>(), It.IsAny<string>())).ReturnsAsync(EVvExceptionElgibleSchedules()).Verifiable();


        //    //----------------------------------------Act    
        //    //Invoke the method to test
        //    var class_AutoGenerateException = new AutoGenerateException(service_1.Object, service_2.Object);
        //    var actualValue = await class_AutoGenerateException.AutoGenerateMissedCheckoutException(HHASystemCodeDetails_MissedCheckout(), 10);


        //    //---------------------------------------Assert     
        //    Assert.Equal(Expected, actualValue);
        //    //verify the mock
        //    service_1.Verify();
        //    service_2.Verify();
        //}

        //[Fact]
        //public async void AutoGenerateUNSCHEDULEDVISITException_GetExceptionsAndSaveExceptions_returnTrue()
        //{

        //    //---------------------------------------Arrange  
        //    var Expected = true;

        //    var service_1 = new Mock<ICaregiverTaskEVVExceptionsRepository>();
        //    var service_2 = new Mock<ICaregivertasksRepository>();

        //    service_1.Setup(h => h.AddEVVException(It.IsAny<EvvDefaultExceptionEventsEntity>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
        //    service_2.Setup(h => h.GetEVVExceptionEligibleSchedules(It.IsAny<ScheduleFilters>(), It.IsAny<string>())).ReturnsAsync(EVvExceptionElgibleSchedules()).Verifiable();


        //    //----------------------------------------Act    
        //    //Invoke the method to test
        //    var class_AutoGenerateException = new AutoGenerateException(service_1.Object, service_2.Object);
        //    var actualValue = await class_AutoGenerateException.AutoGenerateUnScheduledVisitException(HHASystemCodeDetails_UNSCHEDULED_VISIT(), 10);


        //    //---------------------------------------Assert  
        //    Assert.Equal(Expected, actualValue);
        //    //verify the mock
        //    service_1.Verify();
        //    service_2.Verify();
        //}


        private IEnumerable<CaregivertasksEntity> EVvExceptionElgibleSchedules() {

            List<CaregivertasksEntity> DBs = new List<CaregivertasksEntity>()
            {
                new CaregivertasksEntity
                {
                    CGTASK_ID = 500
                },
                new CaregivertasksEntity
                {
                    CGTASK_ID = 100
                }
            };

            return DBs;

        }

        private IEnumerable<UAAgencyDatabaseMasterListEntity> GetDBList()
        { 
            List<UAAgencyDatabaseMasterListEntity> DBs = new List<UAAgencyDatabaseMasterListEntity>()
            {
                new UAAgencyDatabaseMasterListEntity
                {
                    DatabaseID=  100,
                    DatabaseName= "Production100",
                    Server = "100.100.100.999"
                },
                new UAAgencyDatabaseMasterListEntity
                {
                    DatabaseID=  200,
                    DatabaseName= "Production104",
                    Server = "100.100.100.888"
                }
            };

            return DBs;
        }

        //private IEnumerable<EvvDefaultExceptionEventsEntity> GetHHAEVVEventist()
        //{
        //    List<EvvDefaultExceptionEventsEntity> HHAEventList = new List<EvvDefaultExceptionEventsEntity>()
        //    {
        //        new EvvDefaultExceptionEventsEntity
        //        {
        //            DefaultCode = "01",                    
        //            DefaultEventDescription = "Test EVv Exception Event No show",
        //            EVVVendorVersionMasterID = 10,
        //            hha = 100,
        //            DefaultEvent = "VISIT_NO_SHOW",
        //            lastQueueExecutedon = null
        //        },
        //        new EvvDefaultExceptionEventsEntity
        //        {
        //            DefaultCode = "04",
        //            DefaultEventDescription = "Test EVV Exception Event Missed CheckOut",
        //            EVVVendorVersionMasterID = 5,
        //            hha = 100,
        //            DefaultEvent = "MISSED_CHECKOUT",
        //            lastQueueExecutedon = null
        //        },
        //        new EvvDefaultExceptionEventsEntity
        //        {
        //            DefaultCode = "55",
        //            DefaultEventDescription = "Test EVV Exception Event UNSCHEDULED VISIT",
        //            EVVVendorVersionMasterID = 5,
        //            hha = 100,
        //            DefaultEvent = "UNSCHEDULED_VISIT",
        //            lastQueueExecutedon = null
        //        }
        //    };

        //    return HHAEventList;
        //}

        //private ScheduleEVVExceptionsQuery GetQueryParam()
        //{
        //    ScheduleEVVExceptionsQuery QueryParam = new ScheduleEVVExceptionsQuery();
        //    return QueryParam;
        //}
        //private EvvDefaultExceptionEventsEntity HHASystemCodeDetails_NoShow()
        //{
        //    EvvDefaultExceptionEventsEntity HHAEvent = new EvvDefaultExceptionEventsEntity()
        //    {
        //        DefaultCode = "01",
        //        DefaultEventDescription = "Test EVv Exception Event No show",
        //        EVVVendorVersionMasterID = 10,
        //        hha = 100,
        //        DefaultEvent = "VISIT_NO_SHOW",
        //        lastQueueExecutedon = null
        //    };

        //    return HHAEvent;
        //}

        //private EvvDefaultExceptionEventsEntity HHASystemCodeDetails_MissedCheckout()
        //{
        //    EvvDefaultExceptionEventsEntity HHAEvent = new EvvDefaultExceptionEventsEntity()
        //    {
        //        DefaultCode = "04",
        //        DefaultEventDescription = "Test EVV Exception Event Missed CheckOut",
        //        EVVVendorVersionMasterID = 5,
        //        hha = 100,
        //        DefaultEvent = "MISSED_CHECKOUT",
        //        lastQueueExecutedon = null
        //    };

        //    return HHAEvent;
        //}

        //private EvvDefaultExceptionEventsEntity HHASystemCodeDetails_UNSCHEDULED_VISIT()
        //{
        //    EvvDefaultExceptionEventsEntity HHAEvent = new EvvDefaultExceptionEventsEntity()
        //    {
        //        DefaultCode = "55",
        //        DefaultEventDescription = "Test EVV Exception Event UNSCHEDULED VISIT",
        //        EVVVendorVersionMasterID = 5,
        //        hha = 100,
        //        DefaultEvent = "UNSCHEDULED_VISIT",
        //        lastQueueExecutedon = null
        //    };

        //    return HHAEvent;
        //}

        //private HHAScheduleAuthSettings GetHHAConfigDetails()
        //{
        //    HHAScheduleAuthSettings output = new HHAScheduleAuthSettings()
        //    {
        //        AutoGenerateMissedCheckoutExceptionMin = 10,
        //        AutoGenerateNoShowExceptionMin = 20,
        //        AutoGenerateUnscheduledExceptionMin = 30
        //    };

        //    return output;
        //}
    }
}
  
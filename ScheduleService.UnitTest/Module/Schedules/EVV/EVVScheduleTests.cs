﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ScheduleService.UnitTest.Module.Schedules.EVV
{
    public class EVVScheduleTests
    {
        private readonly EVVSchedule _eVVSchedule;
        private readonly Mock<IMapper> mock_mapper = new Mock<IMapper>();
        private readonly Mock<ICaregiverTaskEvvSchedulesRepository> mock_caregiverTaskEvvSchedulesRepository = new Mock<ICaregiverTaskEvvSchedulesRepository>();
        private readonly Mock<ICaregivertasksRepository> mock_caregivertasksRepository = new Mock<ICaregivertasksRepository>();
        private readonly Mock<IHHAConfigurationsRepository> mock_hHAConfigurationsRepository = new Mock<IHHAConfigurationsRepository>();
        private readonly Mock<IRepositoryFilterConditions> mock_repositoryFilterConditions = new Mock<IRepositoryFilterConditions>();
        private readonly Mock<IClientsRepository> mock_clientsRepository = new Mock<IClientsRepository>();
        private readonly Mock<IClientEvvTerminalPayersRepository> mock_clientEvvTerminalPayersRepository = new Mock<IClientEvvTerminalPayersRepository>();
        private readonly Mock<ICaregiverTasksTerminalPayerRepository> mock_caregiverTasksTerminalPayerRepository = new Mock<ICaregiverTasksTerminalPayerRepository>();
        private readonly Mock<ISchedule> mock_schedule = new Mock<ISchedule>();
        private readonly Mock<IHttpContextAccessor> mock_httpContextAccessor = new Mock<IHttpContextAccessor>();
        private readonly Mock<ILogger<EVVSchedule>> mock_logger = new Mock<ILogger<EVVSchedule>>();

        public EVVScheduleTests()
        {
            _eVVSchedule = new EVVSchedule(mock_mapper.Object, mock_caregiverTaskEvvSchedulesRepository.Object, mock_caregivertasksRepository.Object,
                mock_repositoryFilterConditions.Object, mock_clientsRepository.Object, mock_hHAConfigurationsRepository.Object,
                mock_clientEvvTerminalPayersRepository.Object, mock_caregiverTasksTerminalPayerRepository.Object, mock_schedule.Object,mock_httpContextAccessor.Object,
                mock_logger.Object);
        }

        [Fact]
        private async void AutoRegisterEvvTerminalPayerSchedules_ReturnList_OnValidInput() 
        {
            //Arrange
            int HHA = 901250;
            int User = 1;
            int ret = 0;
            string Filters = "";
            _M_ConfigurationsEntity m_ConfigurationsEntity = new _M_ConfigurationsEntity
            {
                EvvDataBaseID = 15,
                Evv_terminal_payer_visits_last_registered_on = DateTime.MinValue
            };

            List<ClientEvvTerminalPayersEntity> clientEvvTerminalPayersEntity = new List<ClientEvvTerminalPayersEntity>
            {
                new ClientEvvTerminalPayersEntity
                {
                    hha = 901250,
                    client_id = 42828,
                    hierarchy = 'S',
                    Primary_client_payer_id = 51439,
                    export_effective_from = DateTime.MinValue,
                    terminal_configuration_ID = 10,
                    Terminal_client_payer_id = 3307
                }
            };

            List<ClientPaymentSourcesEntity> clientPaymentSourcesEntities = new List<ClientPaymentSourcesEntity>
            {
                new ClientPaymentSourcesEntity
                {
                    CLIENT_PAYMENT_ID = 51439,
                    CLIENT_ID = 42828,
                    PAYMENT_SOURCE_ID = 605
                }
            };

            List<CaregivertasksEntity> caregivertasksEntities = new List<CaregivertasksEntity>
            {
                new CaregivertasksEntity
                {
                    CGTASK_ID = 6927906,
                    PLANNED_DATE = DateTime.Parse("2021-10-01 00:00:00.000"),
                    CLIENT_ID = 42828,
                    PAYMENT_SOURCE = 605
                }
            };

            List<CaregiverTaskChildSchedulesEntity> caregiverTaskChildSchedulesEntities = new List<CaregiverTaskChildSchedulesEntity>
            {
                new CaregiverTaskChildSchedulesEntity
                {
                    Child_Schedule_Id = 70974,
                    PARENT_CGTASK_ID = 6927907,
                    SCHEDULE_DATE = DateTime.Parse("2021-10-02 00:00:00.000"),
                    CLIENT = 42828,
                    PAYMENT_SOURCE = 605

                }
            };

            List<EvvTerminalPayerSchedulesDto> evvTerminalPayerSchedulesDtos = new List<EvvTerminalPayerSchedulesDto>
            {
                new EvvTerminalPayerSchedulesDto
                {
                    HHA = 901250,
                    CgTaskID = 6927906
                },
                new EvvTerminalPayerSchedulesDto
                {
                    HHA = 901250,
                    CgTaskID = 6927907
                }
            };

            mock_hHAConfigurationsRepository.Setup(x => x.Get_M_Configurations(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(m_ConfigurationsEntity);
            mock_clientEvvTerminalPayersRepository.Setup(x => x.GetClientTerminalPayersList(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(clientEvvTerminalPayersEntity);
            mock_clientsRepository.Setup(x => x.GetClientPaymentSourcesList(HHA, User, It.IsAny<string>(), It.IsAny<string>(), "")).ReturnsAsync(clientPaymentSourcesEntities);
            mock_caregivertasksRepository.Setup(x => x.GetAllSchedulesList(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(caregivertasksEntities);
            mock_caregivertasksRepository.Setup(x => x.GetAllChildSchedulesList(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(caregiverTaskChildSchedulesEntities);
            mock_caregiverTasksTerminalPayerRepository.Setup(x => x.InsertCaregiverTasksTerminalPayer(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(ret);
            mock_caregiverTasksTerminalPayerRepository.Setup(x => x.InsertCaregiverTaskChildScheduleTerminalPayer(HHA, User, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(ret);
            mock_hHAConfigurationsRepository.Setup(x => x.UpdateUAConfigurations(HHA, User, It.IsAny<string>(), It.IsAny<string>()));
            mock_repositoryFilterConditions.Setup(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Scheduleservice.Core.Enums.FilterConditionDataTypeEnums>(), It.IsAny<bool>())).Returns(mock_repositoryFilterConditions.Object);
            mock_repositoryFilterConditions.Setup(x => x.BetweenFilter(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<Scheduleservice.Core.Enums.FilterConditionDataTypeEnums>(), It.IsAny<bool>())).Returns(mock_repositoryFilterConditions.Object);
            mock_repositoryFilterConditions.Setup(x => x.EqualFilter(It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<Scheduleservice.Core.Enums.FilterConditionDataTypeEnums>(), It.IsAny<bool>())).Returns(mock_repositoryFilterConditions.Object);
            mock_repositoryFilterConditions.Setup(x => x.Build()).Returns(Filters);

            //Act
            var ActualResult = await _eVVSchedule.AutoRegisterEvvTerminalPayerSchedules(HHA, User);

            //Assert
            Assert.Equal(evvTerminalPayerSchedulesDtos.Count(), ActualResult.response_payload.Count());
            Assert.Equal(HttpStatusCode.OK, ActualResult.http_status_code);
        }
    }
}

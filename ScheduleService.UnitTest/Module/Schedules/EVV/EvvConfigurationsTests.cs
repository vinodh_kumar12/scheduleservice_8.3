﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Moq;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;

namespace ScheduleService.UnitTest.Module.Schedules.EVV
{
    public class EvvConfigurationsTests
    {
        private readonly EvvConfigurations evvConfigurations;
        private readonly Mock<IHHAScheduleAuthSettingsRepository> mock_HHAScheduleAuthSettingsRepo = new Mock<IHHAScheduleAuthSettingsRepository>();
        private readonly Mock<IRepositoryUpdateColumn> mock_RepositoryUpdateColumn = new Mock<IRepositoryUpdateColumn>();
        private readonly Mock<IMapper> mock_Mapper = new Mock<IMapper>();
        private readonly Mock<ILogger<EvvConfigurations>> mock_Logger = new Mock<ILogger<EvvConfigurations>>();
        private readonly Mock<IHHAEvvExceptionEventsRepository> mock_hHAEvvExceptionEventsRepository = new Mock<IHHAEvvExceptionEventsRepository>();
        private readonly Mock<IUAScheduleConflictExceptionsRepository> mock_uAScheduleConflictExceptionsRepository = new Mock<IUAScheduleConflictExceptionsRepository>();
        private readonly Mock<IHHAScheduleConflictExceptionsRepository> mock_hHAScheduleConflictExceptionsRepository = new Mock<IHHAScheduleConflictExceptionsRepository>();
        private readonly Mock<IRepositoryFilterConditions> mock_repositoryFilterConditions = new Mock<IRepositoryFilterConditions>();
        private readonly Mock<IUAEvvExceptionEventsRepository> mock_uAEvvExceptionEventsRepository = new Mock<IUAEvvExceptionEventsRepository>();
        private readonly Mock<IHHAEvvExceptionEventCodesRepository> mock_hHAEvvExceptionEventCodesRepository = new Mock<IHHAEvvExceptionEventCodesRepository>();
        public EvvConfigurationsTests()
        {
            evvConfigurations = new EvvConfigurations(mock_HHAScheduleAuthSettingsRepo.Object, mock_RepositoryUpdateColumn.Object, mock_Mapper.Object, mock_Logger.Object,
                mock_hHAEvvExceptionEventsRepository.Object, mock_uAScheduleConflictExceptionsRepository.Object,
                mock_hHAScheduleConflictExceptionsRepository.Object, mock_repositoryFilterConditions.Object,
                mock_uAEvvExceptionEventsRepository.Object, mock_hHAEvvExceptionEventCodesRepository.Object);
        }

        [Fact]
        public async void GetEvvConfigurations_ReturnList_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            var HHAScheduleAuthSettingsEntityList = new List<HHAScheduleAuthSettingsEntity> { 
                new HHAScheduleAuthSettingsEntity
                {
                    AutoGenerateLateCheckinExceptionMin = 1
                }
            };
            var EvvConfigurationDtoList = new List<EvvConfigurationDto> { 
                new EvvConfigurationDto
                {
                    AutoGenerateLateCheckinExceptionMin = 1
                }
            };

            mock_HHAScheduleAuthSettingsRepo.Setup(x => x.GetHHAScheduleAuthSettingsDetails(HHA, UserId)).ReturnsAsync(HHAScheduleAuthSettingsEntityList);
            mock_Mapper.Setup(x => x.Map<IEnumerable<EvvConfigurationDto>>(HHAScheduleAuthSettingsEntityList)).Returns(EvvConfigurationDtoList);

            //Act
            var ActualEvvConfigDtoList = await evvConfigurations.GetEvvConfigurations(HHA, UserId);

            //Assert
            Assert.Equal(EvvConfigurationDtoList.First().AutoGenerateLateCheckinExceptionMin, ActualEvvConfigDtoList.First().AutoGenerateLateCheckinExceptionMin);
            mock_HHAScheduleAuthSettingsRepo.Verify(x => x.GetHHAScheduleAuthSettingsDetails(HHA, UserId), Times.Once);
            mock_Mapper.Verify(x => x.Map<IEnumerable<EvvConfigurationDto>>(HHAScheduleAuthSettingsEntityList), Times.Once);
        }

        [Fact]
        public void GetEvvConfigurations_ReturnException_OnException()
        {
            //Arrage
            var evvconfiginfo = new List<EvvConfigurationDto>();
            var HHAScheduleAuthSettingsEntityList = new List<HHAScheduleAuthSettingsEntity> {
                new HHAScheduleAuthSettingsEntity
                {
                    AutoGenerateLateCheckinExceptionMin = 1
                }
            };
            var EvvConfigurationDtoList = new List<EvvConfigurationDto> {
                new EvvConfigurationDto
                {
                    AutoGenerateLateCheckinExceptionMin = 1
                }
            };
            mock_HHAScheduleAuthSettingsRepo.Setup(x => x.GetHHAScheduleAuthSettingsDetails(It.IsAny<int>(), It.IsAny<int>())).Throws(new Exception());
            mock_Mapper.Setup(x => x.Map<IEnumerable<EvvConfigurationDto>>(HHAScheduleAuthSettingsEntityList)).Returns(EvvConfigurationDtoList);
            //Act

            //Assert
            Assert.ThrowsAsync<Exception>(async () => { var ActualEvvConfigDtoList = await evvConfigurations.GetEvvConfigurations(It.IsAny<int>(), It.IsAny<int>()); });
            mock_HHAScheduleAuthSettingsRepo.Verify(x => x.GetHHAScheduleAuthSettingsDetails(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            mock_Mapper.Verify(x => x.Map<IEnumerable<EvvConfigurationDto>>(It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void SaveEvvConfigurations_ReturnErrorFalse_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            string columnName = "AutoGenerateNoShowExceptionMin";
            string ColumnValue = "2";
            string Columns = columnName + " = " + ColumnValue;
            string EvvConfigurationProperties = "{\"" + columnName + "\": " + ColumnValue + "}";

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(Columns);
            mock_HHAScheduleAuthSettingsRepo.Setup(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, Columns)).ReturnsAsync(true);


            //Act
            var ActualResult = await evvConfigurations.SaveEvvConfigurations(HHA, UserId, EvvConfigurationProperties);

            //Assert
            Assert.True(!ActualResult);
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Once);
            mock_HHAScheduleAuthSettingsRepo.Verify(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void SaveEvvConfigurations_ReturnErrorTrue_OnErrorFromDBFunc_OR_InvalidData()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            string columnName = "AutoGenerateNoShowExceptionMinn";
            string ColumnValue = "2";
            string EvvConfigurationProperties = "{\"" + columnName + "\": " + ColumnValue + "}";

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(columnName + " = " + ColumnValue);
            mock_HHAScheduleAuthSettingsRepo.Setup(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await evvConfigurations.SaveEvvConfigurations(HHA, UserId, EvvConfigurationProperties);

            //Assert
            Assert.True(ActualResult);
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Once);
            mock_HHAScheduleAuthSettingsRepo.Verify(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void SaveEvvConfigurations_LogError_OnException()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            string columnName = "AutoGenerateNoShowExceptionMin";
            string ColumnValue = "2";
            string EvvConfigurationProperties = "{\"" + columnName + "\": " + ColumnValue + "}";

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(columnName + " = " + ColumnValue);
            mock_HHAScheduleAuthSettingsRepo.Setup(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, It.IsAny<string>())).Throws(new Exception());

            //Act
            var ActualResult = await evvConfigurations.SaveEvvConfigurations(HHA, UserId, EvvConfigurationProperties);

            //Assert
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Once);
            mock_HHAScheduleAuthSettingsRepo.Verify(x => x.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, It.IsAny<string>()), Times.Once);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Once);
        }

        [Fact]
        public async void GetEvvExceptionEvents_ReturnEventsList_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            List<HHAEvvVendorExceptionEventsEntity> hHAEvvVendorExceptionEventsEntity = new List<HHAEvvVendorExceptionEventsEntity>();

            List<EvvExceptionEventsDto> evvExceptionEventsDtos = new List<EvvExceptionEventsDto> {
                new EvvExceptionEventsDto
                {
                    event_code = "LONG_VISTS"
                }
            };

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEvvVendorExceptionEventCodes(HHA, UserId, AggregatorID)).ReturnsAsync(hHAEvvVendorExceptionEventsEntity);
            mock_Mapper.Setup(x => x.Map<IEnumerable<EvvExceptionEventsDto>>(hHAEvvVendorExceptionEventsEntity)).Returns(evvExceptionEventsDtos);

            //Act
            var ActualResult = await evvConfigurations.GetEvvExceptionEventCodes(HHA, UserId, AggregatorID);

            //Assert
            Assert.Equal(evvExceptionEventsDtos.First().event_code, ActualResult.First().event_code);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEvvVendorExceptionEventCodes(HHA, UserId, AggregatorID), Times.Once);
            mock_Mapper.Verify(x => x.Map<IEnumerable<EvvExceptionEventsDto>>(hHAEvvVendorExceptionEventsEntity), Times.Once);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public void GetEvvExceptionEvents_LogError_OnException()
        {
            //Arrange
            int HHA = -222;
            int UserId = 1;
            int AggregatorID = 10;
            List<HHAEvvVendorExceptionEventsEntity> hHAEvvVendorExceptionEventsEntity = new List<HHAEvvVendorExceptionEventsEntity>();
            List<EvvExceptionEventsDto> evvExceptionEventsDtos = new List<EvvExceptionEventsDto>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEvvVendorExceptionEventCodes(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Throws(new Exception());

            //Act

            //Assert
            Assert.ThrowsAsync<Exception>(async () => { var ActualResult = await evvConfigurations.GetEvvExceptionEventCodes(HHA, UserId, AggregatorID); });
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEvvVendorExceptionEventCodes(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            mock_Mapper.Verify(x => x.Map<IEnumerable<EvvExceptionEventsDto>>(hHAEvvVendorExceptionEventsEntity), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void AddExceptionEvent_ReturnErrorFalse_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string events = "[{\"event_code\":\"LONG_VISIT\"}]";
            string exception_code = "1";
            List<_C_EvvExceptionEventsEntity> EvvExceptionEventsEntities = new List<_C_EvvExceptionEventsEntity>();
            List<_UA_ScheduleConflictExceptionsEntity> ScheduleConflictExceptionsEntities = new List<_UA_ScheduleConflictExceptionsEntity>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(HHA, UserId, AggregatorID)).ReturnsAsync(EvvExceptionEventsEntities);
            mock_hHAEvvExceptionEventsRepository.Setup(x => x.AddEVVExceptionEvents(HHA, UserId, AggregatorID, events, exception_code)).ReturnsAsync(false);
            mock_uAScheduleConflictExceptionsRepository.Setup(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(ScheduleConflictExceptionsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.AddHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await evvConfigurations.AddExceptionEvent(HHA, UserId, AggregatorID, events, exception_code);

            //Assert
            Assert.True(!ActualResult);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(HHA, UserId, AggregatorID), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.AddEVVExceptionEvents(HHA, UserId, AggregatorID, events, exception_code), Times.Once);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.AddHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>()), Times.Once);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void AddExceptionEvent_ReturnErrorTrue_OnErrorTrueFromInnerFunc()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string events = "[{\"event_code\":\"LONG_VISIT\"}]";
            string exception_code = "1";
            List<_C_EvvExceptionEventsEntity> EvvExceptionEventsEntities = new List<_C_EvvExceptionEventsEntity>();
            List<_UA_ScheduleConflictExceptionsEntity> ScheduleConflictExceptionsEntities = new List<_UA_ScheduleConflictExceptionsEntity>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(HHA, UserId, AggregatorID)).ReturnsAsync(EvvExceptionEventsEntities);
            mock_hHAEvvExceptionEventsRepository.Setup(x => x.AddEVVExceptionEvents(HHA, UserId, AggregatorID, events, exception_code)).ReturnsAsync(true);
            mock_uAScheduleConflictExceptionsRepository.Setup(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(ScheduleConflictExceptionsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.AddHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>())).ReturnsAsync(true);

            //Act
            var ActualResult = await evvConfigurations.AddExceptionEvent(HHA, UserId, AggregatorID, events, exception_code);

            //Assert
            Assert.True(ActualResult);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(HHA, UserId, AggregatorID), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.AddEVVExceptionEvents(HHA, UserId, AggregatorID, events, exception_code), Times.Once);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.AddHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>()), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void AddExceptionEvent_LogError_OnException()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string events = "[{\"event_code\":\"LONG_VISIT\"}]";
            string exception_code = "1";

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Throws(new Exception());

            //Act
            var ActualResult = await evvConfigurations.AddExceptionEvent(HHA, UserId, AggregatorID, events, exception_code);

            //Assert
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.AddEVVExceptionEvents(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.AddHHAScheduleConflictExceptions(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Once);
        }

        [Fact]
        public async void UpdateExceptionEvent_ReturnErrorFalse_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string EvvExceptionEventIDs = "[{\"EvvExceptionEventID\":1}]";
            string exception_code = "55";
            string Filters = "";
            var Columns = "";

            mock_repositoryFilterConditions.Setup(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build()).Returns(Filters);

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(Columns);

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.UpdateEVVEventInfo(HHA, UserId, Filters, Columns)).ReturnsAsync(true);
            //Act
            var ActualResult = await evvConfigurations.UpdateExceptionEvent(HHA, UserId, AggregatorID, EvvExceptionEventIDs, exception_code);

            //Assert
            Assert.True(!ActualResult);
            mock_repositoryFilterConditions.Verify(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build(), Times.Once);
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.UpdateEVVEventInfo(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void UpdateExceptionEvent_ReturnErrorTrue_OnErrorInInnerFunc()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string EvvExceptionEventIDs = "[{\"INVALID_EvvExceptionEventID\":1}]";
            string exception_code = "55";
            string Filters = "";
            var Columns = "";

            mock_repositoryFilterConditions.Setup(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build()).Returns(Filters);

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(Columns);

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.UpdateEVVEventInfo(HHA, UserId, Filters, Columns)).ReturnsAsync(false);
            //Act
            var ActualResult = await evvConfigurations.UpdateExceptionEvent(HHA, UserId, AggregatorID, EvvExceptionEventIDs, exception_code);

            //Assert
            Assert.True(ActualResult);
            mock_repositoryFilterConditions.Verify(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build(), Times.Once);
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.UpdateEVVEventInfo(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Never);
        }

        [Fact]
        public async void UpdateExceptionEvent_LogError_OnException()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int AggregatorID = 10;
            string EvvExceptionEventIDs = "[{\"INVALID_EvvExceptionEventID\":1}]";
            string exception_code = "55";
            string Filters = "";
            var Columns = "";

            mock_repositoryFilterConditions.Setup(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build()).Throws(new Exception());

            mock_RepositoryUpdateColumn.Setup(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build()).Returns(Columns);

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.UpdateEVVEventInfo(HHA, UserId, Filters, Columns)).ReturnsAsync(false);
            //Act
            var ActualResult = await evvConfigurations.UpdateExceptionEvent(HHA, UserId, AggregatorID, EvvExceptionEventIDs, exception_code);

            //Assert
            Assert.True(ActualResult);
            mock_repositoryFilterConditions.Verify(x => x.InFilter(It.IsAny<string>(), It.IsAny<string>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>())
                .EqualFilter(It.IsAny<string>(), It.IsAny<int>(), FilterConditionDataTypeEnums.integerType, It.IsAny<bool>()).Build(), Times.Once);
            mock_RepositoryUpdateColumn.Verify(x => x.UpdateColumn(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<FilterConditionDataTypeEnums>()).Build(), Times.Never);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.UpdateEVVEventInfo(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Once);
        }

        [Fact]
        public async void DeleteExceptionEvents_ReturnErrorFalse_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int EvvExceptionEventID = 1;
            string event_code = "LONG_VISIT";
            string EvvExceptionEvents = "[{\"event_code\":\"" + event_code + "\",\"EvvExceptionEventID\":" + EvvExceptionEventID + "}]";
            IEnumerable<_UA_EvvExceptionEventsEntity> uA_EvvExceptionEventsEntity = new List<_UA_EvvExceptionEventsEntity>();
            IEnumerable<_C_EvvExceptionEventsEntity> c_EvvExceptionEventsEntities = new List<_C_EvvExceptionEventsEntity>();
            IEnumerable<_UA_ScheduleConflictExceptionsEntity> uA_ScheduleConflictExceptionsEntities = new List<_UA_ScheduleConflictExceptionsEntity>();
            IEnumerable<HHAScheduleConflictExceptionsEntity> hHAScheduleConfigurationsEntities = new List<HHAScheduleConflictExceptionsEntity>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents)).ReturnsAsync(false);
            mock_uAEvvExceptionEventsRepository.Setup(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_EvvExceptionEventsEntity);
            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(HHA, UserId, 0)).ReturnsAsync(c_EvvExceptionEventsEntities);
            mock_uAScheduleConflictExceptionsRepository.Setup(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_ScheduleConflictExceptionsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.GetHHAScheduleConflictExceptions(HHA, UserId)).ReturnsAsync(hHAScheduleConfigurationsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await evvConfigurations.DeleteExceptionEvents(HHA, UserId, EvvExceptionEvents);

            //Assert
            Assert.True(!ActualResult);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents), Times.Once);
            mock_uAEvvExceptionEventsRepository.Verify(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(HHA, UserId, 0), Times.Once);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.GetHHAScheduleConflictExceptions(HHA, UserId), Times.Once);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void DeleteExceptionEvents_ReturnErrorTrue_OnErrorInInnerFunc()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int EvvExceptionEventID = 1;
            string event_code = "LONG_VISIT";
            string EvvExceptionEvents = "[{\"event_code\":\"" + event_code + "\",\"EvvExceptionEventID\":" + EvvExceptionEventID + "}]";
            IEnumerable<_UA_EvvExceptionEventsEntity> uA_EvvExceptionEventsEntity = new List<_UA_EvvExceptionEventsEntity>();
            IEnumerable<_C_EvvExceptionEventsEntity> c_EvvExceptionEventsEntities = new List<_C_EvvExceptionEventsEntity>();
            IEnumerable<_UA_ScheduleConflictExceptionsEntity> uA_ScheduleConflictExceptionsEntities = new List<_UA_ScheduleConflictExceptionsEntity>();
            IEnumerable<HHAScheduleConflictExceptionsEntity> hHAScheduleConfigurationsEntities = new List<HHAScheduleConflictExceptionsEntity>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents)).ReturnsAsync(true);
            mock_uAEvvExceptionEventsRepository.Setup(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_EvvExceptionEventsEntity);
            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(HHA, UserId, 0)).ReturnsAsync(c_EvvExceptionEventsEntities);
            mock_uAScheduleConflictExceptionsRepository.Setup(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_ScheduleConflictExceptionsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.GetHHAScheduleConflictExceptions(HHA, UserId)).ReturnsAsync(hHAScheduleConfigurationsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await evvConfigurations.DeleteExceptionEvents(HHA, UserId, EvvExceptionEvents);

            //Assert
            Assert.True(ActualResult);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents), Times.Once);
            mock_uAEvvExceptionEventsRepository.Verify(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(HHA, UserId, 0), Times.Never);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.GetHHAScheduleConflictExceptions(HHA, UserId), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async void DeleteExceptionEvents_LogError_OnException()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int EvvExceptionEventID = 1;
            string event_code = "LONG_VISIT";
            string EvvExceptionEvents = "[{\"event_code\":\"" + event_code + "\",\"EvvExceptionEventID\":" + EvvExceptionEventID + "}]";
            IEnumerable<_UA_EvvExceptionEventsEntity> uA_EvvExceptionEventsEntity = new List<_UA_EvvExceptionEventsEntity>();
            IEnumerable<_C_EvvExceptionEventsEntity> c_EvvExceptionEventsEntities = new List<_C_EvvExceptionEventsEntity>();
            IEnumerable<_UA_ScheduleConflictExceptionsEntity> uA_ScheduleConflictExceptionsEntities = new List<_UA_ScheduleConflictExceptionsEntity>();
            IEnumerable<HHAScheduleConflictExceptionsEntity> hHAScheduleConfigurationsEntities = new List<HHAScheduleConflictExceptionsEntity>();

            mock_hHAEvvExceptionEventsRepository.Setup(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents)).Throws(new Exception());
            mock_uAEvvExceptionEventsRepository.Setup(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_EvvExceptionEventsEntity);
            mock_hHAEvvExceptionEventsRepository.Setup(x => x.GetHHAEVVEvents(HHA, UserId, 0)).ReturnsAsync(c_EvvExceptionEventsEntities);
            mock_uAScheduleConflictExceptionsRepository.Setup(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(uA_ScheduleConflictExceptionsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.GetHHAScheduleConflictExceptions(HHA, UserId)).ReturnsAsync(hHAScheduleConfigurationsEntities);
            mock_hHAScheduleConflictExceptionsRepository.Setup(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await evvConfigurations.DeleteExceptionEvents(HHA, UserId, EvvExceptionEvents);

            //Assert
            Assert.True(ActualResult);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEvents), Times.Once);
            mock_uAEvvExceptionEventsRepository.Verify(x => x.GetFilteredEVVVendorEvents(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAEvvExceptionEventsRepository.Verify(x => x.GetHHAEVVEvents(HHA, UserId, 0), Times.Never);
            mock_uAScheduleConflictExceptionsRepository.Verify(x => x.GetFilteredScheduleConflictExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.GetHHAScheduleConflictExceptions(HHA, UserId), Times.Never);
            mock_hHAScheduleConflictExceptionsRepository.Verify(x => x.DeleteHHAScheduleConflictExceptions(HHA, UserId, It.IsAny<string>()), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Once);
        }
    }
}

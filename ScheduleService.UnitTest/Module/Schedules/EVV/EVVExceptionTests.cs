﻿using Microsoft.Extensions.Logging;
using Moq;
using Scheduleservice.Services.Schedules.Service;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Microsoft.AspNetCore.Http;
using KanCache.Interfaces;
using Xunit;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.DTO.Application;

namespace ScheduleService.UnitTest.Module.Schedules.EVV
{
    public class EVVExceptionTests
    {
        private readonly EVVException eVVException;
        private readonly Mock<IAutoGenerateException> mock_autoGenerateException = new Mock<IAutoGenerateException>();
        private readonly Mock<IHHAScheduleAuthSettingsRepository> mock_hHAScheduleAuthSettingsRepository = new Mock<IHHAScheduleAuthSettingsRepository>();
        private readonly Mock<IHHAEvvExceptionEventsRepository> mock_c_EvvExceptionEventsRepository = new Mock<IHHAEvvExceptionEventsRepository>();
        private readonly Mock<IUAEvvExceptionEventsRepository> mock_ua_EvvExceptionEventsRepository = new Mock<IUAEvvExceptionEventsRepository>();
        private readonly Mock<IHHAEVVConfigurationsRepository> mock_i_C_EVVConfigurationsRepository = new Mock<IHHAEVVConfigurationsRepository>();
        private readonly Mock<ICaregiverTaskEVVExceptionsRepository> mock_caregiverTaskEVVExceptionsRepository = new Mock<ICaregiverTaskEVVExceptionsRepository>();
        private readonly Mock<IMapper> mock_mapper = new Mock<IMapper>();
        private readonly Mock<IRepositoryFilterConditions> mock_repositoryFilterConditions = new Mock<IRepositoryFilterConditions>();
        private readonly Mock<IUAAgencyDatabaseMasterList> mock_agencyDatabaseMasterList = new Mock<IUAAgencyDatabaseMasterList>();
        private readonly Mock<IUsersRepository> mock_usersRepository = new Mock<IUsersRepository>();
        private readonly Mock<IHttpContextAccessor> mock_accessor = new Mock<IHttpContextAccessor>();
        private readonly Mock<ICacheController> mock_cacheController = new Mock<ICacheController>();
        private readonly Mock<ILogger<EVVException>> mock_Logger = new Mock<ILogger<EVVException>>();
        private readonly Mock<IServiceProvider> mock_ServiceProvider = new Mock<IServiceProvider>();

        public EVVExceptionTests()
        {
            var actionContextDto = new ActionContextDto()
            {
                InstanceCode = "Working"
            };
            var httpContextAccessor = new HttpContextAccessor();
            httpContextAccessor.HttpContext = new DefaultHttpContext();
            httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext] = actionContextDto;
            mock_ServiceProvider.Setup(x => x.GetService(It.IsAny<Type>())).Returns(httpContextAccessor);

            eVVException = new EVVException(mock_agencyDatabaseMasterList.Object, mock_autoGenerateException.Object, mock_hHAScheduleAuthSettingsRepository.Object,
                mock_c_EvvExceptionEventsRepository.Object, mock_ua_EvvExceptionEventsRepository.Object, mock_i_C_EVVConfigurationsRepository.Object,
                mock_caregiverTaskEVVExceptionsRepository.Object, mock_mapper.Object, mock_repositoryFilterConditions.Object, mock_usersRepository.Object, 
                mock_ServiceProvider.Object, mock_cacheController.Object, mock_Logger.Object);
        }

        [Fact]
        public async void ClearScheduleEVVExceptions_ReturnErrorFalse_OnValidInput()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int CGTASKID = 1;
            string TypeOfOperation = "UNDOCHECKOUT";
            string SystemCodes = "";
            string Context = "Checkout";
            int CaregiverTaskEvvExceptionID = 1;

            IEnumerable<CaregiverTaskEVVExceptionsEntity> caregiverTaskEVVExceptionsEntities = new List<CaregiverTaskEVVExceptionsEntity> { 
                new CaregiverTaskEVVExceptionsEntity
                {
                    HHA = HHA,
                    Context = Context,
                    isSystemAutoAdded = true,
                    CaregiverTaskEvvExceptionID = CaregiverTaskEvvExceptionID,
                    SystemCode = SystemCodes
                }
            };

            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(caregiverTaskEVVExceptionsEntities);
            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>())).ReturnsAsync(true);

            //Act
            var ActualResult = await eVVException.ClearScheduleEVVExceptions(HHA, UserId, CGTASKID, TypeOfOperation, SystemCodes);

            //Assert
            Assert.True(ActualResult);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void ClearScheduleEVVExceptions_ReturnErrorTrue_OnErrorInInnerFunc()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int CGTASKID = 1;
            string TypeOfOperation = "UNDOCHECKOUT";
            string SystemCodes = "";
            string Context = "Checkout";
            int CaregiverTaskEvvExceptionID = 1;

            IEnumerable<CaregiverTaskEVVExceptionsEntity> caregiverTaskEVVExceptionsEntities = new List<CaregiverTaskEVVExceptionsEntity> {
                new CaregiverTaskEVVExceptionsEntity
                {
                    HHA = HHA,
                    Context = Context,
                    isSystemAutoAdded = true,
                    CaregiverTaskEvvExceptionID = CaregiverTaskEvvExceptionID,
                    SystemCode = SystemCodes
                }
            };

            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(caregiverTaskEVVExceptionsEntities);
            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await eVVException.ClearScheduleEVVExceptions(HHA, UserId, CGTASKID, TypeOfOperation, SystemCodes);

            //Assert
            Assert.True(!ActualResult);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async void ClearScheduleEVVExceptions_LogError_OnException()
        {
            //Arrange
            int HHA = 1;
            int UserId = 1;
            int CGTASKID = 1;
            string TypeOfOperation = "UNDOCHECKOUT";
            string SystemCodes = "";
            string Context = "Checkout";
            int CaregiverTaskEvvExceptionID = 1;

            IEnumerable<CaregiverTaskEVVExceptionsEntity> caregiverTaskEVVExceptionsEntities = new List<CaregiverTaskEVVExceptionsEntity> {
                new CaregiverTaskEVVExceptionsEntity
                {
                    HHA = HHA,
                    Context = Context,
                    isSystemAutoAdded = true,
                    CaregiverTaskEvvExceptionID = CaregiverTaskEvvExceptionID,
                    SystemCode = SystemCodes
                }
            };

            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>())).Throws(new Exception());
            mock_caregiverTaskEVVExceptionsRepository.Setup(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>())).ReturnsAsync(false);

            //Act
            var ActualResult = await eVVException.ClearScheduleEVVExceptions(HHA, UserId, CGTASKID, TypeOfOperation, SystemCodes);

            //Assert
            Assert.True(!ActualResult);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.GetCaregiverTaskEVVExceptions(HHA, UserId, It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            mock_caregiverTaskEVVExceptionsRepository.Verify(x => x.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, It.IsAny<string>()), Times.Never);
            mock_Logger.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.Once);
        }
    }
}

using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting; 
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Data.Extensions;
using Scheduleservice.Services.Extensions;
using Scheduleservice.AuthorizationServices.Extensions;
using System.Text.Json;
using Scheduleservice.Core.Common.Response;
using System.Collections.Generic;
using FluentValidation.Results;
using Scheduleservice.Services.Configuration.Extensions; 
using Scheduleservice.API.Helpers;
using System;
using Scheduleservice.Subscriber.Extensions;
using System.Net;
using Scheduleservice.Core.Models;

namespace Scheduleservice.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(config => config.OperationFilter<HeaderFilter>());
            services.AddHttpContextAccessor();
            services.AddServicesServiceCollection();
            services.AddServicesAuthServiceCollection();
            services.AddServicesConfigServiceCollection();
            services.AddServicesSubscriberCollection();
            services.ConfigureConnectionProvider<ConnectionProvider>(Configuration)
                .AddDataServiceCollection();
            services.AddCacheServices(Configuration);
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddCors();

            //services.AddCors(o => o.AddPolicy("ScheduleEVVUIService", builder =>
            //{
            //    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            //}));           

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var BasePath = Configuration["ApplicationBasePath"];

            app.UsePathBase(BasePath);
            app.UseDefaultFiles();
            app.UseStaticFiles();
            
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(BasePath + "/swagger/v1/swagger.json", "ScheduleService V1");
                c.RoutePrefix = "swagger";
            });
            string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (env.IsDevelopment() || environment == " ")
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder => builder
               .AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader().SetPreflightMaxAge(new TimeSpan(10000)));
            }
            else
            {
                app.UseCors(policyBuilder => {
                    policyBuilder
                    .WithOrigins("https://localhost:7060")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetPreflightMaxAge(new TimeSpan(10000));
                });
            }

            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = errorFeature.Error;
                    string ErrorMessage = "";

                    if (context.Request.Path.ToString().Contains("exceptions/validate"))
                    {
                        ValidationErrorDetail validationErrorDetail = new ValidationErrorDetail();
                        string problemDetails = "";
                        switch (exception)
                        {
                            case ValidationException validationException:
                                foreach (var x in ((ValidationException)exception).Errors)
                                {
                                   ErrorMessage += x.ErrorMessage + " ";
                                }
                                if (!string.IsNullOrEmpty(ErrorMessage))
                                    ErrorMessage = ErrorMessage.Substring(0, ErrorMessage.Length - 1);

                                validationErrorDetail.error_code = ResponseErrorCodes.ValidationFailed;
                                validationErrorDetail.error_message = ErrorMessage;
                                problemDetails = JsonSerializer.Serialize(validationErrorDetail);
                                break;
                        }

                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        context.Response.ContentType = "application/problem+json";
                        await JsonSerializer.SerializeAsync(context.Response.Body, problemDetails);

                    }
                    else
                    {
                        var problemDetails = Response.Fail<IEnumerable<ValidationFailure>>(ResponseErrorCodes.InternalServerFailed, "An unexpected error occurred! " + exception.Message.ToString(), HttpStatusCode.BadRequest);

                        switch (exception)
                        {
                            case ValidationException validationException:
                                foreach (var x in ((ValidationException)exception).Errors)
                                {
                                    if (!string.IsNullOrEmpty(x.PropertyName))
                                        ErrorMessage += x.PropertyName + " : " + x.ErrorMessage + " ";
                                    else
                                        ErrorMessage += x.ErrorMessage + " ";
                                }
                                if (!string.IsNullOrEmpty(ErrorMessage))
                                    ErrorMessage = ErrorMessage.Substring(0, ErrorMessage.Length - 1);

                                problemDetails = Response.ValidationError<IEnumerable<ValidationFailure>>(ErrorMessage);
                                break;
                        }

                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        context.Response.ContentType = "application/problem+json";
                        await JsonSerializer.SerializeAsync(context.Response.Body, problemDetails);
                    }
                });
            }); 
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

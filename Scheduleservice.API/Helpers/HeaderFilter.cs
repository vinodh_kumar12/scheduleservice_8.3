﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduleservice.API.Helpers
{
    public class HeaderFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();


            var descriptor = context.ApiDescription.ActionDescriptor as ControllerActionDescriptor;

            if (descriptor != null && !descriptor.ControllerName.StartsWith("Subscriber"))
            {

                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "hha",
                    In = ParameterLocation.Header,
                    Schema = new OpenApiSchema { Type = "integer" },
                    Required = true
                });
                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "user",
                    In = ParameterLocation.Header,
                    Schema = new OpenApiSchema { Type = "integer" },
                    Required = true
                });
            }
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "instance_code",
                In = ParameterLocation.Header,
                Schema = new OpenApiSchema { Type = "string" },
                Required = true
            });
        }
    }
}

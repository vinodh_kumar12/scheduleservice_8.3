using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Scheduleservice.API.Controllers
{
    public class BaseController : ControllerBase
    {
        public IActionResult GetActionResultResponse<TResponse>(Response<TResponse> response)
        { //  return this.GetType().Name;
          //string _url = this.Url.ActionLink("ActionName", "ControllerName", new { product = product.Id, price = price, ..}[, protocol = null, host = null, string fragment = null]);
          //  string _url = this.Url.ActionLink("ActionName", "ControllerName", new { product = product.Id, price = price, .. }[, protocol = null, host = null, string fragment = null]);

            var url = this.Url.Link("Default", new { Controller = "MyMvc", Action = "MyAction", param1 = 1 });

            IActionResult actionResult = response.http_status_code switch
            {
                //100
                HttpStatusCode.Continue => BadRequest(new { error_code = response.error_code, error_messages = response.error_message, error_details = response.error_details }),

                HttpStatusCode.OK => Ok(response.response_payload),//200

                HttpStatusCode.Created => Created(url??"", response.response_payload), //201

                HttpStatusCode.NoContent => NoContent(),//204
                //400
                HttpStatusCode.BadRequest =>  BadRequest(new { error_code = response.error_code, error_messages = response.error_message, error_details = response.error_details }),

                HttpStatusCode.NotFound => NotFound(),//404
                //500 or 
                _ => Problem(JsonConvert.SerializeObject(new { error_code = response.error_code, error_messages = response.error_message }),"", ((int)response.http_status_code))

                

            };

            return actionResult;
        }

        public IActionResult GetActionResultResponseNew<TResponse>(Response<TResponse> response)
        { //  return this.GetType().Name;
          //string _url = this.Url.ActionLink("ActionName", "ControllerName", new { product = product.Id, price = price, ..}[, protocol = null, host = null, string fragment = null]);
          //  string _url = this.Url.ActionLink("ActionName", "ControllerName", new { product = product.Id, price = price, .. }[, protocol = null, host = null, string fragment = null]);

            var url = this.Url.Link("Default", new { Controller = "MyMvc", Action = "MyAction", param1 = 1 });

            IActionResult actionResult = response.http_status_code switch
            {
                //100
                HttpStatusCode.Continue => BadRequest(new { error_code = response.error_code, error_messages = response.error_message, error_details = response.error_details }),

                HttpStatusCode.OK => Ok(response.response_payload),//200

                HttpStatusCode.Created => Created(url ?? "", response.response_payload), //201

                HttpStatusCode.NoContent => NoContent(),//204
                //400
                HttpStatusCode.BadRequest => BadRequest(response.response_payload),

                HttpStatusCode.NotFound => NotFound(),//404
                //500 or 
                _ => Problem(JsonConvert.SerializeObject(new { error_code = response.error_code, error_messages = response.error_message }), "", ((int)response.http_status_code))



            };

            return actionResult;
        }


    }
}

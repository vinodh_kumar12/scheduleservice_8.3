﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Services.Schedules.Models;
using Scheduleservice.Services.Schedules.Query;

namespace Scheduleservice.API.Controllers.Schedules
{
    [Route("schedules/")]
    [ApiController]
    public class ScheduleController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ScheduleController> _logger;
        private readonly IMapper _mapper;

        public ScheduleController(IMediator mediator, ILogger<ScheduleController> logger, IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }

        [Route("search/validation_conflict_exceptions")]
        [HttpPost]
        [ProducesDefaultResponseType(typeof(ValidationConflictexceptionsDto))]
        public async Task<IActionResult> GetValidationConflictsExceptions([FromBody] ScheduleSearchValidationConflictexceptionsQuery  validationConflictexceptionsQuery)
        {
            _logger.LogInformation("----- Get validationConflictsQuery");
            var result = await _mediator.Send(validationConflictexceptionsQuery);
            var response= this.GetActionResultResponse(result);
            return response;

        }

        // GET: api/Schedule
        [Route("search/batch")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ScheduleEVVBatch scheduleEVVBatch)
        {
            ScheduleEVVBatchQuery scheduleEVVBatchQuery = _mapper.Map<ScheduleEVVBatchQuery>(scheduleEVVBatch);
            _logger.LogInformation("----- Get ScheduleEVVBatchQuery");
            var result = await _mediator.Send(scheduleEVVBatchQuery);
            return this.GetActionResultResponse(result);
        }

        // GET: api/Schedule
        [Route("search")]  
        [HttpGet]
        public async Task<IActionResult> GetScheduleListsCGTaskIDs([FromQuery] bool return_count, [FromQuery] int page_no, 
            [FromQuery] string[] output_fields, [FromBody] ScheduleSearch scheduleSearch)
        {
            dynamic result = (dynamic)null;

            ScheduleSearchQuery scheduleSearchQuery = _mapper.Map<ScheduleSearchQuery>(scheduleSearch);
            scheduleSearchQuery.return_count = return_count;
            scheduleSearchQuery.page_no = page_no;
            scheduleSearchQuery.output_fields = output_fields;

            _logger.LogInformation("----- Get Schedule CGTASKID's");

            if (!scheduleSearchQuery.return_count)
            {
                result = await _mediator.Send(scheduleSearchQuery);
            }
            else
            {
                var scheduleCountQuery = _mapper.Map<ScheduleSearchCountQuery>(scheduleSearchQuery);
                result = await _mediator.Send(scheduleCountQuery);
            }
            return this.GetActionResultResponse(result);
        }
        // GET: api/Schedule
        [Route("search/Details")]
        [HttpGet]
        public async Task<IActionResult> GetScheduleLists([FromQuery] ScheduleSearchDetailsQuery scheduleSearchQuery)
        {
            _logger.LogInformation("----- Get Schedule Details");
            var result = await _mediator.Send(scheduleSearchQuery);
            return this.GetActionResultResponse(result);
        }

        ///schedules​/{scheduleid}
        [Route("{scheduleid}")]
        [HttpGet]
        public async Task<IActionResult> GetScheduleInfo(int scheduleid)
        {
            _logger.LogInformation("----- Get GetScheduleInfo");
            var result = await _mediator.Send(new ScheduleInfoQuery { CGTask_ID = scheduleid });
            return this.GetActionResultResponse(result);
        }
        
        // GET: api/Schedule
        [Route("{scheduleid}")]
        [HttpPut]
        public async Task<IActionResult> UpdateScheduleInfo(int scheduleid, [FromBody] SaveScheduleinfoCommand saveScheduleinfoCommand)
        {
            _logger.LogInformation("----- Get ScheduleEVVBatchQuery");
            saveScheduleinfoCommand.CgtaskID = scheduleid;
            var result = await _mediator.Send(saveScheduleinfoCommand);
            return this.GetActionResultResponse(result);
        }

        //  GET /schedules/{scheduleid}/Short_Info 
         
        [Route("{scheduleid}/Short_Info")]
        [HttpGet]
        public async Task<IActionResult> GetScheduleShortInfo(int scheduleid,[FromQuery]  BaseRequest baseRequest)
        {
            _logger.LogInformation("----- Get GetScheduleShortInfo");
            var result = await _mediator.Send(new ScheduleShortInfoQuery { CGTask_ID = scheduleid, HHA = baseRequest.HHA, UserId = baseRequest.UserId, PAGEID = baseRequest.PAGEID });
            return this.GetActionResultResponse(result);
        }
        


        //PUT /schedules/{scheduleid}/Approve
        [Route("{scheduleid}/Approve")]
        [HttpPut]
        public async Task<IActionResult> ApproveSchedule(int scheduleid, [FromBody] BaseRequest  baseRequest)
        {
            _logger.LogInformation("----- update ApproveSchedule");
             
            var result = await _mediator.Send(new ApproveScheduleCommand { CGTask_ID = scheduleid, HHA = baseRequest.HHA, UserId = baseRequest.UserId, PAGEID = baseRequest.PAGEID });
            return this.GetActionResultResponse(result);
        }
        //PUT /schedules/{scheduleid}/checkin
        [Route("{scheduleid}/checkin")]
        [HttpPut]
        public async Task<IActionResult>CheckInSchedule(int scheduleid, [FromBody] ScheduleCheckin scheduleCheckin, [FromQuery] bool? confirm_softwarning)
        {
            _logger.LogInformation("----- update scheduleCheckin");
            if(confirm_softwarning==null)
                confirm_softwarning = true;

            var scheduleCheckinCommand = _mapper.Map<ScheduleCheckinCommand>(scheduleCheckin);
            scheduleCheckinCommand.CGTask_ID = scheduleid;
            scheduleCheckinCommand.confirm_softwarning = (bool)confirm_softwarning;

            var result = await _mediator.Send(scheduleCheckinCommand);            
            return this.GetActionResultResponse(result);
        }
        //PUT /schedules/{scheduleid}/checkout
        [Route("{scheduleid}/checkout")]
        [HttpPut]
        public async Task<IActionResult> CheckOutSchedule(int scheduleid, [FromBody] ScheduleCheckout  scheduleCheckout, [FromQuery] bool? confirm_softwarning)
        {
            _logger.LogInformation("----- update checkout");
            if (confirm_softwarning == null)
                confirm_softwarning = true;
            var scheduleCheckoutCommand = _mapper.Map<ScheduleCheckoutCommand>(scheduleCheckout);
            scheduleCheckoutCommand.CGTask_ID = scheduleid;
            scheduleCheckoutCommand.confirm_softwarning = (bool)confirm_softwarning;

            var result = await _mediator.Send(scheduleCheckoutCommand);
            return this.GetActionResultResponse(result);
        }
        //PUT /schedules/{scheduleid}/checkinout
        [Route("{scheduleid}/checkinout")]
        [HttpPut]
        public async Task<IActionResult> CheckInOutSchedule(int scheduleid, [FromBody] ScheduleCheckinout  scheduleCheckinout, [FromQuery] bool? confirm_softwarning)
        {
            _logger.LogInformation("----- update Checkinout");
            if (confirm_softwarning == null)
                confirm_softwarning = true;
            var scheduleCheckinoutCommand = _mapper.Map<ScheduleCheckinoutCommand>(scheduleCheckinout);
            scheduleCheckinoutCommand.CGTask_ID = scheduleid;
            scheduleCheckinoutCommand.confirm_softwarning = (bool)confirm_softwarning;

            var result = await _mediator.Send(scheduleCheckinoutCommand);
            return this.GetActionResultResponse(result);
        }

       // //DELETE /schedules/{scheduleid}/checkin
       // [Route("{scheduleid}/checkin")]
       // [HttpDelete]
       // public async Task<IActionResult> UndoCheckInSchedule(int scheduleid)
       // {
       //     _logger.LogInformation("----- update UndoCheckInSchedule");

       //     var result = await _mediator.Send(new ScheduleUndoCheckinCommand { CGTask_ID = scheduleid });
       //     return this.GetActionResultResponse(result);
       // }
       // //DELETE /schedules/{scheduleid}/checkout
       // [Route("{scheduleid}/checkout")]
       // [HttpDelete]
       // public async Task<IActionResult> UndoCheckOutSchedule(int scheduleid)
       //{
       //     _logger.LogInformation("----- update UndoCheckOutSchedule");

       //     var result = await _mediator.Send(new ScheduleUndoCheckoutCommand { CGTask_ID = scheduleid});
       //     return this.GetActionResultResponse(result);
       // }pos
        // To get EVV Terminal Payer=>Added by shruthi pj
        [Route("{scheduleid}/terminal-payers")]
        [HttpGet]
        public async Task<IActionResult> GetEvvTerminalPayer(int scheduleid)
        {
            _logger.LogInformation("----- Get EVV Terminal Payer");

            //EvvTerminalPayersQuery evvTerminalPayersQuery = new EvvTerminalPayersQuery { CGTask_ID = scheduleid }

            var result = await _mediator.Send(new EvvTerminalPayersQuery { CGTask_ID = scheduleid });
            return this.GetActionResultResponse(result);
        }

         
        [HttpPost]
        public async Task<IActionResult> CreateSchedule([FromBody] ScheduleCreateCommand scheduleCreateCommand)
        {
            _logger.LogInformation("-----Create Schedule");

            var result = await _mediator.Send(scheduleCreateCommand);
            return this.GetActionResultResponse(result);

        }

        [Route("search/count")]
        [HttpGet]
        public async Task<IActionResult> GetEVVSchedulesCount([FromQuery] bool? evv_dirty_flag, [FromBody] EVVSchedulesSearch scheduleSearch)
        {
            EVVScheduleSearchCountQuery eVVScheduleSearchCountQuery = _mapper.Map<EVVScheduleSearchCountQuery>(scheduleSearch);
            eVVScheduleSearchCountQuery.evv_dirty_flag = evv_dirty_flag;

            _logger.LogInformation("----- Get EVV Schedules Count");

            var result = await _mediator.Send(eVVScheduleSearchCountQuery);

            return this.GetActionResultResponse(result);
        }

        [Route("evv/batches/{batchid}/evv_dirty_flag")]
        [HttpPut]
        public async Task<IActionResult> Putevv_dirty_flag(string batchid, [FromQuery] bool? set)
        {
            bool setValue = set ?? true;
            _logger.LogInformation("----- Update ScheduleEVVBatch");
            ScheduleEVVDirtyBatchCommand scheduleEVVDirtyBatchCommand = new ScheduleEVVDirtyBatchCommand { ScheduleEVVBatchID = batchid, set = setValue };
            var result = await _mediator.Send(scheduleEVVDirtyBatchCommand);
            return this.GetActionResultResponse(result);
        }

        [Route("{schedule_uuid}/exceptions/validate")]
        [HttpGet]
        public async Task<IActionResult> ScheduleValidation(string schedule_uuid)
        {
            _logger.LogInformation("----- GET schedule Validation");
            
            var result = await _mediator.Send(new ScheduleValidationQuery { schedule_uuid=schedule_uuid});
            return this.GetActionResultResponseNew(result);
        }
    }
}
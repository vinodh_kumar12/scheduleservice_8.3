﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Models;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Services.Schedules.Models;
using Scheduleservice.Services.Schedules.Query; 


namespace Scheduleservice.API.Controllers.Schedules
{
    [Route("schedule/")]
    [ApiController]
    public class EVVScheduleController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<EVVScheduleController> _logger;
        private readonly IMapper _mapper;
        public EVVScheduleController(IMediator mediator, ILogger<EVVScheduleController> logger, IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }
         

        [HttpGet]
        [Route("evv/exceptions/short_list")]
        public async Task<IActionResult> GetEvvExceptionList([FromQuery] ScheduleEVVExceptionsListQuery _exceptionsQuery)
        {
            _logger.LogInformation("----- Get  ExceptionsQuery ");
            var ExceptionList = await _mediator.Send(_exceptionsQuery);
            return this.GetActionResultResponse(ExceptionList);

        }

        [Route("evv/batches/{batchid}/evv_export_flag")]
        [HttpPut]
        public async Task<IActionResult> Putevv_export_flag( string batchid ,[FromBody]  ScheduleEVVBatchCommand scheduleEVVBatchCommand)
        {
            _logger.LogInformation("----- Update ScheduleEVVBatch");
            scheduleEVVBatchCommand.ScheduleEVVBatchID = batchid; 

            var result = await _mediator.Send(scheduleEVVBatchCommand);
            return this.GetActionResultResponse(result);
        }
        [Route("evv/exceptions/auto_generate")]
        [HttpPost]
        public async Task<IActionResult> autogenerateExceptions(ScheduleEVVExceptionsCommand scheduleEVVExceptionsQuery)
        {
            _logger.LogInformation("----- autogenerateExceptions");
            var result = await _mediator.Send(scheduleEVVExceptionsQuery);
            return this.GetActionResultResponse(result);
        }
        [Route("{scheduleid}/evv/exceptions")]
        [HttpPost]
        public async Task<IActionResult> CreateEVVException(int scheduleid, [FromBody]  AddEVVExceptionCommand addEVVExceptionCommand)
        {
            _logger.LogInformation("----- CreateEVVException");
            addEVVExceptionCommand.CgTaskID = scheduleid;
            var result = await _mediator.Send(addEVVExceptionCommand);
            return this.GetActionResultResponse(result);
        }

        [Route("{scheduleid}/evv/exceptions")]
        [HttpDelete]
        public async Task<IActionResult> DeleteEVVExceptions(int scheduleid, [FromBody]  ClearEVVExceptionsCommand  clearEVVExceptionsCommand)
        {
            _logger.LogInformation("----- DeleteEVVExceptions");
            var result = await _mediator.Send(clearEVVExceptionsCommand);
            return this.GetActionResultResponse(result);
        }

         
        [Route("{scheduleid}/evv/exceptions")]
        [HttpPut]
        public async Task<IActionResult> UpdateScheduleEVVExceptions(int scheduleid,  [FromBody] UpdateEVVExceptionCommand updateEVVExceptionCommand)
        {
            _logger.LogInformation("----- Get validation_rules");
            var result = await _mediator.Send(updateEVVExceptionCommand);
            return this.GetActionResultResponse(result);
        }

        [Route("evv-terminal-payer/auto-register")]
        [HttpPost]
        public async Task<IActionResult> AutoRegisterEvvTerminalPayerSchedules()
        {
            _logger.LogInformation("Auto register evv terminal payer schedules");
            var result = await _mediator.Send(new AutoRegisterEvvTerminalPayerSchedulesCommand());
            return this.GetActionResultResponse(result);
        }

        [Route("evv-terminal-payer/dashboard")]
        [HttpPost]
        public async Task<IActionResult> GetEvvTerminalPayerDashboardCounters([FromBody] EvvTerminalPayerDashboard evvTerminalPayerDashboard = default)
        {
            _logger.LogInformation("Get evv terminal payer dashboard counters based on location");
            var result = await _mediator.Send(new EvvTerminalPayerdashboardQuery { Location = evvTerminalPayerDashboard.Location });
            return this.GetActionResultResponse(result);
        }

        [Route("evv-terminal-payer/search")]
        [HttpGet]
        public async Task<IActionResult> GetEVVTerminalPayerSchedulesLists(int page_no,  [FromQuery] bool terminal_export_registered, [FromBody] EvvTerminalPayerSearchSchedules evvterminalpayersearchschedules)
        {
            _logger.LogInformation("----- Get EVV Terminal Payer Schedule Details");
            EvvTerminalPayerSearchSchedulesQuery evvTerminalPayerSearchSchedulesQuery = _mapper.Map<EvvTerminalPayerSearchSchedulesQuery>(evvterminalpayersearchschedules);
            evvTerminalPayerSearchSchedulesQuery.terminal_export_registered = terminal_export_registered;
            evvTerminalPayerSearchSchedulesQuery.page_no = page_no;
            
            var result = await _mediator.Send(evvTerminalPayerSearchSchedulesQuery);

            return this.GetActionResultResponse(result);
        }

        [Route("/schedule/evv/cds_auth_serivce/evv_export_dirty_flag")]
        [HttpPut]
        public async Task<IActionResult> UpdateCDSAuthServiceExportDirtyFlag()
        {
            _logger.LogInformation("----- Get validation_rules");
            var result = await _mediator.Send(new CDSAuthServiceExportFlagCommand());
            return this.GetActionResultResponse(result);
        }


        [Route("evv-terminal-payer")]
        [HttpPost]
        public async Task<IActionResult> RegisterEvvTerminalPayerSchedules([FromBody] RegisterEvvTerminalPayerSchedulesCommand terminalPayerSchedules = default)
        {
            _logger.LogInformation("Register evv terminal payer schedules");
            var result = await _mediator.Send(terminalPayerSchedules);
            return this.GetActionResultResponse(result);
        }

        /// Get schedule/{scheduleid}/evv/missed-visit-codes
        /// made by anish
        [Route("{scheduleid}/evv/missed-visit-codes")]
        [HttpGet]
        public async Task<IActionResult> GetMissedVisitEVVReasonCodes(int scheduleid)
        {
            _logger.LogInformation("----- Get GetScheduleShortInfo");
            var result = await _mediator.Send(new EvvMissedVisitCodesQuery { CGTask_ID = scheduleid });
            return this.GetActionResultResponse(result);
        }

        // To add the EvvMissedVisitcodeinfo to CaregiverTaskMissedvisitReasons =>added by shruthipj
        [Route("{scheduleid}/evv/{Aggregator_ID}/missed-visit-code")]
        [HttpPost]

        public async Task<IActionResult> MissedVisitsCodesinfo(int scheduleid, [FromBody] MissedvisitcodesinfoCommand missedvisitcodesinfocmd)
        {

            _logger.LogInformation(" Get the Missed visit Code Info");
            missedvisitcodesinfocmd.CGTask_ID = scheduleid;
            var result = await _mediator.Send(missedvisitcodesinfocmd);

            return this.GetActionResultResponse(result);

        }

        [Route("{scheduleid}/evv/missed-visit-codes/{missed_visit_code_id}")]
        [HttpPut]
        public async Task<IActionResult> UpdateMissedVisitsReasonCodesinfo(int scheduleid,int missed_visit_code_id,[FromBody] UpdateMissedVisitReasonCodeInfoCommand updateMissedVisitReasonCodeInfoCommand)
        { 
            _logger.LogInformation("----- Update missedvisitinfo");
            updateMissedVisitReasonCodeInfoCommand.cgtask_id = scheduleid;
            var result = await _mediator.Send(updateMissedVisitReasonCodeInfoCommand );
            return this.GetActionResultResponse(result);
        }

        [Route("{scheduleid}/evv/missed-visit-codes")]
        [HttpDelete]
        public async Task<IActionResult> DeleteMissedVisitsReasonCodesinfo(int scheduleid)
        { 
            _logger.LogInformation("----- Delete missedvisitinfo");
            DeleteMissedVisitReasonCodesCommand deleteMissedVisitReasonCodesCommand = new ();
            deleteMissedVisitReasonCodesCommand.cgtask_id = scheduleid; 

            var result = await _mediator.Send(deleteMissedVisitReasonCodesCommand);
            return this.GetActionResultResponse(result);
        }

        //DELETE /schedule/evv-terminal-payer
        [Route("/schedule/evv-terminal-payer")]
        [HttpDelete]
        public async Task<IActionResult> UnRegisterVisitTerminalPayer([FromBody] DeleteVisitTerminalPayersCommand deleteVisitTerminalPayersCommand)
        {
            _logger.LogInformation("----- delete visit terminal payers");

            var result = await _mediator.Send(deleteVisitTerminalPayersCommand);
            return this.GetActionResultResponse(result);
        }
    }
}
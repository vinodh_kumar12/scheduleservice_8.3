﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Services.Schedules.Query;

namespace Scheduleservice.API.Controllers.Schedules
{
    [Route("schedule/")]
    [ApiController]
    public class ExceptionsController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ScheduleController> _logger;

        public ExceptionsController(IMediator mediator, ILogger<ScheduleController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [Route("exceptions/recalculate/init")]
        [HttpPost]
        public async Task<IActionResult> RecalculateScheduleExceptionsinit([FromBody] RecalculateScheduleExceptionsinitCommand recalculateScheduleExceptionsinitCommand)
        {
            _logger.LogInformation("----- post exceptionsrecalculate_init");
            var result = await _mediator.Send(recalculateScheduleExceptionsinitCommand);
            return this.GetActionResultResponse(result);
        }

        [Route("exceptions/recalculate/batch/{batch_id}")]
        [HttpPost]
        public async Task<IActionResult> RecalculateScheduleExceptionsinit(string batch_id, [FromQuery] BaseRequest baseRequest)
        {
            _logger.LogInformation("----- post exceptionsrecalculate_init");
            var result = await _mediator.Send(new RecalulateScheduleExceptionsBatchCommand { batch_id = batch_id, HHA = baseRequest.HHA, UserId = baseRequest.UserId, PAGEID = baseRequest.PAGEID });
            return this.GetActionResultResponse(result);
        }

        // GET /schedules/exceptions/{scheduleid}/conflict_schedules
        [Route("{scheduleid}/exceptions/conflict_schedules")]
        [HttpGet]
        public async Task<IActionResult> GetconflictSchedules(int scheduleid, [FromQuery] BaseRequest baseRequest)
        {
             
            var result = await _mediator.Send(new ScheduleExceptionsConflictSchedulesQuery{ CGTask_ID = scheduleid, HHA = baseRequest.HHA, UserId = baseRequest.UserId, PAGEID = baseRequest.PAGEID });
            return this.GetActionResultResponse(result);
        }
        //GET /schedules/exceptions/{scheduleid}/validation_rules
        [Route("{scheduleid}/exceptions/validation_rules")]
        [HttpGet]
        public async Task<IActionResult> Getvalidationrules(int scheduleid,[FromQuery] BaseRequest baseRequest)
        {

            var result = await _mediator.Send(new ScheduleExceptionsValidationRulesQuery {CGTask_ID=scheduleid,HHA=baseRequest.HHA,UserId = baseRequest.UserId,PAGEID = baseRequest.PAGEID });
            return this.GetActionResultResponse(result);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Scheduleservice.Core.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduleservice.API.Controllers
{
    [Route("testing/")]
    [ApiController]
    public class TestController : BaseController
    {
        [Route("GetSystemTimeZones")]
        [HttpGet]
        public System.Collections.ObjectModel.ReadOnlyCollection<TimeZoneInfo> GetSystemTimeZones()
        {
            var result = TimeZoneInfo.GetSystemTimeZones();
            return result;
        }

        [Route("GetZoneID")]
        [HttpGet]
        public string GetZoneID(string timeZone)
        {
            var ZoneID = typeof(TimeZones).GetField(timeZone).GetValue(null).ToString();
            return ZoneID;
        }
    }
}

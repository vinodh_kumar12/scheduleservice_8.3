﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Subscriber.Caretracker.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduleservice.API.Controllers.Subscriber
{
    [Route("subscriber/")]
    [ApiController]
    public class SubscriberController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<SubscriberController> _logger;
        private readonly IMapper _mapper;

        public SubscriberController(IMediator mediator, ILogger<SubscriberController> logger, IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }


        [Route("messages/from/crt")]
        [HttpPost]
        public async Task<IActionResult> SubscribeMessage([FromBody] SubscribeMessageCommand caretrackerInputRequestCommand)
        {
            _logger.LogInformation("Invoke RTB Subscribe from CRT");
            var result = await _mediator.Send(caretrackerInputRequestCommand);
            return this.GetActionResultResponse(result);
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Models;
using Scheduleservice.Services.CDS.Query;
using Scheduleservice.Core.DTO.CDS;

namespace Scheduleservice.API.Controllers.CDS
{

    [Route("CDS/")]
    [ApiController]
    public class AuthorizationServiceController: BaseController
    {

        private readonly IMediator _mediator;
        private readonly ILogger<AuthorizationServiceController> _logger;
        private readonly IMapper _mapper;

        public AuthorizationServiceController(IMediator mediator, ILogger<AuthorizationServiceController> logger, IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }


        [HttpGet]
        [Route("planyear/{planyearid}/authorization-service")]
        public async Task<IActionResult> GetEvvExceptionList(int planyearid)
        {
            CDSPlanyearAuthorizationServices cDSPlanyearAuthorizationServices = new CDSPlanyearAuthorizationServices();
            cDSPlanyearAuthorizationServices.PlanYearID = planyearid;
            _logger.LogInformation("----- Get  cDSPlanyearAuthorizationServicesQuery ");
            var ExceptionList = await _mediator.Send(cDSPlanyearAuthorizationServices);
            return this.GetActionResultResponse(ExceptionList);

        }

        [Route("search/CDS_Budgetlineitem_report")]
        [HttpPost]
        [ProducesDefaultResponseType(typeof(CDSBudgetLineItemDTO))]
        public async Task<IActionResult> GetCDSBudgetLineitemReport([FromBody] CDSBudgetLineitemreportQuery CDSBudgetLineitemreportQuery)
        {
            _logger.LogInformation("----- Get CDSBudgetLineitemreportQuery");
            var result = await _mediator.Send(CDSBudgetLineitemreportQuery);
            var response = this.GetActionResultResponse(result);
            return response;

        }
    }
}

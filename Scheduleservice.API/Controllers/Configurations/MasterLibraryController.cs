﻿using System;
using System.Collections.Generic; 
using System.Threading.Tasks;
using AutoMapper;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.MasterLibrary.Query;

namespace Scheduleservice.API.Controllers.Configurations
{
    [Route("configurations/")]
    [ApiController]
    public class MasterLibraryController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<MasterLibraryController> _logger;
        private readonly IMapper _mapper;

        public MasterLibraryController(IMediator mediator, ILogger<MasterLibraryController> logger,
                IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }

        //[Route("Agencies/{Agency_Id}/missed_visit_reasons")]
        //[HttpGet]
        //public async Task<IActionResult> GetMissedVisitReason(int Agency_Id, [FromQuery] string Context)
        //{
        //    _logger.LogInformation("----- Get missedVisitReasons");
        //    var result = await _mediator.Send(new MissedVisitReasonQuery { });
        //    return this.GetActionResultResponse(result);
        //}
    }
}
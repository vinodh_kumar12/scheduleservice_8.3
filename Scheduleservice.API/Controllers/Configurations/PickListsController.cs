﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.PickList.Command;
using Scheduleservice.Services.Configuration.PickList.Query;

namespace Scheduleservice.API.Controllers.Configurations
{
    [Route("configurations/")]
    [ApiController]
    public class PickListsController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PickListsController> _logger;
        private readonly IMapper _mapper;

        public PickListsController(IMediator mediator, ILogger<PickListsController> logger,
                IMapper mapper)
        {
            _mediator = mediator;
            _logger = logger;
            _mapper = mapper;
        }

        [Route("Agencies/{agency_Id}/picklists/{PickList_ID}")]
        [HttpGet]
        public async Task<IActionResult> GetPicklists(int agency_Id, string PickList_ID)
        {
            _logger.LogInformation("----- Get picklists");
             
            var result = await _mediator.Send(new PickListQuery {  HHA= agency_Id, Context= PickList_ID });
            return this.GetActionResultResponse(result);
        }
        [Route("Agencies/{Agency_Id}/picklists/{PickList_ID}")]
        [HttpPost]
        public async Task<IActionResult> CreatePicklists(int Agency_Id, string PickList_ID, [FromBody] CreatePickListCommand createPickListCommand)
        {
            createPickListCommand.HHA = Agency_Id;
            createPickListCommand.PickListCategory = PickList_ID;
            var result = await _mediator.Send(createPickListCommand);
            return this.GetActionResultResponse(result);
        }
        [Route("Agencies/{Agency_Id}/picklists/{PickList_ID}")]
        [HttpPut]
        public async Task<IActionResult> UpdatePicklists(int Agency_Id, string PickList_ID, [FromBody] UpdatePickListCommad updatePickListCommad)
        {
            _logger.LogInformation("----- update Picklists");
            updatePickListCommad.HHA = Agency_Id;
            updatePickListCommad.PickListCategory = PickList_ID;
            var result = await _mediator.Send(updatePickListCommad);
            return this.GetActionResultResponse(result);
        }
        [Route("Agencies/{Agency_Id}/picklists/{PickList_ID}")]
        [HttpDelete]
        public async Task<IActionResult> DeletePicklists(int Agency_Id, string PickList_ID, [FromBody] DeletePickListCommand deletePickListCommand)
        {
            _logger.LogInformation("----- delete Picklists");
            deletePickListCommand.HHA = Agency_Id; 
            deletePickListCommand.PickListCategory = PickList_ID;
            var result = await _mediator.Send(deletePickListCommand);
            return this.GetActionResultResponse(result);
        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Scheduleservice.Services.Configuration.MasterLibrary.Query;

namespace Scheduleservice.API.Controllers.Configurations
{
    [Route("ExceptionConfiguration/")]
    [ApiController]
    public class ExceptionConfigurationController : BaseController
    {
        private readonly IMediator _mediator;
        public ExceptionConfigurationController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [Route("Agency/{agency_Id}/exceptions")]
        [HttpGet]
        public async Task<IActionResult> GetExceptionsMasterlist(int agency_Id)
        {  
            var result = await _mediator.Send(new ExceptionsMasterlistQuery { HHA = agency_Id, UserId = 0, PAGEID = 0 });
            return this.GetActionResultResponse(result);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;

namespace Scheduleservice.API.Controllers.Configurations
{
    [Route("configurations/")]
    [ApiController]
    public class ScheduleConfigurationController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<EVVConfigurationController> _logger;

        public ScheduleConfigurationController(IMediator mediator, ILogger<EVVConfigurationController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [Route("Agency/{agency_Id}")]
        [HttpGet]
        public async Task<IActionResult> GetAgencyScheduleConfigurations(int agency_Id,[FromQuery] int UserId, int PAGEID)
        {

            _logger.LogInformation("-----GetAgencyScheduleConfigurations");
            var test = new AgencyScheduleConfigurationsQuery { HHA = agency_Id, UserId = UserId, PAGEID = PAGEID };
            var result = await _mediator.Send(test);
            return this.GetActionResultResponse(result);
        }
    }
}
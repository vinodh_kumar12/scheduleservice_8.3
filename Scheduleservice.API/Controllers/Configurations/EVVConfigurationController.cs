﻿using System.Text;
using System.Threading.Tasks;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations;

namespace Scheduleservice.API.Controllers.Configurations
{
    [Route("configurations/")]
    [ApiController]
    public class EVVConfigurationController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly ILogger<EVVConfigurationController> _logger;

        public EVVConfigurationController(IMediator mediator, ILogger<EVVConfigurationController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [Route("Agencies/{agency_Id}/EVV")]
        [HttpGet]
        public async Task<IActionResult> GetEVVConfigurations(int agency_Id)
        {
            _logger.LogInformation("----- Get Schedule EVV Configurations");
            var evvConfigurationQuery = new EvvConfigurationQuery { HHA = agency_Id, UserId = 1, PAGEID = 0 };            
            var result = await _mediator.Send(evvConfigurationQuery);
            return this.GetActionResultResponse(result);
            
        }

        [Route("Agencies/{agency_Id}/EVV")]
        [HttpPut]
        public async Task<IActionResult> SaveEVVConfigurations(int agency_Id, [FromBody] string EVVConfigurations)
        {
            _logger.LogInformation("----- Save EVV Configurations");
            EvvConfigurationCommand evvConfigurationCommand = new EvvConfigurationCommand { HHA = agency_Id, UserId = 1, PAGEID = 0, EvvConfigurationProperties = EVVConfigurations };

             
            var records = await _mediator.Send(evvConfigurationCommand);
            return this.GetActionResultResponse(records);
        }
         

        //to get HHA EVV Aggregators configurations
        [Route("Agencies/{agency_Id}/EVV/aggregators")]
        [HttpGet] 
        public async Task<IActionResult> GetHHABranchAggregatorConfigurations(int agency_Id)
        {
            _logger.LogInformation("----- Get HHA Branch Aggrigators configurations");
            HHABranchAggregatorConfigurationQuery hHABranchAggregatorConfigurationQuery = new HHABranchAggregatorConfigurationQuery { HHA = agency_Id};

            
            var result = await _mediator.Send(hHABranchAggregatorConfigurationQuery);
            return this.GetActionResultResponse(result);
        }

         
        //to save EVV Aggregator Setttings
        [Route("Agencies/{agency_Id}/EVV/Aggregator/{EVVConfiguration_ID}")]
        [HttpPut]
        public async Task<IActionResult> SaveHHABranchAggregatorConfigurations(int agency_Id, int EVVConfiguration_ID, [FromBody] EVVAggregatorConfigurationCommand evvAggregatorConfigurationCommand)
        {
            _logger.LogInformation("----- Save eVVBranchAggregatorSettinngsCommand");

            evvAggregatorConfigurationCommand.HHA = agency_Id;

            var result = await _mediator.Send(evvAggregatorConfigurationCommand);
            return this.GetActionResultResponse(result);
        }

        //To get EVV Aggregator Provider Name List
        [Route("Agencies/{agency_Id}/EVV/aggregators_List")]
        [HttpGet]
        public async Task<IActionResult> GetHHAEVVAggregators(int agency_Id)
        {
            _logger.LogInformation("----- Get Schedule EVV Provider List");
            HHAEVVAggregatorsQuery hHAEVVAggregatorsQuery = new HHAEVVAggregatorsQuery { HHA = agency_Id };

             
            var result = await _mediator.Send(hHAEVVAggregatorsQuery);
            return this.GetActionResultResponse(result);
        }

        //To get EVV Schedule Edit Code Master List
        [Route("Agencies/{agency_Id}/EVV/aggregators/{Aggregator_ID}/Edit_Codes_Master_List")]
        [HttpGet]
        public async Task<IActionResult> GetHHAEVVVendorVersionCodes(int agency_Id, int Aggregator_ID)
        {
            _logger.LogInformation("----- Get EVV Code List");
            HHAEVVVendorVersionCodesQuery hHAEVVVendorVersionCodesQuery = new HHAEVVVendorVersionCodesQuery { HHA = agency_Id, EVVVVendorVersionMasterID=Aggregator_ID };

            

            var result = await _mediator.Send(hHAEVVVendorVersionCodesQuery);
            return this.GetActionResultResponse(result);
        }

        //Add new EVV Code and Description
        [Route("Agencies/{agency_Id}/EVV/aggregators/{Aggregator_ID}/Edit_Codes_Master")]
        [HttpPost]
        public async Task<IActionResult> HHAAggregatorAddNewEVVCode(int agency_Id, int Aggregator_ID, [FromBody] HHAAggregatorAddNewEVVCodeCommand addNewEVVCodeCommand)
        {
            _logger.LogInformation("----- Add New Code and Description to the EVVScheduleEditCodeMaster table");
            addNewEVVCodeCommand.HHA = agency_Id;
            addNewEVVCodeCommand.EVVVendorVersionMasterID = Aggregator_ID;

             

            var result = await _mediator.Send(addNewEVVCodeCommand);
            return this.GetActionResultResponse(result);
        }

        //Update EVV Code and Description
        [Route("Agencies/{agency_Id}/EVV/aggregators/{Aggregator_ID}/ Edit_Codes_Master/EditCodesMaster_ID")]
        [HttpPut]
        public async Task<IActionResult> HHAAggregatorUpdateEVVCode(int agency_Id, int Aggregator_ID, [FromBody] HHAAggregatorUpdateEVVCodeCommand updateEVVCode)
        {
            _logger.LogInformation("----- Update Code and Description to the EVVScheduleEditCodeMaster table");
            updateEVVCode.HHA = agency_Id;
            updateEVVCode.EVVVendorVersionMasterID = Aggregator_ID;

            
            var result = await _mediator.Send(updateEVVCode);
            return this.GetActionResultResponse(result);
        }

        //Delete EVV Code and Description
        [Route("Agencies/{agency_Id}/EVV/aggregators/{Aggregator_ID}/ Edit_Codes_Master/EditCodesMaster_ID")]
        [HttpDelete]
        public async Task<IActionResult> HHAAggregatorDeleteEVVCode(int agency_Id, int Aggregator_ID, [FromBody] HHAAggregatorDeleteEVVCodeCommand deleteEVVCode)
        {
            _logger.LogInformation("----- Delete Code and Description to the EVVScheduleEditCodeMaster table");
            deleteEVVCode.HHA = agency_Id;
            deleteEVVCode.EVVVendorVersionMasterID = Aggregator_ID;

            
            var result = await _mediator.Send(deleteEVVCode);
            return this.GetActionResultResponse(result);
        }
        //To get EvvExceptionEvents codes
        [Route("Agencies/{Agency_ID}/EVV/aggregators/{Aggregator_ID}/Exceptions/EventCodes")]
        [HttpGet]
        public async Task<IActionResult> GetEvvExceptionEventCodes(int Agency_ID, int Aggregator_ID)
        {
            _logger.LogInformation("----- Get EVV Exception Events");

            EvvExceptionEventCodesQuery evvExceptionEventsQuery = new EvvExceptionEventCodesQuery { HHA = Agency_ID, Aggregator_ID = Aggregator_ID };

            
            var result = await _mediator.Send(evvExceptionEventsQuery);
            return this.GetActionResultResponse(result);
        }

        //To add new event for Evv Exception
        [Route("Agencies/{Agency_ID}/EVV/aggregators/{Aggregator_ID}/Exceptions/Events")]
        [HttpPost]
        public async Task<IActionResult> PostEvvExceptionEvents(int Agency_ID, int Aggregator_ID, [FromBody] AddEvvExceptionEventsCommad addEvvExceptionEventsCommad)
        {
            _logger.LogInformation("----- Get EVV Exception Events");

             

            var result = await _mediator.Send(addEvvExceptionEventsCommad);
            return this.GetActionResultResponse(result);
        }

        //To update event for Evv Exception
        [Route("Agencies/{Agency_ID}/EVV/aggregators/{Aggregator_ID}/Exceptions/Events")]
        [HttpPut]
        public async Task<IActionResult> PutEvvExceptionEvents(int Agency_ID, int Aggregator_ID, [FromBody] UpdateEvvExceptionEventCommand updateEvvExceptionEventsCommad)
        {
            _logger.LogInformation("----- Get EVV Exception Events");

            
            var result = await _mediator.Send(updateEvvExceptionEventsCommad);
            return this.GetActionResultResponse(result);
        }

        //To delete event for Evv Exception
        [Route("Agencies/{Agency_ID}/EVV/aggregators/{Aggregator_ID}/Exceptions/Events")]
        [HttpDelete]
        public async Task<IActionResult> DeleteEvvExceptionEvents(int Agency_ID, int Aggregator_ID, [FromBody] DeleteEvvExceptionEventCommad deleteEvvExceptionEventCommad)
        {
            _logger.LogInformation("----- Get EVV Exception Events");

            

            var result = await _mediator.Send(deleteEvvExceptionEventCommad);
            return this.GetActionResultResponse(result);
        }

        //To get HHA Aggregator Code Type List
        [Route("Agencies/{agency_Id}/EVV/aggregators/{Aggregator_ID}/Code_Type_List")]
        [HttpGet]
        public async Task<IActionResult> GetHHAAggregatorCodeTypes(int agency_Id, int Aggregator_ID)
        {
            _logger.LogInformation("----- Get EVV Code Type List");
            HHAEVVAggregatorCodeTypeQuery hHAEVVAggregatorCodeTypeQuery = new HHAEVVAggregatorCodeTypeQuery { HHA = agency_Id, EVVVVendorVersionMasterID = Aggregator_ID };

             

            var result = await _mediator.Send(hHAEVVAggregatorCodeTypeQuery);
            return this.GetActionResultResponse(result);
        }

        //To get EvvExceptionEventsMaster
        [Route("Agencies/{Agency_ID}/EVV/aggregators/{Aggregator_ID}/Exceptions/Events")]
        [HttpGet]
        public async Task<IActionResult> GetEVVExceptionEvents(int Agency_ID, int Aggregator_ID)
        {
            _logger.LogInformation("----- Get EVV Exception Events Master");

            EvvExceptionEventsQuery evvExceptionEventsQuery = new EvvExceptionEventsQuery { HHA = Agency_ID, Aggregator_ID = Aggregator_ID };

            

            var result = await _mediator.Send(evvExceptionEventsQuery);
            return this.GetActionResultResponse(result);
        }

    }
}
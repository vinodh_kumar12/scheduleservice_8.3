﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.CDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.CDS.Query
{
    public class CDSPlanyearAuthorizationServices:IRequestWrapper<IEnumerable<PlanYearServicesDTO>>
    {
        public int PlanYearID { get; set; }
    }
}

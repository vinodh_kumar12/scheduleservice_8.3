﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.CDS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.CDS.Query
{
    public class CDSBudgetLineitemreportQuery : IRequestWrapper<CDSBudgetLineItemDTO>
    {
        public int page_id { get; set; }
        public string location { get; set; }
        public string payer { get; set; }
        public string service { get; set; }
        public string budget_line_item { get; set; }
        public int client_id { get; set; }
        public int purpose { get; set; }
        public string sort_column { get; set; }
        public bool sort_direction { get; set; }
        public int page_no { get; set; }
        public string client_status { get; set; }
        public string search_auth_no { get; set; }
        public string auth_service_start_from_date { get; set; }
        public string auth_service_start_to_date { get; set; }
        public string auth_service_end_from_date { get; set; }
        public string auth_service_end_to_date { get; set; }
        public bool include_office_auth { get; set; }
        public string plan_start_from_date { get; set; }
        public string plan_start_to_date { get; set; }
        public string plan_end_from_date { get; set; }
        public string plan_end_to_date { get; set; }
        public int cds_planyear_service_id { get; set; }

    }
}

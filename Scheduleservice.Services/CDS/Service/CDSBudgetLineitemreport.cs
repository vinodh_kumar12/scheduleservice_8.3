﻿using Newtonsoft.Json;
using Scheduleservice.Core.DTO.CDS;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.CDS;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Common.Request;

namespace Scheduleservice.Services.CDS.Service
{
    public class CDSBudgetLineitemreport: ICDSBudgetLineitemreport
    {
        private readonly ICDSClientBudgetLineItemRepository _CDSClientBudgetLineItemRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICDSPayerServiceCategoryMasterRepository _CDSPayerServiceCategoryMasterRepository;
        private readonly ICDSPlanYearServicesRepository _CDSPlanYearServicesRepository;
        private readonly ICDSPlanYearMasterRepository _CDSPlanYearMasterRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly ICDSBudgetLineItemMasterRepository _CDSBudgetLineItemMasterRepository;
       

        public CDSBudgetLineitemreport(ICDSClientBudgetLineItemRepository CDSClientBudgetLineItemRepository,
                                           IRepositoryFilterConditions repositoryFilterConditions,
                                           IPaymentsourcesRepository paymentsourcesRepository,
                                           IClientsRepository clientsRepository,
                                           ICDSPayerServiceCategoryMasterRepository CDSPayerServiceCategoryMasterRepository,
                                           ICDSPlanYearServicesRepository CDSPlanYearServicesRepository,
                                           ICDSPlanYearMasterRepository cDSPlanYearMasterRepository,
                                           ICaregiversRepository caregiversRepository,
                                           ICDSBudgetLineItemMasterRepository CDSBudgetLineItemMasterRepository)
        {
            _CDSClientBudgetLineItemRepository = CDSClientBudgetLineItemRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _paymentsourcesRepository = paymentsourcesRepository;
            _clientsRepository = clientsRepository;
            _CDSPayerServiceCategoryMasterRepository = CDSPayerServiceCategoryMasterRepository;
            _CDSPlanYearServicesRepository = CDSPlanYearServicesRepository;
            _CDSPlanYearMasterRepository = cDSPlanYearMasterRepository;
            _caregiversRepository = caregiversRepository;
            _CDSBudgetLineItemMasterRepository = CDSBudgetLineItemMasterRepository;
        }

        public async Task<CDSBudgetLineItemDTO> GetCDSBudgetLineitemReport(int HHA, int UserId, int PageID, string Location, string Payer,
                                               string Service, string BudgetLineItem, int ClientID, int Purpose, string SortColumn,
                                               bool SortDirection, int pageNo, string ClientStatus, string SearchAuthNo, string AuthServiceStartFromDate,
                                               string AuthServiceStartToDate, string AuthServiceEndFromDate, string AuthServiceEndToDate, bool IncludeOfficeAuth,
                                               string PlanStartFromDate, string PlanStartToDate, string PlanEndFromDate, string PlanEndToDate, int CDSPlanYearServiceId)
        {
            CDSBudgetLineItemDTO cdsBudgetlineitemdtos = new CDSBudgetLineItemDTO();
            PagedList<CDSBudgetLineitemReportList> _cdsbudgetlineitemsPageList = null;


            var clientFilter = "";
            var payerFilter = "";
            var cdspayerservicecategoryfilter = "";
            var cdsbudgetlineitemfilter = "";
            var cdsplanyearservicefilter = "";

            try
            {

                if (!string.IsNullOrEmpty(AuthServiceStartFromDate) && !string.IsNullOrEmpty(AuthServiceStartToDate))
                {
                    cdsplanyearservicefilter +=
                        _repositoryFilterConditions.BetweenFilter(" iif(isnull(CDSPlanYearServiceDetails.effectiveStart,'')!='',CDSPlanYearServiceDetails.effectiveStart,CDSPlanYearMaster.StartDate)", AuthServiceStartFromDate, AuthServiceStartToDate, FilterConditionDataTypeEnums.datetimeType)
                                                    .Build();
                }

                if (!string.IsNullOrEmpty(AuthServiceEndFromDate) && !string.IsNullOrEmpty(AuthServiceEndToDate))
                {
                    cdsplanyearservicefilter +=
                       _repositoryFilterConditions.BetweenFilter(" iif(isnull(CDSPlanYearServiceDetails.effectiveEnd,'')!='',CDSPlanYearServiceDetails.effectiveEnd,CDSPlanYearMaster.EndDate)", AuthServiceStartFromDate, AuthServiceStartToDate, FilterConditionDataTypeEnums.datetimeType)
                       .Build();

                }

                if (!string.IsNullOrEmpty(PlanStartFromDate) && !string.IsNullOrEmpty(PlanStartToDate))
                {
                    cdsplanyearservicefilter +=
                        _repositoryFilterConditions.BetweenFilter("CDSPlanYearMaster.StartDate", PlanStartFromDate, PlanStartToDate, FilterConditionDataTypeEnums.datetimeType)
                                                    .Build();
                }

                if (!string.IsNullOrEmpty(PlanEndFromDate) && !string.IsNullOrEmpty(PlanEndToDate))
                {
                    cdsplanyearservicefilter +=
                        _repositoryFilterConditions.BetweenFilter("CDSPlanYearMaster.EndDate", PlanEndFromDate, PlanEndToDate, FilterConditionDataTypeEnums.datetimeType)
                                                    .Build();
                }

                if (!string.IsNullOrEmpty(SearchAuthNo))
                {
                    cdsplanyearservicefilter +=
                       _repositoryFilterConditions.InFilter("CDSPlanYearServices.AuthorizationNo", SearchAuthNo, FilterConditionDataTypeEnums.stringType)
                                                   .Build();
                }

                if (!IncludeOfficeAuth)
                {
                    cdsplanyearservicefilter +=
                       _repositoryFilterConditions.EqualFilter("CDSPlanYearServices.IsOfficeAuth", false, FilterConditionDataTypeEnums.boolType, true)
                                                   .Build();
                }

                if (CDSPlanYearServiceId != 0)
                {
                    cdsplanyearservicefilter +=
                       _repositoryFilterConditions.EqualFilter("CDSPlanYearServices.CDSPlanYearServiceID", CDSPlanYearServiceId, FilterConditionDataTypeEnums.integerType)
                                                   .Build();
                }

                if (ClientID != 0)
                {
                    cdsplanyearservicefilter +=
                       _repositoryFilterConditions.EqualFilter("CDSPlanYearServices.ClientID", ClientID, FilterConditionDataTypeEnums.integerType)
                                                   .Build();
                }

                var CDSPlanYearServicescolumns = "CDSPlanYearServices.AuthorizationNo, CDSPlanYearServices.CDSPlanYearMasterID,CDSPlanYearServices.CDSPlanYearServiceID," +
                                                 "CDSPlanYearServices.InitialPlanYearServiceID,CDSPlanYearServices.Status,CDSPlanYearServices.IsOfficeAuth,CDSPlanYearServices.ClientID," +
                                                 "CDSPlanYearServices.PayerID,CDSPlanYearServices.PayerCDSServiceCategoryID," +
                                                 "CDSPlanYearServices.isDeleted,CDSPlanYearServiceDetails.effectiveStart,CDSPlanYearServiceDetails.effectiveEnd," +
                                                 "CDSPlanYearMaster.StartDate, CDSPlanYearMaster.EndDate";



                var CDSPlanYearServices = await _CDSPlanYearServicesRepository._S_Schedule_GetAllCDSPlanYearServices(HHA, UserId, CDSPlanYearServicescolumns, cdsplanyearservicefilter);





                string ClientIds = String.Join(",", CDSPlanYearServices.Select(x => x.ClientID).Distinct());
                string CdsPlanyearServiceids = String.Join(",", CDSPlanYearServices.Select(x => x.CDSPlanYearServiceID).Distinct());


                //Clients table
                if (Location == "0" || Location == "ALL")
                {
                    Location = "";
                }

                if (!string.IsNullOrEmpty(ClientStatus))
                {
                    var ClientStatuses = await _clientsRepository.GetClientStatus(HHA, UserId, ClientIds);

                    ClientStatuses = ClientStatuses.Where(x => ClientStatus.Contains(Convert.ToString(x.ClientStatus)));

                    ClientIds = String.Join(",", ClientStatuses.Select(x => x.CLIENT_ID).Distinct());

                }

                if (!string.IsNullOrEmpty(Location))
                {
                    clientFilter +=
                        _repositoryFilterConditions.InFilter("Clients.HHA_BRANCH_ID", Location, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                }
                clientFilter +=
                        _repositoryFilterConditions.InFilter("Clients.STATUS", "Active,Inactive", FilterConditionDataTypeEnums.stringType)
                                                   .InFilter("Clients.CLIENT_ID", ClientIds, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                var clientColumns = "Clients.CLIENT_ID,FIRST_NAME,LAST_NAME,PATIENT_ID, Clients.STATUS";

                var Clients = await _clientsRepository.GetALLClients(HHA, UserId, clientColumns, clientFilter);



                //Payer
                if (Payer == "0" || Payer == "ALL")
                {
                    Payer = "";
                }

                var PayerIds = "";
                if (!string.IsNullOrEmpty(Payer))
                {
                    payerFilter +=
                        _repositoryFilterConditions.InFilter("PAYMENT_SOURCE_ID", Payer, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                }
                else
                {
                    PayerIds = String.Join(",", CDSPlanYearServices.Select(x => x.PayerID).Distinct());

                    payerFilter =
                        _repositoryFilterConditions.InFilter("PAYMENT_SOURCE_ID", PayerIds, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                }

                var payerColumns = "PAYMENT_SOURCE_ID, ORG_NAME";

                var Payers = await _paymentsourcesRepository.GetPaymentsources(HHA, UserId, payerColumns, payerFilter);

                //CDSPayerServiceCategoryMaster

                if (Service == "0" || Service == "ALL")
                {
                    Service = "";
                }

                if (!string.IsNullOrEmpty(Service))
                {
                    cdspayerservicecategoryfilter +=
                       _repositoryFilterConditions.InFilter("CDSPayerServiceCategoryMaster.PayerCDSServiceCategoryID", Service, FilterConditionDataTypeEnums.integerType)
                                                   .Build();

                }
                else
                {
                    var payerServicecategoryIDs = "";
                    payerServicecategoryIDs += String.Join(",", CDSPlanYearServices.Select(x => x.PayerCDSServiceCategoryID).Distinct());

                    cdspayerservicecategoryfilter +=
                        _repositoryFilterConditions.InFilter("CDSPayerServiceCategoryMaster.PayerCDSServiceCategoryID", payerServicecategoryIDs, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                }
                var cdspayerservicecategorycolumns = "CDSPayerServiceCategoryMaster.CategoryName,CDSPayerServiceCategoryMaster.PayerCDSServiceCategoryID";

                var cdspayerservicecategory = await _CDSPayerServiceCategoryMasterRepository._S_Schedule_CDSPayerServiceCategoryMaster(HHA, UserId, cdspayerservicecategorycolumns, cdspayerservicecategoryfilter);

                //CDSClientBudgetLineItem
                if (!string.IsNullOrEmpty(BudgetLineItem))
                {
                    cdsbudgetlineitemfilter +=
                       _repositoryFilterConditions.InFilter("CDSClientBudgetLineItem.CDSPlanYearServiceID", CdsPlanyearServiceids, FilterConditionDataTypeEnums.integerType)
                                                  .InFilter("CDSClientBudgetLineItem.BudgetLineItemMasterID", BudgetLineItem, FilterConditionDataTypeEnums.integerType)
                                                   .Build();
                }
                else
                {
                    cdsbudgetlineitemfilter +=
                         _repositoryFilterConditions.InFilter("CDSClientBudgetLineItem.CDSPlanYearServiceID", CdsPlanyearServiceids, FilterConditionDataTypeEnums.integerType)
                                                    .Build();
                }

                var CDSClientBudgetLineItemcolumns = "CDSClientBudgetLineItem.BudgetLineItemMasterID,CDSClientBudgetLineItem.CaregiverID, CDSClientBudgetLineItem.CDSPlanYearServiceID,CDSClientBudgetLineItem.Amount,CDSClientBudgetLineItem.TotalAmount,CDSClientBudgetLineItem.TotalUsedAmount";

                var cdsClientBudgetLineItems = await _CDSClientBudgetLineItemRepository._S_Schedule_CDSClientBudgetLineItems(HHA, UserId, CDSClientBudgetLineItemcolumns, cdsbudgetlineitemfilter);


                //Caregivers
                var careGiverIds = "";
                careGiverIds += String.Join(",", cdsClientBudgetLineItems.Select(x => x.CaregiverID).Distinct());

                var caregiverFilter = "";

                if (careGiverIds != "")
                {
                    caregiverFilter +=
                       _repositoryFilterConditions.InFilter("Caregivers.CAREGIVER_ID", careGiverIds, FilterConditionDataTypeEnums.integerType)
                                                   .Build();
                }
                var caregiverColumns = "Caregivers.CAREGIVER_ID,Caregivers.LAST_NAME ,Caregivers.FIRST_NAME";

                var Clinician = await _caregiversRepository.GetCaregivers(HHA, UserId, caregiverColumns, caregiverFilter);

                //CDSBudgetLineitemMaster
                var cdsbudgetlineitemmasterids = "";
                cdsbudgetlineitemmasterids += String.Join(",", cdsClientBudgetLineItems.Select(x => x.BudgetLineItemMasterID).Distinct());

                var cdsBudgetLineitemmasterfilter = "";

                if (cdsbudgetlineitemmasterids != "")
                {
                    cdsBudgetLineitemmasterfilter +=
                       _repositoryFilterConditions.InFilter("CDSBudgetLineItemMaster.BudgetLineItemMasterID", cdsbudgetlineitemmasterids, FilterConditionDataTypeEnums.integerType)
                                                   .Build();
                }

                var cdsBudgetLineitemmasterColumns = "CDSBudgetLineItemMaster.BudgetLineItemMasterID,CDSBudgetLineItemMaster.ItemName";
                var CDSBudgetLineitemMasters = await _CDSBudgetLineItemMasterRepository._S_Schedule_GetCDSBudgetLineItemMaster(HHA, UserId, cdsBudgetLineitemmasterColumns, cdsBudgetLineitemmasterfilter);


                var tempCDSBudgetLineitemReportList = (from CDSPlanYearService in CDSPlanYearServices
                                                       from Client in Clients.Where(e => e.CLIENT_ID == CDSPlanYearService.ClientID)
                                                       from cdspayerservicecategorys in cdspayerservicecategory.Where(e => CDSPlanYearService.PayerCDSServiceCategoryID == e.PayerCDSServiceCategoryID)
                                                       from payer in Payers.Where(e => CDSPlanYearService.PayerID == e.PAYMENT_SOURCE_ID)
                                                       select new CDSBudgetLineitemReportList
                                                       {
                                                           ClientLastName = Client.LAST_NAME,
                                                           ClientFirstName = Client.FIRST_NAME,
                                                           ClientID= Client.CLIENT_ID,
                                                           PatientId = Client.PATIENT_ID,
                                                           Payer = payer.ORG_NAME,
                                                           PayerID = payer.PAYMENT_SOURCE_ID,
                                                           Service = cdspayerservicecategorys.CategoryName,
                                                           AuthorizationNo = CDSPlanYearService.AuthorizationNo,
                                                           CDSPlanYearMasterID = CDSPlanYearService.CDSPlanYearMasterID,
                                                           CDSPlanYearServiceID = CDSPlanYearService.CDSPlanYearServiceID,
                                                           InitialAuthID = CDSPlanYearService.InitialPlanYearServiceID,
                                                           Status = CDSPlanYearService.Status,
                                                           IsOfficeAuth = CDSPlanYearService.IsOfficeAuth,
                                                           AutheffectiveStart = !string.IsNullOrEmpty(CDSPlanYearService.effectiveStart) ? CDSPlanYearService.effectiveStart.Substring(0,10) : CDSPlanYearService.StartDate.ToString("d"),
                                                           AutheffectiveEnd = !string.IsNullOrEmpty(CDSPlanYearService.effectiveEnd) ? CDSPlanYearService.effectiveEnd.Substring(0,10) : CDSPlanYearService.EndDate.ToString("d"),
                                                           StartDate = CDSPlanYearService.StartDate.ToString("d"),
                                                           EndDate = CDSPlanYearService.EndDate.ToString("d"),
                                                           LineItemDetails = (from cdsClientBudgetLineItem in cdsClientBudgetLineItems.Where(e => CDSPlanYearService.CDSPlanYearServiceID == e.CDSPlanYearServiceID)
                                                                              from caregiver in Clinician.Where(e => cdsClientBudgetLineItem.CaregiverID == e.CAREGIVER_ID).DefaultIfEmpty()
                                                                              from CDSBudgetLineitemMaster in CDSBudgetLineitemMasters.Where(e => cdsClientBudgetLineItem.BudgetLineItemMasterID == e.BudgetLineItemMasterID)
                                                                              select new LineItemList
                                                                              {
                                                                                  ItemName = CDSBudgetLineitemMaster.ItemName,
                                                                                  Amount = cdsClientBudgetLineItem.Amount,
                                                                                  TotalAmount = cdsClientBudgetLineItem.TotalAmount,
                                                                                  TotalUsedAmount = cdsClientBudgetLineItem.TotalUsedAmount,
                                                                                  EmployeeLastName = caregiver?.LAST_NAME,
                                                                                  EmployeeFirstName = caregiver?.FIRST_NAME,
                                                                                  EmployeeID = caregiver?.CAREGIVER_ID
                                                                              })
                                                       }).Where(e => e.LineItemDetails.Count() != 0);



                //order by
                IList<SortbyColumn> _sortbyColumnlist = new List<SortbyColumn>();
                if (SortColumn == "Client")
                    SortColumn = "ClientLastName";

                _sortbyColumnlist.Add(
                    new SortbyColumn { sort_field = SortColumn, is_ascending_filed = SortDirection }
                    );


                tempCDSBudgetLineitemReportList = SortingList<CDSBudgetLineitemReportList>.ApplySort(tempCDSBudgetLineitemReportList.AsQueryable(), _sortbyColumnlist.AsEnumerable());

                //Pagination
                if ((pageNo == 0 || Purpose == 1) && tempCDSBudgetLineitemReportList.Count() > 0)
                {
                    _cdsbudgetlineitemsPageList = PagedList<CDSBudgetLineitemReportList>.ToPagedList(tempCDSBudgetLineitemReportList.AsQueryable(), pageNo, tempCDSBudgetLineitemReportList.Count());
                }
                else if (pageNo >= 1 && tempCDSBudgetLineitemReportList.Count() > 0)
                {
                    _cdsbudgetlineitemsPageList = PagedList<CDSBudgetLineitemReportList>.ToPagedList(tempCDSBudgetLineitemReportList.AsQueryable(), pageNo, 100);
                }



                if (_cdsbudgetlineitemsPageList != null && _cdsbudgetlineitemsPageList.total_count > 0)
                {
                    cdsBudgetlineitemdtos.CDSBudgetLineitemList = _cdsbudgetlineitemsPageList;
                    cdsBudgetlineitemdtos.total_records = _cdsbudgetlineitemsPageList.total_count;
                    cdsBudgetlineitemdtos.current_page_no = _cdsbudgetlineitemsPageList.current_page;
                    cdsBudgetlineitemdtos.number_records_in_this_page = _cdsbudgetlineitemsPageList.number_records_in_this_page;
                    cdsBudgetlineitemdtos.total_pages = _cdsbudgetlineitemsPageList.total_pages;
                    cdsBudgetlineitemdtos.page_size = _cdsbudgetlineitemsPageList.page_size;
                }
                else
                {
                    cdsBudgetlineitemdtos.CDSBudgetLineitemList = null;
                    cdsBudgetlineitemdtos.total_records = 0;
                    cdsBudgetlineitemdtos.current_page_no = pageNo;
                    cdsBudgetlineitemdtos.number_records_in_this_page = 0;
                    cdsBudgetlineitemdtos.total_pages = 0;
                    cdsBudgetlineitemdtos.page_size = 0;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return cdsBudgetlineitemdtos;
        }  
    }
}

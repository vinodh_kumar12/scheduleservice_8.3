﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.CDS;
using Scheduleservice.Services.CDS.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.Services.CDS;
using System.Threading;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using System.Net;

namespace Scheduleservice.Services.CDS.QueryHandler
{
    public class CDSBudgetLineitemsHandler: IHandlerWrapper<CDSBudgetLineitemreportQuery, CDSBudgetLineItemDTO>
    {
        private readonly ICDSBudgetLineitemreport _CDSBudgetlineitemreport;
        public ActionContextDto _actionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CDSBudgetLineitemsHandler(ICDSBudgetLineitemreport CDSBudgetlineitemreport, IHttpContextAccessor httpContextAccessor)
        {
            _CDSBudgetlineitemreport = CDSBudgetlineitemreport;

            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<CDSBudgetLineItemDTO>> Handle(CDSBudgetLineitemreportQuery request, CancellationToken cancellationToken)
        {
            CDSBudgetLineItemDTO CDSBudgetLineItemreportDTO = new CDSBudgetLineItemDTO();
            try
            {

                CDSBudgetLineItemreportDTO = await _CDSBudgetlineitemreport.GetCDSBudgetLineitemReport(_actionContext.HHA, _actionContext.User, request.page_id, request.location, request.payer, request.service,
                                                       request.budget_line_item, request.client_id, request.purpose, request.sort_column, request.sort_direction, request.page_no,
                                                       request.client_status, request.search_auth_no, request.auth_service_start_from_date, request.auth_service_start_to_date,
                                                       request.auth_service_end_from_date, request.auth_service_end_to_date, request.include_office_auth, request.plan_start_from_date,
                                                       request.plan_start_to_date, request.plan_end_from_date, request.plan_end_to_date, request.cds_planyear_service_id);


                return Response.Ok<CDSBudgetLineItemDTO>(CDSBudgetLineItemreportDTO);
            }
            catch(Exception ex)
            {
                return Response.Fail<CDSBudgetLineItemDTO>(ResponseErrorCodes.InvalidInput, ex.Message,HttpStatusCode.BadRequest, null);
            }
        }
    }
}

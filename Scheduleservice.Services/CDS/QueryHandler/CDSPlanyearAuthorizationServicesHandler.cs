﻿using System;

using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.CDS.Query;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System.Threading;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.DTO.CDS;


namespace Scheduleservice.Services.CDS.QueryHandler
{
    public class CDSPlanyearAuthorizationServicesHandler : IHandlerWrapper<CDSPlanyearAuthorizationServices, IEnumerable<PlanYearServicesDTO>>
    {
        public ActionContextDto ActionContext
        {
            get;
        }
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly ICDSPayerServiceCategoryMasterRepository cDSPayerServiceCategoryMasterRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;


        public CDSPlanyearAuthorizationServicesHandler(IHttpContextAccessor httpContextAccessor, 
            ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
            ICDSPayerServiceCategoryMasterRepository cDSPayerServiceCategoryMasterRepository,
            IRepositoryFilterConditions repositoryFilterConditions)
        {
            _httpContextAccessor = httpContextAccessor;

            this._cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            this.cDSPayerServiceCategoryMasterRepository = cDSPayerServiceCategoryMasterRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

        }
        public async Task<Response<IEnumerable<PlanYearServicesDTO>>> Handle(CDSPlanyearAuthorizationServices request, CancellationToken cancellationToken)
        {

            try
            {
                var cdsplanyearfilters = _repositoryFilterConditions.EqualFilter("CDSplanyearmasterid", request.PlanYearID, FilterConditionDataTypeEnums.integerType).Build();
                var cdsplanyearcolumns = "cdsplanyearservices.CDSPlanYearServiceId,effectiveStart,effectiveEnd,IsOfficeAuth,PayerCDSServiceCategoryID";
                var cDSPlanYearServicesinfo = await _cDSPlanYearServicesRepository._S_Schedule_GetAllCDSPlanYearServices(ActionContext.HHA, ActionContext.User, cdsplanyearcolumns, cdsplanyearfilters);
                string PayerCDSServiceCategoryIDs = string.Join(",", cDSPlanYearServicesinfo.Select(x => x.PayerCDSServiceCategoryID).ToList().Distinct());
                var cdspayercategories = _repositoryFilterConditions.InFilter("PayerCDSServiceCategoryID", PayerCDSServiceCategoryIDs, FilterConditionDataTypeEnums.integerType).Build();

                var CDSplanyearservicecategories = await cDSPayerServiceCategoryMasterRepository._S_Schedule_CDSPayerServiceCategoryMaster(ActionContext.HHA, ActionContext.User, "CategoryName,PayerCDSServiceCategoryID", cdspayercategories);


                var FilteredCdsplanyearservicesList =
                   from planyearservicelist in cDSPlanYearServicesinfo
                   join cdspayercategory in CDSplanyearservicecategories
                   on planyearservicelist.PayerCDSServiceCategoryID
                   equals cdspayercategory.PayerCDSServiceCategoryID


                   select new PlanYearServicesDTO { planyear_service_id=planyearservicelist.CDSPlanYearServiceID ,
                       planyear_service_name=cdspayercategory.CategoryName,effective_start_date
                       =planyearservicelist.effectiveStart,effective_end_date=planyearservicelist.effectiveEnd
                        } ;



                return Response.Ok<IEnumerable<PlanYearServicesDTO>>(FilteredCdsplanyearservicesList);
            }
            catch (Exception ex)
            {
                throw ex;

            }


        }
    }
}

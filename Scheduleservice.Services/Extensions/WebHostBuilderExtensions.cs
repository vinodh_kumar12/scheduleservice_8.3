﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Sinks.MSSqlServer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;

namespace Scheduleservice.Services.Extensions
{
    public static class WebHostBuilderExtensions
    {
        public static IWebHostBuilder ConfigureLogger(this IWebHostBuilder webBuilder)
        {
            webBuilder.UseSerilog((context, config) =>
            {
                var con = context.Configuration.GetConnectionString("LoggerConnectionString");               
                var tableName = context.Configuration["LoggerTableName"];
                var LogTableColumns = new ColumnOptions
                {
                    AdditionalColumns = new Collection<SqlColumn>
                   {
                            new SqlColumn
                            { ColumnName = "UniqueRequestId", PropertyName = "UniqueRequestId", DataType = SqlDbType.NVarChar, DataLength = 100 },
                            new SqlColumn
                            { ColumnName = "HHA", PropertyName = "HHA", DataType = SqlDbType.NVarChar, DataLength = 100 },
                            new SqlColumn
                            { ColumnName = "QueryString", PropertyName = "QueryString", DataType = SqlDbType.NVarChar },
                            new SqlColumn
                            { ColumnName = "RequestHeaders", PropertyName = "RequestHeaders", DataType = SqlDbType.NVarChar },
                            new SqlColumn
                            { ColumnName = "InstanceCode", PropertyName = "InstanceCode", DataType = SqlDbType.NVarChar }
                   }
                };

                var sinkOptions = new MSSqlServerSinkOptions
                {
                    TableName = tableName,
                    AutoCreateSqlTable = true
                };

                //Log Error and above level to DB
                config.WriteTo.MSSqlServer(con, sinkOptions, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error, columnOptions: LogTableColumns)
                .Enrich.FromLogContext();

                //Log Info and above level to Console
                config.WriteTo.Console(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information);
            });

            return webBuilder;
        }
    }
}

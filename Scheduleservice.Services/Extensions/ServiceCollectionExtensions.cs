﻿
using AutoMapper;
using FluentValidation;
using MediatR; 
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Common.Behaviors; 
using Scheduleservice.Services.Schedules.Service;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System.Reflection;
using Scheduleservice.Core.TaskQueue;
using Scheduleservice.Core.Interfaces.TaskQueue;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.RequestValidations;
using Scheduleservice.Services.CDS.Service;
using Scheduleservice.Core.Interfaces.Services.CDS;

namespace Scheduleservice.Services.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServicesServiceCollection(this IServiceCollection services)
        {
            
           
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(HttpContextMiddleware<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationBehaviour<,>));
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(SerilogMiddleware<,>));
           // services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ExceptionMiddleware<,>));

            services.AddMediatR(Assembly.GetExecutingAssembly()); 
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            services.AddScoped<IEVVSchedule, EVVSchedule>();
            services.AddScoped<ISchedule, Schedule>();
            services.AddScoped<IEVVException, EVVException>();
            services.AddScoped<IAutoGenerateException, AutoGenerateException>();
            services.AddScoped <IConflictExceptionsValidationRules, ConflictExceptionsValidationRules>();

            services.AddTransient<IValidationConflictExceptions, ValidationConflictExceptions>();

            services.AddTransient<IRepositoryFilterConditions, RepositoryFilterConditions>();
            services.AddTransient<IRepositoryUpdateColumn, RepositoryUpdateColumn>();
            services.AddHostedService<QueuedHostedService>();
            services.AddTransient<ITaskQueue, TaskQueue>();
            services.AddScoped<IGetScheduleBasicDetail, GetScheduleBasicDetail>();
            services.AddScoped<ICustomValidators, CustomValidators>();
            services.AddTransient<ICDSBudgetLineitemreport, CDSBudgetLineitemreport>();
            return services;
        }
    }
}

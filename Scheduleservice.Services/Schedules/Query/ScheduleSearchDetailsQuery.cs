﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleSearchDetailsQuery : BaseRequest, IRequestWrapper<ScheduleSearchDetailsDto>
    {
        public string search_conditionsJSON { get; set; }
        public string[] output_fields { get; set; }
    }
}

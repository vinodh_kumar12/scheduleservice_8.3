﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleExceptionsValidationRulesQuery : BaseRequest, IRequestWrapper<IEnumerable<ScheduleValidationRulesDto>>
    {
        public int CGTask_ID { get; set; }
    }
}

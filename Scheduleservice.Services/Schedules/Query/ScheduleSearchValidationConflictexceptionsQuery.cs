﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Query
{
   public class ScheduleSearchValidationConflictexceptionsQuery : BaseRequest, IRequestWrapper<ValidationConflictexceptionsDto>
    {
        public string Filters { get; set; }
    }
}

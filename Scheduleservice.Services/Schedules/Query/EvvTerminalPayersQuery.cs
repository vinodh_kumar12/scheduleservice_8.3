﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Services.Schedules.Models;

    namespace Scheduleservice.Services.Schedules.Query
{
    public class EvvTerminalPayersQuery : BaseRequest, IRequestWrapper<IEnumerable<EvvTerminalPayersDto>>
    {
        public int CGTask_ID { get; set; }

    }
}

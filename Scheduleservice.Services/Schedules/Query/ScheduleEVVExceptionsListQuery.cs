﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleEVVExceptionsListQuery : BaseRequest, IRequestWrapper<IEnumerable<ExceptionListDTO>>
    {
        //Null-exception check
         
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int[] Locations { get; set; }
        public string resolved { get; set; }

    }
}

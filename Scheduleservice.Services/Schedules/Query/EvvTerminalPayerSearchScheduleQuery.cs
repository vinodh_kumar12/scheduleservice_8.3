﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Services.Schedules.Models;
namespace Scheduleservice.Services.Schedules.Query
{
    public class EvvTerminalPayerSearchSchedulesQuery : BaseRequest, IRequestWrapper<EVVTerminalPayerSerachSchedulesDto>
    {

        public string schedule_date_from { get; set; }
        public string schedule_date_to { get; set; }
        public string schedule_status { get; set; }
        public string location { get; set; }
        public int client_id { get; set; }
        public string payer_id { get; set; }
        public string terminal_payer_id { get; set; }
        public string terminal_payer_hierarchy { get; set; }
        //public int terminal_aggregator { get; set; }
        public int terminal_configuration_ID { get; set; }
        public string export_status { get; set; }
        public string disc { get; set; }
        public Sort_By sortby { get; set; }
        public bool terminal_export_registered { get; set; }
        public int page_no { get; set; }
        public int purpose { get; set; }
        public string client_status { get; set; }
    }
}

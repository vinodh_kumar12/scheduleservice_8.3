﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule; 

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleInfoQuery : IRequestWrapper<ScheduleInfoDto>
    {
        public int CGTask_ID { get; set; }
    }
}

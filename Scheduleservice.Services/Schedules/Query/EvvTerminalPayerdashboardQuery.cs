﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Query
{
    public class EvvTerminalPayerdashboardQuery : BaseRequest, IRequestWrapper<EvvTerminalPayerdashboardDto>
    {
        public int[] Location { get; set; }
    }
}

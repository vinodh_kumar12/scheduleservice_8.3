﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleValidationQuery : BaseRequest, IRequestWrapper<string>
    {
        public string schedule_uuid { get; set; }
    }
}

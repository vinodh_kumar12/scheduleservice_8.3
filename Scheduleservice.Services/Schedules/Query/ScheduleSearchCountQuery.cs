﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.EVV;
using Scheduleservice.Services.Schedules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleSearchCountQuery : BaseRequest,IRequestWrapper<ScheduleSearchCountEVVInfoDto>
    {
        public bool return_count { get; set; }
        public int page_no { get; set; }
        public string[] output_fields { get; set; }
        public int? client_id { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public bool? is_evv_export_dirty { get; set; }
        public int? is_billable { get; set; }
        public bool? is_invoiced { get; set; }
        public bool? is_payrolled { get; set; }
        public string schedule_status { get; set; }
        public bool? is_evv_aggregator_export_required { get; set; }
        public string payer_ids { get; set; }
        public bool? split_for_billing_parent { get; set; }
        public bool? is_evv_schedule { get; set; }
        public bool? include_split_schedules { get; set; }
        public IList<Sort_By_List> sort_by { get; set; }

        public ScheduleSearchCountQuery()
        {
            sort_by = new List<Sort_By_List>();
        }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Query
{
    public class ScheduleShortInfoQuery : BaseRequest, IRequestWrapper<ScheduleShortInfoDto>
    {
        public int CGTask_ID { get; set; }
    }
}

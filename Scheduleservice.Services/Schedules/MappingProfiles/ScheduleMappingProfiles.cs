﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.DTO.Schedule;
using System;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Services.Schedules.Models;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Enums;
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Core.DTO.EVV;

namespace Scheduleservice.Services.Schedules.MappingProfiles
{
    public class ScheduleMappingProfiles : Profile
    {
        public ScheduleMappingProfiles()
        {
            CreateMap<CaregivertasksEntity, ScheduleBasicListDto>();
            CreateMap<CaregivertasksEntity, SchedulesListDto>();
            CreateMap<ScheduleCheckin, ScheduleCheckinCommand>();
            CreateMap<ScheduleCheckout, ScheduleCheckoutCommand>();
            CreateMap<ScheduleCheckinout, ScheduleCheckinoutCommand>();
            CreateMap<CaregiverTasksTerminalPayerEntity, EvvTerminalPayersDto>();
            CreateMap<CaregiverTaskEvvMissedVisitsReasonsEntity, EvvMissedVisitCodesDto>();
            CreateMap<ScheduleSearchQuery, ScheduleSearchCountQuery>();
            CreateMap<ScheduleSearchQuery, ScheduleFitlersDto>()
                 .ForMember(dest => dest.client_id_list, act => act.MapFrom(src => src.client_id != null ? new[] { src.client_id } : null))
                 .ForMember(dest => dest.IS_BILLED, act => act.MapFrom(src => src.is_invoiced != null ? src.is_invoiced : null))
                 .ForMember(dest => dest.visit_status, act => act.MapFrom(src => src.schedule_status != null ? src.schedule_status : null))
                 .ForMember(dest => dest.IsSplitForBilling, act => act.MapFrom(src => src.split_for_billing_parent != null ? src.split_for_billing_parent : null));

            CreateMap<ScheduleSearchCountQuery, ScheduleFitlersDto>()
                .ForMember(dest => dest.client_id_list, act => act.MapFrom(src => src.client_id != null ? new[] { src.client_id } : null))
                 .ForMember(dest => dest.IS_BILLED, act => act.MapFrom(src => src.is_invoiced != null ? src.is_invoiced : null))
                 .ForMember(dest => dest.visit_status, act => act.MapFrom(src => src.schedule_status != null ? src.schedule_status : null))
                 .ForMember(dest => dest.IsSplitForBilling, act => act.MapFrom(src => src.split_for_billing_parent != null ? src.split_for_billing_parent : null));

            CreateMap<ScheduleEVVBatchQuery, ScheduleFitlersDto>()
                .ForMember(dest => dest.client_id_list, act => act.MapFrom(src => src.client_id_list != null ? src.client_id_list : null))
                .ForMember(dest => dest.IS_BILLED, act => act.MapFrom(src => src.billed != null ? src.billed : null))
                .ForMember(dest => dest.is_billable, act => act.MapFrom(src => src.billable != null ? src.billable : null));

            CreateMap<EVVScheduleSearchCountQuery, ScheduleFitlersDto>()
                .ForMember(dest => dest.client_id_list, act => act.MapFrom(src => src.client_id != null ? new[] { src.client_id } : null))
                .ForMember(dest => dest.IS_BILLED, act => act.MapFrom(src => src.is_invoiced != null ? src.is_invoiced : null))
                .ForMember(dest => dest.visit_status, act => act.MapFrom(src => src.schedule_status != null ? src.schedule_status : null))
                .ForMember(dest => dest.IsSplitForBilling, act => act.MapFrom(src => src.split_for_billing_parent != null ? src.split_for_billing_parent : null));

            CreateMap<int, ScheduleCGTaskIDDto>().ForMember(d => d.CGTaskID, opt => opt.MapFrom(src => src));

            CreateMap<CaregivertasksEntity, ScheduleEVVBatchDto>()
               .ForMember(dest => dest.CGTaskID, act => act.MapFrom(src => src.CGTASK_ID));

            CreateMap<Guid, ScheduleEVVBatchListDto>().ForMember(d => d.BatchID, opt => opt.MapFrom(src => src));

            CreateMap<CaregivertasksEntity, ScheduleShortInfoDto>()
                .ForMember(dest => dest.PLANNED_START_TIME, act => act.MapFrom(src => src.PLANNED_START_TIME.ToTimeAMPMFormat()))
                 .ForMember(dest => dest.PLANNED_END_TIME, act => act.MapFrom(src => src.PLANNED_END_TIME.ToTimeAMPMFormat()))
                 .ForMember(dest => dest.ACCTUAL_START_TIME, act => act.MapFrom(src => src.ACCTUAL_START_TIME.ToTimeAMPMFormat()))
                 .ForMember(dest => dest.ACCTUAL_END_TIME, act => act.MapFrom(src => src.ACCTUAL_END_TIME.ToTimeAMPMFormat()))
                 .ForMember(dest => dest.PLANNED_DATE, act => act.MapFrom(src => src.PLANNED_DATE.ToDateFormat()));

            CreateMap<CaregivertasksEntity, ScheduleInfoDto>()
               .ForMember(dest => dest.PLANNED_START_TIME, act => act.MapFrom(src => src.PLANNED_START_TIME.ToTimeAMPMFormat()))
                .ForMember(dest => dest.PLANNED_END_TIME, act => act.MapFrom(src => src.PLANNED_END_TIME.ToTimeAMPMFormat()))
                .ForMember(dest => dest.ACCTUAL_START_TIME, act => act.MapFrom(src => src.ACCTUAL_START_TIME.ToTimeAMPMFormat()))
                .ForMember(dest => dest.ACCTUAL_END_TIME, act => act.MapFrom(src => src.ACCTUAL_END_TIME.ToTimeAMPMFormat()))
                .ForMember(dest => dest.PLANNED_DATE, act => act.MapFrom(src => src.PLANNED_DATE.ToDateFormat()));
            CreateMap<ScheduleCreateSingleCommand, CreateSchedulesingleDTO>();

            CreateMap<ScheduleCreateCommand, CreateScheduleDTO>();
            //CreateMap<ScheduleCheckinCommand, EditScheduleProperties>()
            //      .ForMember(dest => dest.CheckInSource, act => act.MapFrom(src => Convert.ToByte(src.checkin_source)))
            //      .ForMember(dest => dest.isAttested, act => act.MapFrom(src => src.is_attestated == "1" ? true : false)) 
            //      .ForMember(dest => dest.CHECK_IN_LOCATION, act => act.MapFrom(src => src.caregiver_geolocation))
            //      .ForMember(dest => dest.ACCTUAL_START_TIME, act => act.MapFrom(src => Convert.ToDateTime(src.schedule_date + " " + src.check_in)))
            //      .ForMember(dest => dest.STATUS, act => ScheduleStatusEnums.In_Progress.ToString())
            //      .ForMember(dest => dest.EVVOriginalCheckinTime, act => act.MapFrom(src => Convert.ToDateTime(src.schedule_date + " " + src.check_in)))
            //       ;


            CreateMap<EvvTerminalPayerSearchSchedules, EvvTerminalPayerSearchSchedulesQuery>()
                .ForMember(dest => dest.export_status, act => act.MapFrom(src => src.export_status == null ? "" : src.export_status))
                .ForMember(dest => dest.schedule_status, act => act.MapFrom(src => src.schedule_status == null ? "" : src.schedule_status))
                .ForMember(dest => dest.schedule_date_from, act => act.MapFrom(src => src.schedule_date_from == null ? "" : src.schedule_date_from))
                .ForMember(dest => dest.schedule_date_to, act => act.MapFrom(src => src.schedule_date_to == null ? "" : src.schedule_date_to))
                .ForMember(dest => dest.terminal_payer_hierarchy, act => act.MapFrom(src => src.terminal_payer_hierarchy == null ? "" : src.terminal_payer_hierarchy))
                ;

            CreateMap<ScheduleSearch, ScheduleSearchQuery>();
            CreateMap<EVVSchedulesSearch, EVVScheduleSearchCountQuery>();
            CreateMap<ScheduleEVVBatch, ScheduleEVVBatchQuery>();

            CreateMap<ValidationErrorInfo, ScheduleValidationErrorsDto>(); 


            CreateMap<MissedvisitcodesinfoCommand, MissedVisitCodesDto>();
            CreateMap<ValidationErrorInfo, ValidationErrorDetail>()
                .ForMember(dest => dest.error_code, act => act.MapFrom(src => src.validation_type != null ? src.validation_type : null))
                 .ForMember(dest => dest.error_message, act => act.MapFrom(src => src.validation_error_message != null ? src.validation_error_message : null));
        }
    }
}

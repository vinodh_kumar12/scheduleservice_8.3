﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ServiceTerminatedValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly ServiceCodesEntity serviceInfo;

        public ServiceTerminatedValidationHandler(int hha, int user, ServiceCodesEntity serviceData)
        {
            HHA = hha;
            User = user;
            serviceInfo = serviceData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Validate fields
                // Service terminated
                if (serviceInfo.IsTerminated == true && serviceInfo.effectiveEndDate < newScheduleData.PLANNED_DATE)
                {
                    result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Cannot change date since service terminated before selected date" });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

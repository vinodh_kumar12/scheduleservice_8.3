﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleApproveValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        public ScheduleApproveValidationHandler(int hha, int user)
        {
            HHA = hha;
            User = user;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Chart not apporved
                if (!newScheduleData.TimesheetSubmitted ?? false && newScheduleData.STATUS == ScheduleStatusEnums.Approved.ToString()) 
                {
                    result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Cannot approve schedule since chart is not submitted" });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

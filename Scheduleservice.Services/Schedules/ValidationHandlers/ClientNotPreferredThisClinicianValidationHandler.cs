﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClientNotPreferredThisClinicianValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IClientsRepository _clientsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ClientNotPreferredThisClinicianValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       IClientsRepository clientsRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _clientsRepository = clientsRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var Columns = "NON_PREFERRED_CLINICIAN_ID,CLIENT_ID,CLINICIAN_ID,EffectiveFrom";
                var Filter = _repositoryFilterConditions.EqualFilter("CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                    .Build();

                var nonPreferredClinicians = _clientsRepository.GetNonPreferredClinicians(HHA, UserID, Columns, Filter).Result;

                if (nonPreferredClinicians.Any())
                {
                    if (nonPreferredClinicians.Where(e => e.CLINICIAN_ID == caregivertasksEntity.CAREGIVER)
                                                .Where(e => e.EffectiveFrom <= caregivertasksEntity.PLANNED_DATE || e.EffectiveFrom == null).Any())
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.CLIENTTNOTPREFERREDTHISCLINICIAN.ToString(), validation_error_message = "Client does not prefer this clincian." });
                }
            }
            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq; 
using Scheduleservice.Core.Schedules;
using Scheduleservice.Core.Models;
using GeoCoordinatePortable;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class GeoFencingValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHHAEVVScheduleConfigurationRepository _hHAEVVScheduleConfigurationRepository;
        private readonly IHHAEVVScheduleConfigurationGroupRepository _hHAEVVScheduleConfigurationGroupRepository; 
        private readonly IClientTreatmentAddressRepository _clientTreatmentAddressRepository;
        private readonly IAddressRepository _addressRepository;
        public GeoFencingValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       IRepositoryFilterConditions repositoryFilterConditions,
                                                       IHHAEVVScheduleConfigurationRepository hHAEVVScheduleConfigurationRepository,
                                                       IHHAEVVScheduleConfigurationGroupRepository hHAEVVScheduleConfigurationGroupRepository,
                                                      
                                                       IClientTreatmentAddressRepository clientTreatmentAddressRepository,
                                                       IAddressRepository addressRepository
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _repositoryFilterConditions = repositoryFilterConditions;
            _hHAEVVScheduleConfigurationRepository = hHAEVVScheduleConfigurationRepository;
            _hHAEVVScheduleConfigurationGroupRepository = hHAEVVScheduleConfigurationGroupRepository;
            
            _clientTreatmentAddressRepository = clientTreatmentAddressRepository;
            _addressRepository = addressRepository;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled && caregivertasksEntity.isEvvschedule 
                && 
                ((caregivertasksEntity.ACCTUAL_START_TIME != null && (caregivertasksEntity.CheckInSource == 0|| caregivertasksEntity.CheckInSource == 4 || caregivertasksEntity.CheckInSource == 5))                
                  ||
                  (caregivertasksEntity.ACCTUAL_END_TIME != null && (caregivertasksEntity.CheckOutSource == 0 || caregivertasksEntity.CheckOutSource == 4 || caregivertasksEntity.CheckOutSource == 5))                
                 )
                )
            {
                int EvvScheduleConfigurationGroupID = 0;
                int TreatmentAddressID = 0;
                double ClientLatitude = 0;
                double ClientLongitude = 0;
                double ClinicianLatitude = 0;
                double ClinicianLongitude = 0;
                bool LocationValidation_isMandatory = false;
                bool LocationValidation_EnableGeoFencing = false;
                int LocationValidation_MaxDistance = 0;
                string LocationValidation_OutsideFenceRule = "";
                string LocationValidation_NoLocationRule = "";
                int ClientTreatmentLocationID = 0;

                bool IsEVVSchedule = caregivertasksEntity.isEvvschedule;
                // var ClientDetails = _clientsRepository.GetClientsBasicInfo(HHA, UserID, caregivertasksEntity.CLIENT_ID).Result;
                var ClientDetails = validateScheduleRules.clientsEntity;
               int HHABranchID = ClientDetails.HHA_BRANCH_ID;

                if (IsEVVSchedule)
                {
                    var HHAEVVScheduleConfigurationFilter = _repositoryFilterConditions.EqualFilter("HHABranchID", HHABranchID, FilterConditionDataTypeEnums.integerType)
                                                            .Build();

                    var _hHAEVVScheduleConfigurations =  _hHAEVVScheduleConfigurationRepository.GetHHAEVVScheduleConfigurationSettings(HHA, UserID, "EvvScheduleConfigurationGroup.EvvScheduleConfigurationGroupID", HHAEVVScheduleConfigurationFilter).Result;

                    EvvScheduleConfigurationGroupID = _hHAEVVScheduleConfigurations.Select(x => x.EvvScheduleConfigurationGroupID).FirstOrDefault();

                    if(EvvScheduleConfigurationGroupID>0)
                    {
                        var HHAEVVScheduleConfigurationGroupFilter = _repositoryFilterConditions.EqualFilter("EvvScheduleConfigurationGroupID",EvvScheduleConfigurationGroupID, FilterConditionDataTypeEnums.integerType)
                                                                    .Build();
                        var _hHAEVVScheduleConfigurationGroupsetting = _hHAEVVScheduleConfigurationGroupRepository.GetHHAEVVScheduleConfigurationGroupSettings(HHA, UserID, "EvvScheduleConfigurationGroupID,LocationValidation_isMandatory,LocationValidation_EnableGeoFencing,LocationValidation_MaxDistance,LocationValidation_NoLocationRule,LocationValidation_OutsideFenceRule", HHAEVVScheduleConfigurationGroupFilter).Result;

                        LocationValidation_isMandatory = _hHAEVVScheduleConfigurationGroupsetting.LocationValidation_isMandatory;
                        LocationValidation_EnableGeoFencing = _hHAEVVScheduleConfigurationGroupsetting.LocationValidation_EnableGeoFencing;
                        LocationValidation_MaxDistance = _hHAEVVScheduleConfigurationGroupsetting.LocationValidation_MaxDistance;
                        LocationValidation_OutsideFenceRule = _hHAEVVScheduleConfigurationGroupsetting.LocationValidation_OutsideFenceRule;
                        LocationValidation_NoLocationRule = _hHAEVVScheduleConfigurationGroupsetting.LocationValidation_NoLocationRule;

                        if(caregivertasksEntity.ACCTUAL_END_TIME.HasValue && !string.IsNullOrEmpty(caregivertasksEntity.CHECK_OUT_LOCATION))
                        {
                            ClientTreatmentLocationID = Convert.ToInt32(caregivertasksEntity.CheckoutTreatmentLocation);
                            string[] clincian_coordinates = caregivertasksEntity.CHECK_OUT_LOCATION.Split(',');
                            ClinicianLatitude = Convert.ToDouble(clincian_coordinates[0]);
                            ClinicianLongitude = Convert.ToDouble(clincian_coordinates[1]);
                        }
                        else if (caregivertasksEntity.ACCTUAL_START_TIME.HasValue && !string.IsNullOrEmpty(caregivertasksEntity.CHECK_IN_LOCATION))
                        {
                            ClientTreatmentLocationID = Convert.ToInt32(caregivertasksEntity.CheckinTreatmentLocation);
                            string[] clincian_coordinates = caregivertasksEntity.CHECK_IN_LOCATION.Split(',');
                            ClinicianLatitude = Convert.ToDouble(clincian_coordinates[0]);
                            ClinicianLongitude = Convert.ToDouble(clincian_coordinates[1]);
                        }

                        if (ClientTreatmentLocationID > 0)
                        {
                            var clientTreatmentAddress = _clientTreatmentAddressRepository._S_Schedule_GetClientTreatmentAddress(HHA, UserID, ClientTreatmentLocationID).Result;
                            TreatmentAddressID = clientTreatmentAddress.TreatmentAddressID;
                        }
                        else
                        {
                            TreatmentAddressID = ClientDetails.ADDRESS;
                        }

                        if(TreatmentAddressID>0)
                        {
                            var AddressEntity = _addressRepository._S_Schedule_GetTreatmentLocation(HHA, UserID, TreatmentAddressID).Result;
                            ClientLatitude = Convert.ToDouble(string.IsNullOrEmpty(AddressEntity.Latitude)? 0: AddressEntity.Latitude);
                            ClientLongitude = Convert.ToDouble(string.IsNullOrEmpty(AddressEntity.Longitude)? 0: AddressEntity.Longitude);
                        }

                        if(ClientLatitude != 0 && ClientLongitude != 0 && LocationValidation_isMandatory && LocationValidation_EnableGeoFencing && (!string.IsNullOrEmpty(caregivertasksEntity.CHECK_OUT_LOCATION) || !string.IsNullOrEmpty(caregivertasksEntity.CHECK_IN_LOCATION)))
                        {
                            GeoCoordinate clientgeocoordinate = new GeoCoordinate(ClientLatitude, ClientLongitude);
                            GeoCoordinate cliniciangeocoordinate = new GeoCoordinate(ClinicianLatitude, ClinicianLongitude);
                            double distance = (double)Common.GetDistanceFromSource(clientgeocoordinate, cliniciangeocoordinate);
                            
                            if (distance > LocationValidation_MaxDistance && (LocationValidation_OutsideFenceRule == "HardStop" || LocationValidation_OutsideFenceRule == "ReasonCode"))
                            {
                                result.Add(new ValidationErrorInfo() { validation_type = "GEOLOCATION", validation_error_message = "Clinician Location is outside the set distance from client." });
                            }
                        }
                        else if (LocationValidation_isMandatory && (ClinicianLatitude == 0 || ClinicianLongitude == 0))
                        {
                           if(LocationValidation_NoLocationRule == "HardStop" || LocationValidation_NoLocationRule == "ReasonCode")
                            {
                                result.Add(new ValidationErrorInfo() { validation_type = "GEOLOCATION", validation_error_message = "Geo location is mandatory" });
                            }
                        }
                    }
                }
            }

            //if (_nextValidationHandler != null)
            //    result.AddRange(_nextValidationHandler.HandleValidation(caregivertasksEntity));

            return result; 
        }
    }
}
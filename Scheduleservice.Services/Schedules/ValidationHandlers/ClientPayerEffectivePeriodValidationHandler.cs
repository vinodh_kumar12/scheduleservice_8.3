﻿using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClientPayerEffectivePeriodValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriods;

        public ClientPayerEffectivePeriodValidationHandler(int hha, int UserID,
            IEnumerable<ClientPayerEffectivePeriodsEntity> ClientPayerEffectivePeriods)
        {
            HHA = hha;
            User = UserID;
            clientPayerEffectivePeriods = ClientPayerEffectivePeriods;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Client Payer Effective Period
                if (clientPayerEffectivePeriods.Any())
                {
                    if (!clientPayerEffectivePeriods
                        .Any(x =>
                        (x.EffectiveStart < newScheduleData.PLANNED_DATE || x.EffectiveStart == DateTime.MinValue)
                        && (x.EffectiveEnd > newScheduleData.PLANNED_DATE || x.EffectiveEnd == DateTime.MinValue)
                        )
                    )
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Cannot change payer since the schedule date is out of client payment source effective period" });
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }
    }
}

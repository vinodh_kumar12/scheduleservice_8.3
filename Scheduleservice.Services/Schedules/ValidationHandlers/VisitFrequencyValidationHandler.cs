﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    class VisitFrequencyValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        public VisitFrequencyValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregivertasksRepository caregivertasksRepository
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregivertasksRepository = caregivertasksRepository;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (_scheduleValidationRulesDto.Validationenabled)
            {
                if (caregivertasksEntity.CGTASK_ID > 0)
                {
                    if (!_caregivertasksRepository.GetVisitFrequencyValidation(HHA, UserID, caregivertasksEntity.CGTASK_ID, caregivertasksEntity.ACCTUAL_START_TIME.ToString(), caregivertasksEntity.ACCTUAL_END_TIME.ToString()).Result)
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.VF.ToString(), validation_error_message = "Visit Frequency validation exists." });

                    }
                }
                else
                {
                   DataTable dt = new DataTable();
                    dt.Columns.Add("CGTaskID");
                    dt.Columns.Add("Payer"); 
                    dt.Columns.Add("serviceID");
                    dt.Columns.Add("planneddate");
                    dt.Columns.Add("plannedstarttime");
                    dt.Columns.Add("plannedendtime");
                    dt.Columns.Add("CheckInTime");
                    dt.Columns.Add("CheckOutTime");
                    dt.Columns.Add("EditedHours");
                    dt.Columns.Add("TobeValidated");

                    DataRow dr = dt.NewRow();
                    dr["CGTaskID"] = 0 ;
                    dr["Payer"] = caregivertasksEntity.PAYMENT_SOURCE; 
                    dr["serviceID"] = caregivertasksEntity.SERVICECODE_ID; 
                    dr["planneddate"] = caregivertasksEntity.PLANNED_DATE.ToString("MM/dd/yyyy");
                    dr["plannedstarttime"] = caregivertasksEntity.PLANNED_START_TIME ;
                    dr["plannedendtime"] = caregivertasksEntity.PLANNED_END_TIME;

                    dt.Rows.Add(dr);
                    if (! _caregivertasksRepository.GetCreateScheduleVisitFrequencyValidation(HHA, UserID, caregivertasksEntity.CLIENT_ID, caregivertasksEntity.PAYMENT_SOURCE, dt,1).Result)
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.VF.ToString(), validation_error_message = "Visit Frequency validation exists." });

                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

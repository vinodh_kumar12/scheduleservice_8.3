﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class PastVisitValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly int HHABranchID;

        public PastVisitValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                        IRepositoryFilterConditions repositoryFilterConditions,
                                                        IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository,
                                                        ICaregivertasksRepository caregivertasksRepository,int hhabranchid)
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _repositoryFilterConditions = repositoryFilterConditions;
            _hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository;
            _caregivertasksRepository = caregivertasksRepository;
            HHABranchID = hhabranchid;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity caregivertasksEntity)
        {
             if (_scheduleValidationRulesDto!=null && _scheduleValidationRulesDto.Validationenabled)
            {
                
                DateTime CheckinTime = new DateTime();
                DateTime CheckoutTime = new DateTime();

                if (caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                    CheckinTime =  Convert.ToDateTime(caregivertasksEntity.ACCTUAL_START_TIME);
                if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue)
                    CheckoutTime = Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME);

                var GetCurrentTimeTostring = _hHABranchScheduleConfigurationsRepository._VC_GetCurrentTime(HHA, UserID, HHABranchID, "Date_Time").Result;
                DateTime GetCurrentTime = Convert.ToDateTime(GetCurrentTimeTostring);

                var Columns = "CGTASK_ID,ACCTUAL_START_TIME,ACCTUAL_END_TIME";
                var OldScheduleDetail = new CaregivertasksEntity();
          
                  
                if (caregivertasksEntity.CGTASK_ID > 0)
                {

                    var Filter = _repositoryFilterConditions.EqualFilter("CGTASK_ID", caregivertasksEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                                        .Build();
                    OldScheduleDetail = _caregivertasksRepository.GetScheduleBasicList(HHA, UserID, Columns, Filter).Result.FirstOrDefault();
                }
                if ((caregivertasksEntity.isEvvschedule && _scheduleValidationRulesDto.ValidateForEVV) || (!caregivertasksEntity.isEvvschedule && _scheduleValidationRulesDto.ValidateForNonEVV))
                {
                    if (_scheduleValidationRulesDto.grace_period_mins > 0)
                        GetCurrentTime = GetCurrentTime.AddDays(-_scheduleValidationRulesDto.grace_period_mins);

                    if ((OldScheduleDetail.ACCTUAL_START_TIME == null && caregivertasksEntity.ACCTUAL_START_TIME != null) || (OldScheduleDetail.ACCTUAL_START_TIME==null && caregivertasksEntity.ACCTUAL_START_TIME != null && OldScheduleDetail.ACCTUAL_END_TIME==null && caregivertasksEntity.ACCTUAL_END_TIME!=null))
                    {
                        if (CheckinTime.Date < GetCurrentTime.Date)
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "PASTVISIT", validation_error_message = _scheduleValidationRulesDto.ValidationError });
                        }
                    }
                    else if(OldScheduleDetail.ACCTUAL_END_TIME==null && caregivertasksEntity.ACCTUAL_END_TIME!=null)
                    {
                        if (CheckoutTime.Date < GetCurrentTime.Date)
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "PASTVISIT", validation_error_message = _scheduleValidationRulesDto.ValidationError });
                        }
                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(caregivertasksEntity));

            return result;
        }
    }
}

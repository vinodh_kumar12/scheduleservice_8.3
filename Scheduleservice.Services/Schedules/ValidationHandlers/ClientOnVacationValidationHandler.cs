﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClientOnVacationValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IClientsRepository _clientsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ClientOnVacationValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       IClientsRepository clientsRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _clientsRepository = clientsRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var Columns = "ClientVacationMasterID,ClientID,StartDate,EndDate";
                var Filter = _repositoryFilterConditions.EqualFilter("ClientID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                    .Build();

                var clientVacations = _clientsRepository.GetClientVacationMaster(HHA, UserID, Columns, Filter).Result;

                if (clientVacations.Any())
                {
                    var StartTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME == null ? caregivertasksEntity.PLANNED_START_TIME : caregivertasksEntity.ACCTUAL_START_TIME);
                    var EndTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME == null ? caregivertasksEntity.PLANNED_END_TIME : caregivertasksEntity.ACCTUAL_END_TIME);

                    if (clientVacations.Where(e => (e.StartDate >= StartTime && e.StartDate <= EndTime)
                                                            || (e.EndDate >= StartTime && e.EndDate <= EndTime)
                                                            || (StartTime >= e.StartDate && StartTime <= e.EndDate)
                                                            || (EndTime >= e.StartDate && EndTime <= e.EndDate)
                                                        ).Any())
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.CLIENTONVACATION.ToString(), validation_error_message = "Client on Vacation." });
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClinicianMaxHoursValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICaregiverMaximumHoursEffectivePeriods _caregiverMaximumHoursEffectivePeriods;
        public ClinicianMaxHoursValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregivertasksRepository caregivertasksRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions,
                                                       ICaregiverMaximumHoursEffectivePeriods _CaregiverMaximumHoursEffectivePeriods
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _caregiverMaximumHoursEffectivePeriods = _CaregiverMaximumHoursEffectivePeriods;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validationCreateSchedule)
        {
            var caregivertasksEntity = validationCreateSchedule.caregiverTaskEntity;//swagger level cgtaskid details (user input)
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var weekStartday = _scheduleValidationRulesDto.Min_Duration_Mins;
                var weekStartDate = caregivertasksEntity.PLANNED_DATE.AddDays(-1 * ((7 + ((int)caregivertasksEntity.PLANNED_DATE.DayOfWeek - weekStartday)) % 7)).Date;
                var weekEndDate = weekStartDate.AddDays(6);
                var ClinicianMaxHoursPerWeek = _scheduleValidationRulesDto.Max_Duration_Mins;
                //validationCreateSchedule.caregiverschedluesdetails will have schedules exlclduded Current Schedule 
                var caregivertasks = validationCreateSchedule.caregiverschedluesdetails;//other schedules apart from swagger level cgtaskid

                if (ClinicianMaxHoursPerWeek > 0)
                {
                    double weekTotalHours = 0;
                    if (caregivertasks.Any())
                        weekTotalHours = caregivertasks.Select(e => string.IsNullOrEmpty(e.EDITED_HOURS) ? e.PLANNED_END_TIME.Subtract(e.PLANNED_START_TIME).TotalMinutes : (double)e.TotalHours * 60).Sum();

                    weekTotalHours += Convert.ToDouble(caregivertasksEntity.TotalHours) * 60;
                    if (weekTotalHours > _scheduleValidationRulesDto.Max_Duration_Mins * 60)
                    {
                        string validationtype = ConflictExceptions.CLINICIANMAXHOURS.ToString();

                        if (_scheduleValidationRulesDto.Soft_Warning_Validation)
                            validationtype = ConflictExceptions.CLINICIANMAXHOURS_WARNING.ToString();

                        result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = "Clinician max hours per week is exceeding. Actualhours = " + Math.Round((weekTotalHours / 60), 2) + " and maxhours = " + _scheduleValidationRulesDto.Max_Duration_Mins, is_soft_warning = _scheduleValidationRulesDto.Soft_Warning_Validation });
                    }

                }
                else
                    {
                        var Columns_MaximumHoursEffectivePeriods = "CaregiverMaximumHoursEffectivePeriodID,CaregiverID,HHA,EffectiveStart," +
                            "case when isnull(CaregiverMaximumHoursEffectivePeriods.EffectiveEnd,'')=''then '1/1/9999 12:00:00 AM' else CaregiverMaximumHoursEffectivePeriods.EffectiveEnd end as EffectiveEnd," +
                            "CaregiverMaxHoursPerWeek,CaregiverMaxHoursPerWeek_isHardStop,Createdby,Createdon";
                        var filers_MaximumHoursEffectivePeriods = _repositoryFilterConditions.EqualFilter("CAREGIVERID", caregivertasksEntity.CAREGIVER, FilterConditionDataTypeEnums.integerType)
                                                                                                  .EqualFilter("HHA", caregivertasksEntity.HHA, FilterConditionDataTypeEnums.integerType).Build();

                        var Clinician_EffectivePeriod_recordsets = _caregiverMaximumHoursEffectivePeriods.GetCaregiverMaximumHoursEffectivePeriodsEntity_Details
                        (HHA, UserID, Columns_MaximumHoursEffectivePeriods, filers_MaximumHoursEffectivePeriods).Result;


                        double allowed_maximum_minutes_forweek = 0;
                        double actual_minutes_done_forweek = 0;
                        //if we have only one effective period in the that week range
                        if (Clinician_EffectivePeriod_recordsets.Any())
                        {

                        //Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveEnd.ToString() == "1/1/1900 12:00:00 AM")
                        //                                    .Select(x => { x.EffectiveEnd = DateTime.MaxValue; return x; }).ToList();
                            if (caregivertasks.Any())
                            {
                                caregivertasks = caregivertasks.Where(x => x.CGTASK_ID != Convert.ToInt32(caregivertasksEntity.CGTASK_ID)).ToList();
                                actual_minutes_done_forweek = Convert.ToDouble(caregivertasks.Sum(x => x.TotalHours ?? 0)) * 60;
                            }
                            if (Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveStart <= weekStartDate && x.EffectiveEnd >= weekEndDate).Any())
                            {
                                allowed_maximum_minutes_forweek = Clinician_EffectivePeriod_recordsets.
                                                Where(x => x.EffectiveStart <= weekStartDate && x.EffectiveEnd >= weekEndDate)
                                                .FirstOrDefault().CaregiverMaxHoursPerWeek * 60;

                                _scheduleValidationRulesDto.Soft_Warning_Validation = ! Clinician_EffectivePeriod_recordsets.
                                                Where(x => x.EffectiveStart <= weekStartDate && x.EffectiveEnd >= weekEndDate)
                                                .FirstOrDefault().CaregiverMaxHoursPerWeek_isHardStop;

                            }
                            else
                            {
                                _scheduleValidationRulesDto.Soft_Warning_Validation = false;
                                //if we have overlapping of effective period in a single week 
                                //identifying 1st over lap effective period in the week
                                CaregiverMaximumHoursEffectivePeriodsEntity first_effective_period = new CaregiverMaximumHoursEffectivePeriodsEntity();
                                CaregiverMaximumHoursEffectivePeriodsEntity last_effective_period = new CaregiverMaximumHoursEffectivePeriodsEntity();
                                if (Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveStart <= weekStartDate && x.EffectiveEnd >= weekStartDate).Any())
                                {
                                    first_effective_period = Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveStart <= weekStartDate && x.EffectiveEnd >= weekStartDate).FirstOrDefault();

                                    allowed_maximum_minutes_forweek = ((first_effective_period.CaregiverMaxHoursPerWeek) / 7) * 60
                                                                       * ((Convert.ToDateTime(first_effective_period.EffectiveEnd).Subtract(Convert.ToDateTime(weekStartDate))).TotalDays + 1);

                                    _scheduleValidationRulesDto.Soft_Warning_Validation = !first_effective_period.CaregiverMaxHoursPerWeek_isHardStop;

                                }

                                //identifying last overlap effective period in the week 
                                if (Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveEnd >= weekEndDate && x.EffectiveStart <= weekEndDate).Any())
                                {
                                    last_effective_period = Clinician_EffectivePeriod_recordsets.Where(x => x.EffectiveEnd >= weekEndDate && x.EffectiveStart <= weekEndDate).FirstOrDefault();

                                    if(_scheduleValidationRulesDto.Soft_Warning_Validation == true && !first_effective_period.CaregiverMaxHoursPerWeek_isHardStop)
                                        _scheduleValidationRulesDto.Soft_Warning_Validation = !last_effective_period.CaregiverMaxHoursPerWeek_isHardStop;

                                    allowed_maximum_minutes_forweek += (last_effective_period.CaregiverMaxHoursPerWeek / 7) * 60 *
                                                                            ((Convert.ToDateTime(weekEndDate).Subtract(Convert.ToDateTime(last_effective_period.EffectiveStart))).TotalDays + 1);
                                }


                                if ((first_effective_period.CaregiverID != 0 && last_effective_period.CaregiverID == 0)
                                     ||
                                     (last_effective_period.CaregiverID != 0 && first_effective_period.CaregiverID == 0)
                                   )
                                {
                                    //only first effective period falls in the week period and there is no last effective period available
                                    if (first_effective_period.CaregiverID != 0 && last_effective_period.CaregiverID == 0)
                                    {
                                        if (first_effective_period.EffectiveEnd < weekEndDate)
                                            weekEndDate = first_effective_period.EffectiveEnd;
                                    }

                                    //only last effective period falls in the week period and there is no first effective period available
                                    if (last_effective_period.CaregiverID != 0 && first_effective_period.CaregiverID == 0)
                                    {
                                        if (last_effective_period.EffectiveStart > weekStartDate)
                                            weekStartDate = last_effective_period.EffectiveStart;
                                    }

                                //week schedule total hours 
                                if(caregivertasks.Any())
                                    actual_minutes_done_forweek = caregivertasks.Where(x => x.PLANNED_DATE >= weekStartDate && x.PLANNED_DATE <= weekEndDate).Sum(x => x.TotalHours ?? 0) * 60;
                                }
                            }
                            actual_minutes_done_forweek = actual_minutes_done_forweek + Convert.ToDouble(caregivertasksEntity.TotalHours) * 60;
                            if (actual_minutes_done_forweek > allowed_maximum_minutes_forweek && allowed_maximum_minutes_forweek > 0)
                            {
                                string validationtype = ConflictExceptions.CLINICIANMAXHOURS.ToString();

                                if (_scheduleValidationRulesDto.Soft_Warning_Validation)
                                    validationtype = ConflictExceptions.CLINICIANMAXHOURS_WARNING.ToString();

                                result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = "Clinician max hours per week is exceeding. Actual hours = " + Math.Round((actual_minutes_done_forweek / 60), 2) + " and Max hours = " + Math.Round((allowed_maximum_minutes_forweek / 60), 2), is_soft_warning = _scheduleValidationRulesDto.Soft_Warning_Validation });
                            }

                        }
                }

            }
            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validationCreateSchedule));

            return result;
        }
    }
}
﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleServiceChangeValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly ServiceCodesEntity serviceInfo;

        public ScheduleServiceChangeValidationHandler(int hha, int user,
            ServiceCodesEntity serviceData)
        {
            HHA = hha;
            User = user;
            serviceInfo = serviceData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {

                // Validations
                #region validations start
                // Chart submitted
                if (newScheduleData.isChartSubmitted == true)
                {
                    result.Add(new ValidationErrorInfo() { validation_error_message = "Cannot change service since visit chart is submitted" });
                }

                // Service terminated
                ServiceTerminatedValidationHandler serviceTerminatedValidationHandler = new ServiceTerminatedValidationHandler(HHA, User, serviceInfo);
                result.AddRange(serviceTerminatedValidationHandler.HandleValidation(newScheduleData));
                #endregion
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleDatePriorToEpisodeValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly HHABranchScheduleConfigurationsEntity agencyBranchConfigInfo;
        private readonly EpisodeEntity firstEpisodeInfo;

        public ScheduleDatePriorToEpisodeValidationHandler(int hha, int user, HHABranchScheduleConfigurationsEntity agencyBranchConfigData,
            EpisodeEntity firstEpisodeData)
        {
            HHA = hha;
            User = user;
            agencyBranchConfigInfo = agencyBranchConfigData;
            firstEpisodeInfo = firstEpisodeData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                if(newScheduleData.PLANNED_DATE.Date < firstEpisodeInfo.EpStartDate.Date && !agencyBranchConfigInfo.AllowNonBillableSchedulesPriorToSOC
                    && !agencyBranchConfigInfo.AllowSchedulesPriortoSOC)
                {
                    result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Schedule date cannot be prior to SOC Date" });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

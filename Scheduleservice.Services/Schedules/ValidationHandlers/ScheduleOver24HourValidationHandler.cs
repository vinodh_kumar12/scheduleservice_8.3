﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using System;
using System.Collections.Generic;
using Scheduleservice.Core.Schedules;
using Scheduleservice.Core.Models;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleOver24HourValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        public ScheduleOver24HourValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       IPaymentsourcesRepository paymentsourcesRepository
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _paymentsourcesRepository = paymentsourcesRepository;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (_scheduleValidationRulesDto.Validationenabled)
            {
                if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue)
                {

                    var CheckinTime = Convert.ToDateTime(caregivertasksEntity.ACCTUAL_START_TIME);
                    var CheckoutTime = Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME);

                    //var paymentsourceDetail = _paymentsourcesRepository.GetPaymentSourceAdditionalInfo(HHA, UserID, caregivertasksEntity.PAYMENT_SOURCE, 0).Result;

                    string edited_hours = caregivertasksEntity.EDITED_HOURS; /*Common.GetVisitEditedHours(CheckinTime, CheckoutTime, (int)paymentsourceDetail.ROUNDOFF_Direction, (int)paymentsourceDetail.ROUNDOFF_Minutes);*/

                    if (edited_hours != "" && edited_hours != null)
                    {
                        int TotalMinutes = Common.ConvertStringToMins(edited_hours);

                        if (TotalMinutes > 1500)
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "SCHEDULEOVER24HOURS", validation_error_message = "Schedule is over 24 hours." });
                        }
                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
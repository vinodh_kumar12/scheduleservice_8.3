﻿using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ChartTypeValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly CaregivertasksEntity _oldScheduleInfo;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IVisitChartsRepository _visitChartsRepository;
        private readonly eChartMasterEntity _echartMasterEntity;
        private readonly pChartMasterEntity _pchartMasterEntity;

        public ChartTypeValidationHandler(int hha, int user, CaregivertasksEntity oldScheduleInfo,
            IRepositoryFilterConditions repositoryFilterConditions,
            IVisitChartsRepository visitChartsRepository,
            eChartMasterEntity echartMasterEntity,
            pChartMasterEntity pchartMasterEntity)
        {
            HHA = hha;
            User = user;
            _oldScheduleInfo = oldScheduleInfo;
            _repositoryFilterConditions = repositoryFilterConditions;
            _visitChartsRepository = visitChartsRepository;
            _echartMasterEntity = echartMasterEntity;
            _pchartMasterEntity = pchartMasterEntity;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            string columns = "";
            string filters = "";
            VisitChartsEntity visitChartInfo = new VisitChartsEntity();

            try
            {
                // Get required info
                // Get visit chart info if exists
                if(_oldScheduleInfo.VISIT_CHART.HasValue && _oldScheduleInfo.VISIT_CHART > 0)
                {
                    columns = "VisitCharts.CHART_ID," +
                        "VisitCharts.STATUS";
                    filters = _repositoryFilterConditions.EqualFilter("VisitCharts.CHART_ID", _oldScheduleInfo.VISIT_CHART, Core.Enums.FilterConditionDataTypeEnums.integerType)
                        .Build();
                    visitChartInfo = _visitChartsRepository.GetVisitChartsList(HHA, User, columns, filters).Result.FirstOrDefault();
                }

                // Validate Fields
                // Validate for chart submitted/approved
                if(_oldScheduleInfo.Chart_Type == 'E' && newScheduleData.Chart_Type == 'P')
                {
                    if (visitChartInfo.STATUS == "Done" || visitChartInfo.STATUS == "Approved")
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Cannot change chart type since chart is already Submitted/Approved" });
                    }
                    
                    if(_echartMasterEntity.isSubmitted || _echartMasterEntity.isApproved)
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Cannot change chart type since chart is already Submitted/Approved" });
                    }
                }

                if(_oldScheduleInfo.Chart_Type == 'P' && newScheduleData.Chart_Type == 'E')
                {
                    if((_pchartMasterEntity.pChartSubmitted.HasValue && _pchartMasterEntity.pChartSubmitted.Value) 
                        || (_pchartMasterEntity.pChartApproved.HasValue && _pchartMasterEntity.pChartApproved.Value))
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Cannot change chart type since chart is already Submitted/Approved" });
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }
    }
}

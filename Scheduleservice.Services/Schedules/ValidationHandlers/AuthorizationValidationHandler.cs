﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    class AuthorizationValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        public AuthorizationValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregivertasksRepository caregivertasksRepository
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregivertasksRepository = caregivertasksRepository;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var caregivertasks = _caregivertasksRepository.GetScheduleBasicInfo(HHA, UserID, caregivertasksEntity.CGTASK_ID).Result;

                if (_scheduleValidationRulesDto.block_schedule_creation
                    || (_scheduleValidationRulesDto.block_checkin && caregivertasks.STATUS != "Planned")
                    || (_scheduleValidationRulesDto.block_approval && caregivertasks.STATUS == "Approved")
                    )
                {

                    if (caregivertasks.isAuthorized == null || caregivertasks.isAuthorized == false)
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.AUTHORIZATION.ToString(), validation_error_message = "AUTHORIZATION" });
                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
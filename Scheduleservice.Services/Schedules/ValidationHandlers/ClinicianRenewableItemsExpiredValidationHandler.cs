﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClinicianRenewableItemsExpiredValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICExpiringItemsRepository _cExpiringItemsRepository;
        private readonly IEExpiringItemsRepository _eExpiringItemsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ClinicianRenewableItemsExpiredValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICExpiringItemsRepository cExpiringItemsRepository,
                                                       IEExpiringItemsRepository eExpiringItemsRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _cExpiringItemsRepository = cExpiringItemsRepository;
            _eExpiringItemsRepository = eExpiringItemsRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var eExpiringItemsColumns = "CLINICIAN_EXPIRING_ITEM_ID,HHA_EXPIRING_ITEM_ID,CLINICIAN_ID,EXPIRY_DATE";
                var eExpiringItemsFilter = _repositoryFilterConditions.EqualFilter("CLINICIAN_ID", caregivertasksEntity.CAREGIVER, FilterConditionDataTypeEnums.integerType)
                                    .Build();

                var eExpiringItems = _eExpiringItemsRepository.GetEExpiringItems(HHA, UserID, eExpiringItemsColumns, eExpiringItemsFilter).Result;

                if (eExpiringItems.Any())
                {
                    var cExpiringItemsColumns = "EXPIRING_ITEM_ID,blockFromschedulingwhenExpires,ITEM_NAME";
                    var cExpiringItemsFilter = _repositoryFilterConditions.InFilter("EXPIRING_ITEM_ID", String.Join(",", eExpiringItems.Select(x => x.HHA_EXPIRING_ITEM_ID).Distinct()), FilterConditionDataTypeEnums.integerType)
                                        .Build();

                    var cExpiringItems = _cExpiringItemsRepository.GetCExpiringItems(HHA, UserID, cExpiringItemsColumns, cExpiringItemsFilter).Result;

                    if ((from c in eExpiringItems.Where(e => e.EXPIRY_DATE < caregivertasksEntity.PLANNED_DATE && e.EXPIRY_DATE != null)
                         from exp in cExpiringItems.Where(e => e.EXPIRING_ITEM_ID == c.HHA_EXPIRING_ITEM_ID && e.blockFromschedulingwhenExpires == true)
                         select new
                         {
                             exp.EXPIRING_ITEM_ID
                         }).Any())
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.CLINICIANRENEWABLEITEMSEXPIRED.ToString(), validation_error_message = _scheduleValidationRulesDto.ValidationError });
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

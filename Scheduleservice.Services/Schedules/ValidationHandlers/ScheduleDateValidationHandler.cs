﻿using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleDateValidationHandler : ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly CaregivertasksEntity _oldScheduleInfo;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClaimsRepository _claimsRepository;
        private readonly PaymentsourcesEntity payerInfo;
        private readonly ClientPaymentSourcesEntity clientPayer;
        private readonly IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriods;
        private readonly ServiceCodesEntity serviceInfo;
        private readonly CaregiversEntity caregiverInfo;
        private readonly EpisodeEntity scheduleEpisodeInfo;
        private readonly HConfigurations2Entity hConfig2Info;
        private readonly _UA_FormsEntity serviceFormInfo;
        IEnumerable<UserRolesEntity> userPemissionList;
        private readonly int timepoint;
        private readonly EpisodeEntity firstEpisodeInfo;
        private readonly HHABranchScheduleConfigurationsEntity agencyBranchConfigInfo;
        private readonly ClientsEntity clientInfo;

        public ScheduleDateValidationHandler(int hha, int user, CaregivertasksEntity oldScheduleInfo,
            IRepositoryFilterConditions repositoryFilterConditions,
            IClientsRepository clientsRepository,
            ICaregivertasksRepository caregivertasksRepository,
            IClaimsRepository claimsRepository,
            PaymentsourcesEntity payerData,
            ClientPaymentSourcesEntity clientPayerData,
            IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriodsData,
            ServiceCodesEntity serviceData,
            CaregiversEntity caregiverData,
            EpisodeEntity scheduleEpisodeData,
            HConfigurations2Entity hConfig2Data,
            _UA_FormsEntity serviceFormData,
            IEnumerable<UserRolesEntity> UserPemissionList,
            int Timepoint,
            EpisodeEntity firstEpisodeData,
            HHABranchScheduleConfigurationsEntity agencyBranchConfigData,
            ClientsEntity clientData
            )
        {
            HHA = hha;
            User = user;
            _oldScheduleInfo = oldScheduleInfo;
            _repositoryFilterConditions = repositoryFilterConditions;
            _clientsRepository = clientsRepository;
            _caregivertasksRepository = caregivertasksRepository;
            _claimsRepository = claimsRepository;
            payerInfo = payerData;
            clientPayer = clientPayerData;
            clientPayerEffectivePeriods = clientPayerEffectivePeriodsData;
            serviceInfo = serviceData;
            caregiverInfo = caregiverData;
            scheduleEpisodeInfo = scheduleEpisodeData;
            hConfig2Info = hConfig2Data;
            serviceFormInfo = serviceFormData;
            userPemissionList = UserPemissionList;
            timepoint = Timepoint;
            firstEpisodeInfo = firstEpisodeData;
            agencyBranchConfigInfo = agencyBranchConfigData;
            clientInfo = clientData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            var Columns = "";
            var Filters = "";
            DateTime weekStartDate;
            DateTime weekEndDate;
            IEnumerable<ClientInactivePeriodsEntity> clientInactivePeriodsInfo;
            IEnumerable<CaregivertasksEntity> episodeSchdules;
            try
            {
                // Get require info
                #region getting required data
                // get claims info
                Columns = "Claims.CLAIM_ID,"
                    + "ClaimDetails.SERVICE_DATE,"
                    + "Claims.EPISODE,"
                    + "Claims.PAYMENT_SOURCE,"
                    + "Claims.APPROVED,"
                    + "Claims.STATUS,"
                    + "Claims.SubEpisodeMasterID";
                Filters = _repositoryFilterConditions.EqualFilter("Claims.EPISODE", scheduleEpisodeInfo.EPISODEID, FilterConditionDataTypeEnums.integerType)
                    .EqualFilter("Claims.PAYMENT_SOURCE", newScheduleData.PAYMENT_SOURCE, FilterConditionDataTypeEnums.integerType)
                    .EqualFilter("Claims.APPROVED", true, FilterConditionDataTypeEnums.boolType, true)
                    .InFilter("Claims.CLAIM_TYPE", "RAP,NOA", FilterConditionDataTypeEnums.stringType)
                    .NotEqualFilter("Claims.[STATUS]", "Deleted", FilterConditionDataTypeEnums.stringType)
                    .EqualFilter("Claims.SubEpisodeMasterID", 0, FilterConditionDataTypeEnums.integerType, true)
                    .Build();
                var scheduleClaimsInfo = _claimsRepository.GetScheduleClaims(HHA, User, Columns, Filters).Result;

                if (scheduleClaimsInfo.Any())
                {
                    // Get episode schedules
                    Columns = "CaregiverTasks.CLIENT_ID,"
                        + "CaregiverTasks.PAYMENT_SOURCE,"
                        + "CaregiverTasks.PLANNED_DATE,"
                        + "CaregiverTasks.POC";
                    Filters = _repositoryFilterConditions.EqualFilter("CaregiverTasks.CLIENT_ID", newScheduleData.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("CaregiverTasks.PAYMENT_SOURCE", newScheduleData.PAYMENT_SOURCE, FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("CaregiverTasks.POC", scheduleEpisodeInfo.POC, FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("CaregiverTasks.PLANNED_DATE", scheduleClaimsInfo.Min(x => x.SERVICE_DATE), FilterConditionDataTypeEnums.datetimeType)
                        .Build();
                    episodeSchdules = _caregivertasksRepository.GetAllSchedulesList(HHA, User, Columns, Filters).Result;
                }
                else
                {
                    episodeSchdules = new List<CaregivertasksEntity>();
                }

                // Get Client Inactive periods data
                clientInactivePeriodsInfo = _clientsRepository.GetClientInactivePeriods(HHA, User, clientInfo.CLIENT_ID).Result;

                weekStartDate = _oldScheduleInfo.PLANNED_DATE.AddDays(-(int)(7 + (_oldScheduleInfo.PLANNED_DATE.DayOfWeek - hConfig2Info.WEEK_START_DAY - 1)) % 7).Date;
                weekEndDate = weekStartDate.AddDays(6);
                #endregion

                // Validate fields
                #region validations start
                // Client Payer Effective Period
                ClientPayerEffectivePeriodValidationHandler clientPayerEffectivePeriodValidationHandler = new ClientPayerEffectivePeriodValidationHandler(HHA, User, clientPayerEffectivePeriods);
                result.AddRange(clientPayerEffectivePeriodValidationHandler.HandleValidation(newScheduleData));

                // Service terminated
                ServiceTerminatedValidationHandler serviceTerminatedValidationHandler = new ServiceTerminatedValidationHandler(HHA, User, serviceInfo);
                result.AddRange(serviceTerminatedValidationHandler.HandleValidation(newScheduleData));

                // Caregiver effective end date
                if (caregiverInfo.CAREGIVER_ID != 0)
                {
                    CaregiverEffetiveEndDataValidationHandler caregiverEffetiveEndDataValidationHandler = new CaregiverEffetiveEndDataValidationHandler(HHA, User, caregiverInfo);
                    result.AddRange(caregiverEffetiveEndDataValidationHandler.HandleValidation(newScheduleData));
                }

                // Date prior to SOC/Episode
                ScheduleDatePriorToEpisodeValidationHandler scheduleDatePriorToEpisodeValidationHandler =
                    new ScheduleDatePriorToEpisodeValidationHandler(HHA, User, agencyBranchConfigInfo, firstEpisodeInfo);
                result.AddRange(scheduleDatePriorToEpisodeValidationHandler.HandleValidation(newScheduleData));

                // VP validations
                if (_oldScheduleInfo.PLAN_ENTRY > 0 && clientPayer.SchedulingRule == "VP")
                {
                    if (!(newScheduleData.isPRNVisit.HasValue && newScheduleData.isPRNVisit.Value)
                        && (newScheduleData.PLANNED_DATE.Date < weekStartDate.Date || newScheduleData.PLANNED_DATE.Date > weekEndDate.Date))
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule cannot be moved outside the week" });
                    }
                }

                // Medicare (Episodic billing validations)
                if (payerInfo.IsEpisodicBilling == true && payerInfo.PayerPrimaryType == true)
                {
                    // RAP created, so schedule cannot move prior to first schedule
                    if (newScheduleData.PLANNED_DATE != _oldScheduleInfo.PLANNED_DATE
                        && newScheduleData.CanBill == true
                        && episodeSchdules.Any(x => x.PLANNED_DATE > newScheduleData.PLANNED_DATE))
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Cannot modify visit date prior to the first visit of the episode as RAP/NOA is already created for this episode." });
                    }
                }

                // Date outside episode
                if(!(serviceFormInfo.isPreSOCForm.HasValue && serviceFormInfo.isPreSOCForm.Value)
                    && (newScheduleData.PLANNED_DATE.Date < scheduleEpisodeInfo.EpStartDate.Date 
                        || newScheduleData.PLANNED_DATE.Date > scheduleEpisodeInfo.EpEndDate.Date
                    )
                )
                {
                    result.Add(new ValidationErrorInfo() { validation_error_message = "Reschedule Date cannot be outside the Episode Period" });
                }

                // Clinician cannot move schedule
                if((serviceInfo.DonotAllowClinicianToReschedule.HasValue && serviceInfo.DonotAllowClinicianToReschedule.Value) && userPemissionList.FirstOrDefault().User_Type == 1)
                {
                    result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot be modified since reschedule permission is not set for this service" });
                }

                // inactive region
                if (timepoint != 9)
                {
                    ScheduleInactiveRegionValidationHandler scheduleInactiveRegionValidationHandler = 
                        new ScheduleInactiveRegionValidationHandler(HHA, User, clientInactivePeriodsInfo, payerInfo, clientInfo, agencyBranchConfigInfo);
                    result.AddRange(scheduleInactiveRegionValidationHandler.HandleValidation(newScheduleData));
                }
                #endregion
            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }
    }
}

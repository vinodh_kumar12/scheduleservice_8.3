﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleBillableChangeValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly HHABranchScheduleConfigurationsEntity agencyBranchConfigInfo;
        private readonly EpisodeEntity firstEpisodeInfo;
        private readonly IClientsRepository _clientsRepository;
        private readonly ClientsEntity clientInfo;
        private readonly PaymentsourcesEntity payerInfo;
        private readonly int timepoint;

        public ScheduleBillableChangeValidationHandler(int hha, int user,
            HHABranchScheduleConfigurationsEntity agencyBranchConfigData,
            EpisodeEntity firstEpisodeData,
            IClientsRepository clientsRepository,
            ClientsEntity clientData,
            PaymentsourcesEntity payerData,
            int Timepoint)
        {
            HHA = hha;
            User = user;
            agencyBranchConfigInfo = agencyBranchConfigData;
            firstEpisodeInfo = firstEpisodeData;
            _clientsRepository = clientsRepository;
            clientInfo = clientData;
            payerInfo = payerData;
            timepoint = Timepoint;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            IEnumerable<ClientInactivePeriodsEntity> clientInactivePeriodsInfo;
            try
            {
                // Getting Data
                // Get Client Inactive periods data
                clientInactivePeriodsInfo = _clientsRepository.GetClientInactivePeriods(HHA, User, clientInfo.CLIENT_ID).Result;

                // Validations
                #region validations
                // Date prior to SOC/Episode
                ScheduleDatePriorToEpisodeValidationHandler scheduleDatePriorToEpisodeValidationHandler =
                    new ScheduleDatePriorToEpisodeValidationHandler(HHA, User, agencyBranchConfigInfo, firstEpisodeInfo);
                result.AddRange(scheduleDatePriorToEpisodeValidationHandler.HandleValidation(newScheduleData));

                // inactive region
                if (timepoint != 9)
                {
                    ScheduleInactiveRegionValidationHandler scheduleInactiveRegionValidationHandler =
                        new ScheduleInactiveRegionValidationHandler(HHA, User, clientInactivePeriodsInfo, payerInfo, clientInfo, agencyBranchConfigInfo);
                    result.AddRange(scheduleInactiveRegionValidationHandler.HandleValidation(newScheduleData));
                }
                #endregion
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

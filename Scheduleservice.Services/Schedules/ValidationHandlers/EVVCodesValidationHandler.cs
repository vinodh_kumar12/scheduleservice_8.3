﻿using Newtonsoft.Json;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class EVVCodesValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICaregiverTaskEVVReasonsRepository _caregiverTaskEVVReasonsRepository;

        public EVVCodesValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user, IRepositoryFilterConditions repositoryFilterConditions,
                                                       ICaregiverTaskEVVReasonsRepository caregiverTaskEVVReasonsRepository)
        {
            this._scheduleValidationRulesDto = scheduleValidationRulesDto;
            this.HHA = hha;
            this.UserID = user;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregiverTaskEVVReasonsRepository = caregiverTaskEVVReasonsRepository;
        }

        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled && (caregivertasksEntity.STATUS== ScheduleStatusEnums.Completed.ToString() || caregivertasksEntity.STATUS== ScheduleStatusEnums.Approved.ToString()))
            {
                var json_result = _caregiverTaskEVVReasonsRepository._S_GetApproveScheduleValidationErrors(HHA, caregivertasksEntity.CGTASK_ID,
                    UserID, false, null, false,0).Result;

                if(!string.IsNullOrEmpty(json_result))
                {
                    var Caregivertaskvalidationerrorslist = JsonConvert.DeserializeObject<IEnumerable<CaregiverTaskValidationErrorEntity>>(json_result);

                    if(Caregivertaskvalidationerrorslist.Any())
                    {
                        foreach(var Caregivertaskvalidationerrors in Caregivertaskvalidationerrorslist)
                        {
                            if (Caregivertaskvalidationerrors.ErrorCode == "NOATTESTATION")
                                Caregivertaskvalidationerrors.ErrorCode = ConflictExceptions.NO_ATTESTATION.ToString();
                            else if(Caregivertaskvalidationerrors.ErrorCode == "NOREASONCODE")
                                Caregivertaskvalidationerrors.ErrorCode = ConflictExceptions.NO_REASONCODE.ToString();
                            else if (Caregivertaskvalidationerrors.ErrorCode == "NOACTIONCODE")
                                Caregivertaskvalidationerrors.ErrorCode = ConflictExceptions.NO_ACTIONCODE.ToString();

                            result.Add(new ValidationErrorInfo() { validation_type = Caregivertaskvalidationerrors.ErrorCode, validation_error_message = Caregivertaskvalidationerrors.ErrorText });
                        }
                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

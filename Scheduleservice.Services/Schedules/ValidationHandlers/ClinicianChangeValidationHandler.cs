﻿using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClinicianChangeValidationHandler : ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly HHABranchScheduleConfigurationsEntity agencyScheduleSettingsInfo;

        public ClinicianChangeValidationHandler(int hha, int user,
            HHABranchScheduleConfigurationsEntity agencyScheduleSettingsData)
        {
            HHA = hha;
            User = user;
            agencyScheduleSettingsInfo = agencyScheduleSettingsData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Validations
                if(!agencyScheduleSettingsInfo.AllowToChangeClinicianWheneChartIsFilled 
                    && (newScheduleData.eChartMasterID > 0 || newScheduleData.oasisDatasetID > 0)
                    && (newScheduleData.isChartSubmitted == true || newScheduleData.isChartApproved == true
                        || newScheduleData.isChartSendForCorrection == true
                    )
                )
                {
                    result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Clinician cannot changed since chart is already filled by this clinician" });
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return result;
        }
    }
}

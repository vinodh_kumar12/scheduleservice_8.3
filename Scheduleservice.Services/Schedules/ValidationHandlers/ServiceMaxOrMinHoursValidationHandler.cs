﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ServiceMaxOrMinHoursValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        public ServiceMaxOrMinHoursValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto)
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

            if (caregivertasksEntity.ACCTUAL_END_TIME != null && _scheduleValidationRulesDto.Validationenabled)
            {

                if (Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME).Subtract(Convert.ToDateTime(caregivertasksEntity.ACCTUAL_START_TIME)).TotalMinutes > _scheduleValidationRulesDto.Min_Duration_Mins
                    || Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME).Subtract(Convert.ToDateTime(caregivertasksEntity.ACCTUAL_START_TIME)).TotalMinutes < _scheduleValidationRulesDto.Max_Duration_Mins)
                    result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.SERVICEMAXORMINHOURS.ToString(), validation_error_message = "Schedule Service Maximum/Minimum hours is exceeded." });
            }


            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

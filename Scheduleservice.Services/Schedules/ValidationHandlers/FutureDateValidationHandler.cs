﻿using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Schedules;
using System;
using System.Collections.Generic;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class FutureDateValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly IClientsRepository _clientsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHConfigurations2Repository _hConfigurations2Repository;
        public FutureDateValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       IClientsRepository clientsRepository,
                                                       IHConfigurations2Repository hConfigurations2Repository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _clientsRepository = clientsRepository;
            _hConfigurations2Repository = hConfigurations2Repository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override  List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                
                var ClientDetails = _clientsRepository.GetClientsBasicInfo(HHA, UserID, caregivertasksEntity.CLIENT_ID).Result;
                int HHABranchID = ClientDetails.HHA_BRANCH_ID;

                if (caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                {
                    var CurrentTime = Common.GetCurrentPSTTime();
                    string HConfiguration2Filter = _repositoryFilterConditions.EqualFilter("HHA_BRANCHID", HHABranchID, FilterConditionDataTypeEnums.integerType)
                                               .Build();

                    var hconfiguration2detail = _hConfigurations2Repository._S_Schedule_GetHConfiguration2Settings(HHA, UserID, "TIME_ZONE", HConfiguration2Filter).Result;

                    string Zone = hconfiguration2detail.TIME_ZONE;
                    DateTime ClientLocationCurrentDateTime = CurrentTime;

                    if (Zone.ToUpper() != "PST")
                    {
                        CurrentTime = Common.GetCurrentUTCTime();
                        ClientLocationCurrentDateTime = CurrentTime.ToConvertUTCTimeZone(Zone);
                    }

                    
                    if (ClientLocationCurrentDateTime.Date < caregivertasksEntity.PLANNED_DATE)
                    {
                        result.Add(new ValidationErrorInfo() { validation_type = "FUTUREDATE", validation_error_message = "Schedule date is Future Date." });
                    }
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
﻿using Newtonsoft.Json;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq; 

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class DeviationValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int _hha;
        private readonly int _user;        
        private readonly ICaregivertasksRepository _caregivertaskRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        
        private readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository; 

        public DeviationValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                            int hha, int user,                                            
                                            ICaregivertasksRepository caregivertaskRepository,
                                            IRepositoryFilterConditions repositoryFilterConditions,
                                            IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository,                                            
                                            IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository)
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            _hha = hha;
            _user = user;
            
            _caregivertaskRepository = caregivertaskRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            
            this._hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository; 
        } 
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;

                if (validateScheduleRules.validationErrorsDependentSchedules != null && validateScheduleRules.AllowToCheckoutifHardStopValidationExists)
                    result = ValidateDeviationBasedOnConflictsData(validateScheduleRules);
                else
                    result = ValidateDeviationOnrealData(validateScheduleRules);
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
        private List<ValidationErrorInfo> ValidateDeviationBasedOnConflictsData(ValidateScheduleRules validateScheduleRules)
        {
            var cgtaskIDs = String.Join(",", validateScheduleRules.validationErrorsDependentSchedules.Where(e => e.exception_type == "DEVIATION").Select(e => e.dependentCgTaskID).Distinct());

            if (!string.IsNullOrEmpty(cgtaskIDs))
            {
                var Columns = "CGTASK_ID,CAREGIVER,CanBill,CONFIRMED,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,PLANNED_START_TIME,PLANNED_END_TIME";
                var Filters = _repositoryFilterConditions.InFilter("CGTASK_ID", cgtaskIDs, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var deviationSchedules = _caregivertaskRepository.GetAllSchedulesList(_hha, _user, Columns, Filters).Result;

                var StartTime = (DateTime)(validateScheduleRules.caregiverTaskEntity.ACCTUAL_END_TIME == null ? validateScheduleRules.caregiverTaskEntity.PLANNED_START_TIME : validateScheduleRules.caregiverTaskEntity.ACCTUAL_START_TIME);
                var EndTime = (DateTime)(validateScheduleRules.caregiverTaskEntity.ACCTUAL_END_TIME != null ? validateScheduleRules.caregiverTaskEntity.ACCTUAL_END_TIME : validateScheduleRules.caregiverTaskEntity.ACCTUAL_START_TIME != null ? ((DateTime)validateScheduleRules.caregiverTaskEntity.ACCTUAL_START_TIME).AddMinutes(2) : validateScheduleRules.caregiverTaskEntity.PLANNED_END_TIME);

                var Schedules = from ds in deviationSchedules
                                        .Where(s => (_scheduleValidationRulesDto.discard_non_billable_schedules == true && s.CanBill == true)
                                                            || (_scheduleValidationRulesDto.block_clinician_confirmation && s.CONFIRMED == true)
                                                            || (_scheduleValidationRulesDto.block_checkin && s.ACCTUAL_START_TIME != null)
                                                            || (_scheduleValidationRulesDto.block_approval && s.STATUS == "Approved")
                                                            || (_scheduleValidationRulesDto.block_schedule_creation)
                                                        )
                                select new
                                {
                                    StartTime = (DateTime)(ds.ACCTUAL_START_TIME != null ? ds.ACCTUAL_START_TIME : ds.PLANNED_START_TIME),
                                    EndTime = (DateTime)(ds.ACCTUAL_END_TIME != null ? ds.ACCTUAL_END_TIME : ds.ACCTUAL_START_TIME != null ? ((DateTime)ds.ACCTUAL_START_TIME).AddMinutes(2) : ds.PLANNED_END_TIME)

                                };

                if (Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                            .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                        && e.EndTime.Subtract(StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins)
                                                                        ||
                                                                        (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                            && EndTime.Subtract(e.StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins
                                                                        )
                                                                    ).Any())
                {
                    result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.DEVIATION.ToString(), validation_error_message = "There is Schedule Deviation Conflict validation." });
                }
            }

            return result;
        }

        public List<ValidationErrorInfo> ValidateDeviationOnrealData(ValidateScheduleRules validationCreateSchedule)
        {
            bool requireDeviationValidation = false;
            var caregivertasksEntity = validationCreateSchedule.caregiverTaskEntity;
            if ((_scheduleValidationRulesDto.block_clinician_confirmation && caregivertasksEntity.CONFIRMED == true)
                || (_scheduleValidationRulesDto.block_checkin && caregivertasksEntity.ACCTUAL_START_TIME != null)
                || (_scheduleValidationRulesDto.block_approval && caregivertasksEntity.STATUS == "Approved")
                || (_scheduleValidationRulesDto.block_schedule_creation)
               )
            {
                requireDeviationValidation = true;
            }

            if ((_scheduleValidationRulesDto.discard_non_billable_schedules == true && !caregivertasksEntity.CanBill))
            {
                requireDeviationValidation = false;
            }


            if (requireDeviationValidation)
            {   
                //check for intermittent clients
                var clientpayers = validationCreateSchedule.clientPaymentSourcesEntity;
                    //(_clientsRepository.GetClientPaymentSourcesInfo(_hha, _user, caregivertasksEntity.CLIENT_ID, caregivertasksEntity.PAYMENT_SOURCE)).Result.FirstOrDefault();

                if (clientpayers.SchedulingRule == "VP" && caregivertasksEntity.CONFIRMED == false)
                    requireDeviationValidation = false;

                if (requireDeviationValidation)
                {
                    var clientinfo = validationCreateSchedule.clientsEntity;

                    //hha branch configurations
                    var hHABranchScheduleConfiguration = _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(_hha, _user, clientinfo.HHA_BRANCH_ID).Result;
                   // validationCreateSchedule.clientschedluesdetails=validationCreateSchedule.clientschedluesdetails.Where(x => x.PLANNED_DATE >= caregivertasksEntity.PLANNED_DATE.AddDays(-1) && x.PLANNED_DATE <= caregivertasksEntity.PLANNED_DATE.AddDays(1));

                    if (_scheduleValidationRulesDto.discard_non_billable_schedules)
                        validationCreateSchedule.clientschedluesdetails = validationCreateSchedule.clientschedluesdetails.Where(x => x.CanBill == true).Select(x => x);

                    //validationCreateSchedule.clientschedluesdetails = validationCreateSchedule.clientschedluesdetails;

                    if (hHABranchScheduleConfiguration.ExcludePlannedSchedulesDeviationConflictWithCompletedSchedule)
                    {
                        validationCreateSchedule.clientschedluesdetails = validationCreateSchedule.clientschedluesdetails.Where(x => x.STATUS == "Completed" && x.STATUS == "Approved");
                     }

                    var deviationschedules = validationCreateSchedule.clientschedluesdetails;
                        
                    if (deviationschedules.Any())
                    { 
                        var caregivers = validationCreateSchedule.caregivers;

                        if (hHABranchScheduleConfiguration.ignoreDeviationForSupervisorSchedules)
                        {

                            if (caregivers.Where(x => x.supervisingClinician > 0).Any())
                            {
                                if (caregivers.Where(x => x.CAREGIVER_ID == caregivertasksEntity.CAREGIVER && x.supervisingClinician > 0).Any())
                                {
                                    deviationschedules = new List<CaregivertasksEntity>();
                                }
                                else
                                    deviationschedules = (from ds in deviationschedules
                                                          from c in caregivers.Where(e => e.CAREGIVER_ID == ds.CAREGIVER && (e.supervisingClinician > 0))
                                                          select ds);
                            }

                        }

                        deviationschedules.ToList().ForEach(x => { x.CAREGIVER_DISC_ID = caregivers.Where(y => y.CAREGIVER_ID == x.CAREGIVER).FirstOrDefault().DISCIPLINE; });
                        caregivertasksEntity.CAREGIVER_DISC_ID = caregivers.Where(y => y.CAREGIVER_ID == caregivertasksEntity.CAREGIVER).FirstOrDefault().DISCIPLINE;

                        //get clincian disc categortyID
                        var clinicianDiscInfo = validationCreateSchedule.caregiverDisciplines;
                        clinicianDiscInfo=clinicianDiscInfo.Where(x => String.Join(",", caregivers.Select(x => x.CAREGIVER_DISC_ID).Distinct()).Contains(x.CAREGIVER_DISC_ID.ToString())).Select(x=>x);
                            //_caregiverDisciplinesRepository.GetCaregiverDiscipines(_hha, _user, columns, filters).Result;


                        if (clinicianDiscInfo.Any())
                        {
                            deviationschedules.ToList().ForEach(x => { x.ServiceCategory = clinicianDiscInfo.Where(y => y.CAREGIVER_DISC_ID == x.CAREGIVER_DISC_ID).FirstOrDefault().ServiceCategory; });
                            caregivertasksEntity.ServiceCategory = clinicianDiscInfo.Where(y => y.CAREGIVER_DISC_ID == caregivertasksEntity.CAREGIVER_DISC_ID).FirstOrDefault().ServiceCategory;
                        }


                        if (deviationschedules.Where(z => z.ServiceCategory == 0).Any() || caregivertasksEntity.ServiceCategory == 0)
                        { 
                            //get clincian other disciplines 
                            var caregiverotherDiscinfo = validationCreateSchedule.CaregiverOtherDisciplines;
                            caregiverotherDiscinfo.Where(x => String.Join(",", caregivers.Select(x => x.CAREGIVER_DISC_ID).Distinct()).Contains(x.CAREGIVER_DISC_ID.ToString()));
                            if (caregiverotherDiscinfo.Any())
                            {
                                deviationschedules.Where(z => z.ServiceCategory == 0)?.ToList().ForEach(x => { x.ServiceCategory = caregiverotherDiscinfo.Where(y => y.CAREGIVER_DISC_ID == x.CAREGIVER_DISC_ID && y.CaregiverID == x.CAREGIVER).FirstOrDefault().ServiceCategory; });
                                caregivertasksEntity.ServiceCategory = caregivertasksEntity.ServiceCategory == 0 ? caregiverotherDiscinfo.Where(y => y.CAREGIVER_DISC_ID == caregivertasksEntity.CAREGIVER_DISC_ID && y.CaregiverID == caregivertasksEntity.CAREGIVER).FirstOrDefault().ServiceCategory : caregivertasksEntity.ServiceCategory;
                            }
                        }


                        if (deviationschedules.Where(z => z.ServiceCategory == 0).Any() || caregivertasksEntity.ServiceCategory == 0)
                        {
                            //get service groupIds 
                            string servicecodeids = caregivertasksEntity.SERVICECODE_ID.ToString();
                            if (deviationschedules.Any())
                            {
                                if (!string.IsNullOrEmpty(servicecodeids))
                                    servicecodeids += "," + String.Join(",", deviationschedules.Select(x => x.SERVICECODE_ID).Distinct());
                                else
                                    servicecodeids = String.Join(",", deviationschedules.Select(x => x.SERVICECODE_ID).Distinct());
                            }

                            
                            var serviceinfo = validationCreateSchedule.servicecodes.Where(x => servicecodeids.Contains(x.SERVICE_CODE_ID.ToString()));
                            if (serviceinfo.Any())
                            {
                                if (deviationschedules.Where(z => z.ServiceCategory == 0).Any())
                                    deviationschedules.Where(z => z.ServiceCategory == 0).ToList().ForEach(x => { x.ServiceCategory = serviceinfo.Where(y => y.SERVICE_CODE_ID == x.SERVICECODE_ID).FirstOrDefault().ServiceCategory; });

                                caregivertasksEntity.ServiceCategory = caregivertasksEntity.ServiceCategory == 0 ? serviceinfo.Where(y => y.SERVICE_CODE_ID == caregivertasksEntity.SERVICECODE_ID).FirstOrDefault().ServiceCategory : caregivertasksEntity.ServiceCategory;
                            }
                        }

                        if (!hHABranchScheduleConfiguration.ignoreDeviationForDifferentServiceDiscplines)
                        {
                            if (deviationschedules.Where(x => x.ServiceCategory <= 4).Any())
                                deviationschedules.Where(x => x.ServiceCategory <= 4).ToList().ForEach(y => y.ServiceCategory = 2);

                            if (caregivertasksEntity.ServiceCategory <= 4)
                                caregivertasksEntity.ServiceCategory = 2;
                        }

                        deviationschedules = deviationschedules.Where(x => x.ServiceCategory == caregivertasksEntity.ServiceCategory);
                         
                        if (deviationschedules.Any())
                        {
                            var StartTime = (DateTime)(caregivertasksEntity.ACCTUAL_START_TIME == null ? caregivertasksEntity.PLANNED_START_TIME : caregivertasksEntity.ACCTUAL_START_TIME);
                            var EndTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME != null ? caregivertasksEntity.ACCTUAL_END_TIME : caregivertasksEntity.ACCTUAL_START_TIME != null ? ((DateTime)caregivertasksEntity.ACCTUAL_START_TIME).AddMinutes(2) : caregivertasksEntity.PLANNED_END_TIME);


                            var Schedules = from ds in deviationschedules
                                            select new
                                            {
                                                StartTime = (DateTime)(ds.ACCTUAL_START_TIME != null ? ds.ACCTUAL_START_TIME : ds.PLANNED_START_TIME),
                                                EndTime = (DateTime)(ds.ACCTUAL_END_TIME != null ? ds.ACCTUAL_END_TIME : ds.ACCTUAL_START_TIME != null ? ((DateTime)ds.ACCTUAL_START_TIME).AddMinutes(2) : ds.PLANNED_END_TIME),
                                                CgtaskID = ds.CGTASK_ID
                                            };


                            if (Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                                   .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                               && e.EndTime.Subtract(StartTime).TotalMinutes + 1 > _scheduleValidationRulesDto.grace_period_mins)
                                                                               ||
                                                                               (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                                   && EndTime.Subtract(e.StartTime).TotalMinutes + 1 > _scheduleValidationRulesDto.grace_period_mins
                                                                               )
                                                                           ).Any())
                            {
                                var dependenctschedules_json = JsonConvert.SerializeObject(Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                                   .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                               && e.EndTime.Subtract(StartTime).TotalMinutes + 1 > _scheduleValidationRulesDto.grace_period_mins)
                                                                               ||
                                                                               (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                                   && EndTime.Subtract(e.StartTime).TotalMinutes + 1 > _scheduleValidationRulesDto.grace_period_mins
                                                                               )
                                                                           ).Select(x => x.CgtaskID));

                                result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.DEVIATION.ToString(), validation_error_message = "There is Schedule Deviation Conflict validation.", dependent_cgtaskIds_json = dependenctschedules_json });
                            }
                        }
                    }
                }
            }
            return result;
        }
         
    }
}

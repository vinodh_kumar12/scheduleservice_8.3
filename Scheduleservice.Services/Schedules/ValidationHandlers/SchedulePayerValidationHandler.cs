﻿using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class SchedulePayerValidationHandler : ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriods;

        public SchedulePayerValidationHandler(int hha, int user, IEnumerable<ClientPayerEffectivePeriodsEntity> ClientPayerEffectivePeriods)
        {
            HHA = hha;
            User = user;
            clientPayerEffectivePeriods = ClientPayerEffectivePeriods;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Validate fields
                // Client Payer Effective Period
                ClientPayerEffectivePeriodValidationHandler clientPayerEffectivePeriodValidationHandler = new ClientPayerEffectivePeriodValidationHandler(HHA, User, clientPayerEffectivePeriods);
                result.AddRange(clientPayerEffectivePeriodValidationHandler.HandleValidation(newScheduleData));
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

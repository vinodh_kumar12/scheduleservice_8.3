﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClientMaxHoursValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public ClientMaxHoursValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregivertasksRepository caregivertasksRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var weekStartday = _scheduleValidationRulesDto.Min_Duration_Mins;
                var ClientMaxHoursPerWeek = _scheduleValidationRulesDto.Max_Duration_Mins;


                if (validateScheduleRules.clientsEntity.overrideClientMaxHoursPerWeek)
                {
                    var caregivertasks = validateScheduleRules.clientschedluesdetails.Where(x => x.CLIENT_ID == caregivertasksEntity.CLIENT_ID).Select(x => x);
                    if (caregivertasks != null)
                    {
                        var weekTotalHours = caregivertasks.Select(e => string.IsNullOrEmpty(e.EDITED_HOURS) ? e.PLANNED_END_TIME.Subtract(e.PLANNED_START_TIME).TotalMinutes : (double)e.TotalHours * 60).Sum();
                        if ((weekTotalHours + (caregivertasksEntity.TotalHours * 60)) > ClientMaxHoursPerWeek)
                        {
                            string validationtype = ConflictExceptions.CLIENTMAXHOURS.ToString();

                            if (_scheduleValidationRulesDto.Soft_Warning_Validation)
                                validationtype = ConflictExceptions.CLIENTMAXHOURS.ToString();

                            result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = "Client Max hours is exceeding.", is_soft_warning = _scheduleValidationRulesDto.Soft_Warning_Validation });
                        }
                    }
                }
                else if (ClientMaxHoursPerWeek > 0)
                {
                    DayOfWeek dayOfWeek = (DayOfWeek)Enum.GetValues(typeof(DayOfWeek)).GetValue(weekStartday);
                    var weekStartDate = Common.GetWeekStartdate(caregivertasksEntity.PLANNED_DATE, dayOfWeek);
                    var weekEndDate = weekStartDate.AddDays(6);

                    // var Columns = "CGTASK_ID,PLANNED_DATE,EDITED_HOURS,PLANNED_START_TIME,PLANNED_END_TIME,STATUS,TotalHours";
                    //var Filter = _repositoryFilterConditions.EqualFilter("CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                    //                                    .EqualFilter("IS_BILLABLE", 0, FilterConditionDataTypeEnums.boolType, true)
                    //                                    .InFilter("STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                    //                                    .BetweenFilter("PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                    //                                    .Build();

                    // var caregivertasks = _caregivertasksRepository.GetAllSchedulesList(HHA, UserID, Columns, Filter).Result;
                    var caregivertasks = validateScheduleRules.clientschedluesdetails;
                    double weekTotalHours = 0;
                    if (caregivertasks.Any())
                        weekTotalHours = caregivertasks.Select(e => string.IsNullOrEmpty(e.EDITED_HOURS) ? e.PLANNED_END_TIME.Subtract(e.PLANNED_START_TIME).TotalMinutes : (double)e.TotalHours * 60).Sum();

                    if ((weekTotalHours + (caregivertasksEntity.TotalHours * 60)) > ClientMaxHoursPerWeek)
                    {
                        string validationtype = ConflictExceptions.CLIENTMAXHOURS.ToString();

                        if (_scheduleValidationRulesDto.Soft_Warning_Validation)
                            validationtype = ConflictExceptions.CLIENTMAXHOURS_WARNING.ToString();

                        result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = "Client Max hours is exceeding.", is_soft_warning = _scheduleValidationRulesDto.Soft_Warning_Validation });

                    }

                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}


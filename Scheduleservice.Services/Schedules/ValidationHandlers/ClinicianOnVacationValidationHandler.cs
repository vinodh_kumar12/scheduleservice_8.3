﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ClinicianOnVacationValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ClinicianOnVacationValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregiversRepository caregiversRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregiversRepository = caregiversRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                var Columns = "VACATIONID,CAREGIVERID,START_DATE,END_DATE";
                var Filter = _repositoryFilterConditions.EqualFilter("CAREGIVERID", caregivertasksEntity.CAREGIVER, FilterConditionDataTypeEnums.integerType)
                                    .Build();

                var caregiverVacations = _caregiversRepository.GetCaregiverVacations(HHA, UserID, Columns, Filter).Result;

                if (caregiverVacations.Any())
                {
                    var StartTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME == null ? caregivertasksEntity.PLANNED_START_TIME : caregivertasksEntity.ACCTUAL_START_TIME);
                    var EndTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME == null ? caregivertasksEntity.PLANNED_END_TIME : caregivertasksEntity.ACCTUAL_END_TIME);

                    if (caregiverVacations.Where(e => (e.START_DATE >= StartTime && e.START_DATE <= EndTime)
                                                            || (e.END_DATE >= StartTime && e.END_DATE <= EndTime)
                                                            || (StartTime >= e.START_DATE && StartTime <= e.END_DATE)
                                                            || (EndTime >= e.START_DATE && EndTime <= e.END_DATE)
                                                        ).Any())
                        result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.CLINICIANONVACATION.ToString(), validation_error_message = "Clinician is on Vacation." });
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class CaregiverEffetiveEndDataValidationHandler: ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly CaregiversEntity caregiverInfo;

        public CaregiverEffetiveEndDataValidationHandler(int hha, int user, CaregiversEntity caregiverData)
        {
            HHA = hha;
            User = user;
            caregiverInfo = caregiverData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Caregiver effective end date
                if (caregiverInfo.BlockSchedulingEffectiveDate != DateTime.MinValue)
                {
                    DateTime sDate = newScheduleData.PLANNED_DATE;
                    if (newScheduleData.PLANNED_START_TIME.Date < newScheduleData.PLANNED_END_TIME.Date)
                    {
                        sDate = newScheduleData.PLANNED_END_TIME.Date;
                    }

                    if (sDate > caregiverInfo.BlockSchedulingEffectiveDate)
                        result.Add(new ValidationErrorInfo() { validation_type = "", validation_error_message = "Cannot modify date since schedule date is after clinician effective end date" });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    class AuthorizationValidationHandler_SP : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID; 
        private readonly IAuthorization _authorization;
        
        public AuthorizationValidationHandler_SP(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                 int hha, int user,                                                 
                                                 IAuthorization authorization                                                 
                                                )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user; 
            _authorization = authorization; 
        }

        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {

            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled && caregivertasksEntity.CanBill && caregivertasksEntity.IS_AUTH_MANDATORY == true)
            {
                EditScheduleProperties editScheduleProperties = new();
                editScheduleProperties.AUTHORIZATION_ID = caregivertasksEntity.AUTHORIZATION_ID;
                editScheduleProperties.PLANNED_DATE = caregivertasksEntity.PLANNED_DATE;
                editScheduleProperties.Units = caregivertasksEntity.Units;
                editScheduleProperties.SERVICECODE_ID = caregivertasksEntity.SERVICECODE_ID;
                editScheduleProperties.PAYMENT_SOURCE = caregivertasksEntity.PAYMENT_SOURCE;
                editScheduleProperties.STATUS = caregivertasksEntity.STATUS;
                editScheduleProperties.IS_BILLED = caregivertasksEntity.IS_BILLED;
                editScheduleProperties.CLIENT_ID = caregivertasksEntity.CLIENT_ID;
                editScheduleProperties.TotalHours = caregivertasksEntity.TotalHours;
                var ret = _authorization.ValidateScheduleAuth(HHA, UserID, caregivertasksEntity.CGTASK_ID, editScheduleProperties, validateScheduleRules.confirm_softwarning);

                if (ret.Result.iserror)
                {
                    var errormessage = "Insufficient Authorization";
                    switch (ret.Result.error_id)
                    {
                        case -1:
                            errormessage = "Select schedule service does not exists";
                            break;
                        case -2:
                            errormessage = "There is no sufficient Authorization available for this schedule";
                            break;
                        case -3:
                            errormessage = "There is no sufficient Authorization available for this schedule";
                            break;
                        case -4:
                            errormessage = "Daily Limit is exceeding for this schedule";
                            break;
                        case -5:
                            errormessage = "Weekly Limit is exceeding for this schedule";
                            break;
                        case -6:
                            errormessage = "Monthly Limit is exceeding for this schedule";
                            break;
                        default:
                            errormessage = "Insufficient Authorization.";
                            break;
                    }

                    
                    if (caregivertasksEntity.STATUS == ScheduleStatusEnums.In_Progress.ToString())
                        errormessage += " to Check-in.";
                    else if(caregivertasksEntity.STATUS!=ScheduleStatusEnums.Approved.ToString())
                        errormessage += " to Check-out.";

                    string validationtype = ConflictExceptions.AUTHORIZATION.ToString();

                    if (ret.Result.is_softwarning)
                        validationtype = ConflictExceptions.AUTHORIZATION_WARNING.ToString();

                    result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = errormessage, is_soft_warning= ret.Result.is_softwarning });
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
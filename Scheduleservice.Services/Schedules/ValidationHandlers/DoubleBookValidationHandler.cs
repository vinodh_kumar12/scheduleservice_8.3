﻿using Newtonsoft.Json;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class DoubleBookValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int _hha;
        private readonly int _user; 
        private readonly ICaregivertasksRepository _caregivertaskRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICaregiversRepository _caregiversRepository; 
        public DoubleBookValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                          int hha, int user,                                          
                                          ICaregivertasksRepository caregivertaskRepository,
                                          IRepositoryFilterConditions repositoryFilterConditions,
                                          ICaregiversRepository caregiversRepository 
                                          )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            _hha = hha;
            _user = user; 
            _caregivertaskRepository = caregivertaskRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _caregiversRepository = caregiversRepository; 
        }

        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            if (_scheduleValidationRulesDto.Validationenabled)
            { 
                if (validateScheduleRules.validationErrorsDependentSchedules != null && validateScheduleRules.AllowToCheckoutifHardStopValidationExists)
                    result = ValidateDoublebookBasedOnConflictsData(validateScheduleRules);
                else
                    result = ValidatDoublebookOnrealData(validateScheduleRules); 
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        } 

        private List<ValidationErrorInfo> ValidateDoublebookBasedOnConflictsData(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity; 
            var cgtaskIDs = String.Join(",", validateScheduleRules.validationErrorsDependentSchedules.Where(e => e.exception_type == "DOUBLEBOOK").Select(e => e.dependentCgTaskID).Distinct());

            if (!string.IsNullOrEmpty(cgtaskIDs))
            {
                var Columns = "CGTASK_ID,CAREGIVER,CanBill,CONFIRMED,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,PLANNED_START_TIME,PLANNED_END_TIME";
                var Filters = _repositoryFilterConditions.InFilter("CGTASK_ID", cgtaskIDs, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var doublebookSchedules = _caregivertaskRepository.GetAllSchedulesList(_hha, _user, Columns, Filters).Result;

                var StartTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME == null ? caregivertasksEntity.PLANNED_START_TIME : caregivertasksEntity.ACCTUAL_START_TIME);
                var EndTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME != null ? caregivertasksEntity.ACCTUAL_END_TIME : caregivertasksEntity.ACCTUAL_START_TIME != null ? ((DateTime)caregivertasksEntity.ACCTUAL_START_TIME).AddMinutes(2) : caregivertasksEntity.PLANNED_END_TIME);

                if (_scheduleValidationRulesDto.discard_third_party_clinicians)
                {
                    var caregiverColumns = "CAREGIVER_ID,EMPLOYER_VENDOR";
                    var caregiverFilters = _repositoryFilterConditions.InFilter("CAREGIVER_ID", String.Join(",", doublebookSchedules.Where(e => e.CAREGIVER != null).Select(x => x.CAREGIVER).Distinct()), FilterConditionDataTypeEnums.integerType)
                                    .Build();
                    var caregivers = _caregiversRepository.GetCaregivers(_hha, _user, caregiverColumns, caregiverFilters).Result;

                    doublebookSchedules = (from ds in doublebookSchedules
                                           from c in caregivers.Where(e => e.CAREGIVER_ID == ds.CAREGIVER && (e.EMPLOYER_VENDOR == 0 || e.EMPLOYER_VENDOR == null))
                                           select ds);
                }


                var Schedules = from ds in doublebookSchedules
                                        .Where(s => (_scheduleValidationRulesDto.discard_non_billable_schedules == true && s.CanBill == true)
                                                            || (_scheduleValidationRulesDto.block_clinician_confirmation && s.CONFIRMED == true)
                                                            || (_scheduleValidationRulesDto.block_checkin && s.ACCTUAL_START_TIME != null)
                                                            || (_scheduleValidationRulesDto.block_approval && s.STATUS == "Approved")
                                                            || (_scheduleValidationRulesDto.block_schedule_creation)
                                                        )
                                select new
                                {
                                    StartTime = (DateTime)(ds.ACCTUAL_START_TIME != null ? ds.ACCTUAL_START_TIME : ds.PLANNED_START_TIME),
                                    EndTime = (DateTime)(ds.ACCTUAL_END_TIME != null ? ds.ACCTUAL_END_TIME : ds.ACCTUAL_START_TIME != null ? ((DateTime)ds.ACCTUAL_START_TIME).AddMinutes(2) : ds.PLANNED_END_TIME)

                                };


                if (Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                            .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                        && e.EndTime.Subtract(StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins)
                                                                        ||
                                                                        (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                            && EndTime.Subtract(e.StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins
                                                                        )
                                                                    ).Any())
                    result.Add( new ValidationErrorInfo() { validation_type = ConflictExceptions.DOUBLEBOOK.ToString(), validation_error_message = "There is schedule Double book Conflict validation." });
            }
            return result;
        }
         
        public List<ValidationErrorInfo> ValidatDoublebookOnrealData(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            bool requireDoubleBooknValidation = false;
            if ((_scheduleValidationRulesDto.block_clinician_confirmation && caregivertasksEntity.CONFIRMED == true)
                || (_scheduleValidationRulesDto.block_checkin && caregivertasksEntity.ACCTUAL_START_TIME != null)
                || (_scheduleValidationRulesDto.block_approval && caregivertasksEntity.STATUS == "Approved")
                || _scheduleValidationRulesDto.block_schedule_creation
               )
            {
                requireDoubleBooknValidation = true;
            }

            if (_scheduleValidationRulesDto.discard_non_billable_schedules && !caregivertasksEntity.CanBill)
                requireDoubleBooknValidation = false;


            if (requireDoubleBooknValidation)
            {
                //validate for discard doublebook
                var clientinfo = validateScheduleRules.clientsEntity;
                //_clientsRepository.GetClientsBasicInfo(_hha, _user, caregivertasksEntity.CLIENT_ID).Result;

                if (requireDoubleBooknValidation)
                {
                    //check for intermittent clients
                    var clientpayers = validateScheduleRules.clientPaymentSourcesEntity;
                    //(_clientsRepository.GetClientPaymentSourcesInfo(_hha, _user, caregivertasksEntity.CLIENT_ID, caregivertasksEntity.PAYMENT_SOURCE)).Result.FirstOrDefault();

                    if (clientpayers.SchedulingRule == "VP" && caregivertasksEntity.CONFIRMED == false)
                        requireDoubleBooknValidation = false;
                }


                if (requireDoubleBooknValidation)
                {

                    //hha branch configurations
                    var hHABranchScheduleConfiguration = validateScheduleRules.hHABranchScheduleConfigurationsEntity;


                    var doublebookschedules = validateScheduleRules.caregiverschedluesdetails;//.Where(x => x.PLANNED_DATE>= caregivertasksEntity.PLANNED_DATE.AddDays(-1) && x.PLANNED_DATE <= caregivertasksEntity.PLANNED_DATE.AddDays(1));
                    if (hHABranchScheduleConfiguration.ExcludePlannedSchedulesDoubleBookConflictWithCompletedSchedule)
                    {
                        doublebookschedules = doublebookschedules.Where(x => x.STATUS.Contains("Completed") && x.STATUS.Contains("Approved")).Select(x => x);
                    }
                    if (_scheduleValidationRulesDto.discard_non_billable_schedules)
                        doublebookschedules = doublebookschedules.Where(x => x.CanBill).Select(x => x);

                    var caregivers = validateScheduleRules.caregivers;

                    if (_scheduleValidationRulesDto.discard_third_party_clinicians)
                    {
                        if (caregivertasksEntity.CAREGIVER > 0)
                        {
                            doublebookschedules = (from ds in doublebookschedules
                                                   from c in caregivers.Where(e => e.CAREGIVER_ID == ds.CAREGIVER && (e.EMPLOYER_VENDOR == 0 || e.EMPLOYER_VENDOR == null))
                                                   select ds);
                        }
                    }

                    doublebookschedules.ToList().ForEach(x => { x.CAREGIVER_DISC_ID = caregivers.Where(y => y.CAREGIVER_ID == x.CAREGIVER).FirstOrDefault().DISCIPLINE; });
                    caregivertasksEntity.CAREGIVER_DISC_ID = caregivers.Where(y => y.CAREGIVER_ID == caregivertasksEntity.CAREGIVER).FirstOrDefault().DISCIPLINE;
                    //String.Join(",", caregivers.Select(x => x.CAREGIVER_DISC_ID).Distinct()), FilterConditionDataTypeEnums.integerType).Build();
                    //get clincian disc categortyID
                    var clinicianDiscInfo = validateScheduleRules.caregiverDisciplines;
                    //_caregiverDisciplinesRepository.GetCaregiverDiscipines(_hha, _user, columns, filters).Result;


                    if (doublebookschedules.Any() || caregivertasksEntity.ServiceCategory == 0)
                    {

                        string servicecodeids = caregivertasksEntity.SERVICECODE_ID.ToString();
                        if (doublebookschedules.Any())
                        {
                            if (!string.IsNullOrEmpty(servicecodeids))
                                servicecodeids += "," + String.Join(",", doublebookschedules.Select(x => x.SERVICECODE_ID).Distinct());
                            else
                                servicecodeids = String.Join(",", doublebookschedules.Select(x => x.SERVICECODE_ID).Distinct());
                        }


                        var serviceinfo = validateScheduleRules.servicecodes.Where(x => servicecodeids.Contains(x.SERVICE_CODE_ID.ToString()));
                        if (serviceinfo.Any())
                        {
                            if (doublebookschedules.Where(z => z.ServiceCategory == 0).Any())
                                doublebookschedules.Where(z => z.ServiceCategory == 0).ToList().ForEach(x => { x.ServiceCategory = serviceinfo.Where(y => y.SERVICE_CODE_ID == x.SERVICECODE_ID).FirstOrDefault().ServiceCategory; });

                            caregivertasksEntity.ServiceCategory = caregivertasksEntity.ServiceCategory == 0 ? serviceinfo.Where(y => y.SERVICE_CODE_ID == caregivertasksEntity.SERVICECODE_ID).FirstOrDefault().ServiceCategory : caregivertasksEntity.ServiceCategory;
                        }
                    }

                    if (clinicianDiscInfo.Any())
                    {
                        if (doublebookschedules.Where(x => x.ServiceCategory == 0 && clinicianDiscInfo.Where(y => y.CAREGIVER_DISC_ID == x.CAREGIVER_DISC_ID).Any()).Any())
                        {
                            doublebookschedules.Where(z => z.ServiceCategory == 0).ToList().ForEach(x => { x.ServiceCategory = clinicianDiscInfo.Where(y => y.CAREGIVER_DISC_ID == x.CAREGIVER_DISC_ID).FirstOrDefault().ServiceCategory; });
                            caregivertasksEntity.ServiceCategory = clinicianDiscInfo.Where(y => y.CAREGIVER_DISC_ID == caregivertasksEntity.CAREGIVER_DISC_ID).FirstOrDefault().ServiceCategory;
                        }
                    }


                    if (doublebookschedules.Where(z => z.ServiceCategory == 0).Any() || caregivertasksEntity.ServiceCategory == 0)
                    { 
                        var caregiverotherDiscinfo = validateScheduleRules.CaregiverOtherDisciplines;

                        if (caregiverotherDiscinfo.Any())
                        {
                            doublebookschedules.Where(z => z.ServiceCategory == 0)?.ToList().ForEach(x => { x.ServiceCategory = caregiverotherDiscinfo.Where(y => y.CAREGIVER_DISC_ID == x.CAREGIVER_DISC_ID && y.CaregiverID == x.CAREGIVER).FirstOrDefault().ServiceCategory; });
                            caregivertasksEntity.ServiceCategory = caregivertasksEntity.ServiceCategory == 0 ? caregiverotherDiscinfo.Where(y => y.CAREGIVER_DISC_ID == caregivertasksEntity.CAREGIVER_DISC_ID && y.CaregiverID == caregivertasksEntity.CAREGIVER).FirstOrDefault().ServiceCategory : caregivertasksEntity.ServiceCategory;
                        }
                    } 

                    if (!hHABranchScheduleConfiguration.ignoreDeviationForDifferentServiceDiscplines)
                    {
                        if (doublebookschedules.Where(x => x.ServiceCategory <= 4).Any())
                            doublebookschedules.Where(x => (x.ServiceCategory <= 4)).ToList().ForEach(y => y.ServiceCategory = 2);

                        if (caregivertasksEntity.ServiceCategory <= 4)
                            caregivertasksEntity.ServiceCategory = 2;
                    }

                    doublebookschedules = doublebookschedules.Where(x => x.ServiceCategory == caregivertasksEntity.ServiceCategory);


                    if (doublebookschedules.Any())
                    {

                        var StartTime = (DateTime)(caregivertasksEntity.ACCTUAL_START_TIME == null ? caregivertasksEntity.PLANNED_START_TIME : caregivertasksEntity.ACCTUAL_START_TIME);
                        var EndTime = (DateTime)(caregivertasksEntity.ACCTUAL_END_TIME != null ? caregivertasksEntity.ACCTUAL_END_TIME : caregivertasksEntity.ACCTUAL_START_TIME != null ? ((DateTime)caregivertasksEntity.ACCTUAL_START_TIME).AddMinutes(2) : caregivertasksEntity.PLANNED_END_TIME);

                        var Schedules = from ds in doublebookschedules
                                        select new
                                        {
                                            StartTime = (DateTime)(ds.ACCTUAL_START_TIME != null ? ds.ACCTUAL_START_TIME : ds.PLANNED_START_TIME),
                                            EndTime = (DateTime)(ds.ACCTUAL_END_TIME != null ? ds.ACCTUAL_END_TIME : ds.ACCTUAL_START_TIME != null ? ((DateTime)ds.ACCTUAL_START_TIME).AddMinutes(2) : ds.PLANNED_END_TIME),
                                            CgtaskID = ds.CGTASK_ID
                                        };

                        if (Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                               .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                           && e.EndTime.Subtract(StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins)
                                                                           ||
                                                                           (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                               && EndTime.Subtract(e.StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins
                                                                           )
                                                                       ).Any())
                        {
                            var dependenctschedules_json = JsonConvert.SerializeObject(Schedules.Where(e => (e.StartTime.AddMinutes(-1) <= StartTime ? e.EndTime.Subtract(e.StartTime).TotalMinutes : _scheduleValidationRulesDto.grace_period_mins + 1) > _scheduleValidationRulesDto.grace_period_mins)
                                                               .Where(e => (StartTime >= e.StartTime && StartTime <= e.EndTime
                                                                           && e.EndTime.Subtract(StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins)
                                                                           ||
                                                                           (e.StartTime >= StartTime && e.StartTime <= EndTime
                                                                               && EndTime.Subtract(e.StartTime).TotalMinutes > _scheduleValidationRulesDto.grace_period_mins
                                                                           )
                                                                       ).Select(x => x.CgtaskID));
                            result.Add(new ValidationErrorInfo() { validation_type = ConflictExceptions.DOUBLEBOOK.ToString(), validation_error_message = "There is schedule Double book Conflict validation.", dependent_cgtaskIds_json = dependenctschedules_json });
                        }
                    }
                }
            }

            return result;
        }


    }
}


﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class CDSValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        private readonly int HHA;
        private readonly int UserID;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHHAClinicalRepository _hHAClinicalRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly IHConfigurations2Repository _hConfigurations2Repository;
        private readonly ICaregiverTaskChildSchedulesRepository _caregiverTaskChildSchedulesRepository;
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly ICDSPayerServiceCategoryMasterRepository _cDSPayerServiceCategoryMasterRepository;
        private readonly ICDSClientBudgetLineItemRepository _cDSClientBudgetLineItemRepository;
        private readonly ICDSBudgetLineItemMasterRepository _cDSBudgetLineItemMasterRepository;
        public CDSValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto,
                                                       int hha, int user,
                                                       ICaregivertasksRepository caregivertasksRepository,
                                                       IRepositoryFilterConditions repositoryFilterConditions,
                                                       IHHAClinicalRepository hHAClinicalRepository,
                                                       IClientsRepository clientsRepository,
                                                       IHConfigurations2Repository hConfigurations2Repository,
                                                       ICaregiverTaskChildSchedulesRepository caregiverTaskChildSchedulesRepository,
                                                       ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
                                                       ICDSPayerServiceCategoryMasterRepository cDSPayerServiceCategoryMasterRepository,
                                                       ICDSClientBudgetLineItemRepository cDSClientBudgetLineItemRepository,
                                                       ICDSBudgetLineItemMasterRepository cDSBudgetLineItemMasterRepository
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
            HHA = hha;
            UserID = user;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _hHAClinicalRepository = hHAClinicalRepository;
            _clientsRepository = clientsRepository;
            _hConfigurations2Repository = hConfigurations2Repository;
            _caregiverTaskChildSchedulesRepository = caregiverTaskChildSchedulesRepository;
            _cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            _cDSPayerServiceCategoryMasterRepository = cDSPayerServiceCategoryMasterRepository;
            _cDSClientBudgetLineItemRepository = cDSClientBudgetLineItemRepository;
            _cDSBudgetLineItemMasterRepository = cDSBudgetLineItemMasterRepository;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
            if (_scheduleValidationRulesDto.Validationenabled && caregivertasksEntity.CDSClientBudgetLineItemID > 0)
            {
                bool ConsiderSplitScheduleToValidateAuthService = false;
                int CDSLineItemUsedMinutes = 0;
                int CDSLineItemCurrentMinutes = 0;
                float CDSWeeklyHours = 0;
                float CDSBudgetMinutesPerWeek = 0;
                double TotalLineItemAmountPerWeek = 0;
                double TotalLineItemUsedAmountPerWeek = 0;
                double CurrentScheduleAmountPerWeek = 0;
                int weekStartday = 0;
                var PlannedStartDateTime = Convert.ToDateTime(caregivertasksEntity.PLANNED_START_TIME);
                var PlannedEndDateTime = Convert.ToDateTime(caregivertasksEntity.PLANNED_END_TIME);
                string Columns = "";
                string Filter = "";
                int PayerCDSServiceCategoryID = 0;
                string WeeklyHourExceedingRule = "";
                bool LineItem_enableWeeklyAmountScheduleValidation = false;
                bool CDSLineItemisPayHourly = false;

                //var ClientDetails = _clientsRepository.GetClientsBasicInfo(HHA, UserID, caregivertasksEntity.CLIENT_ID).Result;
                var ClientDetails = validateScheduleRules.clientsEntity;
                int HHABranchID = ClientDetails.HHA_BRANCH_ID;

                string HConfiguration2Filter = _repositoryFilterConditions.EqualFilter("HHA_BRANCHID", HHABranchID, FilterConditionDataTypeEnums.integerType)
                                               .Build();

                var  hconfiguration2detail =  _hConfigurations2Repository._S_Schedule_GetHConfiguration2Settings(HHA, UserID, "WEEK_START_DAY", HConfiguration2Filter).Result;

                weekStartday = hconfiguration2detail.WEEK_START_DAY;
                DayOfWeek dayOfWeek = (DayOfWeek)Enum.GetValues(typeof(DayOfWeek)).GetValue(weekStartday);
                DateTime weekStartDate = Common.GetWeekStartdate(caregivertasksEntity.PLANNED_DATE, dayOfWeek);
                DateTime weekEndDate = weekStartDate.AddDays(6);


                var hhaClinicaldetail =  _hHAClinicalRepository._S_Schedule_GetHHAClinicalSettings(HHA, UserID, "ConsiderSplitScheduleToValidateAuthService", "").Result;

                ConsiderSplitScheduleToValidateAuthService = hhaClinicaldetail.ConsiderSplitScheduleToValidateAuthService;

                
                var CDSPlanYearDetails = validateScheduleRules.cDSplanyearservices;
                if(CDSPlanYearDetails.enableWeekAuthLimitOnHours)
                {
                    CDSWeeklyHours = CDSPlanYearDetails.WeeklyHours;
                }
                PayerCDSServiceCategoryID = CDSPlanYearDetails.PayerCDSServiceCategoryID;

                if(PayerCDSServiceCategoryID>0)
                {
                    Columns = "PayerCDSServiceCategoryID,WeeklyHourExceedingRule";
                    Filter = _repositoryFilterConditions.EqualFilter("PayerCDSServiceCategoryID", PayerCDSServiceCategoryID, FilterConditionDataTypeEnums.integerType)
                                .Build();

                    var cDSPayerServiceCategoryMaster = _cDSPayerServiceCategoryMasterRepository._S_Schedule_CDSPayerServiceCategoryMaster(HHA, UserID, Columns, Filter).Result.FirstOrDefault();
                    WeeklyHourExceedingRule=cDSPayerServiceCategoryMaster.WeeklyHourExceedingRule;
                }

                var cDSClientBudgetLineItemDetails = validateScheduleRules.cDSClientBudgetLineItem;

                if (cDSClientBudgetLineItemDetails.CaregiverID!= caregivertasksEntity.CAREGIVER)
                {
                    result.Add(new ValidationErrorInfo() { validation_type = "CDSVALIDATION", validation_error_message = "CDS Auth Service limit Exceeded" });

                }
                if (caregivertasksEntity.PAYRATE>0)
                {
                    CurrentScheduleAmountPerWeek = (float)(caregivertasksEntity.PAYRATE * (int)caregivertasksEntity.TotalHours);
                }
                else
                {
                    CurrentScheduleAmountPerWeek = (float)(cDSClientBudgetLineItemDetails.Rate * (int)caregivertasksEntity.TotalHours);
                }

                if (cDSClientBudgetLineItemDetails.BudgetLineItemMasterID > 0)
                {
                    Columns = "DataCapture,enableWeeklyHoursScheduleValidation,enableWeeklyAmountScheduleValidation,isPayHourly";
                    Filter = _repositoryFilterConditions.EqualFilter("BudgetLineItemMasterID", cDSClientBudgetLineItemDetails.BudgetLineItemMasterID, FilterConditionDataTypeEnums.integerType)
                            .Build();

                    var cDSBudgetLineItemMasterDetail= _cDSBudgetLineItemMasterRepository._S_Schedule_CDSBudgetLineItemMaster(HHA, UserID, Columns, Filter).Result;

                    LineItem_enableWeeklyAmountScheduleValidation = cDSBudgetLineItemMasterDetail.enableWeeklyAmountScheduleValidation;
                    CDSLineItemisPayHourly = cDSBudgetLineItemMasterDetail.isPayHourly;

                    if ((cDSBudgetLineItemMasterDetail.DataCapture== "RateHoursWeeks"
                       || cDSBudgetLineItemMasterDetail.DataCapture == "RateHours"
                       || cDSBudgetLineItemMasterDetail.DataCapture == "RateTimesPerYear") && cDSBudgetLineItemMasterDetail.enableWeeklyHoursScheduleValidation)
                    {
                        CDSBudgetMinutesPerWeek = cDSClientBudgetLineItemDetails.HoursPerWeek * 60;
                    }
                }

                if(cDSClientBudgetLineItemDetails.WeeksPerYear>1)
                {
                    TotalLineItemAmountPerWeek = (float)cDSClientBudgetLineItemDetails.TotalAmount / cDSClientBudgetLineItemDetails.WeeksPerYear;
                }
                else
                {
                    TotalLineItemAmountPerWeek = (float)cDSClientBudgetLineItemDetails.TotalAmount;
                }

                if (CDSWeeklyHours > 0)
                {
                    CDSLineItemUsedMinutes = 0;
                    CDSLineItemCurrentMinutes = 0;

                    //Columns = "CGTASK_ID,PLANNED_DATE,EDITED_HOURS,PLANNED_START_TIME,PLANNED_END_TIME,STATUS,TotalHours,Has_Child_Schedules";
                    //Filter = _repositoryFilterConditions.EqualFilter("CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                    //                                    .InFilter("STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                    //                                    .BetweenFilter("PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                    //                                    .EqualFilter("CdsPlanYearService", caregivertasksEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType)
                    //                                    .Build();

                    var caregivertasks = validateScheduleRules.clientschedluesdetails.Where(x=>x.CdsPlanYearService== caregivertasksEntity.CdsPlanYearService);
                        //_caregivertasksRepository.GetAllSchedulesList(HHA, UserID, Columns, Filter).Result;

                    if (ConsiderSplitScheduleToValidateAuthService)
                    {
                        CDSLineItemUsedMinutes = 0;
                        CDSLineItemCurrentMinutes = 0;

                        if (caregivertasks.Any())
                        {
                            CDSLineItemUsedMinutes = (int)caregivertasks.Where(y=>y.CGTASK_ID != caregivertasksEntity.CGTASK_ID).Select(x => x.TotalHours*60).Sum();
                        }

                        if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue && caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                        {
                            CDSLineItemCurrentMinutes = caregivertasksEntity.EDITED_HOURS.ConvertStringToMins();
                        }
                        else
                        {
                            TimeSpan intervalminutes = PlannedEndDateTime - PlannedStartDateTime;
                            CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                        }
                    }
                    else
                    {
                        CDSLineItemUsedMinutes = 0;
                        CDSLineItemCurrentMinutes = 0;

                        caregivertasks = caregivertasks.Where(x => x.Has_Child_Schedules == false);

                        if (caregivertasks.Any())
                        {
                            CDSLineItemUsedMinutes = (int)caregivertasks.Select(x => x.TotalHours * 60).Sum();
                        }


                        Columns = "CaregiverTaskChildSchedules.TOTALMINUTES_ACTUAL";
                        Filter = _repositoryFilterConditions.EqualFilter("CaregiverTasks.CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                            .InFilter("CaregiverTasks.STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                                                            .BetweenFilter("CaregiverTaskChildSchedules.SCHEDULE_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                                            .EqualFilter("CaregiverTaskAdditional2.CdsPlanYearService", caregivertasksEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType)
                                                            .Build();

                        var caregivertaskchildschedules = _caregiverTaskChildSchedulesRepository.GetAllChildSchedulesList(HHA, UserID, Columns, Filter).Result;

                        if (caregivertaskchildschedules.Any())
                        {
                            CDSLineItemUsedMinutes += caregivertaskchildschedules.Select(x => x.TOTALMINUTES_ACTUAL??0).Sum();
                        }

                        if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue && caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                        {
                            if (caregivertasksEntity.PLANNED_DATE.Date == weekEndDate.Date && Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME).Date !=weekEndDate.Date)
                            {
                                DateTime dt = new DateTime(caregivertasksEntity.PLANNED_DATE.Year, caregivertasksEntity.PLANNED_DATE.Month, caregivertasksEntity.PLANNED_DATE.Day,23,59,59);
                                TimeSpan intervalminutes = (TimeSpan)(dt - caregivertasksEntity.ACCTUAL_START_TIME);
                                CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                            }
                            else
                            {
                                CDSLineItemCurrentMinutes = Common.ConvertStringToMins(caregivertasksEntity.EDITED_HOURS);
                            }
                        }
                        else
                        {
                            if (caregivertasksEntity.PLANNED_DATE.Date == weekEndDate.Date && Convert.ToDateTime(caregivertasksEntity.PLANNED_END_TIME).Date != weekEndDate.Date)
                            {
                                DateTime dt = new DateTime(caregivertasksEntity.PLANNED_DATE.Year, caregivertasksEntity.PLANNED_DATE.Month, caregivertasksEntity.PLANNED_DATE.Day, 23, 59, 59);
                                TimeSpan intervalminutes = (TimeSpan)(dt - caregivertasksEntity.PLANNED_START_TIME);
                                CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                            }
                            else
                            {
                                TimeSpan intervalminutes = PlannedEndDateTime - PlannedStartDateTime;
                                CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                            }
                        }

                    }

                    if ((CDSLineItemUsedMinutes + CDSLineItemCurrentMinutes) > (CDSWeeklyHours * 60))
                    {
                        if(WeeklyHourExceedingRule.ToLower()== "hardstop")
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "CDSVALIDATION", validation_error_message = "CDS Auth Service limit Exceeded" });
                        }
                    }
                }

                if (CDSBudgetMinutesPerWeek > 0)
                {
                    CDSLineItemUsedMinutes = 0;
                    CDSLineItemCurrentMinutes = 0;

                    Columns = "CGTASK_ID,PLANNED_DATE,EDITED_HOURS,PLANNED_START_TIME,PLANNED_END_TIME,STATUS,TotalHours,Has_Child_Schedules";
                    Filter = _repositoryFilterConditions.EqualFilter("CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                        .InFilter("STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                                                        .BetweenFilter("PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                                        .NotEqualFilter("CGTASK_ID", caregivertasksEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                                        .EqualFilter("CDSClientBudgetLineItemID", caregivertasksEntity.CDSClientBudgetLineItemID, FilterConditionDataTypeEnums.integerType)
                                                        .Build();

                    var caregivertasks = _caregivertasksRepository.GetAllSchedulesList(HHA, UserID, Columns, Filter).Result;

                    if (ConsiderSplitScheduleToValidateAuthService)
                    {
                        CDSLineItemUsedMinutes = 0;
                        CDSLineItemCurrentMinutes = 0;

                        if (caregivertasks.Any())
                        {
                            CDSLineItemUsedMinutes = (int)caregivertasks.Select(x => x.TotalHours*60).Sum();
                        }

                        if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue && caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                        {
                            CDSLineItemCurrentMinutes = Common.ConvertStringToMins(caregivertasksEntity.EDITED_HOURS);
                        }
                        else
                        {
                            TimeSpan intervalminutes = PlannedEndDateTime - PlannedStartDateTime;
                            CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                        }
                    }
                    else
                    {
                        CDSLineItemUsedMinutes = 0;
                        CDSLineItemCurrentMinutes = 0;

                        caregivertasks = caregivertasks.Where(x => x.Has_Child_Schedules == false);

                        if (caregivertasks.Any())
                        {
                            CDSLineItemUsedMinutes = (int)caregivertasks.Select(x => x.TotalHours * 60).Sum();
                        }


                        Columns = "CaregiverTaskChildSchedules.TOTALMINUTES_ACTUAL";
                        Filter = _repositoryFilterConditions.EqualFilter("CaregiverTasks.CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                            .InFilter("CaregiverTasks.STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                                                            .BetweenFilter("CaregiverTasks.PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                                            .EqualFilter("CaregiverTaskAdditional2.CDSClientBudgetLineItemID", caregivertasksEntity.CDSClientBudgetLineItemID, FilterConditionDataTypeEnums.integerType)
                                                            .NotEqualFilter("CaregiverTaskChildSchedules.PARENT_CGTASK_ID", caregivertasksEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                                            .Build();

                        var caregivertaskchildschedules = _caregiverTaskChildSchedulesRepository.GetAllChildSchedulesList(HHA, UserID, Columns, Filter).Result;

                        if (caregivertaskchildschedules.Any())
                        {
                            CDSLineItemUsedMinutes += caregivertaskchildschedules.Select(x => x.TOTALMINUTES_ACTUAL??0).Sum();
                        }

                        if (caregivertasksEntity.ACCTUAL_END_TIME.HasValue && caregivertasksEntity.ACCTUAL_START_TIME.HasValue)
                        {
                            if (caregivertasksEntity.PLANNED_DATE.Date == weekEndDate.Date && Convert.ToDateTime(caregivertasksEntity.ACCTUAL_END_TIME).Date != weekEndDate.Date)
                            {
                                DateTime dt = new DateTime(caregivertasksEntity.PLANNED_DATE.Year, caregivertasksEntity.PLANNED_DATE.Month, caregivertasksEntity.PLANNED_DATE.Day, 23, 59, 59);
                                TimeSpan intervalminutes = (TimeSpan)(dt - caregivertasksEntity.ACCTUAL_START_TIME);
                                CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                            }
                            else
                            {
                                CDSLineItemCurrentMinutes = Common.ConvertStringToMins(caregivertasksEntity.EDITED_HOURS);
                            }
                        }
                        else
                        {
                            if (caregivertasksEntity.PLANNED_DATE.Date == weekEndDate.Date && caregivertasksEntity.PLANNED_START_TIME >= caregivertasksEntity.PLANNED_END_TIME)
                                if (caregivertasksEntity.PLANNED_DATE.Date == weekEndDate.Date && Convert.ToDateTime(caregivertasksEntity.PLANNED_END_TIME).Date != weekEndDate.Date)
                                {
                                    DateTime dt = new DateTime(caregivertasksEntity.PLANNED_DATE.Year, caregivertasksEntity.PLANNED_DATE.Month, caregivertasksEntity.PLANNED_DATE.Day, 23, 59, 59);
                                    TimeSpan intervalminutes = (TimeSpan)(dt - caregivertasksEntity.PLANNED_START_TIME);
                                    CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                                }
                                else
                                {
                                    TimeSpan intervalminutes = PlannedEndDateTime - PlannedStartDateTime;
                                    CDSLineItemCurrentMinutes = (int)intervalminutes.TotalMinutes;
                                }
                        }

                    }

                    if ((CDSLineItemUsedMinutes + CDSLineItemCurrentMinutes) > CDSBudgetMinutesPerWeek)
                    {
                        if (WeeklyHourExceedingRule.ToLower() == "hardstop")
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "CDSVALIDATION", validation_error_message = "CDS Budget line item limit exceeded." });
                        }
                    }
                }

                if(LineItem_enableWeeklyAmountScheduleValidation && WeeklyHourExceedingRule.ToLower()!="ignore")
                {
                    Columns = "CGTASK_ID,PLANNED_DATE,EDITED_HOURS,PLANNED_START_TIME,PLANNED_END_TIME,STATUS,TotalHours,Has_Child_Schedules";
                    Filter = _repositoryFilterConditions.EqualFilter("CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                        .InFilter("STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                                                        .BetweenFilter("PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                                        .NotEqualFilter("CGTASK_ID", caregivertasksEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                                        .EqualFilter("CdsPlanYearService", caregivertasksEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType)
                                                        .EqualFilter("CDSClientBudgetLineItemID", caregivertasksEntity.CDSClientBudgetLineItemID, FilterConditionDataTypeEnums.integerType)
                                                        .Build();

                    var caregivertasks = _caregivertasksRepository.GetAllSchedulesList(HHA, UserID, Columns, Filter).Result;

                    if (ConsiderSplitScheduleToValidateAuthService)
                    {
                        if (caregivertasks.Any())
                        {
                            if(CDSLineItemisPayHourly)
                                TotalLineItemUsedAmountPerWeek = (double)caregivertasks.Select(x => (x.TotalHours * cDSClientBudgetLineItemDetails.Rate)).Sum();
                            else
                                TotalLineItemUsedAmountPerWeek = caregivertasks.Select(x =>  cDSClientBudgetLineItemDetails.Rate).Sum();
                        }
                    }
                    else
                    {
                        caregivertasks = caregivertasks.Where(x => x.Has_Child_Schedules == false);

                        if (caregivertasks.Any())
                        {
                            if (CDSLineItemisPayHourly)
                                TotalLineItemUsedAmountPerWeek = (double)caregivertasks.Select(x => (x.TotalHours * cDSClientBudgetLineItemDetails.Rate)).Sum();
                            else
                                TotalLineItemUsedAmountPerWeek = (double)caregivertasks.Select(x => (int)cDSClientBudgetLineItemDetails.Rate).Sum();
                        }


                        Columns = "CaregiverTaskChildSchedules.TOTALMINUTES_ACTUAL";
                        Filter = _repositoryFilterConditions.EqualFilter("CaregiverTasks.CLIENT_ID", caregivertasksEntity.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                            .InFilter("CaregiverTasks.STATUS", "Planned,In_Progress,Completed,Approved", FilterConditionDataTypeEnums.stringType)
                                                            .BetweenFilter("CaregiverTasks.PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                                            .EqualFilter("CaregiverTaskAdditional2.CdsPlanYearService", caregivertasksEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType)
                                                            .EqualFilter("CaregiverTaskAdditional2.CDSClientBudgetLineItemID", caregivertasksEntity.CDSClientBudgetLineItemID, FilterConditionDataTypeEnums.integerType)
                                                            .NotEqualFilter("CaregiverTaskChildSchedules.PARENT_CGTASK_ID", caregivertasksEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                                            .Build();

                        var caregivertaskchildschedules = _caregiverTaskChildSchedulesRepository.GetAllChildSchedulesList(HHA, UserID, Columns, Filter).Result;

                        if (caregivertaskchildschedules.Any())
                        {
                            if (CDSLineItemisPayHourly)
                                TotalLineItemUsedAmountPerWeek += (double)caregivertaskchildschedules.Select(x => (int)(x.TOTALMINUTES_ACTUAL * cDSClientBudgetLineItemDetails.Rate)).Sum();
                            else
                                TotalLineItemUsedAmountPerWeek += (double)caregivertaskchildschedules.Select(x => (int)cDSClientBudgetLineItemDetails.Rate).Sum();
                        }
                    }

                    if (TotalLineItemAmountPerWeek < (TotalLineItemUsedAmountPerWeek + CurrentScheduleAmountPerWeek))
                    {
                        if (WeeklyHourExceedingRule.ToLower() == "hardstop")
                        {
                            result.Add(new ValidationErrorInfo() { validation_type = "CDSVALIDATION", validation_error_message = "CDS  budget line item amount is exceeded." });
                        }
                    }
                }
            }

            //if (_nextValidationHandler != null)
            //    result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
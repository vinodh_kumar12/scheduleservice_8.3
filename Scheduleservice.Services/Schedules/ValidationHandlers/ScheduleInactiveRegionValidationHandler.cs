﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class ScheduleInactiveRegionValidationHandler : ValidationHandler<CaregivertasksEntity>, IValidationHandler<CaregivertasksEntity>
    {
        private readonly int HHA;
        private readonly int User;
        private readonly IEnumerable<ClientInactivePeriodsEntity> clientInactivePeriods;
        private readonly PaymentsourcesEntity payerInfo;
        private readonly ClientsEntity clientInfo;
        private readonly HHABranchScheduleConfigurationsEntity agencyBranchConfigInfo;

        public ScheduleInactiveRegionValidationHandler(int hha, int user, IEnumerable<ClientInactivePeriodsEntity> clientInactivePeriodsData,
            PaymentsourcesEntity payerData,
            ClientsEntity clientData,
            HHABranchScheduleConfigurationsEntity agencyBranchConfigData)
        {
            HHA = hha;
            User = user;
            clientInactivePeriods = clientInactivePeriodsData;
            payerInfo = payerData;
            clientInfo = clientData;
            agencyBranchConfigInfo = agencyBranchConfigData;
        }

        public override List<ValidationErrorInfo> HandleValidation(CaregivertasksEntity newScheduleData)
        {
            try
            {
                // Validate
                if (clientInactivePeriods
                    .Any(x => 
                        (x.ROCDate ?? newScheduleData.PLANNED_DATE).Date.AddDays(-1).Date > x.TrasferDate.Date
                        && newScheduleData.PLANNED_DATE > x.TrasferDate
                        && newScheduleData.PLANNED_DATE < (x.ROCDate ?? newScheduleData.PLANNED_DATE.Date.AddDays(1).Date)
                    )
                )
                {
                    // medicare payer schedule
                    if(payerInfo.IsEpisodicBilling == true && payerInfo.PayerPrimaryType == true)
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot fall in inactive region (on-hold/transfer/discharge)" });
                    }
                    // non-billable and non-medicare schedule
                    else if (!newScheduleData.CanBill)
                    {
                        if (!agencyBranchConfigInfo.allowToCreateNonBillableScheduleOnInactiveRegion)
                        {
                            result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot fall in inactive region (on-hold/transfer/discharge)" });
                        }
                    }
                    // billable pediatric non-medicare schedule
                    else if (clientInfo.IsChildCare)
                    {
                        if(!agencyBranchConfigInfo.allowToHaveSchedulesInInactiveRegion_Pedi)
                        {
                            result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot fall in inactive region (on-hold/transfer/discharge)" });
                        }
                    }
                    // billable adult non-medicare schedule
                    else if(!agencyBranchConfigInfo.allowToHaveSchedulesInInactiveRegion_AdultNonMedicare)
                    {
                        result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot fall in inactive region (on-hold/transfer/discharge)" });
                    }
                }

                // Discharge date is prior to schedule date
                if((clientInfo.AssessmentSequence.EndsWith('7')
                        || clientInfo.AssessmentSequence.EndsWith('8')
                        || clientInfo.AssessmentSequence.EndsWith('9')
                    )
                    && clientInfo.DischargeDate.HasValue
                    && clientInfo.DischargeDate.Value.Date < newScheduleData.PLANNED_DATE.Date
                )
                {
                    result.Add(new ValidationErrorInfo() { validation_error_message = "Schedule date cannot be after client discharge date" });
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
    }
}

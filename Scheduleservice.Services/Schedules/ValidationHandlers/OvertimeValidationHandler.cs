﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.ValidationHandlers
{
    public class OvertimeValidationHandler : ValidationHandler<ValidateScheduleRules>, IValidationHandler<ValidateScheduleRules>
    {
        private readonly ScheduleValidationRulesDto _scheduleValidationRulesDto;
        public OvertimeValidationHandler(ScheduleValidationRulesDto scheduleValidationRulesDto
                                                        )
        {
            _scheduleValidationRulesDto = scheduleValidationRulesDto;
        }
        public override List<ValidationErrorInfo> HandleValidation(ValidateScheduleRules validateScheduleRules)
        {
            var caregivertasksEntity = validateScheduleRules.caregiverTaskEntity;
           
            if (_scheduleValidationRulesDto.Validationenabled)
            {
                double CurrentScheduleHours = 0;
                double weekTotalHours = 0;

                var weekStartday = _scheduleValidationRulesDto.Min_Duration_Mins;

                var weekStartDate = caregivertasksEntity.PLANNED_DATE.AddDays(-1 * ((7 + ((int)caregivertasksEntity.PLANNED_DATE.DayOfWeek - weekStartday)) % 7)).Date;
                var weekEndDate = weekStartDate.AddDays(6);

                var caregivertasks = validateScheduleRules.caregiverschedluesdetails;

                if (caregivertasksEntity?.CGTASK_ID > 0)
                {
                    CurrentScheduleHours = string.IsNullOrEmpty(caregivertasksEntity.EDITED_HOURS) ? caregivertasksEntity.PLANNED_END_TIME.Subtract(caregivertasksEntity.PLANNED_START_TIME).TotalMinutes : (double)caregivertasksEntity.TotalHours * 60;
                }

                if (caregivertasks != null)
                {
                    weekTotalHours = caregivertasks.Select(e => string.IsNullOrEmpty(e.EDITED_HOURS) ? e.PLANNED_END_TIME.Subtract(e.PLANNED_START_TIME).TotalMinutes : (double)e.TotalHours * 60).Sum();
                }

                if (weekTotalHours + CurrentScheduleHours > 2400)
                {
                    string validationtype = ConflictExceptions.OVERTIME.ToString();

                    if (_scheduleValidationRulesDto.Soft_Warning_Validation)
                        validationtype = ConflictExceptions.OVERTIME_WARNING.ToString();

                    result.Add(new ValidationErrorInfo() { validation_type = validationtype, validation_error_message = "Over time validation exists.", is_soft_warning = _scheduleValidationRulesDto.Soft_Warning_Validation });
                }
            }

            if (_nextValidationHandler != null)
                result.AddRange(_nextValidationHandler.HandleValidation(validateScheduleRules));

            return result;
        }
    }
}
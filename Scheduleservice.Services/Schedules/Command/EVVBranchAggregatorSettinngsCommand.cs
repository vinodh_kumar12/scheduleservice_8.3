﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class EVVBranchAggregatorSettinngsCommand : BaseRequest, IRequestWrapper<bool>
    {
        public int EvvConfigurationID { get; set; } 
        public string ModifiedData_JSON { get; set; }
        //public bool AttestationMandatoryForAllReasonCodes { get; set; }
        //public string AttestationMandatoryEvent { get; set; }
        //public bool AttestationHideFromClinician { get; set; }
    }
}

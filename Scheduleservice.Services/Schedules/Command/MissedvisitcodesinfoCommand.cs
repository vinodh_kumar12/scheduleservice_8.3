﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;


namespace Scheduleservice.Services.Schedules.Command
  
{
   

    public class MissedvisitcodesinfoCommand : IRequestWrapper<bool>
    {
         
        public int CGTask_ID { get; set; }
        public int aggregator_id { get; set; }
        public string reason_code { get; set; }
        public string reason_description { get; set; }
        public string action_code { get; set; }
        public string action_description { get; set; }

        public string action_comments { get; set; }

        public string Notes { get; set; }

        public string Context { get; set; }


    }

  
   
}

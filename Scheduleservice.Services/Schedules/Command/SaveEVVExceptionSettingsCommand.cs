﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class SaveEVVExceptionSettingsCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string Field { get; set; }
        public int OldValue { get; set; }
        public int NewValue { get; set; }
    }
}

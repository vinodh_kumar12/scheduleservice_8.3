﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class RegisterEvvTerminalPayerSchedulesCommand : IRequestWrapper<RegisterEvvTerminalPayerSchedulesDto>
    {
        public RegisterEvvTerminalPayerSchedulesCommand()
        {
            terminalPayerSchedules = new List<EVVTerminalPayerSchedules>();
        }
        public IEnumerable<EVVTerminalPayerSchedules> terminalPayerSchedules { get; set; }
    }

    public class EVVTerminalPayerSchedules
    {
        public int cgtask_id { get; set; }
        public int child_schedule_id { get; set; }
        public int primary_client_payer_id { get; set; }
        public int terminal_client_payer_id { get; set; }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class ScheduleCreateCommand : IRequestWrapper<IEnumerable<string>>
	{
     public List<ScheduleCreateSingleCommand> Schedule { get; set; }
	}
	public class ScheduleCreateSingleCommand
    {
		public string schedule_date { get; set; }

		public string planned_start_time { get; set; }

		public string planned_end_time { get; set; }

		public int client_id { get; set; }

		public int service_code_id { get; set; }

		public int payment_source_id { get; set; }
		public int caregiver_id { get; set; }

		public int? cds_auth_service_id { get; set; }

		public int miles { get; set; }

		public int place_of_service { get; set; }

		public bool is_billable { get; set; }

		public bool is_payable { get; set; }

		public string check_in_time { get; set; }

		public string check_in_location { get; set; }

		public int? check_in_treatment_location { get; set; }


		public string evv_check_in_time { get; set; }

		public string check_out_time { get; set; }

		public string check_out_location { get; set; }

		public int? check_out_treatment_location { get; set; }

		public float check_in_distance { get; set; }
		public float check_out_distance { get; set; }


		public string evv_check_out_time { get; set; }

		public string edited_hours { get; set; }

		public string payable_hours { get; set; }

		public float break_hours { get; set; }

		public float travel_time_min { get; set; }

		public int documentation_time_in_min { get; set; }
		public int? cds_plan_year { get; set; }
	}
}

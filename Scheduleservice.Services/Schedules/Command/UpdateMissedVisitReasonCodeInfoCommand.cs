﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;


namespace Scheduleservice.Services.Schedules.Command
{
    public class UpdateMissedVisitReasonCodeInfoCommand :  IRequestWrapper<bool>
    {


        public string ReasonCode { get; set; }
        public string ReasonCodesDescription { get; set; }

        public string ActionCode { get; set; }
        public string ActionCodeDescription { get; set; }

        public string Notes { get; set; }

        public string ActionCodeComments { get; set; }

        public int missed_visit_code_id { get; set; }

        public int cgtask_id { get; set; }

    }
}

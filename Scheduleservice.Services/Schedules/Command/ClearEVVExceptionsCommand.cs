﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class ClearEVVExceptionsCommand : BaseRequest, IRequestWrapper<bool>
    { 
        public int CGTASKID { get; set; }
        public string TypeOfOperation { get; set; }
        public string SystemCode { get; set; }
        public string AgencyLoginID { get; set; }
        public string SuperAdminLoginID { get; set; }
    }
}

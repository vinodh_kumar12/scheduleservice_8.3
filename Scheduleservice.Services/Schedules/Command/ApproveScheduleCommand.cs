﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class ApproveScheduleCommand : BaseRequest, IRequestWrapper<IEnumerable<string>>
    { 
        public int CGTask_ID { get; set; }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
   public class UpdateEVVExceptionCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string EVVExceptionInfo_JSON { get; set; }
        public string CaregiverTaskEvvExceptionIDs { get; set; }
        
    }
}

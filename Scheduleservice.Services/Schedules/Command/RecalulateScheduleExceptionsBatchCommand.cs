﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class RecalulateScheduleExceptionsBatchCommand : BaseRequest, IRequestWrapper<RecalculateScheduleExceptionsBatchDto>
    {
        public string batch_id { get; set; }
    }
}

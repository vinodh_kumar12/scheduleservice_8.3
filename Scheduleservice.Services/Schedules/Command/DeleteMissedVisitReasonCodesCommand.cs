﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class DeleteMissedVisitReasonCodesCommand : IRequestWrapper<bool>
    {         
        //public int missed_visit_code_id { get; set; }

        public int cgtask_id { get; set; }

    }
}

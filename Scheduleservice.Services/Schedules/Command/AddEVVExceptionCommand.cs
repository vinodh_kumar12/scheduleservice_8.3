﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class AddEVVExceptionCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string EVVExceptionInfo_JSON { get; set; }
        public int CgTaskID { get; set; }
    }
}

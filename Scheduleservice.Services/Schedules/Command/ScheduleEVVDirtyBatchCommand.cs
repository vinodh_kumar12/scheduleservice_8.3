﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class ScheduleEVVDirtyBatchCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string ScheduleEVVBatchID { get; set; }
        public bool set { get; set; }
    }
}

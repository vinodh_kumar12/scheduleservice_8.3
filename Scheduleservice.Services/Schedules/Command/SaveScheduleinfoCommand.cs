﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.Command
{
    public class SaveScheduleinfoCommand : BaseRequest, IRequestWrapper<bool>
    {
        public int CgtaskID { get; set; }
        public string ModifiedFields { get; set; }
        public string AuditInfo { get; set; }
    }
}

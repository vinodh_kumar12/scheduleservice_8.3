﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class ScheduleCheckoutCommand : IRequestWrapper<IEnumerable<string>>
    { 
        public int CGTask_ID { get; set; }
        public string checkout_time { get; set; }
        public string edited_hours { get; set; }
        public string payable_hours { get; set; }
        public string exception_code { get; set; }
        public string exception_desc { get; set; }
        public string reason_code { get; set; }
        public string reason_desc { get; set; }
        public string action_code { get; set; }
        public string action_desc { get; set; }
        public string reason_notes { get; set; }
        public bool is_attested { get; set; }
        public string caregiver_geolocation { get; set; }
        public int client_treatment_adress_id { get; set; }
        public int checkout_source { get; set; }
        public string fob_code { get; set; }
        public bool is_clincian_checkout { get; set; }
        public string reason_context { get; set; }
        public string fob_checkout_device_id { get; set; }
        public bool confirm_softwarning { get; set; }
    }
}

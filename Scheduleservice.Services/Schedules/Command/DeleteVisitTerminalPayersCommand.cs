﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Command
{
    public class DeleteVisitTerminalPayersCommand : IRequestWrapper<bool>
    {
        public int[] CgtaskIDs { get; set; }
        public int[] ChildCgtaskIDs { get; set; }
    }
}

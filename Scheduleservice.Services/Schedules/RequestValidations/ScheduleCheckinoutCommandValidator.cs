﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Interfaces.ValildationHandlers;


namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ScheduleCheckinoutCommandValidator : AbstractValidator<ScheduleCheckinoutCommand>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;

        [Obsolete]
        public ScheduleCheckinoutCommandValidator(IServiceProvider serviceProvider)
        {

            this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforScheduleCheckinoutCommandValidator();
        }
        [Obsolete]
        public void RulesforScheduleCheckinoutCommandValidator()
        {
            When(e => !string.IsNullOrEmpty(e.edited_hours), () =>
            {

                RuleFor(e => e.edited_hours).Must(_validators.BeAValidEditedHours).WithMessage("Please Enter Valid Edited Hours");
            });

            When(e => !string.IsNullOrEmpty(e.payable_hours), () =>
            {

                RuleFor(e => e.payable_hours).Must(_validators.BeAValidEditedHours).WithMessage("Please Enter Valid Payable Hours");
            });

        }
    }
}

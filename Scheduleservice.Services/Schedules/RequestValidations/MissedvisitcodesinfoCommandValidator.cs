﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using FluentValidation;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.Command;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class MissedvisitcodesinfoCommandValidator: AbstractValidator<MissedvisitcodesinfoCommand>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;

        [Obsolete]
        public MissedvisitcodesinfoCommandValidator(IServiceProvider serviceProvider)
        {

            this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforAddEVVExceptionCommandValidator();
        }

        private void RulesforAddEVVExceptionCommandValidator()
        {
            RuleFor(e => e.aggregator_id).NotEmpty();
            RuleFor(e => e.reason_code).NotEmpty();
            RuleFor(e => e.action_code).NotEmpty();
            RuleFor(e => e.CGTask_ID).NotEmpty();
            RuleFor(e => e.Context).NotEmpty().Equal("MISSED_VISIT").WithMessage("Invalid Context!");
        }
    }
}

﻿using FluentValidation;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Entities;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ScheduleSearchCountQueryValidator : AbstractValidator<ScheduleSearchCountQuery>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;
        private readonly CaregivertasksEntity _caregivertasks;

        [Obsolete]
        public ScheduleSearchCountQueryValidator(IServiceProvider serviceProvider)
        {
            this._validators = serviceProvider.GetService<ICustomValidators>();
            this._caregivertasks = new CaregivertasksEntity();
            this.RulesforScheduleSearchCountQueryValidator();
        }

        [Obsolete]
        public void RulesforScheduleSearchCountQueryValidator()
        {
            RuleFor(e => e.HHA).NotEmpty();

            RuleFor(e => e.UserId).NotEmpty();

            RuleFor(e => e.return_count).Must(x => x == false || x == true).WithMessage("Please Enter Valid return_count");

            When(x => x.start_date != null, () =>
            {
                RuleFor(e => e.start_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid Start Date");
            });

            When(x => x.end_date != null, () =>
            {
                RuleFor(e => e.end_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid End Date");
            });

            When(x => (x.include_split_schedules != null|| x.split_for_billing_parent != null), () =>
            {
                RuleFor(e => e).Must(x => x.client_id != null || x.start_date != null
                                        || x.end_date != null || x.is_evv_export_dirty != null
                                        || x.is_billable != null || x.is_invoiced != null
                                        || x.is_payrolled != null || x.schedule_status != null
                                        || x.is_evv_aggregator_export_required != null
                                        || x.payer_ids != null
                                        || x.is_evv_schedule != null)
                .WithMessage("Pass any one of the parent body parameter fields:client_id,start_date," +
                "end_date,is_evv_export_dirty,is_billable,is_invoiced,is_payrolled,schedule_status," +
                "is_evv_aggregator_export_required,payer_ids,is_evv_schedule");
            }).Otherwise(() =>
            {
                RuleFor(e => e).Must(x => x.client_id != null || x.start_date != null
                || x.end_date != null || x.is_evv_export_dirty != null || x.is_billable != null || x.is_invoiced != null
                || x.is_payrolled != null || x.schedule_status != null || x.is_evv_aggregator_export_required != null || x.payer_ids != null
                || x.split_for_billing_parent != null || x.is_evv_schedule != null || x.include_split_schedules != null)
                .WithMessage("Enter Valid input JSON Filter");
            });

            When(e => !string.IsNullOrEmpty(e.schedule_status), () =>
            {

                RuleFor(e => e.schedule_status).Must(_validators.BeAValidCommaSeparatedString).WithMessage("schedule_status field should be comma separated value");
            });

            When(e => !string.IsNullOrEmpty(e.schedule_status), () =>
            {

                RuleFor(e => e.schedule_status).Must(_validators.BeAValidScheduleStatus).WithMessage("Please Enter Valid Schedule Status");
            });

            When(x => x.output_fields != null, () =>
            {
                RuleFor(e => e.output_fields).Must(_validators.BeAUniqueStringArray).WithMessage("output_fields should be unique");

                RuleForEach(e => e.output_fields).Cascade(CascadeMode.StopOnFirstFailure).NotNull().NotEmpty().ChildRules(y =>
                {
                    y.RuleFor(z => z).NotEmpty().WithMessage("Please Enter Valid output_fields")
                    .NotNull().WithMessage("Please Enter Valid output_fields")
                    .Must((x, y) => _validators.BeAValidProperty(_caregivertasks, y)).WithMessage("Please Enter Valid output_fields");
                });
            });

            When(x => x.sort_by != null, () =>
            {
                RuleFor(e => e.sort_by.Select(x => x.sort_field).ToList()).Must(_validators.BeAUniqueStringList).WithMessage("sort_by fields should be unique");

                RuleForEach(e => e.sort_by).Cascade(CascadeMode.StopOnFirstFailure).NotNull().NotEmpty().ChildRules(y =>
                {
                    y.RuleFor(z => z).NotEmpty().WithMessage("Please Enter Valid sort_by fields")
                    .NotNull().WithMessage("Please Enter Valid sort_by fields")
                    .Must((x, y) => _validators.BeAValidProperty(_caregivertasks, y.sort_field)).WithMessage("Please Enter Valid sort_by fields");
                });
            });

            When(x => x.sort_by != null && x.output_fields != null, () =>
            {
                RuleFor(e => e).Must(x => _validators.BeAValidSortField(x.sort_by.Select(x => x.sort_field).ToList(), x.output_fields))
                .WithMessage("sort_by field should be matched with Output field(s)");
            });
        }
    }
}

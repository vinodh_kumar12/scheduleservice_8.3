﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Text;
using Scheduleservice.Core.Common.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Core.Entities;
using System.Linq;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class EVVScheduleSearchCountQueryValidator: AbstractValidator<EVVScheduleSearchCountQuery>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;


        [Obsolete]
        public EVVScheduleSearchCountQueryValidator(IServiceProvider serviceProvider)
        {
            this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforEVVScheduleSearchCountQueryValidator();
        }

        [Obsolete]
        public void RulesforEVVScheduleSearchCountQueryValidator()
        {
            RuleFor(e => e.HHA).NotEmpty();

            RuleFor(e => e.UserId).NotEmpty();

            When(x => x.start_date != null, () =>
            {
                RuleFor(e => e.start_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid Start Date");
            });

            When(x => x.end_date != null, () =>
            {
                RuleFor(e => e.end_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid End Date");
            });

            When(e => !string.IsNullOrEmpty(e.schedule_status), () =>
            {

                RuleFor(e => e.schedule_status).Must(_validators.BeAValidScheduleStatus).WithMessage("Please Enter Valid Schedule Status");
            });

            RuleFor(e => e).Must(x => x.client_id != null || x.start_date != null
            || x.end_date != null || x.is_billable != null || x.is_invoiced != null
            || x.is_payrolled != null || x.schedule_status != null || x.payer_ids != null
            || x.split_for_billing_parent != null || x.is_evv_schedule != null)
                .WithMessage("Enter Valid input JSON Filter");

            When(e => !string.IsNullOrEmpty(e.schedule_status), () =>
            {
                RuleFor(e => e.schedule_status).Must(_validators.BeAValidCommaSeparatedString).WithMessage("schedule_status field should be comma separated value");
            });
        }
    }
}

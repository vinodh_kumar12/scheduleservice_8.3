﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class CreateScheduleCommandValidator : AbstractValidator<ScheduleCreateCommand>
    {
        public CreateScheduleCommandValidator()
        {
            RuleForEach(x => x.Schedule).SetValidator(new ScheduleCreateSingleCommandValidator());
            //RuleFor(e => e.caregiver_id).NotEqual(0);

        }
        public class ScheduleCreateSingleCommandValidator : AbstractValidator<ScheduleCreateSingleCommand>
        {
            public ScheduleCreateSingleCommandValidator()
            {

                RuleFor(e => e.schedule_date).NotEmpty();
                RuleFor(e => e.client_id).NotEqual(0);
                RuleFor(e => e.payment_source_id).NotEqual(0);
                RuleFor(e => e.service_code_id).NotEqual(0);
                RuleFor(e => e.schedule_date).Must(IsDateFormatCorrect).WithMessage("Invalid Date/Time");
            
            }
            private bool IsDateFormatCorrect(string dateString)
            {
                string formats = "MM/dd/yyyy";
                DateTime dateValue;
                if (DateTime.TryParseExact(dateString, formats,
                                     CultureInfo.InvariantCulture,
                                     DateTimeStyles.None,
                                     out dateValue))
                    return true;
                else
                    return false;
            }

        }
        
    }
}

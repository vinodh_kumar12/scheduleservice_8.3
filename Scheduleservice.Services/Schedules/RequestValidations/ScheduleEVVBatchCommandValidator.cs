﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleEVVBatchCommandValidator : AbstractValidator<ScheduleEVVBatchCommand>
    {
        public ScheduleEVVBatchCommandValidator()
        {
            RuleFor(e => e.ScheduleEVVBatchID).NotEmpty().NotNull(); 
            RuleFor(e => e.ScheduleEVVBatchID).NotEmpty().NotNull().Must(ValidateGuiD);

            RuleFor(e => e.HHA).NotEqual(0); 
            RuleFor(e => e.UserId).NotEqual(0); 
        }

        private bool ValidateGuiD(string val)
        {
            return Guid.TryParse(val, out var result);
        }
    }
}

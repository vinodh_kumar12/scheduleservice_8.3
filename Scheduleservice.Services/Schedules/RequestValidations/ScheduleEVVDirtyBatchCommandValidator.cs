﻿using FluentValidation;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ScheduleEVVDirtyBatchCommandValidator:AbstractValidator<ScheduleEVVDirtyBatchCommand>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;

        [Obsolete]
        public ScheduleEVVDirtyBatchCommandValidator(IServiceProvider serviceProvider)
        {
            this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforScheduleEVVDirtyBatchCommandValidator();
        }


        [Obsolete]
        public void RulesforScheduleEVVDirtyBatchCommandValidator()
        {
            RuleFor(e => e.HHA).NotEmpty();
            RuleFor(e => e.UserId).NotEmpty();
            RuleFor(e => e.ScheduleEVVBatchID).NotEmpty().NotNull().Must(_validators.BeAValidGUID).WithMessage("Enter Valid BatchID");
        }

    }
}

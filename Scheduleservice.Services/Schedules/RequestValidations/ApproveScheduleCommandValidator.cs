﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ApproveScheduleCommandValidator : AbstractValidator<ApproveScheduleCommand>
    {
        public ApproveScheduleCommandValidator()
        {
            RuleFor(e => e.CGTask_ID).NotEmpty().NotNull().NotEqual(0);
            RuleFor(e => e.HHA).NotEqual(0);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Scheduleservice.Services.Schedules.Command;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ClearEVVExceptionsCommandValidator : AbstractValidator<ClearEVVExceptionsCommand>
    {
        public ClearEVVExceptionsCommandValidator()
        {
            RuleFor(e => e.CGTASKID).NotNull().NotEqual(0);
            //RuleFor(e => e.TypeOfOperation).NotNull().NotEmpty();
        }
    }
}

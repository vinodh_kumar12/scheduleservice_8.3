﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ScheduleValidationQueryValidator : AbstractValidator<ScheduleValidationQuery>
    {
        public ScheduleValidationQueryValidator()
        {
            RuleFor(e => e.schedule_uuid).NotEmpty().NotNull();
            RuleFor(e => e.schedule_uuid).NotEmpty().NotNull().Must(ValidateGuiD).WithMessage("Enter Valid schedule_uuid");

            RuleFor(e => e.HHA).NotEqual(0);
            RuleFor(e => e.UserId).NotEqual(0);
        }

        private bool ValidateGuiD(string val)
        {
            return Guid.TryParse(val, out var result);
        }
    }
}

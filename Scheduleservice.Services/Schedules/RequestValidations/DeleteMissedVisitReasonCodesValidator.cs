﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class DeleteMissedVisitReasonCodesValidator : AbstractValidator<DeleteMissedVisitReasonCodesCommand>
    { 

        [Obsolete]
        public DeleteMissedVisitReasonCodesValidator(IServiceProvider serviceProvider)
        {

            //this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforScheduleCheckoutCommandValidator();
        }

         [Obsolete]
        public void RulesforScheduleCheckoutCommandValidator()
        {
            RuleFor(e => e.cgtask_id).NotEmpty().NotNull().NotEqual(0).WithMessage("Please provide cgtask_id");
        }
    }
}

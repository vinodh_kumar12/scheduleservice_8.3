﻿using FluentValidation;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class EvvTerminalPayerSearchSchedulesQueryValidator : AbstractValidator<EvvTerminalPayerSearchSchedulesQuery>
    {
        List<string> Schedulestatuslist = new List<string>() { "approved", "completed", "deleted ", "missedvisit", "planned","" };
        List<string> Terminalpayerhierarchy = new List<string>() { "s", "d", "t","" };
        List<string> Exportstatus = new List<string>() { "failed", "pending", "success","" };
        public EvvTerminalPayerSearchSchedulesQueryValidator()
        {
            //RuleFor(x => x.schedule_date_from ).NotEmpty().WithMessage("From date should not be empty");
            //RuleFor(x => x.schedule_date_to).NotEmpty().WithMessage("To date should not be empty");
            RuleFor(x => x.schedule_date_from)
                .Must(datetimefunc)
                .WithMessage("Enter date in 'MM/DD/YYYY' format");

            RuleFor(x => x.schedule_date_to)
                .Must(datetimefunc)
                .WithMessage("Enter date in 'MM/DD/YYYY' format");

            //RuleFor(x => x.schedule_status.ToLower())
            //    .Must(x => Schedulestatuslist.Contains(x))
            //    .WithMessage("Enter proper Schedule Status");

            RuleFor(x => x.terminal_payer_hierarchy.ToLower())
                .Must(x => Terminalpayerhierarchy.Contains(x))
                .WithMessage("Enter proper Terminal Payer Hierarchy");

            RuleFor(x => x.export_status.ToLower())
                .Must(x => Exportstatus.Contains(x))
                .WithMessage("Enter proper Export Status");
        }
        private bool datetimefunc(string val)
        {
            var res = false;
            if (val == "")
            {
                res = true;
            }
            else
            {
                bool isValid = DateTime.TryParse(val, out DateTime dt);
                if (isValid)
                {
                    res = true;
                }
            }
            return res;
        }
    }
}

﻿using FluentValidation;
using Scheduleservice.Core.Interfaces.ValildationHandlers;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class ScheduleEVVBatchQueryValidator: AbstractValidator<ScheduleEVVBatchQuery>
    {
        private readonly ICustomValidators _validators;
        private readonly IServiceProvider _serviceProvider;

        [Obsolete]
        public ScheduleEVVBatchQueryValidator(IServiceProvider serviceProvider)
        {

            this._validators = serviceProvider.GetService<ICustomValidators>();
            this.RulesforScheduleEVVBatchQueryValidator();
        }
        [Obsolete]
        public void RulesforScheduleEVVBatchQueryValidator()
        {

            RuleFor(e => e.HHA).NotEmpty();
            RuleFor(e => e.UserId).NotEmpty();

            When(x => x.start_date != null, () =>
            {
                RuleFor(e => e.start_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid Start Date");
            });

            When(x => x.end_date != null, () =>
            {
                RuleFor(e => e.end_date).Must(_validators.BeAValidDate).WithMessage("Please Enter Valid End Date");
            });

            When(e => !string.IsNullOrEmpty(e.visit_status), () =>
            {

                RuleFor(e => e.visit_status).Must(_validators.BeAValidScheduleStatus).WithMessage("Please Enter Valid Schedule Status");
            });

            When(e => !string.IsNullOrEmpty(e.hha_branch_ids), () =>
            {

                RuleFor(e => e.hha_branch_ids).Must(_validators.BeAValidCommaSeparatedString).WithMessage("HHABranchIDs field should be comma separated value");
            });

            When(e => !string.IsNullOrEmpty(e.payer_ids), () =>
            {

                RuleFor(e => e.payer_ids).Must(_validators.BeAValidCommaSeparatedString).WithMessage("PayerIDs field should be comma separated value");
            });

            When(e => !string.IsNullOrEmpty(e.visit_status), () =>
            {

                RuleFor(e => e.visit_status).Must(_validators.BeAValidCommaSeparatedString).WithMessage("visit_status field should be comma separated value");
            });

            When(x => (x.hha_branch_ids != null || x.aggregator_id != null || x.context!=null), () =>
            {
                RuleFor(e => e).Must(x => x.client_id != null || x.start_date != null
                                        || x.end_date != null || x.payer_ids != null
                                        || x.visit_status != null || x.billed != null
                                        || x.billable != null || x.client_id_list.Length > 0)
                .WithMessage("Pass any one of the parent query parameter fields:client_id (or) client_id_list,start_date," +
                "end_date,payer_ids,visit_status,billed,billable");

            }).Otherwise(() =>
            {
                RuleFor(e => e).Must(x=>x.client_id != null || x.start_date != null
                                        || x.end_date != null || x.payer_ids != null
                                        || x.visit_status != null || x.billed != null
                                        || x.billable != null || x.client_id_list.Length > 0
                                        || x.context!=null || x.hha_branch_ids!=null || x.aggregator_id!=null)
                .WithMessage("Enter any one of the Query paramter field such as client_id (or) client_id_list,payer_ids,start_date,end_date," +
                "hha_branch_ids,context,aggregator_id,visit_status,billed,billable");
            });
        }
    }
}

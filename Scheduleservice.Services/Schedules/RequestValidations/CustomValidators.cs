﻿using Scheduleservice.Core.Interfaces.ValildationHandlers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.RequestValidations
{
    public class CustomValidators : ICustomValidators
    {
        public bool BeAValidProperty(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        public bool BeAValidSortField(List<string> FieldName, string[] output_fields)
        {
            bool IsValid = false;

            if (FieldName != null && output_fields != null)
            {
                int sortFieldCount = FieldName.Count;
                int sortFieldMatchOutputMatchCount = 0;

                foreach (var sortfield in FieldName)
                {
                    foreach (var outputfield in output_fields)
                    {
                        if(sortfield == outputfield)
                        {
                            sortFieldMatchOutputMatchCount++;
                            break;
                        }
                    }
                }

                if(sortFieldMatchOutputMatchCount== sortFieldCount)
                {
                    IsValid = true;
                }
            }
                
            return IsValid;
        }

        public bool BeAUniqueStringList(List<string> FieldName)
        {
            bool IsValid = false;
            if(FieldName!=null)
            {
                IsValid = FieldName.Distinct().Count() == FieldName.Count();
            }
            return IsValid;
        }
        public bool BeAUniqueStringArray(string[] output_fields)
        {
            bool IsValid = false;
            if(output_fields!=null)
            {
                IsValid = output_fields.Distinct().Count() == output_fields.Count();
            }
            return IsValid;
        }

        public bool BeAValidCommaSeparatedString(string CommaSeparatedString)
        {

            if (CommaSeparatedString.EndsWith(","))
            {
                return false;
            }

            var splitCodes = CommaSeparatedString.Split(",");

            foreach (var code in splitCodes)
            {
                if (code.Trim() == String.Empty)
                {
                    return false;
                }
            }

            return true;
        }

        public bool BeAValidGUID(string val)
        {
            return Guid.TryParse(val, out var result);
        }

        public bool BeAValidDate(string date)
        {
            string[] formats = { "M/d/yyyy", "MM/dd/yyyy", "yyyy/MM/dd", "yyyy/M/d", "MM-dd-yyyy", "yyyy-MM-dd", "M-d-yyyy" };
            DateTime dDate;
            bool ret = false;
            if (date != null && date != "")
            {
                if (DateTime.TryParseExact(date, formats, null, DateTimeStyles.None, out dDate) == true)
                {
                    ret = true;
                }
                else
                    ret = false;
            }
            else
                ret = true;

            return ret;
        }

        public bool BeAValidScheduleStatus(string CommaSeparatedString)
        {
            if (CommaSeparatedString.EndsWith(","))
            {
                return false;
            }

            var splitCodes = CommaSeparatedString.Split(",");


            foreach (var code in splitCodes)
            {
                if (code.Trim() == String.Empty)
                {
                    return false;
                }
                else if (!(code == "Planned" || code == "In_Progress"
                    || code == "Completed" || code == "Approved"
                    || code == "Deleted" || code == "MissedVisit"
                    || code == "NotCreated" || code == "ReSchedule"))
                {
                    return false;
                }
            }

            return true;
        }

        public bool BeAValidEditedHours(string EditedHours)
        {
            bool IsValid = false;

            if(!string.IsNullOrEmpty(EditedHours))
            {
                string[] parts = EditedHours.Split(':');
                if (parts.Count() == 2)
                {
                    int hour = int.Parse(parts[0]);
                    int mins = int.Parse(parts[1]);
                    if (hour >= 0 && (mins >= 0 && mins < 60))
                        return true;
                }
            }

            return IsValid;
        }
    }
}

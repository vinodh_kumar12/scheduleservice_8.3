﻿using Newtonsoft.Json;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Text.Json;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.DTO.EVV;
using Scheduleservice.Core.Schedules;

namespace Scheduleservice.Services.Schedules.Service
{
    public class AutoGenerateException : IAutoGenerateException
    {
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEvvExceptionsRepository;
        private readonly ICaregivertasksRepository caregivertasksRepository;
        private readonly IEvvScheduleEditCodesMasterRepository _evvScheduleEditCodesMasterRepository;
        private readonly IHHAEvvExceptionEventsRepository c_EvvExceptionEventsRepository;
        private readonly IUAEvvExceptionEventsRepository ua_EvvExceptionEventsRepository;
        private readonly IUAEvvExceptionEventCodesRepository uAEvvExceptionEventCodesRepository;
        private readonly IHHAEvvExceptionEventCodesRepository hHAEvvExceptionEventCodesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;        
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        private readonly IPaymentsourcesRepository _IPaymentsourcesRepository;

        public AutoGenerateException(ICaregiverTaskEVVExceptionsRepository caregiverTaskEvvExceptions, ICaregivertasksRepository _caregivertasksRepository,
            IEvvScheduleEditCodesMasterRepository evvScheduleEditCodesMaster,
            IHHAEvvExceptionEventsRepository i_C_EvvExceptionEventsRepository,
            IUAEvvExceptionEventsRepository i_UA_EvvExceptionEventsRepository,
            IUAEvvExceptionEventCodesRepository _uAEvvExceptionEventCodesRepository,
            IHHAEvvExceptionEventCodesRepository _hHAEvvExceptionEventCodesRepository,
            IRepositoryFilterConditions repositoryFilterConditions, 
            IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository, 
            IPaymentsourcesRepository iPaymentsourcesRepository)
        {
            this._caregiverTaskEvvExceptionsRepository = caregiverTaskEvvExceptions;
            this.caregivertasksRepository = _caregivertasksRepository;
            this._evvScheduleEditCodesMasterRepository = evvScheduleEditCodesMaster;
            this.c_EvvExceptionEventsRepository = i_C_EvvExceptionEventsRepository;
            this.ua_EvvExceptionEventsRepository = i_UA_EvvExceptionEventsRepository;
            this.uAEvvExceptionEventCodesRepository = _uAEvvExceptionEventCodesRepository;
            this.hHAEvvExceptionEventCodesRepository = _hHAEvvExceptionEventCodesRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            this._IPaymentsourcesRepository = iPaymentsourcesRepository;
        }
        public async Task<IEnumerable<CaregivertasksEntity>> GetExceptionEligibleSchedules(int HHA, int UserID, string SystemCode, int GraceMin)
        {
            //UNSCHEDULED_VISIT   
            //Get Exceptions eligible schedules
            var EVVExceptionEligibleSchedules = await caregivertasksRepository.GetExceptionEligibleSchedulesList(HHA, UserID, SystemCode, GraceMin);

            return EVVExceptionEligibleSchedules;
        }

        public async Task<bool> RaiseEVVExceptionsOnCheckinCheckout(int HHA, int User, CaregivertasksEntity oldScheduleDetails, ScheduleEVVInfo scheduleEVVInfo,
           int EVVVendorVersionMasterID, int EvvCheckinReasonID = 0, int EvvCheckoutReasonID = 0,bool is_clincian_checkout = false)
        {
            bool issuccess = false; 
             
            bool RaiseExceptionWhenCS_VR_Missed = true;
            var hHAScheduleAuthSettingsEntity = await _hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(HHA, User);
             
            var RequiredEvents = Common.GetEVVExceptionEvents(oldScheduleDetails, scheduleEVVInfo, hHAScheduleAuthSettingsEntity.FirstOrDefault(), EVVVendorVersionMasterID, !is_clincian_checkout, RaiseExceptionWhenCS_VR_Missed, EvvCheckinReasonID, EvvCheckoutReasonID, false, false);

            if (RequiredEvents.Any())
            {
                foreach (var item in RequiredEvents)
                {
                    issuccess = await this.GenerateException(HHA, User, oldScheduleDetails.CGTASK_ID, item);
                }
            }
            else
                issuccess = true;

            return issuccess;
        }
         
        public async Task<bool> GenerateException(int HHA, int UserId, int CgtaskID, EVVExceptioninfoDto eVVExceptioninfoDto)
        {
            bool ret = false;
            var Code = "";
            var Description = ""; 
            string EVVExceptionInfo = "";
            var PayerID = 0;
            var MasterPayerCode="";
            
            ScheduleExceptionDetailsDto scheduleExceptionDetailsDto = new ScheduleExceptionDetailsDto();

            scheduleExceptionDetailsDto.Context = eVVExceptioninfoDto.Context;
            scheduleExceptionDetailsDto.SystemCode = eVVExceptioninfoDto.Event;
            scheduleExceptionDetailsDto.EVVVendorVersionMasterId = eVVExceptioninfoDto.EVVVendorVersionMasterID;
            try
            {
                var Scheduleinfo = await caregivertasksRepository.GetScheduleBasicInfo(HHA, UserId, CgtaskID);
                PayerID=Scheduleinfo.PAYMENT_SOURCE;

                string payercolumns = "PaymentSources.MasterPayerCode";
                string payerfilters = _repositoryFilterConditions.EqualFilter(" PaymentSources.PAYMENT_SOURCE_ID ", PayerID, FilterConditionDataTypeEnums.integerType).Build();
                var PayerEVVDetails = await _IPaymentsourcesRepository.GetPaymentsources(HHA, UserId, payercolumns, payerfilters);

                MasterPayerCode = PayerEVVDetails.FirstOrDefault().MasterPayerCode;

                var EVVExceptionCodeList = await _evvScheduleEditCodesMasterRepository.GetHHAEVVCodes(HHA, UserId, "Exception", eVVExceptioninfoDto.EVVVendorVersionMasterID);

                scheduleExceptionDetailsDto.isKanTimeCode = true;
                if (!string.IsNullOrEmpty(eVVExceptioninfoDto.Code))
                {
                    scheduleExceptionDetailsDto.isAutoGeneratedException = false;
                    scheduleExceptionDetailsDto.Code = eVVExceptioninfoDto.Code;
                    scheduleExceptionDetailsDto.Description = eVVExceptioninfoDto.Description;
                    scheduleExceptionDetailsDto.Notes = eVVExceptioninfoDto.Notes;

                    if (EVVExceptionCodeList.Where(e => e.Code == eVVExceptioninfoDto.Code).Any())
                    {
                        if (EVVExceptionCodeList.Any(x => !string.IsNullOrEmpty(x.MasterPayerCodes) && x.MasterPayerCodes.Split(',').Any(v => v == MasterPayerCode)))
                        {
                            EVVExceptionCodeList = EVVExceptionCodeList.Where(x => !string.IsNullOrEmpty(x.MasterPayerCodes) && x.MasterPayerCodes.Split(',').Any(v => v == MasterPayerCode));
                        }

                        scheduleExceptionDetailsDto.isKanTimeCode = EVVExceptionCodeList.Where(e => e.Code == eVVExceptioninfoDto.Code).FirstOrDefault().isKanTimeCode;
                        scheduleExceptionDetailsDto.ExceptionSystemType = EVVExceptionCodeList.Where(e => e.Code == eVVExceptioninfoDto.Code).FirstOrDefault().ExceptionSystemType;
                    }

                    if (eVVExceptioninfoDto.ResolvedReasoncodeID > 0)
                    {
                        scheduleExceptionDetailsDto.isResolved = true;
                        scheduleExceptionDetailsDto.ReasolvedCaregiverTaskEvvReasonID = eVVExceptioninfoDto.ResolvedReasoncodeID;
                        scheduleExceptionDetailsDto.isAutoGeneratedException = true;
                    }

                    EVVExceptionInfo = JsonConvert.SerializeObject(scheduleExceptionDetailsDto);
                    ret = await _caregiverTaskEvvExceptionsRepository.AddCaregiverTaskEVVExceptions(HHA, UserId, CgtaskID, EVVExceptionInfo);

                }
                else
                {

                    //get EVV Events for the HHA, Kantime Specific
                    var HHAEVVEvents = await c_EvvExceptionEventsRepository.GetHHAEVVEvents(HHA, UserId, eVVExceptioninfoDto.EVVVendorVersionMasterID);

                    //Get EVV Events for the Vendor
                    var EVVVendorEvents = await ua_EvvExceptionEventsRepository.GetEVVVendorEvents(HHA, UserId, eVVExceptioninfoDto.EVVVendorVersionMasterID);

                    scheduleExceptionDetailsDto.isAutoGeneratedException = true;
                    IEnumerable<HHAEVVExceptionEventCodesEntity> EVVEventCodes = new List<HHAEVVExceptionEventCodesEntity>();

                    if (HHAEVVEvents?.Count() > 0)
                    {//check if HHA specific vendor Exception event is enabled 
                        var AgencyEventID = HHAEVVEvents?.Where(e => e.Event == scheduleExceptionDetailsDto.SystemCode).FirstOrDefault()?.EvvExceptionEventID;
                        if (AgencyEventID > 0)
                        {
                            string FilterDetails = _repositoryFilterConditions.EqualFilter("EvvExceptionEventID", AgencyEventID, FilterConditionDataTypeEnums.integerType).Build();

                            EVVEventCodes = await hHAEvvExceptionEventCodesRepository.GetHHAEVVExceptionEventCodes(HHA, UserId, eVVExceptioninfoDto.EVVVendorVersionMasterID, FilterDetails, "");

                        }
                    }
                    else if (EVVVendorEvents?.Count() > 0)
                    {//check if Vendor level Exception event is enabled 
                        var VendorEventID = EVVVendorEvents?.Where(e => e.Event == scheduleExceptionDetailsDto.SystemCode).FirstOrDefault()?.ExceptionEventMasterID;
                        if (VendorEventID > 0)
                        {
                            string FilterDetails = _repositoryFilterConditions.EqualFilter("ExceptionEventMasterID", VendorEventID, FilterConditionDataTypeEnums.integerType).Build();

                            var VendorEVVEventCodes = await uAEvvExceptionEventCodesRepository.GetUAEVVExceptionEventCodes(UserId, eVVExceptioninfoDto.EVVVendorVersionMasterID, FilterDetails, "");

                            if (VendorEVVEventCodes?.Count() > 0)
                            {
                                EVVEventCodes = (from s in VendorEVVEventCodes

                                                 select new HHAEVVExceptionEventCodesEntity()
                                                 {
                                                     Code = s.Code,
                                                     EvvExceptionEventID = s.ExceptionEventCodeID,
                                                     EvvVendorversionMasterID = s.ExceptionEventMasterID,
                                                 }
                                                );
                            }
                        } 
                    }

                    if (EVVEventCodes?.Count() > 0)
                    {
                        foreach (var item in EVVEventCodes)
                        {
                            if (EVVExceptionCodeList.Any(x => !string.IsNullOrEmpty(x.MasterPayerCodes) && x.MasterPayerCodes.Split(',').Any(v => v == MasterPayerCode)))
                            {
                                EVVExceptionCodeList = EVVExceptionCodeList.Where(x => !string.IsNullOrEmpty(x.MasterPayerCodes) && x.MasterPayerCodes.Split(',').Any(v => v == MasterPayerCode));
                            }
                            Code = item.Code;

                            if (!string.IsNullOrEmpty(Code))
                            {
                                if (EVVExceptionCodeList?.Where(e => e.Code == Code).FirstOrDefault() != null)
                                {

                                    Description = EVVExceptionCodeList?.Where(e => e.Code == Code).FirstOrDefault().Description;
                                }

                                if (!string.IsNullOrEmpty(Description))
                                {
                                    scheduleExceptionDetailsDto.Code = Code;
                                    scheduleExceptionDetailsDto.Description = Description;
                                    scheduleExceptionDetailsDto.isKanTimeCode = EVVExceptionCodeList.Where(e => e.Code == Code).FirstOrDefault().isKanTimeCode;
                                    scheduleExceptionDetailsDto.ExceptionSystemType = EVVExceptionCodeList.Where(e => e.Code == Code).FirstOrDefault().ExceptionSystemType;

                                    if (eVVExceptioninfoDto.ResolvedReasoncodeID > 0)
                                    {
                                        scheduleExceptionDetailsDto.isResolved = true;
                                        scheduleExceptionDetailsDto.ReasolvedCaregiverTaskEvvReasonID = eVVExceptioninfoDto.ResolvedReasoncodeID;
                                    }

                                    EVVExceptionInfo = JsonConvert.SerializeObject(scheduleExceptionDetailsDto);
                                    ret = await _caregiverTaskEvvExceptionsRepository.AddCaregiverTaskEVVExceptions(HHA, UserId, CgtaskID, EVVExceptionInfo);

                                }
                            }
                        } 
                    }
                    else
                        ret = true;
                }

            }            
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        public async Task<bool> AutoRaiseExceptionForSchedules(int HHA, int UserID, string Event, List<_UA_EvvExceptionEventsEntity> evvExceptionEventsDto, IEnumerable<CaregivertasksEntity> schedules)
        {
            bool ret = true;
            int EVVVendorVersionMasterID ;

            foreach (var item in schedules)
            {
                EVVVendorVersionMasterID = Convert.ToInt32(item.EvvVendorVersionMasterID);

                _UA_EvvExceptionEventsEntity exceptionEventsDto = evvExceptionEventsDto.Where(x => x.EVVVendorVersionMasterID == EVVVendorVersionMasterID && x.Event == Event).FirstOrDefault();

                if (exceptionEventsDto != null)
                {
                    EVVExceptioninfoDto eVVExceptioninfoDto = new EVVExceptioninfoDto(){ 
                        Event = exceptionEventsDto.Event,
                        EVVVendorVersionMasterID = EVVVendorVersionMasterID,
                        Context = "Visit"
                    };

                    ret = await GenerateException(HHA, UserID, item.CGTASK_ID, eVVExceptioninfoDto);
                }
            } 

            return ret;
        } 
    }
}

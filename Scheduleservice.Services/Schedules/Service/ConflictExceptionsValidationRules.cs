﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Service
{
    public class ConflictExceptionsValidationRules : IConflictExceptionsValidationRules
    {
        private readonly IClientsRepository _clientsRepository;
        private readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository;
        private readonly IHHAScheduleConfigurationsRepository _hHAScheduleConfigurationsRepository;
        private readonly IHHAScheduleConflictExceptionsRepository _hHAScheduleConflictExceptionsRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IUserRolesRepository _userRolesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public ConflictExceptionsValidationRules(IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository,
            IClientsRepository clientsRepository,
            ICaregiversRepository caregiversRepository,
            IHHAScheduleConflictExceptionsRepository hHAScheduleConflictExceptionsRepository,
            IServiceCodesRepository serviceCodesRepository,
            IPaymentsourcesRepository paymentsourcesRepository,
            IHHAScheduleConfigurationsRepository hHAScheduleConfigurationsRepository,
            IUserRolesRepository userRolesRepository,
            IRepositoryFilterConditions repositoryFilterConditions
            )
        {
            _hHAScheduleConfigurationsRepository = hHAScheduleConfigurationsRepository;
            _hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository;
            _caregiversRepository = caregiversRepository;
            _hHAScheduleConflictExceptionsRepository = hHAScheduleConflictExceptionsRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _clientsRepository = clientsRepository;
            _userRolesRepository = userRolesRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public async Task<IEnumerable<ScheduleValidationRulesDto>> GetConflictExceptionsValidationRules(int HHA, int UserId, ValidateScheduleRules validateScheduleRules)
        {
            List<ScheduleValidationRulesDto> scheduleValidationRulesDtos = new List<ScheduleValidationRulesDto>();
            bool AllowToEditPastVisit = false;
            try
            {
                int HHA_BRANCH_ID = validateScheduleRules.clientsEntity.HHA_BRANCH_ID;
                int PAYMENT_SOURCE = validateScheduleRules.caregiverTaskEntity.PAYMENT_SOURCE;
                int CLIENT_ID = validateScheduleRules.caregiverTaskEntity.CLIENT_ID;
                int SERVICECODE_ID = validateScheduleRules.caregiverTaskEntity.SERVICECODE_ID;
                int CAREGIVER = validateScheduleRules.caregiverTaskEntity.CAREGIVER ?? 0;
                //conflict exceptions master lists
                var hHAScheduleConflictExceptions = await _hHAScheduleConflictExceptionsRepository.GetHHAScheduleConflictExceptions(HHA, UserId);

                //hha configurations
                var hHAScheduleConfiguration = await _hHAScheduleConfigurationsRepository.GetHHAScheduleConfigurations(HHA, UserId);

                //hha branch configurations
                var hHABranchScheduleConfiguration = validateScheduleRules.hHABranchScheduleConfigurationsEntity;// await _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(HHA, UserId, HHA_BRANCH_ID);

                //Get payer info
                var paymentsources = validateScheduleRules.paymentsourcesEntity;// await _paymentsourcesRepository.GetPaymentSourceAdditionalInfo(HHA, UserId, PAYMENT_SOURCE, HHA_BRANCH_ID);

                //Get client payer info
                var clientpayers = validateScheduleRules.clientPaymentSourcesEntity;// (await _clientsRepository.GetClientPaymentSourcesInfo(HHA, UserId, CLIENT_ID, PAYMENT_SOURCE)).FirstOrDefault();

                //get service info 
                var serviceCodes = validateScheduleRules.serviceCodesEntity;// await _serviceCodesRepository.GetServiceCodeAdditionalInfo(HHA, UserId, SERVICECODE_ID, CLIENT_ID);

                //get Client
                var client = validateScheduleRules.clientsEntity;

                //User Permissions
                var columns = "RolePermissionID,RoleID,RolePermissions.HHA,PermissionIdx,USER_ID";
                var Filters = _repositoryFilterConditions.EqualFilter("Users.USER_ID", UserId, FilterConditionDataTypeEnums.integerType)
                                .EqualFilter("RolePermissions.PermissionIdx", 8142, FilterConditionDataTypeEnums.integerType)
                               .Build();

                var UserPermissionList = _userRolesRepository.GetUserEnabledPermissionList(HHA, UserId, columns, Filters).Result.FirstOrDefault();

                if (UserPermissionList != null)
                {
                    AllowToEditPastVisit = true;
                }

                var caregivers = new CaregiversEntity();
                if (CAREGIVER > 0)
                {
                    caregivers = await _caregiversRepository.GetCaregiverAdditionalInfo(HHA, UserId, (int)CAREGIVER);
                }

                bool EnableClientNonPreferredClinician = true;
                bool isSERVICEMAXORMINHOURSEnabled = ((serviceCodes.MinDurationMins ?? 0) > 0 || (serviceCodes.MinDurationMins ?? 0) > 0) ? true : false;
                bool isClientMaxHoursEnabled = client.overrideClientMaxHoursPerWeek || hHABranchScheduleConfiguration.EnableClientMaxHoursPerWeek;
                bool isClinicianMaxHoursEnabled = caregivers.caregiveradditional_overrideClinicianMaxHoursPerWeek || hHABranchScheduleConfiguration.EnableClinicianMaxHoursPerWeek;
                bool isClientOnVacation = true;
                bool isClinicianOnVacation = true;
                bool isClinicianRenewableItemsExpired = true;
                bool is_clinicianDiscMaxHoursPerWeek_isHardStop = false;
                bool is_client_max_hours_softwarning = false;
                float ClientMaxHoursPerWeek = 0;


                if (validateScheduleRules.caregiverTaskEntity.ACCTUAL_END_TIME == null)
                {
                    isClientMaxHoursEnabled = false;
                    isClinicianMaxHoursEnabled = false;
                }
                

                if (isClientMaxHoursEnabled)
                {
                    if (client.overrideClientMaxHoursPerWeek)
                    {
                        is_client_max_hours_softwarning = !client.ClientMaxHoursPerWeek_isHardStop;
                        ClientMaxHoursPerWeek = client.ClientMaxHoursPerWeek;
                    }
                    else if (!hHABranchScheduleConfiguration.ClientMaxHoursPerWeek_isHardStop)
                    {
                        is_client_max_hours_softwarning = true;
                        ClientMaxHoursPerWeek = hHABranchScheduleConfiguration.ClientMaxHoursPerWeek;
                    }
                    else if(hHABranchScheduleConfiguration.ClientMaxHoursPerWeek_isHardStop)
                    {
                        is_client_max_hours_softwarning = false;
                        ClientMaxHoursPerWeek = hHABranchScheduleConfiguration.ClientMaxHoursPerWeek;
                    }
                }


                if (hHABranchScheduleConfiguration.EnableClinicianMaxHoursPerWeek)
                    is_clinicianDiscMaxHoursPerWeek_isHardStop = caregivers.CaregiverDisciplines_ClinicianMaxHoursPerWeek_isHardStop;

                //OT - overtime  validation
                bool isOVERTIMEEnabled = char.IsWhiteSpace(hHABranchScheduleConfiguration.scheduleOverTimeRule) ? false : true;

                //visit frequency validation
                bool isVisitFrequencyEnabled = hHABranchScheduleConfiguration.EnableVisitFrequency && (clientpayers != null ? clientpayers.SchedulingRule.Equals("VF") : false) ? true : false;

                //authorization
                bool isAuthorizationEnabled = clientpayers != null ? clientpayers.IS_AUTH_MAND : false; ;

                bool isGRACEPERIODEnabled = false;
                bool isNO_ATTESTATIONEnabled = false;
                bool isNO_ATTESTATIONEnabled_ForScheduleApproval = false;
                bool isNO_ACTIONCODEEnabled = false;
                bool discardDoubleBooking = false; //ClientAdditionalDetails.DiscardDoubleBook  PaymentSourcesAdditional.DiscardDoubleBooking  
                bool discardDeviation = false; //ClientAdditionalDetails.DiscardDeviation  PaymentSourcesAdditional.DiscardDoubleBooking  ServiceCodeAdditional.DiscardDeviation

                bool isEVVEnabled = false;

                if ((paymentsources.IsEnableEVV ?? false) && (serviceCodes.isEvvEnabled) && (((int)CAREGIVER == 0) || caregivers.IsEnableEVV))
                    isEVVEnabled = true;


                if (paymentsources.DiscardDoubleBooking == true)
                    discardDoubleBooking = true;
                else if (client.DiscardDoubleBook == true)
                    discardDoubleBooking = true;

                if (paymentsources.DiscardDoubleBooking == true)
                    discardDeviation = true;
                else if (client.DiscardDeviation == true)
                    discardDeviation = true;
                else if (serviceCodes.DiscardDeviation == true)
                    discardDeviation = true;
                if (isEVVEnabled)
                {
                    isNO_ACTIONCODEEnabled = paymentsources.requireEvvReasonCodesResolutionForApproval ?? false;

                    if (paymentsources.requireAttestationForScheduleApprovalForEvv == true)
                        isNO_ATTESTATIONEnabled_ForScheduleApproval = true;
                }
                else
                {
                    if (paymentsources.requireAttestationForScheduleApprovalForNonEvv == true)
                        isNO_ATTESTATIONEnabled_ForScheduleApproval = true;
                }

                bool isNO_CHECKIN_REASONCODEnabled = false;
                bool isNO_CHECKOUT_REASONCODEnabled = false;

                hHABranchScheduleConfiguration.WEEK_START_DAY = hHABranchScheduleConfiguration.WEEK_START_DAY - 1;

                foreach (var ConflictExceptions in hHAScheduleConflictExceptions)
                {
                    var ValidationRule = new ScheduleValidationRulesDto
                    {
                        ValidationType = ConflictExceptions.exception_type,
                        Validationenabled = true,
                        block_schedule_creation = false,
                        block_clinician_confirmation = false,
                        block_checkin = false,
                        block_approval = false,
                        grace_period_mins = 0,
                        discard_non_billable_schedules = false,
                        discard_third_party_clinicians = true,
                        ValidationError = ConflictExceptions.exception_message
                    };

                    switch (ConflictExceptions.exception_code)
                    {
                        case 901: //DOUBLEBOOK                         
                            ValidationRule.Validationenabled = discardDoubleBooking ? false : hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking > 0 ? true : false;
                            ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 1 ? true : false;
                            ValidationRule.block_clinician_confirmation = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 2 ? true : false;
                            ValidationRule.block_checkin = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 3 ? true : false;
                            ValidationRule.block_approval = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 4 ? true : false;
                            ValidationRule.grace_period_mins = hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes;
                            ValidationRule.discard_non_billable_schedules = hHABranchScheduleConfiguration.ignoreDoubleBookForNonBillable;
                            ValidationRule.discard_third_party_clinicians = hHABranchScheduleConfiguration.DonotValdateDoubleBookforThirdPartyClinicians;
                            ValidationRule.ValidationError = "Double book conflict validation" + (hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes > 0 ? "grace period: " + hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes + " mins" : "");

                            break;
                        case 902: //DEVIATION
                            if ((serviceCodes.DiscardDeviation ?? false) == false)
                            {
                                ValidationRule.Validationenabled = discardDeviation ? false : hHABranchScheduleConfiguration.NotAllowScheduleDeviation > 0 ? true : false;
                                ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 1 ? true : false;
                                ValidationRule.block_clinician_confirmation = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 2 ? true : false;
                                ValidationRule.block_checkin = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 3 ? true : false;
                                ValidationRule.block_approval = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 4 ? true : false;
                                ValidationRule.grace_period_mins = hHABranchScheduleConfiguration.scheduleDeviationGracePeriod;
                                ValidationRule.discard_non_billable_schedules = hHABranchScheduleConfiguration.IgnoreDeviationForNonBillable;
                                ValidationRule.ValidationError = "Deviation conflict validation" + (hHABranchScheduleConfiguration.scheduleDeviationGracePeriod > 0 ? "grace period: " + hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes + " mins" : "");

                            }
                            break;
                        case 903: //SERVICEMAXORMINHOURS
                            ValidationRule.Validationenabled = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.doNotAllowSchedulePlannedHoursOutsideServiceDurations && isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_checkin = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_approval = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.Min_Duration_Mins = serviceCodes.MinDurationMins ?? 0;
                            ValidationRule.Max_Duration_Mins = serviceCodes.MaxDurationMins ?? 0;
                            ValidationRule.ValidationError = "Service Max or Min hours validation";

                            break;
                        case 904: //CLIENTTNOTPREFERREDTHISCLINICIAN
                            ValidationRule.Validationenabled = EnableClientNonPreferredClinician;
                            ValidationRule.block_schedule_creation = EnableClientNonPreferredClinician;
                            ValidationRule.block_clinician_confirmation = EnableClientNonPreferredClinician;
                            ValidationRule.block_checkin = EnableClientNonPreferredClinician;
                            ValidationRule.block_approval = EnableClientNonPreferredClinician;
                            ValidationRule.ValidationError = "Client not preferred cliicians";

                            break;
                        case 905://CLINICIANONVACATION
                            ValidationRule.Validationenabled = isClinicianOnVacation;
                            ValidationRule.block_checkin = isClinicianOnVacation;
                            ValidationRule.block_approval = isClinicianOnVacation;
                            ValidationRule.ValidationError = "Clinician on Vacation";
                            break;
                        case 906: //CLIENTONVACATION
                            ValidationRule.Validationenabled = isClientOnVacation;
                            ValidationRule.block_checkin = isClientOnVacation;
                            ValidationRule.block_approval = isClientOnVacation;
                            ValidationRule.ValidationError = "Client on Vacation";
                            break;
                        case 907: //CLINICIANRENEWABLEITEMSEXPIRED
                            ValidationRule.Validationenabled = isClinicianRenewableItemsExpired;
                            ValidationRule.block_checkin = isClinicianRenewableItemsExpired;
                            ValidationRule.block_approval = isClinicianRenewableItemsExpired;
                            ValidationRule.ValidationError = "Clinician Renewable Item Expired";
                            break;
                        case 908://CLIENTMAXHOURS
                            ValidationRule.Validationenabled = isClientMaxHoursEnabled;
                            ValidationRule.block_checkin = isClientMaxHoursEnabled;
                            ValidationRule.block_approval = isClientMaxHoursEnabled;
                            ValidationRule.Max_Duration_Mins =ClientMaxHoursPerWeek * 60;
                            //isClientMaxHoursEnabled ? Convert.ToInt32(hHABranchScheduleConfiguration.ClientMaxHoursPerWeek * Convert.ToDouble(60)) : 0;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.ValidationError = "Client Max hours for week validation";
                            ValidationRule.Soft_Warning_Validation = is_client_max_hours_softwarning;
                            break;
                        case 909: //CLINICIANMAXHOURSOVERTIME
                            ValidationRule.Validationenabled = isClinicianMaxHoursEnabled;
                            ValidationRule.block_checkin = isClinicianMaxHoursEnabled;
                            ValidationRule.block_approval = isClinicianMaxHoursEnabled;
                            if (caregivers.caregiveradditional_overrideClinicianMaxHoursPerWeek == false)
                                ValidationRule.Max_Duration_Mins = caregivers.CaregiverDiscipline_ClinicianMaxHoursPerWeek;
                            else
                                ValidationRule.Max_Duration_Mins = 0;
                            //ValidationRule.Max_Duration_Mins = isClinicianMaxHoursEnabled ? Convert.ToInt32(caregivers.CaregiverDiscipline_ClinicianMaxHoursPerWeek * Convert.ToDouble(60)) : 0;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.ValidationError = "Clinicain Max hours for week validation exists";
                            ValidationRule.Soft_Warning_Validation = !is_clinicianDiscMaxHoursPerWeek_isHardStop;
                            break;
                        case 910: //OVERTIME
                            ValidationRule.Validationenabled = isOVERTIMEEnabled;
                            ValidationRule.block_checkin = isOVERTIMEEnabled;
                            ValidationRule.block_approval = isOVERTIMEEnabled;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.Soft_Warning_Validation = (isOVERTIMEEnabled && hHABranchScheduleConfiguration.scheduleOverTimeRule == 'W') ? true : false;
                            ValidationRule.ValidationError = "There is Overtime Validation Exists";

                            break;

                        case 911://VF
                            ValidationRule.Validationenabled = isVisitFrequencyEnabled;
                            ValidationRule.block_schedule_creation = isVisitFrequencyEnabled;
                            ValidationRule.block_clinician_confirmation = isVisitFrequencyEnabled;
                            ValidationRule.block_checkin = isVisitFrequencyEnabled;
                            ValidationRule.block_approval = isVisitFrequencyEnabled;
                            ValidationRule.discard_non_billable_schedules = true;
                            ValidationRule.ValidationError = "Visit Frequency validation Exceeds";
                            break;

                        case 912: //AUTHORIZATION
                            ValidationRule.Validationenabled = isAuthorizationEnabled;
                            ValidationRule.block_schedule_creation = paymentsources.Auth_RuleOnNonAuth == 1 ? true : false;
                            ValidationRule.block_clinician_confirmation = paymentsources.Auth_RuleOnNonAuth == 1 ? true : false;
                            ValidationRule.block_checkin = paymentsources.Auth_RuleOnNonAuth == 5 ? true : false;
                            ValidationRule.block_approval = paymentsources.Auth_RuleOnNonAuth == 2 ? true : false;
                            ValidationRule.discard_non_billable_schedules = true;
                            ValidationRule.ValidationError = "Authorization";
                            break;

                        case 913: //GRACEPERIOD
                            ValidationRule.Validationenabled = isGRACEPERIODEnabled;
                            ValidationRule.block_schedule_creation = isGRACEPERIODEnabled;
                            ValidationRule.block_clinician_confirmation = isGRACEPERIODEnabled;
                            ValidationRule.block_checkin = isGRACEPERIODEnabled;
                            ValidationRule.block_approval = isGRACEPERIODEnabled;
                            ValidationRule.ValidationError = "Grace Period exists";
                            break;
                        case 914: //NO_ATTESTATION
                            ValidationRule.Validationenabled = isEVVEnabled == true ? true:false ;
                            ValidationRule.block_schedule_creation = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_checkin = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_approval = isEVVEnabled == true ? true : false;
                            ValidationRule.ValidationError = "Attestation";
                            break;
                        case 915: //NO_CHECKIN_REASONCODE
                            ValidationRule.Validationenabled = isEVVEnabled == true ? true : false;
                            ValidationRule.block_schedule_creation = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_checkin = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_approval = isEVVEnabled == true ? true : false;
                            ValidationRule.ValidationError = "Checkin Reasoncode";
                            break;
                        case 916: //NO_CHECKOUT_REASONCODE
                            ValidationRule.Validationenabled = isEVVEnabled == true ? true : false;
                            ValidationRule.block_schedule_creation = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_checkin = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_approval = isEVVEnabled == true ? true : false;
                            ValidationRule.ValidationError = "Checkout Reasoncode";
                            break;
                        case 917: //NO_ACTIONCODE
                            ValidationRule.Validationenabled = isEVVEnabled == true ? true : false;
                            ValidationRule.block_schedule_creation = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_checkin = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_approval = isEVVEnabled == true ? true : false;
                            ValidationRule.ValidationError = "Actioncode";
                            break;

                    }
                    scheduleValidationRulesDtos.Add(ValidationRule);
                }

                var FutureDateValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "FUTUREDATE",
                    Validationenabled = true,
                    block_schedule_creation = true,
                    block_clinician_confirmation = true,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = 0,
                    ValidationError = "Cannot Checkin/out future schedules."
                };
                scheduleValidationRulesDtos.Add(FutureDateValidationRule);

                var scheduleOverValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "SCHEDULEOVER24HOURS",
                    Validationenabled = !isEVVEnabled,
                    block_schedule_creation = true,
                    block_clinician_confirmation = true,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = 0,
                    ValidationError = "Difference of Checkin checkout cannot be more than 24 hours."
                };

                scheduleValidationRulesDtos.Add(scheduleOverValidationRule);

                var geofencingValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "GEOLOCATION",
                    Validationenabled = true,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = false,
                    grace_period_mins = 0,
                    ValidationError = "Clincian is outside the set distance."
                };
                scheduleValidationRulesDtos.Add(geofencingValidationRule);

                var cdsvalidationValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "CDSVALIDATION",
                    Validationenabled = true,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = false,
                    grace_period_mins = 0,
                    ValidationError = "CDS Auth Service limit is not met"
                };

                scheduleValidationRulesDtos.Add(cdsvalidationValidationRule);


                var pastvisitValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "PASTVISIT",
                    Validationenabled = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisits && !AllowToEditPastVisit,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsDays,
                    ValidationError = "Cannot Checkin/Checkout for Past Visit",
                    ValidateForEVV = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsForEVV,
                    ValidateForNonEVV = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsForNonEVV
                };

                scheduleValidationRulesDtos.Add(pastvisitValidationRule);

            }
            catch (Exception ex)
            {
                throw;
            }
            return scheduleValidationRulesDtos;
        }

        public async Task<IEnumerable<ScheduleValidationRulesDto>> GetConflictExceptionsValidationRules_old(int HHA, int UserId, int HHA_BRANCH_ID, int CLIENT_ID, int PAYMENT_SOURCE, int SERVICECODE_ID, int? CAREGIVER)
        {
            List<ScheduleValidationRulesDto> scheduleValidationRulesDtos = new List<ScheduleValidationRulesDto>();
            bool AllowToEditPastVisit = false;
            try
            {
                //conflict exceptions master lists
                var hHAScheduleConflictExceptions = await _hHAScheduleConflictExceptionsRepository.GetHHAScheduleConflictExceptions(HHA, UserId);

                //hha configurations
                var hHAScheduleConfiguration = await _hHAScheduleConfigurationsRepository.GetHHAScheduleConfigurations(HHA, UserId);

                //hha branch configurations
                var hHABranchScheduleConfiguration = await _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(HHA, UserId, HHA_BRANCH_ID);

                //Get payer info
                var paymentsources = await _paymentsourcesRepository.GetPaymentSourceAdditionalInfo(HHA, UserId, PAYMENT_SOURCE, HHA_BRANCH_ID);

                //Get client payer info
                var clientpayers = (await _clientsRepository.GetClientPaymentSourcesInfo(HHA, UserId, CLIENT_ID, PAYMENT_SOURCE)).FirstOrDefault();

                //get service info 
                var serviceCodes = await _serviceCodesRepository.GetServiceCodeAdditionalInfo(HHA, UserId, SERVICECODE_ID, CLIENT_ID);

                //get Client
                var client = await _clientsRepository.GetClientsBasicInfo(HHA, UserId, CLIENT_ID);

                //User Permissions
                var columns = "RolePermissionID,RoleID,RolePermissions.HHA,PermissionIdx,USER_ID";
                var Filters = _repositoryFilterConditions.EqualFilter("Users.USER_ID", UserId, FilterConditionDataTypeEnums.integerType)
                                .EqualFilter("RolePermissions.PermissionIdx", 8142, FilterConditionDataTypeEnums.integerType)
                               .Build();

                var UserPermissionList = _userRolesRepository.GetUserEnabledPermissionList(HHA, UserId, columns, Filters).Result.FirstOrDefault();

                if (UserPermissionList != null)
                {
                    AllowToEditPastVisit = true;
                }

                var caregivers = new CaregiversEntity();
                if (CAREGIVER > 0)
                {
                    caregivers = await _caregiversRepository.GetCaregiverAdditionalInfo(HHA, UserId, (int)CAREGIVER);
                }

                bool EnableClientNonPreferredClinician = true;
                bool isSERVICEMAXORMINHOURSEnabled = ((serviceCodes.MinDurationMins ?? 0) > 0 || (serviceCodes.MinDurationMins ?? 0) > 0) ? true : false;
                bool isClientMaxHoursEnabled = hHABranchScheduleConfiguration.ClientMaxHoursPerWeek > 0 && hHABranchScheduleConfiguration.EnableClientMaxHoursPerWeek;
                bool isClinicianMaxHoursEnabled = caregivers.caregiveradditional_overrideClinicianMaxHoursPerWeek || hHABranchScheduleConfiguration.EnableClinicianMaxHoursPerWeek;
                bool isClientOnVacation = true;
                bool isClinicianOnVacation = true;
                bool isClinicianRenewableItemsExpired = true;


                //OT - overtime  validation
                bool isOVERTIMEEnabled = char.IsWhiteSpace(hHABranchScheduleConfiguration.scheduleOverTimeRule) ? false : true;

                //visit frequency validation
                bool isVisitFrequencyEnabled = hHABranchScheduleConfiguration.EnableVisitFrequency && (clientpayers != null ? clientpayers.SchedulingRule.Equals("VF") : false) ? true : false;

                //authorization
                bool isAuthorizationEnabled = clientpayers != null ? clientpayers.IS_AUTH_MAND : false; ;

                bool isGRACEPERIODEnabled = false;
                bool isNO_ATTESTATIONEnabled = false;
                bool isNO_ATTESTATIONEnabled_ForScheduleApproval = false;
                bool isNO_ACTIONCODEEnabled = false;
                bool discardDoubleBooking = false; //ClientAdditionalDetails.DiscardDoubleBook  PaymentSourcesAdditional.DiscardDoubleBooking  
                bool discardDeviation = false; //ClientAdditionalDetails.DiscardDeviation  PaymentSourcesAdditional.DiscardDoubleBooking  ServiceCodeAdditional.DiscardDeviation

                bool isEVVEnabled = false;

                if (paymentsources.IsEnableEVV ?? false && serviceCodes.isEvvEnabled && (((int)CAREGIVER == 0) || caregivers.IsEnableEVV))
                    isEVVEnabled = true;


                if (paymentsources.DiscardDoubleBooking == true)
                    discardDoubleBooking = true;
                else if (client.DiscardDoubleBook == true)
                {
                    discardDoubleBooking = true;
                }


                if (paymentsources.DiscardDoubleBooking == true)
                    discardDeviation = true;
                else if (client.DiscardDeviation == true)
                {
                    discardDeviation = true;
                }
                else if (serviceCodes.DiscardDeviation == true)
                {
                    discardDeviation = true;
                }


                if (isEVVEnabled)
                {
                    isNO_ACTIONCODEEnabled = paymentsources.requireEvvReasonCodesResolutionForApproval ?? false;

                    if (paymentsources.requireAttestationForScheduleApprovalForEvv == true)
                    {
                        isNO_ATTESTATIONEnabled_ForScheduleApproval = true;
                    }
                }
                else
                {
                    if (paymentsources.requireAttestationForScheduleApprovalForNonEvv == true)
                    {
                        isNO_ATTESTATIONEnabled_ForScheduleApproval = true;
                    }
                }

                bool isNO_CHECKIN_REASONCODEnabled = false;
                bool isNO_CHECKOUT_REASONCODEnabled = false;



                foreach (var ConflictExceptions in hHAScheduleConflictExceptions)
                {
                    var ValidationRule = new ScheduleValidationRulesDto
                    {
                        ValidationType = ConflictExceptions.exception_type,
                        Validationenabled = true,
                        block_schedule_creation = false,
                        block_clinician_confirmation = false,
                        block_checkin = false,
                        block_approval = false,
                        grace_period_mins = 0,
                        discard_non_billable_schedules = false,
                        discard_third_party_clinicians = true,
                        ValidationError = ConflictExceptions.exception_message
                    };

                    switch (ConflictExceptions.exception_code)
                    {
                        case 901: //DOUBLEBOOK                         
                            ValidationRule.Validationenabled = discardDoubleBooking ? false : hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking > 0 ? true : false;
                            ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 1 ? true : false;
                            ValidationRule.block_clinician_confirmation = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 2 ? true : false;
                            ValidationRule.block_checkin = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 3 ? true : false;
                            ValidationRule.block_approval = hHABranchScheduleConfiguration.NotAllowScheduleDoubleBooking == 4 ? true : false;
                            ValidationRule.grace_period_mins = hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes;
                            ValidationRule.discard_non_billable_schedules = hHABranchScheduleConfiguration.ignoreDoubleBookForNonBillable;
                            ValidationRule.discard_third_party_clinicians = hHABranchScheduleConfiguration.DonotValdateDoubleBookforThirdPartyClinicians;
                            ValidationRule.ValidationError = "Double book conflict validation" + (hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes > 0 ? "grace period: " + hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes + " mins" : "");

                            break;
                        case 902: //DEVIATION
                            if ((serviceCodes.DiscardDeviation ?? false) == false)
                            {
                                ValidationRule.Validationenabled = discardDeviation ? false : hHABranchScheduleConfiguration.NotAllowScheduleDeviation > 0 ? true : false;
                                ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 1 ? true : false;
                                ValidationRule.block_clinician_confirmation = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 2 ? true : false;
                                ValidationRule.block_checkin = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 3 ? true : false;
                                ValidationRule.block_approval = hHABranchScheduleConfiguration.NotAllowScheduleDeviation == 4 ? true : false;
                                ValidationRule.grace_period_mins = hHABranchScheduleConfiguration.scheduleDeviationGracePeriod;
                                ValidationRule.discard_non_billable_schedules = hHABranchScheduleConfiguration.IgnoreDeviationForNonBillable;
                                ValidationRule.ValidationError = "Deviation conflict validation" + (hHABranchScheduleConfiguration.scheduleDeviationGracePeriod > 0 ? "grace period: " + hHABranchScheduleConfiguration.doubleBookGracePeriodMinutes + " mins" : "");

                            }
                            break;
                        case 903: //SERVICEMAXORMINHOURS
                            ValidationRule.Validationenabled = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_schedule_creation = hHABranchScheduleConfiguration.doNotAllowSchedulePlannedHoursOutsideServiceDurations && isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_checkin = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.block_approval = isSERVICEMAXORMINHOURSEnabled;
                            ValidationRule.Min_Duration_Mins = serviceCodes.MinDurationMins ?? 0;
                            ValidationRule.Max_Duration_Mins = serviceCodes.MaxDurationMins ?? 0;
                            ValidationRule.ValidationError = "Service Max or Min hours validation";

                            break;
                        case 904: //CLIENTTNOTPREFERREDTHISCLINICIAN
                            ValidationRule.Validationenabled = EnableClientNonPreferredClinician;
                            ValidationRule.block_schedule_creation = EnableClientNonPreferredClinician;
                            ValidationRule.block_clinician_confirmation = EnableClientNonPreferredClinician;
                            ValidationRule.block_checkin = EnableClientNonPreferredClinician;
                            ValidationRule.block_approval = EnableClientNonPreferredClinician;
                            ValidationRule.ValidationError = "Client not preferred cliicians";

                            break;
                        case 905://CLINICIANONVACATION
                            ValidationRule.Validationenabled = isClinicianOnVacation;
                            ValidationRule.block_checkin = isClinicianOnVacation;
                            ValidationRule.block_approval = isClinicianOnVacation;
                            ValidationRule.ValidationError = "Clinician on Vacation";
                            break;
                        case 906: //CLIENTONVACATION
                            ValidationRule.Validationenabled = isClientOnVacation;
                            ValidationRule.block_checkin = isClientOnVacation;
                            ValidationRule.block_approval = isClientOnVacation;
                            ValidationRule.ValidationError = "Client on Vacation";
                            break;
                        case 907: //CLINICIANRENEWABLEITEMSEXPIRED
                            ValidationRule.Validationenabled = isClinicianRenewableItemsExpired;
                            ValidationRule.block_checkin = isClinicianRenewableItemsExpired;
                            ValidationRule.block_approval = isClinicianRenewableItemsExpired;
                            ValidationRule.ValidationError = "Clinicain Rebewable Item Expired";
                            break;
                        case 908://CLIENTMAXHOURS
                            ValidationRule.Validationenabled = isClientMaxHoursEnabled;
                            ValidationRule.block_checkin = isClientMaxHoursEnabled;
                            ValidationRule.block_approval = isClientMaxHoursEnabled;
                            ValidationRule.Max_Duration_Mins = isClientMaxHoursEnabled ? Convert.ToInt32(hHABranchScheduleConfiguration.ClientMaxHoursPerWeek * Convert.ToDouble(60)) : 0;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.ValidationError = "Client Max hours for week validation";

                            break;
                        case 909: //CLINICIANMAXHOURSOVERTIME
                            ValidationRule.Validationenabled = isClinicianMaxHoursEnabled;
                            ValidationRule.block_checkin = isClinicianMaxHoursEnabled;
                            ValidationRule.block_approval = isClinicianMaxHoursEnabled;
                            if (caregivers.caregiveradditional_overrideClinicianMaxHoursPerWeek == false && isClinicianMaxHoursEnabled == true)
                                ValidationRule.Max_Duration_Mins = caregivers.CaregiverDiscipline_ClinicianMaxHoursPerWeek;
                            else
                                ValidationRule.Max_Duration_Mins = 0;
                            //ValidationRule.Max_Duration_Mins = isClinicianMaxHoursEnabled ? Convert.ToInt32(caregivers.CaregiverDiscipline_ClinicianMaxHoursPerWeek * Convert.ToDouble(60)) : 0;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.ValidationError = "Clinicain Max hours for week validation exists";
                            break;
                        case 910: //OVERTIME
                            ValidationRule.Validationenabled = isOVERTIMEEnabled;
                            ValidationRule.block_checkin = isOVERTIMEEnabled;
                            ValidationRule.block_approval = isOVERTIMEEnabled;
                            ValidationRule.Min_Duration_Mins = hHABranchScheduleConfiguration.WEEK_START_DAY;
                            ValidationRule.ValidationError = "There is Overtime Validation Exists";
                            break;

                        case 911://VF
                            ValidationRule.Validationenabled = isVisitFrequencyEnabled;
                            ValidationRule.block_schedule_creation = isVisitFrequencyEnabled;
                            ValidationRule.block_clinician_confirmation = isVisitFrequencyEnabled;
                            ValidationRule.block_checkin = isVisitFrequencyEnabled;
                            ValidationRule.block_approval = isVisitFrequencyEnabled;
                            ValidationRule.discard_non_billable_schedules = true;
                            ValidationRule.ValidationError = "Visit Frequency validation Exceeds";
                            break;

                        case 912: //AUTHORIZATION
                            ValidationRule.Validationenabled = isAuthorizationEnabled;
                            ValidationRule.block_schedule_creation = paymentsources.Auth_RuleOnNonAuth == 1 ? true : false;
                            ValidationRule.block_clinician_confirmation = paymentsources.Auth_RuleOnNonAuth == 1 ? true : false;
                            ValidationRule.block_checkin = paymentsources.Auth_RuleOnNonAuth == 5 ? true : false;
                            ValidationRule.block_approval = paymentsources.Auth_RuleOnNonAuth == 2 ? true : false;
                            ValidationRule.discard_non_billable_schedules = true;
                            ValidationRule.ValidationError = "Authorization";
                            break;

                        case 913: //GRACEPERIOD
                            ValidationRule.Validationenabled = isGRACEPERIODEnabled;
                            ValidationRule.block_schedule_creation = isGRACEPERIODEnabled;
                            ValidationRule.block_clinician_confirmation = isGRACEPERIODEnabled;
                            ValidationRule.block_checkin = isGRACEPERIODEnabled;
                            ValidationRule.block_approval = isGRACEPERIODEnabled;
                            ValidationRule.ValidationError = "Graceperiod";
                            break;
                        case 914: //NO_ATTESTATION
                            ValidationRule.Validationenabled = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_schedule_creation = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_checkin = isNO_ATTESTATIONEnabled;
                            ValidationRule.block_approval = isNO_ATTESTATIONEnabled_ForScheduleApproval;
                            ValidationRule.ValidationError = "Attestation";
                            break;
                        case 915: //NO_CHECKIN_REASONCODE
                            ValidationRule.Validationenabled = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_schedule_creation = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_checkin = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.block_approval = isNO_CHECKIN_REASONCODEnabled;
                            ValidationRule.ValidationError = "Checkin Reasoncode";
                            break;
                        case 916: //NO_CHECKOUT_REASONCODE
                            ValidationRule.Validationenabled = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_schedule_creation = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_checkin = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.block_approval = isNO_CHECKOUT_REASONCODEnabled;
                            ValidationRule.ValidationError = "Checkout Reasoncode";
                            break;
                        case 917: //NO_ACTIONCODE
                            ValidationRule.Validationenabled = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_schedule_creation = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_clinician_confirmation = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_checkin = isNO_ACTIONCODEEnabled;
                            ValidationRule.block_approval = isNO_ACTIONCODEEnabled;
                            ValidationRule.ValidationError = "Actioncode";
                            break;

                    }
                    scheduleValidationRulesDtos.Add(ValidationRule);
                }

                var FutureDateValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "FUTUREDATE",
                    Validationenabled = true,
                    block_schedule_creation = true,
                    block_clinician_confirmation = true,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = 0,
                    ValidationError = "Cannot Checkin/out future schedules."
                };
                scheduleValidationRulesDtos.Add(FutureDateValidationRule);

                var scheduleOverValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "SCHEDULEOVER24HOURS",
                    Validationenabled = true,
                    block_schedule_creation = true,
                    block_clinician_confirmation = true,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = 0,
                    ValidationError = "Difference of Checkin checkout cannot be more than 24 hours."
                };

                scheduleValidationRulesDtos.Add(scheduleOverValidationRule);

                var geofencingValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "GEOLOCATION",
                    Validationenabled = true,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = false,
                    grace_period_mins = 0,
                    ValidationError = "Clincian is outside the set distance."
                };
                scheduleValidationRulesDtos.Add(geofencingValidationRule);

                var cdsvalidationValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "CDSVALIDATION",
                    Validationenabled = true,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = false,
                    grace_period_mins = 0,
                    ValidationError = "CDS Auth Service limit is not met"
                };

                scheduleValidationRulesDtos.Add(cdsvalidationValidationRule);


                var pastvisitValidationRule = new ScheduleValidationRulesDto
                {
                    ValidationType = "PASTVISIT",
                    Validationenabled = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisits && !AllowToEditPastVisit,
                    block_schedule_creation = false,
                    block_clinician_confirmation = false,
                    block_checkin = true,
                    block_approval = true,
                    grace_period_mins = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsDays,
                    ValidationError = "Cannot Checkin/Checkout for Past Visit",
                    ValidateForEVV = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsForEVV,
                    ValidateForNonEVV = hHAScheduleConfiguration.BlockCheckInCheckoutForPastVisitsForNonEVV
                };

                scheduleValidationRulesDtos.Add(pastvisitValidationRule);

            }
            catch (Exception ex)
            {
                throw;
            }
            return scheduleValidationRulesDtos;
        }
    }
}

﻿using Newtonsoft.Json;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.ValidationHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Service
{
    public class ValidationConflictExceptions : IValidationConflictExceptions
    {
        private readonly ICaregivertasksRepository _caregivertaskRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEvvExceptionsRepository;
        private readonly IScheduleProcessingQueueRepository _scheduleProcessingQueueRepository;
        private readonly IHHAConfigurationsRepository _mConfigurationsRepository;
        private readonly IConflictExceptionsValidationRules _conflictExceptionsValidationRules;
        private readonly ICExpiringItemsRepository _cExpiringItemsRepository;
        private readonly IEExpiringItemsRepository _eExpiringItemsRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IPaymentSourcesBranchesRepository _paymentSourcesBranchesRepository;
        private readonly IUAScheduleConflictExceptionsRepository _iUAScheduleConflictExceptionsRepository;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        private readonly IPaymentsourcesAdditionalRepository _paymentsourcesAdditionalRepository;
        private readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository;
        private readonly ICaregiverDisciplinesRepository _caregiverDisciplinesRepository;
        private readonly ICaregiverOtherDisciplinesRepository _caregiverOtherDisciplinesRepository;
        private readonly ICaregiverMaximumHoursEffectivePeriods _caregiverMaximumHoursEffectivePeriods;

        public ValidationConflictExceptions(ICaregivertasksRepository caregivertaskRepository,
                                            IRepositoryFilterConditions repositoryFilterConditions,
                                            IClientsRepository clientsRepository,
                                            IPaymentsourcesRepository paymentsourcesRepository,
                                            IServiceCodesRepository serviceCodesRepository,
                                            ICaregiversRepository caregiversRepository,
                                            ICaregiverTaskEVVExceptionsRepository caregiverTaskEvvExceptionsRepository,
                                            IScheduleProcessingQueueRepository scheduleProcessingQueueRepository,
                                            IHHAConfigurationsRepository mConfigurationsRepository,
                                            IConflictExceptionsValidationRules conflictExceptionsValidationRules,
                                            ICExpiringItemsRepository cExpiringItemsRepository,
                                            IEExpiringItemsRepository eExpiringItemsRepository,
                                            IUsersRepository usersRepository,
                                            IPaymentSourcesBranchesRepository paymentSourcesBranchesRepository,
                                            IUAScheduleConflictExceptionsRepository iUAScheduleConflictExceptionsRepository,
                                            IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository,
                                            IPaymentsourcesAdditionalRepository paymentsourcesAdditionalRepository,
                                            IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository,
                                            ICaregiverDisciplinesRepository caregiverDisciplinesRepository,
                                            ICaregiverOtherDisciplinesRepository caregiverOtherDisciplinesRepository,
                                            ICaregiverMaximumHoursEffectivePeriods caregiverMaximumHoursEffectivePeriods)
        {
            _caregivertaskRepository = caregivertaskRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _clientsRepository = clientsRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _caregiversRepository = caregiversRepository;
            _caregiverTaskEvvExceptionsRepository = caregiverTaskEvvExceptionsRepository;
            _scheduleProcessingQueueRepository = scheduleProcessingQueueRepository;
            _mConfigurationsRepository = mConfigurationsRepository;
            _conflictExceptionsValidationRules = conflictExceptionsValidationRules;
            _cExpiringItemsRepository = cExpiringItemsRepository;
            _eExpiringItemsRepository = eExpiringItemsRepository;
            _usersRepository = usersRepository;
            _paymentSourcesBranchesRepository = paymentSourcesBranchesRepository;
            _iUAScheduleConflictExceptionsRepository = iUAScheduleConflictExceptionsRepository;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            this._paymentsourcesAdditionalRepository = paymentsourcesAdditionalRepository;
            this._hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository;
            this._caregiverDisciplinesRepository = caregiverDisciplinesRepository;
            this._caregiverOtherDisciplinesRepository = caregiverOtherDisciplinesRepository;
            this._caregiverMaximumHoursEffectivePeriods = caregiverMaximumHoursEffectivePeriods;
        }
        public async Task<ValidationConflictexceptionsDto> GetValidationConflictexceptions(int HHA, int UserId, string ValidationConflictexceptionsfilters)
        {
            ValidationConflictexceptionsDto validationConflictexceptionsDtos = new ValidationConflictexceptionsDto();
            ValidationConflictExceptionsFilters ConflictFilter = JsonConvert.DeserializeObject<ValidationConflictExceptionsFilters>(ValidationConflictexceptionsfilters);

            //get user locations
            if (string.IsNullOrEmpty(ConflictFilter.ddl_location))
            {
                var userLocationFilter = _repositoryFilterConditions.EqualFilter("[USER]", UserId, FilterConditionDataTypeEnums.integerType).Build();
                var userLocations = await _usersRepository.GetUsersBranchList(HHA, UserId, "HHA_BRANCH_ID", userLocationFilter);
                ConflictFilter.ddl_location = String.Join(",", userLocations.Select(x => x.HHA_BRANCH_ID).Distinct());
            }
            var usersFilter = _repositoryFilterConditions.EqualFilter("[USER_ID]", UserId, FilterConditionDataTypeEnums.integerType).Build();
            var usersColumns = "LOB_ID, USER_PK, ROLE, User_Type, ROLE_TYPE, RestrictAccessToOwnRecords";
            var user = (await _usersRepository.GetHHAUsers(HHA, usersColumns, usersFilter)).FirstOrDefault();

            if ((ConflictFilter.ddl_Clientcoordinator1 == null || ConflictFilter.ddl_Clientcoordinator1 == 0) && user.ROLE_TYPE == 11 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator1 = user.USER_PK;
            }

            if ((ConflictFilter.ddl_Clientcoordinator2 == null || ConflictFilter.ddl_Clientcoordinator2 == 0) && user.ROLE_TYPE == 12 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator2 = user.USER_PK;
            }

            if ((ConflictFilter.ddl_Clientcoordinator3 == null || ConflictFilter.ddl_Clientcoordinator3 == 0) && user.ROLE_TYPE == 13 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator3 = user.USER_PK;
            }
            if ((ConflictFilter.ddl_scheduler == null || ConflictFilter.ddl_scheduler == 0) && user.ROLE_TYPE == 9)
            {
                ConflictFilter.ddl_scheduler = user.USER_PK;
            }
            if ((ConflictFilter.ddl_Supervisor == null || ConflictFilter.ddl_Supervisor == 0) && user.ROLE_TYPE == 19)
            {
                ConflictFilter.ddl_Supervisor = user.USER_PK;
            }
            //EVV Vendor

            var scheduleallFilter =
                _repositoryFilterConditions.BetweenFilter("PLANNED_DATE", ConflictFilter.StartDate, ConflictFilter.EndDate, FilterConditionDataTypeEnums.datetimeType)
                                            .InFilter("PAYMENT_SOURCE", ConflictFilter.ddl_payer, FilterConditionDataTypeEnums.integerType)
                                            .EqualFilter("CAREGIVER", ConflictFilter.ddl_clinician, FilterConditionDataTypeEnums.integerType)
                                            .EqualFilter("CLIENT_ID", ConflictFilter.ddl_client, FilterConditionDataTypeEnums.integerType)
                                            .InFilter("[STATUS]", ConflictFilter.ddl_schedulestatus, FilterConditionDataTypeEnums.stringType)
                                            .EqualFilter("IS_BILLED", ConflictFilter.ddl_billed, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("IS_PAID", ConflictFilter.ddl_Payrolled, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("CanBill", ConflictFilter.ddl_Billable, FilterConditionDataTypeEnums.boolType)
                                            .EqualFilter("CaregiverTaskAdditional.isEvvschedule", ConflictFilter.ddl_evvschedule, FilterConditionDataTypeEnums.boolType)
                                            .EqualFilter("IS_PAYABLE", ConflictFilter.ddl_Payable, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("CaregiverTaskAdditional.isEvvAggregatorExportRequired", ConflictFilter.ddl_evvexportrequires, FilterConditionDataTypeEnums.boolType)
                                            .Build();

            if (!string.IsNullOrEmpty(ConflictFilter.ddl_echartstatus))
            {
                var echartStatusFilter = "";
                
                var echartStatus = ConflictFilter.ddl_echartstatus.Split(',')
                                                .Select(x => x.Trim())
                                                .Where(x => !string.IsNullOrWhiteSpace(x));

                if(echartStatus.Where(e => e == "Open").Any())
                {
                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=0 AND ISNULL(CaregiverTasks.isChartApproved,0)=0 AND ISNULL(CaregiverTasks.isChartSendForCorrection,0)=0) ";
                }

                if (echartStatus.Where(e => e == "Submitted").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=1 AND ISNULL(CaregiverTasks.isChartApproved,0)=0) ";
                }

                if (echartStatus.Where(e => e == "Sent for Correction").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=0 AND ISNULL(CaregiverTasks.isChartApproved,0)=0 AND ISNULL(CaregiverTasks.isChartSendForCorrection,0)=1) ";
                }

                if (echartStatus.Where(e => e == "Approved").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartApproved,0)=1) ";
                }

                if (!string.IsNullOrEmpty(echartStatusFilter))
                    scheduleallFilter += " AND (" + echartStatusFilter + ")";

            }

            var schedulesColumns = "CGTASK_ID,CLIENT_ID,CAREGIVER,PAYMENT_SOURCE,SERVICECODE_ID,PLANNED_DATE,STATUS,PLANNED_START_TIME,PLANNED_END_TIME,ACCTUAL_START_TIME,ACCTUAL_END_TIME,EDITED_HOURS,CaregiverTaskAdditional.isEvvschedule";

            var Schedules = await _caregivertaskRepository.GetAllSchedulesList(HHA, UserId, schedulesColumns, scheduleallFilter);

            var distincetClientID = string.Empty;

            if (ConflictFilter.ddl_client == 0 || ConflictFilter.ddl_client == null)
                distincetClientID = String.Join(",", Schedules.Select(x => x.CLIENT_ID).Distinct());
            
            var clientFilter =
                    _repositoryFilterConditions.EqualFilter("CASE_MANAGER", ConflictFilter.ddl_casemanager, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("CLIENT_ID", ConflictFilter.ddl_client, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("LOB_ID", ConflictFilter.ddl_LOB, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CLIENT_ID", distincetClientID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("Coordinator1", ConflictFilter.ddl_Clientcoordinator1, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("Coordinator2", ConflictFilter.ddl_Clientcoordinator2, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("Coordinator3", ConflictFilter.ddl_Clientcoordinator3, FilterConditionDataTypeEnums.integerType, true)
                                                .InFilter("TeamMasterID", ConflictFilter.ddl_Team, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("ClientAdditionalDetails.Scheduler", ConflictFilter.ddl_scheduler, FilterConditionDataTypeEnums.integerType, true)
                                                .Build();
            var clientColumns = "CLIENT_ID,FIRST_NAME,LAST_NAME,PATIENT_ID,AdmitNo,CASE_MANAGER,LOB_ID";

            var Clients = await _clientsRepository.GetALLClients(HHA, UserId, clientColumns, clientFilter);


            if (string.IsNullOrEmpty(ConflictFilter.ddl_payer))
                ConflictFilter.ddl_payer = String.Join(",", Schedules.Select(x => x.PAYMENT_SOURCE).Distinct());

            var payerFilter =
                    _repositoryFilterConditions.InFilter("PAYMENT_SOURCE_ID", ConflictFilter.ddl_payer, FilterConditionDataTypeEnums.integerType)
                                                .Build();

            var payerColumns = "PAYMENT_SOURCE_ID, ORG_NAME, IsEnableEVV, EvvAggregatorVendorVersionMasterID";

            var Payers = await _paymentsourcesRepository.GetPaymentsources(HHA, UserId, payerColumns, payerFilter);

            var distincetServiceID = String.Join(",", Schedules.Select(x => x.SERVICECODE_ID).Distinct());

            var serviceFilter =
                    _repositoryFilterConditions.InFilter("GroupID", ConflictFilter.ddl_discipline, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("SERVICE_CODE_ID", distincetServiceID, FilterConditionDataTypeEnums.integerType)
                                                .Build();

            var serviceColumns = "SERVICE_CODE_ID, DESCRIPTION, ServiceCodes.GroupID, isEvvEnabled";
            var Services = await _serviceCodesRepository.GetServiceCodes(HHA, UserId, serviceColumns, serviceFilter);

            var distincetCaregiverID = string.Empty;

            if (ConflictFilter.ddl_clinician == 0 || ConflictFilter.ddl_clinician == null)
                distincetCaregiverID = String.Join(",", Schedules.Select(x => x.CAREGIVER).Where(x => x != null).Distinct());

            var caregiverFilter =
                    _repositoryFilterConditions.EqualFilter("CAREGIVER_ID", ConflictFilter.ddl_clinician, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CAREGIVER_ID", distincetCaregiverID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("Caregivers.supervisingClinician", ConflictFilter.ddl_Supervisor, FilterConditionDataTypeEnums.integerType, true)
                                                .Build();

            var caregiverColumns = "CAREGIVER_ID, FIRST_NAME, LAST_NAME, CLINICIAN_ID";

            var Clinician = await _caregiversRepository.GetCaregivers(HHA, UserId, caregiverColumns, caregiverFilter);

            var caregiverTaskEVVExceptionsFilter = string.Empty;


            var ExceptionsCGtaskIDs = string.Empty;

            if (!string.IsNullOrEmpty(ConflictFilter.Exceptions))
            {
                
                caregiverTaskEVVExceptionsFilter = _repositoryFilterConditions.EqualFilter("isResolved", ConflictFilter.isResolved, FilterConditionDataTypeEnums.boolType)
                                                .InFilter("CgTaskID", ExceptionsCGtaskIDs, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("ExceptionCode", ConflictFilter.Exceptions, FilterConditionDataTypeEnums.integerType)
                                                .Build();
                ExceptionsCGtaskIDs = String.Join(",", (await _caregiverTaskEvvExceptionsRepository.GetCaregiverTaskEVVExceptions(HHA, UserId, "CgTaskID", caregiverTaskEVVExceptionsFilter)).Select(x => x.CgTaskID).Distinct());
                caregiverTaskEVVExceptionsFilter = string.Empty;
                if (string.IsNullOrEmpty(ExceptionsCGtaskIDs))
                {
                    ExceptionsCGtaskIDs = "0";
                }
                
            }
            else
            {
                ExceptionsCGtaskIDs = string.Join(",", Schedules.Select(x => x.CGTASK_ID).Distinct());
            }

            caregiverTaskEVVExceptionsFilter = _repositoryFilterConditions.EqualFilter("isResolved", ConflictFilter.isResolved, FilterConditionDataTypeEnums.boolType)
                                                    .InFilter("CgTaskID", ExceptionsCGtaskIDs, FilterConditionDataTypeEnums.integerType)
                                                    .Build();

            var caregiverTaskEVVExceptionsColumns = "CaregiverTaskEvvExceptionID,isResolved,ExceptionCode,ExceptionReason,CgTaskID,Resolution,Resolution_comments";

            var caregiverTaskEvvExceptions = await _caregiverTaskEvvExceptionsRepository.GetCaregiverTaskEVVExceptions(HHA, UserId, caregiverTaskEVVExceptionsColumns, caregiverTaskEVVExceptionsFilter);
            var scheduleConflictExceptions = await _iUAScheduleConflictExceptionsRepository.GetScheduleConflictExceptions(HHA, UserId);

            if (!string.IsNullOrEmpty(ConflictFilter.ddl_evvvendor))
            {
                var evvVendors = ConflictFilter.ddl_evvvendor.Split(',')
                                                .Select(x => x.Trim())
                                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                                .Select(e => Convert.ToInt32(e));


                var paymentSourcesBranches = await _paymentSourcesBranchesRepository.GetHHAPayerBranches(HHA, UserId, Payers.Select(e => e.PAYMENT_SOURCE_ID).Distinct());

                Schedules = (from schedule in Schedules
                                    from payer in Payers.Where(e => schedule.PAYMENT_SOURCE == e.PAYMENT_SOURCE_ID && e.IsEnableEVV == true)
                                    from service in Services.Where(e => schedule.SERVICECODE_ID == e.SERVICE_CODE_ID && e.isEvvEnabled == true)
                                    from client in Clients.Where(e => schedule.CLIENT_ID == e.CLIENT_ID)
                                    from payerBranch in paymentSourcesBranches.Where(e => e.PaymentSource_ID == payer.PAYMENT_SOURCE_ID && client.HHA_BRANCH_ID == e.Branch_ID).DefaultIfEmpty(new PaymentSourcesBranchesEntity { })
                                    from evvVendor in evvVendors.Where(e => e == (payerBranch.EvvAggregatorVendorVersionMasterID == 0 ? payer.EvvAggregatorVendorVersionMasterID: payerBranch.EvvAggregatorVendorVersionMasterID))
                                    select schedule);
            }

            var conflictReportScheduleList = (from schedule in Schedules
                                              from client in Clients.Where(e => schedule.CLIENT_ID == e.CLIENT_ID)
                                              from payer in Payers.Where(e => schedule.PAYMENT_SOURCE == e.PAYMENT_SOURCE_ID)
                                              from service in Services.Where(e => schedule.SERVICECODE_ID == e.SERVICE_CODE_ID)
                                              from caregiver in Clinician.Where(e => schedule.CAREGIVER == e.CAREGIVER_ID)
                                              select new ConflictReportScheduleList
                                              {
                                                  CGTASKID = schedule.CGTASK_ID,
                                                  ClientID = schedule.CLIENT_ID,
                                                  ClinicianID = schedule.CAREGIVER,
                                                  PayerID = schedule.PAYMENT_SOURCE,
                                                  ServiceID = schedule.SERVICECODE_ID,
                                                  Client = client.LAST_NAME + ", " + client.FIRST_NAME + "(" + client.PATIENT_ID + ")",
                                                  Payer = payer.ORG_NAME,
                                                  Service = service.DESCRIPTION,
                                                  ScheduleDate = schedule.PLANNED_DATE,
                                                  ScheduleStatus = schedule.STATUS,
                                                  Clinician = caregiver.LAST_NAME + ", " + caregiver.FIRST_NAME + "(" + caregiver.CLINICIAN_ID + ")",
                                                  PlannedStart = schedule.PLANNED_START_TIME,
                                                  PlannedEnd = schedule.PLANNED_END_TIME,
                                                  PlannedHours = "",
                                                  ActualStart = schedule.ACCTUAL_START_TIME,
                                                  AcrualEnd = schedule.ACCTUAL_END_TIME,
                                                  ActualHours = schedule.EDITED_HOURS,
                                                  AdmitNo = client.AdmitNo,
                                                  isEvvSchedule = schedule.isEvvschedule,
                                                  Exceptions = (from exception in caregiverTaskEvvExceptions.Where(e => schedule.CGTASK_ID == e.CgTaskID)
                                                                from sconf in scheduleConflictExceptions.Where(e => Convert.ToString(e.exception_code) == exception.ExceptionCode).DefaultIfEmpty(new _UA_ScheduleConflictExceptionsEntity { })
                                                                select new CaregiverTaskEvvException
                                                                {
                                                                    CaregiverTaskEvvExceptionID = exception.CaregiverTaskEvvExceptionID,
                                                                    isResolved = exception.isResolved,
                                                                    ExceptionCode = exception.ExceptionCode,
                                                                    ExceptionReason = exception.ExceptionReason,
                                                                    Resolution = exception.Resolution,
                                                                    Resolution_comments = exception.Resolution_comments,
                                                                    can_resolve = sconf.can_resolve == true ? true : false
                                                                })
                                              }).Where(e => e.Exceptions.Any());

            //order by
            if (ConflictFilter.Sort_By == 3)
            {
                if (ConflictFilter.Sort_Order)
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.PlannedStart ascending, s.Client, s.ScheduleDate
                                                 select s;
                else
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.PlannedStart descending, s.Client, s.ScheduleDate
                                                 select s;
            }
            else if (ConflictFilter.Sort_By == 2)
            {
                if (ConflictFilter.Sort_Order)
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.ScheduleDate ascending, s.Client, s.PlannedStart
                                                 select s;
                else
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.ScheduleDate descending, s.Client, s.PlannedStart
                                                 select s;
            }
            else
            {
                if (ConflictFilter.Sort_Order)
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.Client ascending, s.ScheduleDate, s.PlannedStart
                                                 select s;
                else
                    conflictReportScheduleList = from s in conflictReportScheduleList
                                                 orderby s.Client descending, s.ScheduleDate, s.PlannedStart
                                                 select s;
            }

            //page records
            var TotalRecord = conflictReportScheduleList.Count();
            var TotalPages = Convert.ToInt32(TotalRecord / 100) + (TotalRecord % 100 > 0 ? 1 : 0);
            if (ConflictFilter.PageNo > TotalPages || ConflictFilter.PageNo == 0)
                ConflictFilter.PageNo = 1;

            var indexStart = ConflictFilter.PageNo * 100 - 100;
            var indexEnd = indexStart+99;
            if (indexEnd > TotalRecord)
                indexEnd = TotalRecord - 1;

            conflictReportScheduleList = conflictReportScheduleList.Select((s, i) => new { schedules = s, index = i })
                                                                  .Where(e => e.index >= indexStart && e.index <= indexEnd).Select(e => e.schedules);

            validationConflictexceptionsDtos.TotalRecord = TotalRecord;
            validationConflictexceptionsDtos.PageNo = ConflictFilter.PageNo;
            validationConflictexceptionsDtos.RecordPerPage = 100;
            validationConflictexceptionsDtos.ScheduleList = conflictReportScheduleList;

            return validationConflictexceptionsDtos;

        }

        public async Task<IEnumerable<RecalculateScheduleExceptionsinitDto>> GetValidationConflictRecalculate_init(int HHA, int UserId, string ValidationConflictexceptionsfilters)
        {
            List<RecalculateScheduleExceptionsinitDto> recalculateScheduleExceptionsinitDto = new List<RecalculateScheduleExceptionsinitDto>();
            ValidationConflictExceptionsFilters ConflictFilter = JsonConvert.DeserializeObject<ValidationConflictExceptionsFilters>(ValidationConflictexceptionsfilters);
            bool ret = false;

            var mConfigurations = await _mConfigurationsRepository.Get_M_Configurations(HHA, UserId, "schedule_conflict_revalidation_min_interval_mins", "");
            mConfigurations.schedule_conflict_revalidation_min_interval_mins = mConfigurations.schedule_conflict_revalidation_min_interval_mins == 0 ? 5 : mConfigurations.schedule_conflict_revalidation_min_interval_mins;

            var LastConflictValiDatedOn = DateTime.Now.AddMinutes(-mConfigurations.schedule_conflict_revalidation_min_interval_mins).ToString("MM/dd/yyyy HH:mm:ss");
            //get user locations
            if (string.IsNullOrEmpty(ConflictFilter.ddl_location))
            {
                var userLocationFilter = _repositoryFilterConditions.EqualFilter("[USER]", UserId, FilterConditionDataTypeEnums.integerType).Build();
                var userLocations = await _usersRepository.GetUsersBranchList(HHA, UserId, "HHA_BRANCH_ID", userLocationFilter);
                ConflictFilter.ddl_location = String.Join(",", userLocations.Select(x => x.HHA_BRANCH_ID).Distinct());
            }
            var usersFilter = _repositoryFilterConditions.EqualFilter("[USER_ID]", UserId, FilterConditionDataTypeEnums.integerType).Build();
            var usersColumns = "LOB_ID, USER_PK, ROLE, User_Type, ROLE_TYPE, RestrictAccessToOwnRecords";
            var user = (await _usersRepository.GetHHAUsers(HHA, usersColumns, usersFilter)).FirstOrDefault();

            if ((ConflictFilter.ddl_Clientcoordinator1 == null || ConflictFilter.ddl_Clientcoordinator1 == 0) && user.ROLE_TYPE == 11 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator1 = user.USER_PK;
            }

            if ((ConflictFilter.ddl_Clientcoordinator2 == null || ConflictFilter.ddl_Clientcoordinator2 == 0) && user.ROLE_TYPE == 12 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator2 = user.USER_PK;
            }

            if ((ConflictFilter.ddl_Clientcoordinator3 == null || ConflictFilter.ddl_Clientcoordinator3 == 0) && user.ROLE_TYPE == 13 && user.RestrictAccessToOwnRecords == true)
            {
                ConflictFilter.ddl_Clientcoordinator3 = user.USER_PK;
            }
            if ((ConflictFilter.ddl_scheduler == null || ConflictFilter.ddl_scheduler == 0) && user.ROLE_TYPE == 9)
            {
                ConflictFilter.ddl_scheduler = user.USER_PK;
            }
            if ((ConflictFilter.ddl_Supervisor == null || ConflictFilter.ddl_Supervisor == 0) && user.ROLE_TYPE == 19)
            {
                ConflictFilter.ddl_Supervisor = user.USER_PK;
            }
            //EVV Vendor

            var scheduleallFilter =
                _repositoryFilterConditions.BetweenFilter("PLANNED_DATE", ConflictFilter.StartDate, ConflictFilter.EndDate, FilterConditionDataTypeEnums.datetimeType)
                                            .InFilter("PAYMENT_SOURCE", ConflictFilter.ddl_payer, FilterConditionDataTypeEnums.integerType)
                                            .EqualFilter("CAREGIVER", ConflictFilter.ddl_clinician, FilterConditionDataTypeEnums.integerType)
                                            .EqualFilter("CLIENT_ID", ConflictFilter.ddl_client, FilterConditionDataTypeEnums.integerType)
                                            .InFilter("[STATUS]", ConflictFilter.ddl_schedulestatus, FilterConditionDataTypeEnums.stringType)
                                            .EqualFilter("IS_BILLED", ConflictFilter.ddl_billed, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("IS_PAID", ConflictFilter.ddl_Payrolled, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("CanBill", ConflictFilter.ddl_Billable, FilterConditionDataTypeEnums.boolType)
                                            .EqualFilter("CaregiverTaskAdditional.isEvvschedule", ConflictFilter.ddl_evvschedule, FilterConditionDataTypeEnums.boolType)
                                            .EqualFilter("IS_PAYABLE", ConflictFilter.ddl_Payable, FilterConditionDataTypeEnums.boolType, true)
                                            .EqualFilter("CaregiverTaskAdditional.isEvvAggregatorExportRequired", ConflictFilter.ddl_evvexportrequires, FilterConditionDataTypeEnums.boolType)
                                            .LessThanFilter("LAST_CONFLICT_VALIDATED_ON", LastConflictValiDatedOn, FilterConditionDataTypeEnums.datetimeType, true)
                                            .Build();

            if (!string.IsNullOrEmpty(ConflictFilter.ddl_echartstatus))
            {
                var echartStatusFilter = "";

                var echartStatus = ConflictFilter.ddl_echartstatus.Split(',')
                                                .Select(x => x.Trim())
                                                .Where(x => !string.IsNullOrWhiteSpace(x));

                if (echartStatus.Where(e => e == "Open").Any())
                {
                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=0 AND ISNULL(CaregiverTasks.isChartApproved,0)=0 AND ISNULL(CaregiverTasks.isChartSendForCorrection,0)=0) ";
                }

                if (echartStatus.Where(e => e == "Submitted").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=1 AND ISNULL(CaregiverTasks.isChartApproved,0)=0) ";
                }

                if (echartStatus.Where(e => e == "Sent for Correction").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartSubmitted,0)=0 AND ISNULL(CaregiverTasks.isChartApproved,0)=0 AND ISNULL(CaregiverTasks.isChartSendForCorrection,0)=1) ";
                }

                if (echartStatus.Where(e => e == "Approved").Any())
                {
                    if (!string.IsNullOrEmpty(echartStatusFilter))
                        echartStatusFilter += " OR ";

                    echartStatusFilter += " (ISNULL(CaregiverTasks.isChartApproved,0)=1) ";
                }

                if (!string.IsNullOrEmpty(echartStatusFilter))
                    scheduleallFilter += " AND (" + echartStatusFilter + ")";

            }

            var schedulesColumns = "CGTASK_ID,CLIENT_ID,CAREGIVER,PAYMENT_SOURCE,SERVICECODE_ID";

            var Schedules = await _caregivertaskRepository.GetAllSchedulesList(HHA, UserId, schedulesColumns, scheduleallFilter);

            var distincetClientID = string.Empty;

            if (ConflictFilter.ddl_client == 0 || ConflictFilter.ddl_client == null)
                distincetClientID = String.Join(",", Schedules.Select(x => x.CLIENT_ID).Distinct());

            var clientFilter =
                    _repositoryFilterConditions.EqualFilter("CASE_MANAGER", ConflictFilter.ddl_casemanager, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("CLIENT_ID", ConflictFilter.ddl_client, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("LOB_ID", ConflictFilter.ddl_LOB, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CLIENT_ID", distincetClientID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("Coordinator1", ConflictFilter.ddl_Clientcoordinator1, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("Coordinator2", ConflictFilter.ddl_Clientcoordinator2, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("Coordinator3", ConflictFilter.ddl_Clientcoordinator3, FilterConditionDataTypeEnums.integerType, true)
                                                .InFilter("TeamMasterID", ConflictFilter.ddl_Team, FilterConditionDataTypeEnums.integerType, true)
                                                .EqualFilter("ClientAdditionalDetails.Scheduler", ConflictFilter.ddl_scheduler, FilterConditionDataTypeEnums.integerType, true)
                                                .Build();

            var clientColumns = "CLIENT_ID,CASE_MANAGER,LOB_ID,HHA_BRANCH_ID";

            var Clients = await _clientsRepository.GetALLClients(HHA, UserId, clientColumns, clientFilter);

            var distincetServiceID = String.Join(",", Schedules.Select(x => x.SERVICECODE_ID).Distinct());

            var serviceFilter =
                    _repositoryFilterConditions.InFilter("GroupID", ConflictFilter.ddl_discipline, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("SERVICE_CODE_ID", distincetServiceID, FilterConditionDataTypeEnums.integerType)
                                                .Build();

            var serviceColumns = "SERVICE_CODE_ID, GroupID, isEvvEnabled";
            var Services = await _serviceCodesRepository.GetServiceCodes(HHA, UserId, serviceColumns, serviceFilter);

            var distincetCaregiverID = string.Empty;

            if (ConflictFilter.ddl_clinician == 0 || ConflictFilter.ddl_clinician == null)
                distincetCaregiverID = String.Join(",", Schedules.Select(x => x.CAREGIVER).Where(x => x != null).Distinct());

            var caregiverFilter =
                    _repositoryFilterConditions.EqualFilter("CAREGIVER_ID", ConflictFilter.ddl_clinician, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CAREGIVER_ID", distincetCaregiverID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("Caregivers.supervisingClinician", ConflictFilter.ddl_Supervisor, FilterConditionDataTypeEnums.integerType, true)
                                                .Build();

            var caregiverColumns = "CAREGIVER_ID";

            var Clinician = await _caregiversRepository.GetCaregivers(HHA, UserId, caregiverColumns, caregiverFilter);

            if (!string.IsNullOrEmpty(ConflictFilter.ddl_evvvendor))
            {
                var evvVendors = ConflictFilter.ddl_evvvendor.Split(',')
                                                .Select(x => x.Trim())
                                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                                .Select(e => Convert.ToInt32(e));

                if (string.IsNullOrEmpty(ConflictFilter.ddl_payer))
                    ConflictFilter.ddl_payer = String.Join(",", Schedules.Select(x => x.PAYMENT_SOURCE).Distinct());

                var payerFilter =
                        _repositoryFilterConditions.InFilter("PAYMENT_SOURCE_ID", ConflictFilter.ddl_payer, FilterConditionDataTypeEnums.integerType)
                                                    .Build();

                var payerColumns = "PAYMENT_SOURCE_ID, IsEnableEVV, EvvAggregatorVendorVersionMasterID";

                var Payers = await _paymentsourcesRepository.GetPaymentsources(HHA, UserId, payerColumns, payerFilter);

                var paymentSourcesBranches = await _paymentSourcesBranchesRepository.GetHHAPayerBranches(HHA, UserId, Payers.Select(e => e.PAYMENT_SOURCE_ID).Distinct());

                Schedules = (from schedule in Schedules
                             from payer in Payers.Where(e => schedule.PAYMENT_SOURCE == e.PAYMENT_SOURCE_ID && e.IsEnableEVV == true)
                             from service in Services.Where(e => schedule.SERVICECODE_ID == e.SERVICE_CODE_ID && e.isEvvEnabled == true)
                             from client in Clients.Where(e => schedule.CLIENT_ID == e.CLIENT_ID)
                             from payerBranch in paymentSourcesBranches.Where(e => e.PaymentSource_ID == payer.PAYMENT_SOURCE_ID && client.HHA_BRANCH_ID == e.Branch_ID).DefaultIfEmpty(new PaymentSourcesBranchesEntity { })
                             from evvVendor in evvVendors.Where(e => e == (payerBranch.EvvAggregatorVendorVersionMasterID == 0 ? payer.EvvAggregatorVendorVersionMasterID : payerBranch.EvvAggregatorVendorVersionMasterID))
                             select schedule);
            }

            Schedules = (from schedule in Schedules
                         from client in Clients.Where(e => schedule.CLIENT_ID == e.CLIENT_ID)
                         from service in Services.Where(e => schedule.SERVICECODE_ID == e.SERVICE_CODE_ID)
                         from caregiver in Clinician.Where(e => schedule.CAREGIVER == e.CAREGIVER_ID)
                         select schedule);

            var results = Schedules.GroupBy(c => new
            {
                c.CLIENT_ID,
                c.PAYMENT_SOURCE,
                c.SERVICECODE_ID,
                c.CAREGIVER
            })
            .Select(gcs => new
            {
                ClientID = gcs.Key.CLIENT_ID,
                PayerID = gcs.Key.PAYMENT_SOURCE,
                ServiceID = gcs.Key.SERVICECODE_ID,
                CaregiverID = gcs.Key.CAREGIVER,
                Scheduleids = String.Join(",", gcs.Select(e => e.CGTASK_ID)),
                totalSchedule = gcs.Count(),
                Batchid = Guid.NewGuid()
            });

            foreach (var item in results)
            {
                var HHABranchID = Clients.Where(e => e.CLIENT_ID == item.ClientID).FirstOrDefault().HHA_BRANCH_ID;

                var scheduleValidationRulesDtos = await _conflictExceptionsValidationRules.GetConflictExceptionsValidationRules_old(HHA, UserId, HHABranchID, item.ClientID, item.PayerID, item.ServiceID, item.CaregiverID);
                var ValidationRulesJson = JsonConvert.SerializeObject(scheduleValidationRulesDtos, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                recalculateScheduleExceptionsinitDto.Add(new RecalculateScheduleExceptionsinitDto { batch_guid = item.Batchid, total_records = item.totalSchedule });
                ret = await _scheduleProcessingQueueRepository.InsertScheduleProcessingQueue(HHA, UserId, item.Batchid, "CONFLICT_VALIDATION", ValidationRulesJson, item.Scheduleids);
            }

            return recalculateScheduleExceptionsinitDto;
        }

        public async Task<RecalculateScheduleExceptionsBatchDto> GetValidationConflictRecalculate_Batch(int HHA, int UserId, string batch_id)
        {
            RecalculateScheduleExceptionsBatchDto recalculateScheduleExceptionsBatchDto = new RecalculateScheduleExceptionsBatchDto();
            var ProcessingQueueFilters =
                    _repositoryFilterConditions.EqualFilter("batch_guid", batch_id, FilterConditionDataTypeEnums.stringType)
                                                        .Build();
            var ProcessingQueueColumns = "cgtask_id,batch_guid,validation_rule_json";
            var scheduleProcessingQueue = await _scheduleProcessingQueueRepository.GetScheduleProcessingQueue(HHA, UserId, ProcessingQueueColumns, ProcessingQueueFilters);

            foreach (ScheduleProcessingQueueEntity schedule in scheduleProcessingQueue)
            {
                var validationrule = schedule.validation_rule_json;
                var scheduleValidationRulesDtos = JsonConvert.DeserializeObject<IEnumerable<ScheduleValidationRulesDto>>(validationrule);

                var caregivertaskColumns = " CGTASK_ID, PLANNED_DATE, PLANNED_START_TIME, PLANNED_END_TIME, ACCTUAL_START_TIME, ACCTUAL_END_TIME, CLIENT_ID, CAREGIVER";
                var caregivertaskFilters =
                    _repositoryFilterConditions.EqualFilter("CGTASK_ID", schedule.cgtask_id, FilterConditionDataTypeEnums.integerType)
                                                        .Build();

                var caregivertask = (await _caregivertaskRepository.GetScheduleBasicList(HHA, UserId, caregivertaskColumns, caregivertaskFilters)).FirstOrDefault();
                ValidateScheduleRules validationCreateSchedule = new ValidateScheduleRules();
                validationCreateSchedule.caregiverTaskEntity = caregivertask;
                //DOUBLEBOOK
                var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();
                var DoublBookvalidationHandler = new DoubleBookValidationHandler(doublebookvalidationRule, HHA, UserId, _caregivertaskRepository, _repositoryFilterConditions, _caregiversRepository);

                //DEVIATION
                var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
                var DeviationvalidationHandler = new DeviationValidationHandler(DeviationvalidationRule, HHA, UserId, _caregivertaskRepository, _repositoryFilterConditions, _hHAScheduleAuthSettingsRepository, _hHABranchScheduleConfigurationsRepository);
                DoublBookvalidationHandler.SetNextValidation(DeviationvalidationHandler);

                //SERVICEMAXORMINHOURS
                var serviceMaxvalidationnRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SERVICEMAXORMINHOURS").FirstOrDefault();
                var serviceMaxvalidationHandler = new ServiceMaxOrMinHoursValidationHandler(serviceMaxvalidationnRule);
                serviceMaxvalidationHandler.SetNextValidation(serviceMaxvalidationHandler);

                //CLIENTTNOTPREFERREDTHISCLINICIAN
                var clientNotPreferredThisClinicianRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTTNOTPREFERREDTHISCLINICIAN").FirstOrDefault();
                var clientNotPreferredThisClinicianHandler = new ClientNotPreferredThisClinicianValidationHandler(clientNotPreferredThisClinicianRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
                clientNotPreferredThisClinicianHandler.SetNextValidation(clientNotPreferredThisClinicianHandler);

                //CLINICIANONVACATION
                var clinicianOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANONVACATION").FirstOrDefault();
                var clinicianOnVacationHandler = new ClinicianOnVacationValidationHandler(clinicianOnVacationRule, HHA, UserId, _caregiversRepository, _repositoryFilterConditions);
                clinicianOnVacationHandler.SetNextValidation(clinicianOnVacationHandler);

                //CLIENTONVACATION
                var clientOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTONVACATION").FirstOrDefault();
                var clientOnVacationHandler = new ClientOnVacationValidationHandler(clientOnVacationRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
                clientOnVacationHandler.SetNextValidation(clientOnVacationHandler);

                //CLINICIANRENEWABLEITEMSEXPIRED
                var clinicianRenewableItemsExpiredRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANRENEWABLEITEMSEXPIRED").FirstOrDefault();
                var clinicianRenewableItemsExpiredHandler = new ClinicianRenewableItemsExpiredValidationHandler(clinicianRenewableItemsExpiredRule, HHA, UserId, _cExpiringItemsRepository, _eExpiringItemsRepository, _repositoryFilterConditions);
                clinicianRenewableItemsExpiredHandler.SetNextValidation(clinicianRenewableItemsExpiredHandler);

                //CLIENTMAXHOURS
                var clientMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();
                var clientMaxHoursHandler = new ClientMaxHoursValidationHandler(clientMaxHoursRule, HHA, UserId, _caregivertaskRepository, _repositoryFilterConditions);
                clientMaxHoursHandler.SetNextValidation(clientMaxHoursHandler);

                //CLINICIANMAXHOURS
                var clinicianMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();
                var clinicianMaxHoursHandler = new ClinicianMaxHoursValidationHandler(clinicianMaxHoursRule, HHA, UserId, _caregivertaskRepository, _repositoryFilterConditions, _caregiverMaximumHoursEffectivePeriods);
                clinicianMaxHoursHandler.SetNextValidation(clinicianMaxHoursHandler);

                //OVERTIME
                var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();
                var overtimeHandler = new OvertimeValidationHandler(overtimeRule);
                overtimeHandler.SetNextValidation(overtimeHandler);

                //GRACEPERIOD

                //VF
                var visitFrequencyRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "VF").FirstOrDefault();
                var visitFrequencyHandler = new VisitFrequencyValidationHandler(visitFrequencyRule, HHA, UserId, _caregivertaskRepository);
                visitFrequencyHandler.SetNextValidation(visitFrequencyHandler);

                //AUTHORIZATION
                var authorizationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "AUTHORIZATION").FirstOrDefault();
                var authorizationHandler = new AuthorizationValidationHandler(authorizationRule, HHA, UserId, _caregivertaskRepository);
                authorizationHandler.SetNextValidation(authorizationHandler);

                var errors_list = DoublBookvalidationHandler.HandleValidation(validationCreateSchedule);

                List<string> result = new List<string>();

                if (result.Any())
                {
                    foreach (var item in errors_list)
                    {
                        result.Add(item.validation_error_message);
                    }
                }

                //Get all exceting exception for  schedule
                var exceptionColumn = "CaregiverTaskEvvExceptionID,SystemCode,isResolved";

                var exceptionFilter =
                    _repositoryFilterConditions.EqualFilter("CgTaskID", schedule.cgtask_id, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("isConflictValidation", true, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                var exceptions = await _caregiverTaskEvvExceptionsRepository.GetCaregiverTaskEVVExceptions(HHA, UserId, exceptionColumn, exceptionFilter);

                //check is there any new exception to add
                var Newexceptions = result.Where(e => !exceptions.Select(x => x.SystemCode).Contains(e));

                if (Newexceptions.Any())
                {
                    var NewexceptionsJson = JsonConvert.SerializeObject((from e in Newexceptions
                                             select new
                                             {
                                                 CGTASKID = caregivertask.CGTASK_ID,
                                                 ERRORDESC = "",
                                                 ERRORCODE = e,
                                                 EVENT = ""
                                             }));

                    await _caregiverTaskEvvExceptionsRepository.AddCaregiverTaskEVVExceptions_ValidationErrors(HHA, UserId, NewexceptionsJson);
                }

                //check if required to delete andy excsting exceptions
                var deleteexceptions = String.Join(",", exceptions.Where(e => e.isResolved == false && !result.Contains(e.SystemCode)).Select(e => e.CaregiverTaskEvvExceptionID).Distinct()); 

                if (deleteexceptions.Any())
                {
                    await _caregiverTaskEvvExceptionsRepository.ClearCaregiverTaskEVVExceptions(HHA, UserId, caregivertask.CGTASK_ID, deleteexceptions);
                }

                //update CaregiverTaskAdditional2.LAST_CONFLICT_VALIDATED_ON
                var ModifiedFields = JsonConvert.SerializeObject(new { LAST_CONFLICT_VALIDATED_ON = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") });
                string updatedCaregivertaskAdditional2Columns = new BuildUpdateQuery<CaregivertaskAdditionalEntity>().GetBuildUpdateQueryStatement(ModifiedFields);

                if (!string.IsNullOrEmpty(updatedCaregivertaskAdditional2Columns))
                    await _caregivertaskRepository.UpdateScheduleAdditional2(HHA, UserId, caregivertask.CGTASK_ID.ToString(), updatedCaregivertaskAdditional2Columns);

            }

            await _scheduleProcessingQueueRepository.DeleteScheduleProcessingQueue(HHA, UserId, batch_id);

            return recalculateScheduleExceptionsBatchDto;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Schedules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Service
{
    public class GetScheduleBasicDetail : IGetScheduleBasicDetail
    {
        private readonly ActionContextDto _actionContext;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IClientsRepository _clientsRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IConflictExceptionsValidationRules _conflictExceptionsValidationRules;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly ICDSClientBudgetLineItemRepository _cDSClientBudgetLineItemRepository;
        private readonly IHHALineofbusinessRepository _hHALineofbusinessRepository;
        public readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository;
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        public readonly ICaregiversRepository _caregiversRepository;
        private readonly ICaregiverDisciplinesRepository _caregiverDisciplinesRepository;
        private readonly ICaregiverOtherDisciplinesRepository _caregiverOtherDisciplinesRepository;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEVVExceptionsRepository;
       
        private IHHAEVVConfigurationsRepository _HHAEVVConfigurationsRepository;
        public GetScheduleBasicDetail(IClientsRepository clientsRepository,
            IPaymentsourcesRepository paymentsourcesRepository,
            IServiceCodesRepository serviceCodesRepository,
            ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
            IHttpContextAccessor httpContextAccessor,
            IRepositoryFilterConditions repositoryFilterConditions,
            ICDSClientBudgetLineItemRepository cDSClientBudgetLineItemRepository,
            IHHALineofbusinessRepository hHALineofbusinessRepository,
            IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository,
            ICaregivertasksRepository caregivertasksRepository,
            ICaregiversRepository caregiversRepository,
            ICaregiverOtherDisciplinesRepository caregiverOtherDisciplinesRepository,
            ICaregiverDisciplinesRepository caregiverDisciplinesRepository,
            IConflictExceptionsValidationRules conflictExceptionsValidationRules,
            IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository,
            ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptionsRepository,
            
            IHHAEVVConfigurationsRepository HHAEVVConfigurationsRepository
         )
        {
            _httpContextAccessor = httpContextAccessor;
            _serviceCodesRepository = serviceCodesRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            _cDSClientBudgetLineItemRepository = cDSClientBudgetLineItemRepository;
            _hHALineofbusinessRepository = hHALineofbusinessRepository;
            _clientsRepository = clientsRepository;
            _hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository;
            _caregivertasksRepository = caregivertasksRepository;
            _caregiversRepository = caregiversRepository;
            _caregiverDisciplinesRepository = caregiverDisciplinesRepository;
            _caregiverOtherDisciplinesRepository = caregiverOtherDisciplinesRepository;
            _conflictExceptionsValidationRules = conflictExceptionsValidationRules;
            _hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            _caregiverTaskEVVExceptionsRepository = caregiverTaskEVVExceptionsRepository;
            
            _HHAEVVConfigurationsRepository = HHAEVVConfigurationsRepository;

            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

        }
        public async Task<ValidateScheduleRules> getScheduleDetails(int hha, int user, CaregivertasksEntity caregiverTaskEntity)

        {
            var clientsEntity = _clientsRepository.GetClientsBasicInfo(hha, user, caregiverTaskEntity.CLIENT_ID);

            var PaymentSourceEntity = _paymentsourcesRepository.GetPaymentSourceAdditionalInfo(hha, user, caregiverTaskEntity.PAYMENT_SOURCE, clientsEntity.Result.HHA_BRANCH_ID);
            var servicecodeEntity = _serviceCodesRepository.GetServiceCodeAdditionalInfo(hha, user, caregiverTaskEntity.SERVICECODE_ID, clientsEntity.Result.HHA_BRANCH_ID, caregiverTaskEntity.PLANNED_DATE.ToString()).Result;
            var Clientpaymentsources = _clientsRepository.GetClientPaymentSourcesInfo(hha, user, caregiverTaskEntity.CLIENT_ID, caregiverTaskEntity.PAYMENT_SOURCE).Result.FirstOrDefault();
            var cdsplanyearfilters = _repositoryFilterConditions.EqualFilter("cdsplanyearservices.CDSPlanYearServiceId", caregiverTaskEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType).Build();
            var cdsplanyearcolumns = "cdsplanyearservices.CDSPlanYearServiceId, isnull(MinutesPerUnit, 0) MinutesPerUnit, isnull(NumberOfUnits, 0)NumberOfUnits,WeeklyHours,enableWeekAuthLimitOnHours,PayerCDSServiceCategoryID ";
            var LOBColumns = " LOB_ID,LOB_NAME,isEpisodeLess,LOBType ";
            int lobid = clientsEntity.Result.LOB_ID;
            var LOBFilter = _repositoryFilterConditions.EqualFilter("LOB_ID", lobid, FilterConditionDataTypeEnums.integerType).Build();
            var hhaLobs = await _hHALineofbusinessRepository.GetHHALOBs(hha, user, LOBColumns, LOBFilter);
            var hHABranchScheduleConfiguration = _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(hha, user, clientsEntity.Result.HHA_BRANCH_ID).Result;
            var hHAScheduleAuthSettings = _hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(hha, user).Result;
            
            CDSPlanYearServicesEntity cDSPlanYearServicesinfo = new CDSPlanYearServicesEntity();
            CDSClientBudgetLineItemEntity cDSClientBudgetLineItemEntity = new CDSClientBudgetLineItemEntity();
            if (caregiverTaskEntity.CdsPlanYearService > 0)
            {
                cDSPlanYearServicesinfo = await _cDSPlanYearServicesRepository._S_Schedule_GetCDSPlanYearServices(hha, user, cdsplanyearcolumns, cdsplanyearfilters);

                var CDSBudgetLineItemMasterID = servicecodeEntity.CDSBudgetLineItemMasterID;

                var Columns = "ClientBudgetLineItemID,BudgetLineItemMasterID,HoursPerWeek,WeeksPerYear,TotalAmount,Rate,CaregiverID";
                var Filter = "";
                Filter = _repositoryFilterConditions.EqualFilter("BudgetLineItemMasterID", CDSBudgetLineItemMasterID, FilterConditionDataTypeEnums.integerType).
                    EqualFilter("CDSPlanYearServiceID", caregiverTaskEntity.CdsPlanYearService, FilterConditionDataTypeEnums.integerType).
                    LessThanEqualFilter("EffectiveStartDate", caregiverTaskEntity.PLANNED_DATE, FilterConditionDataTypeEnums.datetimeType).
                    GreaterThanEqualFilter("EffectiveEndDate", caregiverTaskEntity.PLANNED_DATE, FilterConditionDataTypeEnums.datetimeType)
                    .Build();
                cDSClientBudgetLineItemEntity = await _cDSClientBudgetLineItemRepository._S_Schedule_GetCDSClientBudgetLineItem(hha, user, Columns, Filter);
                caregiverTaskEntity.CDSClientBudgetLineItemID = cDSClientBudgetLineItemEntity?.ClientBudgetLineItemID;
            }


            ValidateScheduleRules validateScheduleRules = new ValidateScheduleRules();
            validateScheduleRules.caregiverTaskEntity = caregiverTaskEntity;
            validateScheduleRules.paymentsourcesEntity = PaymentSourceEntity.Result;
            validateScheduleRules.serviceCodesEntity = servicecodeEntity;
            validateScheduleRules.lineofbusinessEntity = hhaLobs.FirstOrDefault();
            validateScheduleRules.clientsEntity = clientsEntity.Result;
            validateScheduleRules.clientPaymentSourcesEntity = Clientpaymentsources;
            validateScheduleRules.cDSplanyearservices = cDSPlanYearServicesinfo;
            validateScheduleRules.cDSClientBudgetLineItem = cDSClientBudgetLineItemEntity;
            validateScheduleRules.hHABranchScheduleConfigurationsEntity = hHABranchScheduleConfiguration;
            validateScheduleRules.hHAScheduleAuthSettingsEntities = hHAScheduleAuthSettings;

            var scheduleValidationRulesDtos = await _conflictExceptionsValidationRules.GetConflictExceptionsValidationRules(hha, user, validateScheduleRules);
            var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();
            var ClinicianOvertimeValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();

            validateScheduleRules.scheduleValidationRulesDtos = scheduleValidationRulesDtos;

            var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();

            var weekStartday = overtimeRule.Min_Duration_Mins;
            validateScheduleRules.caregiverschedluesdetails = new List<CaregivertasksEntity>();
           
                var schedule_status = ScheduleStatusEnums.Planned.ToString() + "," + ScheduleStatusEnums.In_Progress.ToString() + "," +
                ScheduleStatusEnums.Completed.ToString() + "," + ScheduleStatusEnums.Approved.ToString();

                DayOfWeek dayOfWeek = (DayOfWeek)Enum.GetValues(typeof(DayOfWeek)).GetValue(weekStartday);
                var weekStartDate = Common.GetWeekStartdate(caregiverTaskEntity.PLANNED_DATE, dayOfWeek);
                var weekEndDate = weekStartDate.AddDays(6);

                var schedulefilters = _repositoryFilterConditions.BetweenFilter("CaregiverTasks.PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                        .EqualFilter("CaregiverTasks.CAREGIVER", caregiverTaskEntity.CAREGIVER, FilterConditionDataTypeEnums.integerType, true)
                                        .Build();
                var scheduleColumns = "PLANNED_DATE,CGTASK_ID,CAREGIVER,CanBill,CONFIRMED,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,PLANNED_START_TIME,PLANNED_END_TIME,SERVICECODE_ID,cdsplanyearservice,Has_Child_Schedules,TotalHours,IS_BILLABLE";

                //schedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                var SchedulesList = _caregivertasksRepository.GetAllSchedulesList(hha, user, scheduleColumns, schedulefilters).Result;
                if (SchedulesList.Count() > 0)
                         SchedulesList = SchedulesList.Where(x => x.IS_BILLABLE == true &&x.CGTASK_ID!=caregiverTaskEntity.CGTASK_ID
                          && (x.STATUS=="Planned" || x.STATUS=="In-Progress"|| x.STATUS=="Completed" || x.STATUS=="Approved") ).Select(x=>x);
                
                if (SchedulesList != null)
                    validateScheduleRules.caregiverschedluesdetails = SchedulesList;
            


            var caregiverIDs = "";
            if (validateScheduleRules.caregiverschedluesdetails != null)
            {
                caregiverIDs = String.Join(",", validateScheduleRules.caregiverschedluesdetails.Where(e => e.CAREGIVER != null).Select(x => x.CAREGIVER).Distinct());
            }

            if (caregiverTaskEntity.CAREGIVER > 0)
            {
                if (!string.IsNullOrEmpty(caregiverIDs))
                    caregiverIDs += "," + caregiverTaskEntity.CAREGIVER.ToString();
                else
                    caregiverIDs = caregiverTaskEntity.CAREGIVER.ToString();
            }

            //DeviationList
            var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
            var client_maxhorusvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();


            string clientIds = "";
            validateScheduleRules.clientschedluesdetails = new List<CaregivertasksEntity>();
            
                if (!hHABranchScheduleConfiguration.ignoreacrossLobAdmits)
                {
                    //get all clients based on client group
                    if (clientsEntity.Result.ClientGroupNo > 0)
                    {
                        string clientFilters = _repositoryFilterConditions.EqualFilter("ClientGroupNo", clientsEntity.Result.ClientGroupNo, FilterConditionDataTypeEnums.integerType).Build();

                        var clientgrouplists = _clientsRepository.GetALLClients(hha, user, "CLIENT_ID", clientFilters).Result;
                        if (clientgrouplists.Any())
                            clientIds = string.Join(",", clientgrouplists.Select(x => x.CLIENT_ID).ToList());

                    }
                    else
                        clientIds = caregiverTaskEntity.CLIENT_ID.ToString();
                }
                else
                    clientIds = caregiverTaskEntity.CLIENT_ID.ToString();


                  weekStartDate = caregiverTaskEntity.PLANNED_DATE.AddDays(-1 * ((7 + ((int)caregiverTaskEntity.PLANNED_DATE.DayOfWeek - weekStartday)) % 7)).Date;
                  weekEndDate = weekStartDate.AddDays(6);

                  schedule_status = ScheduleStatusEnums.Planned.ToString() + "," + ScheduleStatusEnums.In_Progress.ToString() + "," +
                ScheduleStatusEnums.Completed.ToString() + "," + ScheduleStatusEnums.Approved.ToString();

                  schedulefilters = _repositoryFilterConditions.BetweenFilter("CaregiverTasks.PLANNED_DATE", weekStartDate, weekEndDate, FilterConditionDataTypeEnums.datetimeType)
                                        .InFilter("CaregiverTasks.Client_id", clientIds, FilterConditionDataTypeEnums.stringType)
                                        .EqualFilter("CaregiverTasks.IS_BILLABLE", true, FilterConditionDataTypeEnums.boolType)
                                        .NotEqualFilter("CaregiverTasks.CGTASK_ID", caregiverTaskEntity.CGTASK_ID, FilterConditionDataTypeEnums.integerType)
                                        .Build();
                  scheduleColumns = "CGTASK_ID,CLIENT_ID,CAREGIVER,CanBill,CONFIRMED,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,PLANNED_START_TIME,PLANNED_END_TIME,SERVICECODE_ID,cdsplanyearservice,Has_Child_Schedules,TotalHours";

                schedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                  SchedulesList = _caregivertasksRepository.GetAllSchedulesList(hha, user, scheduleColumns, schedulefilters).Result;

                validateScheduleRules.clientschedluesdetails = new List<CaregivertasksEntity>();
                if (SchedulesList != null)
                {
                    validateScheduleRules.clientschedluesdetails = SchedulesList.ToList().Where(x=> x.CLIENT_ID == caregiverTaskEntity.CLIENT_ID);
                    if (validateScheduleRules.clientschedluesdetails.Where(e => e.CAREGIVER != null).Any())
                    {
                        if (!string.IsNullOrEmpty(caregiverIDs))
                            caregiverIDs += "," + String.Join(",", validateScheduleRules.clientschedluesdetails.Where(e => e.CAREGIVER != null).Select(x => x.CAREGIVER).Distinct());
                        else
                            caregiverIDs += String.Join(",", validateScheduleRules.clientschedluesdetails.Where(e => e.CAREGIVER != null).Select(x => x.CAREGIVER).Distinct());
                    }
                }
            

            if (DeviationvalidationRule.Validationenabled || doublebookvalidationRule.Validationenabled)
            {
                var DependentSchedules = _caregiverTaskEVVExceptionsRepository.GetCaregiverTaskValidationErrorsDependentSchedules(hha, user, caregiverTaskEntity.CGTASK_ID).Result;

                if (DependentSchedules.Any())
                {
                    DependentSchedules = DependentSchedules.Where(x => x.DependentScheduleID > 0);
                    if (DependentSchedules.Any())                    
                        validateScheduleRules.validationErrorsDependentSchedules = DependentSchedules;
                }
            } 

            IEnumerable<CaregiversEntity> caregiversDetails = new List<CaregiversEntity>();
            validateScheduleRules.caregivers = new List<CaregiversEntity>();

            if (!string.IsNullOrEmpty(caregiverIDs))
            {
                var caregiverColumns = "CAREGIVER_ID,supervisingClinician,DISCIPLINE ,EMPLOYER_VENDOR   ";

                var caregiverFilters = _repositoryFilterConditions.InFilter("CAREGIVER_ID", caregiverIDs, FilterConditionDataTypeEnums.integerType)
                                       .Build();

                caregiversDetails = _caregiversRepository.GetCaregivers(hha, user, caregiverColumns, caregiverFilters).Result;
                validateScheduleRules.caregivers = caregiversDetails.Select(x => x).Where(x => caregiverIDs.Contains(x.CAREGIVER_ID.ToString()));
            }

            string columns = "ServiceGroups.ServiceCategory, CaregiverDisciplines.CAREGIVER_DISC_ID";

            var Cargiverdiscipline_DB = String.Join(",", validateScheduleRules.caregivers.Select(x => x.DISCIPLINE).Distinct());
            var Cargiverdiscipline_DV = String.Join(",", validateScheduleRules.caregivers.Select(x => x.DISCIPLINE).Distinct());
            var caregiverdiscids = Cargiverdiscipline_DB + Cargiverdiscipline_DV;
            string filters = _repositoryFilterConditions.InFilter("CaregiverDisciplines.CAREGIVER_DISC_ID", caregiverdiscids, FilterConditionDataTypeEnums.stringType).Build();

            var caregiverDisciplines = _caregiverDisciplinesRepository.GetCaregiverDiscipines(hha, user, columns, filters).Result;
            if (caregiverDisciplines != null)
                validateScheduleRules.caregiverDisciplines = caregiverDisciplines;
            else
                validateScheduleRules.caregiverDisciplines = new List<CaregiverDisciplinesEntity>();

            columns += ",CaregiverOtherDisciplines.CaregiverID";
            filters = "";
            filters = _repositoryFilterConditions.InFilter("CaregiverOtherDisciplines.CaregiverID", caregiverIDs, FilterConditionDataTypeEnums.integerType).
                            InFilter("CaregiverOtherDisciplines.CAREGIVER_DISC_ID", String.Join(",", caregiversDetails.Select(x => x.CAREGIVER_DISC_ID).Distinct()), FilterConditionDataTypeEnums.integerType).
                            Build();
            var CaregiverOtherDisciplines = _caregiverOtherDisciplinesRepository.GetCaregiverOtherDiscipines(hha, user, columns, filters).Result;
            validateScheduleRules.CaregiverOtherDisciplines = CaregiverOtherDisciplines;

            if (CaregiverOtherDisciplines != null)
                validateScheduleRules.CaregiverOtherDisciplines = CaregiverOtherDisciplines;
            else
                validateScheduleRules.CaregiverOtherDisciplines = new List<CaregiverOtherDisciplinesEntity>();


            string servicecodeids_DB = caregiverTaskEntity.SERVICECODE_ID.ToString();
            string servicecodeids_DV = "";

            if (validateScheduleRules.caregiverschedluesdetails != null && validateScheduleRules.caregiverschedluesdetails.Count() > 0 && caregiverTaskEntity.ServiceCategory == 0)
            {
                //get service groupIds  
                if (!string.IsNullOrEmpty(servicecodeids_DB))
                    servicecodeids_DB += "," + String.Join(",", validateScheduleRules.caregiverschedluesdetails.Select(x => x.SERVICECODE_ID).Distinct());
                else
                    servicecodeids_DB = String.Join(",", validateScheduleRules.caregiverschedluesdetails.Select(x => x.SERVICECODE_ID).Distinct());
            }

            if (validateScheduleRules.clientschedluesdetails != null && validateScheduleRules.clientschedluesdetails.Count() > 0)
            {
                servicecodeids_DV = String.Join(",", validateScheduleRules.clientschedluesdetails.Select(x => x.SERVICECODE_ID).Distinct());
                if (!string.IsNullOrEmpty(servicecodeids_DV))
                {
                    servicecodeids_DV += "," + servicecodeids_DV;
                }
                else
                    servicecodeids_DV = caregiverTaskEntity.SERVICECODE_ID.ToString();
            }

            var servicecodeids = "";

            if (!string.IsNullOrEmpty(servicecodeids_DB))
                servicecodeids = servicecodeids_DB;

            if (!string.IsNullOrEmpty(servicecodeids_DV))
            {
                if (!string.IsNullOrEmpty(servicecodeids))
                    servicecodeids += "," + servicecodeids_DV;
                else
                    servicecodeids = servicecodeids_DV;
            }

            if (!string.IsNullOrEmpty(servicecodeids))
            {
                columns = "ServiceGroups.ServiceCategory,ServiceCodes.SERVICE_CODE_ID";
                filters = "";
                filters = _repositoryFilterConditions.InFilter("ServiceCodes.SERVICE_CODE_ID", servicecodeids, FilterConditionDataTypeEnums.integerType).Build();

                var serviceinfo = _serviceCodesRepository.GetServiceCodes(hha, user, columns, filters).Result;
                validateScheduleRules.servicecodes = serviceinfo.Select(x => x).Where(x => servicecodeids_DB.Contains(x.ServiceCodeID.ToString()));
            }

            bool AllowToCheckoutifHardStopValidationExists = false;
            if (validateScheduleRules.hHAScheduleAuthSettingsEntities.FirstOrDefault().AllowToCheckoutifHardStopValidationExists)
            {
                if (caregiverTaskEntity.isEvvschedule && validateScheduleRules.hHAScheduleAuthSettingsEntities.FirstOrDefault().AllowToCheckoutifHardStopValidationExistsForEVV)
                    AllowToCheckoutifHardStopValidationExists = true;

                if (!caregiverTaskEntity.isEvvschedule && validateScheduleRules.hHAScheduleAuthSettingsEntities.FirstOrDefault().AllowToCheckoutifHardStopValidationExistsForNonEVV)
                    AllowToCheckoutifHardStopValidationExists = true;
            }
            validateScheduleRules.AllowToCheckoutifHardStopValidationExists = AllowToCheckoutifHardStopValidationExists;

            int EVVVendorVersionMasterID = 0;
            int evv_configuration_id = 0;
            bool require_latlongforFOb = false;
            if (caregiverTaskEntity.isEvvschedule)
            {

                EVVVendorVersionMasterID = Convert.ToInt32(PaymentSourceEntity.Result.EvvAggregatorVendorVersionMasterID);
                evv_configuration_id = Convert.ToInt32(PaymentSourceEntity.Result.evv_configuration_id);
               
                var EVVConfigurationFilter = _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .EqualFilter("EvvVendorVersionMasterID", EVVVendorVersionMasterID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("EvvConfigurationID",evv_configuration_id, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("HHABranchID", clientsEntity.Result.HHA_BRANCH_ID,FilterConditionDataTypeEnums.integerType)
                                                .Build();


                var HHAEVVconfigdetails = await _HHAEVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(hha, user, "require_latlong_for_fob", EVVConfigurationFilter);
                require_latlongforFOb = HHAEVVconfigdetails.Select(x => x.require_latlong_for_fob).FirstOrDefault();

                //Fob Geolocation validation

                if ((caregiverTaskEntity.CheckInSource == 6 || caregiverTaskEntity.CheckOutSource == 6)
                   && require_latlongforFOb == true)
                {
                    validateScheduleRules.RequireGeoLocationforFOB = true;

                }

                if (EVVVendorVersionMasterID > 0)
                {
                    validateScheduleRules.caregiverTaskEntity.EvvVendorVersionMasterID = EVVVendorVersionMasterID;
                }
            }
            
            return validateScheduleRules;
        }   
    }
}

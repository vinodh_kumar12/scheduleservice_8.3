﻿using AutoMapper;
using Newtonsoft.Json;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Models;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Scheduleservice.Services.Schedules.Service
{
    public class EVVSchedule : IEVVSchedule
    {
        IMapper _mapper;
        ICaregiverTaskEvvSchedulesRepository _caregiverTaskEvvSchedulesRepository;
        ICaregivertasksRepository _caregivertasksRepository;
        private readonly IHHAConfigurationsRepository _hHAConfigurationsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository;
        private readonly IClientEvvTerminalPayersRepository _clientEvvTerminalPayersRepository;
        private readonly ICaregiverTasksTerminalPayerRepository _caregiverTasksTerminalPayerRepository;
        private readonly ISchedule _schedule;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContext;
        private readonly ILogger<EVVSchedule> _logger;

        public EVVSchedule(IMapper mapper, ICaregiverTaskEvvSchedulesRepository caregiverTaskEvvSchedulesRepository, ICaregivertasksRepository caregivertasksRepository,
            IRepositoryFilterConditions repositoryFilterConditions, IClientsRepository clientsRepository, IHHAConfigurationsRepository hHAConfigurationsRepository,
            IClientEvvTerminalPayersRepository clientEvvTerminalPayersRepository, ICaregiverTasksTerminalPayerRepository caregiverTasksTerminalPayerRepository,
            ISchedule schedule, IHttpContextAccessor httpContextAccessor, ILogger<EVVSchedule> logger)
        {
            this._httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];


            this._mapper = mapper;
            this._caregiverTaskEvvSchedulesRepository = caregiverTaskEvvSchedulesRepository;
            this._caregivertasksRepository = caregivertasksRepository;
            this._hHAConfigurationsRepository = hHAConfigurationsRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._clientsRepository = clientsRepository;
            this._clientEvvTerminalPayersRepository = clientEvvTerminalPayersRepository;
            this._caregiverTasksTerminalPayerRepository = caregiverTasksTerminalPayerRepository;
            this._schedule = schedule;
            this._logger = logger;
        }
        public async Task<bool> RecalculateCaregiverTaskEvvSchedules(int HHA, int UserID, string ScheduleEVVBatchID)
        {   //Get  CaregiverTask Evv Schedules batches
            var CaregiverTaskEvvSchedules = await this._caregiverTaskEvvSchedulesRepository.GetCaregiverTaskEvvSchedules(HHA, UserID, ScheduleEVVBatchID);
            
            //get all  CGTASKID from   CaregiverTask Evv Schedules batches
            var RecalculateEVVSchedules = CaregiverTaskEvvSchedules.Where(y => y.SplitScheduleID == 0).Select(x => x.cgtaskid);
            var CGTaskIDs = _mapper.Map<IEnumerable<ScheduleCGTaskIDDto>>(RecalculateEVVSchedules);

            //convert to Json string
            var CGTaskIDsjson = JsonConvert.SerializeObject(CGTaskIDs);

            //Recalculate evv flag and update
            var EvvSchedules = await this._caregivertasksRepository.UpdateRecalculateEVVSchedules(HHA, UserID, CGTaskIDsjson, 1);

            //Delete  CaregiverTask Evv Schedules batches
            var DeleteCaregiverTaskEvvSchedules =  await this._caregiverTaskEvvSchedulesRepository.DeleteCaregiverTaskEvvSchedules(HHA, UserID, ScheduleEVVBatchID);
            return true;

        }

        public async Task<IEnumerable<ScheduleEVVBatchListDto>> InsertCaregiverTaskEvvSchedules(int HHA, int UserID, IEnumerable<ScheduleEVVBatchDto> CGTaskIDs)
        {
            var CGTaskIDsjson = JsonConvert.SerializeObject(CGTaskIDs);
            var result = await this._caregiverTaskEvvSchedulesRepository.InsertCaregiverTaskEvvSchedules(HHA, UserID, CGTaskIDsjson);
            var  ScheduleEVVBatchList= _mapper.Map<IEnumerable<ScheduleEVVBatchListDto>>(result);
            return ScheduleEVVBatchList;
        }

        public async Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> AutoRegisterEvvTerminalPayerSchedules(int HHA, int User)
        {
            IEnumerable<EvvTerminalPayerSchedulesDto> evvTerminalPayerSchedules = new List<EvvTerminalPayerSchedulesDto>();
            try
            {
                var Today = DateTime.UtcNow.ToConvertUTCTimeZone("PST");

                if(_actionContext.InstanceCode.ToLower() == "working")
                {
                    Today = DateTime.UtcNow.ToConvertUTCTimeZone("IST");
                }

                // Get last registered date
                var serviceColumns = "EvvDataBaseID";
                var serviceFilter = "";
                var m_ConfigurationsEntity = await _hHAConfigurationsRepository.Get_M_Configurations(HHA, User, serviceColumns, serviceFilter);

                //Get registered terminal payers
                var Columns = "ClientEVVTerminalPayers.client_evv_terminal_payer_id," +
                    "ClientEVVTerminalPayers.client_id," +
                    "ClientEVVTerminalPayers.Primary_client_payer_id," +
                    "ClientEVVTerminalPayers.hierarchy," +
                    "ClientEVVTerminalPayers.Terminal_client_payer_id," +
                    //"ClientEVVTerminalPayers.terminal_aggregator_id," +
                    "ClientEVVTerminalPayers.export_effective_from," +
                    "ISNULL(ClientEVVTerminalPayers.visits_last_registered_on,ClientEVVTerminalPayers.export_effective_from) AS visits_last_registered_on,"+
                    "ClientEVVTerminalPayers.evv_configuration_id";
                var Filters = _repositoryFilterConditions.EqualFilter("is_deleted", false, Core.Enums.FilterConditionDataTypeEnums.boolType).Build();
                var clientTerminalPayersEntities = await _clientEvvTerminalPayersRepository.GetClientTerminalPayersList(HHA, User, Columns, Filters);

                if (!clientTerminalPayersEntities.Any())
                {
                    return Response.NoContent(evvTerminalPayerSchedules);
                }

                //Get PrimaryPayerID
                Columns = "ClientPaymentSources.CLIENT_PAYMENT_ID," +
                    "ClientPaymentSources.CLIENT_ID," +
                    "ClientPaymentSources.PAYMENT_SOURCE_ID";
                var clientIDs = String.Join(',', clientTerminalPayersEntities.Select(x => x.client_id).Distinct().ToList());
                Filters = _repositoryFilterConditions.InFilter("ClientPaymentSources.CLIENT_ID", clientIDs, Core.Enums.FilterConditionDataTypeEnums.integerType).Build();
                var clientPayers = await _clientsRepository.GetClientPaymentSourcesList(HHA, User, Columns, Filters, "");

                string client_payer_json =
                    JsonConvert.SerializeObject(
                        from terminalPayers in clientTerminalPayersEntities
                        group terminalPayers by new
                        {
                            terminalPayers.client_id,
                            terminalPayers.Primary_client_payer_id
                        }
                        into groupedTerminalPayers
                        join payers in clientPayers on groupedTerminalPayers.Key.Primary_client_payer_id equals payers.CLIENT_PAYMENT_ID
                        select new
                        {
                            client_id = groupedTerminalPayers.Key.client_id,
                            payer_id = payers.PAYMENT_SOURCE_ID,
                            modified_time_from = groupedTerminalPayers.Min(x => x.visits_last_registered_on < Convert.ToDateTime("01/01/1900") ? Convert.ToDateTime("01/01/1900") : x.visits_last_registered_on)
                        }
                    );

                // Get Schedules
                var scheduleColumns = "CaregiverTasks.CGTASK_ID," +
                    "CaregiverTasks.PLANNED_DATE," +
                    "CaregiverTasks.CLIENT_ID," +
                    "CaregiverTaskAdditional.EvvMyUniqueID," +
                    "CaregiverTasks.PAYMENT_SOURCE";
                var scheduleFilters = _repositoryFilterConditions.EqualFilter("CaregiverTaskAdditional.isEvvschedule", true, Core.Enums.FilterConditionDataTypeEnums.boolType)
                    .EqualFilter("CaregiverTaskAdditional.IsSplitForBilling", false, Core.Enums.FilterConditionDataTypeEnums.boolType, true)
                    .InFilter("CaregiverTasks.STATUS", Core.Enums.ScheduleStatusEnums.Planned.ToString() + ","
                        + Core.Enums.ScheduleStatusEnums.In_Progress.ToString() + "," + Core.Enums.ScheduleStatusEnums.Completed.ToString() + ","
                        + Core.Enums.ScheduleStatusEnums.Approved.ToString(), Core.Enums.FilterConditionDataTypeEnums.stringType)
                    .EqualFilter("CaregiverTasks.CanBill", true, Core.Enums.FilterConditionDataTypeEnums.boolType, true)
                    .Build();
                var caregivertasksEntities = await _caregivertasksRepository.GetEvvTerminalPayerSchedulesToRegister(HHA, User, scheduleColumns, scheduleFilters, client_payer_json);

                //Get Child Schedules
                scheduleColumns = "CaregiverTaskChildSchedules.Child_Schedule_Id," +
                    "CaregiverTaskChildSchedules.PARENT_CGTASK_ID," +
                    "CaregiverTaskChildSchedules.SCHEDULE_DATE," +
                    "CaregiverTaskChildSchedules.CLIENT," +
                    "CaregiverTaskChildSchedules.childScheduleUniqueIDKT," +
                    "CaregiverTaskChildSchedules.PAYMENT_SOURCE";
                scheduleFilters = _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.isDeleted", false, Core.Enums.FilterConditionDataTypeEnums.boolType)
                    .EqualFilter("CaregiverTaskAdditional.IsSplitForBilling", true, Core.Enums.FilterConditionDataTypeEnums.boolType, true)
                    .EqualFilter("CaregiverTaskChildSchedules.isEvvschedule", true, Core.Enums.FilterConditionDataTypeEnums.boolType, true)
                    .EqualFilter("CaregiverTaskChildSchedules.CANBILL", true, Core.Enums.FilterConditionDataTypeEnums.boolType, true)
                    .Build();
                var caregiverTaskChildSchedulesEntities = await _caregivertasksRepository.GetEvvTerminalPayerChildSchedulesToRegister(HHA, User, scheduleColumns, scheduleFilters, client_payer_json);

                //Combine Parent and Child Schedules
                var combinedSchedules =
                    caregivertasksEntities.Select(x => new
                    {
                        ClientID = x.CLIENT_ID,
                        CgtaskID = x.CGTASK_ID,
                        ChildScheduleID = 0,
                        ScheduleDate = x.PLANNED_DATE,
                        PrimaryPayerID = x.PAYMENT_SOURCE,
                        ClientPayerID = 0,
                        Hierarchy = ' ',
                        TerminalPayerID = 0,
                        TerminalPayerAggregatorID = 0,
                        KanTimeUniqueID = (
                            x.EvvMyUniqueID.HasValue ?
                                x.EvvMyUniqueID.ToString().Substring(1, x.EvvMyUniqueID.ToString().Length - 1) :
                                m_ConfigurationsEntity.EvvDataBaseID.ToString() + x.CGTASK_ID.ToString() 
                        )
                    }).Union(
                        caregiverTaskChildSchedulesEntities.Select(x => new
                        {
                            ClientID = x.CLIENT,
                            CgtaskID = x.PARENT_CGTASK_ID,
                            ChildScheduleID = x.Child_Schedule_Id,
                            ScheduleDate = x.SCHEDULE_DATE,
                            PrimaryPayerID = x.PAYMENT_SOURCE,
                            ClientPayerID = 0,
                            Hierarchy = ' ',
                            TerminalPayerID = 0,
                            TerminalPayerAggregatorID = 0,
                            KanTimeUniqueID = (
                                x.childScheduleUniqueIDKT.HasValue ?
                                    x.childScheduleUniqueIDKT.ToString().Substring(1, x.childScheduleUniqueIDKT.ToString().Length - 1) :
                                    m_ConfigurationsEntity.EvvDataBaseID.ToString() + x.Child_Schedule_Id.ToString()
                            )
                        })
                    );

                if (!combinedSchedules.Any())
                {
                    return Response.NoContent(evvTerminalPayerSchedules);
                }

                //Get Effective Periods for terminal payers
                var terminalPayerIDs = string.Join(',',
                        clientTerminalPayersEntities.Where(x => x.hierarchy == 'S' || x.hierarchy == 'T')
                            .Select(x => x.Terminal_client_payer_id).Distinct().ToList()
                    );
                Columns = "ClientPayerEffectivePeriods.PayerEffectivePeriodID," +
                    "ClientPayerEffectivePeriods.Client_Additional_Payer_ID," +
                    "ClientPayerEffectivePeriods.EffectiveStart," +
                    "ClientPayerEffectivePeriods.EffectiveEnd";
                Filters = _repositoryFilterConditions.InFilter("ClientPayerEffectivePeriods.Client_Additional_Payer_ID", terminalPayerIDs, Core.Enums.FilterConditionDataTypeEnums.integerType).Build();
                var terminalPayerEffectivePeriods = await _clientsRepository.GetClientPayerEffectivePeriods(HHA, User, Columns, Filters);

                // Update terminal payer effective dates
                var clientTerminalPayers =
                    from cPayers in clientTerminalPayersEntities
                    join tPayers in terminalPayerEffectivePeriods on cPayers.Terminal_client_payer_id equals tPayers.Client_Additional_Payer_ID into terminalEffectivePeriods
                    from tEffectivePayers in terminalEffectivePeriods.DefaultIfEmpty()
                    select new
                    {
                        client_id = cPayers.client_id,
                        Primary_client_payer_id = cPayers.Primary_client_payer_id,
                        Terminal_client_payer_id = cPayers.Terminal_client_payer_id,
                        //terminal_aggregator_id = cPayers.terminal_aggregator_id,
                        hierarchy = cPayers.hierarchy,
                        export_effective_from = cPayers.export_effective_from,
                        EffectiveStart = tEffectivePayers?.EffectiveStart,
                        EffectiveEnd = tEffectivePayers?.EffectiveEnd,
                        rank = 0,
                        evv_configuration_id = cPayers.evv_configuration_id
                    };

                // Get Dual eligible payer effective dates
                var DEPayerIDs = string.Join(',',
                        clientTerminalPayersEntities.Where(x => x.hierarchy == 'D')
                            .Select(x => x.Terminal_client_payer_id).Distinct().ToList()
                    );
                Columns = "DualEligibilePayers.DualEligibilePayerID," +
                    "DualEligibilePayers.Effective_Start," +
                    "DualEligibilePayers.Effective_End";
                Filters = _repositoryFilterConditions.InFilter("DualEligibilePayers.DualEligibilePayerID", DEPayerIDs, Core.Enums.FilterConditionDataTypeEnums.integerType).Build();
                var DEPEffectivePeriods = await _clientsRepository.GetClientDualEligiblePaymentSourceslist(HHA, User, Columns, Filters);

                // update dual eligible payers effective dates
                clientTerminalPayers =
                    from cPayers in clientTerminalPayers
                    join dPayers in DEPEffectivePeriods on cPayers.Terminal_client_payer_id equals dPayers.DualEligibilePayerID into dualEffectivePeriods
                    from dEffectivePayers in dualEffectivePeriods.DefaultIfEmpty()
                    select new
                    {
                        client_id = cPayers.client_id,
                        Primary_client_payer_id = cPayers.Primary_client_payer_id,
                        Terminal_client_payer_id = cPayers.Terminal_client_payer_id,
                        //terminal_aggregator_id = cPayers.terminal_aggregator_id,
                        hierarchy = cPayers.hierarchy,
                        export_effective_from = cPayers.export_effective_from,
                        EffectiveStart = dEffectivePayers?.Effective_Start??cPayers.EffectiveStart,
                        EffectiveEnd = dEffectivePayers?.Effective_End??cPayers.EffectiveEnd,
                        rank = 0,
                        evv_configuration_id = cPayers.evv_configuration_id
                    };

                //Calculate rank for terminal payers
                clientTerminalPayers =
                    clientTerminalPayers
                    .GroupBy(x => (x.Primary_client_payer_id, x.hierarchy))
                    .SelectMany(
                        x => x.OrderBy(item => item.Terminal_client_payer_id).Select((item, index) => new
                        {
                            client_id = item.client_id,
                            Primary_client_payer_id = item.Primary_client_payer_id,
                            Terminal_client_payer_id = item.Terminal_client_payer_id,
                            //terminal_aggregator_id = item.terminal_aggregator_id,
                            hierarchy = item.hierarchy,
                            export_effective_from = item.export_effective_from,
                            EffectiveStart = item.EffectiveStart,
                            EffectiveEnd = item.EffectiveEnd,
                            rank = (index + 1),
                            evv_configuration_id = item.evv_configuration_id
                        })
                        );

                // Generate terminal payer schedules within terminal payer effective period
                var terminalPayerSchedules =
                    (from schedules in combinedSchedules
                    join payers in clientPayers on schedules.PrimaryPayerID equals payers.PAYMENT_SOURCE_ID
                    join terPayers in clientTerminalPayers on schedules.ClientID equals terPayers.client_id
                    where payers.CLIENT_PAYMENT_ID == terPayers.Primary_client_payer_id
                    select new EvvTerminalPayerSchedules
                    {
                        client_id = schedules.ClientID,
                        cgtask_id = schedules.CgtaskID,
                        child_schedule_id = schedules.ChildScheduleID,
                        client_payer_id = terPayers.Primary_client_payer_id,
                        hierarchy = terPayers.hierarchy,
                        client_terminal_payer_id = terPayers.Terminal_client_payer_id,
                        //terminal_payer_aggregator_id = terPayers.terminal_aggregator_id,
                        kantime_unique_id = (
                                schedules.ChildScheduleID > 0
                                ?
                                    (terPayers.hierarchy == 'S' ? "4" :
                                        terPayers.hierarchy == 'T' ? "6" :
                                        terPayers.hierarchy == 'D' ? "8" : "0"
                                    ) + terPayers.rank.ToString() + schedules.KanTimeUniqueID
                                :
                                    (terPayers.hierarchy == 'S' ? "3" :
                                        terPayers.hierarchy == 'T' ? "5" :
                                        terPayers.hierarchy == 'D' ? "7" : "0"
                                    ) + terPayers.rank.ToString() + schedules.KanTimeUniqueID
                            ),
                        schedule_date = schedules.ScheduleDate,
                        evv_configuration_id = terPayers.evv_configuration_id
                    }).ToList();

                if (!terminalPayerSchedules.Any())
                {
                    return Response.NoContent(evvTerminalPayerSchedules);
                }

                // update is_deleted to true for schedules outside terminal payer effective period or export effective period
                terminalPayerSchedules.ForEach(schedule =>
                {
                    schedule.is_deleted = !clientTerminalPayers.Any(x => x.Terminal_client_payer_id == schedule.client_terminal_payer_id
                                   && (schedule.schedule_date >= x.export_effective_from || x.export_effective_from == DateTime.MinValue)
                                   && (schedule.schedule_date >= x.EffectiveStart || x.EffectiveStart == DateTime.MinValue)
                                   && (schedule.schedule_date <= x.EffectiveEnd || x.EffectiveEnd == DateTime.MinValue)
                        );
                });

                var registeredSchedulesResponse = await this.RegisterTerminalPayerSchedules(HHA, User, terminalPayerSchedules);

                string json_data = JsonConvert.SerializeObject(clientTerminalPayersEntities.Select(x => new
                {
                    client_evv_terminal_payer_id = x.client_evv_terminal_payer_id,
                    hha = x.hha,
                    visits_last_registered_on = Today.ToString("MM/dd/yyyy hh:mm:ss")
                }));
                
                if(json_data != "[]")
                {
                    var res = _clientEvvTerminalPayersRepository.UpdateClientEvvTerminalPayers(HHA, User, json_data);
                }

                if (registeredSchedulesResponse.http_status_code == HttpStatusCode.OK)
                {
                    evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(registeredSchedulesResponse.response_payload);
                }
                else if(registeredSchedulesResponse.http_status_code == HttpStatusCode.NoContent)
                {
                    Response.NoContent(evvTerminalPayerSchedules);
                }
                else
                {
                    return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to register schedules");
                }


            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }

            return Response.Ok(evvTerminalPayerSchedules);
        }

        public async Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> InsertEvvTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> terminalPayerSchedules)
        {
            IEnumerable<EvvTerminalPayerSchedulesDto> evvTerminalPayerSchedules = new List<EvvTerminalPayerSchedulesDto>();
            try
            {
                var maxRecordsToInsert = 2000;
                for (int i = 0; i < Math.Ceiling((decimal)terminalPayerSchedules.Count() / maxRecordsToInsert); i++)
                {
                    int ret;
                    var ScheduleToInsert = terminalPayerSchedules.Skip(i * maxRecordsToInsert).Take(maxRecordsToInsert).ToList();

                    var insertColumns = "HHA, CgTaskID, KanTimeUniqueID, ClientID, PrimaryPayerID, Hierarchy, TerminalPayerID, /*Terminal_aggregator_id*/, evv_configuration_id";
                    var insertValues =
                        JsonConvert.SerializeObject(
                            ScheduleToInsert
                            .Where(x => x.child_schedule_id == 0)
                            .Select(x => new
                            {
                                HHA = HHA,
                                CgTaskID = x.cgtask_id,
                                KanTimeUniqueID = x.kantime_unique_id,
                                ClientID = x.client_id,
                                PrimaryPayerID = x.client_payer_id,
                                Hierarchy = x.hierarchy,
                                TerminalPayerID = x.client_terminal_payer_id,
                                //Terminal_aggregator_id = x.terminal_payer_aggregator_id,
                                evv_configuration_id = x.evv_configuration_id
                            }));
                    if (insertValues != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.InsertCaregiverTasksTerminalPayer(HHA, User, insertColumns, insertValues);
                        if (ret == 0)
                        {
                            evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(
                                    ScheduleToInsert
                                    .Where(x => x.child_schedule_id == 0)
                                    .Select(x => new EvvTerminalPayerSchedulesDto
                                    {
                                        HHA = HHA,
                                        CgTaskID = x.cgtask_id
                                    })
                                );
                        }
                        else
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to Insert Registered Schedules");
                        }
                    }

                    insertColumns = "HHA, CgTaskID, ChildScheduleID, KanTimeUniqueID, ClientID, PrimaryPayerID, Hierarchy, TerminalPayerID, /*Terminal_aggregator_id*/,evv_configuration_id";
                    insertValues =
                        JsonConvert.SerializeObject(
                            ScheduleToInsert
                            .Where(x => x.child_schedule_id > 0)
                            .Select(x => new
                            {
                                HHA = HHA,
                                CgTaskID = x.cgtask_id,
                                ChildScheduleID = x.child_schedule_id,
                                KanTimeUniqueID = x.kantime_unique_id,
                                ClientID = x.client_id,
                                PrimaryPayerID = x.client_payer_id,
                                Hierarchy = x.hierarchy,
                                TerminalPayerID = x.client_terminal_payer_id,
                                //Terminal_aggregator_id = x.terminal_payer_aggregator_id,
                                evv_configuration_id = x.evv_configuration_id
                            }));
                    if(insertValues != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.InsertCaregiverTaskChildScheduleTerminalPayer(HHA, User, insertColumns, insertValues);
                        if (ret == 0)
                        {
                            evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(
                                    ScheduleToInsert
                                    .Where(x => x.child_schedule_id > 0)
                                    .Select(x => new EvvTerminalPayerSchedulesDto
                                    {
                                        HHA = HHA,
                                        CgTaskID = x.cgtask_id,
                                        child_schedule_id = x.child_schedule_id
                                    })
                                );
                        }
                        else
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to Insert Registered Child Schedules");
                        }
                    }
                }

            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }
            return Response.Ok(evvTerminalPayerSchedules);
        }

        public async Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> UpdateEvvTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> terminalPayerSchedules)
        {
            IEnumerable<EvvTerminalPayerSchedulesDto> evvTerminalPayerSchedules = new List<EvvTerminalPayerSchedulesDto>();
            var jsonData = "";
            var ret = 1;
            try
            {
                var maxRecordsToInsert = 2000;
                for (int i = 0; i < Math.Ceiling((decimal)terminalPayerSchedules.Count() / maxRecordsToInsert); i++)
                {
                    var ScheduleToUpdate = terminalPayerSchedules.Skip(i * maxRecordsToInsert).Take(maxRecordsToInsert).ToList();
                    // activate deleted parent schedules
                    jsonData = JsonConvert.SerializeObject(ScheduleToUpdate.Where(x => x.can_activate == true && x.child_schedule_id == 0).Select(x => new
                    {
                        CaregiverTasksTerminalPayerID = x.caregiver_task_terminal_payer_id,
                        CgTaskID = x.cgtask_id,
                        KanTimeUniqueID = x.kantime_unique_id,
                        ClientID = x.client_id,
                        PrimaryPayerID = x.client_payer_id,
                        Hierarchy = x.hierarchy,
                        TerminalPayerID = x.client_terminal_payer_id,
                        //Terminal_aggregator_id = x.terminal_payer_aggregator_id,
                        isDeleted = false,
                        evv_configuration_id = x.evv_configuration_id
                    }));

                    if (jsonData != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.UpdateTerminalPayerSchedules(HHA, User, jsonData);
                        if (ret == 0)
                        {
                            evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(
                                    ScheduleToUpdate
                                    .Where(x => x.child_schedule_id == 0)
                                    .Select(x => new EvvTerminalPayerSchedulesDto
                                    {
                                        HHA = HHA,
                                        CgTaskID = x.cgtask_id
                                    })
                                );
                        }
                        else
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to update Registered Schedules");
                        }
                    }

                    // activate deleted child schedules
                    jsonData = JsonConvert.SerializeObject(ScheduleToUpdate.Where(x => x.can_activate == true && x.child_schedule_id > 0).Select(x => new
                    {
                        CaregiverChildTaskTerminalPayerID = x.caregiver_child_task_terminal_payer_id,
                        CgTaskID = x.cgtask_id,
                        KanTimeUniqueID = x.kantime_unique_id,
                        ClientID = x.client_id,
                        PrimaryPayerID = x.client_payer_id,
                        Hierarchy = x.hierarchy,
                        TerminalPayerID = x.client_terminal_payer_id,
                        //Terminal_aggregator_id = x.terminal_payer_aggregator_id,
                        isDeleted = false,
                        evv_configuration_id = x.evv_configuration_id
                    }));

                    if (jsonData != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.UpdateTerminalPayerChildSchedules(HHA, User, jsonData);
                        if (ret == 0)
                        {
                            evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(
                                    ScheduleToUpdate
                                    .Where(x => x.child_schedule_id > 0)
                                    .Select(x => new EvvTerminalPayerSchedulesDto
                                    {
                                        HHA = HHA,
                                        CgTaskID = x.cgtask_id,
                                        child_schedule_id = x.child_schedule_id
                                    })
                                );
                        }
                        else
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to update Registered Child Schedules");
                        }
                    }

                    // delete unwanted parent schedules
                    jsonData = JsonConvert.SerializeObject(ScheduleToUpdate.Where(x => x.can_delete == true && x.child_schedule_id == 0).Select(x => new
                    {
                        CaregiverTasksTerminalPayerID = x.caregiver_task_terminal_payer_id,
                        isDeleted = true
                    }));

                    if (jsonData != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.UpdateTerminalPayerSchedules(HHA, User, jsonData);
                        if (ret != 0)
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to delete Un-registered Schedules");
                        }
                    }

                    // delete unwanted child schedules
                    jsonData = JsonConvert.SerializeObject(ScheduleToUpdate.Where(x => x.can_delete == true && x.child_schedule_id > 0).Select(x => new
                    {
                        CaregiverChildTaskTerminalPayerID = x.caregiver_child_task_terminal_payer_id,
                        isDeleted = true
                    }));

                    if (jsonData != "[]")
                    {
                        ret = await _caregiverTasksTerminalPayerRepository.UpdateTerminalPayerChildSchedules(HHA, User, jsonData);
                        if (ret != 0)
                        {
                            return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to delete Un-registered Schedules");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }

            return Response.Ok(evvTerminalPayerSchedules);
        }

        public async Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> RegisterTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> terminalPayerSchedules)
        {
            IEnumerable<EvvTerminalPayerSchedulesDto> evvTerminalPayerSchedules = new List<EvvTerminalPayerSchedulesDto>();
            var Columns = "";
            var Filters = "";

            try
            {
                //Get already registered schedules including soft deleted schedules
                Columns = "CaregiverTasksTerminalPayerID,"
                    + "HHA,"
                    + "CgTaskID,"
                    + "KanTimeUniqueID,"
                    + "ClientID,"
                    + "PrimaryPayerID,"
                    + "Hierarchy,"
                    + "TerminalPayerID,"
                    //+ "Terminal_aggregator_id,"
                    + "isDeleted,"
                    + "evv_configuration_id";
                Filters = _repositoryFilterConditions.EqualFilter("HHA", HHA, Core.Enums.FilterConditionDataTypeEnums.integerType)
                    .InFilter("CgTaskID", string.Join(",", terminalPayerSchedules.Select(x => x.cgtask_id).Distinct().ToList()), Core.Enums.FilterConditionDataTypeEnums.integerType)
                    .Build();
                var registeredTerminalPayerSchedules = await _caregiverTasksTerminalPayerRepository.GetTerminalPayerSchedules(HHA, User, Columns, Filters);

                //Get already registered child schedules including soft deleted schedules
                Columns = "CaregiverChildTaskTerminalPayerID,"
                    + "HHA,"
                    + "CgTaskID,"
                    + "ChildScheduleID,"
                    + "KanTimeUniqueID,"
                    + "ClientID,"
                    + "PrimaryPayerID,"
                    + "Hierarchy,"
                    + "TerminalPayerID,"
                    //+ "Terminal_aggregator_id,"
                    + "isDeleted,"
                    + "evv_configuration_id";
                Filters = _repositoryFilterConditions.EqualFilter("HHA", HHA, Core.Enums.FilterConditionDataTypeEnums.integerType)
                    .InFilter("CgTaskID", string.Join(",", terminalPayerSchedules.Select(x => x.cgtask_id).Distinct().ToList()), Core.Enums.FilterConditionDataTypeEnums.integerType)
                    .Build();
                var registeredTerminalPayerChildSchedules = await _caregiverTasksTerminalPayerRepository.GetTerminalPayerChildSchedules(HHA, User, Columns, Filters);

                //Combine registered parent and child schedules
                List<EvvTerminalPayerSchedules> combinedRegisteredSchedules =
                    registeredTerminalPayerSchedules.Select(x => new EvvTerminalPayerSchedules
                    {
                        client_id = x.ClientID,
                        cgtask_id = x.CgTaskID,
                        kantime_unique_id = x.KanTimeUniqueID.ToString(),
                        caregiver_task_terminal_payer_id = x.CaregiverTasksTerminalPayerID,
                        is_deleted = x.isDeleted,
                        client_payer_id = x.PrimaryPayerID,
                        client_terminal_payer_id = x.TerminalPayerID,
                        hierarchy = x.Hierarchy,
                        //terminal_payer_aggregator_id = x.Terminal_aggregator_id,
                        evv_configuration_id = x.evv_configuration_id
                    })
                    .Union(registeredTerminalPayerChildSchedules.Select(x => new EvvTerminalPayerSchedules
                    {
                        client_id = x.ClientID,
                        cgtask_id = x.CgTaskID,
                        child_schedule_id = x.ChildScheduleID,
                        kantime_unique_id = x.KanTimeUniqueID.ToString(),
                        caregiver_child_task_terminal_payer_id = x.CaregiverChildTaskTerminalPayerID,
                        is_deleted = x.isDeleted,
                        client_payer_id = x.PrimaryPayerID,
                        client_terminal_payer_id = x.TerminalPayerID,
                        hierarchy = x.Hierarchy,
                        //terminal_payer_aggregator_id = x.Terminal_aggregator_id,
                        evv_configuration_id = x.evv_configuration_id
                    })).ToList();

                if (combinedRegisteredSchedules.Any())
                {
                    // update is_deleted schedules which are outside temrinal payer effective period or export effective period
                    combinedRegisteredSchedules.ForEach(rSchedule =>
                    {
                        rSchedule.can_delete =
                            terminalPayerSchedules.Any(schedule =>
                                schedule.kantime_unique_id == rSchedule.kantime_unique_id
                                && rSchedule.is_deleted == false
                                && schedule.is_deleted == true // schedules outside terminal payer effective period or export effective peiod
                            );
                    });

                    //get schedules to be deleted in database
                    combinedRegisteredSchedules.Where(rSchedule => rSchedule.can_delete == false).ToList().ForEach(rSchedule =>
                        {
                            rSchedule.can_delete =
                                terminalPayerSchedules.Any(schedule =>
                                    schedule.cgtask_id == rSchedule.cgtask_id
                                    && ((schedule.child_schedule_id == 0 && rSchedule.child_schedule_id > 0) // child schedule become parent schedule
                                        || (schedule.child_schedule_id > 0 && rSchedule.child_schedule_id == 0) // parent schedule become child schedule
                                    )
                                );
                        });

                    //get schedules to be activated in database
                    combinedRegisteredSchedules.ForEach(rSchedule =>
                    {
                        rSchedule.can_activate =
                            terminalPayerSchedules.Any(schedule =>
                                schedule.cgtask_id == rSchedule.cgtask_id
                                && schedule.child_schedule_id == rSchedule.child_schedule_id
                                && rSchedule.is_deleted == true
                                && rSchedule.kantime_unique_id == schedule.kantime_unique_id
                                && schedule.is_deleted == false
                            );
                    });

                    // Get new payer info for existing schedules
                    combinedRegisteredSchedules =
                        (from rSchedules in combinedRegisteredSchedules
                        join schedules in terminalPayerSchedules
                            on new { rSchedules.kantime_unique_id, rSchedules.client_id, rSchedules.cgtask_id, rSchedules.child_schedule_id }
                            equals new { schedules.kantime_unique_id, schedules.client_id, schedules.cgtask_id, schedules.child_schedule_id }
                            into jSchedules
                        from schedules in jSchedules.DefaultIfEmpty()
                        select new EvvTerminalPayerSchedules
                        {
                            client_id = rSchedules.client_id,
                            cgtask_id = rSchedules.cgtask_id,
                            child_schedule_id = rSchedules.child_schedule_id,
                            kantime_unique_id = rSchedules.kantime_unique_id,
                            caregiver_task_terminal_payer_id = rSchedules.caregiver_task_terminal_payer_id,
                            caregiver_child_task_terminal_payer_id = rSchedules.caregiver_child_task_terminal_payer_id,
                            client_payer_id = schedules?.client_payer_id ?? rSchedules.client_payer_id,
                            hierarchy = schedules?.hierarchy ?? rSchedules.hierarchy,
                            client_terminal_payer_id = schedules?.client_terminal_payer_id ?? rSchedules.client_terminal_payer_id,
                            //terminal_payer_aggregator_id = schedules?.terminal_payer_aggregator_id ?? rSchedules.terminal_payer_aggregator_id,
                            is_deleted = rSchedules.is_deleted,
                            can_delete = rSchedules.can_delete,
                            can_activate = rSchedules.can_activate,
                            evv_configuration_id = schedules?.evv_configuration_id ?? rSchedules.evv_configuration_id,
                        }).ToList();

                    // delete existing schedules from new schedules
                    terminalPayerSchedules =
                        from schedule in terminalPayerSchedules
                        where !combinedRegisteredSchedules.Any(x => x.kantime_unique_id == schedule.kantime_unique_id)
                        select schedule;
                }

                // delete schedules outside export effective or temrinal payer effective date
                terminalPayerSchedules = from schedule in terminalPayerSchedules
                                         where schedule.is_deleted == false
                                         select schedule;
                
                // insert new schedules
                if (terminalPayerSchedules.Any())
                {
                    var insertSchedulesResponse = await this.InsertEvvTerminalPayerSchedules(HHA, User, terminalPayerSchedules);

                    if (insertSchedulesResponse.http_status_code == HttpStatusCode.OK)
                    {
                        evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(insertSchedulesResponse.response_payload);
                    }
                    else
                    {
                        return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(insertSchedulesResponse.error_code, insertSchedulesResponse.error_message);
                    }
                }

                // update existing schedules
                if (combinedRegisteredSchedules.Where(x => x.can_activate == true || x.can_delete == true).Any())
                {
                    var updateScheduleResponse = await this.UpdateEvvTerminalPayerSchedules(HHA, User, combinedRegisteredSchedules.Where(x => x.can_activate == true || x.can_delete == true));

                    if (updateScheduleResponse.http_status_code == HttpStatusCode.OK)
                    {
                        evvTerminalPayerSchedules = evvTerminalPayerSchedules.Union(updateScheduleResponse.response_payload);
                    }
                    else
                    {
                        return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(updateScheduleResponse.error_code, updateScheduleResponse.error_message);
                    }
                }

                if(!terminalPayerSchedules.Any() && !combinedRegisteredSchedules.Where(x => x.can_activate == true || x.can_delete == true).Any())
                {
                    return Response.NoContent(evvTerminalPayerSchedules);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }

            return Response.Ok(evvTerminalPayerSchedules);
        }       
    }
}

﻿using AutoMapper; 
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts; 
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers; 
using KanCache.Interfaces; 
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Schedules.Service
{
    public class EVVException : IEVVException
    {
         
        private readonly IAutoGenerateException autoGenerateException;
        private readonly IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository;
        private readonly IHHAEvvExceptionEventsRepository c_EvvExceptionEventsRepository;
        private readonly IUAEvvExceptionEventsRepository ua_EvvExceptionEventsRepository;
        private readonly IHHAEVVConfigurationsRepository i_C_EVVConfigurationsRepository;
        private readonly ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptionsRepository;
        private readonly IMapper mapper;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IUAAgencyDatabaseMasterList _agencyDatabaseMasterList;
        private readonly IUsersRepository usersRepository;

        private readonly IHttpContextAccessor _accessor;
        private readonly ICacheController _cacheController;
        private readonly ILogger<EVVException> logger;
        public ActionContextDto ActionContext { get; }

        public EVVException(IUAAgencyDatabaseMasterList agencyDatabaseMasterList,
            IAutoGenerateException _autoGenerateException, IHHAScheduleAuthSettingsRepository _HHAScheduleAuthSettingsRepository,
            IHHAEvvExceptionEventsRepository i_C_EvvExceptionEventsRepository,
            IUAEvvExceptionEventsRepository i_UA_EvvExceptionEventsRepository,
            IHHAEVVConfigurationsRepository _i_C_EVVConfigurationsRepository,
            ICaregiverTaskEVVExceptionsRepository _caregiverTaskEVVExceptionsRepository,
            IMapper _mapper,
            IRepositoryFilterConditions repositoryFilterConditions,
            IUsersRepository _usersRepository,
            IServiceProvider serviceProvider, ICacheController cacheController,
            ILogger<EVVException> _logger)
        {
            this._agencyDatabaseMasterList = agencyDatabaseMasterList;    
            this.autoGenerateException = _autoGenerateException;
            this.hHAScheduleAuthSettingsRepository = _HHAScheduleAuthSettingsRepository;
            this.c_EvvExceptionEventsRepository = i_C_EvvExceptionEventsRepository;
            this.ua_EvvExceptionEventsRepository = i_UA_EvvExceptionEventsRepository;
            this.i_C_EVVConfigurationsRepository = _i_C_EVVConfigurationsRepository;
            this.caregiverTaskEVVExceptionsRepository = _caregiverTaskEVVExceptionsRepository;
            this.mapper = _mapper;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this.usersRepository = _usersRepository;
            this.logger = _logger;

            this._cacheController = cacheController;
            _accessor = serviceProvider.GetService<IHttpContextAccessor>();
            if (_accessor.HttpContext != null)
                ActionContext = (ActionContextDto)_accessor.HttpContext.Items[ConstantKeys.ActionContext];
            _cacheController.InstanceCode = ActionContext.InstanceCode;
        }  
        public async Task<bool> AutogenerateEVVEventExceptions(int HHA, int UserID)
        {
            bool ret = true; 
            try
            { 
                var agencyDatabaselistResult = _agencyDatabaseMasterList.GetAgencyDatabaseList();
               
               
                int HHA_SupportUserID = 0;

                if (agencyDatabaselistResult.Count() > 0 && HHA > 0)
                {
                    agencyDatabaselistResult = agencyDatabaselistResult.Where(x => x.HHA_ID == HHA).ToList();
                   
                }

                //For each DB
                foreach (var HHADetails in agencyDatabaselistResult)
                {
                    ActionContext.HHA = HHADetails.HHA_ID;
                    ActionContext.DbServer = HHADetails.Server;
                    HHA_SupportUserID = 0;

                    //get hha event grace min
                    var HHAConfigDetails = await hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(HHADetails.HHA_ID, UserID);

                    //get HHA EVV Vendor list
                    var EVVConfigurationFilter = _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                    var HHAEVVVendors = await i_C_EVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(HHADetails.HHA_ID, UserID, "EvvConfigurationID,EvvVendorVersionMasterID, EffectiveDate,AttestationMandatoryForAllReasonCodes,MissedVisitReasonCodeAvailable,MissedVisitActionCodeAvailable,ExcepionCodeAvailable,ReasonCodeAvailable,ActionCodesAvailable,captureExceptionCodeOnReasonCode,requireAllExceptionsToClearForApproval,isActionCodeMandatoryOnReasonCode,isExceptionCodeMandatoryOnReasonCode,isMissedVisitActionCodeMandatoryOnReasonCode,canGrowMissedVisitReason,canGrowMissedVisitAction,canGrowException,canGrowReasonCode,canGrowActionCode", EVVConfigurationFilter);

                    HHAEVVVendors = HHAEVVVendors.Where(x => x.ExcepionCodeAvailable == true).ToList();

                    if (HHAEVVVendors.Count() > 0)
                    {
                        //get EVV Events for the HHA, Kantime Specific
                        var HHAEVVEvents = await c_EvvExceptionEventsRepository.GetHHAEVVEvents(HHADetails.HHA_ID, UserID, 0);

                        //Get EVV Events for the Vendor
                        var EVVVendorEvents = await ua_EvvExceptionEventsRepository.GetEVVVendorEvents(HHADetails.HHA_ID, UserID, 0);

                        if (HHAEVVEvents.Count() > 0 || EVVVendorEvents.Count() > 0)
                        {
                            var IsQueueEnabledEvents = EVVVendorEvents.Where(x => x.IsQueueEnabled == true).ToList();

                            if (HHAEVVEvents.Count() > 0)
                            {
                                
                                IsQueueEnabledEvents = (from item in HHAEVVEvents.Where(x => x.IsQueueEnabled == true)
                                                            select new _UA_EvvExceptionEventsEntity()
                                                            { 
                                                              Event = item.Event,
                                                              ExceptionEventMasterID = item.EvvExceptionEventID,
                                                              EVVVendorVersionMasterID = item.EVVVendorVersionMasterID,
                                                              IsQueueEnabled = true 
                                                            }
                                                       ).ToList();

                            }

                            if (IsQueueEnabledEvents.Count() > 0)
                            {
                                IEnumerable<CaregivertasksEntity> VISIT_NO_SHOWEligibleSchedules = null;
                                IEnumerable<CaregivertasksEntity> UNSCHEDULED_VISITEligibleSchedules = null;
                                IEnumerable<CaregivertasksEntity> MISSED_CHECKOUTEligibleSchedules = null;

                                if (IsQueueEnabledEvents.Where(x => x.Event == "VISIT_NO_SHOW").Count() > 0)
                                {                                     
                                    VISIT_NO_SHOWEligibleSchedules = await autoGenerateException.GetExceptionEligibleSchedules(HHADetails.HHA_ID, UserID, "VISIT_NO_SHOW", HHAConfigDetails.Select(x => x.AutoGenerateNoShowExceptionMin).SingleOrDefault());

                                    if (VISIT_NO_SHOWEligibleSchedules.Count() > 0)
                                    {
                                        if (UserID == 0)
                                        {
                                            var userinfo = await usersRepository.GetHHAUsers(HHADetails.HHA_ID, " Top 1 User_ID ", " and ROLE_TYPE = -1 ");

                                            if (userinfo.Count() > 0)
                                                HHA_SupportUserID = userinfo.Select(x => x.USER_ID).Single();

                                        }
                                        else
                                            HHA_SupportUserID = UserID;

                                        ret = await autoGenerateException.AutoRaiseExceptionForSchedules(HHADetails.HHA_ID, HHA_SupportUserID, "VISIT_NO_SHOW", IsQueueEnabledEvents, VISIT_NO_SHOWEligibleSchedules);
                                    }
                                }

                                if (IsQueueEnabledEvents.Where(x => x.Event == "UNSCHEDULED_VISIT").Count() > 0)
                                {
                                    //Raise UNSCHEDULED_VISIT Exception 
                                    UNSCHEDULED_VISITEligibleSchedules = await autoGenerateException.GetExceptionEligibleSchedules(HHADetails.HHA_ID, UserID, "UNSCHEDULED_VISIT", HHAConfigDetails.Select(x => x.AutoGenerateUnscheduledExceptionMin).SingleOrDefault());

                                    if (UNSCHEDULED_VISITEligibleSchedules.Count() > 0)
                                    {
                                        if (UserID == 0 && HHA_SupportUserID == 0)
                                        {
                                            var userinfo = await usersRepository.GetHHAUsers(HHADetails.HHA_ID, " Top 1 User_ID ", " and ROLE_TYPE = -1 ");

                                            if (userinfo.Count() > 0)
                                                HHA_SupportUserID = userinfo.Select(x => x.USER_ID).Single();

                                        }
                                        else if (HHA_SupportUserID == 0)
                                            HHA_SupportUserID = UserID;

                                        ret = await autoGenerateException.AutoRaiseExceptionForSchedules(HHADetails.HHA_ID, HHA_SupportUserID, "UNSCHEDULED_VISIT", IsQueueEnabledEvents, UNSCHEDULED_VISITEligibleSchedules);
                                    }
                                }

                                if (IsQueueEnabledEvents.Where(x => x.Event == "MISSED_CHECKOUT").Count() > 0)
                                {
                                    //Raise MISSED_CHECKOUT Exception 
                                    MISSED_CHECKOUTEligibleSchedules = await autoGenerateException.GetExceptionEligibleSchedules(HHADetails.HHA_ID, UserID, "MISSED_CHECKOUT", HHAConfigDetails.Select(x => x.AutoGenerateMissedCheckoutExceptionMin).SingleOrDefault());
                                    if (MISSED_CHECKOUTEligibleSchedules.Count() > 0)
                                    {
                                        if (UserID == 0 && HHA_SupportUserID == 0)
                                        {
                                            var userinfo = await usersRepository.GetHHAUsers(HHADetails.HHA_ID, " Top 1 User_ID ", " and ROLE_TYPE = -1 ");

                                            if (userinfo.Count() > 0)
                                                HHA_SupportUserID = userinfo.Select(x => x.USER_ID).Single();

                                        }
                                        else if (HHA_SupportUserID == 0)
                                            HHA_SupportUserID = UserID;

                                        ret = await autoGenerateException.AutoRaiseExceptionForSchedules(HHADetails.HHA_ID, HHA_SupportUserID, "MISSED_CHECKOUT", IsQueueEnabledEvents, MISSED_CHECKOUTEligibleSchedules);
                                    }
                                }

                                //Update Last Executedon 
                                string ModifiedFields = "lastQueueExecutedon = '" + DateTime.Now + "' ";
                                //string filters = string.Join(",", IsQueueEnabledEvents.Select(x => x.Event).ToList());
                                string FilterDetails = _repositoryFilterConditions.EqualFilter("IsQueueEnabled", true, FilterConditionDataTypeEnums.boolType).Build();

                                ret = await c_EvvExceptionEventsRepository.UpdateEVVEventInfo(HHADetails.HHA_ID, UserID, FilterDetails, ModifiedFields);

                            }
                        } 
                    } 
                }  
            }
            catch (Exception ex)
            {
                throw ex;
            } 

            return ret;
        }

        public async Task<bool> ClearScheduleEVVExceptions(int HHA, int UserId, int CGTASKID, string TypeOfOperation, string SystemCodes)
        {
            bool ret = true;
            try
            {
                // implement the functionality and call CaregiverTaskEVVExceptionsRepository and use _S_DELETEEVVExceptions
                IEnumerable<CaregiverTaskEVVExceptionsEntity> scheduleExceptionsList = null;
                var Columns = " CaregiverTaskEvvExceptionID, SystemCode, Context, isSystemAutoAdded ";
                var ExceptionFilters = " AND CgTaskID = " + CGTASKID + " AND ISDELETED = 0 ";
                string CodeIDs = "";
                scheduleExceptionsList = await caregiverTaskEVVExceptionsRepository.GetCaregiverTaskEVVExceptions(HHA, UserId, Columns, ExceptionFilters);


                if (scheduleExceptionsList.Count() > 0)
                {
                    switch (TypeOfOperation)
                    {
                        case "UNDOCHECKOUT":
                            var Exceptions = scheduleExceptionsList.Where(x => (x.Context == "CheckOut" || x.Context == "Visit") && x.isSystemAutoAdded == true).Select(x => x.CaregiverTaskEvvExceptionID);
                            CodeIDs = string.Join(",",Exceptions);
                            break;
                        case "UNDOCHECKIN":
                            Exceptions = scheduleExceptionsList.Where(x => x.isSystemAutoAdded == true).Select(x => x.CaregiverTaskEvvExceptionID);
                            CodeIDs = string.Join(",", Exceptions);
                            break;
                        case "VISIT":
                            Exceptions = scheduleExceptionsList.Where(x => x.Context == "Visit" && x.isSystemAutoAdded == true).Select(x => x.CaregiverTaskEvvExceptionID);
                            CodeIDs = string.Join(",", Exceptions);
                            break;
                        default:
                            Exceptions = scheduleExceptionsList.Where(x => x.SystemCode == SystemCodes && x.isSystemAutoAdded == true).Select(x => x.CaregiverTaskEvvExceptionID);
                            CodeIDs = string.Join(",", Exceptions);
                            break;
                    }

                    if (CodeIDs != "")
                    {
                        ret = await caregiverTaskEVVExceptionsRepository.ClearCaregiverTaskEVVExceptions(HHA, UserId, CGTASKID, CodeIDs);
                    }
                }
            }
            catch (Exception e)
            {
                ret = false;
                logger.LogError(e,e.Message);
            }
            return ret;
        }
         
    }
}

﻿
namespace Scheduleservice.Services.Schedules.Service
{
    using AutoMapper;
    using GeoCoordinatePortable;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Interfaces.TaskQueue;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.RepositoryQueryBuilder;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Core.SQLQueryBuilder; 
    using Scheduleservice.Services.Schedules.ValidationHandlers;
    using System;
    using System.Collections.Generic;
    using System.Data; 
    using System.Linq;
    using System.Threading.Tasks;

    public class Schedule : ISchedule
    {
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IMapper _mapper;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository;
        private readonly IConflictExceptionsValidationRules _conflictExceptionsValidationRules;
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEvvExceptionsRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly ICExpiringItemsRepository _cExpiringItemsRepository;
        private readonly IEExpiringItemsRepository _eExpiringItemsRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IHHABranchScheduleConfigurationsRepository _hHABranchScheduleConfigurationsRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IHModificationActivityLog _hModificationActivityLog;
        private readonly IHHAEVVScheduleConfigurationRepository _hHAEVVScheduleConfigurationRepository;
        private readonly IHHAEVVScheduleConfigurationGroupRepository _hHAEVVScheduleConfigurationGroupRepository;
        private readonly IClientTreatmentAddressRepository _clientTreatmentAddressRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IHHAClinicalRepository _hHAClinicalRepository;
        private readonly IHConfigurations2Repository _hConfigurations2Repository;
        private readonly ICaregiverTaskChildSchedulesRepository _caregiverTaskChildSchedulesRepository;
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly ICDSPayerServiceCategoryMasterRepository _cDSPayerServiceCategoryMasterRepository;
        private readonly ICDSClientBudgetLineItemRepository _cDSClientBudgetLineItemRepository;
        private readonly ICDSBudgetLineItemMasterRepository _cDSBudgetLineItemMasterRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContext;
        private readonly IAuthorization _authorization;
        private readonly IPaymentSourcesBranchesRepository _paymentSourcesBranchesRepository;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        private readonly IPaymentsourcesAdditionalRepository _paymentsourcesAdditionalRepository;
        private readonly ICaregiverDisciplinesRepository _caregiverDisciplinesRepository;
        private readonly ICaregiverDisciplineHistoryRepository _caregiverDisciplineHistoryRepository;
        private readonly ICaregiverOtherDisciplinesRepository _caregiverOtherDisciplinesRepository;
        private readonly IHHALineofbusinessRepository _hHALineofbusinessRepository;
        private readonly IEpisodeRepository _episodeRepository;
        private readonly ITaskQueue _taskQueue;
         private readonly IHomeHealthAgencies_BillingPayrollSettingsRepository _homeHealthAgencies_BillingPayrollSettingsRepository;
        private readonly ILogger<Schedule> _logger;
        private readonly IUserRolesRepository _userRolesRepository;
        private readonly IClaimsRepository _claimsRepository;
        private readonly IeChartMasterRepository _eChartMasterRepository;
        private readonly IpChartMasterRepository _pChartMasterRepository;
        private readonly IOasisDatasetsRepository _oasisDatasetsRepository;
        private readonly IVisitChartsRepository _visitChartsRepository;      
        private readonly IGetScheduleBasicDetail _getScheduleBasicDetail;
        private readonly ICaregiverMaximumHoursEffectivePeriods _caregiverMaximumHoursEffectivePeriods;
        private readonly IPaymentSourceAuthorizationRepository _paymentSourceAuthorizationRepository;
        private readonly ICaregiverTaskEVVReasonsRepository _caregiverTaskEVVReasonsRepository;

        public Schedule(ICaregivertasksRepository caregivertasksRepository,
                IRepositoryFilterConditions repositoryFilterConditions,
                IMapper mapper,
                IClientsRepository clientsRepository,
                IConflictExceptionsValidationRules conflictExceptionsValidationRules,
                ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptionsRepository,
                ICaregiversRepository caregiversRepository,
                ICExpiringItemsRepository cExpiringItemsRepository,
                IEExpiringItemsRepository eExpiringItemsRepository,
                IPaymentsourcesRepository paymentsourcesRepository,
                IHHABranchScheduleConfigurationsRepository hHABranchScheduleConfigurationsRepository,
                IServiceCodesRepository serviceCodesRepository,
                IHModificationActivityLog hModificationActivityLog,
                IHHAEVVScheduleConfigurationRepository hHAEVVScheduleConfigurationRepository,
                IHHAEVVScheduleConfigurationGroupRepository hHAEVVScheduleConfigurationGroupRepository,
                IClientTreatmentAddressRepository clientTreatmentAddressRepository,
                IAddressRepository addressRepository,
                IHHAClinicalRepository hHAClinicalRepository,
                IHConfigurations2Repository hConfigurations2Repository,
                ICaregiverTaskChildSchedulesRepository caregiverTaskChildSchedulesRepository,
                ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
                ICDSPayerServiceCategoryMasterRepository cDSPayerServiceCategoryMasterRepository,
                ICDSClientBudgetLineItemRepository cDSClientBudgetLineItemRepository,
                ICDSBudgetLineItemMasterRepository cDSBudgetLineItemMasterRepository,
                IHttpContextAccessor httpContextAccessor, 
                IAuthorization authorization,
                IPaymentSourcesBranchesRepository paymentSourcesBranchesRepository,
                IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository,
                IPaymentsourcesAdditionalRepository paymentsourcesAdditionalRepository,
                ICaregiverDisciplinesRepository caregiverDisciplinesRepository,
                ICaregiverDisciplineHistoryRepository CaregiverDisciplineHistoryRepository,
                ICaregiverOtherDisciplinesRepository caregiverOtherDisciplinesRepository ,
                IHHALineofbusinessRepository hHALineofbusinessRepository,
                IEpisodeRepository episodeRepository,
                ITaskQueue taskQueue, 
                IHomeHealthAgencies_BillingPayrollSettingsRepository homeHealthAgencies_BillingPayrollSettingsRepository,
                 ILogger<Schedule> logger,
               IUserRolesRepository userRolesRepository,
               IClaimsRepository claimsRepository,
               IeChartMasterRepository eChartMasterRepository,
               IpChartMasterRepository pChartMasterRepository,
               IOasisDatasetsRepository oasisDatasetsRepository,
               IVisitChartsRepository visitChartsRepository,
                IGetScheduleBasicDetail getScheduleBasicDetail,
                ICaregiverMaximumHoursEffectivePeriods caregiverMaximumHoursEffectivePeriods,
                IPaymentSourceAuthorizationRepository paymentSourceAuthorizationRepository,
                ICaregiverTaskEVVReasonsRepository caregiverTaskEVVReasonsRepository
            )
        {
            _caregivertasksRepository = caregivertasksRepository;
            _mapper = mapper;
            _repositoryFilterConditions = repositoryFilterConditions;
            _clientsRepository = clientsRepository;
            _conflictExceptionsValidationRules = conflictExceptionsValidationRules;
            _caregiverTaskEvvExceptionsRepository = caregiverTaskEVVExceptionsRepository;
            _caregiversRepository = caregiversRepository;
            _cExpiringItemsRepository = cExpiringItemsRepository;
            _eExpiringItemsRepository = eExpiringItemsRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _hHABranchScheduleConfigurationsRepository = hHABranchScheduleConfigurationsRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _hModificationActivityLog = hModificationActivityLog; 
            this._hHAEVVScheduleConfigurationRepository = hHAEVVScheduleConfigurationRepository;
            this._hHAEVVScheduleConfigurationGroupRepository = hHAEVVScheduleConfigurationGroupRepository;
            this._clientTreatmentAddressRepository = clientTreatmentAddressRepository;
            this._addressRepository = addressRepository;
            this._hHAClinicalRepository = hHAClinicalRepository;
            this._hConfigurations2Repository = hConfigurations2Repository;
            this._caregiverTaskChildSchedulesRepository = caregiverTaskChildSchedulesRepository;
            this._cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            this._cDSPayerServiceCategoryMasterRepository = cDSPayerServiceCategoryMasterRepository;
            this._cDSClientBudgetLineItemRepository = cDSClientBudgetLineItemRepository;
            this._cDSBudgetLineItemMasterRepository = cDSBudgetLineItemMasterRepository;
            this._paymentSourcesBranchesRepository = paymentSourcesBranchesRepository;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            this._paymentsourcesAdditionalRepository = paymentsourcesAdditionalRepository;
            this._caregiverDisciplinesRepository = caregiverDisciplinesRepository;
            this._caregiverDisciplineHistoryRepository = CaregiverDisciplineHistoryRepository;
            this._caregiverOtherDisciplinesRepository = caregiverOtherDisciplinesRepository;

            this._hHALineofbusinessRepository = hHALineofbusinessRepository;
            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
            this. _homeHealthAgencies_BillingPayrollSettingsRepository = homeHealthAgencies_BillingPayrollSettingsRepository;
            this._authorization = authorization;
            this._episodeRepository = episodeRepository;
            this._taskQueue = taskQueue; 
            this._logger = logger;
            this._userRolesRepository = userRolesRepository;
            this._claimsRepository = claimsRepository;
            this._eChartMasterRepository = eChartMasterRepository;
            this._pChartMasterRepository = pChartMasterRepository;
            this._oasisDatasetsRepository = oasisDatasetsRepository;
            this._visitChartsRepository = visitChartsRepository;
            this._getScheduleBasicDetail = getScheduleBasicDetail;
            this._caregiverMaximumHoursEffectivePeriods = caregiverMaximumHoursEffectivePeriods;
            this._paymentSourceAuthorizationRepository = paymentSourceAuthorizationRepository;
            this._caregiverTaskEVVReasonsRepository = caregiverTaskEVVReasonsRepository;
        }

        public async Task<string> GetSchedulesAllList(int HHA, int UserId, string scheduleFiltersjson, string[] Output_columns)
        {

            string schedulesFilter = GetScheduleFilterConditions(scheduleFiltersjson);
            var columns = String.Join(",", Output_columns);
            var schedules = await _caregivertasksRepository.GetAllSchedulesList(HHA, UserId, columns, schedulesFilter);

            var schedules_Output_columns_List = schedules.Select(a => a.DynamicSelectGenerator(Output_columns));

            string scheduleDetails = JsonConvert.SerializeObject(schedules_Output_columns_List);

            return scheduleDetails;
        }

        public async Task<string> GetAllChildSchedulesList(int HHA, int UserId, string scheduleFiltersjson, string[] Output_columns)
        {

            string schedulesFilter = GetChildScheduleFilterConditions(scheduleFiltersjson);
            var columns = String.Join(",", Output_columns);
            var child_schedules = await _caregivertasksRepository.GetAllChildSchedulesList(HHA, UserId, columns, schedulesFilter);

            var child_schedules_Output_columns_List = child_schedules.Select(a => a.DynamicSelectGenerator(Output_columns));

            string childScheduleDetails = JsonConvert.SerializeObject(child_schedules_Output_columns_List);

            return childScheduleDetails;
        }

        public async Task<IEnumerable<ScheduleBasicListDto>> GetSchedulesBasicList(int HHA, int UserId, string scheduleFilters)
        {
            var schedules = await _caregivertasksRepository.GetScheduleBasicList(HHA, UserId, "", scheduleFilters);
            var scheduleFilterDetails = _mapper.Map<IEnumerable<ScheduleBasicListDto>>(schedules);
            return scheduleFilterDetails;
        }

        public string GetScheduleFilterConditions(string scheduleFiltersjson)
        {
            Dictionary<string, string> QueryFilterProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(scheduleFiltersjson);

            var scheduleallFilter = _repositoryFilterConditions;
            foreach (var FilterConditions in QueryFilterProperties)
            {
                string ColumnName = FilterConditions.Key;
                object ColumnValue = FilterConditions.Value;
                switch (ColumnName)
                {
                    case "Client_ID":
                        scheduleallFilter.EqualFilter(ColumnName, ColumnValue, FilterConditionDataTypeEnums.integerType);
                        break;
                    case "startDate":
                        if (!string.IsNullOrEmpty(QueryFilterProperties["startDate"]))
                        {
                            if (QueryFilterProperties.ContainsKey("EndDate") && !String.IsNullOrEmpty(QueryFilterProperties["EndDate"]))
                                scheduleallFilter.BetweenFilter("Planned_Date", QueryFilterProperties["startDate"], QueryFilterProperties["EndDate"], FilterConditionDataTypeEnums.datetimeType);
                            else
                                scheduleallFilter.GreaterThanEqualFilter("Planned_Date", ColumnValue, FilterConditionDataTypeEnums.datetimeType);
                        }
                        break;
                    case "EndDate":
                        if (!string.IsNullOrEmpty(ColumnValue.ToString()))
                        {
                            if((QueryFilterProperties.ContainsKey("startDate") && String.IsNullOrEmpty(QueryFilterProperties["startDate"])) || !QueryFilterProperties.ContainsKey("startDate"))
                                scheduleallFilter.LessThanEqualFilter("PLANNED_DATE", ColumnValue, FilterConditionDataTypeEnums.datetimeType);
                        }
                        break;
                    case "isEVVExportDirty":
                        scheduleallFilter.EqualFilter("IsEVVDirty", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "isBillable":
                        scheduleallFilter.EqualFilter("CanBill", ColumnValue, FilterConditionDataTypeEnums.integerType);
                        break;
                    case "isInvoiced":
                        scheduleallFilter.EqualFilter("IS_BILLED", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "isPayrolled":
                        scheduleallFilter.EqualFilter("IS_PAID", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "ScheduleStatus":
                        scheduleallFilter.InFilter("Status", ColumnValue.ToString(), FilterConditionDataTypeEnums.stringType);
                        break;
                    case "isEvvAggregatorExportRequired":
                        scheduleallFilter.EqualFilter("isEvvAggregatorExportRequired", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "PayerIDs":
                        scheduleallFilter.InFilter("PAYMENT_SOURCE", ColumnValue.ToString(), FilterConditionDataTypeEnums.integerType);
                        break;
                    case "SplitForBilling_Parent":
                        scheduleallFilter.EqualFilter("IsSplitForBilling", ColumnValue, FilterConditionDataTypeEnums.boolType, true);
                        break;
                    case "isEvvschedule":
                        scheduleallFilter.EqualFilter("isEvvschedule", ColumnValue, FilterConditionDataTypeEnums.boolType, true);
                        break;
                }
            }
            var schedulesFilter = scheduleallFilter.Build();

            return schedulesFilter;
        }
        public string GetChildScheduleFilterConditions(string scheduleFiltersjson)
        {
            Dictionary<string, string> QueryFilterProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(scheduleFiltersjson);

            var scheduleallFilter = _repositoryFilterConditions;
            foreach (var FilterConditions in QueryFilterProperties)
            {
                string ColumnName = FilterConditions.Key;
                object ColumnValue = FilterConditions.Value;
                switch (ColumnName)
                {
                    case "Client_ID":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.CLIENT", ColumnValue, FilterConditionDataTypeEnums.integerType);
                        break;
                    case "startDate":
                        if (!string.IsNullOrEmpty(QueryFilterProperties["startDate"]))
                        {
                            if (QueryFilterProperties.ContainsKey("EndDate") && !String.IsNullOrEmpty(QueryFilterProperties["EndDate"]))
                                scheduleallFilter.BetweenFilter("SCHEDULE_DATE", QueryFilterProperties["startDate"], QueryFilterProperties["EndDate"], FilterConditionDataTypeEnums.datetimeType);
                            else
                                scheduleallFilter.GreaterThanEqualFilter("SCHEDULE_DATE", ColumnValue, FilterConditionDataTypeEnums.datetimeType);
                        }
                        break;
                    case "EndDate":
                        if (!string.IsNullOrEmpty(ColumnValue.ToString()))
                        {
                            if((QueryFilterProperties.ContainsKey("startDate") && string.IsNullOrEmpty(QueryFilterProperties["startDate"])) || !QueryFilterProperties.ContainsKey("startDate"))
                                scheduleallFilter.LessThanEqualFilter("SCHEDULE_DATE", ColumnValue, FilterConditionDataTypeEnums.datetimeType);
                        }
                        break;
                    case "isEVVExportDirty":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.isEvvScheduleDirty", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "isBillable":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.CanBill", ColumnValue, FilterConditionDataTypeEnums.integerType);
                        break;
                    case "isInvoiced":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.IS_BILLED", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "isPayrolled":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.IsPayrolled", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "ScheduleStatus":
                        scheduleallFilter.InFilter("CaregiverTasks.[Status]", ColumnValue.ToString(), FilterConditionDataTypeEnums.stringType);
                        break;
                    case "isEvvAggregatorExportRequired":
                        scheduleallFilter.EqualFilter("isEvvAggregatorExportRequired", ColumnValue, FilterConditionDataTypeEnums.boolType);
                        break;
                    case "PayerIDs":
                        scheduleallFilter.InFilter("CaregiverTaskChildSchedules.PAYMENT_SOURCE", ColumnValue.ToString(), FilterConditionDataTypeEnums.integerType);
                        break;
                    case "SplitForBilling_Split":
                        scheduleallFilter.EqualFilter("IsSplitForBilling", ColumnValue, FilterConditionDataTypeEnums.boolType, true);
                        break;
                    case "isEvvschedule":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.isEvvschedule", ColumnValue, FilterConditionDataTypeEnums.boolType, true);
                        break;
                    case "isDeleted":
                        scheduleallFilter.EqualFilter("CaregiverTaskChildSchedules.isDeleted", ColumnValue, FilterConditionDataTypeEnums.boolType, true);
                        break;
                }
            }
            var schedulesFilter = scheduleallFilter.Build();

            return schedulesFilter;
        }

        public async Task<IEnumerable<ValidationErrorInfo>> ValidateApproveSchedule(int HHA, int UserId, int CGTask_ID)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>();

            //get schedules info
            var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", CGTask_ID, FilterConditionDataTypeEnums.integerType)
                            .Build();

            var caregivertaskentity = await _caregivertasksRepository.GetAllSchedulesList(HHA, UserId, "", Filters);

            var caregivertask = caregivertaskentity.FirstOrDefault();

            ScheduleStatusEnums scheduleCompletedStatus = ScheduleStatusEnums.Approved;

            if (caregivertask.STATUS == scheduleCompletedStatus.ToString())
            {
                errorList.Add(new ValidationErrorInfo() { validation_error_message = "Schedule is Approved" });
                return errorList;
            }

            scheduleCompletedStatus = ScheduleStatusEnums.Completed;
            if (caregivertask.STATUS != scheduleCompletedStatus.ToString())
            {
                errorList.Add(new ValidationErrorInfo() { validation_error_message = "Schedule should be completed" });
                return errorList;
            }


            ValidateScheduleRules validateScheduleRules = await _getScheduleBasicDetail.getScheduleDetails(HHA, UserId, caregivertask);

            //get client info
            var Client = await _clientsRepository.GetClientsBasicInfo(HHA, UserId, caregivertask.CLIENT_ID);

            int CAREGIVER = (caregivertask.CAREGIVER ?? 0);
            var scheduleValidationRulesDtos = validateScheduleRules.scheduleValidationRulesDtos;


            var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();
            var DoublBookvalidationHandler = new DoubleBookValidationHandler(doublebookvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiversRepository);

            var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
            var DeviationvalidationHandler = new DeviationValidationHandler(DeviationvalidationRule, HHA, UserId,  _caregivertasksRepository, _repositoryFilterConditions, _hHAScheduleAuthSettingsRepository, _hHABranchScheduleConfigurationsRepository);
            DoublBookvalidationHandler.SetNextValidation(DeviationvalidationHandler);

            var serviceMaxvalidationnRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SERVICEMAXORMINHOURS").FirstOrDefault();
            var serviceMaxvalidationHandler = new ServiceMaxOrMinHoursValidationHandler(serviceMaxvalidationnRule);
            DeviationvalidationHandler.SetNextValidation(serviceMaxvalidationHandler);

            //CLIENTTNOTPREFERREDTHISCLINICIAN
            var clientNotPreferredThisClinicianRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTTNOTPREFERREDTHISCLINICIAN").FirstOrDefault();
            var clientNotPreferredThisClinicianHandler = new ClientNotPreferredThisClinicianValidationHandler(clientNotPreferredThisClinicianRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            serviceMaxvalidationHandler.SetNextValidation(clientNotPreferredThisClinicianHandler);

            //CLINICIANONVACATION
            var clinicianOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANONVACATION").FirstOrDefault();
            var clinicianOnVacationHandler = new ClinicianOnVacationValidationHandler(clinicianOnVacationRule, HHA, UserId, _caregiversRepository, _repositoryFilterConditions);
            clientNotPreferredThisClinicianHandler.SetNextValidation(clinicianOnVacationHandler);

            //CLIENTONVACATION
            var clientOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTONVACATION").FirstOrDefault();
            var clientOnVacationHandler = new ClientOnVacationValidationHandler(clientOnVacationRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            clinicianOnVacationHandler.SetNextValidation(clientOnVacationHandler);

            //CLINICIANRENEWABLEITEMSEXPIRED
            var clinicianRenewableItemsExpiredRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANRENEWABLEITEMSEXPIRED").FirstOrDefault();
            var clinicianRenewableItemsExpiredHandler = new ClinicianRenewableItemsExpiredValidationHandler(clinicianRenewableItemsExpiredRule, HHA, UserId, _cExpiringItemsRepository, _eExpiringItemsRepository, _repositoryFilterConditions);
            clientOnVacationHandler.SetNextValidation(clinicianRenewableItemsExpiredHandler);

            //CLIENTMAXHOURS
            var clientMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();
            var clientMaxHoursHandler = new ClientMaxHoursValidationHandler(clientMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions);
            clinicianRenewableItemsExpiredHandler.SetNextValidation(clientMaxHoursHandler);

            //CLINICIANMAXHOURS
            var clinicianMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();
            var clinicianMaxHoursHandler = new ClinicianMaxHoursValidationHandler(clinicianMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiverMaximumHoursEffectivePeriods);
            clientMaxHoursHandler.SetNextValidation(clinicianMaxHoursHandler);

            //OVERTIME
            var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();
            var overtimeHandler = new OvertimeValidationHandler(overtimeRule);
            clinicianMaxHoursHandler.SetNextValidation(overtimeHandler);

            //GRACEPERIOD

            //VF
            var visitFrequencyRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "VF").FirstOrDefault();
            var visitFrequencyHandler = new VisitFrequencyValidationHandler(visitFrequencyRule, HHA, UserId, _caregivertasksRepository);
            overtimeHandler.SetNextValidation(visitFrequencyHandler);

            //AUTHORIZATION
            var authorizationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "AUTHORIZATION").FirstOrDefault();
            var authorizationHandler = new AuthorizationValidationHandler(authorizationRule, HHA, UserId, _caregivertasksRepository);
            visitFrequencyHandler.SetNextValidation(authorizationHandler);

            //SCHEDULEOVER24HOURS
            var scheduleover24HoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SCHEDULEOVER24HOURS").FirstOrDefault();
            var scheduleOver24HourValidationHandler = new ScheduleOver24HourValidationHandler(scheduleover24HoursRule, HHA, UserId, _paymentsourcesRepository);
            authorizationHandler.SetNextValidation(scheduleOver24HourValidationHandler);

            //EVVVALIDATION
            var evvCodeValidationRule = scheduleValidationRulesDtos.Where(x => (x.ValidationType == "NO_ATTESTATION") || (x.ValidationType== "NO_REASONCODE") || (x.ValidationType== "NO_ACTIONCODE")).FirstOrDefault();
            var eVVCodesValidationHandler = new EVVCodesValidationHandler(evvCodeValidationRule, HHA, UserId,_repositoryFilterConditions, _caregiverTaskEVVReasonsRepository);
            scheduleOver24HourValidationHandler.SetNextValidation(eVVCodesValidationHandler);

            errorList = DoublBookvalidationHandler.HandleValidation(validateScheduleRules);

            return errorList;

        }

        public async Task<int> ApproveSchedule(int HHA, int UserId, int CGTask_ID)
        {
            int result = 1;
            var caregivertask = await _caregivertasksRepository.GetScheduleBasicInfo(HHA, UserId, CGTask_ID);
            var payer = await _paymentsourcesRepository.GetPaymentSourceBasicInfo(HHA, UserId, caregivertask.PAYMENT_SOURCE);
            var client = await _clientsRepository.GetClientsBasicInfo(HHA, UserId, caregivertask.CLIENT_ID);
            var HhhBranchConfig = await _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(HHA, UserId, client.HHA_BRANCH_ID);
            if (!payer.SplitOvernightSchedules)
            {
                if (HhhBranchConfig.SplitOvernightSchedulesForPayroll)
                    payer.SplitOvernightSchedules = true;
            }

            if (payer.SplitOvernightSchedules)
            {
                var splitedTaskDetails = caregivertask.CGTASK_ID + "¥" + (caregivertask.ACCTUAL_START_TIME == null ? "" : ((DateTime)caregivertask.ACCTUAL_START_TIME).ToString("HH:mm")) + "¥" + (caregivertask.ACCTUAL_END_TIME == null ? "" : ((DateTime)caregivertask.ACCTUAL_END_TIME).ToString("HH:mm")) + '¥' + caregivertask.EDITED_HOURS + '¥' + caregivertask.EDITED_HOURS_PAYABLE + "¥" + "1" + "¥" + "" + "$";
                if (!string.IsNullOrEmpty(splitedTaskDetails))
                {
                    result = await _caregivertasksRepository.ReConstructChildSchedules(HHA, UserId, splitedTaskDetails, false);
                }
            }

            if (result == 1)
            {
                var TravelTime = caregivertask.travelTimeHours ?? 0;
                var DocTime = caregivertask.documentationTimeMins ?? 0;

                if (HhhBranchConfig.SetTravelTimeRuleasPayrolledHours)
                {
                    string _hours = caregivertask.EDITED_HOURS_PAYABLE == null ? caregivertask.EDITED_HOURS : caregivertask.EDITED_HOURS_PAYABLE;
                    if (!string.IsNullOrEmpty(_hours))
                    {
                        var hours = Convert.ToInt32(_hours.Split(':')[0]);
                        var minutes = Convert.ToInt32(_hours.Split(':')[1]);
                        TravelTime = hours + minutes; ;
                    }

                }

                if (TravelTime != 0 || DocTime != 0)
                {
                    var Details = caregivertask.CGTASK_ID + "¥" + (caregivertask.ACCTUAL_START_TIME == null ? "" : caregivertask.ACCTUAL_START_TIME.ToString()) + "¥" + (caregivertask.ACCTUAL_END_TIME == null ? "" : caregivertask.ACCTUAL_END_TIME.ToString()) + "¥" + caregivertask.EDITED_HOURS + "¥" + caregivertask.EDITED_HOURS_PAYABLE + "¥" + "1" + "¥" + caregivertask.PLANNED_DATE.ToString("MM/dd/yyyy") + "¥" + TravelTime + "¥" + DocTime + "¥" + caregivertask.CAREGIVER + "¥" + caregivertask.SERVICECODE_ID + "$";

                    await _caregivertasksRepository.AddEditDeleteTrivelDocumentTimeMiscVisit(HHA, UserId, Details);
                }

            }

            if (result == 1 && HhhBranchConfig.EnabledautoApprovepChart)
            {
                var servicecord = await _serviceCodesRepository.GetServiceCodeBasicInfo(HHA, UserId, caregivertask.SERVICECODE_ID);

                if (HhhBranchConfig.autoApprovepChartDisciplines.Contains(servicecord.GroupID + ","))
                {
                    var formsColumns = "StartOfCareType";
                    var formsFilter = _repositoryFilterConditions.EqualFilter("_C_Forms.HHA_FormId", servicecord.FormID, FilterConditionDataTypeEnums.integerType).Build();
                    var forms = (await _serviceCodesRepository.GetUA_FormsEntities(HHA, UserId, formsColumns, formsFilter)).FirstOrDefault();

                    if (forms.StartOfCareType == 0)
                    {
                        await _caregivertasksRepository.pChartSaveSubmit(HHA, UserId, caregivertask.CGTASK_ID, 1, 1, 0, caregivertask.ACCTUAL_START_TIME == null ? "" : ((DateTime)caregivertask.ACCTUAL_START_TIME).ToString("HH:mm"), caregivertask.ACCTUAL_END_TIME == null ? "" : ((DateTime)caregivertask.ACCTUAL_END_TIME).ToString("HH:mm"), caregivertask.MILES == null ? "0" : Convert.ToString(caregivertask.MILES), caregivertask.EDITED_HOURS, caregivertask.PlaceOfService == null ? 1 : (int)caregivertask.PlaceOfService, caregivertask.CAREGIVER);
                    }
                }
            }
            var AuditDesc = string.Empty;
            if (result == 1)
            {
                int ret = 0;

                var scheduleallUpdateColumn = new RepositoryUpdateColumn();
                ScheduleStatusEnums scheduleStatus = ScheduleStatusEnums.Approved;

                var updatedCaregivertasksColumns = scheduleallUpdateColumn.UpdateColumn("STATUS", scheduleStatus.ToString(), FilterConditionDataTypeEnums.stringType).Build();

                ret = await _caregivertasksRepository.UpdateScheduleBasic(HHA, UserId, CGTask_ID, updatedCaregivertasksColumns);

                AuditDesc = "Modified " + caregivertask.PLANNED_DATE.ToString("MM/dd/yyyy") + " schedule status from 'Completed' to 'Approved'";

                if (ret == 0)
                {
                    var updatedCaregiverTaskAdditional2Columns
                            = scheduleallUpdateColumn.UpdateColumn("ApprovedBy", Convert.ToString(UserId), FilterConditionDataTypeEnums.integerType)
                                        .UpdateColumn("ApprovedOn", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), FilterConditionDataTypeEnums.datetimeType)
                                        .Build();
                    ret = await _caregivertasksRepository.UpdateScheduleAdditional2(HHA, UserId, CGTask_ID.ToString(), updatedCaregiverTaskAdditional2Columns);
                }

                if (ret == 0 && HhhBranchConfig.PayrollRule == 2)
                {
                    var updatedCaregiverTaskAdditionalColumns = scheduleallUpdateColumn.UpdateColumn("ApprovedForPayroll", "true", FilterConditionDataTypeEnums.boolType).Build();
                    ret = await _caregivertasksRepository.UpdateScheduleAdditional(HHA, UserId, CGTask_ID, updatedCaregiverTaskAdditionalColumns);
                    ret = await _caregivertasksRepository.UpdateScheduleAdditional(HHA, UserId, CGTask_ID, updatedCaregiverTaskAdditionalColumns);
                    AuditDesc += " Approved for payroll from 'No' to 'Yes'";
                }
                if (ret == 0 && !string.IsNullOrEmpty(AuditDesc))
                {

                    dynamic AuditInfo = new HModificationActivityLogEntity();
                    AuditInfo.ActivityName = "Modify Schedule";
                    AuditInfo.ActivityType = 6;
                    AuditInfo.RecordIdentityName = "CGTASK_ID";
                    AuditInfo.RecordIdentityValue = CGTask_ID;
                    AuditInfo.isSingleActivity = true;
                    AuditInfo.Category = "Schedule";
                    AuditInfo.Description = AuditDesc;
                    AuditInfo.UserDefined2 = "api/schedules/v1/schedules/{scheduleid}";

                    string AuditInfoJSON = JsonConvert.SerializeObject(AuditInfo);
                    ret = await _hModificationActivityLog.AddAuditData(HHA, UserId, AuditInfoJSON);
                }

                if (ret != 0)
                {
                    result = 0;
                }
            }

            return result; 
        } 

        //model for CaregivertasksEntity => CaregivertasksModel
        public async Task<int> ModifyScheduleAsync(int HHA, int UserId, int CgtaskID, EditScheduleProperties editSchedulePropertiesmodel, CaregivertasksEntity oldcaregivertasksEntity, bool BlockCheckinValidation)
        {
            int result = 1;

            //get payer level setting
            //var payer = await _paymentsourcesRepository.GetPaymentSourceBasicInfo(HHA, UserId, editSchedulePropertiesmodel.PAYMENT_SOURCE ?? 0);
            //editSchedulePropertiesmodel.Has_Child_Schedules = oldcaregivertasksEntity.Has_Child_Schedules;

            bool requireSplit = false;
            if (oldcaregivertasksEntity.Has_Child_Schedules == true
                 && (editSchedulePropertiesmodel.STATUS == ScheduleStatusEnums.Completed.ToString()
                    ||
                     editSchedulePropertiesmodel.STATUS == ScheduleStatusEnums.Approved.ToString()
                    )
                )
            {
                requireSplit = true;
            }

            //to validate overage hours
            //create one more function in this page //public task<string> calculateoveragerequiredflag()
            string isoverhoursrequired = await CalculateOverageHoursRequired(HHA, UserId,editSchedulePropertiesmodel, oldcaregivertasksEntity);
            
            if(isoverhoursrequired == "1")
                requireSplit = true;

            if (requireSplit && editSchedulePropertiesmodel.ACCTUAL_END_TIME != null)
            {
                var splitedTaskDetails = CgtaskID + "¥" + (editSchedulePropertiesmodel.ACCTUAL_START_TIME == null ? "" : ((DateTime)editSchedulePropertiesmodel.ACCTUAL_START_TIME).ToTimeAMPMFormat()) + "¥" + (editSchedulePropertiesmodel.ACCTUAL_END_TIME == null ? "" : ((DateTime)editSchedulePropertiesmodel.ACCTUAL_END_TIME).ToTimeAMPMFormat()) + '¥' + editSchedulePropertiesmodel.EDITED_HOURS + '¥' + editSchedulePropertiesmodel.EDITED_HOURS_PAYABLE + "¥" + "0" + "¥" + "" +"¥" + "0" + "¥" + isoverhoursrequired + "$";
                if (!string.IsNullOrEmpty(splitedTaskDetails))
                {
                    result = await _caregivertasksRepository.ReConstructChildSchedules(HHA, UserId, splitedTaskDetails, BlockCheckinValidation);
                }

                if (result == 1)
                    result = 0;
            }
            else
                result = 0;

            if (result == 0)
            {
                var updatedCaregivertasksColumns = new BuildUpdateQueryBasedOnEntity<CaregivertaskBasicEntity>().GetBuildUpdateQueryStatementBasedOnEntity(editSchedulePropertiesmodel, oldcaregivertasksEntity);

                var updatedCaregivertasksAdditionalColumns = new BuildUpdateQueryBasedOnEntity<CaregivertaskAdditionalEntity>().GetBuildUpdateQueryStatementBasedOnEntity(editSchedulePropertiesmodel, oldcaregivertasksEntity);

                var updatedCaregivertasksAdditional2Columns = new BuildUpdateQueryBasedOnEntity<CaregivertaskAdditional2Entity>().GetBuildUpdateQueryStatementBasedOnEntity(editSchedulePropertiesmodel, oldcaregivertasksEntity);


                if (!string.IsNullOrEmpty(updatedCaregivertasksColumns.columns) || !string.IsNullOrEmpty(updatedCaregivertasksAdditionalColumns.columns) || !string.IsNullOrEmpty(updatedCaregivertasksAdditional2Columns.columns))
                {
                    result = await _caregivertasksRepository.UpdateScheduleInfo(HHA, UserId, CgtaskID, updatedCaregivertasksColumns.columns, updatedCaregivertasksAdditionalColumns.columns, updatedCaregivertasksAdditional2Columns.columns);
                }

                if (result == 0)
                {
                     


                    dynamic AuditInfo = new HModificationActivityLogEntity();
                    AuditInfo.ActivityName = "Modify Schedule";
                    AuditInfo.ActivityType = 6;
                    AuditInfo.RecordIdentityName = "CGTASK_ID";
                    AuditInfo.RecordIdentityValue = CgtaskID;
                    AuditInfo.isSingleActivity = true;
                    AuditInfo.Category = "Schedule";
                    AuditInfo.Description = "Modified Schedule " + updatedCaregivertasksColumns.audit;
                    if (!string.IsNullOrEmpty(updatedCaregivertasksAdditionalColumns.audit))
                    {
                        if (updatedCaregivertasksColumns.audit.Length > 0)
                            AuditInfo.Description += " and " + updatedCaregivertasksAdditionalColumns.audit;
                        else
                            AuditInfo.Description += updatedCaregivertasksAdditionalColumns.audit;
                    }

                    if (!string.IsNullOrEmpty(updatedCaregivertasksAdditional2Columns.audit))
                    {
                        if (updatedCaregivertasksColumns.audit.Length > 0 || updatedCaregivertasksAdditionalColumns.audit.Length > 0)
                            AuditInfo.Description += " and " + updatedCaregivertasksAdditional2Columns.audit;
                        else
                            AuditInfo.Description += updatedCaregivertasksAdditional2Columns.audit;
                    }

                    AuditInfo.UserDefined2 = "api/schedules/v1/schedules";

                    string AuditInfoJSON = JsonConvert.SerializeObject(AuditInfo);
                    result = await _hModificationActivityLog.AddAuditData(HHA, UserId, AuditInfoJSON);
                }
            }
            return result;
        }
          
        public async Task<IEnumerable<ValidationErrorInfo>> ValidateScheduleCheckInOutAsync(int HHA, int UserId, CaregivertasksEntity caregivertask,ValidateScheduleRules validateScheduleRules)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>(); 

             //get client info
            var Client = validateScheduleRules.clientsEntity;
            int HHABranchId = Client.HHA_BRANCH_ID;

            //var scheduleValidationRulesDtos = await _conflictExceptionsValidationRules.GetConflictExceptionsValidationRules(HHA, UserId, validateScheduleRules);
            var scheduleValidationRulesDtos = validateScheduleRules.scheduleValidationRulesDtos;
            //DOUBLEBOOK
            var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();
            var DoublBookvalidationHandler = new DoubleBookValidationHandler(doublebookvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiversRepository);
            errorList.AddRange(DoublBookvalidationHandler.HandleValidation(validateScheduleRules));
            //DEVIATION
            var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
            var DeviationvalidationHandler = new DeviationValidationHandler(DeviationvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _hHAScheduleAuthSettingsRepository, _hHABranchScheduleConfigurationsRepository);            
            errorList.AddRange(DeviationvalidationHandler.HandleValidation(validateScheduleRules));

            //SERVICEMAXHOURS
            var serviceMaxvalidationnRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SERVICEMAXORMINHOURS").FirstOrDefault();
            var serviceMaxvalidationHandler = new ServiceMaxOrMinHoursValidationHandler(serviceMaxvalidationnRule);
            errorList.AddRange(serviceMaxvalidationHandler.HandleValidation(validateScheduleRules));

            //CLINICIANONVACATION
            var clinicianOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANONVACATION").FirstOrDefault();
            var clinicianOnVacationHandler = new ClinicianOnVacationValidationHandler(clinicianOnVacationRule, HHA, UserId, _caregiversRepository, _repositoryFilterConditions);
            errorList.AddRange(clinicianOnVacationHandler.HandleValidation(validateScheduleRules));

            //CLIENTONVACATION
            var clientOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTONVACATION").FirstOrDefault();
            var clientOnVacationHandler = new ClientOnVacationValidationHandler(clientOnVacationRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            errorList.AddRange(clientOnVacationHandler.HandleValidation(validateScheduleRules));

            //CLINICIANRENEWABLEITEMSEXPIRED
            var clinicianRenewableItemsExpiredRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANRENEWABLEITEMSEXPIRED").FirstOrDefault();
            var clinicianRenewableItemsExpiredHandler = new ClinicianRenewableItemsExpiredValidationHandler(clinicianRenewableItemsExpiredRule, HHA, UserId, _cExpiringItemsRepository, _eExpiringItemsRepository, _repositoryFilterConditions);
            errorList.AddRange(clinicianRenewableItemsExpiredHandler.HandleValidation(validateScheduleRules));


            //CLIENTTNOTPREFERREDTHISCLINICIAN
            var clientNotPreferredThisClinicianRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTTNOTPREFERREDTHISCLINICIAN").FirstOrDefault();
            var clientNotPreferredThisClinicianHandler = new ClientNotPreferredThisClinicianValidationHandler(clientNotPreferredThisClinicianRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            errorList.AddRange(clientNotPreferredThisClinicianHandler.HandleValidation(validateScheduleRules));


            //VF
            var visitFrequencyRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "VF").FirstOrDefault();
            var visitFrequencyHandler = new VisitFrequencyValidationHandler(visitFrequencyRule, HHA, UserId, _caregivertasksRepository);
            errorList.AddRange(visitFrequencyHandler.HandleValidation(validateScheduleRules));
             

            //OVERTIME
            var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();
            var overtimeHandler = new OvertimeValidationHandler(overtimeRule);
            errorList.AddRange(overtimeHandler.HandleValidation(validateScheduleRules));

            //ScheduleOver24HourValidation
            var over24hoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SCHEDULEOVER24HOURS").FirstOrDefault();
            var over24hoursHandler = new ScheduleOver24HourValidationHandler(over24hoursRule, HHA, UserId, _paymentsourcesRepository);
            errorList.AddRange(over24hoursHandler.HandleValidation(validateScheduleRules));

            //OUTSIDEFENCE
            var geolocationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "GEOLOCATION").FirstOrDefault();
            var geolocationHandler = new GeoFencingValidationHandler(geolocationRule, HHA, UserId, _repositoryFilterConditions, _hHAEVVScheduleConfigurationRepository, _hHAEVVScheduleConfigurationGroupRepository, _clientTreatmentAddressRepository, _addressRepository);
            errorList.AddRange(geolocationHandler.HandleValidation(validateScheduleRules));

            //CDS Validation
            var cdsValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CDSVALIDATION").FirstOrDefault();
            var cdsValidationHandler = new CDSValidationHandler(cdsValidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _hHAClinicalRepository, _clientsRepository, _hConfigurations2Repository, _caregiverTaskChildSchedulesRepository, _cDSPlanYearServicesRepository, _cDSPayerServiceCategoryMasterRepository, _cDSClientBudgetLineItemRepository, _cDSBudgetLineItemMasterRepository);
            errorList.AddRange(cdsValidationHandler.HandleValidation(validateScheduleRules));

            //FutureDateValidation
            var futuredateValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "FUTUREDATE").FirstOrDefault();
            var futuredateValidationHandler = new FutureDateValidationHandler(futuredateValidationRule, HHA, UserId, _clientsRepository, _hConfigurations2Repository, _repositoryFilterConditions);
            errorList.AddRange(futuredateValidationHandler.HandleValidation(validateScheduleRules));

            //PastVisitValidation
            var pastVisitValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "PASTVISIT").FirstOrDefault();
            var pastvisitValidationHandler = new PastVisitValidationHandler(pastVisitValidationRule, HHA, UserId, _repositoryFilterConditions, _hHABranchScheduleConfigurationsRepository,_caregivertasksRepository, HHABranchId);
            errorList.AddRange(pastvisitValidationHandler.HandleValidation(caregivertask)); 

            //CLIENTMAXHOURS
            var clientMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();
            var clientMaxHoursHandler = new ClientMaxHoursValidationHandler(clientMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions);
            errorList.AddRange(clientMaxHoursHandler.HandleValidation(validateScheduleRules));

            //CLINICIANMAXHOURS
            var clinicianMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();
            var clinicianMaxHoursHandler = new ClinicianMaxHoursValidationHandler(clinicianMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiverMaximumHoursEffectivePeriods);
            errorList.AddRange(clinicianMaxHoursHandler.HandleValidation(validateScheduleRules));

            //AUTHORIZATION
            var authorizationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "AUTHORIZATION").FirstOrDefault();
            var authorizationHandler = new AuthorizationValidationHandler_SP(authorizationRule, HHA, UserId, _authorization);
            errorList.AddRange(authorizationHandler.HandleValidation(validateScheduleRules));

            return errorList;
        }

        public async Task<IEnumerable<ValidationErrorInfo>> ValidateCreateScheduleasync(int HHA, int UserId,
            ValidateScheduleRules validationCreateSchedule)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>();
         
            //get client info
            var Client = validationCreateSchedule.clientsEntity;
            var caregivertask = validationCreateSchedule.caregiverTaskEntity; 
                //await _clientsRepository.GetClientsBasicInfo(HHA, UserId, validationCreateSchedule.caregivertask.CLIENT_ID);
            int HHABranchId = Client.HHA_BRANCH_ID;
            var scheduleValidationRulesDtos = validationCreateSchedule.scheduleValidationRulesDtos;
            //hha branch configurations

            //DOUBLEBOOK
            var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();            
            var DoublBookvalidationHandler = new DoubleBookValidationHandler(doublebookvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiversRepository);
            errorList.AddRange(DoublBookvalidationHandler.HandleValidation(validationCreateSchedule));
           
            //DEVIATION

            var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
            var DeviationvalidationHandler = new DeviationValidationHandler(DeviationvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _hHAScheduleAuthSettingsRepository, _hHABranchScheduleConfigurationsRepository);
            errorList.AddRange(DeviationvalidationHandler.HandleValidation(validationCreateSchedule));
            
            //SERVICEMAXHOURS
            var serviceMaxvalidationnRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SERVICEMAXORMINHOURS").FirstOrDefault();
            var serviceMaxvalidationHandler = new ServiceMaxOrMinHoursValidationHandler(serviceMaxvalidationnRule);
            errorList.AddRange(serviceMaxvalidationHandler.HandleValidation(validationCreateSchedule));
           
            //CLINICIANONVACATION
            var clinicianOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANONVACATION").FirstOrDefault();
            var clinicianOnVacationHandler = new ClinicianOnVacationValidationHandler(clinicianOnVacationRule, HHA, UserId, _caregiversRepository, _repositoryFilterConditions);
            errorList.AddRange(clinicianOnVacationHandler.HandleValidation(validationCreateSchedule));
            
            //CLIENTONVACATION
            var clientOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTONVACATION").FirstOrDefault();
            var clientOnVacationHandler = new ClientOnVacationValidationHandler(clientOnVacationRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            errorList.AddRange(clientOnVacationHandler.HandleValidation(validationCreateSchedule));
           
            //CLINICIANRENEWABLEITEMSEXPIRED
            var clinicianRenewableItemsExpiredRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANRENEWABLEITEMSEXPIRED").FirstOrDefault();
            var clinicianRenewableItemsExpiredHandler = new ClinicianRenewableItemsExpiredValidationHandler(clinicianRenewableItemsExpiredRule, HHA, UserId, _cExpiringItemsRepository, _eExpiringItemsRepository, _repositoryFilterConditions);
            errorList.AddRange(clinicianRenewableItemsExpiredHandler.HandleValidation(validationCreateSchedule));
             
            //VF
            var visitFrequencyRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "VF").FirstOrDefault();
            var visitFrequencyHandler = new VisitFrequencyValidationHandler(visitFrequencyRule, HHA, UserId, _caregivertasksRepository);
            errorList.AddRange(visitFrequencyHandler.HandleValidation(validationCreateSchedule));
             
            //AUTHORIZATION
            var authorizationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "AUTHORIZATION").FirstOrDefault();
            var authorizationHandler = new AuthorizationValidationHandler_SP(authorizationRule, HHA, UserId, _authorization);
            errorList.AddRange(authorizationHandler.HandleValidation(validationCreateSchedule));
            
            //OVERTIME
            var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();
            var overtimeHandler = new OvertimeValidationHandler(overtimeRule);
            errorList.AddRange(overtimeHandler.HandleValidation(validationCreateSchedule));
           
            //ScheduleOver24HourValidation
            var over24hoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SCHEDULEOVER24HOURS").FirstOrDefault();
            var over24hoursHandler = new ScheduleOver24HourValidationHandler(over24hoursRule, HHA, UserId, _paymentsourcesRepository);
            errorList.AddRange(over24hoursHandler.HandleValidation(validationCreateSchedule));
            
          
            //CDS Validation
            var cdsValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CDSVALIDATION").FirstOrDefault();
            var cdsValidationHandler = new CDSValidationHandler(cdsValidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _hHAClinicalRepository, _clientsRepository, _hConfigurations2Repository, _caregiverTaskChildSchedulesRepository, _cDSPlanYearServicesRepository, _cDSPayerServiceCategoryMasterRepository, _cDSClientBudgetLineItemRepository, _cDSBudgetLineItemMasterRepository);
            errorList.AddRange(cdsValidationHandler.HandleValidation(validationCreateSchedule));
           
            //FutureDateValidation
            var futuredateValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "FUTUREDATE").FirstOrDefault();
            var futuredateValidationHandler = new FutureDateValidationHandler(futuredateValidationRule, HHA, UserId, _clientsRepository, _hConfigurations2Repository, _repositoryFilterConditions);
            errorList.AddRange(futuredateValidationHandler.HandleValidation(validationCreateSchedule));
           
            //PastVisitValidation
            var pastVisitValidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "PASTVISIT").FirstOrDefault();
            var pastvisitValidationHandler = new PastVisitValidationHandler(pastVisitValidationRule, HHA, UserId, _repositoryFilterConditions, _hHABranchScheduleConfigurationsRepository, _caregivertasksRepository, HHABranchId);
            errorList.AddRange(pastvisitValidationHandler.HandleValidation(caregivertask));
            //CLIENTMAXHOURS
            var clientMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();
            var clientMaxHoursHandler = new ClientMaxHoursValidationHandler(clientMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions);
            errorList.AddRange(clientMaxHoursHandler.HandleValidation(validationCreateSchedule));

            //CLINICIANMAXHOURS
            var clinicianMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();
            var clinicianMaxHoursHandler = new ClinicianMaxHoursValidationHandler(clinicianMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiverMaximumHoursEffectivePeriods);
            errorList.AddRange(clinicianMaxHoursHandler.HandleValidation(validationCreateSchedule));
            return errorList;
        }


        public async Task<int> GetScheduleEVVVendorVersionMasterID(int HHA, int UserId, CaregivertasksEntity caregivertasksEntity, int HHABranchID, bool ConsiderSettings)
        {



            var HHAEVVScheduleConfigurationFilter = _repositoryFilterConditions.EqualFilter("HHABranchID", HHABranchID, FilterConditionDataTypeEnums.integerType)
                                                        .EqualFilter("EvvScheduleConfigurationGroup.Interface", "Online", FilterConditionDataTypeEnums.stringType)
                                                        .Build();
            var _hHAEVVScheduleConfigurations = await _hHAEVVScheduleConfigurationRepository.GetHHAEVVScheduleConfigurationSettings(HHA, UserId, "EvvScheduleConfigurationGroup.EvvScheduleConfigurationGroupID", HHAEVVScheduleConfigurationFilter);

            int EvvScheduleConfigurationGroupID = _hHAEVVScheduleConfigurations.Select(x => x.EvvScheduleConfigurationGroupID).FirstOrDefault();

            var HHAEVVScheduleConfigurationGroupFilter = _repositoryFilterConditions.EqualFilter("EvvScheduleConfigurationGroupID", EvvScheduleConfigurationGroupID, FilterConditionDataTypeEnums.integerType)
                                                                     .Build();

            var _hHAEVVScheduleConfigurationGroupsetting = await _hHAEVVScheduleConfigurationGroupRepository.GetHHAEVVScheduleConfigurationGroupSettings(HHA, UserId, "EvvScheduleConfigurationGroupID, isApplicableToSecondaryEvvSchedule, isApplicableToPrimaryEvvSchedule,PayerDisciplinesJSON ", HHAEVVScheduleConfigurationGroupFilter);


            var serviceFilter = _repositoryFilterConditions.EqualFilter("SERVICE_CODE_ID", caregivertasksEntity.SERVICECODE_ID, FilterConditionDataTypeEnums.integerType).Build();

            var serviceColumns = "ServiceCodes.GroupID";
            var serviceinfo = await _serviceCodesRepository.GetServiceCodes(HHA, UserId, serviceColumns, serviceFilter);

            //get payer profile info 
            string payercolumns = "PrimaryEvvVendorID,EvvAggregatorVendorVersionMasterID,IsPrimaryEVV";
            string payerfilters = _repositoryFilterConditions.EqualFilter(" PaymentSources.PAYMENT_SOURCE_ID ", caregivertasksEntity.PAYMENT_SOURCE, FilterConditionDataTypeEnums.integerType).Build();
            var PayerEVVDetails = await _paymentsourcesRepository.GetPaymentsources(HHA, UserId, payercolumns, payerfilters);

            var paymentsourcesEntity = PayerEVVDetails.FirstOrDefault();

            //get payer branch info
            var Payerbranchinfo = await _paymentSourcesBranchesRepository.GetPayerBranchInfo(HHA, UserId, caregivertasksEntity.PAYMENT_SOURCE, HHABranchID, "EvvAggregatorVendorVersionMasterID,PrimaryEvvVendorID,IsPrimaryEVV,EnableEvv");

            int ServiceGroupID = Convert.ToInt32(serviceinfo.FirstOrDefault().GroupID);


            int EVVVendorVersionMasterID = Common.ScheduleEVVVendorVersionMasterID(ServiceGroupID, paymentsourcesEntity, Payerbranchinfo, _hHAEVVScheduleConfigurationGroupsetting, ConsiderSettings);
             

            return EVVVendorVersionMasterID;
        }


         
        public async Task<EditScheduleProperties> GetEditSchedulePropertiesModel(int HHA, int User, EditScheduleProperties request, CaregivertasksEntity oldscheduleinfo, ClientsEntity clientsEntity, bool RequireGeoLocationforFOB = false)
        {
            EditScheduleProperties updatededitScheduleProperties = new EditScheduleProperties();

            updatededitScheduleProperties.CanBill = oldscheduleinfo.CanBill;
            updatededitScheduleProperties.IS_AUTH_MANDATORY = oldscheduleinfo.IS_AUTH_MANDATORY;
            updatededitScheduleProperties.isAuthorized = oldscheduleinfo.isAuthorized;
            updatededitScheduleProperties.Units = oldscheduleinfo.Units;
            updatededitScheduleProperties.TotalHours = request.TotalHours;
            updatededitScheduleProperties.SERVICECODE_ID = oldscheduleinfo.SERVICECODE_ID;
            updatededitScheduleProperties.PAYMENT_SOURCE = oldscheduleinfo.PAYMENT_SOURCE;
            updatededitScheduleProperties.EDITED_HOURS = oldscheduleinfo.EDITED_HOURS;
            updatededitScheduleProperties.EDITED_HOURS_PAYABLE = oldscheduleinfo.EDITED_HOURS_PAYABLE;

            //Get Checkin date time 
            updatededitScheduleProperties.ACCTUAL_START_TIME = request.ACCTUAL_START_TIME;
            updatededitScheduleProperties.PLANNED_DATE = request.PLANNED_DATE;
            updatededitScheduleProperties.ACCTUAL_DATE = request.PLANNED_DATE;

            //update checkout 
            updatededitScheduleProperties.ACCTUAL_END_TIME = request.ACCTUAL_END_TIME;


            //status
            updatededitScheduleProperties.STATUS = request.STATUS;

            updatededitScheduleProperties.CLIENT_ID = oldscheduleinfo.CLIENT_ID;

            var columns = "ROUNDOFF_Direction, ROUNDOFF_Minutes, Round_ZeroUnitScheduleToOne ";
            var payeradditionalinfo = await _paymentsourcesAdditionalRepository.GetPayerAdditionalInfo(HHA, User, oldscheduleinfo.PAYMENT_SOURCE, columns);

            //total hours 
            if (updatededitScheduleProperties.ACCTUAL_END_TIME != null)
            {
                updatededitScheduleProperties.TotalHours = Common.GetVisitActualHours(Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_START_TIME), Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_END_TIME));

                //Edited hours  
                updatededitScheduleProperties.EDITED_HOURS = request.EDITED_HOURS;
                if (string.IsNullOrEmpty(request.EDITED_HOURS))
                    updatededitScheduleProperties.EDITED_HOURS = Common.GetVisitEditedHours(Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_START_TIME), Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_END_TIME), 0, 0);

                //payable hours  
                updatededitScheduleProperties.EDITED_HOURS_PAYABLE = request.EDITED_HOURS_PAYABLE;
                if (string.IsNullOrEmpty(request.EDITED_HOURS_PAYABLE))
                    updatededitScheduleProperties.EDITED_HOURS_PAYABLE = Common.GetVisitEditedHours(Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_START_TIME), Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_END_TIME), payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes);

                //Units   
                updatededitScheduleProperties.Units = await GetVisitUnits(HHA, User, payeradditionalinfo, updatededitScheduleProperties.SERVICECODE_ID ?? 0, Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_START_TIME), Convert.ToDateTime(updatededitScheduleProperties.ACCTUAL_END_TIME), clientsEntity.HHA_BRANCH_ID, oldscheduleinfo.CGTASK_ID);

            }

            //first time checkin
            if (updatededitScheduleProperties.ACCTUAL_START_TIME != null && oldscheduleinfo.ACCTUAL_START_TIME == null)
            {
                //checkin source
                updatededitScheduleProperties.CheckInSource = request.CheckInSource;
                //confirmed
                updatededitScheduleProperties.CONFIRMED = true;
                updatededitScheduleProperties.ClinicianConfirmedBy = User;
                updatededitScheduleProperties.ClinicianConfirmedOn = DateTime.UtcNow.ToConvertUTCTimeZone("PST");
                updatededitScheduleProperties.EVVOriginalCheckinTime = updatededitScheduleProperties.ACCTUAL_START_TIME;
                updatededitScheduleProperties.OriginalCheckinTimeUTC = Common.GetCurrentUTCTime();
                updatededitScheduleProperties.CHECK_IN_LOCATION = request.CHECK_IN_LOCATION;
            }
            else
                updatededitScheduleProperties.CheckInSource = oldscheduleinfo.CheckInSource;

            // first time checkout
            if (updatededitScheduleProperties.ACCTUAL_END_TIME != null && oldscheduleinfo.ACCTUAL_END_TIME == null)
            {
                updatededitScheduleProperties.CheckOutSource = request.CheckOutSource;
                updatededitScheduleProperties.EVVOriginalCheckoutTime = updatededitScheduleProperties.ACCTUAL_END_TIME;
                updatededitScheduleProperties.OriginalCheckoutTimeUTC = Common.GetCurrentUTCTime();
                updatededitScheduleProperties.CHECK_OUT_LOCATION = request.CHECK_OUT_LOCATION;
            }
            else
                updatededitScheduleProperties.CheckOutSource = oldscheduleinfo.CheckOutSource;

            updatededitScheduleProperties.CheckinTreatmentLocation = request.CheckinTreatmentLocation;
            updatededitScheduleProperties.CheckoutTreatmentLocation = request.CheckoutTreatmentLocation;

            //check for attestation
            if (request.isAttested == true && oldscheduleinfo.isAttested == false)
            {
                updatededitScheduleProperties.isAttested = true;
                updatededitScheduleProperties.AttestedBy = Convert.ToInt32(User);
                updatededitScheduleProperties.AttestedOn = Common.GetCurrentPSTTime();
            }

            //GEO LOCATION UPDATION OF CLIENT ADDRESS/CLIENT TREATMENT ADDRESS WHILE DOING FOB CHECKIN/OUT OR request.CHECK_IN_LOCATION not empty
            GeoCoordinate CaregiverLocation = new GeoCoordinate();
            GeoCoordinate checkinClientLocation = new GeoCoordinate();
            GeoCoordinate checkoutClientLocation = new GeoCoordinate();
            if (!string.IsNullOrEmpty(updatededitScheduleProperties.CHECK_IN_LOCATION)
                ||
                (updatededitScheduleProperties.CheckInSource == 6 && RequireGeoLocationforFOB && oldscheduleinfo.STATUS == ScheduleStatusEnums.Planned.ToString()))
            {
                int checkinTreatmentAddressID = clientsEntity.ADDRESS;

                if (updatededitScheduleProperties.CheckinTreatmentLocation > 0)
                {
                    var clientTreatmentAddress = await _clientTreatmentAddressRepository._S_Schedule_GetClientTreatmentAddress(HHA, User, updatededitScheduleProperties.CheckinTreatmentLocation ?? 0);
                    checkinTreatmentAddressID = clientTreatmentAddress.TreatmentAddressID;
                }

                //get Client treatment location (lat, long)

                if (checkinTreatmentAddressID > 0)
                {
                    var AddressEntity = await _addressRepository._S_Schedule_GetTreatmentLocation(HHA, User, checkinTreatmentAddressID);

                    if (AddressEntity != null)
                    {
                        checkinClientLocation.Latitude = Convert.ToDouble(AddressEntity.Latitude == "" ? 0 : AddressEntity.Latitude);
                        checkinClientLocation.Longitude = Convert.ToDouble(AddressEntity.Longitude == "" ? 0 : AddressEntity.Longitude);

                    }
                }

                
                 if (RequireGeoLocationforFOB && updatededitScheduleProperties.CheckInSource == 6)
                 {
                        updatededitScheduleProperties.CHECK_IN_LOCATION = checkinClientLocation.Latitude + "," + checkinClientLocation.Longitude;
                        updatededitScheduleProperties.CHECK_IN_DISTANCE = 0;
                        updatededitScheduleProperties.EvvCheckinLocationVerified = 'Y';
                    
                 }
                 else if (!string.IsNullOrEmpty(updatededitScheduleProperties.CHECK_IN_LOCATION))
                 {
                     CaregiverLocation.Latitude = Convert.ToDouble(updatededitScheduleProperties.CHECK_IN_LOCATION.Split(',')[0]);
                     CaregiverLocation.Longitude = Convert.ToDouble(updatededitScheduleProperties.CHECK_IN_LOCATION.Split(',')[1]);

                     if (checkinClientLocation.Latitude != double.NaN && checkinClientLocation.Latitude > 0)
                     {
                         updatededitScheduleProperties.CHECK_IN_DISTANCE = Common.GetDistanceFromSource(checkinClientLocation, CaregiverLocation);
                         updatededitScheduleProperties.EvvCheckinLocationVerified = Common.GetLocationVerifiedStatus(updatededitScheduleProperties.CHECK_IN_DISTANCE ?? 0, 0);
                     }
                 }

            }

            

            if (!string.IsNullOrEmpty(updatededitScheduleProperties.CHECK_OUT_LOCATION)
                ||
                (updatededitScheduleProperties.CheckOutSource == 6 && RequireGeoLocationforFOB))
            {
                
                int checkoutTreatmentAddressID = clientsEntity.ADDRESS;
                if (request.CheckoutTreatmentLocation > 0)
                {
                    var clientTreatmentAddress = await _clientTreatmentAddressRepository._S_Schedule_GetClientTreatmentAddress(HHA, User, request.CheckoutTreatmentLocation ?? 0);
                    checkoutTreatmentAddressID = clientTreatmentAddress.TreatmentAddressID;
                }

                //get Client treatment location (lat, long) 
                if (checkoutTreatmentAddressID > 0)
                {
                    var checkoutAddressEntity = await _addressRepository._S_Schedule_GetTreatmentLocation(HHA, User, checkoutTreatmentAddressID);

                    if (checkoutAddressEntity != null)
                    {
                        checkoutClientLocation.Latitude = Convert.ToDouble(checkoutAddressEntity.Latitude == "" ? 0 : checkoutAddressEntity.Latitude);
                        checkoutClientLocation.Longitude = Convert.ToDouble(checkoutAddressEntity.Longitude == "" ? 0 : checkoutAddressEntity.Longitude);
                    }
                }

                if (RequireGeoLocationforFOB && updatededitScheduleProperties.CheckOutSource == 6)
                {
                    if (checkoutClientLocation.Latitude != double.NaN && checkoutClientLocation.Latitude > 0)
                    {
                        updatededitScheduleProperties.CHECK_OUT_LOCATION = checkoutClientLocation.Latitude + "," + checkoutClientLocation.Longitude;
                        updatededitScheduleProperties.CHECK_OUT_DISTANCE = 0;
                        updatededitScheduleProperties.EvvCheckoutLocationVerified = 'Y';
                    }
                }
                else if (!string.IsNullOrEmpty(updatededitScheduleProperties.CHECK_OUT_LOCATION))
                {
                    CaregiverLocation.Latitude = Convert.ToDouble(request.CHECK_OUT_LOCATION.Split(',')[0]);
                    CaregiverLocation.Longitude = Convert.ToDouble(request.CHECK_OUT_LOCATION.Split(',')[1]);


                    if (checkoutClientLocation.Latitude != double.NaN && checkoutClientLocation.Latitude > 0)
                    {
                        updatededitScheduleProperties.CHECK_OUT_DISTANCE = Common.GetDistanceFromSource(checkoutClientLocation, CaregiverLocation);
                        updatededitScheduleProperties.EvvCheckoutLocationVerified = Common.GetLocationVerifiedStatus(updatededitScheduleProperties.CHECK_OUT_DISTANCE ?? 0, 0);
                    }
                }
            }

            //fob checkin code
            if (!string.IsNullOrEmpty(request.fob_checkin_code))
                updatededitScheduleProperties.fob_checkin_code = request.fob_checkin_code;

            //fob checkout code
            if (!string.IsNullOrEmpty(request.fob_checkout_code))
                updatededitScheduleProperties.fob_checkout_code = request.fob_checkout_code;

            //fob checkin device id
            if (!string.IsNullOrEmpty(request.fob_checkin_device_id))
            {
                if (request.fob_checkin_device_id != oldscheduleinfo.fob_checkin_device_id)
                    updatededitScheduleProperties.fob_checkin_device_id = request.fob_checkin_device_id;
            }
            //fob checkout device id 
            if (!string.IsNullOrEmpty(request.fob_checkout_device_id))
            {
                if (request.fob_checkout_device_id != oldscheduleinfo.fob_checkout_device_id)
                    updatededitScheduleProperties.fob_checkout_device_id = request.fob_checkout_device_id;

            }
            //get Authorization releated fields
            if (updatededitScheduleProperties.CanBill == true && updatededitScheduleProperties.IS_AUTH_MANDATORY == true)
            {
                var ret = await _authorization.ValidateScheduleAuth(HHA, User, oldscheduleinfo.CGTASK_ID, updatededitScheduleProperties, true);

                if (!ret.iserror)
                {
                    updatededitScheduleProperties.AUTHORIZATION_ID = ret.AuthID;

                    if (updatededitScheduleProperties.AUTHORIZATION_ID > 0)
                        updatededitScheduleProperties.isAuthorized = true;
                }
            }
            else
            {
                updatededitScheduleProperties.AUTHORIZATION_ID = oldscheduleinfo.AUTHORIZATION_ID;
                updatededitScheduleProperties.isAuthorized = oldscheduleinfo.isAuthorized;
            }



            return updatededitScheduleProperties;
        }

        public async Task<float> GetVisitUnits(int HHA, int UserID, PaymentSourcesAdditionalEntity paymentSourcesAdditionalEntity, int ServiceID, DateTime Checkindatetime, DateTime CheckOutDatetime, int HHA_BRANCH_ID,
                      int CgTaskID, CDSPlanYearServicesEntity cDSPlanYearServicesEntity = null,int TotalMinutes=0)
        {
            float units;
            if(  TotalMinutes==0)
            {
                TotalMinutes = Convert.ToInt32(CheckOutDatetime.Subtract(Checkindatetime).TotalMinutes);

            }
            
            int ROUNDOFFUnits_Direction;
            int ROUNDOFFUnits_Minutes;
            bool isUnitsFixed;
            double FixedUnits;
            int MinutesPerUnit;
            double NumberOfUnits;
            bool Round_ZeroUnitScheduleToOne;
            int SplitRevenueCodeHoursMins;

            double RevSplit_FixedUnits;
            int RevSplit_RoundDirection;
            int RevSplit_Round_OffMinutes;
            int RevSplit_MinutesPerUnit;

            double RevSplit_NumberofUnits;
            bool ServicemaxunitEnabled;
            double Servicemaxunits;

            int RevSecondSplit_RoundDirection;
            int RevSecondSplit_Round_OffMinutes;
            bool RevSplit_isUnitsFixed;
            bool RevSecondSplit_OverrideUnits = false;
            double RevSplit_FixedUnits_secondsplit = 0;
            bool RevSplit_isUnitsFixed_secondsplit = false;
            int RevSplit_MinutesPerUnit_secondsplit = 0;
            double RevSplit_NumberofUnits_secondsplit = 0;


            ROUNDOFFUnits_Direction = paymentSourcesAdditionalEntity.ROUNDOFF_Direction;
            ROUNDOFFUnits_Minutes = paymentSourcesAdditionalEntity.ROUNDOFF_Minutes;
            Round_ZeroUnitScheduleToOne = paymentSourcesAdditionalEntity.Round_ZeroUnitScheduleToOne ?? false;
            var scheduleadditional2filter ="" ;
            var scheduleadditional2column = "CGTaskID,CdsPlanYearService ";
            IEnumerable<CaregivertasksEntity> schedulecdsadditioanlinfo=new List<CaregivertasksEntity>();
            //if (cdsplanyearserviceid == 0)
            //{
            //      scheduleadditional2filter = _repositoryFilterConditions.EqualFilter("CGTaskID", CgTaskID, FilterConditionDataTypeEnums.integerType)
            //                                       .GreaterThanFilter("CDSClientBudgetLineItemID", 0, FilterConditionDataTypeEnums.integerType)
            //                                       .Build();
            //      schedulecdsadditioanlinfo = await _caregivertasksRepository.GetScheduleAdditional2List(HHA, UserID, scheduleadditional2column, scheduleadditional2filter);
            //    cdsplanyearserviceid =(int) schedulecdsadditioanlinfo.FirstOrDefault().CdsPlanYearService;
            //}
            
            //get service info 
            var serviceCodesinfo = await _serviceCodesRepository.GetServiceCodeAdditionalInfo(HHA, UserID, ServiceID, HHA_BRANCH_ID, Checkindatetime.ToDateFormat());

            isUnitsFixed = serviceCodesinfo.isUnitsFixed ?? false;
            FixedUnits = serviceCodesinfo.FixedUnits ?? 0;
            MinutesPerUnit = serviceCodesinfo.MinutesPerUnit ?? 0;
            NumberOfUnits = serviceCodesinfo.NumberOfUnits ?? 0;
            SplitRevenueCodeHoursMins = serviceCodesinfo.enableSplitRevenueCode == true ? Convert.ToInt32(Math.Round(serviceCodesinfo.SplitRevenueCodeHours ?? 0, 2) * 60) : 0;

            RevSplit_RoundDirection = ROUNDOFFUnits_Direction;
            RevSplit_Round_OffMinutes = ROUNDOFFUnits_Minutes;
            RevSecondSplit_RoundDirection = ROUNDOFFUnits_Direction;
            RevSecondSplit_Round_OffMinutes = ROUNDOFFUnits_Minutes;
            RevSplit_FixedUnits = FixedUnits;
            RevSplit_isUnitsFixed = isUnitsFixed;
            RevSplit_MinutesPerUnit = MinutesPerUnit;
            RevSplit_NumberofUnits = NumberOfUnits;

            if (cDSPlanYearServicesEntity!=null && cDSPlanYearServicesEntity.CDSPlanYearServiceID > 0)
            {
                isUnitsFixed = false;
                FixedUnits = 0;

                //var cdsplanyearfilters = _repositoryFilterConditions.EqualFilter("cdsplanyearservices.CDSPlanYearServiceId", cdsplanyearserviceid, FilterConditionDataTypeEnums.integerType).Build();
                //var cdsplanyearcolumns = "cdsplanyearservices.CDSPlanYearServiceId, isnull(MinutesPerUnit, 0) MinutesPerUnit, isnull(NumberOfUnits, 0) NumberOfUnits ";
                //var cDSPlanYearServicesinfo = await _cDSPlanYearServicesRepository._S_Schedule_GetCDSPlanYearServices(HHA, UserID, cdsplanyearcolumns, cdsplanyearfilters);

                
                    MinutesPerUnit = cDSPlanYearServicesEntity.MinutesPerUnit;
                    NumberOfUnits = cDSPlanYearServicesEntity.NumberOfUnits;
               
            }
            else
            {
                if (serviceCodesinfo.RevSecondSplit_OverrideUnits == true && serviceCodesinfo.enableSplitRevenueCode == true)
                {
                    RevSecondSplit_OverrideUnits = true;
                    RevSplit_FixedUnits_secondsplit = serviceCodesinfo.RevSecondSplit_FixedUnits ?? 0;
                    RevSplit_isUnitsFixed_secondsplit = serviceCodesinfo.RevSecondSplit_isUnitsFixed ?? false;
                    RevSplit_MinutesPerUnit_secondsplit = serviceCodesinfo.RevSecondSplit_MinutesPerUnit ?? 0;
                    RevSplit_NumberofUnits_secondsplit = serviceCodesinfo.RevSecondSplit_NumberofUnits ?? 0;
                    RevSplit_NumberofUnits = serviceCodesinfo.RevSplit_NumberofUnits ?? 0;
                    RevSplit_MinutesPerUnit = serviceCodesinfo.RevSplit_MinutesPerUnit ?? 0;
                    if (serviceCodesinfo.RevFirstSplit_UnitRoundOffDirection > 0)
                    {
                        RevSplit_Round_OffMinutes = serviceCodesinfo.RevFirstSplit_UnitRoundOffMinutes ?? 0;
                        RevSplit_RoundDirection = serviceCodesinfo.RevFirstSplit_UnitRoundOffDirection ?? 0;
                    }

                    if (serviceCodesinfo.RevSecondSplit_UnitRoundOffDirection > 0)
                    {
                        RevSecondSplit_RoundDirection = serviceCodesinfo.RevSecondSplit_UnitRoundOffDirection ?? 0;
                        RevSecondSplit_Round_OffMinutes = serviceCodesinfo.RevSecondSplit_UnitRoundOffMinutes ?? 0;
                    }
                }
            }

            Servicemaxunits = serviceCodesinfo.MaximumUnitsForTheSchedule ?? 0;
            ServicemaxunitEnabled = serviceCodesinfo.enableMaximumUnitsForTheSchedule ?? false;

            if (RevSecondSplit_OverrideUnits == true && TotalMinutes > SplitRevenueCodeHoursMins && TotalMinutes > 0)
            {
                isUnitsFixed = RevSplit_isUnitsFixed_secondsplit;
                FixedUnits = RevSplit_FixedUnits_secondsplit;
                MinutesPerUnit = RevSplit_MinutesPerUnit_secondsplit;
                NumberOfUnits = RevSplit_NumberofUnits_secondsplit;
                ROUNDOFFUnits_Direction = RevSecondSplit_RoundDirection;
                ROUNDOFFUnits_Minutes = RevSecondSplit_Round_OffMinutes;
            }

            units = Common.GetVisitUnits(TotalMinutes,
                        ROUNDOFFUnits_Direction, ROUNDOFFUnits_Minutes, isUnitsFixed, FixedUnits, MinutesPerUnit,
                        NumberOfUnits, Round_ZeroUnitScheduleToOne, SplitRevenueCodeHoursMins,
                        RevSplit_isUnitsFixed, RevSplit_FixedUnits, RevSplit_RoundDirection,
                        RevSplit_Round_OffMinutes, RevSplit_MinutesPerUnit,
                        RevSplit_NumberofUnits, ServicemaxunitEnabled, Servicemaxunits,
                        RevSecondSplit_OverrideUnits, RevSplit_isUnitsFixed_secondsplit, RevSplit_FixedUnits_secondsplit,
                        RevSplit_MinutesPerUnit_secondsplit, RevSplit_NumberofUnits_secondsplit, RevSecondSplit_RoundDirection,
                        RevSecondSplit_Round_OffMinutes
                        );

            return units;
        }


        //public async Task<IEnumerable<string>> CreateSchedule(int hha,int user,IEnumerable<CreateScheduleDTO> request)
        //{
        //    List<string> errorList = new List<string>();

        //    for (int i=0;i<= request.Count();i++)
        //    {
        //        //var ClientFilters = _repositoryFilterConditions.EqualFilter("CLIENT_ID", request.ElementAt(i).client_id, FilterConditionDataTypeEnums.integerType);

        //        var record = request.ElementAt(i);
        //        var clientsEntity = _clientsRepository.GetClientsBasicInfo(hha, user, record.client_id);
        //        var CaregiverEntity = _caregiversRepository.GetCaregiverBasicInfo(hha,user, record.caregiver_id);
        //        //var paymentsourcesFilter = _repositoryFilterConditions.EqualFilter("Payment_Source_ID", record.payment_source_id, FilterConditionDataTypeEnums.integerType).Build();

        //        var PaymentSourceEntity = _paymentsourcesRepository.GetPaymentSourceBasicInfo(hha,user,record.payment_source_id);
        //        //var updatedCaregiverEntityforValidation = (CaregivertasksEntity).Shallowcopy();


        //        if (clientsEntity.Result.CLIENT_ID == 0)
        //        {
        //            errorList.Add("Invalid Client");
        //        }

        //        if (CaregiverEntity.Result.CAREGIVER_ID == 0)
        //        {
        //            errorList.Add("Invalid Clinician");

        //        }
        //        if (PaymentSourceEntity.Result.PAYMENT_SOURCE_ID==0)
        //        {
        //            errorList.Add("Invalid Payer Source");

        //        }


        //        if (record.is_approved && string.IsNullOrEmpty(record.check_in_time) && string.IsNullOrEmpty(record.check_out_time))
        //        {
        //            errorList.Add("Check-in and Check-out cannot be empty ");

        //        }


        //        if (record.is_approved)
        //            record.status = "Approved";

        //        else if (string.IsNullOrEmpty(record.check_in_time))
        //        {
        //            record.status = "Planned";
        //        }
        //        else if (string.IsNullOrEmpty(record.check_in_time) && (!string.IsNullOrEmpty(record.check_out_time)))
        //            record.status = "In_Progress";
        //        else
        //            record.status = "Completed";

        //            if (record.total_hours==0)
        //        {
        //            if(string.IsNullOrEmpty(record.check_in_time)  || string.IsNullOrEmpty(record.check_out_time))

        //                record.total_hours= Common.GetVisitActualHours(Convert.ToDateTime(record.planned_start_time), Convert.ToDateTime(record.planned_end_time));
        //           else if (!string.IsNullOrEmpty(record.check_in_time) && !string.IsNullOrEmpty(record.check_out_time))
        //            {
        //                record.total_hours = Common.GetVisitActualHours(Convert.ToDateTime(record.check_in_time), Convert.ToDateTime(record.check_out_time));

        //            }
        //        }

        //        if (Convert.ToDateTime(record.planned_end_time) <= Convert.ToDateTime(record.planned_start_time))
        //            record.planned_end_time = (Convert.ToDateTime(record.planned_end_time)).AddDays(1).ToString();
        //        if (Convert.ToDateTime(record.check_out_time) <= Convert.ToDateTime(record.check_in_time))
        //            record.planned_end_time = (Convert.ToDateTime(record.check_out_time)).AddDays(1).ToString();

        //        var caregiver_taskdetails =  (new
        //        {
        //            planned_date = record.schedule_date,
        //            planned_start_time = record.planned_start_time,
        //            planned_end_time = record.planned_end_time,
        //            client_id = record.client_id,
        //            service_code_id = record.service_code_id,
        //            payment_source_id = record.payment_source_id,
        //            caregiver_id = record.caregiver_id ,
        //            is_billable=record.is_billable,
        //            is_payable=record.is_payable,
        //            check_in_time=record.check_in_time,
        //            check_in_location=record.check_in_location,
        //            check_out_time=record.check_out_time,
        //            edited_hours=record.edited_hours,
        //            payable_hours=record.payable_hours,
        //            break_hours=record.break_hours,
        //            total_hours= record.total_hours,
        //            miles=record.miles,
        //            status=record.status,
        //            place_of_service = record.place_of_service,
        //            units = record.units,
        //            travel_time_min = record.travel_time_min,
        //            documentation_time_in_min = record.documentation_time_in_min,
        //            is_evvschedule = record.is_evvschedule,
        //            is_evvschedule_dirty = record.is_evvschedule_dirty,
        //            check_out_location = record.check_out_location,
        //            check_in_distance = record.check_in_distance,
        //            check_out_distance = record.check_out_distance,
        //            check_in_treatment_location = record.check_in_treatment_location,
        //            check_out_treatment_location = record.check_out_treatment_location,
        //            evv_check_in_location_verified = record.evv_check_in_location_verified,
        //            evv_check_out_location_verified = record.evv_check_out_location_verified,
        //            is_approved_for_payroll = record.is_approved,
        //            cds_auth_service_id = record.cds_auth_service_id,
        //            cds_plan_year = record.cds_plan_year 

        //        });


        //        //var caregiver_taskAdditioanldetails =  (new
        //        //{
        //        //    //place_of_service=record.place_of_service,
        //        //    //units=record.units,
        //        //    //travel_time_min=record.travel_time_min,
        //        //    //documentation_time_in_min=record.documentation_time_in_min,
        //        //    //is_evvschedule=record.is_evvschedule,
        //        //    //is_evvschedule_dirty=record.is_evvschedule_dirty,
        //        //    //check_in_location=record.check_in_location,
        //        //    //check_out_location=record.check_out_location,
        //        //    //check_in_distance=record.check_in_distance,
        //        //    //check_out_distance = record.check_out_distance,
        //        //    //check_in_treatment_location=record.check_in_treatment_location,
        //        //    //check_out_treatment_location=record.check_out_treatment_location,
        //        //    //evv_check_in_location_verified=record.evv_check_in_location_verified,
        //        //    //evv_check_out_location_verified=record.evv_check_out_location_verified,
        //        //    //is_approved_for_payroll=record.is_approved,
        //        //    //cds_auth_service_id = record.cds_auth_service_id,
        //        //    //cds_plan_year = record.cds_plan_year,
        //        //    //Created_on = DateTime.Now,
        //        //    //created_by = user
        //        //});
        //        //var caregiver_taskAdditioanldetails2 = (new
        //        //{

        //        //});
        //        var ScheduleJson = JsonConvert.SerializeObject(caregiver_taskdetails);
        //      await  _caregivertasksRepository.CreateSchedule(hha, user, ScheduleJson);

        //    }
        //    return errorList;

        //}
        public async Task<IEnumerable<ValidationErrorInfo>> CreateSchedule(int hha, int user, CreateScheduleDTO request)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>();
            int cgtaskid_out = 0;
            //var ClientFilters = _repositoryFilterConditions.EqualFilter("CLIENT_ID", request.ElementAt(i).client_id, FilterConditionDataTypeEnums.integerType);

            for (int i = 0; i < request.Schedule.Count; i++)
            {
                var record = request.Schedule.ElementAt(i);

                //var clientsEntity = _clientsRepository.GetClientsBasicInfo(hha, user, record.client_id);
                ////var CaregiverEntity = _caregiversRepository.GetCaregiverBasicInfo(hha, user, record.caregiver_id);
                ////var paymentsourcesFilter = _repositoryFilterConditions.EqualFilter("Payment_Source_ID", record.payment_source_id, FilterConditionDataTypeEnums.integerType).Build();
                var episodesEntity = _episodeRepository._CL_GetClientEpisodes(hha, user, record.client_id);
                if(episodesEntity.Result.Count()==0)
                {
                    errorList.Add(new ValidationErrorInfo() { validation_type = "Invalid Client.", validation_error_message = "Invalid Client." });
                    continue;
                }
                 
                var payeradditionalinfo = await _paymentsourcesAdditionalRepository.GetPayerAdditionalInfo(hha, user, record.payment_source_id, "");
                var IsEpisodlessClient = false;
                var isCDSClient = false;
                //var hhascheduleauthsettings = _hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(hha, user).Result;
                var AuditDesc = string.Empty;

              
                if (record.is_approved && string.IsNullOrEmpty(record.check_in_time) && string.IsNullOrEmpty(record.check_out_time))
                {
                    errorList.Add(new ValidationErrorInfo() { validation_type = "Check-in and Check-out cannot be empty", 
                        validation_error_message = "Check-in and Check-out cannot be empty"
                    });

                    continue;

                }
                var Edited_Hours_Initial = record.edited_hours;

                if (string.IsNullOrEmpty(record.edited_hours))
                {


                    if (!string.IsNullOrEmpty(record.check_out_time))
                        record.edited_hours = Common.GetVisitEditedHours(Convert.ToDateTime(record.check_in_time),
                            Convert.ToDateTime(record.check_out_time),  payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes);
                    else
                        record.edited_hours = Common.GetVisitEditedHours(Convert.ToDateTime(record.planned_start_time),
                            Convert.ToDateTime(record.planned_end_time), payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes);

                }
                else
                {
                    record.authorized_hours = Common.ConvertStringToMins(record.edited_hours) * 60;
                    record.total_hours = record.authorized_hours;
                }
                if (string.IsNullOrEmpty(record.check_out_time))
                    record.edited_hours = null;

                var schedule_date = record.schedule_date;
                var CurrentEpisodes = episodesEntity.Result.Where(x => Convert.ToDateTime(schedule_date) >= x.EpStartDate &&
                Convert.ToDateTime(schedule_date) <= x.EpEndDate).FirstOrDefault();
                var POC = 0;
                if(CurrentEpisodes!=null && CurrentEpisodes.POC>0)
                {
                    POC = CurrentEpisodes.POC;
                }
                var LatestEpisodeEndDate = new DateTime();




                record.planned_start_time = record.schedule_date + ' ' + record.planned_start_time;
                record.planned_end_time = record.schedule_date + ' ' + record.planned_end_time;
                if(!string.IsNullOrEmpty(record.evv_check_in_time))
                {
                    record.evv_check_in_time = record.schedule_date + ' ' + record.evv_check_in_time;

                }
                else if(!string.IsNullOrEmpty(record.check_in_time))
                {
                    record.evv_check_in_time = record.schedule_date + ' '+record.check_in_time;

                }
                if (!string.IsNullOrEmpty(record.evv_check_in_time))
                {
                    record.evv_check_out_time = record.schedule_date + ' ' + record.evv_check_out_time;

                }
                else if (!string.IsNullOrEmpty(record.check_out_time))
                {
                    record.evv_check_out_time = record.schedule_date + ' '+record.check_out_time;

                }


                //var updatedCaregiverEntityforValidation = (CaregivertasksEntity).Shallowcopy();

                if (!string.IsNullOrEmpty(record.check_in_time))
                    record.check_in_time = record.schedule_date + ' ' + record.check_in_time;

                if (!string.IsNullOrEmpty(record.check_out_time))
                    record.check_out_time = record.schedule_date + ' ' + record.check_out_time;


                if (record.is_approved)
                {
                    record.status = "Approved";
                    record.is_approved_for_payroll = record.is_approved;
                }


                else if (string.IsNullOrEmpty(record.check_in_time))
                {
                    record.status = "Planned";
                }
                else if (!string.IsNullOrEmpty(record.check_in_time) && (string.IsNullOrEmpty(record.check_out_time)))
                    record.status = "In_Progress";
                else
                    record.status = "Completed";
                if (Convert.ToDateTime(record.planned_end_time) <= Convert.ToDateTime(record.planned_start_time))
                    record.planned_end_time = (Convert.ToDateTime(record.planned_end_time)).AddDays(1).ToString();
                if (!string.IsNullOrEmpty(record.check_in_time) && !string.IsNullOrEmpty(record.check_out_time))
                {
                    if (Convert.ToDateTime(record.check_out_time) <= Convert.ToDateTime(record.check_in_time))
                        record.check_out_time = (Convert.ToDateTime(record.check_out_time)).AddDays(1).ToString();
                }
                if (record.authorized_hours == 0)
                    record.authorized_hours = Common.GetVisitActualHours(Convert.ToDateTime(record.planned_start_time), Convert.ToDateTime(record.planned_end_time));

                if (record.total_hours == 0)
                {
                    if (string.IsNullOrEmpty(record.check_in_time) || string.IsNullOrEmpty(record.check_out_time))
                        record.total_hours = record.authorized_hours;
                    else if (!string.IsNullOrEmpty(record.check_in_time) && !string.IsNullOrEmpty(record.check_out_time))
                    {
                        record.total_hours = Common.GetVisitActualHours(Convert.ToDateTime(record.check_in_time), Convert.ToDateTime(record.check_out_time));

                    }
                }
                





                //Columns = "BudgetLineItemMasterID,enableWeeklyHoursScheduleValidation,enableWeeklyAmountScheduleValidation";

                //Filter = _repositoryFilterConditions.EqualFilter("SERVICE_CODE_ID", record.service_code_id, FilterConditionDataTypeEnums.integerType)
                //           .Build();

                var CDSBudgetLineItemMaster = await _serviceCodesRepository.GetServiceCodeBasicInfo(hha, user, record.service_code_id);
                record.CDSBudgetLineItemMasterID = CDSBudgetLineItemMaster.CDSBudgetLineItemMasterID;




                if (!string.IsNullOrEmpty(record.check_in_time))
                {
                    record.original_UTC_checkin_time = Common.GetCurrentUTCTime().Date;
                }
                else
                {
                    record.original_UTC_checkin_time = null;
                }

                if (!string.IsNullOrEmpty(record.check_out_time))
                {
                    record.original_UTC_checkout_time = Common.GetCurrentUTCTime().Date;
                }
                else
                {
                    record.original_UTC_checkout_time = null;
                }
                //convert to Json string
                var RecalculateEVVCgtaskids = (new
                {
                    ClientID = record.client_id,
                    ServiceID = record.service_code_id,
                    PayerID = record.payment_source_id,
                    ScheduleDate = record.schedule_date,
                    CaregiverID = record.caregiver_id,
                    CDSPlanYearServiceID = record.cds_auth_service_id
                });





                var CGTaskIDsjson = JsonConvert.SerializeObject(RecalculateEVVCgtaskids);
                //Recalculate evv flag and update
                var res = await this._caregivertasksRepository.UpdateRecalculateEVVSchedules(hha, user, CGTaskIDsjson, 0);
                if (!string.IsNullOrEmpty(res))
                {
                    var evvScheduleslist = JsonConvert.DeserializeObject<List<EVVBasicList>>(res).FirstOrDefault(); ;
                    if (
                     evvScheduleslist.ClientID == record.client_id
                     && evvScheduleslist.PayerID == record.payment_source_id
                     && evvScheduleslist.ServiceID == record.service_code_id
                     && evvScheduleslist.CDSPlanYearServiceID == record.cds_auth_service_id
                     )
                    {
                        record.is_evvschedule = true;
                    }
                }
                 

                var caregiver_taskdetails = (new CaregivertasksEntity
                {
                    PLANNED_DATE = Convert.ToDateTime(record.schedule_date),
                    PLANNED_START_TIME = Convert.ToDateTime(record.planned_start_time),
                    PLANNED_END_TIME = Convert.ToDateTime(record.planned_end_time),
                    CLIENT_ID = record.client_id,
                    SERVICECODE_ID = record.service_code_id,
                    PAYMENT_SOURCE = record.payment_source_id,
                    CAREGIVER = record.caregiver_id,
                    IS_BILLABLE = true,
                    ACCTUAL_START_TIME = string.IsNullOrEmpty(record.check_in_time) ? null : Convert.ToDateTime(record.check_in_time),
                    CHECK_IN_LOCATION = record.check_in_location,
                    ACCTUAL_END_TIME = string.IsNullOrEmpty(record.check_out_time) ? null : Convert.ToDateTime(record.check_out_time),
                    EDITED_HOURS = record.edited_hours,
                    EDITED_HOURS_PAYABLE = record.payable_hours,
                    TotalBreakHours = record.break_hours,
                    ManualTravelTimeMins = record.travel_time_min,
                    MILES = record.miles,
                    STATUS = record.status,
                    documentationTimeMins = record.documentation_time_in_min,
                    isEvvschedule = record.is_evvschedule,
                    IsEVVDirty = record.is_evvschedule_dirty,
                    CHECK_OUT_LOCATION = record.check_out_location,
                    CHECK_IN_DISTANCE = record.check_in_distance,
                    CHECK_OUT_DISTANCE = record.check_out_distance,
                    CheckinTreatmentLocation = record.check_in_treatment_location,
                    CheckoutTreatmentLocation = record.check_out_treatment_location,
                    EvvCheckinLocationVerified = record.evv_check_in_location_verified,
                    EvvCheckoutLocationVerified = record.evv_check_out_location_verified,
                    CdsPlanYearService = record.cds_auth_service_id,
                    CdsPlanYear = record.cds_plan_year,
                    TotalHours = record.total_hours,
                    ApprovedForPayroll = record.is_approved_for_payroll,
                    POC = POC,
                    enablePayableHours = record.enable_payable_hours,
                    AuthorizedHours = record.authorized_hours,
                    CDSClientBudgetLineItemID = record.CDSBudgetLineItemID,
                    EVVOriginalCheckinTime = string.IsNullOrEmpty(record.evv_check_in_time) ? null : Convert.ToDateTime(record.evv_check_in_time),
                    EVVOriginalCheckoutTime = string.IsNullOrEmpty(record.evv_check_out_time) ? null : Convert.ToDateTime(record.evv_check_out_time),
                    OriginalCheckinTimeUTC = record.original_UTC_checkin_time,
                    OriginalCheckoutTimeUTC = record.original_UTC_checkout_time,
                    IS_PAYABLE = record.is_payable,
                    travelTimeHours = record.travel_time_min != 0 ? record.travel_time_min / 60 : null,
                    AUTHORIZATION_ID= record.Authorization_ID,
                    PlaceOfService=record.place_of_service

                });
               
                var allscheduleDetaisobj = await _getScheduleBasicDetail.getScheduleDetails(hha, user, caregiver_taskdetails);


                var clientsEntity = allscheduleDetaisobj.clientsEntity;
                //var CaregiverEntity = _caregiversRepository.GetCaregiverBasicInfo(hha, user, record.caregiver_id);
                //var paymentsourcesFilter = _repositoryFilterConditions.EqualFilter("Payment_Source_ID", record.payment_source_id, FilterConditionDataTypeEnums.integerType).Build();
                //var episodesEntity = _episodeRepository._CL_GetClientEpisodes(hha, user, record.client_id);
                var PaymentSourceEntity = allscheduleDetaisobj.paymentsourcesEntity;
                var servicecodeEntity = allscheduleDetaisobj.serviceCodesEntity;
                var Clientpaymentsources = allscheduleDetaisobj.clientPaymentSourcesEntity;
                var cDSPlanYearServicesinfo = allscheduleDetaisobj.cDSplanyearservices;
                int cdsplanyearserviceid = cDSPlanYearServicesinfo.CDSPlanYearServiceID;
                var hhalobs = allscheduleDetaisobj.lineofbusinessEntity;
                var ClientBudgetLineIteminfo = allscheduleDetaisobj.cDSClientBudgetLineItem;
                var hhascheduleauthsettings = allscheduleDetaisobj.hHAScheduleAuthSettingsEntities;
                var hHABranchScheduleConfiguration = allscheduleDetaisobj.hHABranchScheduleConfigurationsEntity;
                if (Clientpaymentsources?.SchedulingRule == "VP")
                {
                    errorList.Add(new ValidationErrorInfo()
                    {
                        validation_type = "Schedule Can not be Created for Visit Plan Based Client Payer ",
                        validation_error_message = "Schedule Can not be Created for Visit Plan Based Client Payer "
                    });
     
                    continue;
                }
                if(record.cds_auth_service_id>0 &&ClientBudgetLineIteminfo?.ClientBudgetLineItemID==null)
                {
                    errorList.Add(new ValidationErrorInfo()
                    {
                        validation_type = "Schedule Can not be Created since Budget Line item is not available for given date. ",
                        validation_error_message = "Schedule Can not be Created since Budget Line item is not available for given date. "
                    });
                    continue;
                }
                record.canbill = record.is_billable == null ? servicecodeEntity.Is_Billable : record.is_billable;
                caregiver_taskdetails.CanBill = (bool)record.canbill;
                record.is_auth_mand= Clientpaymentsources.IS_AUTH_MAND;
                caregiver_taskdetails.IS_AUTH_MANDATORY = Clientpaymentsources.IS_AUTH_MAND;
                 
                caregiver_taskdetails.isAuthorized = Clientpaymentsources.IS_AUTH_MAND == false || record.canbill == false ? true : false;

                if (ClientBudgetLineIteminfo != null)
                {
                    caregiver_taskdetails.CDSClientBudgetLineItemID = ClientBudgetLineIteminfo.ClientBudgetLineItemID==0?null: ClientBudgetLineIteminfo.ClientBudgetLineItemID;
                }
                else
                    caregiver_taskdetails.CDSClientBudgetLineItemID = null;

                int TotalMinutes = 0;
                 
                    TotalMinutes = Common.ConvertStringToMins(record.edited_hours);
                if(record.break_hours>0)
                {
                    TotalMinutes = Convert.ToInt32( TotalMinutes - (record.break_hours * 60));
                }
                caregiver_taskdetails.Units = await GetVisitUnits(hha, user, payeradditionalinfo, caregiver_taskdetails.SERVICECODE_ID, caregiver_taskdetails.ACCTUAL_END_TIME != null ? (DateTime)caregiver_taskdetails.ACCTUAL_START_TIME :
                    caregiver_taskdetails.PLANNED_START_TIME, caregiver_taskdetails.ACCTUAL_END_TIME != null ? (DateTime)caregiver_taskdetails.ACCTUAL_END_TIME :
                      caregiver_taskdetails.PLANNED_END_TIME, clientsEntity.HHA_BRANCH_ID, 0, cDSPlanYearServicesinfo, TotalMinutes);
                if (caregiver_taskdetails.IS_PAYABLE == null && servicecodeEntity.IsPayable == true)
                {
                    caregiver_taskdetails.IS_PAYABLE = true;
                }
                if (caregiver_taskdetails.PlaceOfService == 0)
                {
                    caregiver_taskdetails.PlaceOfService = clientsEntity.PlaceOfService;
                }
                
                if (!string.IsNullOrEmpty(record.check_out_time))
                {
                    if (caregiver_taskdetails.EDITED_HOURS_PAYABLE == "")
                    {
                        if (PaymentSourceEntity.ROUNDOFFEditedHours_ExcludePayableHours == true && servicecodeEntity.PayrollRoundDirection==0)
                        {
                            caregiver_taskdetails.EDITED_HOURS_PAYABLE = Common.ConvertMinsToString(Common.GetVisitActualHours(Convert.ToDateTime(record.check_in_time), Convert.ToDateTime(record.check_out_time)));
                            caregiver_taskdetails.enablePayableHours = true;
                        }
                        else if(hHABranchScheduleConfiguration.PayrollRoundOffDirection>0)
                        {
                            caregiver_taskdetails.EDITED_HOURS_PAYABLE = Common.GetVisitEditedHours(caregiver_taskdetails.ACCTUAL_START_TIME.Value, caregiver_taskdetails.ACCTUAL_END_TIME.Value, hHABranchScheduleConfiguration.PayrollRoundOffDirection, hHABranchScheduleConfiguration.PayrollRoundOffMinutes);
                        }
                        else
                        {
                            caregiver_taskdetails.EDITED_HOURS_PAYABLE = record.edited_hours;
                        }
                    }

                    if (caregiver_taskdetails.EDITED_HOURS_PAYABLE !=
                        caregiver_taskdetails.EDITED_HOURS)
                    {
                        caregiver_taskdetails.enablePayableHours = true;
                    }
                }

                if (hhalobs.LOB_ID > 0)
                {
                    IsEpisodlessClient = hhalobs.isEpisodeLess;
                    isCDSClient = hhalobs.LOBType == "CDS" ? true : false;
                }
               if(POC==0 && !isCDSClient)
                {
                    errorList.Add(new ValidationErrorInfo()
                    {
                        validation_type = "Schedule cannot be created since it is outside the episode period ",
                        validation_error_message = "Schedule cannot be created since it is outside the episode period "
                    });
                    continue;
                }
                if ((POC == 0) && isCDSClient
                      && (record.check_in_time != "" || record.check_out_time != ""))
                {
                    LatestEpisodeEndDate = episodesEntity.Result.OrderByDescending(x => x.EpEndDate).FirstOrDefault().EpEndDate;

                    if (@LatestEpisodeEndDate < Convert.ToDateTime(schedule_date))
                    {
                        errorList.Add(new ValidationErrorInfo()
                        {
                            validation_type = "Schedule cannot be created since it is outside the episode period  ",
                            validation_error_message = "Schedule cannot be created since it is outside the episode period "
                        });
                        continue;

                    }
                }
                record.caregiver_taskdetails = caregiver_taskdetails;

                var ValidationResult = await ValidateCreateScheduleasync(hha, user, allscheduleDetaisobj);

                if (ValidationResult.Any())
                {
                    errorList.AddRange(ValidationResult);
                }
                else
                {
                    //get AuthorizationID
                    if (!isCDSClient && caregiver_taskdetails.CanBill == true)
                    {
                        var authInfoDto = await _paymentSourceAuthorizationRepository.GetAvailableAuthID(hha, user, 0, record.client_id,
                           record.payment_source_id, record.service_code_id, record.schedule_date,
                           record.units ?? 0, false, true, record.total_hours, true);

                        caregiver_taskdetails.AUTHORIZATION_ID = authInfoDto.AuthID;

                    }

                    //after getting auth id deallocate units/visits/hours
                    if (caregiver_taskdetails.AUTHORIZATION_ID > 0)
                    {
                        caregiver_taskdetails.isAuthorized = true;
                        //Get Fields to Update on Checkin
                        EditScheduleProperties editScheduleProperties = GetCreateScheduleModel(hha, user, caregiver_taskdetails);
                        var result = await _authorization.UpdateAuthorizationInfo(hha, user, editScheduleProperties);

                    }
                    var ScheduleJson = JsonConvert.SerializeObject(caregiver_taskdetails);

                    cgtaskid_out = await _caregivertasksRepository.CreateSchedule(hha, user, ScheduleJson);
                    
                            bool AllowSplit = false;
                            if(record.status=="Completed" || record.status == "Approved")
                            {
                                if(Convert.ToDateTime(record.check_in_time).Date != Convert.ToDateTime(record.check_out_time).AddMinutes(-1).Date)
                                {
                                    AllowSplit = true;
                                }
                            }
                            else  if (Convert.ToDateTime(record.planned_start_time).Date != Convert.ToDateTime(record.planned_end_time).AddMinutes(-1).Date)

                            {
                                AllowSplit = true;
                            }
                    //child schedule
                    var hhabillingpayrollsettingsFilter = _repositoryFilterConditions.EqualFilter("HHA_BRANCH_ID", clientsEntity.HHA_BRANCH_ID, FilterConditionDataTypeEnums.integerType).Build();
                    var hhabranchlistsetting = _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(hha, user, clientsEntity.HHA_BRANCH_ID).Result;
                    var hhabillingpayrollsettings = _homeHealthAgencies_BillingPayrollSettingsRepository.GetBillingPayrollSettingsDetails(hha, user, "", hhabillingpayrollsettingsFilter).Result.FirstOrDefault();
                    var CaregivertaskChildSchedules1 = new CaregiverTaskChildSchedulesEntity();
                    var CaregivertaskChildSchedules2 = new CaregiverTaskChildSchedulesEntity();
                    GeoCoordinate Child_CHECK_IN_LOCATION = new GeoCoordinate();
                    GeoCoordinate Child_CHECK_OUT_LOCATION = new GeoCoordinate();

                    if (!string.IsNullOrEmpty(record.check_in_location))
                    {
                        var check_in_location_split = record.check_in_location.Split(',');
                        if (check_in_location_split.Length > 1)
                        {
                            Child_CHECK_IN_LOCATION.Latitude = Convert.ToDouble(check_in_location_split[0]);
                            Child_CHECK_IN_LOCATION.Longitude = Convert.ToDouble(check_in_location_split[1]);
                        }
                    }
                    if (!string.IsNullOrEmpty(record.check_out_location))
                    {
                        var check_out_location_split = record.check_out_location.Split(',');
                        if (check_out_location_split.Length > 1)
                        {

                            Child_CHECK_OUT_LOCATION.Latitude = Convert.ToDouble(check_out_location_split[0]);
                            Child_CHECK_OUT_LOCATION.Longitude = Convert.ToDouble(check_out_location_split[1]);
                        }
                    }
                    if (hhascheduleauthsettings.FirstOrDefault().SplitOvernightSchedulesAtCreationlevel)
                    {
                        var hhascheudlesettingFilter = _repositoryFilterConditions.EqualFilter("HHABranchID", clientsEntity.HHA_BRANCH_ID,
                            FilterConditionDataTypeEnums.integerType).Build();
                        var hconfig2Filter = _repositoryFilterConditions.EqualFilter("HHA_BRANCHID", clientsEntity.HHA_BRANCH_ID, FilterConditionDataTypeEnums.integerType).Build();

                        var hconfig2entity = _hConfigurations2Repository._S_Schedule_GetHConfiguration2Settings(hha, user, "WEEK_START_DAY", hconfig2Filter).Result;
                        int weekStartday = hconfig2entity.WEEK_START_DAY;

                        DayOfWeek dayOfWeek = (DayOfWeek)Enum.GetValues(typeof(DayOfWeek)).GetValue(weekStartday);
                        DateTime weekStartDate = Common.GetWeekStartdate(Convert.ToDateTime(record.schedule_date), dayOfWeek);
                        DateTime weekEndDate = weekStartDate.AddDays(6);
                        if (PaymentSourceEntity.SplitOvernightSchedules
                            && (PaymentSourceEntity.SplitOvernightScheduleByWeek == false || (PaymentSourceEntity.SplitOvernightScheduleByWeek == true && weekStartDate == Convert.ToDateTime(record.schedule_date)))
                            && ((record.is_evvschedule == false && PaymentSourceEntity.OverNightScheduleApplicabletoNonEVV == true)
                            || (record.is_evvschedule == true && PaymentSourceEntity.OverNightScheduleApplicabletoPrimaryEVV == true && PaymentSourceEntity.PrimaryEvvVendorID == "PRM_KANTIME")
                            || (record.is_evvschedule == true && PaymentSourceEntity.OverNightScheduleApplicabletoPrimaryEVV == false && PaymentSourceEntity.PrimaryEvvVendorID != "PRM_KANTIME")))
                        {
                            record.SplitOvernightSchedulesForBilling = true;
                        }
                        if (hHABranchScheduleConfiguration.EnableScheduleAutoSplitBasedOnBusinessHours)
                        {
                            if (record.SplitOvernightSchedulesForBilling)
                            {
                                if (Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.BillingBusinessHoursStartTime)
                                     >= Convert.ToDateTime(record.planned_start_time) &&
                                     Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.BillingBusinessHoursStartTime)
                                     <= Convert.ToDateTime(record.planned_end_time)
                                     ||
                                     Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.BillingBusinessHoursEndTime)
                                     >= Convert.ToDateTime(record.planned_start_time) &&
                                     Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.BillingBusinessHoursEndTime)
                                     <= Convert.ToDateTime(record.planned_end_time))
                                {
                                    record.AutoSplitBasedOnBusinessHours = true;
                                }
                            }
                            if (hhabranchlistsetting.SplitOvernightSchedulesForPayroll && hhabillingpayrollsettings.splitHolidayOvernightSchedule == true
                                   && servicecodeEntity.isHolidayPayRateApplicable == true)
                            {
                                record.SplitOvernightSchedulesForPayroll = true;
                            }
                            if (record.SplitOvernightSchedulesForPayroll)
                            {
                                if (Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.PayrollBusinessHoursStartTime)
                                    >= Convert.ToDateTime(record.planned_start_time) &&
                                    Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.PayrollBusinessHoursStartTime)
                                    <= Convert.ToDateTime(record.planned_end_time)
                                    ||
                                    Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.PayrollBusinessHoursEndTime)
                                    >= Convert.ToDateTime(record.planned_start_time) &&
                                    Convert.ToDateTime(record.schedule_date + ' ' + hhabillingpayrollsettings.PayrollBusinessHoursEndTime)
                                    <= Convert.ToDateTime(record.planned_end_time))
                                {
                                    record.AutoSplitBasedOnBusinessHours = true;

                                }
                            }

                            if (record.AutoSplitBasedOnBusinessHours)
                            {
                                

                                record.has_child_schedules = true;
                                CaregivertaskChildSchedules1 = (new CaregiverTaskChildSchedulesEntity
                                {
                                    SCHEDULE_DATE = Convert.ToDateTime(schedule_date),
                                    PLANNED_STARTTIME = Convert.ToDateTime(record.planned_start_time),
                                    PLANNED_ENDTIME = Convert.ToDateTime(record.planned_end_time),
                                    PAYMENT_SOURCE = record.payment_source_id,
                                    SERVICECODE_ID = record.service_code_id,
                                    CANBILL = (bool)record.canbill,
                                    CLIENT = record.client_id,
                                    POC = POC,
                                    PARENT_CGTASK_ID = cgtaskid_out,
                                    ACTUAL_DATE = Convert.ToDateTime(record.schedule_date).Date,
                                    isManualSplit = true,
                                    IsAuthMandatory = record.is_auth_mand,
                                    isEvvschedule = record.is_evvschedule,
                                    enablePayableHours= caregiver_taskdetails.enablePayableHours.Value,
                                    CHECK_IN_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_IN_LOCATION.Latitude + " " + Child_CHECK_IN_LOCATION.Longitude : null,
                                    CHECK_OUT_LOCATION = !Child_CHECK_OUT_LOCATION.IsUnknown ? Child_CHECK_OUT_LOCATION.Latitude + " " + Child_CHECK_OUT_LOCATION.Longitude : null,
                                    CHECK_IN_DISTANCE = Convert.ToInt32(record.check_in_distance),
                                    CHECK_OUT_DISTANCE = Convert.ToInt32(record.check_out_distance)
                                });
                                if (CaregivertaskChildSchedules1.PLANNED_ENDTIME.Date != CaregivertaskChildSchedules1.PLANNED_STARTTIME.Date)
                                {
                                    CaregivertaskChildSchedules1.PLANNED_ENDTIME = Convert.ToDateTime(CaregivertaskChildSchedules1.SCHEDULE_DATE.AddDays(1).Date + " " + hhabillingpayrollsettings.BillingBusinessHoursStartTime);
                                }
                                else
                                {
                                    CaregivertaskChildSchedules1.PLANNED_ENDTIME = Convert.ToDateTime(CaregivertaskChildSchedules1.SCHEDULE_DATE + " " + hhabillingpayrollsettings.BillingBusinessHoursStartTime);

                                }

                                CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED = Common.ConvertStringToMins(
                                    Common.GetVisitEditedHours(CaregivertaskChildSchedules1.PLANNED_STARTTIME, CaregivertaskChildSchedules1.PLANNED_ENDTIME,  payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes)
                                    );
                                CaregivertaskChildSchedules1.TOTALMINUTES_ACTUAL = CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED;
                                CaregivertaskChildSchedules1.TOTALMINUTES_ACTUAL_PAYABLE = CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED;
                                CaregivertaskChildSchedules1.Units = await GetVisitUnits(hha, user, payeradditionalinfo, record.service_code_id, CaregivertaskChildSchedules1.ACTUAL_STARTTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules1.ACTUAL_STARTTIME) :
                      Convert.ToDateTime(CaregivertaskChildSchedules1.PLANNED_STARTTIME), CaregivertaskChildSchedules1.ACTUAL_ENDTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules1.ACTUAL_ENDTIME) :
                         Convert.ToDateTime(CaregivertaskChildSchedules1.ACTUAL_ENDTIME), clientsEntity.HHA_BRANCH_ID, 0);
                                CaregivertaskChildSchedules2 = (new CaregiverTaskChildSchedulesEntity
                                {
                                    SCHEDULE_DATE = payeradditionalinfo.EnableOvernightSplitScheduleToHaveSameDate ? Convert.ToDateTime(schedule_date) : CaregivertaskChildSchedules1.PLANNED_ENDTIME.Date,
                                    PLANNED_STARTTIME = CaregivertaskChildSchedules1.PLANNED_ENDTIME,
                                    PLANNED_ENDTIME = Convert.ToDateTime(record.planned_end_time),
                                    PAYMENT_SOURCE = record.payment_source_id,
                                    SERVICECODE_ID = record.service_code_id,
                                    CANBILL = (bool)record.canbill,
                                    CLIENT = record.client_id,
                                    POC = POC,
                                    PARENT_CGTASK_ID = cgtaskid_out,
                                    ACTUAL_DATE = Convert.ToDateTime(record.schedule_date).Date,
                                    isManualSplit = true,
                                    IsAuthMandatory = record.is_auth_mand,
                                    isEvvschedule = record.is_evvschedule,
                                    enablePayableHours = caregiver_taskdetails.enablePayableHours.Value,
                                    CHECK_IN_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_IN_LOCATION.Latitude + " " + Child_CHECK_IN_LOCATION.Longitude : null,
                                    CHECK_OUT_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_OUT_LOCATION.Latitude + " " + Child_CHECK_OUT_LOCATION.Longitude : null,
                                    CHECK_IN_DISTANCE =Convert.ToInt32( record.check_in_distance),
                                    CHECK_OUT_DISTANCE= Convert.ToInt32(record.check_out_distance )
                                });

                                CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED = Common.ConvertStringToMins(
                                    Common.GetVisitEditedHours(CaregivertaskChildSchedules2.PLANNED_STARTTIME, CaregivertaskChildSchedules2.PLANNED_ENDTIME,  payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes)
                                    );
                                CaregivertaskChildSchedules2.TOTALMINUTES_ACTUAL = CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED;
                                CaregivertaskChildSchedules2.TOTALMINUTES_ACTUAL_PAYABLE = CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED;

                                CaregivertaskChildSchedules2.Units = await GetVisitUnits(hha, user, payeradditionalinfo, record.service_code_id, CaregivertaskChildSchedules2.ACTUAL_STARTTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules2.ACTUAL_STARTTIME) :
                      Convert.ToDateTime(CaregivertaskChildSchedules2.PLANNED_STARTTIME), CaregivertaskChildSchedules2.ACTUAL_ENDTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules2.ACTUAL_ENDTIME) :
                         Convert.ToDateTime(CaregivertaskChildSchedules2.ACTUAL_ENDTIME), clientsEntity.HHA_BRANCH_ID, 0);
                                List<CaregiverTaskChildSchedulesEntity> obj = new List<CaregiverTaskChildSchedulesEntity>();
                                obj.Add(CaregivertaskChildSchedules1);
                                obj.Add(CaregivertaskChildSchedules2);
                                var ChildScheduleJson = JsonConvert.SerializeObject(obj);
                                await _caregivertasksRepository.CreateChildSchedules(hha, user, ChildScheduleJson, record.SplitOvernightSchedulesForBilling, record.SplitOvernightSchedulesForPayroll);

                            }

                        }

                        else
                        {
                            if(AllowSplit)
                            {
                                if (record.SplitOvernightSchedulesForPayroll || record.SplitOvernightSchedulesForBilling)
                                {

                                    record.has_child_schedules = true;
                                    CaregivertaskChildSchedules1 = (new CaregiverTaskChildSchedulesEntity
                                    {
                                        SCHEDULE_DATE = Convert.ToDateTime(schedule_date),
                                        PLANNED_STARTTIME = Convert.ToDateTime(schedule_date + " " + Convert.ToDateTime(record.planned_start_time).TimeOfDay),
                                        PLANNED_ENDTIME = Convert.ToDateTime(schedule_date).AddDays(1),
                                        PAYMENT_SOURCE = record.payment_source_id,
                                        SERVICECODE_ID = record.service_code_id,
                                        CANBILL = (bool)caregiver_taskdetails.CanBill,
                                        CLIENT = record.client_id,
                                        POC = POC,
                                        PARENT_CGTASK_ID = cgtaskid_out,
                                        ACTUAL_DATE = Convert.ToDateTime(record.schedule_date).Date,
                                        ACTUAL_STARTTIME = record.check_in_time != null ? Convert.ToDateTime(schedule_date + " " + Convert.ToDateTime(record.check_in_time).TimeOfDay) : Convert.ToDateTime(schedule_date + " " + Convert.ToDateTime(record.planned_start_time).TimeOfDay),
                                        ACTUAL_ENDTIME =   Convert.ToDateTime(schedule_date).AddDays(1)  ,
                                        isManualSplit = true,
                                        IsAuthMandatory = record.is_auth_mand,
                                        isEvvschedule = record.is_evvschedule,
                                        enablePayableHours =caregiver_taskdetails.enablePayableHours.Value,
                                        CHECK_IN_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_IN_LOCATION.Latitude+" "+ Child_CHECK_IN_LOCATION.Longitude:null,
                                        CHECK_OUT_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_OUT_LOCATION.Latitude + " " + Child_CHECK_OUT_LOCATION.Longitude : null,
                                        CHECK_IN_DISTANCE = Convert.ToInt32(record.check_in_distance),
                                        CHECK_OUT_DISTANCE = Convert.ToInt32(record.check_out_distance)
                                    });
                                   
                                    CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED = Common.ConvertStringToMins(
                                        Common.GetVisitEditedHours(CaregivertaskChildSchedules1.ACTUAL_STARTTIME.Value, CaregivertaskChildSchedules1.ACTUAL_ENDTIME.Value, payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes)
                                        );

                                    CaregivertaskChildSchedules1.TOTALMINUTES_ACTUAL = CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED;
                                    CaregivertaskChildSchedules1.TOTALMINUTES_ACTUAL_PAYABLE = CaregivertaskChildSchedules1.TOTALMINUTES_PLANNED;

                                    CaregivertaskChildSchedules1.Units = await GetVisitUnits(hha, user, payeradditionalinfo, record.service_code_id, CaregivertaskChildSchedules1.ACTUAL_STARTTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules1.ACTUAL_STARTTIME) :
                           Convert.ToDateTime(CaregivertaskChildSchedules1.PLANNED_STARTTIME), CaregivertaskChildSchedules1.ACTUAL_ENDTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules1.ACTUAL_ENDTIME) :
                              Convert.ToDateTime(CaregivertaskChildSchedules1.PLANNED_ENDTIME), clientsEntity.HHA_BRANCH_ID, CaregivertaskChildSchedules1.PARENT_CGTASK_ID);

                                    CaregivertaskChildSchedules2 = (new CaregiverTaskChildSchedulesEntity
                                    {
                                        SCHEDULE_DATE = payeradditionalinfo.EnableOvernightSplitScheduleToHaveSameDate ? Convert.ToDateTime(schedule_date) : CaregivertaskChildSchedules1.PLANNED_ENDTIME.Date,
                                        PLANNED_STARTTIME = Convert.ToDateTime(caregiver_taskdetails.PLANNED_END_TIME.Date),
                                        PLANNED_ENDTIME = caregiver_taskdetails.PLANNED_END_TIME,
                                        PAYMENT_SOURCE = record.payment_source_id,
                                        SERVICECODE_ID = record.service_code_id,
                                        CANBILL = (bool)caregiver_taskdetails.CanBill,
                                        CLIENT = record.client_id,
                                        POC = POC,
                                        PARENT_CGTASK_ID = cgtaskid_out,
                                        ACTUAL_DATE = Convert.ToDateTime(record.schedule_date).Date,
                                        ACTUAL_STARTTIME = record.check_in_time != null ? Convert.ToDateTime(caregiver_taskdetails.ACCTUAL_END_TIME.Value.Date) : Convert.ToDateTime(caregiver_taskdetails.PLANNED_END_TIME.Date),
                                        ACTUAL_ENDTIME = record.check_out_time != null ? caregiver_taskdetails.ACCTUAL_END_TIME : caregiver_taskdetails.PLANNED_END_TIME,
                                        isManualSplit = true,
                                        IsAuthMandatory = record.is_auth_mand,
                                        isEvvschedule = record.is_evvschedule,
                                        enablePayableHours = caregiver_taskdetails.enablePayableHours.Value,
                                        CHECK_IN_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_IN_LOCATION.Latitude + " " + Child_CHECK_IN_LOCATION.Longitude : null,
                                        CHECK_OUT_LOCATION = !Child_CHECK_IN_LOCATION.IsUnknown ? Child_CHECK_OUT_LOCATION.Latitude + " " + Child_CHECK_OUT_LOCATION.Longitude : null,
                                        CHECK_IN_DISTANCE = Convert.ToInt32(record.check_in_distance),
                                        CHECK_OUT_DISTANCE = Convert.ToInt32(record.check_out_distance)

                                    });
                                    CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED = Common.ConvertStringToMins(
                                        Common.GetVisitEditedHours(CaregivertaskChildSchedules2.ACTUAL_STARTTIME.Value, CaregivertaskChildSchedules2.ACTUAL_ENDTIME.Value, payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes)
                                        );
                                    CaregivertaskChildSchedules2.TOTALMINUTES_ACTUAL = CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED;
                                    CaregivertaskChildSchedules2.TOTALMINUTES_ACTUAL_PAYABLE = CaregivertaskChildSchedules2.TOTALMINUTES_PLANNED;

                                    CaregivertaskChildSchedules2.Units = await GetVisitUnits(hha, user, payeradditionalinfo, record.service_code_id, CaregivertaskChildSchedules2.ACTUAL_STARTTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules2.ACTUAL_STARTTIME) :
                         Convert.ToDateTime(CaregivertaskChildSchedules2.PLANNED_STARTTIME), CaregivertaskChildSchedules2.ACTUAL_ENDTIME != null ? Convert.ToDateTime(CaregivertaskChildSchedules2.ACTUAL_ENDTIME) :
                            Convert.ToDateTime(CaregivertaskChildSchedules2.PLANNED_ENDTIME), clientsEntity.HHA_BRANCH_ID, CaregivertaskChildSchedules2.PARENT_CGTASK_ID);

                                    List<CaregiverTaskChildSchedulesEntity> obj = new List<CaregiverTaskChildSchedulesEntity>();
                                    obj.Add(CaregivertaskChildSchedules1);
                                    obj.Add(CaregivertaskChildSchedules2);
                                    var ChildScheduleJson = JsonConvert.SerializeObject(obj);
                                        await _caregivertasksRepository.CreateChildSchedules(hha, user, ChildScheduleJson, record.SplitOvernightSchedulesForBilling, record.SplitOvernightSchedulesForPayroll);
                                }
                            }
                        }
                    }
                }
                AuditDesc = "A schedule has been Created on " + record.schedule_date;
                if (record.caregiver_id > 0)
                {
                    var caregiversEntity = await _caregiversRepository.GetCaregiverBasicInfo(hha, user, record.caregiver_id);

                    AuditDesc += " for the clinician: " + caregiversEntity.FIRST_NAME + ", " + caregiversEntity.LAST_NAME;
                }
                if (record.payment_source_id > 0)
                {
                    AuditDesc += " for the Payer: " + allscheduleDetaisobj.paymentsourcesEntity.ORG_NAME;
                }

                if (record.service_code_id > 0)
                {
                    AuditDesc += " for the Service: " + allscheduleDetaisobj.serviceCodesEntity.DESCRIPTION;
                }
                if (record.planned_start_time != "" && record.planned_end_time != "")
                {
                    AuditDesc += " and Planned Start Time: " + Convert.ToDateTime(record.planned_start_time).ToString("hh:mm")
                        + " and Planned End Time:" + Convert.ToDateTime(record.planned_end_time).ToString("hh:mm tt");
                }
                if (record.check_in_time != "")
                {
                    AuditDesc += " and Check-in Time: " + Convert.ToDateTime(record.check_in_time).ToString("hh:mm tt");
                }
                if (record.check_out_time != "")
                {
                    AuditDesc += " and Check-out Time:" + Convert.ToDateTime(record.check_out_time).ToString("hh:mm tt");
                    
                }


                    var AuditInfo = new HModificationActivityLogEntity();
                AuditInfo.ActivityName = "Create Schedule";
                AuditInfo.ActivityType = 5;
                AuditInfo.ClientNo = record.client_id;
                AuditInfo.ClinicianID = record.caregiver_id;
                AuditInfo.RecordIdentityName = "CGTASK_ID";
                AuditInfo.RecordIdentityValue = cgtaskid_out;
                AuditInfo.UserDefined1 = record.caregiver_id.ToString();
                AuditInfo.Description = AuditDesc;
                AuditInfo.UserDefined2 = "api/schedules/v1/Create";
                AuditInfo.isSingleActivity = true;
                string AuditInfoJSON = JsonConvert.SerializeObject(AuditInfo);
                await _hModificationActivityLog.AddAuditData(hha, user, AuditInfoJSON);

                //int SchedulesLength = request.scheduleCreateSingleCommands.Count;
                //int EndBatch = 0;

                //for (int i= 0; i<SchedulesLength;)
                //{
                //    if (i + 10 >= SchedulesLength)
                //    {
                //        EndBatch = SchedulesLength;
                //    }
                //    else
                //        EndBatch = i + 10;

                //   var batchlist=    request.scheduleCreateSingleCommands.GetRange(i, EndBatch);
                //    var ScheduleJson = JsonConvert.SerializeObject(batchlist.Select(x=>x.caregiver_taskdetails));
                //    _taskQueue.QueueBackgroundWorkItem(async token =>
                //    {

                //        await _caregivertasksRepository.CreateSchedule(hha, user, ScheduleJson);
                //    });
                //    i = EndBatch+1;
                //}




            }
            return errorList;

        }

        public async Task<string> CalculateOverageHoursRequired(int HHA,int UserId, EditScheduleProperties editSchedulePropertiesmodel, CaregivertasksEntity oldcaregivertasksEntity)
        {
            string isoverhoursrequired = "0";
            if (editSchedulePropertiesmodel.CanBill == true)
            {
                if (oldcaregivertasksEntity.ACCTUAL_END_TIME == null && editSchedulePropertiesmodel.ACCTUAL_END_TIME != null)
                {

                    //getting the overrage settings in schedule settings level In UI
                    var HHAScheduleAuthSettingsEntity = await _hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(HHA, UserId);

                    //getting the clinician dicipline
                    string Columns = "Discipline";
                    int Caregiver_disciplineID = 0;
                    string filters = _repositoryFilterConditions.EqualFilter("CaregiverID", oldcaregivertasksEntity.CAREGIVER ?? 0, FilterConditionDataTypeEnums.integerType)
                                                               .BetweenFilter("\'" + editSchedulePropertiesmodel.PLANNED_DATE + "\'", "CaregiverDisciplineHistory.effectiveFrom", "CaregiverDisciplineHistory.efffectiveTill", FilterConditionDataTypeEnums.dateType)
                                                               .Build();
                    var CaregiverDisciplineHistory_Entity = await _caregiverDisciplineHistoryRepository.GetCaregiverDiscipineHistory(HHA, UserId, Columns, filters);
                    if (CaregiverDisciplineHistory_Entity.Count() > 0)
                    {
                        Caregiver_disciplineID = CaregiverDisciplineHistory_Entity.FirstOrDefault().Discipline;
                    }
                    else
                    {
                        int caregiverid = oldcaregivertasksEntity.CAREGIVER ?? 0;
                        string columns = "Caregivers.DISCIPLINE";
                        string filter = _repositoryFilterConditions.EqualFilter("CAREGIVER_ID", caregiverid, FilterConditionDataTypeEnums.integerType)
                                                                    .Build();
                        var CaregiversEntity = await _caregiversRepository.GetCaregivers(HHA, UserId, columns, filter);
                        Caregiver_disciplineID = CaregiversEntity.FirstOrDefault().DISCIPLINE;


                    }


                    //getting the payer information details
                    string column_s = "PaymentSourcesAdditional2.PrimaryEvvVendorID";
                    string filter_s = "And PaymentSourcesAdditional2.Paymentsourceid=" + (editSchedulePropertiesmodel.PAYMENT_SOURCE??0).ToString();
                    var PaymentsourcesEntityinformation = await _paymentsourcesRepository.GetPaymentsources(HHA, UserId, column_s, filter_s);

                    if (PaymentsourcesEntityinformation.Count() > 0 || HHAScheduleAuthSettingsEntity.Count() > 0)
                    {
                        bool isoverhour = Common.ScheduleOverageHoursAsync(HHAScheduleAuthSettingsEntity.FirstOrDefault(), editSchedulePropertiesmodel, PaymentsourcesEntityinformation.FirstOrDefault(), oldcaregivertasksEntity, Caregiver_disciplineID);
                        if (isoverhour)
                            isoverhoursrequired = "1";
                    }
                }
            }
            return isoverhoursrequired;
        }
        
        public async Task<Response<IEnumerable<ValidationErrorInfo>>> ValidateModifyScheduleProperties(int HHA, int User, CaregivertasksEntity oldScheduleData, CaregivertasksEntity newScheduleData)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>();
            string Columns;
            string Filters;
            ClientsEntity clientInfo;
            ClientPaymentSourcesEntity clientPayer;
            IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriods;
            CaregiversEntity caregiverInfo = null;
            ServiceCodesEntity serviceInfo;
            PaymentsourcesEntity payerInfo;
            EpisodeEntity scheduleEpisodeInfo;
            IEnumerable<UserRolesEntity> userPemissionList;
            _UA_FormsEntity serviceFormInfo;
            HConfigurations2Entity hConfig2Info;
            HHABranchScheduleConfigurationsEntity agencyBranchConfigInfo;
            int timepoint = 0;
            EpisodeEntity firstEpisodeInfo;
            eChartMasterEntity eChartMasterInfo = new eChartMasterEntity();
            pChartMasterEntity pChartMasterInfo = new pChartMasterEntity();

            try
            {
                // Get data
                #region getting_data
                // Get client branch
                Columns = "Clients.CLIENT_ID,"
                    + "Clients.HHA_BRANCH_ID,"
                    + "ClientAdditionalDetails.IsChildCare,"
                    + "Clients.AssessmentSequence,"
                    + "Clients.DischargeDate";
                Filters = _repositoryFilterConditions.EqualFilter("Clients.CLIENT_ID", newScheduleData.CLIENT_ID, FilterConditionDataTypeEnums.integerType).Build();
                var clientsInfo = await _clientsRepository.GetALLClients(HHA, User, Columns, Filters);
                if (!clientsInfo.Any())
                {
                    _logger.LogError("Not found any client records for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                clientInfo = clientsInfo.FirstOrDefault();

                // Get Client Payer Info
                Columns = "ClientPaymentSources.CLIENT_PAYMENT_ID,"
                    + "ClientPaymentSources.CLIENT_ID,"
                    + "ClientPaymentSources.PAYMENT_SOURCE_ID,"
                    + "ClientPaymentSources.SchedulingRule";
                Filters = _repositoryFilterConditions.EqualFilter("ClientPaymentSources.PAYMENT_SOURCE_ID", newScheduleData.PAYMENT_SOURCE, FilterConditionDataTypeEnums.integerType)
                    .EqualFilter("ClientPaymentSources.CLIENT_ID", newScheduleData.CLIENT_ID, FilterConditionDataTypeEnums.integerType).Build();
                var clientPayers = await _clientsRepository.GetClientPaymentSourcesList(HHA, User, Columns, Filters, "");
                if (!clientPayers.Any())
                {
                    _logger.LogError("Not found any client payer records for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                clientPayer = clientPayers.FirstOrDefault();

                // Get Client Payer Effective Periods info
                Columns = "ClientPayerEffectivePeriods.ClientPaymentSourceID,"
                    + "ClientPayerEffectivePeriods.EffectiveStart,"
                    + "ClientPayerEffectivePeriods.EffectiveEnd";
                Filters = _repositoryFilterConditions.EqualFilter("ClientPayerEffectivePeriods.ClientPaymentSourceID",
                    clientPayer.CLIENT_PAYMENT_ID, FilterConditionDataTypeEnums.integerType)
                    .EqualFilter("PrimaryPayerType", "P", FilterConditionDataTypeEnums.stringType)
                    .Build();
                clientPayerEffectivePeriods = await _clientsRepository.GetClientPayerEffectivePeriods(HHA, User, Columns, Filters);

                // Get caregiver info
                if (newScheduleData.CAREGIVER != null)
                {
                    Columns = "Caregivers.CAREGIVER_ID";
                    Filters = _repositoryFilterConditions.EqualFilter("Caregivers.CAREGIVER_ID", newScheduleData.CAREGIVER, FilterConditionDataTypeEnums.integerType)
                        .Build();
                    var caregiversEntities = await _caregiversRepository.GetCaregivers(HHA, User, Columns, Filters);
                    var caregiverAdditionalInfo = await _caregiversRepository.GetCaregiverAdditionalInfo(HHA, User, newScheduleData.CAREGIVER.Value);
                    if (!caregiversEntities.Any())
                    {
                        _logger.LogError("Not found any caregiver records for filters: " + Filters);
                        return Response.NoContent(errorList.AsEnumerable());
                    }
                    caregiverInfo = caregiversEntities.FirstOrDefault();
                    caregiverInfo.BlockFromFurtherScheduling = caregiverAdditionalInfo.BlockFromFurtherScheduling;
                    caregiverInfo.BlockSchedulingEffectiveDate = caregiverAdditionalInfo.BlockSchedulingEffectiveDate;
                }

                // Get service details
                Columns = "ServiceCodes.SERVICE_CODE_ID,"
                    + "ServiceCodes.IsTerminated,"
                    + "ServiceCodes.effectiveEndDate,"
                    + "ServiceCodes.FormID";
                Filters = _repositoryFilterConditions.EqualFilter("ServiceCodes.SERVICE_CODE_ID", newScheduleData.SERVICECODE_ID, FilterConditionDataTypeEnums.integerType)
                    .Build();
                var servicesInfo = await _serviceCodesRepository.GetServiceCodes(HHA, User, Columns, Filters);
                var serviceAdditionalInfo = await _serviceCodesRepository.GetServiceCodeAdditionalInfo(HHA, User, newScheduleData.SERVICECODE_ID, clientInfo.HHA_BRANCH_ID);
                if (!servicesInfo.Any())
                {
                    _logger.LogError("Not found any service records for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                serviceInfo = servicesInfo.FirstOrDefault();
                serviceInfo.DonotAllowClinicianToReschedule = serviceAdditionalInfo.DonotAllowClinicianToReschedule;

                // Get payer info
                Columns = "PaymentSources.PAYMENT_SOURCE_ID,"
                    + "PaymentSources.PayerPrimaryType,"
                    + "PaymentSources.IsEpisodicBilling";
                Filters = _repositoryFilterConditions.EqualFilter("PaymentSources.PAYMENT_SOURCE_ID", newScheduleData.PAYMENT_SOURCE, FilterConditionDataTypeEnums.integerType)
                    .Build();
                var payersInfo = await _paymentsourcesRepository.GetPaymentsources(HHA, User, Columns, Filters);
                if (!payersInfo.Any())
                {
                    _logger.LogError("Not found any payer records for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                payerInfo = payersInfo.FirstOrDefault();

                // Get episode info
                var clientEpisodesInfo = await _episodeRepository._CL_GetClientEpisodes(HHA, User, newScheduleData.CLIENT_ID);
                if (!clientEpisodesInfo.Any())
                {
                    _logger.LogError("Not found any episodes for client: " + newScheduleData.CLIENT_ID);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                scheduleEpisodeInfo = clientEpisodesInfo.Where(x => x.POC == newScheduleData.POC).FirstOrDefault();
                firstEpisodeInfo = clientEpisodesInfo.Where(x => x.EpStartDate == clientEpisodesInfo.Min(x => x.EpStartDate)).FirstOrDefault();

                // Get User Roles info
                Columns = "RolePermissions.RolePermissionID,"
                    + "RolePermissions.RoleID,"
                    + "RolePermissions.HHA,"
                    + "RolePermissions.PermissionIdx,"
                    + "Users.USER_ID,"
                    + "Users.User_Type";
                Filters = _repositoryFilterConditions.EqualFilter("Users.USER_ID", User, FilterConditionDataTypeEnums.integerType)
                    .Build();
                userPemissionList = await _userRolesRepository.GetUserEnabledPermissionList(HHA, User, Columns, Filters);
                if (!userPemissionList.Any())
                {
                    _logger.LogError("Not found any permissions for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }

                // Get HConfigurations2 data
                Columns = "HConfigurations2.HHA_BRANCHID," +
                    "HConfigurations2.WEEK_START_DAY";
                Filters = _repositoryFilterConditions.EqualFilter("HConfigurations2.HHA_BRANCHID", clientInfo.HHA_BRANCH_ID, FilterConditionDataTypeEnums.integerType)
                    .Build();
                hConfig2Info = await _hConfigurations2Repository._S_Schedule_GetHConfiguration2Settings(HHA, User, Columns, Filters);
                
                // Get _C_Forms
                Columns = "_C_Forms.HHA_FormId," +
                    "_UA_Forms.isPreSOCForm";
                Filters = _repositoryFilterConditions.EqualFilter("_C_Forms.HHA_FormId", serviceInfo.FormID, FilterConditionDataTypeEnums.integerType)
                    .Build();
                var serviceFormsInfo = await _serviceCodesRepository.GetUA_FormsEntities(HHA, User, Columns, Filters);
                if (!serviceFormsInfo.Any())
                {
                    _logger.LogError("Not found any forms for filters: " + Filters);
                    return Response.NoContent(errorList.AsEnumerable());
                }
                serviceFormInfo = serviceFormsInfo.FirstOrDefault();

                // Get agency branch info
                agencyBranchConfigInfo = await _hHABranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(HHA, User, clientInfo.HHA_BRANCH_ID);

                var weekStartDate = oldScheduleData.PLANNED_DATE.AddDays(-(int)(7 + (oldScheduleData.PLANNED_DATE.DayOfWeek - hConfig2Info.WEEK_START_DAY - 1)) % 7).Date;
                var weekEndDate = weekStartDate.AddDays(6);

                // eChart Data
                if(newScheduleData.eChartMasterID > 0)
                {
                    Columns = "eChartMasterID, TimePoint, isSubmitted, isApproved";
                    Filters = _repositoryFilterConditions.EqualFilter("eChartMasterID", newScheduleData.eChartMasterID, FilterConditionDataTypeEnums.integerType)
                        .Build();
                    var eChartMasterList = await _eChartMasterRepository.GetEChartMasterList(HHA, User, Columns, Filters);
                    if (!eChartMasterList.Any())
                    {
                        _logger.LogError("Not found eChart info for filters: " + Filters);
                        return Response.NoContent(errorList.AsEnumerable());
                    }
                    eChartMasterInfo = eChartMasterList.FirstOrDefault();
                    timepoint = eChartMasterInfo.TimePoint;
                }

                // pChart Data
                if(newScheduleData.pChartMasterID > 0)
                {
                    Columns = "pChartMasterID, TimePoint, pChartSubmitted, pChartApproved";
                    Filters = _repositoryFilterConditions.EqualFilter("pChartMasterID", newScheduleData.pChartMasterID, FilterConditionDataTypeEnums.integerType)
                        .Build();
                    var pChartMasterList = await _pChartMasterRepository.GetPChartMasterList(HHA, User, Columns, Filters);
                    if (!pChartMasterList.Any())
                    {
                        _logger.LogError("Not found pChart info for filters: " + Filters);
                        return Response.NoContent(errorList.AsEnumerable());
                    }
                    pChartMasterInfo = pChartMasterList.FirstOrDefault();
                    timepoint = pChartMasterInfo.TimePoint;
                }

                // Oasis Data
                if(newScheduleData.oasisDatasetID > 0)
                {
                    Columns = "DATASET_ID, TIME_POINT";
                    Filters = _repositoryFilterConditions.EqualFilter("DATASET_ID", newScheduleData.oasisDatasetID, FilterConditionDataTypeEnums.integerType)
                        .Build();
                    var OasisDatasetsInfo = await _oasisDatasetsRepository.GetOasisDatasetsList(HHA, User, Columns, Filters);
                    if (!OasisDatasetsInfo.Any())
                    {
                        _logger.LogError("Not found Oasis info for filters: " + Filters);
                        return Response.NoContent(errorList.AsEnumerable());
                    }

                    switch (OasisDatasetsInfo.FirstOrDefault().TIME_POINT)
                    {
                        case "SOC":
                            timepoint = 1;
                            break;
                        case "ROC":
                            timepoint = 3;
                            break;
                        case "RECERT":
                            timepoint = 4;
                            break;
                        case "OTHFLUP":
                            timepoint = 5;
                            break;
                        case "TINPNOTDIS":
                            timepoint = 6;
                            break;
                        case "TINPDIS":
                            timepoint = 7;
                            break;
                        case "DETH":
                            timepoint = 8;
                            break;
                        case "DISCH":
                            timepoint = 9;
                            break;
                        default:
                            timepoint = 0;
                            break;
                    }
                }
                #endregion

                // Vadliate Data
                #region validation_region
                // Validate Payer Change
                if (oldScheduleData.PAYMENT_SOURCE != newScheduleData.PAYMENT_SOURCE)
                {
                    SchedulePayerValidationHandler schedulePayerValidationHandler = new SchedulePayerValidationHandler(HHA, User, clientPayerEffectivePeriods);
                    errorList.AddRange(schedulePayerValidationHandler.HandleValidation(newScheduleData));
                }

                // Validate Data Change
                if(oldScheduleData.PLANNED_DATE.Date != newScheduleData.PLANNED_DATE.Date)
                {
                    ScheduleDateValidationHandler scheduleDateValidationHandler = new ScheduleDateValidationHandler(HHA, User, oldScheduleData,
                        _repositoryFilterConditions, _clientsRepository, _caregivertasksRepository, _claimsRepository, payerInfo, clientPayer,
                        clientPayerEffectivePeriods, serviceInfo, caregiverInfo, scheduleEpisodeInfo, hConfig2Info, serviceFormInfo, userPemissionList,
                        timepoint, firstEpisodeInfo, agencyBranchConfigInfo, clientInfo);
                    errorList.AddRange(scheduleDateValidationHandler.HandleValidation(newScheduleData));
                }

                // Validate Clinician Change
                if(oldScheduleData.CAREGIVER != newScheduleData.CAREGIVER)
                {
                    // Unassign Clinician
                    if(newScheduleData.CAREGIVER == 0)
                    {
                        throw new NotImplementedException();
                    }
                    else
                    {
                        // Change Clinician
                        ClinicianChangeValidationHandler clinicianChangeValidationHandler = new ClinicianChangeValidationHandler(HHA, User, agencyBranchConfigInfo);
                        errorList.AddRange(clinicianChangeValidationHandler.HandleValidation(newScheduleData));
                    }
                }

                // Validate Approve Status
                if(newScheduleData.STATUS == ScheduleStatusEnums.Approved.ToString() && oldScheduleData.STATUS != ScheduleStatusEnums.Approved.ToString())
                {
                    ScheduleApproveValidationHandler scheduleApproveValidationHandler = new ScheduleApproveValidationHandler(HHA, User);
                    errorList.AddRange(scheduleApproveValidationHandler.HandleValidation(newScheduleData));
                }

                // Validate chart type
                if(newScheduleData.Chart_Type != oldScheduleData.Chart_Type)
                {
                    ChartTypeValidationHandler chartTypeValidationHandler = new ChartTypeValidationHandler(HHA, User, oldScheduleData,
                        _repositoryFilterConditions, _visitChartsRepository, eChartMasterInfo, pChartMasterInfo);
                    errorList.AddRange(chartTypeValidationHandler.HandleValidation(newScheduleData));
                }

                // Validate Service
                if(newScheduleData.SERVICECODE_ID != oldScheduleData.SERVICECODE_ID)
                {
                    ScheduleServiceChangeValidationHandler scheduleServiceChangeValidationHandler =
                        new ScheduleServiceChangeValidationHandler(HHA, User, serviceInfo);
                    errorList.AddRange(scheduleServiceChangeValidationHandler.HandleValidation(newScheduleData));
                }

                // Validate Billable flag change
                if(newScheduleData.CanBill != oldScheduleData.CanBill)
                {
                    ScheduleBillableChangeValidationHandler billableChangeValdiationHandler =
                        new ScheduleBillableChangeValidationHandler(HHA, User, agencyBranchConfigInfo, firstEpisodeInfo, _clientsRepository,
                        clientInfo, payerInfo, timepoint);
                    errorList.AddRange(billableChangeValdiationHandler.HandleValidation(newScheduleData));
                }
                #endregion
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.ToString());
                return Response.Fail<IEnumerable<ValidationErrorInfo>>(ResponseErrorCodes.InternalServerFailed, e.Message);
            }

            if (errorList.Any())
            {
                return Response.ValidationError(JsonConvert.SerializeObject(errorList), errorList.AsEnumerable());
            }

            return Response.Ok(errorList.AsEnumerable());
        }
        private EditScheduleProperties GetCreateScheduleModel(int HHA, int User, CaregivertasksEntity caregivertasksEntity)
        {
            //map ScheduleCheckinCommand to EditScheduleProperties

            EditScheduleProperties editScheduleProperties = new EditScheduleProperties();

            //Get Checkin date time

            editScheduleProperties.ACCTUAL_START_TIME = caregivertasksEntity.ACCTUAL_START_TIME;
            editScheduleProperties.ACCTUAL_END_TIME = caregivertasksEntity.ACCTUAL_END_TIME;
            editScheduleProperties.PLANNED_DATE = caregivertasksEntity.PLANNED_DATE;
            editScheduleProperties.ACCTUAL_DATE = caregivertasksEntity.ACCTUAL_DATE;
            editScheduleProperties.PLANNED_START_TIME = caregivertasksEntity.PLANNED_START_TIME;
            editScheduleProperties.PLANNED_END_TIME = caregivertasksEntity.PLANNED_END_TIME;
            editScheduleProperties.CLIENT_ID = caregivertasksEntity.CLIENT_ID;
            editScheduleProperties.STATUS = caregivertasksEntity.STATUS;
            editScheduleProperties.CheckInSource = caregivertasksEntity.CheckInSource;
            editScheduleProperties.CheckinTreatmentLocation = caregivertasksEntity.CheckinTreatmentLocation;
            if (editScheduleProperties.CheckinTreatmentLocation > 0)
                editScheduleProperties.CheckoutTreatmentLocation = editScheduleProperties.CheckinTreatmentLocation;
            editScheduleProperties.TotalHours = caregivertasksEntity.TotalHours;
            editScheduleProperties.Units = caregivertasksEntity.Units;
            editScheduleProperties.CanBill = caregivertasksEntity.CanBill;
            editScheduleProperties.IS_AUTH_MANDATORY = caregivertasksEntity.IS_AUTH_MANDATORY;
            editScheduleProperties.isAuthorized = caregivertasksEntity.isAuthorized;
            editScheduleProperties.Units = caregivertasksEntity.Units;
            editScheduleProperties.TotalHours = caregivertasksEntity.TotalHours;
            editScheduleProperties.SERVICECODE_ID = caregivertasksEntity.SERVICECODE_ID;
            editScheduleProperties.PAYMENT_SOURCE = caregivertasksEntity.PAYMENT_SOURCE;
            editScheduleProperties.EDITED_HOURS = caregivertasksEntity.EDITED_HOURS;
            editScheduleProperties.EDITED_HOURS_PAYABLE = caregivertasksEntity.EDITED_HOURS_PAYABLE;
            editScheduleProperties.CheckInSource = caregivertasksEntity.CheckInSource;
            editScheduleProperties.CheckOutSource = caregivertasksEntity.CheckOutSource;
            editScheduleProperties.AUTHORIZATION_ID = caregivertasksEntity.AUTHORIZATION_ID;

            return editScheduleProperties;
        }

        public async Task<Response<bool>> GenerateConflictExceptions(int HHA, int User, IEnumerable<ValidationErrorInfo> validationerrors, int CgtaskID)
        {
            try 
            {
                //Get all exceting exception for  schedule
                var exceptionColumn = "CaregiverTaskEvvExceptionID,SystemCode,isResolved";

                var exceptionFilter =
                    _repositoryFilterConditions.EqualFilter("CgTaskID", CgtaskID, FilterConditionDataTypeEnums.integerType)
                                                .EqualFilter("isConflictValidation", true, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                var exceptions = await _caregiverTaskEvvExceptionsRepository.GetCaregiverTaskEVVExceptions(HHA, User, exceptionColumn, exceptionFilter);

                //check is there any new exception to add
                var Newexceptions = validationerrors.Where(e => !exceptions.Select(x => x.SystemCode).Contains(e.validation_type));

                var NewexceptionsJson = JsonConvert.SerializeObject((from e in validationerrors
                                                                     select new
                                                                     {
                                                                         CGTASKID = CgtaskID,
                                                                         ERRORDESC = e.validation_error_message,
                                                                         ERRORCODE = e.validation_type,
                                                                         EVENT = ""
                                                                     }));
                var ret = await _caregiverTaskEvvExceptionsRepository.AddCaregiverTaskEVVExceptions_ValidationErrors(HHA, User, NewexceptionsJson);

                if(ret)
                    return Response.Ok(ret);
                else
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to add conflict Exceptions. ");
            }
            catch (Exception e) {
                _logger.LogError(e, e.ToString());
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, e.Message);

            }
        }

        public async Task<IEnumerable<ValidationErrorInfo>> ValidateSchedule(int HHA, int UserId, int CGTask_ID)
        {
            List<ValidationErrorInfo> errorList = new List<ValidationErrorInfo>();

            //get schedules info
            var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", CGTask_ID, FilterConditionDataTypeEnums.integerType)
                            .Build();

            var caregivertaskentity = await _caregivertasksRepository.GetAllSchedulesList(HHA, UserId,"", Filters);

            var caregivertask = caregivertaskentity.FirstOrDefault();

            if (caregivertask.STATUS != ScheduleStatusEnums.Approved.ToString() && caregivertask.STATUS != ScheduleStatusEnums.Completed.ToString())
            {
                errorList.Add(new ValidationErrorInfo() { validation_type=ResponseErrorCodes.InvalidInput,validation_error_message = "Schedule should be Completed/Approved" });
                return errorList;
            }

            ValidateScheduleRules validateScheduleRules = await _getScheduleBasicDetail.getScheduleDetails(HHA, UserId, caregivertask);
            validateScheduleRules.confirm_softwarning = false;

            //get client info
            var Client = await _clientsRepository.GetClientsBasicInfo(HHA, UserId, caregivertask.CLIENT_ID);

            int CAREGIVER = (caregivertask.CAREGIVER ?? 0);
            var scheduleValidationRulesDtos = validateScheduleRules.scheduleValidationRulesDtos;


            var doublebookvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DOUBLEBOOK").FirstOrDefault();
            var DoublBookvalidationHandler = new DoubleBookValidationHandler(doublebookvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiversRepository);

            var DeviationvalidationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "DEVIATION").FirstOrDefault();
            var DeviationvalidationHandler = new DeviationValidationHandler(DeviationvalidationRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _hHAScheduleAuthSettingsRepository, _hHABranchScheduleConfigurationsRepository);
            DoublBookvalidationHandler.SetNextValidation(DeviationvalidationHandler);

            var serviceMaxvalidationnRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SERVICEMAXORMINHOURS").FirstOrDefault();
            var serviceMaxvalidationHandler = new ServiceMaxOrMinHoursValidationHandler(serviceMaxvalidationnRule);
            DeviationvalidationHandler.SetNextValidation(serviceMaxvalidationHandler);

            //CLIENTTNOTPREFERREDTHISCLINICIAN
            var clientNotPreferredThisClinicianRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTTNOTPREFERREDTHISCLINICIAN").FirstOrDefault();
            var clientNotPreferredThisClinicianHandler = new ClientNotPreferredThisClinicianValidationHandler(clientNotPreferredThisClinicianRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            serviceMaxvalidationHandler.SetNextValidation(clientNotPreferredThisClinicianHandler);

            //CLINICIANONVACATION
            var clinicianOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANONVACATION").FirstOrDefault();
            var clinicianOnVacationHandler = new ClinicianOnVacationValidationHandler(clinicianOnVacationRule, HHA, UserId, _caregiversRepository, _repositoryFilterConditions);
            clientNotPreferredThisClinicianHandler.SetNextValidation(clinicianOnVacationHandler);

            //CLIENTONVACATION
            var clientOnVacationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTONVACATION").FirstOrDefault();
            var clientOnVacationHandler = new ClientOnVacationValidationHandler(clientOnVacationRule, HHA, UserId, _clientsRepository, _repositoryFilterConditions);
            clinicianOnVacationHandler.SetNextValidation(clientOnVacationHandler);

            //CLINICIANRENEWABLEITEMSEXPIRED
            var clinicianRenewableItemsExpiredRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANRENEWABLEITEMSEXPIRED").FirstOrDefault();
            var clinicianRenewableItemsExpiredHandler = new ClinicianRenewableItemsExpiredValidationHandler(clinicianRenewableItemsExpiredRule, HHA, UserId, _cExpiringItemsRepository, _eExpiringItemsRepository, _repositoryFilterConditions);
            clientOnVacationHandler.SetNextValidation(clinicianRenewableItemsExpiredHandler);

            //CLIENTMAXHOURS
            var clientMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLIENTMAXHOURS").FirstOrDefault();
            var clientMaxHoursHandler = new ClientMaxHoursValidationHandler(clientMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions);
            clinicianRenewableItemsExpiredHandler.SetNextValidation(clientMaxHoursHandler);

            //CLINICIANMAXHOURS
            var clinicianMaxHoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "CLINICIANMAXHOURS").FirstOrDefault();
            var clinicianMaxHoursHandler = new ClinicianMaxHoursValidationHandler(clinicianMaxHoursRule, HHA, UserId, _caregivertasksRepository, _repositoryFilterConditions, _caregiverMaximumHoursEffectivePeriods);
            clientMaxHoursHandler.SetNextValidation(clinicianMaxHoursHandler);

            //OVERTIME
            var overtimeRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "OVERTIME").FirstOrDefault();
            var overtimeHandler = new OvertimeValidationHandler(overtimeRule);
            clinicianMaxHoursHandler.SetNextValidation(overtimeHandler);

            //GRACEPERIOD

            //VF
            var visitFrequencyRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "VF").FirstOrDefault();
            var visitFrequencyHandler = new VisitFrequencyValidationHandler(visitFrequencyRule, HHA, UserId, _caregivertasksRepository);
            overtimeHandler.SetNextValidation(visitFrequencyHandler);

            //AUTHORIZATION
            var authorizationRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "AUTHORIZATION").FirstOrDefault();
            var authorizationHandler = new AuthorizationValidationHandler(authorizationRule, HHA, UserId, _caregivertasksRepository);
            visitFrequencyHandler.SetNextValidation(authorizationHandler);

            //SCHEDULEOVER24HOURS
            var scheduleover24HoursRule = scheduleValidationRulesDtos.Where(x => x.ValidationType == "SCHEDULEOVER24HOURS").FirstOrDefault();
            var scheduleOver24HourValidationHandler = new ScheduleOver24HourValidationHandler(scheduleover24HoursRule, HHA, UserId, _paymentsourcesRepository);
            authorizationHandler.SetNextValidation(scheduleOver24HourValidationHandler);

            //EVVVALIDATION
            var evvCodeValidationRule = scheduleValidationRulesDtos.Where(x => (x.ValidationType == "NO_ATTESTATION") || (x.ValidationType == "NO_REASONCODE") || (x.ValidationType == "NO_ACTIONCODE")).FirstOrDefault();
            var eVVCodesValidationHandler = new EVVCodesValidationHandler(evvCodeValidationRule, HHA, UserId, _repositoryFilterConditions, _caregiverTaskEVVReasonsRepository);
            scheduleOver24HourValidationHandler.SetNextValidation(eVVCodesValidationHandler);

            errorList = DoublBookvalidationHandler.HandleValidation(validateScheduleRules);

            return errorList;

        }
    }
}

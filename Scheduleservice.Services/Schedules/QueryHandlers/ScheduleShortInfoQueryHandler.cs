﻿using AutoMapper;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using System; 
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Common.Extensions; 

namespace Scheduleservice.Services.Schedules.QueryHandlers
{ 
    public class ScheduleShortInfoQueryHandler : IHandlerWrapper<ScheduleShortInfoQuery, ScheduleShortInfoDto>
    {
        private readonly IMapper _mapper;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        public ScheduleShortInfoQueryHandler(ICaregivertasksRepository caregivertasksRepository, 
                IClientsRepository clientsRepository,
                ICaregiversRepository caregiversRepository,
                IServiceCodesRepository serviceCodesRepository,
                IPaymentsourcesRepository paymentsourcesRepository,
                IMapper mapper)
        {
            _caregivertasksRepository = caregivertasksRepository;
            _clientsRepository = clientsRepository;
            _caregiversRepository = caregiversRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _mapper = mapper;
        }
        public async Task<Response<ScheduleShortInfoDto>> Handle(ScheduleShortInfoQuery request, CancellationToken cancellationToken)
        {
            var _caregivertasksEntity = await _caregivertasksRepository.GetScheduleBasicInfo(request.HHA, request.UserId, request.CGTask_ID);
            var scheduleShortInfoDto = _mapper.Map<ScheduleShortInfoDto>(_caregivertasksEntity);

            var _clientsEntity = await _clientsRepository.GetClientsBasicInfo(request.HHA, request.UserId, _caregivertasksEntity.CLIENT_ID);
            scheduleShortInfoDto.CLIENT_NAME = new[] { _clientsEntity.FIRST_NAME, _clientsEntity.LAST_NAME, _clientsEntity.PATIENT_ID }.ClientNameFormat();

            if ((_caregivertasksEntity.CAREGIVER ?? 0) > 0)
            {
                var _caregiversEntity = await _caregiversRepository.GetCaregiverBasicInfo(request.HHA, request.UserId, _caregivertasksEntity.CAREGIVER ?? 0);
                scheduleShortInfoDto.CAREGIVER_NAME = new[] { _caregiversEntity.FIRST_NAME, _caregiversEntity.LAST_NAME, _caregiversEntity.DISCIPLINE_NAME }.ClinicianNameFormat();
            }

            var _serviceCodesEntity = await _serviceCodesRepository.GetServiceCodeBasicInfo(request.HHA, request.UserId, _caregivertasksEntity.SERVICECODE_ID);
            scheduleShortInfoDto.SERVICE_NAME = _serviceCodesEntity.DESCRIPTION;


            var _paymentsourcesEntity = await _paymentsourcesRepository.GetPaymentSourceBasicInfo(request.HHA, request.UserId, _caregivertasksEntity.PAYMENT_SOURCE);
            scheduleShortInfoDto.PAYMENT_SOURCE_NAME = _paymentsourcesEntity.ORG_NAME;


            return Response.Ok<ScheduleShortInfoDto>(scheduleShortInfoDto);
        }
    }
}

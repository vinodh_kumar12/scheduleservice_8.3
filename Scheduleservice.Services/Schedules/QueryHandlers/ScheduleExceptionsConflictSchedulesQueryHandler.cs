﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Entities;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleExceptionsConflictSchedulesQueryHandler : IHandlerWrapper<ScheduleExceptionsConflictSchedulesQuery, IEnumerable<ScheduleExceptionsConflictSchedulesDto>>
    {
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEVVExceptionsRepository;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ScheduleExceptionsConflictSchedulesQueryHandler(ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptionsRepository,
            ICaregivertasksRepository caregivertasksRepository,
        IClientsRepository clientsRepository,
        ICaregiversRepository caregiversRepository,
        IServiceCodesRepository serviceCodesRepository,
        IRepositoryFilterConditions repositoryFilterConditions)
        {
            _caregiverTaskEVVExceptionsRepository = caregiverTaskEVVExceptionsRepository;
            _caregivertasksRepository = caregivertasksRepository;
            _clientsRepository = clientsRepository;
            _caregiversRepository = caregiversRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }
        public async Task<Response<IEnumerable<ScheduleExceptionsConflictSchedulesDto>>> Handle(ScheduleExceptionsConflictSchedulesQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<ScheduleExceptionsConflictSchedulesDto> scheduleExceptionsConflictSchedulesDtos = new List<ScheduleExceptionsConflictSchedulesDto>();
            var DependentSchedules = await _caregiverTaskEVVExceptionsRepository.GetCaregiverTaskValidationErrorsDependentSchedules(request.HHA, request.UserId, request.CGTask_ID); ;
           
            var schedulesSelectedcolumns = "CGTASK_ID, CAREGIVER,PLANNED_DATE, PLANNED_START_TIME, PLANNED_END_TIME, ACCTUAL_DATE, ACCTUAL_START_TIME, ACCTUAL_END_TIME, STATUS, SERVICECODE_ID, CLIENT_ID";           
            var cgtaskids = string.Join(",", DependentSchedules.Select(x => x.dependentCgTaskID).ToList());
            var schedulesfilters = _repositoryFilterConditions.InFilter("CGTASK_ID", cgtaskids, FilterConditionDataTypeEnums.integerType).Build();
            var caregivertasks = await _caregivertasksRepository.GetScheduleBasicList(request.HHA, request.UserId, schedulesSelectedcolumns, schedulesfilters);
            
            string clientIDs = string.Join(",", caregivertasks.Select(x => x.CLIENT_ID).Distinct().ToList());
            var clientsfilters = _repositoryFilterConditions.InFilter("CLIENT_ID", clientIDs, FilterConditionDataTypeEnums.integerType).Build();
            var clients = await _clientsRepository.GetALLClients(request.HHA, request.UserId, "CLIENT_ID, FIRST_NAME, LAST_NAME, PATIENT_ID", clientsfilters);

            string caregiverIDs = string.Join(",", caregivertasks.Where(y => y.CAREGIVER > 0).Select(x => x.CAREGIVER).Distinct().ToList());
            var caregiversfilters= _repositoryFilterConditions.InFilter("CAREGIVER_ID", caregiverIDs, FilterConditionDataTypeEnums.integerType).Build();
            var caregivers = await _caregiversRepository.GetCaregivers(request.HHA, request.UserId, "CAREGIVER_ID, FIRST_NAME, LAST_NAME", caregiversfilters);

            string serviceCodeIDs = string.Join(",", caregivertasks.Select(x => x.SERVICECODE_ID).Distinct().ToList());
            var serviceCodesfilters = _repositoryFilterConditions.InFilter("SERVICE_CODE_ID", serviceCodeIDs, FilterConditionDataTypeEnums.integerType).Build();

            var serviceCodes = await _serviceCodesRepository.GetServiceCodes(request.HHA, request.UserId, "SERVICE_CODE_ID, Description", serviceCodesfilters);

            scheduleExceptionsConflictSchedulesDtos = (from schedule in caregivertasks  
                                                       from dependentschedule in DependentSchedules.Where(e => e.dependentCgTaskID== schedule.CGTASK_ID)
                                                       from client in clients.Where(e => schedule.CLIENT_ID == e.CLIENT_ID).DefaultIfEmpty(new ClientsEntity { })
                                                       from service in serviceCodes.Where(e => schedule.SERVICECODE_ID == e.SERVICE_CODE_ID).DefaultIfEmpty(new ServiceCodesEntity { })
                                                       from caregiver in caregivers.Where(e => schedule.CAREGIVER == e.CAREGIVER_ID).DefaultIfEmpty(new CaregiversEntity { })
                                                       select new ScheduleExceptionsConflictSchedulesDto
                                                       {
                                                           exception_type = dependentschedule.exception_type,
                                                           dependentCgTaskID = dependentschedule.dependentCgTaskID,
                                                           ScheduleDate = schedule.PLANNED_DATE.ToDateFormat(),
                                                           ScheduleClientID = schedule.CLIENT_ID,
                                                           ScheduleClient = new[] { client.LAST_NAME, client.FIRST_NAME, client.PATIENT_ID }.ClientNameFormat(),
                                                           ScheduleClincianID = schedule.CAREGIVER ?? 0,
                                                           ScheduleClincian = new[] { caregiver.LAST_NAME, caregiver.FIRST_NAME}.ClinicianNameFormat(),
                                                           ScheduleService = service.DESCRIPTION,
                                                           ScheduleStatus = schedule.STATUS,
                                                           PlannedTime = schedule.PLANNED_START_TIME.ToTimeAMPMFormat() + " - " + schedule.PLANNED_END_TIME.ToTimeAMPMFormat(),
                                                           ActualStartTime = schedule.ACCTUAL_START_TIME.ToTimeAMPMFormat(),
                                                           ActualEndTime =   schedule.ACCTUAL_END_TIME.ToTimeAMPMFormat()
                                                       });


            return Response.Ok<IEnumerable<ScheduleExceptionsConflictSchedulesDto>>(scheduleExceptionsConflictSchedulesDtos);

        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Common.Extensions;
using AutoMapper;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class EvvTerminalPayersQueryHandler : IHandlerWrapper<EvvTerminalPayersQuery, IEnumerable<EvvTerminalPayersDto>>
    {



        private readonly IMapper _mapper;
        private readonly IUsersRepository _userrepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        private readonly ICaregiverTasksTerminalPayerRepository _caregiverTasksTerminalPayerRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContext;
        

        public EvvTerminalPayersQueryHandler(IHttpContextAccessor httpContextAccessor, IRepositoryFilterConditions repositoryFilterConditions, ICaregiverTasksTerminalPayerRepository caregiverTasksTerminalPayerRepository, IMapper mapper)
        {
            _mapper = mapper;

            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

            this._repositoryFilterConditions = repositoryFilterConditions;
             this._caregiverTasksTerminalPayerRepository = caregiverTasksTerminalPayerRepository;
        }
        public async Task<Response<IEnumerable<EvvTerminalPayersDto>>> Handle(EvvTerminalPayersQuery request, CancellationToken cancellationToken)
        {
            
           IEnumerable<EvvTerminalPayersDto> EvvTerminalPayersDto = new List<EvvTerminalPayersDto>();


            var caregivertaskevvterminalpayerfilters = _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.HHA", _actionContext.HHA, integerType).EqualFilter("CaregiverTasksTerminalPayer.isDeleted", 0, integerType).EqualFilter("CaregiverTasksTerminalPayer.CgTaskID", request.CGTask_ID, integerType).Build();

            var caregivertaskevvterminalpayercolumns = "CaregiverTasksTerminalPayer.Hierarchy,CaregiverTasksTerminalPayer.TerminalPayerID,CaregiverTasksTerminalPayer.evv_configuration_id";

            var _CaregiverTaskSTerminalPayers = await _caregiverTasksTerminalPayerRepository.GetTerminalPayerSchedules(_actionContext.HHA, _actionContext.User, caregivertaskevvterminalpayercolumns, caregivertaskevvterminalpayerfilters);

            //do mapping EvvTerminalPayersDto = _mapper.Map<EvvTerminalPayersDto>(_CaregiverTaskSTerminalPayers);

            if (_CaregiverTaskSTerminalPayers.Count() == 0)
            {
                return Response.Fail<IEnumerable<EvvTerminalPayersDto>>("No_Content", "There are no Records Exists", System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                EvvTerminalPayersDto = _mapper.Map<IEnumerable<EvvTerminalPayersDto>>(_CaregiverTaskSTerminalPayers);
                return Response.Ok(EvvTerminalPayersDto);

            }
        }

    }
}

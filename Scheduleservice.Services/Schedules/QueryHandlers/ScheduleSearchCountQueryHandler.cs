﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Schedule;
using AutoMapper;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.DTO.EVV;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Schedules;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleSearchCountQueryHandler : IHandlerWrapper<ScheduleSearchCountQuery, ScheduleSearchCountEVVInfoDto>
    {
        private readonly ISchedule _scheduleService;
        private readonly IMapper _mapper;
        private readonly ILogger<ScheduleSearchCountQueryHandler> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContextDto;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public ScheduleSearchCountQueryHandler(ISchedule scheduleService, IMapper mapper, ILogger<ScheduleSearchCountQueryHandler> logger,
            IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
            IRepositoryFilterConditions repositoryFilterConditions)
        {
            _scheduleService = scheduleService;
            _mapper = mapper;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;

            _actionContextDto = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<ScheduleSearchCountEVVInfoDto>> Handle(ScheduleSearchCountQuery request, CancellationToken cancellationToken)
        {

            try
            {
                string schedulesFilter = "";
                PagedList<SchedulesListDto> _schedulesPageList = null;

                IEnumerable<SchedulesListDto> _schedulesListDto;
                ScheduleSearchCountEVVInfoDto scheduleCountEVVInfoDto = new ScheduleSearchCountEVVInfoDto();
                ScheduleFitlersDto scheduleFitlersDto = new ScheduleFitlersDto();

                if (request.output_fields == null || request.output_fields.Length == 0)
                {
                    request.output_fields = new string[] { "CGTASK_ID" };
                }
                else if (!request.output_fields.Any(x => x.Equals("CGTASK_ID")))
                {
                    request.output_fields = request.output_fields.Append("CGTASK_ID");
                }

                scheduleFitlersDto = _mapper.Map<ScheduleFitlersDto>(request);

                schedulesFilter = Common.GetScheduleFilters(scheduleFitlersDto);

                var columns = String.Join(",", request.output_fields);
                var schedules = await _caregivertasksRepository.GetAllSchedulesList(_actionContextDto.HHA, _actionContextDto.User, columns, schedulesFilter);

                var schedules_Output_columns_List = schedules.Select(a => a.DynamicSelectGenerator(request.output_fields));

                string _schedulesListjson = JsonConvert.SerializeObject(schedules_Output_columns_List);

                _schedulesListDto = JsonConvert.DeserializeObject<IEnumerable<SchedulesListDto>>(_schedulesListjson);

                if (request.page_no == 0 && _schedulesListDto.Count() > 0)
                {
                    _schedulesPageList = PagedList<SchedulesListDto>.ToPagedList(_schedulesListDto.AsQueryable(), request.page_no, _schedulesListDto.Count());
                }
                else if(request.page_no>=1 && _schedulesListDto.Count() > 0)
                {
                    _schedulesPageList = PagedList<SchedulesListDto>.ToPagedList(_schedulesListDto.AsQueryable(), request.page_no, 100);
                }

                if(_schedulesPageList!=null && _schedulesPageList.total_count > 0)
                {
                    scheduleCountEVVInfoDto.total_records = _schedulesPageList.total_count;
                    scheduleCountEVVInfoDto.current_page_no = _schedulesPageList.current_page;
                    scheduleCountEVVInfoDto.number_records_in_this_page = _schedulesPageList.number_records_in_this_page;
                    scheduleCountEVVInfoDto.total_pages = _schedulesPageList.total_pages;
                    scheduleCountEVVInfoDto.page_size = _schedulesPageList.page_size;
                }
                else
                {
                    scheduleCountEVVInfoDto.total_records = 0;
                    scheduleCountEVVInfoDto.current_page_no = request.page_no;
                    scheduleCountEVVInfoDto.number_records_in_this_page = 0;
                    scheduleCountEVVInfoDto.total_pages = 0;
                    scheduleCountEVVInfoDto.page_size = 0;
                }

                return Response.Ok<ScheduleSearchCountEVVInfoDto>(scheduleCountEVVInfoDto);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Class: ScheduleCountQueryHandler.cs; Method: Handle; Message: {ex.Message}");
                return Response.ValidationError<ScheduleSearchCountEVVInfoDto>(ex.Message, null);
            }

        }
    }
}
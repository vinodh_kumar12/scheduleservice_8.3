﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Enums;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class EvvTerminalPayerdashboardQueryHandler : IHandlerWrapper<EvvTerminalPayerdashboardQuery, EvvTerminalPayerdashboardDto>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContext;
        private readonly IClientEvvTerminalPayersRepository _clientEvvTerminalPayersRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ILogger<EvvTerminalPayerdashboardQueryHandler> _logger;
        private readonly ICaregiverTasksTerminalPayerRepository _caregiverTasksTerminalPayerRepository;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly ISchedule _schedule;
        private readonly IHConfigurations2Repository _hConfigurations2Repository;

        public EvvTerminalPayerdashboardQueryHandler(IHttpContextAccessor httpContextAccessor,
            IClientEvvTerminalPayersRepository clientEvvTerminalPayersRepository, IRepositoryFilterConditions repositoryFilterConditions,
            ILogger<EvvTerminalPayerdashboardQueryHandler> logger, ICaregiverTasksTerminalPayerRepository caregiverTasksTerminalPayerRepository,
            ICaregivertasksRepository caregivertasksRepository, IUsersRepository usersRepository, ICaregiversRepository caregiversRepository,
            ISchedule schedule, IHConfigurations2Repository hConfigurations2Repository)
        {
            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

            this._clientEvvTerminalPayersRepository = clientEvvTerminalPayersRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregiverTasksTerminalPayerRepository = caregiverTasksTerminalPayerRepository;
            this._caregivertasksRepository = caregivertasksRepository;
            this._usersRepository = usersRepository;
            this._caregiversRepository = caregiversRepository;
            this._schedule = schedule;
            this._hConfigurations2Repository = hConfigurations2Repository;
            this._logger = logger;
        }

        public async Task<Response<EvvTerminalPayerdashboardDto>> Handle(EvvTerminalPayerdashboardQuery request, CancellationToken cancellationToken)
        {
            EvvTerminalPayerdashboardDto evvTerminalPayerdashboardDto = new EvvTerminalPayerdashboardDto();
            
            try
            {
                var Locations = "";
                var Columns = "";
                var Filters = "";
                DateTime Today = DateTime.UtcNow;

                if (request.Location != null && request.Location.Length >= 1 && request.Location[0] != 0)
                {
                    Locations = string.Join(',', request.Location);
                }
                else
                {
                    Columns = "UsersBranchList.[USER]," +
                        "UsersBranchList.HHA_BRANCH_ID";
                    Filters = _repositoryFilterConditions.EqualFilter("UsersBranchList.[USER]", _actionContext.User, integerType).Build();
                    var userLocations = await _usersRepository.GetUsersBranchList(_actionContext.HHA, _actionContext.User, Columns, Filters);
                    if (!userLocations.Any())
                    {
                        return Response.ValidationError<EvvTerminalPayerdashboardDto>("No Location(s) Found for the User");
                    }
                    Locations = string.Join(',', userLocations.Select(x => x.HHA_BRANCH_ID).ToList());
                }

                // Get Payroll branch from user id
                Columns = "Users.USER_PK," +
                    "Users.[USER_ID]";
                Filters = _repositoryFilterConditions.EqualFilter("Users.[USER_ID]", _actionContext.User, integerType).Build();
                var users = await _usersRepository.GetHHAUsers(_actionContext.HHA, Columns, Filters);
                if (!users.Any())
                {
                    return Response.ValidationError<EvvTerminalPayerdashboardDto>("Cannot find user");
                }

                Columns = "Caregivers.CAREGIVER_ID," +
                    "Caregivers.PAYROLL_BRANCH_ID";
                Filters = _repositoryFilterConditions.EqualFilter("Caregivers.CAREGIVER_ID", users.Select(x => x.USER_PK).FirstOrDefault(), integerType).Build();
                var caregivers = await _caregiversRepository.GetCaregivers(_actionContext.HHA, _actionContext.User, Columns, Filters);
                if (!caregivers.Any())
                {
                    return Response.ValidationError<EvvTerminalPayerdashboardDto>("Cannot find user");
                }

                Columns = "HConfigurations2.HHA_BRANCHID," +
                        "HConfigurations2.TIME_ZONE";
                Filters = _repositoryFilterConditions.EqualFilter("HConfigurations2.HHA_BRANCHID", caregivers.Select(x => x.PAYROLL_BRANCH_ID).FirstOrDefault(), Core.Enums.FilterConditionDataTypeEnums.integerType).Build();
                var userTimeZoneInfo = await _hConfigurations2Repository._S_Schedule_GetHConfiguration2Settings(_actionContext.HHA, _actionContext.User, Columns, Filters);
                
                Today = Today.ToConvertUTCTimeZone(userTimeZoneInfo.TIME_ZONE);

                DateTime lastWeekStart = Today.AddDays(-(int)Today.DayOfWeek - 7).Date;
                DateTime lastWeekEnd = Today.AddDays(-(int)Today.DayOfWeek - 1).Date;
                DateTime weekStart = Today.AddDays(-(int)Today.DayOfWeek).Date;
                DateTime weekEnd = Today.AddDays(6 - (int)Today.DayOfWeek).Date;

                //Get Client Evv Terminal Payers
                Columns = "ClientEVVTerminalPayers.client_id," +
                    "ClientEVVTerminalPayers.Primary_client_payer_id," +
                    "ClientEVVTerminalPayers.Terminal_client_payer_id";

                Filters = _repositoryFilterConditions.InFilter("Clients.HHA_BRANCH_ID", Locations, integerType)
                    .EqualFilter("ClientEVVTerminalPayers.is_deleted", false, boolType)
                    .NotInFilter("Clients.STATUS", "Deleted, Non-Admit", stringType)
                    .Build();
                
                var clientEvvTerminalPayers = await _clientEvvTerminalPayersRepository.GetClientTerminalPayersList(_actionContext.HHA, _actionContext.User, Columns, Filters);

                if (!clientEvvTerminalPayers.Any())
                {
                    return Response.NoContent(evvTerminalPayerdashboardDto);
                }

                //Get Terminal Payer Parent Schedule
                var ClientIDs = string.Join(',', clientEvvTerminalPayers.Select(x => x.client_id).Distinct().ToList());
                var schedule_status = ScheduleStatusEnums.Planned.ToString() + "," + ScheduleStatusEnums.In_Progress.ToString() + "," +
                       ScheduleStatusEnums.Completed.ToString() + "," + ScheduleStatusEnums.Approved.ToString();
                Columns = "CaregiverTasksTerminalPayer.CaregiverTasksTerminalPayerID," +
                    "CaregiverTasksTerminalPayer.HHA," +
                    "CaregiverTasksTerminalPayer.CgTaskID," +
                    "CaregiverTasksTerminalPayer.ClientID," +
                    "CaregiverTasksTerminalPayer.PrimaryPayerID," +
                    "CaregiverTasksTerminalPayer.Hierarchy," +
                    "CaregiverTasksTerminalPayer.TerminalPayerID," +
                    "CaregiverTasksTerminalPayer.is_exported," +
                    "CaregiverTasksTerminalPayer.Last_export_status," +
                    "CaregiverTasksTerminalPayer.is_dirty," +
                    "CaregiverTasks.PLANNED_DATE";
                Filters = _repositoryFilterConditions.InFilter("CaregiverTasksTerminalPayer.ClientID", ClientIDs, integerType, true)
                    .EqualFilter("CaregiverTasksTerminalPayer.isDeleted", false, boolType, true)
                    .BetweenFilter("CaregiverTasks.PLANNED_DATE", lastWeekStart.ToString("MM/dd/yyyy"), weekEnd.ToString("MM/dd/yyyy"), datetimeType)
                    .InFilter("CaregiverTasks.STATUS", schedule_status, stringType)
                    .Build();
                var terminalParentSchedules = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerSchedules(_actionContext.HHA, _actionContext.User, Columns, Filters);

                //Get Terminal Payer Child Schedules
                Columns = "CaregiverTaskChildScheduleTerminalPayer.CaregiverChildTaskTerminalPayerID," +
                    "CaregiverTaskChildScheduleTerminalPayer.HHA," +
                    "CaregiverTaskChildScheduleTerminalPayer.CgTaskID," +
                    "CaregiverTaskChildScheduleTerminalPayer.ChildScheduleID," +
                    "CaregiverTaskChildScheduleTerminalPayer.ClientID," +
                    "CaregiverTaskChildScheduleTerminalPayer.PrimaryPayerID," +
                    "CaregiverTaskChildScheduleTerminalPayer.Hierarchy," +
                    "CaregiverTaskChildScheduleTerminalPayer.TerminalPayerID," +
                    "CaregiverTaskChildScheduleTerminalPayer.is_exported," +
                    "CaregiverTaskChildScheduleTerminalPayer.Last_export_status,"+
                    "CaregiverTaskChildScheduleTerminalPayer.is_dirty," +
                    "CaregiverTaskChildSchedules.SCHEDULE_DATE";
                Filters = _repositoryFilterConditions.InFilter("CaregiverTaskChildScheduleTerminalPayer.ClientID", ClientIDs, integerType, true)
                    .EqualFilter("CaregiverTaskChildScheduleTerminalPayer.isDeleted", false, boolType, true)
                    .BetweenFilter("CaregiverTaskChildSchedules.SCHEDULE_DATE", lastWeekStart.ToString("MM/dd/yyyy"), weekEnd.ToString("MM/dd/yyyy"), datetimeType)
                    .EqualFilter("CaregiverTaskChildSchedules.isDeleted", false, boolType, true)
                    .Build();
                var terminalChildSchedules = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerChildSchedules(_actionContext.HHA, _actionContext.User, Columns, Filters);

                //Combined parent and child schedules
                var terminalSchedules =
                    terminalParentSchedules.Select(x => new
                    {
                        TerminalPayerScheduleID = x.CaregiverTasksTerminalPayerID,
                        HHA = x.HHA,
                        ClientID = x.ClientID,
                        CgtaskID = x.CgTaskID,
                        ChildScheduleID = 0,
                        ClientPayerID = x.PrimaryPayerID,
                        Hierarchy = x.Hierarchy,
                        TerminalPayerID = x.TerminalPayerID,
                        isExported = x.is_exported,
                        ExportStatus = x.Last_export_status??0,
                        ScheduleDate = x.PLANNED_DATE,
                        isDirty = x.is_dirty
                    }).Union(terminalChildSchedules.Select(x => new
                    {
                        TerminalPayerScheduleID = x.CaregiverChildTaskTerminalPayerID,
                        HHA = x.HHA,
                        ClientID = x.ClientID,
                        CgtaskID = x.CgTaskID,
                        ChildScheduleID = x.ChildScheduleID,
                        ClientPayerID = x.PrimaryPayerID,
                        Hierarchy = x.Hierarchy,
                        TerminalPayerID = x.TerminalPayerID,
                        isExported = x.is_exported,
                        ExportStatus = x.Last_export_status??0,
                        ScheduleDate = x.SCHEDULE_DATE,
                        isDirty = x.is_dirty
                    }));

                if (!terminalSchedules.Any())
                {
                    return Response.NoContent(evvTerminalPayerdashboardDto);
                }

                evvTerminalPayerdashboardDto.exported_visits.last_week =
                    terminalSchedules
                        .Where(x => 
                            x.isDirty == false
                            && x.ExportStatus == 9
                            && x.ScheduleDate >= lastWeekStart
                            && x.ScheduleDate <= lastWeekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.exported_visits.current_week =
                    terminalSchedules
                        .Where(x =>
                            x.isDirty == false
                            && x.ExportStatus == 9
                            && x.ScheduleDate >= weekStart
                            && x.ScheduleDate <= weekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.exported_visits.total = 
                    terminalSchedules
                        .Where(x =>
                            x.isDirty == false
                            && x.ExportStatus == 9
                        ).Count();

                evvTerminalPayerdashboardDto.failed_visits.last_week =
                    terminalSchedules
                        .Where(x =>
                            !(new[] { 1, 4, 5, 6, 9, 0 }.Contains(x.ExportStatus))
                            && x.ScheduleDate >= lastWeekStart
                            && x.ScheduleDate <= lastWeekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.failed_visits.current_week =
                    terminalSchedules
                        .Where(x =>
                            !(new[] { 1, 4, 5, 6, 9, 0 }.Contains(x.ExportStatus))
                            && x.ScheduleDate >= weekStart
                            && x.ScheduleDate <= weekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.failed_visits.total =
                    terminalSchedules
                        .Where(x =>
                            !(new[] { 1, 4, 5, 6, 9, 0 }.Contains(x.ExportStatus))
                        ).Count();

                evvTerminalPayerdashboardDto.export_pending.last_week =
                    terminalSchedules
                        .Where(x =>
                            (new[] { 1, 4, 5, 6, 0 }.Contains(x.ExportStatus) || (x.ExportStatus == 9 && x.isDirty == true))
                            && x.ScheduleDate >= lastWeekStart
                            && x.ScheduleDate <= lastWeekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.export_pending.current_week =
                    terminalSchedules
                        .Where(x =>
                            (new[] { 1, 4, 5, 6, 0 }.Contains(x.ExportStatus) || (x.ExportStatus == 9 && x.isDirty == true))
                            && x.ScheduleDate >= weekStart
                            && x.ScheduleDate <= weekEnd
                        ).Count();

                evvTerminalPayerdashboardDto.export_pending.total = 
                    terminalSchedules
                        .Where(x =>
                            (new[] { 1, 4, 5, 6, 0 }.Contains(x.ExportStatus) || (x.ExportStatus == 9 && x.isDirty == true))
                        ).Count();

                evvTerminalPayerdashboardDto.export_pending.current_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.export_pending.current_week) / (terminalSchedules.Count()), 2);
                evvTerminalPayerdashboardDto.export_pending.last_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.export_pending.last_week) / (terminalSchedules.Count()), 2);
                evvTerminalPayerdashboardDto.exported_visits.current_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.exported_visits.current_week) / (terminalSchedules.Count()), 2);
                evvTerminalPayerdashboardDto.exported_visits.last_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.exported_visits.last_week) / (terminalSchedules.Count()), 2);
                evvTerminalPayerdashboardDto.failed_visits.current_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.failed_visits.current_week) / (terminalSchedules.Count()), 2);
                evvTerminalPayerdashboardDto.failed_visits.last_week_percentage = (float)Math.Round((100f * evvTerminalPayerdashboardDto.failed_visits.last_week) / (terminalSchedules.Count()), 2);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<EvvTerminalPayerdashboardDto>(ResponseErrorCodes.InternalServerFailed, e.Message);
            }

            return Response.Ok(evvTerminalPayerdashboardDto);
        }
    }
}

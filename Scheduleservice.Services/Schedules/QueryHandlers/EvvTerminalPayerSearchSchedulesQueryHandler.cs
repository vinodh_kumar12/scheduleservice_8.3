﻿using AutoMapper;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Entities;
using System.Reflection;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Common.Extensions;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class EvvTerminalPayerSearchSchedulesQueryHandler : IHandlerWrapper<EvvTerminalPayerSearchSchedulesQuery, EVVTerminalPayerSerachSchedulesDto>
    {
        private readonly ICaregivertasksRepository _caregivertasksRepository;

        private readonly IMapper _mapper;
        private readonly IUsersRepository _userrepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHttpContextAccessor _httpcontextaccesssor;
        private readonly IClientEvvTerminalPayersRepository _clientEvvTerminalPayersRepository;
        private readonly ICaregiverTasksTerminalPayerRepository _caregiverTasksTerminalPayerRepository;
        private readonly IPaymentsourcesRepository _paymentSourcesRepository;
        private readonly IServiceCodesRepository _servicecodeRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly IHHALineofbusinessRepository _hHALineofbusinessRepository;

        public ActionContextDto ActionContext { get; }

        public EvvTerminalPayerSearchSchedulesQueryHandler(ICaregivertasksRepository caregivertasksRepository,
                     IMapper mapper, IClientsRepository clientsRepository, IRepositoryFilterConditions repositoryFilterConditions,
            IHttpContextAccessor httpContextAccessor, IClientEvvTerminalPayersRepository clientEvvTerminalPayersRepository,
            ICaregiverTasksTerminalPayerRepository caregiverTasksTerminalPayerRepository, IPaymentsourcesRepository paymentSourcesRepository,
            IServiceCodesRepository servicecodeRepository, IUsersRepository userRepository,
            IHHALineofbusinessRepository hHALineofbusinessRepository)
        {
            _caregivertasksRepository = caregivertasksRepository;
            _mapper = mapper;
            _userrepository = userRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _httpcontextaccesssor = httpContextAccessor;
            _clientEvvTerminalPayersRepository = clientEvvTerminalPayersRepository;
            _caregiverTasksTerminalPayerRepository = caregiverTasksTerminalPayerRepository;
            _paymentSourcesRepository = paymentSourcesRepository;
            _servicecodeRepository = servicecodeRepository;
            _clientsRepository = clientsRepository;
            this._hHALineofbusinessRepository = hHALineofbusinessRepository;


            if (_httpcontextaccesssor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpcontextaccesssor.HttpContext.Items[ConstantKeys.ActionContext];

        }

        public async Task<Response<EVVTerminalPayerSerachSchedulesDto>> Handle(EvvTerminalPayerSearchSchedulesQuery request, CancellationToken cancellationToken)
        {
            EVVTerminalPayerSerachSchedulesDto outputdto = new EVVTerminalPayerSerachSchedulesDto();
            outputdto.evvTerminalPayerSearchSchedulesList = new List<EvvTerminalPayerSchedulesListDto>();
            int HHA = ActionContext.HHA;
            int UserID = ActionContext.User;
            var serviceids = "";
            var servicefilters = "";
            int totalrecords = 0;
            var payerfilters = "";

            try
            {
                var userfilters = _repositoryFilterConditions.EqualFilter("[USER]", UserID, FilterConditionDataTypeEnums.integerType).
                               EqualFilter("HHA", HHA, FilterConditionDataTypeEnums.integerType)
                               .EqualFilter("isDeleted", false, FilterConditionDataTypeEnums.boolType)
                               .Build();

                var usercolumns = "UsersBranchList.HHA_BRANCH_ID";
                var userlocationids = await _userrepository.GetUsersBranchList(HHA, UserID, usercolumns, userfilters);

                string loggeduserlocationids = string.Join(", ", userlocationids.Select(x => x.HHA_BRANCH_ID.ToString()).Distinct().ToArray());


                var parentscheduleFilters = "";

                var childscheduleFilters = _repositoryFilterConditions.BetweenFilter("CaregiverTaskChildSchedules.SCHEDULE_DATE", request.schedule_date_from, request.schedule_date_to, FilterConditionDataTypeEnums.stringType)
                                .EqualFilter("CaregiverTaskChildSchedules.isDeleted", false, FilterConditionDataTypeEnums.boolType)
                                .EqualFilter("CaregiverTaskChildSchedules.isEvvschedule", true, FilterConditionDataTypeEnums.boolType).Build();


                var clientevvpayerfilters = _repositoryFilterConditions.EqualFilter("ClientEVVTerminalPayers.is_deleted", false, FilterConditionDataTypeEnums.boolType).Build();

                var terminalpayerschedulefilters = _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.isDeleted", false, FilterConditionDataTypeEnums.boolType).Build();

                var terminalpayerchildschedulefilters = _repositoryFilterConditions.EqualFilter("CaregiverTaskChildScheduleTerminalPayer.isDeleted", false, FilterConditionDataTypeEnums.boolType).Build();

                if (!string.IsNullOrEmpty(request.schedule_date_from) && !string.IsNullOrEmpty(request.schedule_date_to))
                {
                    parentscheduleFilters += _repositoryFilterConditions.BetweenFilter("PLANNED_DATE", request.schedule_date_from, request.schedule_date_to, FilterConditionDataTypeEnums.stringType)
                    .EqualFilter("isEvvschedule", true, FilterConditionDataTypeEnums.boolType).Build();

                    terminalpayerchildschedulefilters += _repositoryFilterConditions.BetweenFilter("SCHEDULE_DATE", request.schedule_date_from, request.schedule_date_to, FilterConditionDataTypeEnums.stringType).Build();
                    terminalpayerschedulefilters += _repositoryFilterConditions.BetweenFilter("CaregiverTasks.PLANNED_DATE", request.schedule_date_from, request.schedule_date_to, FilterConditionDataTypeEnums.stringType).Build();
                }

                if (request.client_id > 0)
                {
                    parentscheduleFilters += _repositoryFilterConditions.EqualFilter("CLIENT_ID", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                    childscheduleFilters += _repositoryFilterConditions.EqualFilter("CLIENT", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                    clientevvpayerfilters += _repositoryFilterConditions.EqualFilter("ClientEVVTerminalPayers.client_id", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                    terminalpayerschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.ClientID", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                    terminalpayerchildschedulefilters += _repositoryFilterConditions.EqualFilter("ClientID", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                }
                if (!string.IsNullOrEmpty(request.payer_id))
                {
                    request.payer_id = request.payer_id.TrimEnd(',');

                    parentscheduleFilters += _repositoryFilterConditions.InFilter("PAYMENT_SOURCE", request.payer_id, FilterConditionDataTypeEnums.integerType).Build();
                    childscheduleFilters += _repositoryFilterConditions.InFilter("CaregiverTaskChildSchedules.PAYMENT_SOURCE", request.payer_id, FilterConditionDataTypeEnums.integerType).Build();
                    //terminalpayerschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasksTerminalPayer.PrimaryPayerID", request.payer_id, FilterConditionDataTypeEnums.integerType).Build();
                    // terminalpayerchildschedulefilters += _repositoryFilterConditions.InFilter("PrimaryPayerID", request.payer_id, FilterConditionDataTypeEnums.integerType).Build();
                    clientevvpayerfilters += _repositoryFilterConditions.InFilter("ClientPaymentSources.PAYMENT_SOURCE_ID", request.payer_id, FilterConditionDataTypeEnums.integerType).Build();
                }



                if (!string.IsNullOrEmpty(request.terminal_payer_hierarchy))
                {
                    request.terminal_payer_hierarchy = request.terminal_payer_hierarchy.TrimEnd(',');

                    terminalpayerschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.Hierarchy", request.terminal_payer_hierarchy, FilterConditionDataTypeEnums.stringType).Build();
                    terminalpayerchildschedulefilters += _repositoryFilterConditions.EqualFilter("Hierarchy", request.terminal_payer_hierarchy, FilterConditionDataTypeEnums.stringType).Build();

                }

                if (request.terminal_configuration_ID > 0)
                {
                    terminalpayerschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.evv_configuration_id", request.terminal_configuration_ID, FilterConditionDataTypeEnums.integerType).Build();
                    terminalpayerchildschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildScheduleTerminalPayer.evv_configuration_id", request.terminal_configuration_ID, FilterConditionDataTypeEnums.integerType).Build();

                }

                if (!string.IsNullOrEmpty(request.export_status))
                {
                    request.export_status = request.export_status.TrimEnd(',');

                    if (request.export_status == "success")
                    {
                        //isevvexported == true and isevvdirty = false
                        terminalpayerschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.Last_export_status ", 9, FilterConditionDataTypeEnums.integerType)
                            .EqualFilter("CaregiverTasksTerminalPayer.is_dirty", false, FilterConditionDataTypeEnums.boolType)
                            .Build();

                        terminalpayerchildschedulefilters += _repositoryFilterConditions.EqualFilter("Last_export_status", 9, FilterConditionDataTypeEnums.integerType)
                            .EqualFilter("is_dirty", false, FilterConditionDataTypeEnums.boolType)
                            .Build();
                    }
                    else if (request.export_status == "failed")
                    {
                        terminalpayerschedulefilters += _repositoryFilterConditions.NotInFilter("CaregiverTasksTerminalPayer.Last_export_status", "1,4,5,6,0,9", FilterConditionDataTypeEnums.integerType)
                                                         .Build();
                        terminalpayerchildschedulefilters += _repositoryFilterConditions.NotInFilter("Last_export_status", "1,4,5,6,0,9", FilterConditionDataTypeEnums.integerType).Build();
                    }
                    else if (request.export_status == "pending")
                    {
                        //Last_export_status == 1 or isevvdirty = true

                        //terminalpayerschedulefilters += _repositoryFilterConditions.EqualFilter("CaregiverTasksTerminalPayer.Last_export_status", 1, FilterConditionDataTypeEnums.integerType).Build();
                        //terminalpayerschedulefilters += " and (CaregiverTasksTerminalPayer.Last_export_status = 1 OR CaregiverTasksTerminalPayer.is_dirty = 1) ";

                        //terminalpayerschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasksTerminalPayer.Last_export_status", "1,4,5,6,0", FilterConditionDataTypeEnums.integerType)
                        //                                 .Build();
                        //terminalpayerchildschedulefilters += _repositoryFilterConditions.InFilter("Last_export_status", "1,4,5,6,0,9", FilterConditionDataTypeEnums.integerType).Build();
                        terminalpayerschedulefilters += " AND (CaregiverTasksTerminalPayer.Last_export_status in (1,4,5,6,0) OR (CaregiverTasksTerminalPayer.Last_export_status = 9 and CaregiverTasksTerminalPayer.is_dirty = 1))";
                        terminalpayerchildschedulefilters += " AND (Last_export_status in (1,4,5,6,0) OR (Last_export_status = 9 and is_dirty = 1))";


                    }

                }

                if (!string.IsNullOrEmpty(request.location))
                {
                    request.location = request.location.TrimEnd(',');
                    clientevvpayerfilters += _repositoryFilterConditions.InFilter("Clients.HHA_BRANCH_ID", request.location, FilterConditionDataTypeEnums.integerType).Build();
                }
                else
                {
                    clientevvpayerfilters += _repositoryFilterConditions.InFilter("Clients.HHA_BRANCH_ID", loggeduserlocationids, FilterConditionDataTypeEnums.integerType).Build();
                }

                if (request.schedule_status == "" || request.schedule_status == "ALL" || request.schedule_status == null)
                {
                    request.schedule_status = ScheduleStatusEnums.Planned.ToString() + "," + ScheduleStatusEnums.In_Progress.ToString() + "," +
                       ScheduleStatusEnums.Completed.ToString() + "," + ScheduleStatusEnums.Approved.ToString() + "," + ScheduleStatusEnums.MissedVisit.ToString();
                    parentscheduleFilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();
                    terminalpayerschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();
                    terminalpayerchildschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();
                }
                else //filter the schedule based on user given status and exclude deleted schedules
                {
                    request.schedule_status = request.schedule_status.TrimEnd(',');

                    parentscheduleFilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                    terminalpayerschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                    terminalpayerchildschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.STATUS", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                }


                var parentscheduleColumns = "CGTASK_ID,CAREGIVER,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS," +
                   "PLANNED_DATE,EDITED_HOURS,SERVICECODE_ID,CaregiverTaskAdditional.IsSplitForBilling,CaregiverTaskAdditional.IsSplitForPayroll," +
                   "PAYMENT_SOURCE,CLIENT_ID,IS_BILLED,IS_PAID,isEvvschedule,CaregiverTasks.IS_PAYABLE,CaregiverTasks.IS_BILLABLE,CaregiverTasks.PLANNED_START_TIME,CaregiverTasks.PLANNED_END_TIME";


                var childscheduleColumns = "CaregiverTaskChildSchedules.Child_Schedule_Id,CaregiverTaskChildSchedules.HHA,CaregiverTaskChildSchedules.CLIENT,CaregiverTaskChildSchedules.CANBILL," +
                                            "CaregiverTaskChildSchedules.PARENT_CGTASK_ID,CaregiverTaskChildSchedules.SCHEDULE_DATE,CaregiverTaskChildSchedules.ACTUAL_STARTTIME," +
                                            "CaregiverTaskChildSchedules.ACTUAL_ENDTIME,CaregiverTaskChildSchedules.isEvvschedule,CaregiverTaskChildSchedules.SERVICECODE_ID,CaregiverTaskChildSchedules.PAYMENT_SOURCE," +
                                            "CaregiverTaskChildSchedules.PLANNED_STARTTIME,CaregiverTaskChildSchedules.PLANNED_ENDTIME,CaregiverTasks.STATUS ";




                var clientevvpayercolumns = "ClientEvvTerminalPayers.client_id,ClientEvvTerminalPayers.Primary_client_payer_id," +
                                            "ClientEvvTerminalPayers.Terminal_client_payer_id,ClientEvvTerminalPayers.client_evv_terminal_payer_id," +
                                            "ClientEvvTerminalPayers.hierarchy,ClientEvvTerminalPayers.evv_configuration_id," +
                                            "ClientEvvTerminalPayers.export_effective_from,ClientEvvTerminalPayers.is_deleted," +
                                            "ClientPaymentSources.PAYMENT_SOURCE_ID,Clients.PATIENT_ID,Clients.FIRST_NAME,Clients.LAST_NAME,Clients.AdmitNo," +
                                            "VendorName,VendorState,VendorVersionCode";


                var terminalpayerschedulecolumns = "CaregiverTasksTerminalPayer.CaregiverTasksTerminalPayerID,CaregiverTasksTerminalPayer.CgTaskID," +
                                                   "CaregiverTasksTerminalPayer.ClientID,Hierarchy,TerminalPayerID,CaregiverTasksTerminalPayer.evv_configuration_id,is_exported,isnull(Last_export_status,0) Last_export_status," +
                                                   "CaregiverTasksTerminalPayer.PrimaryPayerID,CaregiverTaskAdditional.IsSplitForPayroll,CaregiverTaskAdditional.IsSplitForBilling," +
                                                   "CaregiverTasks.SERVICECODE_ID,CaregiverTasks.PLANNED_START_TIME,CaregiverTasks.PLANNED_END_TIME," +
                                                   "CaregiverTasks.PLANNED_DATE,CaregiverTasks.ACCTUAL_START_TIME,CaregiverTasks.ACCTUAL_END_TIME,CaregiverTasks.STATUS," +
                                                   "CaregiverTasks.EDITED_HOURS,CaregiverTasks.CLIENT_ID,CaregiverTasks.PAYMENT_SOURCE," +
                                                   "CaregiverTasks.IS_BILLED,CaregiverTasks.IS_PAID,CaregiverTaskAdditional.isEvvschedule,CaregiverTasks.IS_PAYABLE,CaregiverTasks.IS_BILLABLE," +
                                                   "CaregiverTasksTerminalPayer.Last_failed_error_code, CaregiverTasksTerminalPayer.Last_failed_error_message, CaregiverTasksTerminalPayer.Last_success_exported_on ";



                var terminalpayerchildschedulecolumns = "CaregiverChildTaskTerminalPayerID," +
                    "CaregiverTaskChildScheduleTerminalPayer.CgTaskID,CaregiverTaskChildScheduleTerminalPayer.ChildScheduleID,CaregiverTaskChildScheduleTerminalPayer.ClientID," +
                    "Hierarchy,TerminalPayerID,CaregiverTaskChildScheduleTerminalPayer.evv_configuration_id,CaregiverTaskChildScheduleTerminalPayer.is_exported,isnull(Last_export_status,0) Last_export_status," +
                    "CaregiverTaskChildScheduleTerminalPayer.PrimaryPayerID,CaregiverTaskChildSchedules.PAYMENT_SOURCE,CaregiverTaskChildSchedules.SERVICECODE_ID," +
                    "CaregiverTaskChildSchedules.CLIENT,CaregiverTaskChildSchedules.Child_Schedule_Id,CaregiverTaskChildSchedules.PARENT_CGTASK_ID," +
                    "CaregiverTaskChildSchedules.SCHEDULE_DATE,CaregiverTaskChildSchedules.ACTUAL_STARTTIME,CaregiverTaskChildSchedules.ACTUAL_ENDTIME," +
                    "CaregiverTaskChildSchedules.IS_BILLED,CaregiverTaskChildSchedules.IsPayrolled,CaregiverTaskChildSchedules.isEvvschedule,CaregiverTaskChildSchedules.Child_Schedule_Id," +
                    "CaregiverTaskChildSchedules.PLANNED_STARTTIME,CaregiverTaskChildSchedules.PLANNED_ENDTIME,CaregiverTaskChildSchedules.CANBILL," +
                    "CaregiverTaskChildScheduleTerminalPayer.Last_failed_error_code, CaregiverTaskChildScheduleTerminalPayer.Last_failed_error_message, " +
                    "CaregiverTaskChildScheduleTerminalPayer.Last_success_exported_on, CaregiverTasks.status,TOTALMINUTES_ACTUAL ";

                IEnumerable<ClientAdditionalPaymentSourcesEntity> clientadditionalpaymentdetails = new List<ClientAdditionalPaymentSourcesEntity>();
                IEnumerable<DualEligibilePayersEntity> clientdualeligiblepayerdetails = new List<DualEligibilePayersEntity>();
                var payerColumns = "PAYMENT_SOURCE_ID,ORG_NAME";
                var serviceColumns = "SERVICE_CODE_ID,DESCRIPTION,ServiceCodes.SERVICE_TYPE";
                var clientevvterminalpayerdetails = await _clientEvvTerminalPayersRepository.GetAllClientTerminalPayersList(HHA, UserID, clientevvpayercolumns, clientevvpayerfilters);

                if (!clientevvterminalpayerdetails.Any())
                {
                    return Response.Fail<EVVTerminalPayerSerachSchedulesDto>("No_Content", "There are no records available", System.Net.HttpStatusCode.NoContent);

                }
                else if(!string.IsNullOrEmpty(request.client_status))
                {
                    var clientIDs = string.Join(",", clientevvterminalpayerdetails.Select(x=>x.client_id).Distinct().ToList());

                    var clientstatusinfo = await _clientsRepository.GetClientStatus(HHA, UserID, clientIDs);

                    if (clientstatusinfo.Any())
                    {
                        clientstatusinfo = clientstatusinfo.Where(x => request.client_status.Contains(x.ClientStatus.ToString()));
                        clientevvterminalpayerdetails = clientevvterminalpayerdetails.Where(x => clientstatusinfo.Where(y => y.CLIENT_ID == x.client_id).Any());
                    }

                    if (!clientevvterminalpayerdetails.Any())
                    {
                        return Response.Fail<EVVTerminalPayerSerachSchedulesDto>("No_Content", "There are no records available", System.Net.HttpStatusCode.NoContent);

                    }
                }

                //payer ID filter
                if (!string.IsNullOrEmpty(request.payer_id) && clientevvterminalpayerdetails.Any())
                {
                    var payeridArray = request.payer_id.Split(",");

                    clientevvterminalpayerdetails = clientevvterminalpayerdetails.Where(x => payeridArray.Where(y => y == x.PAYMENT_SOURCE_ID.ToString()).Any());
                }


                //Get paymentID from clientpaymentsources table
                string payerids = string.Join(", ", clientevvterminalpayerdetails.Select(x => x.PAYMENT_SOURCE_ID.ToString()).Distinct().ToArray());


                //To get  payer ids from DualEligibilePayers
                var dualeligibleterminalpayerreferences = string.Join(", ", clientevvterminalpayerdetails.Where(x => x.hierarchy == 'D').Select(x => x.Terminal_client_payer_id.ToString()).ToArray());

                if (!String.IsNullOrEmpty(dualeligibleterminalpayerreferences))
                {
                    var dualeligiblepayercolumns = "DualEligibilePayers.PaymentsourceId,DualEligibilePayers.DualEligibilePayerID";
                    var dualeligiblepayerfilters = _repositoryFilterConditions.InFilter("DualEligibilePayers.DualEligibilePayerID", dualeligibleterminalpayerreferences, FilterConditionDataTypeEnums.stringType).Build();
                    clientdualeligiblepayerdetails = await _clientsRepository.GetClientDualEligiblePaymentSourceslist(HHA, UserID, dualeligiblepayercolumns, dualeligiblepayerfilters);

                    if (clientdualeligiblepayerdetails.Count() > 0)
                    {
                        payerids = payerids + "," + string.Join(", ", clientdualeligiblepayerdetails.Select(x => x.PaymentsourceId.ToString()).Distinct().ToArray());

                    }
                }


                var clientaddtionalterminalpayerreferences = string.Join(", ", clientevvterminalpayerdetails.Where(x => x.hierarchy == 'S' || x.hierarchy == 'T').Select(x => x.Terminal_client_payer_id.ToString()).ToArray());
                // To get  payer ids from ClientAdditionalPaymentSources
                if (!String.IsNullOrEmpty(clientaddtionalterminalpayerreferences))
                {
                    var clientadditonalfilters = _repositoryFilterConditions.InFilter("ClientAdditionalPaymentSources.ADDITIONAL_PAYER_SOURCE_ID", clientaddtionalterminalpayerreferences, FilterConditionDataTypeEnums.stringType).Build();
                    var clientaddtionalcolumns = "ClientAdditionalPaymentSources.PAYER_SOURCE_ID,ClientAdditionalPaymentSources.ADDITIONAL_PAYER_SOURCE_ID";
                    clientadditionalpaymentdetails = await _clientsRepository.GetClientAdditionalPaymentSourcesList(HHA, UserID, clientaddtionalcolumns, clientadditonalfilters);

                    if (clientadditionalpaymentdetails.Count() > 0)
                    {
                        payerids = payerids + "," + string.Join(", ", clientadditionalpaymentdetails.Select(x => x.PAYER_SOURCE_ID.ToString()).Distinct().ToArray());

                    }
                }

                List<Terminalpayerschedules> terminalSchedules = new List<Terminalpayerschedules>();

                payerfilters = _repositoryFilterConditions.InFilter("PAYMENT_SOURCE_ID", payerids, FilterConditionDataTypeEnums.stringType).Build();
                var payerdetails = await _paymentSourcesRepository.GetPaymentsources(HHA, UserID, payerColumns, payerfilters);

                if (request.terminal_export_registered == true)
                {
                    var terminalpayerscheduledetails = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerSchedules(HHA, UserID, terminalpayerschedulecolumns, terminalpayerschedulefilters);

                    var terminalpayerchildscheduledetails = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerChildSchedules(HHA, UserID, terminalpayerchildschedulecolumns, terminalpayerchildschedulefilters);


                    if (terminalpayerscheduledetails.Any() || terminalpayerchildscheduledetails.Any())
                    {
                       
                        serviceids = string.Join(", ", terminalpayerscheduledetails.Select(x => x.SERVICECODE_ID.ToString()).Distinct().ToArray());

                        if (terminalpayerchildscheduledetails.Any())
                        {
                            serviceids += "," + string.Join(", ", terminalpayerchildscheduledetails.Select(x => x.SERVICECODE_ID.ToString()).Distinct().ToArray());
                        }

                        servicefilters = _repositoryFilterConditions.InFilter("SERVICE_CODE_ID", serviceids, FilterConditionDataTypeEnums.stringType).Build();

                        if (!String.IsNullOrEmpty(request.disc))
                        {
                            servicefilters += _repositoryFilterConditions.InFilter("ServiceGroups.GroupDiscplineName", request.disc, FilterConditionDataTypeEnums.stringType).Build();

                        }
                        var servicedetails = await _servicecodeRepository.GetServiceCodes(HHA, UserID, serviceColumns, servicefilters);


                        terminalpayerchildscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null).ToList()
                            .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = (_terminalschedules.TOTALMINUTES_ACTUAL / 60).ToString(); });

                        terminalpayerchildscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL / 60) < 10).ToList()
                            .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = "0" + _terminalschedules.Edited_Hours; });


                        terminalpayerchildscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL % 60) >= 10).ToList()
                            .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = _terminalschedules.Edited_Hours + ":" + (_terminalschedules.TOTALMINUTES_ACTUAL % 60).ToString(); });

                        terminalpayerchildscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL % 60) < 10).ToList()
                           .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = _terminalschedules.Edited_Hours + ":" + "0" + (_terminalschedules.TOTALMINUTES_ACTUAL % 60).ToString(); });


                        terminalSchedules =
                        terminalpayerscheduledetails.Select(x => new Terminalpayerschedules()
                        {
                            CaregiverTasksTerminalPayerID = x.CaregiverTasksTerminalPayerID,
                            HHA = x.HHA,
                            ClientID = x.ClientID,
                            CgtaskID = x.CgTaskID,
                            ChildScheduleID = 0,
                            PayerID = x.PAYMENT_SOURCE,
                            Hierarchy = x.Hierarchy,
                            TerminalPayerID = x.TerminalPayerID,
                            is_exported = x.is_exported,
                            PayerID_terminal = 0,
                            ScheduleDate = x.PLANNED_DATE,
                            ServiceID = x.SERVICECODE_ID,
                            Status = x.STATUS,
                            CheckinTime = x.ACCTUAL_START_TIME,
                            CheckoutTime = x.ACCTUAL_END_TIME,
                            Actual_Hours = x.EDITED_HOURS ?? "",
                            is_Billed = x.IS_BILLED,
                            is_Paid = x.IS_PAID,
                            is_Evv_Schedule = x.isEvvschedule,
                            Planned_StartTime = x.PLANNED_START_TIME,
                            Planned_EndTime = x.PLANNED_END_TIME,
                            Planned_Hours = x.ACCTUAL_END_TIME - x.ACCTUAL_START_TIME,
                            IsBillable = x.IS_BILLABLE,
                            IsPayable = x.IS_PAYABLE,
                            failure_code = x.Last_failed_error_code ?? "",
                            failure_message = x.Last_failed_error_message ?? "",
                            last_exported_on = x.Last_success_exported_on,
                            last_export_status = x.Last_export_status == 9 ? "Success" : (x.Last_export_status == 1 || x.Last_export_status == 4 || x.Last_export_status == 5 || x.Last_export_status == 6 || x.Last_export_status == 0) ? "Pending" : "Failed"

                        }).Union(terminalpayerchildscheduledetails.Select(x => new Terminalpayerschedules()
                        {
                            CaregiverTasksTerminalPayerID = x.CaregiverChildTaskTerminalPayerID,
                            HHA = x.HHA,
                            ClientID = x.ClientID,
                            CgtaskID = x.CgTaskID,
                            ChildScheduleID = x.ChildScheduleID,
                            PayerID = x.PAYMENT_SOURCE,
                            Hierarchy = x.Hierarchy,
                            TerminalPayerID = x.TerminalPayerID,
                            is_exported = x.is_exported,
                            PayerID_terminal = 0,
                            ScheduleDate = x.SCHEDULE_DATE,
                            ServiceID = x.SERVICECODE_ID,
                            Status = x.status,
                            CheckinTime = x.ACTUAL_STARTTIME,
                            CheckoutTime = x.ACTUAL_ENDTIME,
                            Actual_Hours = x.Edited_Hours,
                            is_Billed = x.IS_BILLED,
                            is_Paid = x.IsPayrolled,
                            is_Evv_Schedule = x.isEvvschedule,
                            Planned_StartTime = x.PLANNED_STARTTIME,
                            Planned_EndTime = x.PLANNED_ENDTIME,
                            Planned_Hours = x.ACTUAL_ENDTIME - x.ACTUAL_STARTTIME,
                            IsBillable = x.CANBILL,
                            IsPayable = false,
                            failure_code = x.Last_failed_error_code ?? "",
                            failure_message = x.Last_failed_error_message ?? "",
                            last_exported_on = x.Last_success_exported_on,
                            last_export_status = x.Last_export_status == 9 ? "Success" : (x.Last_export_status == 1 || x.Last_export_status == 4 || x.Last_export_status == 5 || x.Last_export_status == 6 || x.Last_export_status == 0) ? "Pending" : "Failed"
                        })).ToList();

                        if (terminalSchedules.Any())
                        {
                            terminalSchedules.Where(x => x.ChildScheduleID > 0).ToList().ForEach(_terminalschedules => { _terminalschedules.IsPayable = terminalpayerscheduledetails.Where(x => x.IsSplitForPayroll == true && x.IS_PAYABLE == true).Select(x => x.IS_PAYABLE).FirstOrDefault(); });


                            if (clientdualeligiblepayerdetails.Any())
                            {
                                terminalSchedules.Where(x => x.Hierarchy == 'D').ToList().ForEach(_terminalschedules => { _terminalschedules.PayerID_terminal = clientdualeligiblepayerdetails.Where(x => x.DualEligibilePayerID == _terminalschedules.TerminalPayerID).Select(x => x.PaymentsourceId).FirstOrDefault(); });

                            }
                            if (clientadditionalpaymentdetails.Any())
                            {
                                terminalSchedules.Where(x => x.Hierarchy == 'T' || x.Hierarchy == 'S').ToList().ForEach(_terminalschedules => { _terminalschedules.PayerID_terminal = clientadditionalpaymentdetails.Where(x => x.ADDITIONAL_PAYER_SOURCE_ID == _terminalschedules.TerminalPayerID).Select(x => x.PAYER_SOURCE_ID).FirstOrDefault(); });

                            }

                        }

                        outputdto.evvTerminalPayerSearchSchedulesList = (from _terminalpayerscheduledetails in terminalSchedules

                                                                         join _clientevvterminalpayerdetails in clientevvterminalpayerdetails
                                                                         on _terminalpayerscheduledetails.ClientID equals _clientevvterminalpayerdetails.client_id
                                                                         join _payerdetails in payerdetails on _terminalpayerscheduledetails.PayerID
                                                                         equals _payerdetails.PAYMENT_SOURCE_ID
                                                                         join _servicedetails in servicedetails on _terminalpayerscheduledetails.ServiceID equals _servicedetails.SERVICE_CODE_ID
                                                                         join _terminalpayerdetail in payerdetails
                                                                         on _terminalpayerscheduledetails.PayerID_terminal equals _terminalpayerdetail.PAYMENT_SOURCE_ID
                                                                         where _clientevvterminalpayerdetails.hierarchy == _terminalpayerscheduledetails.Hierarchy
                                                                         && _terminalpayerscheduledetails.TerminalPayerID == _clientevvterminalpayerdetails.Terminal_client_payer_id
                                                                         select new EvvTerminalPayerSchedulesListDto()
                                                                         {
                                                                             CGTASK_ID = _terminalpayerscheduledetails.CgtaskID,
                                                                             CaregiverTasksTerminalPayerID = _terminalpayerscheduledetails.CaregiverTasksTerminalPayerID,
                                                                             Client_ID = _terminalpayerscheduledetails.ClientID,
                                                                             Client_Name = new[] { _clientevvterminalpayerdetails.LAST_NAME, _clientevvterminalpayerdetails.FIRST_NAME, _clientevvterminalpayerdetails.PATIENT_ID }.ClientNameFormat(),
                                                                             Planned_Start_Time = _terminalpayerscheduledetails.Planned_StartTime.ToTimeAMPMFormat(),
                                                                             Planned_End_Time = _terminalpayerscheduledetails.Planned_EndTime.ToTimeAMPMFormat(),
                                                                             Planned_Hours = _terminalpayerscheduledetails.Planned_Hours.ToString(),
                                                                             Admit_No = _clientevvterminalpayerdetails.AdmitNo,
                                                                             Schedule_Date = _terminalpayerscheduledetails.ScheduleDate.ToDateFormat(),
                                                                             Payer_ID = _payerdetails.PAYMENT_SOURCE_ID,
                                                                             Payer_Name = _payerdetails.ORG_NAME,
                                                                             Terminal_Payer_ID = _clientevvterminalpayerdetails.Terminal_client_payer_id,
                                                                             ChildScheduleID = _terminalpayerscheduledetails.ChildScheduleID,
                                                                             Terminal_Payer_Name = _terminalpayerdetail.ORG_NAME,
                                                                             Service_ID = _servicedetails.SERVICE_CODE_ID,
                                                                             Service_Name = _servicedetails.DESCRIPTION,
                                                                             Schedule_Status = _terminalpayerscheduledetails.Status,
                                                                             Actual_Start_Time = _terminalpayerscheduledetails.CheckinTime.ToTimeAMPMFormat(),
                                                                             Actual_End_Time = _terminalpayerscheduledetails.CheckoutTime.ToTimeAMPMFormat(),
                                                                             Actual_Hours = _terminalpayerscheduledetails.Actual_Hours,
                                                                             is_Billed = _terminalpayerscheduledetails.is_Billed,
                                                                             is_Paid = _terminalpayerscheduledetails.is_Paid,
                                                                             is_Evv_Schedule = _terminalpayerscheduledetails.is_Evv_Schedule,
                                                                             is_Billable = _terminalpayerscheduledetails.IsBillable,
                                                                             is_Payable = _terminalpayerscheduledetails.IsPayable,
                                                                             failure_message = _terminalpayerscheduledetails.failure_message,
                                                                             failure_code = _terminalpayerscheduledetails.failure_code,
                                                                             last_exported_on = _terminalpayerscheduledetails.last_exported_on != null ? _terminalpayerscheduledetails.last_exported_on.ToDateFormat() + " " + _terminalpayerscheduledetails.last_exported_on.ToTimeAMPMFormat() : "",
                                                                             aggregator_name = _clientevvterminalpayerdetails.VendorName + " [" + _clientevvterminalpayerdetails.VendorState + " - " + _clientevvterminalpayerdetails.VendorVersionCode + "]",
                                                                             lob = "",
                                                                             service_disc = _servicedetails.SERVICE_TYPE,
                                                                             hierarchy = _clientevvterminalpayerdetails.hierarchy.ToString(),
                                                                             last_export_status = _terminalpayerscheduledetails.last_export_status,
                                                                             Terminal_Paymentsource_ID = _terminalpayerscheduledetails.PayerID_terminal
                                                                         }).ToList();

                    }
                }
                else
                {
                    var parentscheduledetails = await _caregivertasksRepository.GetAllSchedulesList(HHA, UserID, parentscheduleColumns, parentscheduleFilters);

                    if (parentscheduledetails.Count() > 0)
                    {
                        var cgtasIds = string.Join(", ", parentscheduledetails.Select(x => x.CGTASK_ID.ToString()).Distinct().ToArray());
                        terminalpayerschedulecolumns = " CaregiverTasksTerminalPayer.CgTaskID ";
                        terminalpayerschedulefilters += _repositoryFilterConditions.InFilter("CaregiverTasks.CGTASK_ID", cgtasIds, FilterConditionDataTypeEnums.stringType).Build();
                        var terminalpayerscheduledetails = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerSchedules(HHA, UserID, terminalpayerschedulecolumns, terminalpayerschedulefilters);

                        var terminalpayerchildscheduledetails = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerChildSchedules(HHA, UserID, terminalpayerchildschedulecolumns, terminalpayerchildschedulefilters);


                        if (terminalpayerscheduledetails.Any())
                        {
                            parentscheduledetails = parentscheduledetails.Where(x => x.CGTASK_ID != terminalpayerscheduledetails.Where(y => y.CgTaskID == x.CGTASK_ID).Select(y => y.CgTaskID).FirstOrDefault());
                        }

                        IEnumerable<int> parentinfo = null;
                        if (terminalpayerchildscheduledetails.Any())
                        {
                            parentinfo = from item in terminalpayerchildscheduledetails group item by item.CgTaskID into groupedlist select groupedlist.Key;
                            parentscheduledetails = parentscheduledetails.Where(x => x.CGTASK_ID != parentinfo.Where(y => y == x.CGTASK_ID).Select(y => y).FirstOrDefault());
                        }


                        serviceids = string.Join(", ", parentscheduledetails.Select(x => x.SERVICECODE_ID.ToString()).Distinct().ToArray());
                        servicefilters = _repositoryFilterConditions.InFilter("SERVICE_CODE_ID", serviceids, FilterConditionDataTypeEnums.stringType).Build();
                        if (!String.IsNullOrEmpty(request.disc))
                        {
                            servicefilters += _repositoryFilterConditions.InFilter("ServiceGroups.GroupDiscplineName", request.disc, FilterConditionDataTypeEnums.stringType).Build();
                        }
                        var scheduleservicedetails = await _servicecodeRepository.GetServiceCodes(HHA, UserID, serviceColumns, servicefilters);

                        var childscheduledetails = await _caregivertasksRepository.GetAllChildSchedulesList(HHA, UserID, childscheduleColumns, childscheduleFilters);
                         
                        if (childscheduledetails.Any())
                        {
                            if (terminalpayerchildscheduledetails.Any())
                            {
                                childscheduledetails = childscheduledetails.Where(x => x.Child_Schedule_Id != terminalpayerchildscheduledetails.Where(y => y.ChildScheduleID == x.Child_Schedule_Id).Select(y => y.ChildScheduleID).FirstOrDefault());
                            }

                            childscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null).ToList()
                             .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = (_terminalschedules.TOTALMINUTES_ACTUAL / 60).ToString(); });

                            childscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL / 60) < 10).ToList()
                                .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = "0" + _terminalschedules.Edited_Hours; });


                            childscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL % 60) >= 10).ToList()
                                .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = _terminalschedules.Edited_Hours + ":" + (_terminalschedules.TOTALMINUTES_ACTUAL % 60).ToString(); });

                            childscheduledetails.Where(x => x.TOTALMINUTES_ACTUAL != null && (x.TOTALMINUTES_ACTUAL % 60) < 10).ToList()
                               .ForEach(_terminalschedules => { _terminalschedules.Edited_Hours = _terminalschedules.Edited_Hours + ":" + "0" + (_terminalschedules.TOTALMINUTES_ACTUAL % 60).ToString(); });


                        }

                        //if (childscheduledetails.Any())
                        //{
                        //    parentscheduledetails = parentscheduledetails.Where(x => x.CGTASK_ID != childscheduledetails.Where(y => y.PARENT_CGTASK_ID == x.CGTASK_ID).Select(z => z.PARENT_CGTASK_ID).FirstOrDefault());
                        //   // parentscheduledetails = 
                        //    if (!parentscheduledetails.Any())
                        //        childscheduledetails = new List<CaregiverTaskChildSchedulesEntity>();
                        //    else 
                        //        childscheduledetails = childscheduledetails.Where(x => x.PARENT_CGTASK_ID == parentscheduledetails.Where(y => y.CGTASK_ID == x.PARENT_CGTASK_ID).Select(y => y.CGTASK_ID).FirstOrDefault());
                        //}

                        terminalSchedules =
                            parentscheduledetails.Select(x => new Terminalpayerschedules()
                            {
                                ServiceID = x.SERVICECODE_ID,
                                PayerID = x.PAYMENT_SOURCE,
                                PayerID_terminal = 0,
                                CheckinTime = x.ACCTUAL_START_TIME,
                                CheckoutTime = x.ACCTUAL_END_TIME,
                                Actual_Hours = x.EDITED_HOURS ?? "",
                                is_Billed = Convert.ToBoolean(x.IS_BILLED),
                                is_Paid = Convert.ToBoolean(x.IS_PAID),
                                ClientID = x.CLIENT_ID,
                                CgtaskID = x.CGTASK_ID,
                                ChildScheduleID = 0,
                                is_Evv_Schedule = Convert.ToBoolean(x.isEvvschedule),
                                ScheduleDate = Convert.ToDateTime(x.PLANNED_DATE),
                                Status = x.STATUS,
                                Planned_StartTime = x.PLANNED_START_TIME,
                                Planned_EndTime = x.PLANNED_END_TIME,
                                Planned_Hours = x.PLANNED_END_TIME - x.PLANNED_START_TIME,
                                IsPayable = Convert.ToBoolean(x.IS_PAYABLE),
                                IsBillable = x.IS_BILLABLE


                            }).Union(childscheduledetails.Select(x => new Terminalpayerschedules()
                            {
                                ServiceID = x.SERVICECODE_ID,
                                PayerID = x.PAYMENT_SOURCE,
                                PayerID_terminal = 0,
                                CheckinTime = x.ACTUAL_STARTTIME,
                                CheckoutTime = x.ACTUAL_ENDTIME,
                                Actual_Hours = "",
                                is_Billed = false,
                                is_Paid = false,
                                ClientID = x.CLIENT,
                                CgtaskID = x.PARENT_CGTASK_ID,
                                ChildScheduleID = x.Child_Schedule_Id,
                                is_Evv_Schedule = x.isEvvschedule,
                                ScheduleDate = x.SCHEDULE_DATE,
                                Status = x.STATUS,
                                Planned_StartTime = x.PLANNED_STARTTIME,
                                Planned_EndTime = x.PLANNED_ENDTIME,
                                Planned_Hours = x.PLANNED_ENDTIME - x.PLANNED_STARTTIME,
                                IsBillable = x.CANBILL,
                                IsPayable = false
                            })).ToList();


                        terminalSchedules.Where(x => x.ChildScheduleID > 0).ToList().ForEach(_terminalschedules => { _terminalschedules.IsPayable = (bool)parentscheduledetails.Where(x => x.IsSplitForPayroll == true && (x?.IS_PAYABLE ?? false) == true).Select(x => x?.IS_PAYABLE ?? false).FirstOrDefault(); });

                        terminalSchedules.Join(clientevvterminalpayerdetails, (_scheduleinfo) => _scheduleinfo.ClientID, (_payerdetails) => _payerdetails.client_id,
                               (_scheduleinfo, _payerdetails) =>
                               {

                                   _scheduleinfo.TerminalPayerID = _payerdetails.Terminal_client_payer_id;

                                   return _scheduleinfo;
                               }
                               ).ToList();


                        if (clientdualeligiblepayerdetails.Any())
                        {
                            terminalSchedules.ForEach(_terminalschedules => { _terminalschedules.PayerID_terminal = clientdualeligiblepayerdetails.Where(x => x.DualEligibilePayerID == _terminalschedules.TerminalPayerID).Select(x => x.PaymentsourceId).FirstOrDefault(); });

                        }
                        if (clientadditionalpaymentdetails.Any())
                        {
                            terminalSchedules.ForEach(_terminalschedules => { _terminalschedules.PayerID_terminal = clientadditionalpaymentdetails.Where(x => x.ADDITIONAL_PAYER_SOURCE_ID == _terminalschedules.TerminalPayerID).Select(x => x.PAYER_SOURCE_ID).FirstOrDefault(); });

                        }


                        outputdto.evvTerminalPayerSearchSchedulesList = (from _parentscheduledetails in terminalSchedules

                                                                         join _clientevvterminalpayerdetails in clientevvterminalpayerdetails on _parentscheduledetails.ClientID
                                                                         equals _clientevvterminalpayerdetails.client_id
                                                                         join _servicedetails in scheduleservicedetails
                                                                         on _parentscheduledetails.ServiceID equals _servicedetails.SERVICE_CODE_ID
                                                                         join _payerdetails in payerdetails
                                                                         on _parentscheduledetails.PayerID equals _payerdetails.PAYMENT_SOURCE_ID
                                                                         join _terminalpayerdetail in payerdetails
                                                                          on _parentscheduledetails.PayerID_terminal equals _terminalpayerdetail.PAYMENT_SOURCE_ID
                                                                         select new EvvTerminalPayerSchedulesListDto()
                                                                         {
                                                                             CaregiverTasksTerminalPayerID = _parentscheduledetails.CaregiverTasksTerminalPayerID,
                                                                             CGTASK_ID = _parentscheduledetails.CgtaskID,
                                                                             Client_ID = _parentscheduledetails.ClientID,
                                                                             Client_Name = new[] { _clientevvterminalpayerdetails.LAST_NAME, _clientevvterminalpayerdetails.FIRST_NAME, _clientevvterminalpayerdetails.PATIENT_ID }.ClientNameFormat(),
                                                                             ChildScheduleID = _parentscheduledetails.ChildScheduleID,
                                                                             Admit_No = _clientevvterminalpayerdetails.AdmitNo,
                                                                             Planned_Start_Time = _parentscheduledetails.Planned_StartTime.ToTimeAMPMFormat(),
                                                                             Planned_End_Time = _parentscheduledetails.Planned_EndTime.ToTimeAMPMFormat(),
                                                                             Planned_Hours = _parentscheduledetails.Planned_Hours.ToString(),
                                                                             Schedule_Date = _parentscheduledetails.ScheduleDate.ToDateFormat(),
                                                                             Payer_ID = _payerdetails.PAYMENT_SOURCE_ID,
                                                                             Payer_Name = _payerdetails.ORG_NAME,
                                                                             Terminal_Payer_ID = _clientevvterminalpayerdetails.Terminal_client_payer_id,
                                                                             Terminal_Payer_Name = _terminalpayerdetail.ORG_NAME,
                                                                             Service_ID = _servicedetails.SERVICE_CODE_ID,
                                                                             Service_Name = _servicedetails.DESCRIPTION,
                                                                             Schedule_Status = _parentscheduledetails.Status,
                                                                             Actual_Start_Time = _parentscheduledetails.CheckinTime.ToTimeAMPMFormat(),
                                                                             Actual_End_Time = _parentscheduledetails.CheckoutTime.ToTimeAMPMFormat(),
                                                                             Actual_Hours = _parentscheduledetails.Actual_Hours,
                                                                             is_Billed = _parentscheduledetails.is_Billed,
                                                                             is_Paid = _parentscheduledetails.is_Paid,
                                                                             is_Evv_Schedule = _parentscheduledetails.is_Evv_Schedule,
                                                                             is_Billable = _parentscheduledetails.IsBillable,
                                                                             is_Payable = _parentscheduledetails.IsPayable,
                                                                             aggregator_name = _clientevvterminalpayerdetails.VendorName + " [" + _clientevvterminalpayerdetails.VendorState + " - " + _clientevvterminalpayerdetails.VendorVersionCode + "]",
                                                                             lob = "",
                                                                             service_disc = _servicedetails.SERVICE_TYPE,
                                                                             hierarchy = _clientevvterminalpayerdetails.hierarchy.ToString(),
                                                                             last_export_status = "",
                                                                             Terminal_Paymentsource_ID = _parentscheduledetails.PayerID_terminal
                                                                         }).ToList();

                        if (!string.IsNullOrEmpty(request.terminal_payer_hierarchy))
                        {
                            request.terminal_payer_hierarchy = request.terminal_payer_hierarchy.TrimEnd(',');

                            outputdto.evvTerminalPayerSearchSchedulesList = outputdto.evvTerminalPayerSearchSchedulesList.Where(x => x.hierarchy.Contains(request.terminal_payer_hierarchy)).ToList();



                        }

                    }

                }

                if (outputdto.evvTerminalPayerSearchSchedulesList.Count() > 0)
                {
                    var LOBFilter = "";
                    var LOBColumns = " LOB_ID,LOB_NAME ";
                    var hhaLobs = await _hHALineofbusinessRepository.GetHHALOBs(HHA, UserID, LOBColumns, LOBFilter);

                    if (hhaLobs?.Count() > 0)
                    {
                        string clientIDs = string.Join(",", outputdto.evvTerminalPayerSearchSchedulesList.Select(x => x.Client_ID).ToList());
                        string clientFilters = _repositoryFilterConditions.InFilter("CLIENT_ID", clientIDs, FilterConditionDataTypeEnums.integerType).Build();

                        var clientlists = await _clientsRepository.GetALLClients(HHA, UserID, "CLIENT_ID,LOB_ID", clientFilters);

                        outputdto.evvTerminalPayerSearchSchedulesList.ForEach(_terminalschedules => { _terminalschedules.lob_ID = clientlists.Where(x => x.CLIENT_ID == _terminalschedules.Client_ID).Select(x => x.LOB_ID).FirstOrDefault(); });


                        outputdto.evvTerminalPayerSearchSchedulesList.ForEach(_terminalschedules => { _terminalschedules.lob = hhaLobs.Where(x => x.LOB_ID == _terminalschedules.lob_ID).Select(x => x.LOB_NAME).FirstOrDefault(); });
                    }
                }

                if (!string.IsNullOrEmpty(request.terminal_payer_id))
                {
                    request.terminal_payer_id = request.terminal_payer_id.TrimEnd(',');
                    var teminalpayerIDarray = request.terminal_payer_id.Split(",").Select(Int32.Parse).ToList(); ;


                    outputdto.evvTerminalPayerSearchSchedulesList = outputdto.evvTerminalPayerSearchSchedulesList.Where(x => teminalpayerIDarray.Where(y => y == x.Terminal_Paymentsource_ID).Any()).ToList();
                }

                var orderedList = outputdto;
                orderedList.evvTerminalPayerSearchSchedulesList = orderedList.evvTerminalPayerSearchSchedulesList.OrderBy(x => Convert.ToDateTime(x.Schedule_Date)).ToList();
                outputdto = orderedList;
                totalrecords = outputdto.evvTerminalPayerSearchSchedulesList.Count();
                outputdto.TotalRecord = totalrecords;
                outputdto.PageNo = request.page_no;
                outputdto.RecordPerPage = 100;
                outputdto.TotalPages = 1;

                if (totalrecords > 100)
                {
                    int TotalPages = Convert.ToInt32(totalrecords / outputdto.RecordPerPage);
                    if (Convert.ToInt32(totalrecords % outputdto.RecordPerPage) > 0)
                        TotalPages += 1;

                    outputdto.TotalPages = TotalPages;
                }
                int skipRecords = 0;

                if (request.page_no > 1)
                    skipRecords = (request.page_no - 1) * 100;

                if (request.page_no > 0 && totalrecords > 100 && request.purpose == 0)
                {
                    outputdto.evvTerminalPayerSearchSchedulesList = outputdto.evvTerminalPayerSearchSchedulesList.Skip(skipRecords).Take(100).ToList();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return Response.Ok<EVVTerminalPayerSerachSchedulesDto>(outputdto);

        }
    }

}

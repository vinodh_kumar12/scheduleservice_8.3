﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleValidationQueryHandler : IHandlerWrapper<ScheduleValidationQuery, string>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISchedule _schedule;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IMapper _mapper;

        public ScheduleValidationQueryHandler(ISchedule schedule, ICaregivertasksRepository caregivertasksRepository,
            IRepositoryFilterConditions _repositoryFilterConditions, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            this._schedule = schedule;
            this._caregivertasksRepository = caregivertasksRepository;
            this._repositoryFilterConditions = _repositoryFilterConditions;
            this._mapper = mapper;
            this._httpContextAccessor = httpContextAccessor;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<string>> Handle(ScheduleValidationQuery request, CancellationToken cancellationToken)
        {
            string result = "";
            string FailureErrorMessage = "";
            string error_details = "";
            bool has_hard_stop = false;
            List<ValidationErrorDetail> validationErrorDetailList = new List<ValidationErrorDetail>();
            ValidationErrorDetail validationErrorDetail = new ValidationErrorDetail();
            try
            {
                //Schedule Info
                var CaregiverTaskFilter = _repositoryFilterConditions.EqualFilter("CaregiverTasks.Publish_UID", request.schedule_uuid, FilterConditionDataTypeEnums.stringType)
                                              .Build();

                var CaregiverTaskRequiredColumns = "CaregiverTasks.Publish_UID,CaregiverTasks.CGTASK_ID";

                var CaregiverTaskDetailList = await _caregivertasksRepository.GetScheduleBasicList(request.HHA, request.UserId, CaregiverTaskRequiredColumns, CaregiverTaskFilter);

                var CaregiverTaskDetail = CaregiverTaskDetailList.FirstOrDefault();

                if (CaregiverTaskDetail == null)
                {
                    return Response.NoContent<string>("Invalid schedule_uuid");
                }

                var ValidationResultList = await _schedule.ValidateSchedule(request.HHA, request.UserId, CaregiverTaskDetail.CGTASK_ID);

                if (ValidationResultList.Any())
                {
                    if (ValidationResultList.Where(x => x.is_soft_warning == false).Any())
                        has_hard_stop = true;

                    if(has_hard_stop)
                        ValidationResultList = ValidationResultList.Where(x => x.is_soft_warning == false);

                    if (ValidationResultList.Count() > 1)
                    {
                        validationErrorDetailList = _mapper.Map<List<ValidationErrorDetail>>(ValidationResultList);

                        error_details = JsonConvert.SerializeObject(validationErrorDetailList);

                        return Response.ValidationError<string>("", error_details, "");
                    }
                    else
                    {
                        var ValidationResult = ValidationResultList.Select(x => x).FirstOrDefault();

                        validationErrorDetail = _mapper.Map<ValidationErrorDetail>(ValidationResult);

                        error_details = JsonConvert.SerializeObject(validationErrorDetail);

                        return Response.ValidationError<string>("", error_details, "");
                    }
                }
                else
                {
                    result = "{\"error_code\": null,\"error_message\":\"Validated Successfully\",\"isValid\":true,\"Response_payload\":null}";
                }

                return Response.Ok<string>(result);
            }
            catch (Exception ex)
            {
                FailureErrorMessage = "{\"error_code\": "+ResponseErrorCodes.InternalServerFailed+",\"error_message\":\"Failed to get the schedule validation(s)\"}";
                return Response.ValidationError<string>("", FailureErrorMessage);
            }
        }

    }
}

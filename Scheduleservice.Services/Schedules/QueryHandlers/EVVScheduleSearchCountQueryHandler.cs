﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Schedule;
using AutoMapper;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Schedules;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class EVVScheduleSearchCountQueryHandler : IHandlerWrapper<EVVScheduleSearchCountQuery, string>
    {
        private readonly ISchedule _scheduleService;
        private readonly IMapper _mapper;
        private readonly ILogger<EVVScheduleSearchCountQueryHandler> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContextDto;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public EVVScheduleSearchCountQueryHandler(ISchedule scheduleService, IMapper mapper, ILogger<EVVScheduleSearchCountQueryHandler> logger,
           IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
           IRepositoryFilterConditions repositoryFilterConditions)
        {
            _scheduleService = scheduleService;
            _mapper = mapper;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;

            _actionContextDto = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<string>> Handle(EVVScheduleSearchCountQuery request, CancellationToken cancellationToken)
        {
            string schedulesFilter = "";
            string result = "";
            int TotalVisit = 0;
            int EVVDirtyFlagTrueCount = 0;
            int EVVDirtyFlagFalseCount = 0;
            ScheduleFitlersDto scheduleFitlersDto = new ScheduleFitlersDto();
            try
            {
                scheduleFitlersDto = _mapper.Map<ScheduleFitlersDto>(request);

                schedulesFilter = Common.GetScheduleFilters(scheduleFitlersDto);

                var schedules = await _caregivertasksRepository.GetAllSchedulesList(_actionContextDto.HHA, _actionContextDto.User, "CaregiverTasks.CGTASK_ID,CaregiverTaskAdditional2.IsEVVDirty", schedulesFilter);

                if (schedules.Any())
                {
                    TotalVisit = schedules.Count();
                    EVVDirtyFlagTrueCount = schedules.Where(x => x.IsEVVDirty == true).Count();
                    EVVDirtyFlagFalseCount = schedules.Where(x => x.IsEVVDirty == false).Count();

                    if (request.evv_dirty_flag!=null)
                        result = "{\"TotalVisit\":"+ TotalVisit + ",\"EVVDirtyFlag\":{\"True\":"+EVVDirtyFlagTrueCount+",\"False\":"+EVVDirtyFlagFalseCount+"}}";
                    else
                        result = "{\"TotalVisit\":"+TotalVisit+"}";
                }
                else
                    return Response.NoContent<string>(result);

                return Response.Ok<string>(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Class: EVVScheduleSearchCountQueryHandler.cs; Method: Handle; Message: {ex.Message}");
                return Response.ValidationError<string>(ex.Message, null);
            }

        }
    }
}

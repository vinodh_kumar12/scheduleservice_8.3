﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response; 
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Extensions;
using System.Linq;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Services.Schedules.ValidationHandlers;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleExceptionsValidationRulesQueryHandler : IHandlerWrapper<ScheduleExceptionsValidationRulesQuery, IEnumerable<ScheduleValidationRulesDto>>
    {
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly IConflictExceptionsValidationRules _conflictExceptionsValidationRules;
        public ScheduleExceptionsValidationRulesQueryHandler(ICaregivertasksRepository caregivertasksRepository, IClientsRepository clientsRepository, IConflictExceptionsValidationRules conflictExceptionsValidationRules)
        {
            _caregivertasksRepository = caregivertasksRepository;
            _clientsRepository = clientsRepository;
            _conflictExceptionsValidationRules = conflictExceptionsValidationRules;
        }
        public async Task<Response<IEnumerable<ScheduleValidationRulesDto>>> Handle(ScheduleExceptionsValidationRulesQuery request, CancellationToken cancellationToken)
        {
            //get schedules info
            var caregivertask = await _caregivertasksRepository.GetScheduleBasicInfo(request.HHA, request.UserId, request.CGTask_ID);

            //get client info
            var Client = await _clientsRepository.GetClientsBasicInfo(request.HHA, request.UserId, caregivertask.CLIENT_ID);

            int CAREGIVER = (caregivertask.CAREGIVER ?? 0);
            var scheduleValidationRulesDtos = await _conflictExceptionsValidationRules.GetConflictExceptionsValidationRules_old(request.HHA, request.UserId, Client.HHA_BRANCH_ID, caregivertask.CLIENT_ID, caregivertask.PAYMENT_SOURCE, caregivertask.SERVICECODE_ID, CAREGIVER);

            return Response.Ok<IEnumerable<ScheduleValidationRulesDto>>(scheduleValidationRulesDtos);

        }
    }
}

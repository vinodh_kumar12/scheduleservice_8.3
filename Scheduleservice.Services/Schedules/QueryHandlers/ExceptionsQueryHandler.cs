﻿using AutoMapper; 
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Schedule; 
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ExceptionsQueryHandler : IHandlerWrapper<ScheduleEVVExceptionsListQuery, IEnumerable<ExceptionListDTO>>
    {
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEVVExceptions;
        private readonly IMapper _mapper;
        private readonly IClientsRepository _clientsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ExceptionsQueryHandler(ICaregivertasksRepository caregivertasksRepository, ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptions,
                     IMapper mapper,IClientsRepository clientsRepository, IRepositoryFilterConditions repositoryFilterConditions)
        {
            _caregivertasksRepository = caregivertasksRepository;
            _caregiverTaskEVVExceptions = caregiverTaskEVVExceptions;
            _mapper = mapper;
            _clientsRepository = clientsRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
        }

        public async Task<Response<IEnumerable<ExceptionListDTO>>> Handle(ScheduleEVVExceptionsListQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<ExceptionListDTO> ExceptionListDTO = new List<ExceptionListDTO>();
            int HHA = request.HHA;
            int UserID = request.UserId;
            int[] Locations = request.Locations;
            try
            {
                string scheduleFilterDetails = _repositoryFilterConditions.GreaterThanEqualFilter("PLANNED_DATE", request.StartDate, FilterConditionDataTypeEnums.datetimeType)
                                                                          .LessThanEqualFilter("PLANNED_DATE", request.EndDate, FilterConditionDataTypeEnums.datetimeType)
                                                                          .Build();

                var schedulelists = await _caregivertasksRepository.GetScheduleBasicList(request.HHA, request.UserId, "CGTASK_ID,CLIENT_ID", scheduleFilterDetails);
                string clientIDs = string.Join(",", schedulelists.Select(x => x.CLIENT_ID).ToList());
                string clientFilters = _repositoryFilterConditions.InFilter("CLIENT_ID", clientIDs, FilterConditionDataTypeEnums.integerType).Build();

                var clientlists = await _clientsRepository.GetALLClients(HHA, UserID, "CLIENT_ID,HHA_BRANCH_ID", clientFilters);
                var Filteredclientlist = from Location in Locations
                                         join
         clientlist in clientlists
         on Location equals clientlist.HHA_BRANCH_ID
                                         select clientlist;

                var FinalScheduleList =
                   from schedulelist in schedulelists
                   join clientlist in Filteredclientlist
                   on schedulelist.CLIENT_ID
                   equals clientlist.CLIENT_ID


                   select new { schedulelist.CGTASK_ID, clientlist.CLIENT_ID };
                //var FinalList = FinalScheduleList.Where(s =>Locations.Contains(s.CLIENT_ID.ToString()));

                //var CGTaskIDs = String.Join(",", FinalList.Select(x => x.CGTASK_ID));
                string filter = "";
                var _ExceptionListentity = await _caregiverTaskEVVExceptions.GetCaregiverTaskEVVExceptionsList(HHA, UserID, FinalScheduleList.ToString(), filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Response.Ok<IEnumerable<ExceptionListDTO>>(ExceptionListDTO);

        }
    }
}

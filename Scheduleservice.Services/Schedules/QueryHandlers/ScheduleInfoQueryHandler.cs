﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleInfoQueryHandler : IHandlerWrapper<ScheduleInfoQuery, ScheduleInfoDto>
    {
        private readonly IMapper _mapper;
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public ScheduleInfoQueryHandler(ICaregivertasksRepository caregivertasksRepository,
                IClientsRepository clientsRepository,
                ICaregiversRepository caregiversRepository,
                IServiceCodesRepository serviceCodesRepository,
                IPaymentsourcesRepository paymentsourcesRepository,
                IMapper mapper,
                IRepositoryFilterConditions repositoryFilterConditions,
                IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            _clientsRepository = clientsRepository;
            _caregiversRepository = caregiversRepository;
            _serviceCodesRepository = serviceCodesRepository;
            _paymentsourcesRepository = paymentsourcesRepository;
            _mapper = mapper;
            _repositoryFilterConditions = repositoryFilterConditions;
            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

        }

        public async Task<Response<ScheduleInfoDto>> Handle(ScheduleInfoQuery request, CancellationToken cancellationToken)
        {
            var Columns = "CGTASK_ID,CAREGIVER,CanBill,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,PLANNED_START_TIME,PLANNED_END_TIME,PLANNED_DATE,EDITED_HOURS,SERVICECODE_ID,PAYMENT_SOURCE,CLIENT_ID,IS_BILLED,IS_PAID,eChartMasterID,oasisDatasetID,pChartMasterID,isEvvschedule," +
                "EvvCheckinLocationVerified,EvvCheckoutLocationVerified,CheckinTreatmentLocation,CheckoutTreatmentLocation,isAttested," +
                "fob_checkin_code,fob_checkout_code,caregivertasks.Agency_UUID";
            
            var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                            .Build();

            var _caregivertasksEntity = await _caregivertasksRepository.GetAllSchedulesList(ActionContext.HHA, ActionContext.User, Columns, Filters);
            //var _caregivertasksEntity = visitinfo.FirstOrDefault();
            var scheduleInfoDto = _mapper.Map<ScheduleInfoDto>(_caregivertasksEntity.FirstOrDefault());

            var _clientsEntity = await _clientsRepository.GetClientsBasicInfo(ActionContext.HHA, ActionContext.User, scheduleInfoDto.CLIENT_ID);
            scheduleInfoDto.CLIENT_NAME = new[] { _clientsEntity.FIRST_NAME, _clientsEntity.LAST_NAME, _clientsEntity.PATIENT_ID }.ClientNameFormat();
            scheduleInfoDto.HHA_Branch_Id = _clientsEntity.HHA_BRANCH_ID;
            scheduleInfoDto.client_group_uuid = _clientsEntity.ClientGroupNo_uuid;

            if (scheduleInfoDto.CAREGIVER > 0)
            {
                var _caregiversEntity = await _caregiversRepository.GetCaregiverBasicInfo(ActionContext.HHA, ActionContext.User, scheduleInfoDto.CAREGIVER);
                scheduleInfoDto.CAREGIVER_NAME = new[] { _caregiversEntity.FIRST_NAME, _caregiversEntity.LAST_NAME, _caregiversEntity.DISCIPLINE_NAME }.ClinicianNameFormat();
            }

            var _serviceCodesEntity = await _serviceCodesRepository.GetServiceCodeBasicInfo(ActionContext.HHA, ActionContext.User, scheduleInfoDto.SERVICECODE_ID);
            scheduleInfoDto.SERVICE_NAME = _serviceCodesEntity.DESCRIPTION;

            var _paymentsourcesEntity = await _paymentsourcesRepository.GetPaymentSourceBasicInfo(ActionContext.HHA, ActionContext.User, scheduleInfoDto.PAYMENT_SOURCE);
            scheduleInfoDto.PAYMENT_SOURCE_NAME = _paymentsourcesEntity.ORG_NAME;


            return Response.Ok<ScheduleInfoDto>(scheduleInfoDto);
        }

    }
}

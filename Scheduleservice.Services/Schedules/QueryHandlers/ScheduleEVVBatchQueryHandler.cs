﻿using AutoMapper;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Entities;
using System;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Schedules;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleEVVBatchQueryHandler : IHandlerWrapper<ScheduleEVVBatchQuery, IEnumerable<ScheduleEVVBatchListDto>>
    {

        private readonly IEVVSchedule _eVVSchedule;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IMapper _mapper;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository;
        private readonly IPaymentSourcesBranchesRepository _paymentSourcesBranchesRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly ILogger<ScheduleEVVBatchQueryHandler> _logger;
        public ScheduleEVVBatchQueryHandler(IEVVSchedule eVVSchedule, IMapper mapper,
            ICaregivertasksRepository caregivertasksRepository, IRepositoryFilterConditions repositoryFilterConditions
            , IClientsRepository clientsRepository, IPaymentSourcesBranchesRepository paymentSourcesBranchesRepository, 
            IPaymentsourcesRepository paymentsourcesRepository, ILogger<ScheduleEVVBatchQueryHandler> logger)
        {

            this._mapper = mapper;
            this._eVVSchedule = eVVSchedule;
            this._caregivertasksRepository = caregivertasksRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._clientsRepository = clientsRepository;
            this._paymentSourcesBranchesRepository = paymentSourcesBranchesRepository;
            this._paymentsourcesRepository = paymentsourcesRepository;
            this._logger = logger;
        }
        public async Task<Response<IEnumerable<ScheduleEVVBatchListDto>>> Handle(ScheduleEVVBatchQuery request, CancellationToken cancellationToken)
        {
            string scheduleFilter = "";
            string clientFilter = "";
            string payerFilter = "";
            string clientIDs = "";
            string payerIDs = "";
            string[] hhaBranchId = new string[] { };
            IEnumerable<ClientsEntity> clientBranchList = new List<ClientsEntity>();
            ScheduleFitlersDto scheduleFitlersDto = new ScheduleFitlersDto();
            IEnumerable<PaymentSourcesBranchesEntity> payerBranchList = new List<PaymentSourcesBranchesEntity>();
            IEnumerable<ScheduleEVVBatchListDto> scheduleEVVBatchListDtos = new List<ScheduleEVVBatchListDto>();

            try
            {

                if ((request.client_id_list == null || request.client_id_list.Length == 0) && request.client_id > 0)
                    request.client_id_list = new int[] { Convert.ToInt32(request.client_id) };

                scheduleFitlersDto = _mapper.Map<ScheduleFitlersDto>(request);

                scheduleFilter = Common.GetScheduleFilters(scheduleFitlersDto);

                var schedulelists = await _caregivertasksRepository.GetAllSchedulesList(request.HHA, request.UserId, "CaregiverTasks.CGTASK_ID,CaregiverTaskAdditional.isEvvschedule,CaregiverTasks.CLIENT_ID,CaregiverTasks.PAYMENT_SOURCE,CaregiverTaskAdditional.isEvvAggregatorExportRequired", scheduleFilter);


                if (!string.IsNullOrEmpty(request.hha_branch_ids) || request.aggregator_id != null)
                {
                    if (!string.IsNullOrEmpty(request.hha_branch_ids))
                    {
                        clientFilter = _repositoryFilterConditions.InFilter("HHA_BRANCH_ID", request.hha_branch_ids, FilterConditionDataTypeEnums.integerType).Build();
                        clientBranchList = await _clientsRepository.GetALLClients(request.HHA, request.UserId, "Clients.CLIENT_ID,Clients.HHA_BRANCH_ID", clientFilter);
                        if(!clientBranchList.Any())
                            return Response.NoContent<IEnumerable<ScheduleEVVBatchListDto>>(scheduleEVVBatchListDtos);
                        hhaBranchId = request.hha_branch_ids.Split(',');
                    }
                    else
                    {
                        if (schedulelists.Any())
                            clientIDs = string.Join(',', schedulelists.Where(x => x.isEvvschedule == true).Select(x => x.CLIENT_ID).Distinct());

                        if (!string.IsNullOrEmpty(clientIDs))
                        {
                            clientFilter = _repositoryFilterConditions.InFilter("Clients.CLIENT_ID", clientIDs, FilterConditionDataTypeEnums.integerType).Build();
                            clientBranchList = await _clientsRepository.GetALLClients(request.HHA, request.UserId, "Clients.CLIENT_ID,Clients.HHA_BRANCH_ID", clientFilter);

                            if (clientBranchList.Any())
                                hhaBranchId = clientBranchList.Select(x => x.HHA_BRANCH_ID.ToString()).Distinct().ToArray();
                        }
                    }
                }

                if (hhaBranchId.Length > 0 && clientBranchList.Any())
                {
                    var clientReqBranchList = clientBranchList.Where(x => hhaBranchId.Contains(x.HHA_BRANCH_ID.ToString())).ToList();

                    if (clientReqBranchList.Any() && schedulelists.Any())
                        schedulelists = schedulelists.Where(x => clientReqBranchList.Any(y => y.CLIENT_ID == x.CLIENT_ID));
                }

                if (schedulelists.Any() && request.aggregator_id != null)
                {
                    List<CaregivertasksEntity> schedulesWithOutPayerBranchEvvEnabled = new List<CaregivertasksEntity>();
                    List<CaregivertasksEntity> schedulesWithPayerBranchEvvEnabled = new List<CaregivertasksEntity>();

                    payerIDs = string.Join(',', schedulelists.Where(x => x.isEvvschedule == true).Select(x => x.PAYMENT_SOURCE.ToString()).Distinct());
                    payerFilter = _repositoryFilterConditions.InFilter("PaymentSource_ID", payerIDs, FilterConditionDataTypeEnums.integerType)
                        .InFilter("Branch_ID", string.Join(",", hhaBranchId), FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("EnableEvv", true, FilterConditionDataTypeEnums.boolType, true)
                        .Build();
                    payerBranchList = await _paymentSourcesBranchesRepository.GetAllPayerBranchesInfo(request.HHA, request.UserId, "PaymentSource_ID,Branch_ID,HHA,EvvAggregatorVendorVersionMasterID", payerFilter);

                    if (payerBranchList.Any())
                    {
                        var payerclientschedulelist = from payer in payerBranchList
                                                      join client in clientBranchList on payer.Branch_ID equals client.HHA_BRANCH_ID
                                                      join sch in schedulelists on client.CLIENT_ID equals sch.CLIENT_ID
                                                      select new
                                                      {
                                                          payerBranchList = payer,
                                                          clientBranchList = client,
                                                          schedulelists = sch
                                                      };

                        if (payerclientschedulelist.Any())
                        {
                            payerclientschedulelist.ToList().ForEach(x => x.schedulelists.EvvVendorVersionMasterID = x.payerBranchList.EvvAggregatorVendorVersionMasterID);

                            schedulesWithPayerBranchEvvEnabled = payerclientschedulelist.Select(x => x.schedulelists).ToList();

                            schedulesWithOutPayerBranchEvvEnabled = schedulelists.ToList();

                            if (schedulesWithPayerBranchEvvEnabled.Any())
                            {

                                foreach (var schedules in schedulesWithPayerBranchEvvEnabled)
                                {
                                    schedulesWithOutPayerBranchEvvEnabled.RemoveAll(x => x.CGTASK_ID == schedules.CGTASK_ID);
                                }

                                schedulesWithPayerBranchEvvEnabled = schedulesWithPayerBranchEvvEnabled.Where(x => x.EvvVendorVersionMasterID == request.aggregator_id).ToList();
                            }
                        }
                    }
                    else
                        schedulesWithOutPayerBranchEvvEnabled = schedulelists.ToList();

                    if (schedulesWithOutPayerBranchEvvEnabled.Any())
                    {
                        payerIDs = string.Join(',', schedulesWithOutPayerBranchEvvEnabled.Select(x => x.PAYMENT_SOURCE.ToString()).Distinct());
                        payerFilter = _repositoryFilterConditions.InFilter("PaymentSources.PAYMENT_SOURCE_ID", payerIDs, FilterConditionDataTypeEnums.integerType)
                            .EqualFilter("PaymentSourcesAdditional.IsEnableEVV", true, FilterConditionDataTypeEnums.boolType)
                            .Build();

                        var paymentSourceInfoList = await _paymentsourcesRepository.GetPaymentsources(request.HHA, request.UserId, "PaymentSources.PAYMENT_SOURCE_ID,PaymentSourcesAdditional2.EvvAggregatorVendorVersionMasterID", payerFilter);

                        if (paymentSourceInfoList.Any())
                        {
                            var payerschedulelist = from payer in paymentSourceInfoList
                                                    join sch in schedulesWithOutPayerBranchEvvEnabled on payer.PAYMENT_SOURCE_ID equals sch.PAYMENT_SOURCE
                                                    select new
                                                    {
                                                        payer = payer,
                                                        schedulelists = sch
                                                    };

                            if (payerschedulelist.Any())
                            {
                                payerschedulelist.ToList().ForEach(x => x.schedulelists.EvvVendorVersionMasterID = x.payer.EvvAggregatorVendorVersionMasterID ?? 0);
                                schedulesWithOutPayerBranchEvvEnabled = payerschedulelist.Select(x => x.schedulelists).ToList();
                                schedulesWithOutPayerBranchEvvEnabled = schedulesWithOutPayerBranchEvvEnabled.Where(x => x.EvvVendorVersionMasterID == request.aggregator_id).ToList();
                            }
                        }
                    }

                    if (schedulesWithPayerBranchEvvEnabled.Any() || schedulesWithOutPayerBranchEvvEnabled.Any())
                    {
                        if (schedulesWithPayerBranchEvvEnabled.Any() && schedulesWithOutPayerBranchEvvEnabled.Any())
                        {
                            schedulelists = schedulesWithPayerBranchEvvEnabled.UnionBy(schedulesWithOutPayerBranchEvvEnabled, x => x.CGTASK_ID).ToList();
                        }
                        else if (schedulesWithPayerBranchEvvEnabled.Any())
                        {
                            schedulelists = schedulesWithPayerBranchEvvEnabled.Where(x => x.EvvVendorVersionMasterID == request.aggregator_id);
                        }
                        else
                        {
                            schedulelists = schedulesWithOutPayerBranchEvvEnabled.Where(x => x.EvvVendorVersionMasterID == request.aggregator_id);
                        }
                    }
                    else
                        schedulelists = null;
                }

                if (schedulelists != null && schedulelists.Any())
                {
                    var scheduleCGTaskIDDto = _mapper.Map<IEnumerable<ScheduleEVVBatchDto>>(schedulelists);
                    scheduleEVVBatchListDtos = await _eVVSchedule.InsertCaregiverTaskEvvSchedules(request.HHA, request.UserId, scheduleCGTaskIDDto);
                }
                else
                    return Response.NoContent<IEnumerable<ScheduleEVVBatchListDto>>(scheduleEVVBatchListDtos);

                return Response.Ok<IEnumerable<ScheduleEVVBatchListDto>>(scheduleEVVBatchListDtos);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Class: ScheduleEVVBatchQueryHandler.cs; Method: Handle; Message: {ex.Message}");
                return Response.ValidationError<IEnumerable<ScheduleEVVBatchListDto>>(ex.Message, null);
            }
        }
    }
}

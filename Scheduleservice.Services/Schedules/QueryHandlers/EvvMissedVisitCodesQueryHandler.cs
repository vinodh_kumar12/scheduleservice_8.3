﻿using AutoMapper;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Common.Extensions;
using System.Collections.Generic;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using System.Linq;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class EvvMissedVisitCodesQueryHandler : IHandlerWrapper<EvvMissedVisitCodesQuery, EvvMissedVisitCodesDto>
    {
        private readonly IMapper _mapper;
        private readonly ICaregiverTaskEvvMissedVisitsReasonsRepository _caregivermissedvisitreason;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ActionContextDto _actionContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICaregivertaskMissedVisitsRepository _caregivertaskMissedVisitsRepository;



        public EvvMissedVisitCodesQueryHandler( 
            ICaregiverTaskEvvMissedVisitsReasonsRepository caregivermissedvisitreason,
            IMapper mapper,
            IRepositoryFilterConditions RepositoryFilterConditions, 
            IHttpContextAccessor httpContextAccessor,
            ICaregivertaskMissedVisitsRepository caregivertaskMissedVisitsRepository)
        {
            _mapper = mapper;
            _caregivermissedvisitreason = caregivermissedvisitreason;
            _repositoryFilterConditions = RepositoryFilterConditions;
            _httpContextAccessor = httpContextAccessor;
            _caregivertaskMissedVisitsRepository= caregivertaskMissedVisitsRepository;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<EvvMissedVisitCodesDto>> Handle(EvvMissedVisitCodesQuery request,CancellationToken cancellationToken)
        {
            try
            {
                EvvMissedVisitCodesDto return_dto = new EvvMissedVisitCodesDto();
                 
                var filters = _repositoryFilterConditions.
                    EqualFilter("CaregiverTaskEvvMissedVisitsReasons.CGTASKID", request.CGTask_ID, integerType).Build();


                var columns = "CaregiverTaskEvvMissedVisitsReasons.CaregiverTaskEvvskMissedvisitsReasonID," +
                                "CaregiverTaskEvvMissedVisitsReasons.ReasonCode," +
                                "CaregiverTaskEvvMissedVisitsReasons.ReasonCodeDescription," +
                                "CaregiverTaskEvvMissedVisitsReasons.ActionCode," +
                                "CaregiverTaskEvvMissedVisitsReasons.ActionCodeDescription," +
                                "CaregiverTaskEvvMissedVisitsReasons.ActionCodeComments," +
                                "CaregiverTaskEvvMissedVisitsReasons.Notes";

                var _out_list = await _caregivermissedvisitreason.GetMissedVisitReasonCodes(_actionContext.HHA, _actionContext.User, columns, filters);

                if (!_out_list.Any())
                    return Response.NoContent<EvvMissedVisitCodesDto>(null);

                return_dto= _mapper.Map<EvvMissedVisitCodesDto>(_out_list.FirstOrDefault());
                return Response.Ok(return_dto);
            }
            catch(Exception ex)
            {
                return Response.Fail<EvvMissedVisitCodesDto>(ResponseErrorCodes.InternalServerFailed, "exception");
            }

        }



    }
}

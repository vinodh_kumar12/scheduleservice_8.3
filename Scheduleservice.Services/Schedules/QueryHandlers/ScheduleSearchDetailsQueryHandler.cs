﻿using AutoMapper;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleSearchDetailsQueryHandler : IHandlerWrapper<ScheduleSearchDetailsQuery, ScheduleSearchDetailsDto>
    {

        private readonly ISchedule _scheduleService;
        private readonly IMapper _mapper;
        public ScheduleSearchDetailsQueryHandler(ISchedule scheduleService, IMapper mapper)
        {
            _scheduleService = scheduleService;
            _mapper = mapper;
        }

        public async Task<Response<ScheduleSearchDetailsDto>> Handle(ScheduleSearchDetailsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                ScheduleSearchDetailsDto scheduleSearchDetailsDto = new ScheduleSearchDetailsDto();

                var _schedulesListjson = await _scheduleService.GetSchedulesAllList(request.HHA, request.UserId, request.search_conditionsJSON, request.output_fields);

                scheduleSearchDetailsDto.ScheduleSearchDetails_Json = _schedulesListjson;

                return Response.Ok<ScheduleSearchDetailsDto>(scheduleSearchDetailsDto);

            }
            catch (Exception ex)
            {
                return Response.Fail<ScheduleSearchDetailsDto>(ResponseErrorCodes.InternalServerFailed, "exception");
            }
        }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleSearchValidationConflictexceptionsQueryHandler : IHandlerWrapper<ScheduleSearchValidationConflictexceptionsQuery, ValidationConflictexceptionsDto>
    {
        private readonly IValidationConflictExceptions _validationConflictExceptions;
        public ScheduleSearchValidationConflictexceptionsQueryHandler(IValidationConflictExceptions validationConflictExceptions)
        {
            _validationConflictExceptions = validationConflictExceptions;
        }
        public async Task<Response<ValidationConflictexceptionsDto>> Handle(ScheduleSearchValidationConflictexceptionsQuery request, CancellationToken cancellationToken)
        {
            ValidationConflictexceptionsDto validationConflictsDTO = new ValidationConflictexceptionsDto();

            validationConflictsDTO = await _validationConflictExceptions.GetValidationConflictexceptions(request.HHA, request.UserId, request.Filters);
            return Response.Ok<ValidationConflictexceptionsDto>(validationConflictsDTO); 
        }
    }
}

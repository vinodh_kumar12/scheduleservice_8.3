﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Schedule;
using AutoMapper;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using System.Linq.Dynamic.Core;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Schedules;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleSearchQueryHandler : IHandlerWrapper<ScheduleSearchQuery, ScheduleSearchInfoDto>
    {

        private readonly ISchedule _scheduleService;
        private readonly IMapper _mapper;
        private readonly ILogger<ScheduleSearchQueryHandler> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContextDto;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public ScheduleSearchQueryHandler(ISchedule scheduleService, IMapper mapper, ILogger<ScheduleSearchQueryHandler> logger,
            IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
            IRepositoryFilterConditions repositoryFilterConditions)
        {
            _scheduleService = scheduleService;
            _mapper = mapper;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;

            _actionContextDto = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<ScheduleSearchInfoDto>> Handle(ScheduleSearchQuery request, CancellationToken cancellationToken)
        {

            try
            {
                string schedulesFilter = "";
                string childschedulesFilter = "";
                PagedList<Dictionary<string,Object>> _schedulesPageList = null;
                ScheduleSearchInfoDto scheduleSearchInfoDto = new ScheduleSearchInfoDto();
                ScheduleFitlersDto scheduleFitlersDto = new ScheduleFitlersDto();

                IList<Dictionary<string, object>> _schedulesListDto = new List<Dictionary<string, object>>();
                IList<Dictionary<string, object>> _childschedulesListDto = new List<Dictionary<string, object>>();

                if (request.output_fields == null || request.output_fields.Length == 0)
                {
                    request.output_fields = new string[] { "CGTASK_ID" };
                }
                else if (!request.output_fields.Any(x => x.Equals("CGTASK_ID")))
                {
                    request.output_fields = request.output_fields.Append("CGTASK_ID");
                }

                scheduleFitlersDto = _mapper.Map<ScheduleFitlersDto>(request);

                schedulesFilter = Common.GetScheduleFilters(scheduleFitlersDto);

                var columns = String.Join(",", request.output_fields);
                var schedules = await _caregivertasksRepository.GetAllSchedulesList(_actionContextDto.HHA, _actionContextDto.User, columns, schedulesFilter);

                if(schedules!=null && schedules.Any() && request.sort_by!=null && request.sort_by.Count()>0)
                {
                    var sortlist = request.sort_by.Select(x => new { x.sort_field, x.is_ascending_filed, sort_type = x.is_ascending_filed == true ? "ASC" : "DESC" });

                    string sortfields = string.Join(',',sortlist.Select(x=>x.sort_field+" "+x.sort_type));

                    schedules = schedules.AsQueryable().OrderBy(sortfields).ToList();
                }

                var schedules_Output_columns_List = schedules.Select(a => a.DynamicSelectGenerator(request.output_fields));

                string _schedulesListjson = JsonConvert.SerializeObject(schedules_Output_columns_List);

                _schedulesListDto = JsonConvert.DeserializeObject<IList<Dictionary<string, object>>>(_schedulesListjson);

                foreach (var sch in _schedulesListDto)
                {
                    sch.Add("Child_Schedule_Ids", null);
                }

                if (request.include_split_schedules != null && request.include_split_schedules == true)
                {

                    if (request.client_id != null && request.client_id > 0)
                    {
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.CLIENT", request.client_id, FilterConditionDataTypeEnums.integerType).Build();
                    }

                    if (!string.IsNullOrEmpty(request.start_date))
                    {
                        if (!string.IsNullOrEmpty(request.end_date))
                            childschedulesFilter += _repositoryFilterConditions.BetweenFilter("SCHEDULE_DATE", request.start_date, request.end_date, FilterConditionDataTypeEnums.datetimeType).Build();
                        else
                            childschedulesFilter += _repositoryFilterConditions.GreaterThanEqualFilter("SCHEDULE_DATE", request.start_date, FilterConditionDataTypeEnums.datetimeType).Build();
                    }

                    if (!string.IsNullOrEmpty(request.end_date))
                    {
                        if (string.IsNullOrEmpty(request.start_date))
                            childschedulesFilter += _repositoryFilterConditions.LessThanEqualFilter("SCHEDULE_DATE", request.end_date, FilterConditionDataTypeEnums.datetimeType).Build();
                    }

                    if (request.is_evv_export_dirty != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.isEvvScheduleDirty", request.is_evv_export_dirty, FilterConditionDataTypeEnums.boolType).Build();

                    if (request.is_billable != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.CanBill", request.is_billable, FilterConditionDataTypeEnums.integerType).Build();

                    if (request.is_invoiced != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.IS_BILLED", request.is_invoiced, FilterConditionDataTypeEnums.boolType).Build();

                    if (request.is_payrolled != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.IsPayrolled", request.is_payrolled, FilterConditionDataTypeEnums.boolType).Build();

                    if (!string.IsNullOrEmpty(request.schedule_status))
                        childschedulesFilter += _repositoryFilterConditions.InFilter("CaregiverTasks.[Status]", request.schedule_status, FilterConditionDataTypeEnums.stringType).Build();

                    if (request.is_evv_aggregator_export_required != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("isEvvAggregatorExportRequired", request.is_evv_aggregator_export_required, FilterConditionDataTypeEnums.boolType).Build();

                    if (!string.IsNullOrEmpty(request.payer_ids))
                        childschedulesFilter += _repositoryFilterConditions.InFilter("CaregiverTaskChildSchedules.PAYMENT_SOURCE", request.payer_ids, FilterConditionDataTypeEnums.integerType).Build();

                    if (request.split_for_billing_parent != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("IsSplitForBilling", request.split_for_billing_parent, FilterConditionDataTypeEnums.boolType, true).Build();

                    if (request.is_evv_schedule != null)
                        childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.isEvvschedule", request.is_evv_schedule, FilterConditionDataTypeEnums.boolType, true).Build();

                    childschedulesFilter += _repositoryFilterConditions.EqualFilter("CaregiverTaskChildSchedules.isDeleted", false, FilterConditionDataTypeEnums.boolType, true).Build();

                    var child_schedules = await _caregivertasksRepository.GetAllChildSchedulesList(_actionContextDto.HHA, _actionContextDto.User, "PARENT_CGTASK_ID,Child_Schedule_Id", childschedulesFilter);

                    var child_schedules_Output_columns_List = child_schedules.Select(a => a.DynamicSelectGenerator(new[] { "PARENT_CGTASK_ID", "Child_Schedule_Id" }));

                    string child_schedule_list_json = JsonConvert.SerializeObject(child_schedules_Output_columns_List);

                    var scheduleListDto_forChild = from schedule in JsonConvert.DeserializeObject<IEnumerable<CaregiverTaskChildSchedulesEntity>>(child_schedule_list_json)
                                                   group schedule by schedule.PARENT_CGTASK_ID into groupSchedule
                                                   select new SchedulesListDto
                                                   {
                                                       CGTASK_ID = groupSchedule.Key,
                                                       Child_Schedule_Ids = groupSchedule.Select(x => x.Child_Schedule_Id).ToList()
                                                   };

                    if (scheduleListDto_forChild != null && scheduleListDto_forChild.ToList().Count > 0)
                    {
                        child_schedules_Output_columns_List = scheduleListDto_forChild.Select(a => a.DynamicSelectGenerator(new[] { "CGTASK_ID", "Child_Schedule_Ids" }));

                        string childScheduleDetails = JsonConvert.SerializeObject(child_schedules_Output_columns_List);

                        _childschedulesListDto = JsonConvert.DeserializeObject<IList<Dictionary<string, object>>>(childScheduleDetails);

                        if (_childschedulesListDto != null && _childschedulesListDto.Any() && _childschedulesListDto.Count > 0)
                        {
                            foreach (var childsch in _childschedulesListDto)
                            {
                                foreach (var sch in _schedulesListDto)
                                {
                                    object ParentCGTaskID = null;
                                    object ScheduleCGTaskID = null;
                                    object childscheduleids = null;

                                    sch.TryGetValue("CGTASK_ID", out ScheduleCGTaskID);
                                    childsch.TryGetValue("CGTASK_ID", out ParentCGTaskID);
                                    childsch.TryGetValue("Child_Schedule_Ids", out childscheduleids);

                                    if (object.Equals(ScheduleCGTaskID, ParentCGTaskID))
                                    {
                                        sch["Child_Schedule_Ids"] = childscheduleids;
                                    }
                                }
                            }
                        }
                    }
                }

                if (request.page_no == 0 && _schedulesListDto.Count() > 0)
                {
                    _schedulesPageList = PagedList<Dictionary<string,Object>>.ToPagedList(_schedulesListDto.AsQueryable(), request.page_no, _schedulesListDto.Count());
                }
                else if (request.page_no >= 1 && _schedulesListDto.Count() > 0)
                {
                    _schedulesPageList = PagedList<Dictionary<string, Object>>.ToPagedList(_schedulesListDto.AsQueryable(), request.page_no, 100);
                }

                if (_schedulesPageList != null && _schedulesPageList.total_count > 0)
                {
                    scheduleSearchInfoDto.visit_details = _schedulesPageList;
                    scheduleSearchInfoDto.total_records = _schedulesPageList.total_count;
                    scheduleSearchInfoDto.current_page_no = _schedulesPageList.current_page;
                    scheduleSearchInfoDto.number_records_in_this_page = _schedulesPageList.number_records_in_this_page;
                    scheduleSearchInfoDto.total_pages = _schedulesPageList.total_pages;
                    scheduleSearchInfoDto.page_size = _schedulesPageList.page_size;
                }
                else
                {
                    scheduleSearchInfoDto.visit_details = null;
                    scheduleSearchInfoDto.total_records = 0;
                    scheduleSearchInfoDto.current_page_no = request.page_no;
                    scheduleSearchInfoDto.number_records_in_this_page = 0;
                    scheduleSearchInfoDto.total_pages = 0;
                    scheduleSearchInfoDto.page_size = 0;
                }

                return Response.Ok<ScheduleSearchInfoDto>(scheduleSearchInfoDto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Class: ScheduleSearchQueryHandler.cs; Method: Handle; Message: {ex.Message}");
                return Response.ValidationError<ScheduleSearchInfoDto>(ex.Message,null );
            }

        }
    }
}
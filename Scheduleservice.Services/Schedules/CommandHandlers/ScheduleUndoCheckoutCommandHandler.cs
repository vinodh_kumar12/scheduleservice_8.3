﻿using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class ScheduleUndoCheckoutCommandHandler : IHandlerWrapper<ScheduleUndoCheckoutCommand, IEnumerable<string>>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        public ScheduleUndoCheckoutCommandHandler(IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<IEnumerable<string>>> Handle(ScheduleUndoCheckoutCommand request, CancellationToken cancellationToken)
        {
            var caregivertask = await _caregivertasksRepository.GetScheduleBasicInfo(ActionContext.HHA, ActionContext.User, request.CGTask_ID);
            IEnumerable<string> ret = new List<string>();
            return Response.Ok<IEnumerable<string>>(ret);
        }
    }
}

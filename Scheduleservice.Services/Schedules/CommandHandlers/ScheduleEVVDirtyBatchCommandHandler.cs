﻿using AutoMapper;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class ScheduleEVVDirtyBatchCommandHandler : IHandlerWrapper<ScheduleEVVDirtyBatchCommand, bool>
    {
        IMapper _mapper;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly ICaregiverTaskEvvSchedulesRepository _caregiverTaskEvvSchedulesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ILogger<ScheduleEVVDirtyBatchCommandHandler> _logger;

        public ScheduleEVVDirtyBatchCommandHandler(IMapper mapper, ICaregivertasksRepository caregivertasksRepository,
            ICaregiverTaskEvvSchedulesRepository caregiverTaskEvvSchedulesRepository, IRepositoryFilterConditions repositoryFilterConditions,
            ILogger<ScheduleEVVDirtyBatchCommandHandler> logger)
        {

            this._mapper = mapper;
            this._caregivertasksRepository = caregivertasksRepository;
            this._caregiverTaskEvvSchedulesRepository = caregiverTaskEvvSchedulesRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._logger = logger;
        }

        public async Task<Response<bool>> Handle(ScheduleEVVDirtyBatchCommand request, CancellationToken cancellationToken)
        {
            int result = 0;
            try
            {
                //Get  CaregiverTask Evv Schedules batches
                var CaregiverTaskEvvSchedules = await this._caregiverTaskEvvSchedulesRepository.GetCaregiverTaskEvvSchedules(request.HHA, request.UserId, request.ScheduleEVVBatchID);

                if (CaregiverTaskEvvSchedules.Any())
                {
                    var EVVSchedules = CaregiverTaskEvvSchedules.Where(x => x.IsEvvSchedule == true && x.isEvvAggregatorExportRequired == true);

                    if (EVVSchedules.Any())
                    {
                        string CGTASKIDs = string.Join(",", EVVSchedules.Select(x => x.cgtaskid).Distinct());

                        var scheduleallUpdateColumn = new RepositoryUpdateColumn();

                        var updatedCaregivertasksAdditionalColumns = scheduleallUpdateColumn.UpdateColumn("isEVVDirty", request.set.ToString(), FilterConditionDataTypeEnums.boolType).Build();

                        result = await _caregivertasksRepository.UpdateScheduleAdditional2(request.HHA, request.UserId, CGTASKIDs, updatedCaregivertasksAdditionalColumns);
                    }

                    if (result == 0)
                    {
                        var DeleteCaregiverTaskEvvSchedules = await this._caregiverTaskEvvSchedulesRepository.DeleteCaregiverTaskEvvSchedules(request.HHA, request.UserId, request.ScheduleEVVBatchID);
                        if (DeleteCaregiverTaskEvvSchedules != 0)
                            return Response.ValidationError<bool>("Fail to update, Please contact kantime support!", false);
                    }
                    else
                        return Response.ValidationError<bool>("Fail to update, Please contact kantime support!", false);
                }
                else
                    return Response.NoContent(false);

                return Response.Ok(true);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Class: ScheduleEVVDirtyBatchCommandHandler.cs; Method: Handle; Message: {ex.Message}");
                return Response.ValidationError<bool>("Fail to update, Please contact kantime support!", false);
            }
        }
    }
}

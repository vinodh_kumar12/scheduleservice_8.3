﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class RecalulateScheduleExceptionsBatchCommandHandler : IHandlerWrapper<RecalulateScheduleExceptionsBatchCommand, RecalculateScheduleExceptionsBatchDto>
    {
        private readonly IValidationConflictExceptions _validationConflictExceptions;

        public RecalulateScheduleExceptionsBatchCommandHandler(IValidationConflictExceptions validationConflictExceptions)
        {
            _validationConflictExceptions = validationConflictExceptions;
        }
        public async Task<Response<RecalculateScheduleExceptionsBatchDto>> Handle(RecalulateScheduleExceptionsBatchCommand request, CancellationToken cancellationToken)
        {
            RecalculateScheduleExceptionsBatchDto recalculateScheduleExceptionsBatchDto = new RecalculateScheduleExceptionsBatchDto();

            recalculateScheduleExceptionsBatchDto = await _validationConflictExceptions.GetValidationConflictRecalculate_Batch(request.HHA, request.UserId, request.batch_id);
            return Response.Ok<RecalculateScheduleExceptionsBatchDto>(recalculateScheduleExceptionsBatchDto);
        }
    }
}

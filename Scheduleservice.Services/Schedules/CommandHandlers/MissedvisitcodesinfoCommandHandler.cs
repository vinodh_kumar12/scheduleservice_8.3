﻿namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Request;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.EVV;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.Models.Authorization;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Services.Schedules.Command;
    using System;
    using System.Collections.Generic; 
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class MissedVisitCodeCommandHandler : IHandlerWrapper<MissedvisitcodesinfoCommand,bool>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly ICaregivertaskMissedVisitsRepository _caregivertaskMissedVisitsRepository;
        public readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public readonly ICaregiverTaskEvvMissedVisitsReasonsRepository _caregivertaskMissedVisitsreasonsRpository;
        private readonly IEvvScheduleEditCodesMasterRepository _evvScheduleEditCodesMasterRepository;
        private readonly IMapper _mapper;
        public MissedVisitCodeCommandHandler(IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
                   ICaregivertaskMissedVisitsRepository CaregivertaskMissedVisitsRepository,
                   IRepositoryFilterConditions repositoryFilterConditions,
                   ICaregiverTaskEvvMissedVisitsReasonsRepository caregivertaskMissedVisitsreasonsRpository,
                   IEvvScheduleEditCodesMasterRepository evvScheduleEditCodesMaster,
                   IMapper mapper)
        {
            _httpContextAccessor = httpContextAccessor;
            this._caregivertaskMissedVisitsRepository = CaregivertaskMissedVisitsRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregivertaskMissedVisitsreasonsRpository = caregivertaskMissedVisitsreasonsRpository;
            this._evvScheduleEditCodesMasterRepository = evvScheduleEditCodesMaster;
            this._mapper = mapper;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<bool>> Handle(MissedvisitcodesinfoCommand request, CancellationToken cancellationToken)
        {
            bool ret = false;
            try
            {
               
                var Filters = _repositoryFilterConditions.EqualFilter("cgTaskid", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var columns = "CaregivertaskMissedVisits.CaregiverTaskMissedVisitID";
                                       
                var _caregivermissedvisitinfo = await _caregivertaskMissedVisitsRepository.GetMissedVisitInfo(ActionContext.HHA, ActionContext.User, columns, Filters);

                int caregivermissedvisitID = _caregivermissedvisitinfo.Select(x => x.CaregiverTaskMissedVisitID).FirstOrDefault();

                var missedVisitCodesDto = _mapper.Map<MissedVisitCodesDto>(request);
                //get the EVV Codes for the Schedule Vendor Version Master ID
                var EVVMissedVisitCodeList = await _evvScheduleEditCodesMasterRepository.GetHHAEVVCodes(ActionContext.HHA, ActionContext.User, "MissedVisitReasonCode", request.aggregator_id);
                 
                if (EVVMissedVisitCodeList.Any())
                {
                    var masterpayercodes = EVVMissedVisitCodeList.Where(x => x.Code == request.reason_code).FirstOrDefault().MasterPayerCodes;
                    missedVisitCodesDto.MasterPayerCodes = masterpayercodes;
                } 

                string MissedvisitcodeInfo = JsonConvert.SerializeObject(missedVisitCodesDto);                

                int _outMissedvisit_list = await _caregivertaskMissedVisitsreasonsRpository.AddMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, request.CGTask_ID, caregivermissedvisitID, MissedvisitcodeInfo);

                if (_outMissedvisit_list ==  1)
                {
                    return Response.Fail<bool>("No_Content", "There are no Records Exists", System.Net.HttpStatusCode.NoContent);

                }
                else if(_outMissedvisit_list == 100)
                {
                    return Response.Fail<bool>(ResponseErrorCodes.ValidationFailed, "Reason Code already exists");
                }
                else
                {
                    return Response.Ok<bool>(true);

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
            
        }

    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Entities;
using System.Net;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class RegisterEvvTerminalPayerSchedulesCommandHandler: IHandlerWrapper<RegisterEvvTerminalPayerSchedulesCommand, RegisterEvvTerminalPayerSchedulesDto>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ActionContextDto _actionContext;
        private readonly ILogger<RegisterEvvTerminalPayerSchedulesCommandHandler> _logger;
        private readonly IClientEvvTerminalPayersRepository _clientEvvTerminalPayersRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IEVVSchedule _eVVSchedule;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IClientsRepository _clientsRepository;
        public RegisterEvvTerminalPayerSchedulesCommandHandler(
            IHttpContextAccessor httpContextAccessor,
            ILogger<RegisterEvvTerminalPayerSchedulesCommandHandler> logger,
            IClientEvvTerminalPayersRepository clientEvvTerminalPayersRepository,
            IRepositoryFilterConditions repositoryFilterConditions,
            IEVVSchedule eVVSchedule,
            ICaregivertasksRepository caregivertasksRepository,
            IClientsRepository clientsRepository
            )
        {
            this._httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

            this._logger = logger;
            this._clientEvvTerminalPayersRepository = clientEvvTerminalPayersRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._eVVSchedule = eVVSchedule;
            this._caregivertasksRepository = caregivertasksRepository;
            this._clientsRepository = clientsRepository;
        }

        public async Task<Response<RegisterEvvTerminalPayerSchedulesDto>> Handle(RegisterEvvTerminalPayerSchedulesCommand request, CancellationToken cancellationToken)
        {
            RegisterEvvTerminalPayerSchedulesDto registerEvvTerminalPayerSchedules = new RegisterEvvTerminalPayerSchedulesDto();
            var Columns = "";
            var Filters = "";

            try
            {
                if (request.terminalPayerSchedules.Any() && request.terminalPayerSchedules != default)
                {
                    //Get Kantime UniqueID for parent schedules
                    IEnumerable<CaregivertasksEntity> parentScheduleKTIDs = new List<CaregivertasksEntity>();
                    var CgtaskIDs = string.Join(',', request.terminalPayerSchedules.Where(x => x.child_schedule_id == 0).Select(x => x.cgtask_id).ToList());
                    if(CgtaskIDs != "")
                    {
                        Columns = "CaregiverTaskAdditional.EvvMyUniqueID," +
                            "CaregiverTasks.CGTASK_ID," +
                            "CaregiverTasks.PLANNED_DATE";
                        Filters = _repositoryFilterConditions.InFilter("CaregiverTasks.CGTASK_ID", CgtaskIDs, integerType).Build();
                        parentScheduleKTIDs = await _caregivertasksRepository.GetAllSchedulesList(_actionContext.HHA, _actionContext.User, Columns, Filters);

                        if (!parentScheduleKTIDs.Any())
                        {
                            return Response.ValidationError<RegisterEvvTerminalPayerSchedulesDto>("Parent schedule(s) unique id(s) not found");
                        }
                    }

                    //Get Kantime UniqueID for child schedules
                    IEnumerable<CaregiverTaskChildSchedulesEntity> ChildScheduleKTIDs = new List<CaregiverTaskChildSchedulesEntity>();
                    var ChildScheduleIDs = string.Join(',', request.terminalPayerSchedules.Where(x => x.child_schedule_id > 0).Select(x => x.child_schedule_id).ToList());
                    if(ChildScheduleIDs != "")
                    {
                        Columns = "CaregiverTaskChildSchedules.Child_Schedule_Id," +
                            "CaregiverTaskChildSchedules.childScheduleUniqueIDKT," +
                            "CaregiverTaskChildSchedules.SCHEDULE_DATE";
                        Filters = _repositoryFilterConditions.InFilter("CaregiverTaskChildSchedules.Child_Schedule_Id", ChildScheduleIDs, integerType).Build();
                        ChildScheduleKTIDs = await _caregivertasksRepository.GetChildSchedulesList(_actionContext.HHA, _actionContext.User, Columns, Filters);
                        
                        if (!ChildScheduleKTIDs.Any())
                        {
                            return Response.ValidationError<RegisterEvvTerminalPayerSchedulesDto>("Child schedule(s) unique id(s) not found");
                        }
                    }

                    //Get ClientEvvTerminalPayers info
                    var clientPayerIDs = string.Join(',', request.terminalPayerSchedules.Select(x => x.primary_client_payer_id).Distinct().ToList());
                    var clientTerminalPayerIDs = string.Join(',', request.terminalPayerSchedules.Select(x => x.terminal_client_payer_id).Distinct().ToList());
                    Columns = "ClientEVVTerminalPayers.client_id," +
                        "ClientEVVTerminalPayers.Primary_client_payer_id," +
                        "ClientEVVTerminalPayers.hierarchy," +
                        "ClientEVVTerminalPayers.Terminal_client_payer_id," +
                        //"ClientEVVTerminalPayers.terminal_aggregator_id," +
                        "ClientEVVTerminalPayers.export_effective_from,"+
                        "ClientEVVTerminalPayers.evv_configuration_id";
                    Filters = _repositoryFilterConditions.InFilter("ClientEVVTerminalPayers.Primary_client_payer_id", clientPayerIDs, integerType).Build();
                    var clientTerminalPayers = await _clientEvvTerminalPayersRepository.GetClientTerminalPayersList(_actionContext.HHA, _actionContext.User, Columns, Filters);
                    if (!clientTerminalPayers.Any())
                    {
                        return Response.ValidationError<RegisterEvvTerminalPayerSchedulesDto>("Client Terminal Payer(s) not found");
                    }

                    //Get sec/ter payer effective periods
                    var terminalPayerIDs = string.Join(',',
                        clientTerminalPayers.Where(x => x.hierarchy == 'S' || x.hierarchy == 'T')
                            .Select(x => x.Terminal_client_payer_id).Distinct().ToList()
                    );
                    Columns = "ClientPayerEffectivePeriods.PayerEffectivePeriodID," +
                    "ClientPayerEffectivePeriods.Client_Additional_Payer_ID," +
                    "ClientPayerEffectivePeriods.EffectiveStart," +
                    "ClientPayerEffectivePeriods.EffectiveEnd";
                    Filters = _repositoryFilterConditions.InFilter("ClientPayerEffectivePeriods.Client_Additional_Payer_ID", terminalPayerIDs, integerType).Build();
                    var terminalPayerEffectivePeriods = await _clientsRepository.GetClientPayerEffectivePeriods(_actionContext.HHA, _actionContext.User, Columns, Filters);

                    //Update effective dates
                    var effectiveClientTerminalPayers =
                    from cPayers in clientTerminalPayers
                    join tPayers in terminalPayerEffectivePeriods on cPayers.Terminal_client_payer_id equals tPayers.Client_Additional_Payer_ID into terminalEffectivePeriods
                    from tEffectivePayers in terminalEffectivePeriods.DefaultIfEmpty()
                    select new
                    {
                        client_id = cPayers.client_id,
                        Primary_client_payer_id = cPayers.Primary_client_payer_id,
                        Terminal_client_payer_id = cPayers.Terminal_client_payer_id,
                       // terminal_aggregator_id = cPayers.terminal_aggregator_id,
                        hierarchy = cPayers.hierarchy,
                        export_effective_from = cPayers.export_effective_from,
                        EffectiveStart = tEffectivePayers?.EffectiveStart,
                        EffectiveEnd = tEffectivePayers?.EffectiveEnd,
                        rank = 0,
                        evv_configuration_id = cPayers.evv_configuration_id
                    };

                    // Get Dual eligible payer effective dates
                    var DEPayerIDs = string.Join(',',
                            clientTerminalPayers.Where(x => x.hierarchy == 'D')
                                .Select(x => x.Terminal_client_payer_id).Distinct().ToList()
                        );
                    Columns = "DualEligibilePayers.DualEligibilePayerID," +
                        "DualEligibilePayers.Effective_Start," +
                        "DualEligibilePayers.Effective_End";
                    Filters = _repositoryFilterConditions.InFilter("DualEligibilePayers.DualEligibilePayerID", DEPayerIDs, integerType).Build();
                    var DEPEffectivePeriods = await _clientsRepository.GetClientDualEligiblePaymentSourceslist(_actionContext.HHA, _actionContext.User, Columns, Filters);

                    // update dual eligible payers effective dates
                    effectiveClientTerminalPayers =
                        from cPayers in effectiveClientTerminalPayers
                        join dPayers in DEPEffectivePeriods on cPayers.Terminal_client_payer_id equals dPayers.DualEligibilePayerID into dualEffectivePeriods
                        from dEffectivePayers in dualEffectivePeriods.DefaultIfEmpty()
                        select new
                        {
                            client_id = cPayers.client_id,
                            Primary_client_payer_id = cPayers.Primary_client_payer_id,
                            Terminal_client_payer_id = cPayers.Terminal_client_payer_id,
                           // terminal_aggregator_id = cPayers.terminal_aggregator_id,
                            hierarchy = cPayers.hierarchy,
                            export_effective_from = cPayers.export_effective_from,
                            EffectiveStart = dEffectivePayers?.Effective_Start ?? cPayers.EffectiveStart,
                            EffectiveEnd = dEffectivePayers?.Effective_End ?? cPayers.EffectiveEnd,
                            rank = 0,
                            evv_configuration_id = cPayers.evv_configuration_id
                        };

                    // Get rank
                    effectiveClientTerminalPayers =
                        effectiveClientTerminalPayers
                        .GroupBy(x =>
                        (
                            x.Primary_client_payer_id,
                            x.hierarchy
                        ))
                        .SelectMany(x =>
                            x.OrderBy(item => item.Terminal_client_payer_id)
                            .Select((j, i) => new
                            {
                                client_id = j.client_id,
                                Primary_client_payer_id = j.Primary_client_payer_id,
                                Terminal_client_payer_id = j.Terminal_client_payer_id,
                               // terminal_aggregator_id = j.terminal_aggregator_id,
                                hierarchy = j.hierarchy,
                                export_effective_from = j.export_effective_from,
                                EffectiveStart = j.EffectiveStart,
                                EffectiveEnd = j.EffectiveEnd,
                                rank = i + 1,
                                evv_configuration_id = j.evv_configuration_id
                            }
                        ));

                    //Construct Terminal Payer Schedules List
                    var terminalPayerSchedules = (
                        from schedules in request.terminalPayerSchedules
                        join scheduleUID in parentScheduleKTIDs on schedules.cgtask_id equals scheduleUID.CGTASK_ID into parentSchedulesUIDs
                        from parentSchedules in parentSchedulesUIDs.DefaultIfEmpty()
                        join childScheduleUID in ChildScheduleKTIDs on schedules.child_schedule_id equals childScheduleUID.Child_Schedule_Id into childScheduleUIDs
                        from childSchedules in childScheduleUIDs.DefaultIfEmpty()
                        join terminalPayers in effectiveClientTerminalPayers on schedules.terminal_client_payer_id equals terminalPayers.Terminal_client_payer_id
                        where terminalPayers.Primary_client_payer_id == schedules.primary_client_payer_id
                        select new EvvTerminalPayerSchedules
                        {
                            cgtask_id = schedules.cgtask_id,
                            child_schedule_id = schedules.child_schedule_id,
                            kantime_unique_id = (
                                parentSchedules?.EvvMyUniqueID > 0 ?
                                    (terminalPayers.hierarchy == 'S' ? '3' :
                                    terminalPayers.hierarchy == 'T' ? '5' :
                                    terminalPayers.hierarchy == 'D' ? '7' : "0")
                                    + terminalPayers.rank.ToString() + parentSchedules.EvvMyUniqueID.ToString().Substring(1, parentSchedules.EvvMyUniqueID.ToString().Length - 1) :
                                childSchedules?.childScheduleUniqueIDKT > 0 ?
                                    (terminalPayers.hierarchy == 'S' ? '4' :
                                    terminalPayers.hierarchy == 'T' ? '6' :
                                    terminalPayers.hierarchy == 'D' ? '8' : "0")
                                    + terminalPayers.rank.ToString() + childSchedules.childScheduleUniqueIDKT.ToString().Substring(1, childSchedules.childScheduleUniqueIDKT.ToString().Length - 1) : "0"

                            ),
                            client_id = terminalPayers.client_id,
                            client_payer_id = schedules.primary_client_payer_id,
                            hierarchy = terminalPayers.hierarchy,
                            client_terminal_payer_id = schedules.terminal_client_payer_id,
                            //terminal_payer_aggregator_id = terminalPayers.terminal_aggregator_id,
                            schedule_date = schedules.child_schedule_id > 0 ? childSchedules.SCHEDULE_DATE : parentSchedules.PLANNED_DATE,
                            evv_configuration_id = terminalPayers.evv_configuration_id
                        }
                        ).ToList();

                    foreach (var item in terminalPayerSchedules)
                    {
                        item.is_deleted = 
                            !effectiveClientTerminalPayers
                            .Any(terminalPayers =>
                                item.client_terminal_payer_id == terminalPayers.Terminal_client_payer_id
                                && (terminalPayers.EffectiveStart <= item.schedule_date
                                    || terminalPayers.EffectiveStart == DateTime.MinValue
                                )
                                && (terminalPayers.EffectiveEnd >= item.schedule_date
                                    || terminalPayers.EffectiveEnd == DateTime.MinValue
                                )
                                && (terminalPayers.export_effective_from <= item.schedule_date
                                    || terminalPayers.export_effective_from == DateTime.MinValue
                                )
                            );
                    }

                    if (!terminalPayerSchedules.Any())
                    {
                        return Response.ValidationError<RegisterEvvTerminalPayerSchedulesDto>("There are no schedule(s) applicable to given terminal payer(s)");
                    }
                    
                    var result = await _eVVSchedule.RegisterTerminalPayerSchedules(_actionContext.HHA, _actionContext.User, terminalPayerSchedules);

                    if(result.http_status_code == HttpStatusCode.OK)
                    {
                        registerEvvTerminalPayerSchedules = new RegisterEvvTerminalPayerSchedulesDto
                        {
                            cgtask_id = result.response_payload.Select(x => x.CgTaskID).Distinct().ToArray(),
                            child_schedule_id = result.response_payload.Where(x => x.child_schedule_id > 0).Select(x => x.child_schedule_id).Distinct().ToArray()
                        };
                    }
                    else if (result.http_status_code == HttpStatusCode.NoContent)
                    {
                        Response.NoContent(registerEvvTerminalPayerSchedules);
                    }
                    else
                    {
                        return Response.Fail<RegisterEvvTerminalPayerSchedulesDto>(ResponseErrorCodes.InternalServerFailed, "Failed to register schedules");
                    }

                }
                else
                {
                    var result = await _eVVSchedule.AutoRegisterEvvTerminalPayerSchedules(_actionContext.HHA, _actionContext.User);
                    if (result.http_status_code == System.Net.HttpStatusCode.OK)
                    {
                        registerEvvTerminalPayerSchedules = new RegisterEvvTerminalPayerSchedulesDto 
                        { 
                            cgtask_id = result.response_payload.Select(x => x.CgTaskID).Distinct().ToArray(),
                            child_schedule_id = result.response_payload.Where(x => x.child_schedule_id > 0).Select(x => x.child_schedule_id).Distinct().ToArray()
                        };
                    }
                    else if (result.http_status_code == HttpStatusCode.NoContent)
                    {
                        Response.NoContent(registerEvvTerminalPayerSchedules);
                    }
                    else
                    {
                        return Response.Fail<RegisterEvvTerminalPayerSchedulesDto>(result.error_code, result.error_message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<RegisterEvvTerminalPayerSchedulesDto>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }

            if (registerEvvTerminalPayerSchedules.cgtask_id.Length == 0)
            {
                return Response.NoContent(registerEvvTerminalPayerSchedules);
            }

            return Response.Ok(registerEvvTerminalPayerSchedules);
        }
    }
}

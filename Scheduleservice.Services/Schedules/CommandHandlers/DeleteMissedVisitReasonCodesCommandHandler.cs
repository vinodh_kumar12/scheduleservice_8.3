﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Command;
using System;
using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class DeleteMissedVisitReasonCodesCommandHandler : IHandlerWrapper<DeleteMissedVisitReasonCodesCommand, bool>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public readonly ICaregiverTaskEvvMissedVisitsReasonsRepository _caregiverTaskEvvMissedVisitsReasonsRepository;

        public DeleteMissedVisitReasonCodesCommandHandler(IHttpContextAccessor httpContextAccessor,
                   IRepositoryFilterConditions repositoryFilterConditions, ICaregiverTaskEvvMissedVisitsReasonsRepository caregiverTaskEvvMissedVisitsReasonsRepository)
        {
            _httpContextAccessor = httpContextAccessor;

            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregiverTaskEvvMissedVisitsReasonsRepository = caregiverTaskEvvMissedVisitsReasonsRepository;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<bool>> Handle(DeleteMissedVisitReasonCodesCommand request, CancellationToken cancellationToken)
        { 
            try
            { 
                EvvMissedVisitCodesDto return_dto = new EvvMissedVisitCodesDto();

                var filters = _repositoryFilterConditions.EqualFilter("CGTASKID", request.cgtask_id, integerType).Build();

                var columns = "CaregiverTaskEvvskMissedvisitsReasonID";

                var _out_list = await _caregiverTaskEvvMissedVisitsReasonsRepository.GetMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, columns, filters);

                if (!_out_list.Any())
                    return Response.NoContent<bool>(false);
                  
                string MissitvisitsJSON = JsonConvert.SerializeObject(new
                {
                    isDeleted = true,
                    DeletedOn = DateTime.Now.Month +"/" + DateTime.Now.Day + "/" + DateTime.Now.Year,
                    DeletedBy = ActionContext.User
                }); 

                columns = new BuildUpdateQuery<MissedVisitReasonCodeinfo>().GetBuildUpdateQueryStatement(MissitvisitsJSON);
                filters += _repositoryFilterConditions.EqualFilter("isdeleted", false, boolType).Build();
                filters += _repositoryFilterConditions.EqualFilter("Context", "MISSED_VISIT", stringType).Build();

                int _outMissedvisit_list = await _caregiverTaskEvvMissedVisitsReasonsRepository.UpdateMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, filters, columns);
                 
                if (_outMissedvisit_list == 1) 
                    return Response.Fail<bool>("INTERNAL_EXCEPTION", "Failed to delete reason codes"); 
                else 
                    return Response.Ok<bool>(true);  
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using Newtonsoft.Json;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class ApproveScheduleCommandHandler : IHandlerWrapper<ApproveScheduleCommand, IEnumerable<string>>
    {
        private readonly ISchedule _schedule;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        public ApproveScheduleCommandHandler(ISchedule schedule, ICaregivertasksRepository caregivertasksRepository)
        {
            _schedule = schedule;
            _caregivertasksRepository = caregivertasksRepository;
        }

        public async Task<Response<IEnumerable<string>>> Handle(ApproveScheduleCommand request, CancellationToken cancellationToken)
        {
            List<string> result = new List<string>();
            string error_message = "";
            var ValidationResult = await _schedule.ValidateApproveSchedule(request.HHA, request.UserId, request.CGTask_ID);
            
            if (ValidationResult.Any())
            { 
                foreach (var item in ValidationResult)
                {
                    result.Add(item.validation_error_message);
                }
                error_message = JsonConvert.SerializeObject(result);
                 
               
                return Response.ValidationError<IEnumerable<string>>(error_message);
            }
            var ret = await _schedule.ApproveSchedule(request.HHA, request.UserId, request.CGTask_ID);

            if(ret != 1)
            {
                switch (ret)
                {
                    case -770:
                        error_message = "Schedule cannot be approved because service is terminated before the Schedule Date";
                        break;
                    case -888:
                        error_message = "Secondary Authorizations is not available";
                        break;

                    default:
                        error_message = "Failed to Approve schedule";
                        break;
                }

                return Response.ValidationError<IEnumerable<string>>(error_message, result);
            }
            return Response.Ok<IEnumerable<string>>(null);
        }
    }
}

﻿namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Request;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.EVV;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.Models.Authorization;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Services.Schedules.Command;
    using System;
    using System.Collections.Generic; 
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    public class ScheduleCheckinCommandHandler : IHandlerWrapper<ScheduleCheckinCommand, IEnumerable<string>>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly ISchedule _scheduleService;
        private readonly IAutoGenerateException _autoGenerateException;
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregiverTaskEVVReasonsRepository _caregiverTaskEVVReasonsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;   
        private readonly IAuthorization _authorization; 
        private readonly IGetScheduleBasicDetail _getScheduleBasicDetail;
        private readonly IMapper _mapper;

        public ScheduleCheckinCommandHandler(IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
                    ISchedule scheduleService, IAutoGenerateException autoGenerateException,
                    IClientsRepository clientsRepository,
                    ICaregiverTaskEVVReasonsRepository caregiverTaskEVVReasonsRepository,
                    IRepositoryFilterConditions repositoryFilterConditions,
                    IAuthorization authorization, 
                    IGetScheduleBasicDetail getScheduleBasicDetail,
                    IMapper mapper)
        {
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            this._scheduleService = scheduleService;
            this._autoGenerateException = autoGenerateException;
            this._clientsRepository = clientsRepository;
            this._caregiverTaskEVVReasonsRepository = caregiverTaskEVVReasonsRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;             
            this._authorization = authorization; 
            this._getScheduleBasicDetail = getScheduleBasicDetail;
            this._mapper = mapper;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<IEnumerable<string>>> Handle(ScheduleCheckinCommand request, CancellationToken cancellationToken)
        {
            try
            { 
                var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var _caregivertasksEntity = await _caregivertasksRepository.GetAllSchedulesList(ActionContext.HHA, ActionContext.User, "", Filters);

                if (_caregivertasksEntity.Count() == 0)
                {
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Invalid CgtaskID.");
                }

                var caregivertask = _caregivertasksEntity.FirstOrDefault();
                //get Schedule info
                 
                var updatedCaregiverEntityforValidation = (CaregivertasksEntity)caregivertask.Shallowcopy();
                 
                if (caregivertask.CAREGIVER == null) 
                    return Response.ValidationError<IEnumerable<string>>("Please assign Clincian to Checkin");
                 
                if (caregivertask.STATUS != ScheduleStatusEnums.Planned.ToString()) 
                    return Response.ValidationError<IEnumerable<string>>("Schedule is already Checked In"); 
                  
                if (string.IsNullOrEmpty(request.checkin_date))
                    request.checkin_date = caregivertask.PLANNED_DATE.ToDateFormat();

                DateTime CheckinDatetime = Convert.ToDateTime(request.checkin_date + " " + request.checkin_time);
                updatedCaregiverEntityforValidation.ACCTUAL_START_TIME = CheckinDatetime;
                updatedCaregiverEntityforValidation.PLANNED_DATE = Convert.ToDateTime(request.checkin_date);
                updatedCaregiverEntityforValidation.CHECK_IN_LOCATION = request.caregiver_geolocation;
                updatedCaregiverEntityforValidation.STATUS = ScheduleStatusEnums.In_Progress.ToString();
                updatedCaregiverEntityforValidation.CheckInSource = Convert.ToByte(request.checkin_source);
                
                ValidateScheduleRules validateScheduleRules = await _getScheduleBasicDetail.getScheduleDetails(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation);

                validateScheduleRules.confirm_softwarning = request.confirm_softwarning;

                validateScheduleRules.caregiverTaskEntity = updatedCaregiverEntityforValidation;

                //validate schedule checkin
                var ValidationResult = await _scheduleService.ValidateScheduleCheckInOutAsync(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation, validateScheduleRules);

                bool has_soft_warning = false;
                List<string> result = new List<string>();
                bool has_validation = true;
                List<ValidationErrorInfo> validationErrorForConflictExceptions = new List<ValidationErrorInfo>();

                if (ValidationResult.Any())
                { 
                    if (ValidationResult.Where(x => x.is_soft_warning == true).Any())
                    {
                        if (!ValidationResult.Where(x => x.is_soft_warning == false).Any())
                            has_soft_warning = true;
                    } 
                   
                    if ((has_soft_warning && request.confirm_softwarning)
                        ||
                        (validateScheduleRules.AllowToCheckoutifHardStopValidationExists)
                        )
                    {
                        has_validation = false;
                    }

                    if (!has_validation)
                    {
                        foreach (var item in ValidationResult)
                        { 
                            //check if there is validation exists in Conflict Exceptions
                            if (Enum.IsDefined(typeof(ConflictExceptions), item.validation_type))
                                validationErrorForConflictExceptions.Add(item);
                        }

                        if (ValidationResult.Count() != validationErrorForConflictExceptions.Count())
                            has_validation = true;

                        validationErrorForConflictExceptions = validationErrorForConflictExceptions.Where(x => x.is_soft_warning == false).ToList();

                    }
                }
                else
                    has_validation = false;


                if (has_validation)
                {
                    foreach (var item in ValidationResult)
                    {
                        result.Add(item.validation_error_message); 
                    } 

                    List<ScheduleValidationErrorsDto> scheduleValidationErrorsDto = new List<ScheduleValidationErrorsDto>();
                    //map
                    scheduleValidationErrorsDto = _mapper.Map<List<ScheduleValidationErrorsDto>>(ValidationResult);


                    Scheduleerrorinfo scheduleerrorinfo = new Scheduleerrorinfo();
                    scheduleerrorinfo.errors = scheduleValidationErrorsDto.ToList();
                    scheduleerrorinfo.schedule_uuid = caregivertask.Publish_UID.ToString();//_caregivertasksEntity.pub;
                    scheduleerrorinfo.schedule_id = caregivertask.CGTASK_ID;

                    string error_message = JsonConvert.SerializeObject(result);
                    string error_details = JsonConvert.SerializeObject(scheduleerrorinfo);

                    if (has_soft_warning)
                        return Response.SoftWarningError<IEnumerable<string>>(error_message, error_details);

                    return Response.ValidationError<IEnumerable<string>>(error_message, default, error_details);
                }

                //get client info
                var Clientinfo = await _clientsRepository.GetClientsBasicInfo(ActionContext.HHA, ActionContext.User, caregivertask.CLIENT_ID);

                //Get Fields to Update on Checkin
                EditScheduleProperties editScheduleProperties = GetCheckinModel(ActionContext.HHA, ActionContext.User, request, Clientinfo, caregivertask, validateScheduleRules.RequireGeoLocationforFOB);

                //Update Checkin Info
                var ret = await _scheduleService.ModifyScheduleAsync(ActionContext.HHA, ActionContext.User, request.CGTask_ID, editScheduleProperties, caregivertask, !has_validation);

                int ResolvedReasoncodeID = 0;
                int EVVVendorVersionMasterID = 0;

                if (ret == 0 
                       && ((caregivertask.Units != editScheduleProperties.Units)
                           || 
                           (caregivertask.CanBill != editScheduleProperties.CanBill)
                           ||
                           (caregivertask.TotalHours != editScheduleProperties.TotalHours)
                           ||
                           (caregivertask.IS_AUTH_MANDATORY != editScheduleProperties.IS_AUTH_MANDATORY)
                          )
                   )
                {
                    //Update Auth info
                    var result_Authinfo = await _authorization.UpdateAuthorizationInfo(ActionContext.HHA, ActionContext.User, editScheduleProperties, caregivertask);

                    if (!result_Authinfo)
                        ret = 1;
                }

                if (caregivertask.isEvvschedule && ret == 0)
                {
                    //EVVVendorVersionMasterID = await _scheduleService.GetScheduleEVVVendorVersionMasterID(ActionContext.HHA, ActionContext.User,caregivertask, Clientinfo.HHA_BRANCH_ID, false);

                    EVVVendorVersionMasterID = validateScheduleRules.caregiverTaskEntity.EvvVendorVersionMasterID;
                    if (!string.IsNullOrEmpty(request.reason_code) && EVVVendorVersionMasterID > 0)
                    {  
                        string ReasonCodesJSON = JsonConvert.SerializeObject(new
                                                            {
                                                                CaregiverTaskEvvReasonID = 0,
                                                                Code = request.reason_code,
                                                                Description = request.reason_desc,
                                                                Notes = request.reason_notes,
                                                                CheckinCheckoutType = "CheckIn",
                                                                EVVVendorVersionMasterId = EVVVendorVersionMasterID,
                                                                ReasonContext = request.reason_context
                                                            });
                        string ActionCodesJSON = "";
                        if (!string.IsNullOrEmpty(request.action_code))
                        {
                            ActionCodesJSON = JsonConvert.SerializeObject(new
                                                            {
                                                                ActionCode = request.action_code,
                                                                ActionDescription = request.action_desc,
                                                                ActionNotes = request.reason_notes,
                                                                CaregiverTaskEvvReasonID = 0
                                                            });
                        }
                         
                        ResolvedReasoncodeID = await _caregiverTaskEVVReasonsRepository.AddCaregiverTaskEVVReasons(ActionContext.HHA, ActionContext.User, request.CGTask_ID, ReasonCodesJSON, ActionCodesJSON);
                    }

                    if (!string.IsNullOrEmpty(request.exception_code))
                    {
                        //EVVVendorVersionMasterID = await _scheduleService.GetScheduleEVVVendorVersionMasterID(ActionContext.HHA, ActionContext.User, caregivertask, Clientinfo.HHA_BRANCH_ID, false);

                        EVVVendorVersionMasterID=validateScheduleRules.caregiverTaskEntity.EvvVendorVersionMasterID;
                        if (EVVVendorVersionMasterID > 0)
                        {
                            EVVExceptioninfoDto eVVExceptioninfoDto = new EVVExceptioninfoDto()
                            {
                                Code = request.exception_code,
                                Description = request.exception_desc,
                                Context = "CheckIn",
                                EVVVendorVersionMasterID = EVVVendorVersionMasterID, //find evvvendorveriosnmasterid
                                Event = "",
                                ResolvedReasoncodeID = ResolvedReasoncodeID
                            };

                            var issuccess = await _autoGenerateException.GenerateException(ActionContext.HHA, ActionContext.User, request.CGTask_ID, eVVExceptioninfoDto);

                            if (!issuccess)
                                ret = 1;
                        }
                    }
                    else
                    {
                        ScheduleEVVInfo scheduleEVVInfo = new ScheduleEVVInfo()
                        {
                            ACCTUAL_START_TIME = editScheduleProperties.ACCTUAL_START_TIME,
                            EvvCheckinLocationVerified = editScheduleProperties.EvvCheckinLocationVerified,
                            HasCheckInGeoLocation = !string.IsNullOrEmpty(editScheduleProperties.CHECK_IN_LOCATION) ? true : false
                        };

                        var result_evvexception = await _autoGenerateException.RaiseEVVExceptionsOnCheckinCheckout(ActionContext.HHA, ActionContext.User, caregivertask, scheduleEVVInfo,
                                    EVVVendorVersionMasterID,ResolvedReasoncodeID,0, !request.is_clincian_checkin);

                        if (!result_evvexception)
                            ret = 1;
                    }
                }


                if (ret == 0 && validateScheduleRules.AllowToCheckoutifHardStopValidationExists && validationErrorForConflictExceptions.Any())
                { 

                    //Generate conflict Exceptions
                    _ = await _scheduleService.GenerateConflictExceptions(ActionContext.HHA, ActionContext.User, validationErrorForConflictExceptions, caregivertask.CGTASK_ID);

                }

                if (ret == 0)
                {
                    IEnumerable<string> output = new List<string>() { "Success" };
                    return Response.Ok<IEnumerable<string>>(output);
                }
                else
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Failed to CheckIn schedule.");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private EditScheduleProperties GetCheckinModel(int HHA, int User, ScheduleCheckinCommand request, ClientsEntity clientsEntity, CaregivertasksEntity caregivertasksEntity,bool RequireGeoLocationforFOB)
        {
            //map ScheduleCheckinCommand to EditScheduleProperties

            EditScheduleProperties editScheduleProperties = new EditScheduleProperties();

            //Get Checkin date time
            DateTime CheckinDatetime = Convert.ToDateTime(request.checkin_date + " " + request.checkin_time);
            editScheduleProperties.ACCTUAL_START_TIME = CheckinDatetime;
            editScheduleProperties.PLANNED_DATE = Convert.ToDateTime(request.checkin_date);
            editScheduleProperties.ACCTUAL_DATE = Convert.ToDateTime(request.checkin_date);

            editScheduleProperties.STATUS = ScheduleStatusEnums.In_Progress.ToString();
            editScheduleProperties.CheckInSource = Convert.ToByte(request.checkin_source);
            editScheduleProperties.CheckinTreatmentLocation = request.client_treatment_adress_id == 0? null: request.client_treatment_adress_id;
            if (editScheduleProperties.CheckinTreatmentLocation > 0)
                editScheduleProperties.CheckoutTreatmentLocation = editScheduleProperties.CheckinTreatmentLocation;

            editScheduleProperties.fob_checkin_code = request.fob_code;
            editScheduleProperties.isAttested = request.is_attested;

            editScheduleProperties.fob_checkin_device_id = request.fob_checkin_device_id;
            editScheduleProperties.TotalHours = caregivertasksEntity.TotalHours;
            editScheduleProperties.CLIENT_ID = caregivertasksEntity.CLIENT_ID;

            EditScheduleProperties updatededitScheduleProperties = _scheduleService.GetEditSchedulePropertiesModel(HHA, User, editScheduleProperties, caregivertasksEntity, clientsEntity, RequireGeoLocationforFOB).Result;

            return updatededitScheduleProperties;
        }
    }
}

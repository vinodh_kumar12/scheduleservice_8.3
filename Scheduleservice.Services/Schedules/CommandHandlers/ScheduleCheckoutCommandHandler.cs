﻿namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Request;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.EVV;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Services.Schedules.Command; 
    using System;
    using System.Collections.Generic; 
    using System.Linq; 
    using System.Threading;
    using System.Threading.Tasks;
    public class ScheduleCheckoutCommandHandler : IHandlerWrapper<ScheduleCheckoutCommand, IEnumerable<string>>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly ISchedule _scheduleService;
        private readonly IAutoGenerateException _autoGenerateException;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;
        private readonly IPaymentsourcesAdditionalRepository _paymentsourcesAdditionalRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IClientsRepository _clientsRepository; 
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly ICaregiverTaskEVVReasonsRepository _caregiverTaskEVVReasonsRepository;
        private readonly IEchartSignaturesRepository _echartSignaturesRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IClientTreatmentAddressRepository _clientTreatmentAddressRepository;
        private readonly IAuthorization _authorization;
        private readonly IMapper _mapper;
        private readonly IGetScheduleBasicDetail _getScheduleBasicDetail;

        public ScheduleCheckoutCommandHandler(IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository,
                    ISchedule scheduleService, IAutoGenerateException autoGenerateException,
                    IPaymentsourcesRepository paymentsourcesRepository,
                    IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository,
                    IPaymentsourcesAdditionalRepository paymentsourcesAdditionalRepository,
                    IServiceCodesRepository serviceCodesRepository,
                    IRepositoryFilterConditions repositoryFilterConditions,
                    IClientsRepository clientsRepository,
                    ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
                    ICaregiverTaskEVVReasonsRepository caregiverTaskEVVReasonsRepository,
                    IEchartSignaturesRepository echartSignaturesRepository,
                    IAddressRepository addressRepository,
                    IClientTreatmentAddressRepository clientTreatmentAddressRepository,
                    IAuthorization authorization,
                    IMapper mapper,
                    IGetScheduleBasicDetail getScheduleBasicDetail)
        {
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            this._scheduleService = scheduleService;
            this._autoGenerateException = autoGenerateException;
            this._paymentsourcesRepository = paymentsourcesRepository;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            this._paymentsourcesAdditionalRepository = paymentsourcesAdditionalRepository;
            this._serviceCodesRepository = serviceCodesRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._clientsRepository = clientsRepository;
            this._cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            this._caregiverTaskEVVReasonsRepository = caregiverTaskEVVReasonsRepository;
            this._echartSignaturesRepository = echartSignaturesRepository;
            this._addressRepository = addressRepository;
            this._clientTreatmentAddressRepository = clientTreatmentAddressRepository;
            this._authorization = authorization;
            this._mapper = mapper;
            this._getScheduleBasicDetail = getScheduleBasicDetail;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

       
        public async Task<Response<IEnumerable<string>>> Handle(ScheduleCheckoutCommand request, CancellationToken cancellationToken)
        {
            try
            {
                //get Schedule info
                var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var _caregivertasksEntity = await _caregivertasksRepository.GetAllSchedulesList(ActionContext.HHA, ActionContext.User, "", Filters);
                if (_caregivertasksEntity.Count() == 0)
                {
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Invalid CgtaskID.");
                }
                var caregivertask = _caregivertasksEntity.FirstOrDefault();

                var updatedCaregiverEntityforValidation = (CaregivertasksEntity)caregivertask.Shallowcopy();

                if (caregivertask.STATUS != ScheduleStatusEnums.In_Progress.ToString())
                {
                    var errormessage = "Schedule is not Checked In.Please Checkin";
                    if (caregivertask.STATUS != ScheduleStatusEnums.Planned.ToString())
                        errormessage = "Schedule is already checked out..";

                    return Response.ValidationError<IEnumerable<string>>(errormessage);
                } 

                //Get Checkin date time
                DateTime CheckOutDatetime = Convert.ToDateTime(caregivertask.PLANNED_DATE.ToDateFormat() + " " + request.checkout_time);
                if (CheckOutDatetime <= caregivertask.ACCTUAL_START_TIME)
                    CheckOutDatetime = CheckOutDatetime.AddDays(1);
                updatedCaregiverEntityforValidation.ACCTUAL_END_TIME = CheckOutDatetime;
                updatedCaregiverEntityforValidation.CHECK_OUT_LOCATION = request.caregiver_geolocation;
                updatedCaregiverEntityforValidation.STATUS = ScheduleStatusEnums.Completed.ToString();
                updatedCaregiverEntityforValidation.CHECK_IN_LOCATION = "";
                
                //Edited hours  
                var columns = "ROUNDOFF_Direction, ROUNDOFF_Minutes, Round_ZeroUnitScheduleToOne,PayerSourceId ";

                var payeradditionalinfo = await _paymentsourcesAdditionalRepository.GetPayerAdditionalInfo(ActionContext.HHA, ActionContext.User, caregivertask.PAYMENT_SOURCE, columns);

                //Edited hours  
                if (string.IsNullOrEmpty(request.edited_hours))
                    updatedCaregiverEntityforValidation.EDITED_HOURS = Common.GetVisitEditedHours(Convert.ToDateTime(caregivertask.ACCTUAL_START_TIME), CheckOutDatetime, payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes);
                else
                    updatedCaregiverEntityforValidation.EDITED_HOURS = request.edited_hours;

                //total hours 
                updatedCaregiverEntityforValidation.TotalHours = Common.GetVisitActualHours(Convert.ToDateTime(caregivertask.ACCTUAL_START_TIME), CheckOutDatetime);

                updatedCaregiverEntityforValidation.CheckOutSource = Convert.ToByte(request.checkout_source);


                ValidateScheduleRules validateScheduleRules = await _getScheduleBasicDetail.getScheduleDetails(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation);
                
                updatedCaregiverEntityforValidation.Units = await _scheduleService.GetVisitUnits(ActionContext.HHA, ActionContext.User, payeradditionalinfo, caregivertask.SERVICECODE_ID, Convert.ToDateTime(caregivertask.ACCTUAL_START_TIME), CheckOutDatetime, validateScheduleRules.clientsEntity.HHA_BRANCH_ID, request.CGTask_ID);

                validateScheduleRules.confirm_softwarning = request.confirm_softwarning;

                validateScheduleRules.caregiverTaskEntity = updatedCaregiverEntityforValidation;

                //validate schedule checkin
                var ValidationResult = await _scheduleService.ValidateScheduleCheckInOutAsync(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation, validateScheduleRules);

                bool has_soft_warning = false;
                bool has_validation = true;
                List<string> result = new List<string>();
                List<ValidationErrorInfo> validationErrorForConflictExceptions = new List<ValidationErrorInfo>();

                if (ValidationResult.Any())
                {
                    
                    if (ValidationResult.Where(x => x.is_soft_warning == true).Any())
                    {
                        if (!ValidationResult.Where(x => x.is_soft_warning == false).Any())
                            has_soft_warning = true;
                    }

                    if ((has_soft_warning && request.confirm_softwarning)
                        ||
                        (validateScheduleRules.AllowToCheckoutifHardStopValidationExists)
                        )
                    {
                        has_validation = false;
                    }

                    if (!has_validation)
                    {
                        foreach (var item in ValidationResult)
                        {
                            //check if there is validation exists in Conflict Exceptions
                            if (Enum.IsDefined(typeof(ConflictExceptions), item.validation_type))
                                validationErrorForConflictExceptions.Add(item);
                        }

                        if (ValidationResult.Count() != validationErrorForConflictExceptions.Count())
                            has_validation = true;

                        validationErrorForConflictExceptions = validationErrorForConflictExceptions.Where(x => x.is_soft_warning == false).ToList();

                    }
                }
                else
                    has_validation = false;

                if (has_validation)
                {
                    foreach (var item in ValidationResult) 
                        result.Add(item.validation_error_message); 

                    List<ScheduleValidationErrorsDto> scheduleValidationErrorsDto = new List<ScheduleValidationErrorsDto>();
                    //map
                    scheduleValidationErrorsDto = _mapper.Map<List<ScheduleValidationErrorsDto>>(ValidationResult);
                   
                    
                    Scheduleerrorinfo scheduleerrorinfo = new Scheduleerrorinfo();
                    scheduleerrorinfo.errors = scheduleValidationErrorsDto.ToList();
                    scheduleerrorinfo.schedule_uuid = caregivertask.Publish_UID.ToString();//_caregivertasksEntity.pub;
                    scheduleerrorinfo.schedule_id = caregivertask.CGTASK_ID;

                    string error_message = JsonConvert.SerializeObject(result);
                    string error_details = JsonConvert.SerializeObject(scheduleerrorinfo);

                    if (has_soft_warning)
                        return Response.SoftWarningError<IEnumerable<string>>(error_message, error_details);

                    return Response.ValidationError<IEnumerable<string>>(error_message, default, error_details);
                }

                //Split schedule level flags , if it is overnight based on the HHA/ payer setting
                var hHAScheduleAuthSettingsEntity = validateScheduleRules.hHAScheduleAuthSettingsEntities;
                 
                if (hHAScheduleAuthSettingsEntity.FirstOrDefault().SplitOvernightSchedulesAtCreationlevel)
                {
                    //get payer level setting
                    var payer =  await _paymentsourcesRepository.GetPaymentSourceBasicInfo(ActionContext.HHA, ActionContext.User, caregivertask.PAYMENT_SOURCE);

                    if (!caregivertask.Has_Child_Schedules ?? false && payer.SplitOvernightSchedules)
                    {
                        caregivertask.Has_Child_Schedules = payer.SplitOvernightSchedules;
                        caregivertask.IsSplitForBilling = caregivertask.Has_Child_Schedules;
                    }
                }
               
                //Get Fields to Update on Checkin
                EditScheduleProperties editScheduleProperties = GetCheckOutModel(ActionContext.HHA, ActionContext.User, request, caregivertask, validateScheduleRules.clientsEntity, payeradditionalinfo,validateScheduleRules.RequireGeoLocationforFOB);

                //Update Checkin Info
                var ret = await _scheduleService.ModifyScheduleAsync(ActionContext.HHA, ActionContext.User, request.CGTask_ID, editScheduleProperties, caregivertask, !has_validation);

                if (ret < 0)
                {
                    return Response.ValidationError<IEnumerable<string>>("Authorization is not sufficient to split the schedule.");
                }

                if (ret == 0
                       && ((caregivertask.Units != editScheduleProperties.Units)
                           ||
                           (caregivertask.CanBill != editScheduleProperties.CanBill)
                           ||
                           (caregivertask.TotalHours != editScheduleProperties.TotalHours)
                           ||
                           (caregivertask.IS_AUTH_MANDATORY != editScheduleProperties.IS_AUTH_MANDATORY)
                          )
                   )
                {
                    //Update Auth info
                    var result_Authinfo = await _authorization.UpdateAuthorizationInfo(ActionContext.HHA, ActionContext.User, editScheduleProperties, caregivertask);

                    if (!result_Authinfo)
                        ret = 1;
                }

                int ResolvedReasoncodeID = 0;
                int EVVVendorVersionMasterID = 0;

                if (caregivertask.isEvvschedule)
                {
                    //EVVVendorVersionMasterID = await _scheduleService.GetScheduleEVVVendorVersionMasterID(ActionContext.HHA, ActionContext.User, caregivertask, validateScheduleRules.clientsEntity.HHA_BRANCH_ID, false);

                    EVVVendorVersionMasterID=validateScheduleRules.caregiverTaskEntity.EvvVendorVersionMasterID;
                    if (ret == 0 && !string.IsNullOrEmpty(request.reason_code) && EVVVendorVersionMasterID > 0)
                    {
                        string ReasonCodesJSON = JsonConvert.SerializeObject(new
                        {
                            CaregiverTaskEvvReasonID = 0,
                            Code = request.reason_code,
                            Description = request.reason_desc,
                            Notes = request.reason_notes,
                            CheckinCheckoutType = "CheckOut",
                            EVVVendorVersionMasterId = EVVVendorVersionMasterID,
                            ReasonContext = request.reason_context
                        });

                        string ActionCodesJSON = "";
                        if (!string.IsNullOrEmpty(request.action_code))
                        {
                            ActionCodesJSON = JsonConvert.SerializeObject(new
                            {
                                ActionCode = request.action_code,
                                ActionDescription = request.action_desc,
                                ActionNotes = request.reason_notes,
                                CaregiverTaskEvvReasonID = 0
                            });
                        }

                        ResolvedReasoncodeID = await _caregiverTaskEVVReasonsRepository.AddCaregiverTaskEVVReasons(ActionContext.HHA, ActionContext.User, request.CGTask_ID, ReasonCodesJSON, ActionCodesJSON);
                    }

                    if (ret == 0 && !string.IsNullOrEmpty(request.exception_code))
                    {
                        if (EVVVendorVersionMasterID > 0)
                        {
                            EVVExceptioninfoDto eVVExceptioninfoDto = new EVVExceptioninfoDto()
                            {
                                Code = request.exception_code,
                                Description = request.exception_desc,
                                Context = "CheckOut",
                                EVVVendorVersionMasterID = EVVVendorVersionMasterID, //find evvvendorveriosnmasterid
                                Event = "",
                                ResolvedReasoncodeID = ResolvedReasoncodeID
                            };

                            var issuccess = await _autoGenerateException.GenerateException(ActionContext.HHA, ActionContext.User, request.CGTask_ID, eVVExceptioninfoDto);

                            if (!issuccess)
                                ret = 1;

                        }
                    }
                    else if (ret == 0)
                    {

                        var signaturefilters = _repositoryFilterConditions.EqualFilter("CgTaskID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CONTEXT", "eChart,OasisDatasets", FilterConditionDataTypeEnums.stringType)
                                                .Build();

                        var chartinfo = await _echartSignaturesRepository.GetScheduleSignatureInfo(ActionContext.HHA, ActionContext.User, signaturefilters, "SIGNATURE_ID");

                        ScheduleEVVInfo scheduleEVVInfo = new ScheduleEVVInfo()
                        {
                            ACCTUAL_START_TIME = caregivertask.ACCTUAL_START_TIME,
                            EvvCheckinLocationVerified = caregivertask.EvvCheckinLocationVerified,
                            HasCheckInGeoLocation = !string.IsNullOrEmpty(caregivertask.CHECK_IN_LOCATION) ? true : false,
                            ACCTUAL_END_TIME = editScheduleProperties.ACCTUAL_END_TIME,
                            EVVCheckOutLocationVerified = editScheduleProperties.EvvCheckoutLocationVerified,
                            HasCheckOutgeoLocation = !string.IsNullOrEmpty(editScheduleProperties.CHECK_OUT_LOCATION) ? true : false,
                            HasClientSign = chartinfo?.SIGNATURE_ID > 0? true : false,
                            HasClientVoiceRecording = !string.IsNullOrEmpty(caregivertask.EvvCheckOutVoicePath) ? true : false
                        };

                        var result_evvexception = await _autoGenerateException.RaiseEVVExceptionsOnCheckinCheckout(ActionContext.HHA, ActionContext.User,caregivertask, scheduleEVVInfo,
                                    EVVVendorVersionMasterID,0,ResolvedReasoncodeID,!request.is_clincian_checkout);

                        if (!result_evvexception)
                            ret = 1;                         
                    }
                }

                if (ret == 0 && validateScheduleRules.AllowToCheckoutifHardStopValidationExists && validationErrorForConflictExceptions.Any())
                {
                    //Generate conflict Exceptions
                    _ = await _scheduleService.GenerateConflictExceptions(ActionContext.HHA, ActionContext.User, validationErrorForConflictExceptions, caregivertask.CGTASK_ID);

                }


                if (ret == 0)
                {
                    IEnumerable<string> output = new List<string>() { "Success" };
                    return Response.Ok<IEnumerable<string>>(output);
                }
                else
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Failed to CheckOut Schedule");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private EditScheduleProperties GetCheckOutModel(int HHA, int User, ScheduleCheckoutCommand request, CaregivertasksEntity caregivertasksEntity, ClientsEntity clientsEntity, PaymentSourcesAdditionalEntity paymentSourcesAdditionalEntity,bool RequireGeoLocationforFOB)
        {
            EditScheduleProperties editScheduleProperties = new EditScheduleProperties();

            //Get Checkin date time
            editScheduleProperties.ACCTUAL_START_TIME = caregivertasksEntity.ACCTUAL_START_TIME;
            DateTime CheckOutDatetime = Convert.ToDateTime(caregivertasksEntity.PLANNED_DATE.ToDateFormat() + " " + request.checkout_time);
            if (CheckOutDatetime <= editScheduleProperties.ACCTUAL_START_TIME)
                CheckOutDatetime =  CheckOutDatetime.AddDays(1);            
            editScheduleProperties.ACCTUAL_END_TIME = CheckOutDatetime;
            
            editScheduleProperties.STATUS = ScheduleStatusEnums.Completed.ToString();
            editScheduleProperties.CheckOutSource = Convert.ToByte(request.checkout_source);
            editScheduleProperties.CheckoutTreatmentLocation = request.client_treatment_adress_id == 0 ? null : request.client_treatment_adress_id;
            editScheduleProperties.CHECK_OUT_LOCATION = request.caregiver_geolocation;
            editScheduleProperties.EDITED_HOURS_PAYABLE = request.payable_hours;
            editScheduleProperties.EDITED_HOURS = request.edited_hours;

            //fob code
            editScheduleProperties.fob_checkout_code = request.fob_code;
            editScheduleProperties.isAttested = request.is_attested;
            editScheduleProperties.fob_checkout_device_id = request.fob_checkout_device_id;

            EditScheduleProperties updatededitScheduleProperties = _scheduleService.GetEditSchedulePropertiesModel(HHA, User, editScheduleProperties, caregivertasksEntity, clientsEntity, RequireGeoLocationforFOB).Result;
             
            return updatededitScheduleProperties;
        }
         

    } 
}

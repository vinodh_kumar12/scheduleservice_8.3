﻿namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Request;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.EVV;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.Models.Authorization;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Services.Schedules.Command;
    using System;
    using System.Collections.Generic; 
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Scheduleservice.Core.RepositoryQueryBuilder;
    using static Scheduleservice.Core.Enums.FilterConditionDataTypeEnums;
    public class UpdateMissedVisitReasonCodeInfoCommandHandler : IHandlerWrapper<UpdateMissedVisitReasonCodeInfoCommand, bool>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly IRepositoryFilterConditions _repositoryFilterConditions;
        public readonly ICaregiverTaskEvvMissedVisitsReasonsRepository  _caregiverTaskEvvMissedVisitsReasonsRepository;
        private readonly IEvvScheduleEditCodesMasterRepository _evvScheduleEditCodesMasterRepository;
        public UpdateMissedVisitReasonCodeInfoCommandHandler(IHttpContextAccessor httpContextAccessor,
                   IRepositoryFilterConditions repositoryFilterConditions, 
                   ICaregiverTaskEvvMissedVisitsReasonsRepository caregiverTaskEvvMissedVisitsReasonsRepository,
                   IEvvScheduleEditCodesMasterRepository evvScheduleEditCodesMaster
           )
        {
            _httpContextAccessor = httpContextAccessor;

            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregiverTaskEvvMissedVisitsReasonsRepository = caregiverTaskEvvMissedVisitsReasonsRepository;
            this._evvScheduleEditCodesMasterRepository = evvScheduleEditCodesMaster;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<bool>> Handle(UpdateMissedVisitReasonCodeInfoCommand request, CancellationToken cancellationToken)
        { 
            try
            {
                 

                var filters = _repositoryFilterConditions.
                                    EqualFilter("CaregiverTaskEvvMissedVisitsReasons.CaregiverTaskEvvskMissedvisitsReasonID", request.missed_visit_code_id, integerType).Build();


                var columns = "EVVVendorVersionMaserID";                                ;

                var _out_list = await _caregiverTaskEvvMissedVisitsReasonsRepository.GetMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, columns, filters);

                int EVVVendorVersionMaserID = 0;
                if (_out_list.Any())
                {
                    filters = _repositoryFilterConditions.EqualFilter("CGTASKID", request.cgtask_id, FilterConditionDataTypeEnums.integerType).Build();

                    _out_list = await _caregiverTaskEvvMissedVisitsReasonsRepository.GetMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, columns, filters);
                    if (!_out_list.Any())
                    {
                        return Response.ValidationError<bool>("Invalid CGTASKID");
                    }

                    EVVVendorVersionMaserID = _out_list.FirstOrDefault().EVVVendorVersionMaserID;
                }
                else
                    return Response.ValidationError<bool>("Invalid missed_visit_code_id"); 

                //get the EVV Codes for the Schedule Vendor Version Master ID
                var EVVMissedVisitCodeList = await _evvScheduleEditCodesMasterRepository.GetHHAEVVCodes(ActionContext.HHA, ActionContext.User, "MissedVisitReasonCode", EVVVendorVersionMaserID);
                var masterpayercodes = "";
                if (EVVMissedVisitCodeList.Any())
                {
                    if(EVVMissedVisitCodeList.Where(x => x.Code == request.ReasonCode).Any())
                        masterpayercodes = EVVMissedVisitCodeList.Where(x => x.Code == request.ReasonCode).FirstOrDefault().MasterPayerCodes;
                }

                string MissitvisitsJSON = JsonConvert.SerializeObject(new
                {
                    ReasonCode = request.ReasonCode,
                    ReasonCodeDescription = request.ReasonCodesDescription,
                    ActionCode = request.ActionCode,
                    ActionCodeDescription = request.ActionCodeDescription,
                    Notes = request.Notes,
                    MasterPayerCodes = masterpayercodes
                });

                columns = "";
                columns = new BuildUpdateQuery<MissedVisitReasonCodeinfo>().GetBuildUpdateQueryStatement(MissitvisitsJSON);

                int _outMissedvisit_list = await _caregiverTaskEvvMissedVisitsReasonsRepository.UpdateMissedVisitReasonCodes(ActionContext.HHA, ActionContext.User, filters, columns);


                if (_outMissedvisit_list == 1)
                {
                    return Response.Fail<bool>("No_Content", "There are no Records Exists", System.Net.HttpStatusCode.NoContent);

                }
                else
                {
                    return Response.Ok<bool>(true);
                }


            }
            catch (Exception ex)
            {
                throw ex;

            }

        }
    }
}

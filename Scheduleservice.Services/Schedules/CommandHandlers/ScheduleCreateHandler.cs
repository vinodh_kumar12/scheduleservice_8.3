﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.Entities;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class ScheduleCreateHandler : IHandlerWrapper< ScheduleCreateCommand, IEnumerable< string>>
    {
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IHModificationActivityLog hModificationActivityLog;
        private readonly IMapper _mapper;
        private readonly ISchedule _scheduleService;
        public ActionContextDto _actionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;



        public ScheduleCreateHandler(ICaregivertasksRepository caregivertasksRepository, 
            IHModificationActivityLog _hModificationActivityLog, IMapper mapper, ISchedule schedule,
              IHttpContextAccessor httpContextAccessor )
        {
            _caregivertasksRepository = caregivertasksRepository;
            hModificationActivityLog = _hModificationActivityLog;
            _mapper = mapper;
            _scheduleService = schedule;


            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

        }
        public async Task<Response<IEnumerable<string>>> Handle(ScheduleCreateCommand request, CancellationToken cancellationToken)
        {
            CreateScheduleDTO scheduleDTO = _mapper.Map<CreateScheduleDTO>(request);
            var ret =await _scheduleService.CreateSchedule(_actionContext.HHA, _actionContext.User,scheduleDTO);
              

            if (ret.Any())
            {
                
                List<string> result = new List<string>();
                if (ret.Any())
                {
                    foreach (var item in ret)
                    {
                        result.Add(item.validation_error_message);
                    }
                }
                    return Response.ValidationError<IEnumerable<string>>(string.Join(",", result.ToArray()));
            }
            else
            {
                IEnumerable<string> output = new List<string>() { "Success" };
                return Response.Created<IEnumerable<string>>(output);
            }

        }
     
    }
}

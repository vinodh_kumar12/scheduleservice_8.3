﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.Application;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Scheduleservice.Core.DTO.Schedule;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Extensions;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class AutoRegisterEvvTerminalPayerSchedulesCommandHandler : IHandlerWrapper<AutoRegisterEvvTerminalPayerSchedulesCommand, IEnumerable<EvvTerminalPayerSchedulesDto>>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ActionContextDto _actionContext;
        private readonly IEVVSchedule _eVVSchedule;
        private readonly IUAAgencyDatabaseMasterList _agencyDatabaseMasterList;
        private readonly ILogger<AutoRegisterEvvTerminalPayerSchedulesCommand> logger;
        private readonly IHHAConfigurationsRepository _hHAConfigurationsRepository;

        public AutoRegisterEvvTerminalPayerSchedulesCommandHandler(IHttpContextAccessor httpContextAccessor, IEVVSchedule eVVSchedule,
            IUAAgencyDatabaseMasterList agencyDatabaseMasterList, ILogger<AutoRegisterEvvTerminalPayerSchedulesCommand> _logger,
            IHHAConfigurationsRepository hHAConfigurationsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];

            _eVVSchedule = eVVSchedule;
            _agencyDatabaseMasterList = agencyDatabaseMasterList;
            this.logger = _logger;
            this._hHAConfigurationsRepository = hHAConfigurationsRepository;
        }

        public async Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> Handle(AutoRegisterEvvTerminalPayerSchedulesCommand request, CancellationToken cancellationToken)
        {
            IEnumerable<EvvTerminalPayerSchedulesDto> terminalPayerSchedules = new List<EvvTerminalPayerSchedulesDto>();
            try
            {
                if(_actionContext.HHA > 0)
                {
                    var result = await _eVVSchedule.AutoRegisterEvvTerminalPayerSchedules(_actionContext.HHA, _actionContext.User);
                    if (result.http_status_code == System.Net.HttpStatusCode.OK)
                    {
                        terminalPayerSchedules = result.response_payload;
                    }
                    else if(result.http_status_code == System.Net.HttpStatusCode.NoContent)
                    {
                        return Response.NoContent(terminalPayerSchedules);
                    }
                    else
                    {
                        return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, "Failed to register schedules");
                    }
                }
                else
                {
                    var agencyDatabaselistResult = _agencyDatabaseMasterList.GetAgencyDatabaseList();

                    foreach (var database in agencyDatabaselistResult.Select(x => x.DatabaseID).Distinct().ToList()) 
                    {
                        foreach (var agency in agencyDatabaselistResult.Where(x => x.DatabaseID == database).ToList()) 
                        {
                            _actionContext.HHA = agency.HHA_ID;
                            var result = await _eVVSchedule.AutoRegisterEvvTerminalPayerSchedules(agency.HHA_ID, 0);
                            if (result.http_status_code == System.Net.HttpStatusCode.OK)
                            {
                                terminalPayerSchedules = terminalPayerSchedules.Union(result.response_payload);
                            }
                        }
                    }                    
                }
            } 
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvTerminalPayerSchedulesDto>>(ResponseErrorCodes.InternalServerFailed, e.Message.ToString());
            }

            if (terminalPayerSchedules.Count() > 0)
            {
                return Response.Ok<IEnumerable<EvvTerminalPayerSchedulesDto>>(terminalPayerSchedules.Distinct());
            }
            else
            {
                return Response.NoContent<IEnumerable<EvvTerminalPayerSchedulesDto>>(terminalPayerSchedules);
            }
        }
    }
}

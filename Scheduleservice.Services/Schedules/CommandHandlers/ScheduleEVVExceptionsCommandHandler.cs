﻿using AutoMapper;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Query;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.QueryHandlers
{
    public class ScheduleEVVExceptionsCommandHandler : IHandlerWrapper<ScheduleEVVExceptionsCommand, bool>
    {
        public readonly IEVVException _eVVException;
        private readonly IMapper _mapper;
        

        public ScheduleEVVExceptionsCommandHandler(IEVVException eVVSchedule, IMapper mapper)
        {

            this._mapper = mapper;
            this._eVVException = eVVSchedule;
        }

        public async Task<Response<bool>> Handle(ScheduleEVVExceptionsCommand request, CancellationToken cancellationToken)
        {
            var ExceptionPayload = _mapper.Map<ScheduleEVVExceptionsCommand>(request);

            var result = await _eVVException.AutogenerateEVVEventExceptions(request.HHA, request.UserId);

            //test bit bucket merging 

            return Response.Ok<bool>(result);
        }
    }
}

﻿
namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    using AutoMapper;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.Common.Helpers;
    using Scheduleservice.Core.Common.Request;
    using Scheduleservice.Core.Common.Response;
    using Scheduleservice.Core.DTO.Application;
    using Scheduleservice.Core.DTO.EVV;
    using Scheduleservice.Core.DTO.Schedule;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.Schedules;
    using Scheduleservice.Services.Schedules.Command;
    using System;
    using System.Collections.Generic; 
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks; 
    public class ScheduleCheckinoutCommandHandler : IHandlerWrapper<ScheduleCheckinoutCommand, IEnumerable<string>>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly ISchedule _scheduleService;
        private readonly IAutoGenerateException _autoGenerateException;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository; 
        private readonly IPaymentsourcesAdditionalRepository _paymentsourcesAdditionalRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions; 
        private readonly IClientsRepository _clientsRepository;
        private readonly ICaregiverTaskEVVReasonsRepository _caregiverTaskEVVReasonsRepository;
        private readonly IEchartSignaturesRepository _echartSignaturesRepository; 
        private readonly IAuthorization _authorization;
        private readonly IGetScheduleBasicDetail _getScheduleBasicDetail;
        private readonly IMapper _mapper;

        public ScheduleCheckinoutCommandHandler(IHttpContextAccessor httpContextAccessor, ICaregivertasksRepository caregivertasksRepository, 
                        ISchedule scheduleService, IAutoGenerateException autoGenerateException,
                        IPaymentsourcesRepository paymentsourcesRepository, 
                        IPaymentsourcesAdditionalRepository paymentsourcesAdditionalRepository,
                        IRepositoryFilterConditions repositoryFilterConditions, 
                        IClientsRepository clientsRepository,
                        ICaregiverTaskEVVReasonsRepository caregiverTaskEVVReasonsRepository,
                        IEchartSignaturesRepository echartSignaturesRepository, 
                        IAuthorization authorization,
                        IGetScheduleBasicDetail getScheduleBasicDetail,
                        IMapper mapper)
        {
            _httpContextAccessor = httpContextAccessor;
            _caregivertasksRepository = caregivertasksRepository;
            this._scheduleService = scheduleService;
            this._autoGenerateException = autoGenerateException;
            this._paymentsourcesRepository = paymentsourcesRepository; 
            this._paymentsourcesAdditionalRepository = paymentsourcesAdditionalRepository;
            this._repositoryFilterConditions = repositoryFilterConditions; 
            this._clientsRepository = clientsRepository;
            this._caregiverTaskEVVReasonsRepository = caregiverTaskEVVReasonsRepository;
            this._echartSignaturesRepository = echartSignaturesRepository; 
            this._authorization = authorization;
            this._getScheduleBasicDetail = getScheduleBasicDetail;
            this._mapper = mapper;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }
        public async Task<Response<IEnumerable<string>>> Handle(ScheduleCheckinoutCommand request, CancellationToken cancellationToken)
        {
            try
            {
                //get Schedule info
                var Filters = _repositoryFilterConditions.EqualFilter("CGTASK_ID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                .Build();

                var _caregivertasksEntity = await _caregivertasksRepository.GetAllSchedulesList(ActionContext.HHA, ActionContext.User, "", Filters);
                if (_caregivertasksEntity.Count() == 0)
                {
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Invalid CgtaskID.");
                }
                var caregivertask = _caregivertasksEntity.FirstOrDefault();
                var updatedCaregiverEntityforValidation = (CaregivertasksEntity)caregivertask.Shallowcopy();

                if (caregivertask.CAREGIVER == null) 
                    return Response.ValidationError<IEnumerable<string>>("Please assign Clincian to Checkin/out.");
                

                if (caregivertask.STATUS == ScheduleStatusEnums.Approved.ToString())                
                    return Response.ValidationError<IEnumerable<string>>("Schedule is Approved, you cannot edit checkin checkout time.");
                
                //get client info
                var Clientinfo = await _clientsRepository.GetClientsBasicInfo(ActionContext.HHA, ActionContext.User, caregivertask.CLIENT_ID);

                if (string.IsNullOrEmpty(request.checkin_date))
                    request.checkin_date = caregivertask.PLANNED_DATE.ToDateFormat();
                
                updatedCaregiverEntityforValidation.PLANNED_DATE = Convert.ToDateTime(request.checkin_date);
                
                //Get Checkin date time
                DateTime CheckinDatetime = Convert.ToDateTime(request.checkin_date + " " + request.checkin_time);
                updatedCaregiverEntityforValidation.ACCTUAL_START_TIME = CheckinDatetime;

                //Get Checkin date time
                DateTime CheckOutDatetime = Convert.ToDateTime(request.checkin_date + " " + request.checkout_time);
                if (CheckOutDatetime <= CheckinDatetime)
                    CheckOutDatetime = CheckOutDatetime.AddDays(1);

                updatedCaregiverEntityforValidation.ACCTUAL_END_TIME = CheckOutDatetime;
                updatedCaregiverEntityforValidation.STATUS = ScheduleStatusEnums.Completed.ToString();

                //Edited hours -- pending for rounding
                var columns = "ROUNDOFF_Direction, ROUNDOFF_Minutes, Round_ZeroUnitScheduleToOne ";

                var payeradditionalinfo = await _paymentsourcesAdditionalRepository.GetPayerAdditionalInfo(ActionContext.HHA, ActionContext.User, caregivertask.PAYMENT_SOURCE, columns);

                if (string.IsNullOrEmpty(request.edited_hours))
                    updatedCaregiverEntityforValidation.EDITED_HOURS = Common.GetVisitEditedHours(CheckinDatetime, CheckOutDatetime, payeradditionalinfo.ROUNDOFF_Direction, payeradditionalinfo.ROUNDOFF_Minutes);
                else
                    updatedCaregiverEntityforValidation.EDITED_HOURS = request.edited_hours;

                //get units 
                updatedCaregiverEntityforValidation.Units = await _scheduleService.GetVisitUnits(ActionContext.HHA, ActionContext.User, payeradditionalinfo, caregivertask.SERVICECODE_ID, CheckinDatetime, CheckOutDatetime, Clientinfo.HHA_BRANCH_ID, request.CGTask_ID);
                //total hours 
                updatedCaregiverEntityforValidation.TotalHours = Common.GetVisitActualHours(CheckinDatetime, CheckOutDatetime);

               
                if (caregivertask.STATUS == ScheduleStatusEnums.Planned.ToString())
                {
                    updatedCaregiverEntityforValidation.CHECK_IN_LOCATION = request.caregiver_geolocation;
                    updatedCaregiverEntityforValidation.CHECK_OUT_LOCATION = request.caregiver_geolocation;
                    updatedCaregiverEntityforValidation.CheckInSource = Convert.ToByte(request.checkin_source);
                    updatedCaregiverEntityforValidation.CheckOutSource = Convert.ToByte(request.checkout_source);
                }
                else if (caregivertask.STATUS == ScheduleStatusEnums.In_Progress.ToString())
                {
                    updatedCaregiverEntityforValidation.CHECK_IN_LOCATION = caregivertask.CHECK_IN_LOCATION;
                    updatedCaregiverEntityforValidation.CHECK_OUT_LOCATION = request.caregiver_geolocation;
                    updatedCaregiverEntityforValidation.CheckOutSource = Convert.ToByte(request.checkout_source);
                }
                else
                {
                    updatedCaregiverEntityforValidation.CHECK_IN_LOCATION = caregivertask.CHECK_IN_LOCATION;
                    updatedCaregiverEntityforValidation.CHECK_OUT_LOCATION = caregivertask.CHECK_OUT_LOCATION;
                }

                updatedCaregiverEntityforValidation.CONFIRMED = true;

                ValidateScheduleRules validateScheduleRules = await _getScheduleBasicDetail.getScheduleDetails(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation);

                validateScheduleRules.confirm_softwarning = request.confirm_softwarning;

                validateScheduleRules.caregiverTaskEntity = updatedCaregiverEntityforValidation;

                //validate schedule checkin
                var ValidationResult = await _scheduleService.ValidateScheduleCheckInOutAsync(ActionContext.HHA, ActionContext.User, updatedCaregiverEntityforValidation, validateScheduleRules);
                bool has_soft_warning = false;
                bool has_validation = true;
                List<string> result = new List<string>();
                List<ValidationErrorInfo> validationErrorForConflictExceptions = new List<ValidationErrorInfo>();

                if (ValidationResult.Any())
                {                  

                    if (ValidationResult.Where(x => x.is_soft_warning == true).Any())
                    {
                        if (!ValidationResult.Where(x => x.is_soft_warning == false).Any())
                            has_soft_warning = true;
                    }

                    if ((has_soft_warning && request.confirm_softwarning)
                        ||
                        (validateScheduleRules.AllowToCheckoutifHardStopValidationExists)
                        )
                    {
                        has_validation = false;
                    }

                    if (!has_validation)
                    {
                        foreach (var item in ValidationResult)
                        {
                            //check if there is validation exists in Conflict Exceptions
                            if (Enum.IsDefined(typeof(ConflictExceptions), item.validation_type))
                                validationErrorForConflictExceptions.Add(item);
                        }

                        if (ValidationResult.Count() != validationErrorForConflictExceptions.Count())
                            has_validation = true;

                        validationErrorForConflictExceptions = validationErrorForConflictExceptions.Where(x => x.is_soft_warning == false).ToList();

                    } 
                }
                else
                    has_validation = false;

                if (has_validation)
                {
                    foreach (var item in ValidationResult)
                        result.Add(item.validation_error_message);

                    List<ScheduleValidationErrorsDto> scheduleValidationErrorsDto = new List<ScheduleValidationErrorsDto>();
                    //map
                    scheduleValidationErrorsDto = _mapper.Map<List<ScheduleValidationErrorsDto>>(ValidationResult);

                    Scheduleerrorinfo scheduleerrorinfo = new Scheduleerrorinfo();
                    scheduleerrorinfo.errors = scheduleValidationErrorsDto.ToList();
                    scheduleerrorinfo.schedule_uuid = caregivertask.Publish_UID.ToString();//_caregivertasksEntity.pub;
                    scheduleerrorinfo.schedule_id = caregivertask.CGTASK_ID;

                    string error_message = JsonConvert.SerializeObject(result);
                    string error_details = JsonConvert.SerializeObject(scheduleerrorinfo);

                    if (has_soft_warning)
                        return Response.SoftWarningError<IEnumerable<string>>(error_message, error_details);

                    return Response.ValidationError<IEnumerable<string>>(error_message, default, error_details);
                } 

                //Split schedule level flags , if it is overnight based on the HHA/ payer setting
                var hHAScheduleAuthSettingsEntity = validateScheduleRules.hHAScheduleAuthSettingsEntities;

                if (hHAScheduleAuthSettingsEntity.FirstOrDefault().SplitOvernightSchedulesAtCreationlevel)
                {
                    //get payer level setting
                    var payer = await _paymentsourcesRepository.GetPaymentSourceBasicInfo(ActionContext.HHA, ActionContext.User, caregivertask.PAYMENT_SOURCE);

                    if (!caregivertask.Has_Child_Schedules?? false && payer.SplitOvernightSchedules)
                    {
                        caregivertask.Has_Child_Schedules = payer.SplitOvernightSchedules;
                        caregivertask.IsSplitForBilling = caregivertask.Has_Child_Schedules;
                    }
                }
                
                //Get Fields to Update on Checkin
                EditScheduleProperties editScheduleProperties = GetCheckInOutModel(ActionContext.HHA, ActionContext.User,request, caregivertask, payeradditionalinfo, Clientinfo,validateScheduleRules.RequireGeoLocationforFOB);

                //Update Checkin Info
                var ret = await _scheduleService.ModifyScheduleAsync(ActionContext.HHA, ActionContext.User, request.CGTask_ID, editScheduleProperties, caregivertask, !has_validation);

                if (ret < 0)
                {
                    return Response.ValidationError<IEnumerable<string>>("Authorization is not sufficient to split the schedule.");
                }

                if (ret == 0
                       && ((caregivertask.Units != editScheduleProperties.Units)
                           ||
                           (caregivertask.CanBill != editScheduleProperties.CanBill)
                           ||
                           (caregivertask.TotalHours != editScheduleProperties.TotalHours)
                           ||
                           (caregivertask.IS_AUTH_MANDATORY != editScheduleProperties.IS_AUTH_MANDATORY)
                          )
                   )
                {
                    //Update Auth info
                    var result_Authinfo = await _authorization.UpdateAuthorizationInfo(ActionContext.HHA, ActionContext.User, editScheduleProperties, caregivertask);

                    if (!result_Authinfo)
                        ret = 1;
                }

                int checkinResolvedReasoncodeID = 0;
                int EVVVendorVersionMasterID = 0;

                //EVVVendorVersionMasterID = await _scheduleService.GetScheduleEVVVendorVersionMasterID(ActionContext.HHA, ActionContext.User, caregivertask, Clientinfo.HHA_BRANCH_ID, false);

                EVVVendorVersionMasterID= validateScheduleRules.caregiverTaskEntity.EvvVendorVersionMasterID;
                if (caregivertask.isEvvschedule)
                {
                    
                    if (ret == 0 && !string.IsNullOrEmpty(request.checkin_reason_code) && EVVVendorVersionMasterID > 0)
                    {
                        string checkinReasonCodesJSON = JsonConvert.SerializeObject(new
                        {
                            CaregiverTaskEvvReasonID = 0,
                            Code = request.checkin_reason_code,
                            Description = request.checkin_reason_desc,
                            Notes = request.checkin_reason_notes,
                            CheckinCheckoutType = "CheckIn",
                            EVVVendorVersionMasterId = EVVVendorVersionMasterID,
                            ReasonContext = request.checkin_reason_context
                        });

                        string checkinActionCodesJSON = "";
                        if (!string.IsNullOrEmpty(request.checkin_action_code))
                        {
                            checkinActionCodesJSON = JsonConvert.SerializeObject(new
                            {
                                ActionCode = request.checkin_action_code,
                                ActionDescription = request.checkin_action_desc,
                                ActionNotes = request.checkin_reason_notes,
                                CaregiverTaskEvvReasonID = 0
                            });
                        }

                        //implementation pending for reason code addition
                        checkinResolvedReasoncodeID = await _caregiverTaskEVVReasonsRepository.AddCaregiverTaskEVVReasons(ActionContext.HHA, ActionContext.User, request.CGTask_ID, checkinReasonCodesJSON, checkinActionCodesJSON); ;
                    }

                    int checkoutResolvedReasoncodeID = 0;
                    if (ret == 0 && !string.IsNullOrEmpty(request.checkout_reason_code) && EVVVendorVersionMasterID > 0)
                    {
                        string checkoutReasonCodesJSON = JsonConvert.SerializeObject(new
                        {
                            CaregiverTaskEvvReasonID = 0,
                            Code = request.checkout_reason_code,
                            Description = request.checkout_reason_desc,
                            Notes = request.checkout_reason_notes,
                            CheckinCheckoutType = "CheckOut",
                            EVVVendorVersionMasterId = EVVVendorVersionMasterID,
                            ReasonContext = request.checkout_reason_context
                        });

                        string checkoutActionCodesJSON = "";
                        if (!string.IsNullOrEmpty(request.checkout_action_code))
                        {
                            checkoutActionCodesJSON = JsonConvert.SerializeObject(new
                            {
                                ActionCode = request.checkout_action_code,
                                ActionDescription = request.checkout_action_desc,
                                ActionNotes = request.checkout_reason_notes,
                                CaregiverTaskEvvReasonID = 0
                            });
                        }

                        //implementation pending for reason code addition
                        checkoutResolvedReasoncodeID = await _caregiverTaskEVVReasonsRepository.AddCaregiverTaskEVVReasons(ActionContext.HHA, ActionContext.User, request.CGTask_ID, checkoutReasonCodesJSON, checkoutActionCodesJSON); ;
                         
                    }

                    EVVExceptioninfoDto eVVExceptioninfoDto = new EVVExceptioninfoDto();
                     
                    if (EVVVendorVersionMasterID > 0)
                    {

                        if (ret == 0 && !string.IsNullOrEmpty(request.checkin_exception_code))
                        {
                            eVVExceptioninfoDto.Code = request.checkin_exception_code;
                            eVVExceptioninfoDto.Description = request.checkin_exception_desc;
                            eVVExceptioninfoDto.Context = "CheckIn";
                            eVVExceptioninfoDto.ResolvedReasoncodeID = checkinResolvedReasoncodeID;

                            //find evvvendorveriosnmasterid
                            eVVExceptioninfoDto.EVVVendorVersionMasterID = EVVVendorVersionMasterID;

                            //find 
                            eVVExceptioninfoDto.Event = "";
                            var issuccess = await _autoGenerateException.GenerateException(ActionContext.HHA, ActionContext.User, request.CGTask_ID, eVVExceptioninfoDto);

                            if (!issuccess)
                                ret = 1;
                        } 
                    }

                    if (ret == 0 && !string.IsNullOrEmpty(request.checkout_exception_code) && EVVVendorVersionMasterID > 0)
                    {
                        eVVExceptioninfoDto.Code = request.checkout_exception_code;
                        eVVExceptioninfoDto.Description = request.checkout_exception_desc;
                        eVVExceptioninfoDto.Context = "CheckOut";
                        //find evvvendorveriosnmasterid
                        eVVExceptioninfoDto.EVVVendorVersionMasterID = EVVVendorVersionMasterID;
                        eVVExceptioninfoDto.ResolvedReasoncodeID = checkoutResolvedReasoncodeID;

                        //find 
                        eVVExceptioninfoDto.Event = "";
                        var issuccess = await _autoGenerateException.GenerateException(ActionContext.HHA, ActionContext.User, request.CGTask_ID, eVVExceptioninfoDto);

                        if (!issuccess)
                            ret = 1;
                    }

                    if (EVVVendorVersionMasterID > 0 && string.IsNullOrEmpty(request.checkin_exception_code)
                        && string.IsNullOrEmpty(request.checkout_exception_code))
                    {
                        var signaturefilters = _repositoryFilterConditions.EqualFilter("CgTaskID", request.CGTask_ID, FilterConditionDataTypeEnums.integerType)
                                                .InFilter("CONTEXT", "eChart,OasisDatasets", FilterConditionDataTypeEnums.stringType)
                                                .Build();

                        var chartinfo = await _echartSignaturesRepository.GetScheduleSignatureInfo(ActionContext.HHA, ActionContext.User, signaturefilters, "SIGNATURE_ID");

                        ScheduleEVVInfo scheduleEVVInfo = new ScheduleEVVInfo()
                        {
                            ACCTUAL_START_TIME = editScheduleProperties.ACCTUAL_START_TIME,
                            EvvCheckinLocationVerified = editScheduleProperties.EvvCheckinLocationVerified,
                            HasCheckInGeoLocation = !string.IsNullOrEmpty(editScheduleProperties.CHECK_IN_LOCATION) ? true : false,
                            ACCTUAL_END_TIME = editScheduleProperties.ACCTUAL_END_TIME,
                            EVVCheckOutLocationVerified = editScheduleProperties.EvvCheckoutLocationVerified,
                            HasCheckOutgeoLocation = !string.IsNullOrEmpty(editScheduleProperties.CHECK_OUT_LOCATION) ? true : false,
                            HasClientSign = chartinfo?.SIGNATURE_ID > 0 ? true : false,
                            HasClientVoiceRecording = !string.IsNullOrEmpty(caregivertask.EvvCheckOutVoicePath) ? true : false
                        };

                        var result_evvexception = await _autoGenerateException.RaiseEVVExceptionsOnCheckinCheckout(ActionContext.HHA, ActionContext.User, caregivertask, scheduleEVVInfo,
                                    EVVVendorVersionMasterID, checkinResolvedReasoncodeID, checkoutResolvedReasoncodeID, !request.is_clincian_checkin_checkout);

                        if (!result_evvexception)
                            ret = 1;
                    }
                }

                if (ret == 0 && validateScheduleRules.AllowToCheckoutifHardStopValidationExists && validationErrorForConflictExceptions.Any())
                {
                    //Generate conflict Exceptions
                    _ = await _scheduleService.GenerateConflictExceptions(ActionContext.HHA, ActionContext.User, validationErrorForConflictExceptions, caregivertask.CGTASK_ID);

                }

                if (ret == 0)
                {
                    IEnumerable<string> output = new List<string>() { "Success" };
                    return Response.Ok<IEnumerable<string>>(output);
                }
                else
                    return Response.Fail<IEnumerable<string>>("INTERNAL_EXCEPTION", "Failed to CheckOut schedule");
            }
            catch (Exception)
            {
                throw;
            }
        }

        private EditScheduleProperties GetCheckInOutModel(int HHA, int User, ScheduleCheckinoutCommand request, CaregivertasksEntity caregivertasksEntity, PaymentSourcesAdditionalEntity paymentSourcesAdditionalEntity, ClientsEntity clientsEntity,bool RequireGeoLocationforFOB)
        {
            EditScheduleProperties editScheduleProperties = new EditScheduleProperties();

            //Get Checkin date time            
            editScheduleProperties.ACCTUAL_START_TIME = Convert.ToDateTime(request.checkin_date + " " + request.checkin_time); 
            editScheduleProperties.PLANNED_DATE = Convert.ToDateTime(request.checkin_date);
            editScheduleProperties.ACCTUAL_DATE = Convert.ToDateTime(request.checkin_date);

            //Get Checkin date time
            DateTime CheckOutDatetime = Convert.ToDateTime(request.checkin_date + " " + request.checkout_time);
            if (CheckOutDatetime <= editScheduleProperties.ACCTUAL_START_TIME)
                CheckOutDatetime = CheckOutDatetime.AddDays(1);

            editScheduleProperties.ACCTUAL_END_TIME = CheckOutDatetime;
             
            editScheduleProperties.EDITED_HOURS = request.edited_hours;             
            editScheduleProperties.STATUS = ScheduleStatusEnums.Completed.ToString();
            editScheduleProperties.CheckInSource = Convert.ToByte(request.checkin_source);
            editScheduleProperties.CheckinTreatmentLocation = request.checkin_treatment_adress_id == 0? null: request.checkin_treatment_adress_id;
            editScheduleProperties.CheckoutTreatmentLocation = request.checkout_treatment_adress_id == 0? null: request.checkout_treatment_adress_id;
            editScheduleProperties.CHECK_IN_LOCATION = request.caregiver_geolocation;
            editScheduleProperties.EDITED_HOURS_PAYABLE = request.payable_hours;
            editScheduleProperties.CheckOutSource = Convert.ToByte(request.checkout_source);          
            editScheduleProperties.CHECK_OUT_LOCATION = request.caregiver_geolocation;          

            //fob code 
            editScheduleProperties.fob_checkin_code = request.checkin_fob_code;
            editScheduleProperties.fob_checkout_code = request.checkout_fob_code;
            editScheduleProperties.isAttested = request.is_attested;

            //fob devicde
            editScheduleProperties.fob_checkin_device_id = request.fob_checkin_device_id;
            editScheduleProperties.fob_checkout_device_id = request.fob_checkout_device_id;

            EditScheduleProperties updatededitScheduleProperties = _scheduleService.GetEditSchedulePropertiesModel(HHA, User, editScheduleProperties, caregivertasksEntity, clientsEntity, RequireGeoLocationforFOB).Result;

            return updatededitScheduleProperties;
        } 

    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.Services.Schedule;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class RecalculateScheduleExceptionsinitCommandHandler : IHandlerWrapper<RecalculateScheduleExceptionsinitCommand, IEnumerable<RecalculateScheduleExceptionsinitDto>>
    {
        private readonly IValidationConflictExceptions _validationConflictExceptions;

        public RecalculateScheduleExceptionsinitCommandHandler(IValidationConflictExceptions validationConflictExceptions)
        {
            _validationConflictExceptions = validationConflictExceptions;
        }
        public async Task<Response<IEnumerable<RecalculateScheduleExceptionsinitDto>>> Handle(RecalculateScheduleExceptionsinitCommand request, CancellationToken cancellationToken)
        {
            IEnumerable<RecalculateScheduleExceptionsinitDto> validationConflictsDTO = new List<RecalculateScheduleExceptionsinitDto>();

            validationConflictsDTO = await _validationConflictExceptions.GetValidationConflictRecalculate_init(request.HHA, request.UserId, request.Fiters_JSON);
            return Response.Ok<IEnumerable<RecalculateScheduleExceptionsinitDto>>(validationConflictsDTO);
        }
    }
}

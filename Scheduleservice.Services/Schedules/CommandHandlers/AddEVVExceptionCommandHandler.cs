﻿using Newtonsoft.Json;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Command;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.EVV;
using System.Linq;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class AddEVVExceptionCommandHandler : IHandlerWrapper<AddEVVExceptionCommand, bool>
    { 
        private IAutoGenerateException _autoGenerateException;
        
        public AddEVVExceptionCommandHandler(IEvvScheduleEditCodesMasterRepository evvScheduleEditCodesMaster,
            IAutoGenerateException autoGenerateException, IHHAEvvExceptionEventsRepository i_C_EvvExceptionEventsRepository,
            IUAEvvExceptionEventsRepository i_UA_EvvExceptionEventsRepository)
        { 
            _autoGenerateException = autoGenerateException; 
        }

        public async Task<Response<bool>> Handle(AddEVVExceptionCommand request, CancellationToken cancellationToken)
        {
            bool ret = false;
            //Deserialise the string
            var ExceptionInfo = JsonConvert.DeserializeObject<List<EVVExceptioninfoDto>>(request.EVVExceptionInfo_JSON);

            if (ExceptionInfo.Any())
            { 
                foreach (var item in ExceptionInfo)
                {
                   ret = await _autoGenerateException.GenerateException(request.HHA, request.UserId, request.CgTaskID, item);
                } 
            } 
            return Response.Ok<bool>(ret);
        }  
    }
}

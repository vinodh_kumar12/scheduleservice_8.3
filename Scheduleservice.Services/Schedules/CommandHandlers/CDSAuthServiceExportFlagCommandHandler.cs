﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Data.Repository;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class CDSAuthServiceExportFlagCommandHandler : IHandlerWrapper<CDSAuthServiceExportFlagCommand, bool>
    {
        public ActionContextDto ActionContext { get; }
        private readonly IUAAgencyDatabaseMasterList _agencyDatabaseMasterList;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHHAClinicalRepository _hHAClinicalRepository;
        private readonly ICDSPlanYearServicesRepository _cDSPlanYearServicesRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ILogger<CDSAuthServiceExportFlagCommand> _logger;

        public CDSAuthServiceExportFlagCommandHandler(IUAAgencyDatabaseMasterList agencyDatabaseMasterList,
            IHttpContextAccessor httpContextAccessor, IHHAClinicalRepository hHAClinicalRepository,
            ICDSPlanYearServicesRepository cDSPlanYearServicesRepository,
            IUsersRepository usersRepository, IRepositoryFilterConditions repositoryFilterConditions,
            ILogger<CDSAuthServiceExportFlagCommand> logger)
        {
            this._agencyDatabaseMasterList = agencyDatabaseMasterList;
            this._httpContextAccessor = httpContextAccessor;
            this._hHAClinicalRepository = hHAClinicalRepository;
            this._cDSPlanYearServicesRepository = cDSPlanYearServicesRepository;
            this._usersRepository = usersRepository;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._logger = logger;

            if (_httpContextAccessor.HttpContext != null)
                ActionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<bool>> Handle(CDSAuthServiceExportFlagCommand request, CancellationToken cancellationToken)
        {
             
            bool ret = true;// await _caregiverTaskEvvExceptionsRepository.UpdateCaregiverTaskEVVException(request.HHA, request.UserId, request.CaregiverTaskEvvExceptionIDs, updatedColumns);
            int HHA_SupportUserID = 0;

            try
            {
                var agencyDatabaselistResult = _agencyDatabaseMasterList.GetAgencyDatabaseList();
                if (agencyDatabaselistResult.Count() > 0 && ActionContext.HHA > 0)
                {
                    agencyDatabaselistResult = agencyDatabaselistResult.Where(x => x.HHA_ID == ActionContext.HHA).ToList();

                }

                foreach (var HHADetails in agencyDatabaselistResult)
                {
                    ActionContext.HHA = HHADetails.HHA_ID;
                    ActionContext.DbServer = HHADetails.Server;
                    HHA_SupportUserID = 0;

                    var hhaClinicaldetail = await _hHAClinicalRepository._S_Schedule_GetHHAClinicalSettings(ActionContext.HHA, 0, "enableCDS", "");
                    if (hhaClinicaldetail.enableCDS == true)
                    {
                        if (ActionContext.User == 0)
                        {
                            var userinfo = await _usersRepository.GetHHAUsers(ActionContext.HHA, " Top 1 User_ID ", " and ROLE_TYPE = -1 ");

                            if (userinfo.Count() > 0)
                                HHA_SupportUserID = userinfo.Select(x => x.USER_ID).Single();
                        }
                        else
                            HHA_SupportUserID = ActionContext.User;

                        var filters = _repositoryFilterConditions.EqualFilter("CDSPlanYearServices.isDefault ", true, FilterConditionDataTypeEnums.boolType)
                                                .EqualFilter("CDSPlanYearServices.IsEnableEVV ", true, FilterConditionDataTypeEnums.boolType)
                                                .EqualFilter("CDSPlanYearServices.IsEvvDirty ", false,FilterConditionDataTypeEnums.boolType )
                                                .EqualFilter("ISNULL(CDSPlanYearServiceDetails.effectiveStart,CDSPlanYearMaster.StartDate) ", DateTime.Now, FilterConditionDataTypeEnums.dateType)
                                                .Build();

                        var columstoUpdate = " IsEvvDirty = 1 ";

                        var result = await _cDSPlanYearServicesRepository.UpdateCDSPlanYearService_bulk(ActionContext.HHA, HHA_SupportUserID, columstoUpdate, filters);

                        if (result != 0) 
                            ret = false;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                throw ex;
            }

            if (ret)
                return Response.Ok<bool>(ret);
            else
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "failed to Update!");
        }
    }
}

﻿using Newtonsoft.Json;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Newtonsoft.Json.Linq;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class SaveScheduleinfoCommandHandler : IHandlerWrapper<SaveScheduleinfoCommand, bool>
    {
        public readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ILogger<SaveScheduleinfoCommandHandler> _logger;
        private readonly ISchedule _schedule;

        public SaveScheduleinfoCommandHandler(ICaregivertasksRepository caregivertasksRepository,
            IRepositoryFilterConditions repositoryFilterConditions,
            ILogger<SaveScheduleinfoCommandHandler> logger,
            ISchedule schedule
        )
        {
            _caregivertasksRepository = caregivertasksRepository;
            _repositoryFilterConditions = repositoryFilterConditions;
            _logger = logger;
            _schedule = schedule;
        }
        public async Task<Response<bool>> Handle(SaveScheduleinfoCommand request, CancellationToken cancellationToken)
        {
            bool ret = false;
            IEnumerable<string> errorList = new List<string>();

            try
            {
                JToken.Parse(request.ModifiedFields);
            }
            catch (Exception)
            {
                return Response.ValidationError("ModifiedFields is not a valid json data", ret);
            }

            try
            {
                // Get Modified Fields
                EditScheduleProperties modifiedScheduleData = JsonConvert.DeserializeObject<EditScheduleProperties>(request.ModifiedFields);
                modifiedScheduleData.CGTASK_ID = request.CgtaskID;

                // Get old schedule data
                string columns = "*";
                string filters = _repositoryFilterConditions.EqualFilter("Cgtask_ID", modifiedScheduleData.CGTASK_ID, FilterConditionDataTypeEnums.integerType).Build();
                var caregivertasksEntities = await _caregivertasksRepository.GetAllSchedulesList(request.HHA, request.UserId, columns, filters);
                if (!caregivertasksEntities.Any())
                {
                    _logger.LogError("Not found any schedule for the filters: " + filters);
                    return Response.ValidationError("Schedule is not valid", true);
                }
                CaregivertasksEntity oldScheduleData = caregivertasksEntities.FirstOrDefault();
                CaregivertasksEntity newScheduleData = (CaregivertasksEntity)oldScheduleData.Shallowcopy();

                // Update unmodified data with old data
                #region update old data with modified data
                if (modifiedScheduleData.CAREGIVER.HasValue)
                    newScheduleData.CAREGIVER = modifiedScheduleData.CAREGIVER;

                if (modifiedScheduleData.PLANNED_DATE.HasValue)
                {
                    newScheduleData.PLANNED_DATE = modifiedScheduleData.PLANNED_DATE.Value;
                    newScheduleData.ACCTUAL_DATE = modifiedScheduleData.PLANNED_DATE;
                }

                if (!modifiedScheduleData.PLANNED_START_TIME.HasValue)
                    newScheduleData.PLANNED_START_TIME = newScheduleData.PLANNED_DATE.Date.Add(newScheduleData.PLANNED_START_TIME.TimeOfDay);
                else
                    newScheduleData.PLANNED_START_TIME = newScheduleData.PLANNED_DATE.Date.Add(modifiedScheduleData.PLANNED_START_TIME.Value.TimeOfDay);

                if (!modifiedScheduleData.PLANNED_END_TIME.HasValue)
                    newScheduleData.PLANNED_END_TIME = newScheduleData.PLANNED_DATE.Date.Add(newScheduleData.PLANNED_END_TIME.TimeOfDay);
                else
                    newScheduleData.PLANNED_END_TIME = newScheduleData.PLANNED_DATE.Date.Add(modifiedScheduleData.PLANNED_END_TIME.Value.TimeOfDay);

                if (!modifiedScheduleData.ACCTUAL_START_TIME.HasValue && oldScheduleData.ACCTUAL_START_TIME.HasValue)
                    newScheduleData.ACCTUAL_START_TIME = newScheduleData.PLANNED_DATE.Date.Add(oldScheduleData.ACCTUAL_START_TIME.Value.TimeOfDay);
                else if (modifiedScheduleData.ACCTUAL_START_TIME.HasValue)
                    newScheduleData.ACCTUAL_START_TIME = newScheduleData.PLANNED_DATE.Date.Add(modifiedScheduleData.ACCTUAL_START_TIME.Value.TimeOfDay);

                if (!modifiedScheduleData.ACCTUAL_END_TIME.HasValue && oldScheduleData.ACCTUAL_END_TIME.HasValue)
                    newScheduleData.ACCTUAL_END_TIME = newScheduleData.PLANNED_DATE.Date.Add(oldScheduleData.ACCTUAL_END_TIME.Value.TimeOfDay);
                else if (modifiedScheduleData.ACCTUAL_END_TIME.HasValue)
                    newScheduleData.ACCTUAL_END_TIME = newScheduleData.PLANNED_DATE.Date.Add(newScheduleData.ACCTUAL_END_TIME.Value.TimeOfDay);

                if (!String.IsNullOrEmpty(modifiedScheduleData.STATUS))
                    newScheduleData.STATUS = modifiedScheduleData.STATUS;

                if (modifiedScheduleData.PLAN_ENTRY.HasValue)
                    newScheduleData.PLAN_ENTRY = modifiedScheduleData.PLAN_ENTRY.Value;

                if (modifiedScheduleData.CONFIRMED.HasValue)
                    newScheduleData.CONFIRMED = modifiedScheduleData.CONFIRMED;

                if (!String.IsNullOrEmpty(modifiedScheduleData.EDITED_HOURS))
                    newScheduleData.EDITED_HOURS = modifiedScheduleData.EDITED_HOURS;                    

                if (modifiedScheduleData.MILES.HasValue)
                    newScheduleData.MILES = modifiedScheduleData.MILES;

                if (modifiedScheduleData.SERVICECODE_ID.HasValue)
                    newScheduleData.SERVICECODE_ID = modifiedScheduleData.SERVICECODE_ID.Value;

                if (modifiedScheduleData.CLIENT_ID.HasValue)
                    newScheduleData.CLIENT_ID = modifiedScheduleData.CLIENT_ID.Value;

                if (modifiedScheduleData.AUTHORIZATION_ID.HasValue)
                    newScheduleData.AUTHORIZATION_ID = modifiedScheduleData.AUTHORIZATION_ID;

                if (!String.IsNullOrEmpty(modifiedScheduleData.EDITED_HOURS_PAYABLE))
                    newScheduleData.EDITED_HOURS_PAYABLE = modifiedScheduleData.EDITED_HOURS_PAYABLE;

                if (modifiedScheduleData.PAYRATE.HasValue)
                    newScheduleData.PAYRATE = modifiedScheduleData.PAYRATE;

                if (modifiedScheduleData.IS_PAYRATE_HOURLY.HasValue)
                    newScheduleData.IS_PAYRATE_HOURLY = modifiedScheduleData.IS_PAYRATE_HOURLY;

                if (modifiedScheduleData.IS_PAYABLE.HasValue)
                    newScheduleData.IS_PAYABLE = modifiedScheduleData.IS_PAYABLE;

                if (!String.IsNullOrEmpty(modifiedScheduleData.ADDITIONAL_NOTE))
                    newScheduleData.ADDITIONAL_NOTE = modifiedScheduleData.ADDITIONAL_NOTE;

                if (modifiedScheduleData.PAYMENT_SOURCE.HasValue)
                    newScheduleData.PAYMENT_SOURCE = modifiedScheduleData.PAYMENT_SOURCE.Value;

                if (modifiedScheduleData.IS_AUTH_MANDATORY.HasValue)
                    newScheduleData.IS_AUTH_MANDATORY = modifiedScheduleData.IS_AUTH_MANDATORY;

                if (modifiedScheduleData.BillRate.HasValue)
                    newScheduleData.BillRate = modifiedScheduleData.BillRate;

                if (modifiedScheduleData.IS_BILLRATE_HOURLY.HasValue)
                    newScheduleData.IS_BILLRATE_HOURLY = modifiedScheduleData.IS_BILLRATE_HOURLY;

                if (modifiedScheduleData.Chart_Type.HasValue)
                    newScheduleData.Chart_Type = modifiedScheduleData.Chart_Type;

                if (modifiedScheduleData.enablePayableHours.HasValue)
                    newScheduleData.enablePayableHours = modifiedScheduleData.enablePayableHours;

                if (modifiedScheduleData.AuthorizedHours.HasValue)
                    newScheduleData.AuthorizedHours = modifiedScheduleData.AuthorizedHours;

                if (modifiedScheduleData.TotalBreakHours.HasValue)
                    newScheduleData.TotalBreakHours = modifiedScheduleData.TotalBreakHours;

                if (modifiedScheduleData.isOverridePayrate.HasValue)
                    newScheduleData.isOverridePayrate = modifiedScheduleData.isOverridePayrate;

                if (modifiedScheduleData.isOverridePayMode.HasValue)
                    newScheduleData.isOverridePayMode = modifiedScheduleData.isOverridePayMode;

                if (modifiedScheduleData.Units.HasValue)
                    newScheduleData.Units = modifiedScheduleData.Units;

                if (!String.IsNullOrEmpty(modifiedScheduleData.NotesToClinician))
                    newScheduleData.NotesToClinician = modifiedScheduleData.NotesToClinician;

                if (modifiedScheduleData.ApprovedForPayroll.HasValue)
                    newScheduleData.ApprovedForPayroll = modifiedScheduleData.ApprovedForPayroll;

                if (modifiedScheduleData.ApprovedForBonusPayRate.HasValue)
                    newScheduleData.ApprovedForBonusPayRate = modifiedScheduleData.ApprovedForBonusPayRate;

                if (modifiedScheduleData.BonusAmount.HasValue)
                    newScheduleData.BonusAmount = modifiedScheduleData.BonusAmount;

                if (modifiedScheduleData.ShiftID.HasValue)
                    newScheduleData.ShiftID = modifiedScheduleData.ShiftID;

                if (modifiedScheduleData.CoSignStaff.HasValue)
                    newScheduleData.CoSignStaff = modifiedScheduleData.CoSignStaff;

                if (modifiedScheduleData.RequireCoSign.HasValue)
                    newScheduleData.RequireCoSign = modifiedScheduleData.RequireCoSign;

                if (modifiedScheduleData.travelTimeHours.HasValue)
                    newScheduleData.travelTimeHours = modifiedScheduleData.travelTimeHours;

                if (modifiedScheduleData.documentationTimeMins.HasValue)
                    newScheduleData.documentationTimeMins = modifiedScheduleData.documentationTimeMins;

                if (modifiedScheduleData.CdsPlanYear.HasValue)
                    newScheduleData.CdsPlanYear = modifiedScheduleData.CdsPlanYear;

                if (modifiedScheduleData.CdsPlanYearService.HasValue)
                    newScheduleData.CdsPlanYearService = modifiedScheduleData.CdsPlanYearService;

                if (modifiedScheduleData.CDSClientBudgetLineItemID.HasValue)
                    newScheduleData.CDSClientBudgetLineItemID = modifiedScheduleData.CDSClientBudgetLineItemID;

                if(modifiedScheduleData.CanBill.HasValue)
                    newScheduleData.CanBill = modifiedScheduleData.CanBill.Value;
                #endregion

                var response = await _schedule.ValidateModifyScheduleProperties(request.HHA, request.UserId, oldScheduleData, newScheduleData);

                if(response.http_status_code == System.Net.HttpStatusCode.NoContent)
                {
                    return Response.NoContent(true);
                }
                else if(response.http_status_code == System.Net.HttpStatusCode.InternalServerError)
                {
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, response.error_message);
                }
                else if(response.http_status_code == System.Net.HttpStatusCode.BadRequest)
                {
                    if (!string.IsNullOrEmpty(response.error_message))
                    {
                        return Response.ValidationError(response.error_message, ret);
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
            }

            return Response.Ok(ret);
        }   
    }
}

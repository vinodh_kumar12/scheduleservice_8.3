﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class UpdateEVVExceptionCommandHandler : IHandlerWrapper<UpdateEVVExceptionCommand, bool>
    {
        private readonly ICaregiverTaskEVVExceptionsRepository _caregiverTaskEvvExceptionsRepository;
        public UpdateEVVExceptionCommandHandler(ICaregiverTaskEVVExceptionsRepository caregiverTaskEvvExceptionsRepository)
        {
            _caregiverTaskEvvExceptionsRepository = caregiverTaskEvvExceptionsRepository;
        }
        public async Task<Response<bool>> Handle(UpdateEVVExceptionCommand request, CancellationToken cancellationToken)
        {
            string updatedColumns = new BuildUpdateQuery<UpdateCaregiverTaskEVVException>().GetBuildUpdateQueryStatement(request.EVVExceptionInfo_JSON);

            bool ret = await _caregiverTaskEvvExceptionsRepository.UpdateCaregiverTaskEVVException(request.HHA, request.UserId, request.CaregiverTaskEvvExceptionIDs, updatedColumns);
            if (ret)
                return Response.Ok<bool>(ret);
            else
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "failed to udpate!");
        }
    }
}

﻿using AutoMapper;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Command; 
using System; 
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Services.Schedules.Service;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using Microsoft.Extensions.Logging;
namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class ClearEVVExceptionsCommandHandler : IHandlerWrapper<ClearEVVExceptionsCommand, bool>
    {
        private readonly IMapper mapper;
        private readonly ICaregiverTaskEVVExceptionsRepository caregiverTaskEVVExceptionsRepository;
        private readonly IEVVException eVVException;
        private readonly ILogger<ClearEVVExceptionsCommandHandler> logger;
        public ClearEVVExceptionsCommandHandler(IMapper _mapper, ICaregiverTaskEVVExceptionsRepository _caregiverTaskEVVExceptionsRepository, IEVVException _eVVException,
            ILogger<ClearEVVExceptionsCommandHandler> _logger)
        {

            this.mapper = _mapper;
            this.caregiverTaskEVVExceptionsRepository = _caregiverTaskEVVExceptionsRepository;
            this.eVVException = _eVVException;
            this.logger = _logger;
        }
        public async Task<Response<bool>> Handle(ClearEVVExceptionsCommand request, CancellationToken cancellationToken)
        {
            bool result = false;
            try
            {
                string TypeOfOperation = request.TypeOfOperation;
                string SystemCodes = request.SystemCode;
                result = await this.eVVException.ClearScheduleEVVExceptions(request.HHA, request.UserId, request.CGTASKID, TypeOfOperation, SystemCodes);
                
            }
            catch (Exception e)
            {
                result = false;
                logger.LogError(e,e.Message);
            }
            if (result)
                return Response.Ok(true);
            else
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to Clear Exception, Please contact KanTime support!");
        }
    }
}

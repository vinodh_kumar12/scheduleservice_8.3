﻿using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Schedules.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class DeleteVisitTerminalPayersCommandHandler : IHandlerWrapper<DeleteVisitTerminalPayersCommand, bool>
    {
        public ActionContextDto _actionContext { get; }
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICaregiverTasksTerminalPayerRepository _caregiverTasksTerminalPayerRepository;
        private readonly ICaregiverTaskChildSchedulesRepository _caregiverTaskChildSchedulesRepository;

        public DeleteVisitTerminalPayersCommandHandler(IRepositoryFilterConditions repositoryFilterConditions,
            ICaregiverTasksTerminalPayerRepository caregiverTasksTerminalPayerRepository,
            ICaregiverTaskChildSchedulesRepository caregiverTaskChildSchedulesRepository,
            IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._caregiverTasksTerminalPayerRepository = caregiverTasksTerminalPayerRepository;
            this._caregiverTaskChildSchedulesRepository = caregiverTaskChildSchedulesRepository;

            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<bool>> Handle(DeleteVisitTerminalPayersCommand request, CancellationToken cancellationToken)
        {
            bool ret = false;

            if (request.CgtaskIDs.Length == 0 && request.ChildCgtaskIDs.Length == 0)
                return Response.Fail<bool>(ResponseErrorCodes.InvalidInput, "Please provide valid Input.");

            //delete only parent visit terinal payers
            if (request.CgtaskIDs.Length > 0)
            {
                
                var Filters = _repositoryFilterConditions.InFilter("CaregiverTasksTerminalPayer.CgTaskID", string.Join(",", request.CgtaskIDs), FilterConditionDataTypeEnums.integerType)
                                                      .EqualFilter("CaregiverTasksTerminalPayer.isDeleted", false, FilterConditionDataTypeEnums.boolType)
                                                     .Build(); 

                ret = await this.DeleteParentVisitTerminalPayers(Filters); 

            }
            
            
            if(request.ChildCgtaskIDs.Length > 0)
                ret = await this.DeleteChildVisitTerminalPayers(new int[] { }, request.ChildCgtaskIDs);

             
            return Response.Ok<bool>(ret);
        }

        public async Task<bool> DeleteParentVisitTerminalPayers(string Filters)
        {
            bool ret = true;
            var Columns = "isEvvschedule,CaregiverTasksTerminalPayer.CaregiverTasksTerminalPayerID,CaregiverTasks.CanBill,CaregiverTasks.Status, CaregiverTasksTerminalPayer.PrimaryPayerID, ClientPaymentSources.CLIENT_PAYMENT_ID";

            var terminalParentSchedules = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerSchedules(_actionContext.HHA, _actionContext.User, Columns, Filters);

            if (terminalParentSchedules?.Count() > 0)
            {
                //get parent terminal Payers which are not evv
                terminalParentSchedules = terminalParentSchedules.Where(x => x.isEvvschedule == false || x.CanBill == false || x.STATUS == ScheduleStatusEnums.Deleted.ToString() || x.STATUS == ScheduleStatusEnums.MissedVisit.ToString() || x.PrimaryPayerID!=x.CLIENT_PAYMENT_ID);
                if (terminalParentSchedules?.Count() > 0)
                {
                    var ParentTerminalCgtaskids = terminalParentSchedules.Select(x => x.CaregiverTasksTerminalPayerID).ToList();
                    // delete Parent schedule terminal payers
                    var terminalPayerParentVisitFilter = _repositoryFilterConditions.InFilter("CaregiverTasksTerminalPayer.CaregiverTasksTerminalPayerID", string.Join(",", ParentTerminalCgtaskids), FilterConditionDataTypeEnums.integerType)
                                                         .Build();

                    var output = await _caregiverTasksTerminalPayerRepository.DeleteCaregiverTasksTerminalPayer(_actionContext.HHA, _actionContext.User, terminalPayerParentVisitFilter);

                    if (output != 1)
                        ret = false;
                    else
                        ret = await this.DeleteChildVisitTerminalPayers(terminalParentSchedules.Select(x=> x.CGTASK_ID).ToArray(), new int[] { });
                }
            }

            return ret;
        }

        public async Task<bool> DeleteChildVisitTerminalPayers(int[] CgtaskIDs, int[] ChildCgtaskIDs)
        {
            bool ret = true;
            string Columns = null;
            string Filters = null;
            string ChildCgtaskIDsCommaSeparated = "";

            if (CgtaskIDs.Length > 0)
            {
                Columns = "Child_Schedule_Id";
                var Filter = _repositoryFilterConditions.InFilter("CaregiverTaskChildSchedules.PARENT_CGTASK_ID", string.Join(",", CgtaskIDs), FilterConditionDataTypeEnums.integerType)
                                                         .Build();

                var caregivertaskchildschedules = await _caregiverTaskChildSchedulesRepository.GetAllChildSchedulesList(_actionContext.HHA, _actionContext.User, Columns, Filter, true);

                if (caregivertaskchildschedules?.Count() > 0)
                {
                    // Add child schedules to existing array                    
                    ChildCgtaskIDsCommaSeparated = string.Join(",", caregivertaskchildschedules.Select(x => x.Child_Schedule_Id));
                }
            }

            if (ChildCgtaskIDs.Length > 0)
            {
                if (string.IsNullOrEmpty(ChildCgtaskIDsCommaSeparated))
                    ChildCgtaskIDsCommaSeparated = string.Join(",", ChildCgtaskIDs);
                else
                    ChildCgtaskIDsCommaSeparated += "," + string.Join(",", ChildCgtaskIDs);

                if (string.IsNullOrEmpty(ChildCgtaskIDsCommaSeparated) || ChildCgtaskIDsCommaSeparated == "0")
                    return true; 

            }

            if (!string.IsNullOrEmpty(ChildCgtaskIDsCommaSeparated))
            {
                //delete child which are not evv
                Filters = _repositoryFilterConditions.InFilter("CaregiverTaskChildScheduleTerminalPayer.ChildScheduleID", ChildCgtaskIDsCommaSeparated, FilterConditionDataTypeEnums.integerType)
                            .Build();

                ret = await this.DeleteChildVisitTerminalPayersByFilter(Filters);
            }


            return ret;
        }

        public async Task<bool> DeleteChildVisitTerminalPayersByFilter(string Filters)
        {
            bool ret = true;
            
            string Columns = "CaregiverTaskChildSchedules.IsEvvSchedule,CaregiverTaskChildSchedules.CANBILL,CaregiverTaskChildScheduleTerminalPayer.CaregiverChildTaskTerminalPayerID ";

            var terminalChildSchedules = await _caregiverTasksTerminalPayerRepository.GetAllTerminalPayerChildSchedules(_actionContext.HHA, _actionContext.User, Columns, Filters);

            if (terminalChildSchedules?.Count() > 0)
            {

                var childTerminalCgtaskids = terminalChildSchedules.Select(x => x.CaregiverChildTaskTerminalPayerID).ToList();

                // delete Parent schedule terminal payers
                var terminalPayerchildVisitFilter = _repositoryFilterConditions.InFilter("CaregiverTaskChildScheduleTerminalPayer.CaregiverChildTaskTerminalPayerID ", string.Join(",", childTerminalCgtaskids), FilterConditionDataTypeEnums.integerType)
                                                     .Build();

                var output = await _caregiverTasksTerminalPayerRepository.DeleteCaregiverTaskChildScheduleTerminalPayer(_actionContext.HHA, _actionContext.User, terminalPayerchildVisitFilter);

                if (output != 0)
                    ret = false;
            }

            return ret;

        }

    }
}


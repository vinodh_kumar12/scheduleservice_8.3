﻿using AutoMapper; 
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Schedules.Command; 
using System; 
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.CommandHandlers
{
    public class SaveEVVExceptionSettingsCommandHandler : IHandlerWrapper<SaveEVVExceptionSettingsCommand, bool>
    {
        IMapper _mapper;
        private readonly IHHAScheduleAuthSettingsRepository _hHAScheduleAuthSettingsRepository;

        public SaveEVVExceptionSettingsCommandHandler(IMapper mapper, IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository)
        {

            this._mapper = mapper;
            this._hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
        }

        public async Task<Response<bool>> Handle(SaveEVVExceptionSettingsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string settingsProperties = "";
                var result = await this._hHAScheduleAuthSettingsRepository.SaveHHAScheduleAuthSettingsDetails(request.HHA, request.UserId, settingsProperties);
                if(result)
                    return Response.Ok(true);
                else
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            }
            catch (Exception e)
            {
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            }
        }
    }
}

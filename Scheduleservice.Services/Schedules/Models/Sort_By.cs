﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class Sort_By
    {
        public string sort_field1 { get; set; }
        public bool is_ascending_filed1 { get; set; }
        public string sort_field2 { get; set; }
        public bool is_ascending_filed2 { get; set; }
        public string sort_field3 { get; set; }
        public bool is_ascending_filed3 { get; set; }

    }

    public class Sort_By_List
    {
        public string sort_field { get; set; }
        public bool is_ascending_filed { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class ScheduleCheckinout
    {
        public string checkin_time { get; set; }
        public string checkout_time { get; set; }        
        public string checkin_date { get; set; }
        public string edited_hours { get; set; }
        public string payable_hours { get; set; }
        public string checkin_exception_code { get; set; }
        public string checkin_exception_desc { get; set; }
        public string checkin_reason_code { get; set; }
        public string checkin_reason_desc { get; set; }
        public string checkin_action_code { get; set; }
        public string checkin_action_desc { get; set; }
        public string checkin_reason_notes { get; set; }
        public string checkout_exception_code { get; set; }
        public string checkout_exception_desc { get; set; }
        public string checkout_reason_code { get; set; }
        public string checkout_reason_desc { get; set; }
        public string checkout_action_code { get; set; }
        public string checkout_action_desc { get; set; }
        public string checkout_reason_notes { get; set; }
        public bool is_attested { get; set; }
        public string caregiver_geolocation { get; set; }
        public int checkin_treatment_adress_id { get; set; }
        public int checkout_treatment_adress_id { get; set; }
        public int checkin_source { get; set; }
        public int checkout_source { get; set; }
        public string checkin_fob_code { get; set; }
        public string checkout_fob_code { get; set; }
        public bool is_clincian_checkin_checkout { get; set; }
        public string checkin_reason_context { get; set; }
        public string checkout_reason_context { get; set; }
        public string fob_checkin_device_id { get; set; }
        public string fob_checkout_device_id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class ScheduleEVVBatch
    {
        public int? client_id { get; set; }
        public string payer_ids { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string hha_branch_ids { get; set; }
        public string context { get; set; }
        public int[] client_id_list { get; set; }
        public int? aggregator_id { get; set; }
        public string visit_status { get; set; }
        public bool? billed { get; set; }
        public bool? billable { get; set; }
    }
}

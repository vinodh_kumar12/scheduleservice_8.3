﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class ScheduleSearch
    {
        public int? client_id { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public bool? is_evv_export_dirty { get; set; }
        public int? is_billable { get; set; }
        public bool? is_invoiced { get; set; }
        public bool? is_payrolled { get; set; }
        public string schedule_status { get; set; }
        public bool? is_evv_aggregator_export_required { get; set; }
        public string payer_ids { get; set; }
        public bool? split_for_billing_parent { get; set; }
        public bool? is_evv_schedule { get; set; }
        public bool? include_split_schedules { get; set; }
        public IList<Sort_By_List> sort_by { get; set; }
    }
}

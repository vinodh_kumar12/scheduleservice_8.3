﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class EvvTerminalPayerSearchSchedules
    {
        public string schedule_date_from { get; set; }
        public string schedule_date_to { get; set; }
        public string schedule_status { get; set; }
        public string location { get; set; }
        public int client_id { get; set; }
        public string payer_id { get; set; }
        public string terminal_payer_id { get; set; }
        public string terminal_payer_hierarchy { get; set; }
        //public int terminal_aggregator { get; set; }
        public int terminal_configuration_ID { get; set; }
        public string export_status { get; set; }
        public string disc { get; set; }
        public Sort_By sort_by { get; set; }
        public int purpose { get; set; }
        public string client_status { get; set; }
    }
}

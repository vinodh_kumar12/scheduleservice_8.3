﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class ScheduleCheckin
    {
        public string checkin_time { get; set; }
        public string checkin_date { get; set; }
        public string exception_code { get; set; }
        public string exception_desc { get; set; }
        public string reason_code { get; set; }
        public string reason_desc { get; set; }
        public string action_code { get; set; }
        public string action_desc { get; set; }
        public string reason_notes { get; set; }
        public bool is_attested { get; set; }
        public string caregiver_geolocation { get; set; }
        public int client_treatment_adress_id { get; set; }
        public int checkin_source { get; set; }
        public string fob_code { get; set; }
        public bool is_clincian_checkin { get; set; }
        public string reason_context { get; set; }
        public string fob_checkin_device_id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Models.Subscriber;
using Newtonsoft.Json;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Schedules;
using Scheduleservice.Core.Entities;
using Newtonsoft.Json.Linq;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Constants;
using Scheduleservice.Core.Interfaces.Services.Subscriber;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Subscriber.Caretracker.Services.VisitSusbscriber
{
    public class VisitCheckInOutSubscriberService : IVisitCheckInOutSubscriberService
    {
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ICaregivertasksRepository _caregivertasksRepository;
        private readonly IHomeHealthAgenciesRepository _homeHealthAgenciesRepository;
        private readonly ICaregiversRepository _caregiversRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;
        private readonly IServiceCodesRepository _serviceCodesRepository;
        private readonly IPaymentSourceAuthorizationRepository _paymentSourceAuthorizationRepository;
        private readonly IScheduleShiftRepository _scheduleShiftRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IClientTreatmentAddressRepository _clientTreatmentAddressRepository;
        private readonly ILogger<VisitCheckInOutSubscriberService> _logger;

        public VisitCheckInOutSubscriberService(IRepositoryFilterConditions _repositoryFilterConditions, ICaregivertasksRepository _caregivertasksRepository,
                IHomeHealthAgenciesRepository _homeHealthAgenciesRepository, ICaregiversRepository _caregiversRepository,
                IPaymentsourcesRepository _paymentsourcesRepository, IServiceCodesRepository _serviceCodesRepository,
                IPaymentSourceAuthorizationRepository _paymentSourceAuthorizationRepository,
                IScheduleShiftRepository _scheduleShiftRepository, IAddressRepository _addressRepository,
                IClientTreatmentAddressRepository _clientTreatmentAddressRepository,
                ILogger<VisitCheckInOutSubscriberService> _logger)
        {
            this._repositoryFilterConditions = _repositoryFilterConditions;
            this._caregivertasksRepository = _caregivertasksRepository;
            this._homeHealthAgenciesRepository = _homeHealthAgenciesRepository;
            this._caregiversRepository = _caregiversRepository;
            this._paymentsourcesRepository = _paymentsourcesRepository;
            this._serviceCodesRepository = _serviceCodesRepository;
            this._paymentSourceAuthorizationRepository = _paymentSourceAuthorizationRepository;
            this._scheduleShiftRepository = _scheduleShiftRepository;
            this._addressRepository = _addressRepository;
            this._clientTreatmentAddressRepository = _clientTreatmentAddressRepository;
            this._logger = _logger;
        }

        public async Task<Response<bool>> VisitCheckinoutService(JObject _message)
        {
            try
            {
                bool ret = false;
                var UpdateScheduleColumns = "";
                var UpdateScheduleAdditionalInfoColumns = "";
                var UpdateScheduleAdditional2InfoColumns = "";
                int? CaregiverID = null;
                string Error_Message = "";
                string VisitStatus = "";
                string CheckinSource = "";
                string CheckoutSource = "";
                string checkin_geo_location_bounday_flag = "";
                string checkout_geo_location_bounday_flag = "";

                CaregiversEntity caregiverInfo = null;

                //Deserialize the message Payload
                var visitCheckInOutSubscriberModel = _message.ToObject<VisitCheckInOutSubscriberModel>();

                //HomeHealth Agencies Info
                var HomeHealthAgenciesFilter = _repositoryFilterConditions.EqualFilter("Publish_UID", visitCheckInOutSubscriberModel.source_app.source_agency_uid, FilterConditionDataTypeEnums.stringType)
                                                  .Build();

                var HomeHealthAgenciesRequiredColumns = "HHA_ID,Publish_UID";
                var HomeHealthAgenciesList = await _homeHealthAgenciesRepository.GetHomeHealthAgencieseBasicInfo(0, 0, HomeHealthAgenciesRequiredColumns, HomeHealthAgenciesFilter);

                var HomeHealthAgenciesInfo = HomeHealthAgenciesList.FirstOrDefault();

                if (HomeHealthAgenciesInfo == null)
                {
                    return Response.ValidationError<bool>("Source Agency UID is not correct");
                }

                if (HomeHealthAgenciesInfo != null)
                {
                    //Caregiver Info
                    if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.source_app.source_caregiver_uid))
                    {
                        var CaregiverFilter = _repositoryFilterConditions.EqualFilter("Publish_UID", visitCheckInOutSubscriberModel.source_app.source_caregiver_uid, FilterConditionDataTypeEnums.stringType)
                                                  .Build();
                        var CaregiverRequiredColumn = "CAREGIVER_ID,Publish_UID";
                        var caregiverInfoList = await _caregiversRepository.GetCaregivers(HomeHealthAgenciesInfo.HHA_ID, 0, CaregiverRequiredColumn, CaregiverFilter);

                        caregiverInfo = caregiverInfoList.FirstOrDefault();

                        if (caregiverInfo == null)
                        {
                            return Response.ValidationError<bool>("Source Caregiver UID is not correct");
                        }

                        if (caregiverInfo != null)
                        {
                            CaregiverID = caregiverInfo.CAREGIVER_ID;
                        }
                    }

                    //Payer Info
                    var PayerFilter = _repositoryFilterConditions.EqualFilter("PaymentSources.Publish_UID", visitCheckInOutSubscriberModel.source_app.source_payer_uid, FilterConditionDataTypeEnums.stringType)
                                              .Build();
                    var PayerRequiredColumn = "PaymentSources.PAYMENT_SOURCE_ID,PaymentSources.Publish_UID";
                    var payerInfo = _paymentsourcesRepository.GetPaymentsources(HomeHealthAgenciesInfo.HHA_ID, 0, PayerRequiredColumn, PayerFilter).Result.FirstOrDefault();

                    if (payerInfo == null)
                    {
                        return Response.ValidationError<bool>("Source Payer UID is not correct");
                    }

                    //Service Info
                    var ServiceFilter = _repositoryFilterConditions.EqualFilter("Publish_UID", visitCheckInOutSubscriberModel.source_app.source_service_uid, FilterConditionDataTypeEnums.stringType)
                                              .Build();
                    var ServiceRequiredColumn = "SERVICE_CODE_ID,Publish_UID";
                    var ServiceCodeList = await _serviceCodesRepository.GetServiceCodes(HomeHealthAgenciesInfo.HHA_ID, 0, ServiceRequiredColumn, ServiceFilter);

                    var serviceInfo = ServiceCodeList.FirstOrDefault();

                    if (payerInfo == null)
                    {
                        return Response.ValidationError<bool>("Source Service UID is not correct");
                    }

                    //Schedule Info
                    var CaregiverTaskFilter = _repositoryFilterConditions.EqualFilter("CaregiverTasks.Publish_UID", visitCheckInOutSubscriberModel.source_app.source_visit_uid, FilterConditionDataTypeEnums.stringType)
                                                  .Build();

                    var CaregiverTaskRequiredColumns = "CaregiverTasks.Publish_UID,CGTASK_ID,CAREGIVER,PLANNED_DATE,PLANNED_START_TIME,PLANNED_END_TIME,ACCTUAL_START_TIME,ACCTUAL_END_TIME,STATUS,EDITED_HOURS,SERVICECODE_ID,CLIENT_ID,AUTHORIZATION_ID,EDITED_HOURS_PAYABLE,IS_PAYRATE_HOURLY,IS_PAYABLE,IS_BILLABLE,PAYMENT_SOURCE,IS_BILLED,IS_PAID,CaregiverTasks.HHA,IS_AUTH_MANDATORY,CheckInSource,CheckOutSource,isAuthorized,CaregiverTaskAdditional.Units,CHECK_IN_LOCATION,CHECK_OUT_LOCATION,isEvvschedule,isEvvScheduleDirty,CheckinTreatmentLocation,CheckoutTreatmentLocation,IsEVVDirty,EVVOriginalCheckinTime,EVVOriginalCheckoutTime,EvvCheckinLocationVerified,EvvCheckoutLocationVerified,travelTimeHours,documentationTimeMins,Client_UUID,Caregiver_UUID,Service_UUID,Payer_UUID,CONFIRMED,ShiftID,MILES,CanBill,isEvvAggregatorExportRequired";

                    var OldCaregiverTaskDetailList = await _caregivertasksRepository.GetAllSchedulesList(HomeHealthAgenciesInfo.HHA_ID, 0, CaregiverTaskRequiredColumns, CaregiverTaskFilter);

                    var OldCaregiverTaskDetail = OldCaregiverTaskDetailList.FirstOrDefault();

                    if (OldCaregiverTaskDetail == null)
                    {
                        return Response.ValidationError<bool>("Source visit UID is not correct");
                    }

                    if (OldCaregiverTaskDetail != null)
                    {
                        //VISIT CONFIRMED
                        if (visitCheckInOutSubscriberModel.schedule_info.is_visit_confirmed && OldCaregiverTaskDetail.CONFIRMED != visitCheckInOutSubscriberModel.schedule_info.is_visit_confirmed)
                            UpdateScheduleColumns += "CONFIRMED=1,";
                        else if (!visitCheckInOutSubscriberModel.schedule_info.is_visit_confirmed && OldCaregiverTaskDetail.CONFIRMED != visitCheckInOutSubscriberModel.schedule_info.is_visit_confirmed)
                            UpdateScheduleColumns += "CONFIRMED=0,";

                        //CHECK-IN
                        if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_time != null)
                        {
                            if ((OldCaregiverTaskDetail.ACCTUAL_START_TIME == null) || (OldCaregiverTaskDetail.ACCTUAL_START_TIME != null && OldCaregiverTaskDetail.ACCTUAL_START_TIME != Convert.ToDateTime(visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_time)))
                                UpdateScheduleColumns += "ACCTUAL_START_TIME='" + Convert.ToDateTime(visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_time) + "',";
                        }

                        //CHECK-OUT
                        if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_time != null && (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_time != null || (OldCaregiverTaskDetail.ACCTUAL_START_TIME != null)))
                        {
                            if (OldCaregiverTaskDetail.ACCTUAL_END_TIME == null || (OldCaregiverTaskDetail.ACCTUAL_END_TIME != null && OldCaregiverTaskDetail.ACCTUAL_END_TIME != Convert.ToDateTime(visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_time)))
                                UpdateScheduleColumns += "ACCTUAL_END_TIME='" + Convert.ToDateTime(visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_time) + "',";
                        }


                        //VISIT STATUS
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.visit_info.visit_status))
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusCRT.planned || visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusRTB.Planned)
                                VisitStatus = VisitStatusRTB.Planned;
                            else if (visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusCRT.in_progress || visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusRTB.In_Progress)
                                VisitStatus = VisitStatusRTB.In_Progress;
                            else if (visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusCRT.completed || visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusRTB.Completed)
                                VisitStatus = VisitStatusRTB.Completed;
                            else if (visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusCRT.adjudicated || visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusRTB.Approved)
                                VisitStatus = VisitStatusRTB.Approved;
                            else if (visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusCRT.missed || visitCheckInOutSubscriberModel.visit_info.visit_status == VisitStatusRTB.MissedVisit)
                                return Response.ValidationError<bool>("Visit Status cannot be made to Missed Visit. Since Client/Clinician related reason is not provided");

                            if (OldCaregiverTaskDetail.STATUS != "Planned")
                            {
                                Error_Message = "Cannot checkinout as current status of the visit is " + VisitStatus + " Status Must be in planned to do checkin";
                                return Response.ValidationError<bool>(Error_Message);
                            }

                            if (!string.IsNullOrEmpty(VisitStatus) && OldCaregiverTaskDetail.STATUS != VisitStatus)
                                UpdateScheduleColumns += "STATUS='" + VisitStatus + "',";
                        }

                        //EVV ORIGINAL CHECK-IN TIME
                        if (visitCheckInOutSubscriberModel.visit_info.checkin?.evv_checkin_time != null && visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_time != null && OldCaregiverTaskDetail.ACCTUAL_START_TIME == null)
                            UpdateScheduleAdditional2InfoColumns += "EVVOriginalCheckinTime='" + visitCheckInOutSubscriberModel.visit_info.checkin?.evv_checkin_time + "',";

                        //EVV ORIGINAL CHECK-OUT TIME
                        if (visitCheckInOutSubscriberModel.visit_info.checkout?.evv_checkout_time != null && visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_time != null && OldCaregiverTaskDetail.ACCTUAL_END_TIME == null)
                            UpdateScheduleAdditional2InfoColumns += "EVVOriginalCheckoutTime='" + visitCheckInOutSubscriberModel.visit_info.checkout?.evv_checkout_time + "',";

                        //CHECK-IN SOURCE
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source))
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceRTB.Online || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceCRT.Online)
                                CheckinSource = CheckinoutSourceRTB.Online;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceRTB.Telephone || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceCRT.Telephone)
                                CheckinSource = CheckinoutSourceRTB.Telephone;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceRTB.KIce || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceCRT.KIce)
                                CheckinSource = CheckinoutSourceRTB.KIce;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceRTB.KMobile || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceCRT.KMobile)
                                CheckinSource = CheckinoutSourceRTB.KMobile;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceRTB.Fob || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_source == CheckinoutSourceCRT.Fob)
                                CheckinSource = CheckinoutSourceRTB.Fob;

                            if (OldCaregiverTaskDetail.CheckInSource == null || (OldCaregiverTaskDetail.CheckInSource != Convert.ToInt32(CheckinSource)))
                                UpdateScheduleColumns += "CheckInSource=" + Convert.ToInt32(CheckinSource) + ",";
                        }

                        //CHECK-OUT SOURCE
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source))
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceRTB.Online || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceCRT.Online)
                                CheckoutSource = CheckinoutSourceRTB.Online;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceRTB.Telephone || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceCRT.Telephone)
                                CheckoutSource = CheckinoutSourceRTB.Telephone;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceRTB.KIce || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceCRT.KIce)
                                CheckoutSource = CheckinoutSourceRTB.KIce;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceRTB.KMobile || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceCRT.KMobile)
                                CheckoutSource = CheckinoutSourceRTB.KMobile;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceRTB.Fob || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_source == CheckinoutSourceCRT.Fob)
                                CheckoutSource = CheckinoutSourceRTB.Fob;

                            if (OldCaregiverTaskDetail.CheckOutSource == null || (OldCaregiverTaskDetail.CheckOutSource != Convert.ToInt32(CheckoutSource)))
                                UpdateScheduleColumns += "CheckOutSource=" + Convert.ToInt32(CheckoutSource) + ",";
                        }

                        //CHECK-IN GEO LOCATION
                        if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location != null
                            && visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location?.lat != null
                            && visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location?.lng != null
                            && OldCaregiverTaskDetail.CHECK_IN_LOCATION != visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location.lat + "," + visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location.lng)
                            UpdateScheduleAdditionalInfoColumns += "CHECK_IN_LOCATION='" + visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location.lat + "," + visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location.lng + "',";

                        //CHECK-OUT GEO LOCATION
                        if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location != null
                            && visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location?.lat != null
                            && visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location?.lng != null
                            && OldCaregiverTaskDetail.CHECK_OUT_LOCATION != visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location.lat + "," + visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location.lng)
                            UpdateScheduleAdditionalInfoColumns += "CHECK_OUT_LOCATION='" + visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location.lat + "," + visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location.lng + "',";

                        //CHECK-IN GEO LOCATION BOUNDARY FLAG
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag))
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagCRT.Inside || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagRTB.WithInLocation)
                                checkin_geo_location_bounday_flag = GeolocationFlagRTB.WithInLocation;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagCRT.Outside || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagRTB.OutsideLocation)
                                checkin_geo_location_bounday_flag = GeolocationFlagRTB.OutsideLocation;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagCRT.ClientLocationNotAvailable || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagCRT.CheckinLocationNotAvailable || visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_geo_location_bounday_flag == GeolocationFlagRTB.LocationNotVerified)
                                checkin_geo_location_bounday_flag = GeolocationFlagRTB.LocationNotVerified;
                        }

                        if (!string.IsNullOrEmpty(checkin_geo_location_bounday_flag) && OldCaregiverTaskDetail.EvvCheckinLocationVerified != Convert.ToChar(checkin_geo_location_bounday_flag))
                            UpdateScheduleAdditionalInfoColumns += "EvvCheckinLocationVerified='" + Convert.ToChar(checkin_geo_location_bounday_flag) + "',";

                        //CHECK-OUT GEO LOCATION BOUNDARY FLAG
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag))
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagCRT.Inside || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagRTB.WithInLocation)
                                checkout_geo_location_bounday_flag = GeolocationFlagRTB.WithInLocation;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagCRT.Outside || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagRTB.OutsideLocation)
                                checkout_geo_location_bounday_flag = GeolocationFlagRTB.OutsideLocation;
                            else if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagCRT.ClientLocationNotAvailable || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagCRT.CheckinLocationNotAvailable || visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_geo_location_bounday_flag == GeolocationFlagRTB.LocationNotVerified)
                                checkout_geo_location_bounday_flag = GeolocationFlagRTB.LocationNotVerified;
                        }

                        if (!string.IsNullOrEmpty(checkout_geo_location_bounday_flag) && OldCaregiverTaskDetail.EvvCheckoutLocationVerified != Convert.ToChar(checkout_geo_location_bounday_flag))
                            UpdateScheduleAdditionalInfoColumns += "EvvCheckoutLocationVerified='" + Convert.ToChar(checkout_geo_location_bounday_flag) + "',";

                        //CHECK-IN DISTANCE METERS
                        if (visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_distance_meters != null && visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_distance_meters > 0 && OldCaregiverTaskDetail.CHECK_IN_DISTANCE != visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_distance_meters)
                            UpdateScheduleAdditionalInfoColumns += "CHECK_IN_DISTANCE='" + visitCheckInOutSubscriberModel.visit_info.checkin?.checkin_distance_meters + "',";

                        //CHECK-OUT DISTANCE METERS
                        if (visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_distance_meters != null && visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_distance_meters > 0 && OldCaregiverTaskDetail.CHECK_OUT_DISTANCE != visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_distance_meters)
                            UpdateScheduleAdditionalInfoColumns += "CHECK_OUT_DISTANCE='" + visitCheckInOutSubscriberModel.visit_info.checkout?.checkout_distance_meters + "',";

                        //CHECK-IN TREATMENT LOCATION
                        if ((visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_type == null) || (visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_type != null && visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_type.ToLower() == "homeaddress"))
                        {
                            UpdateScheduleAdditionalInfoColumns += "CheckinTreatmentLocation=null,";
                        }
                        else if (visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_type != null && visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_type.ToLower() == "treatmentaddress")
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_geo_location.lat != null && visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_geo_location.lng != null)
                            {
                                var checkinAddressRequiredColumn = "ADDRESS_ID";
                                var checkinAddressRequiredFilter = _repositoryFilterConditions.EqualFilter("Latitude", visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_geo_location.lat, FilterConditionDataTypeEnums.stringType)
                                                       .EqualFilter("Longitude", visitCheckInOutSubscriberModel.visit_info.checkin?.address?.address_geo_location.lng, FilterConditionDataTypeEnums.stringType)
                                                      .Build();

                                var TreatmentAddressInfo = _addressRepository._S_Schedule_GetTreatmentLocationInfo(HomeHealthAgenciesInfo.HHA_ID, 0, checkinAddressRequiredColumn, checkinAddressRequiredFilter).Result;

                                if (TreatmentAddressInfo != null && TreatmentAddressInfo.ADDRESS_ID > 0)
                                {
                                    var ClientTreatmentAddressRequiredColumn = "ClientTreatmentAddressID";
                                    var ClientTreatmentAddressRequiredFilter = _repositoryFilterConditions.EqualFilter("TreatmentAddressID", TreatmentAddressInfo.ADDRESS_ID, FilterConditionDataTypeEnums.integerType)
                                                                                .EqualFilter("ClientID", OldCaregiverTaskDetail.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                                                .Build();
                                    var ClientTreatmentAddressInfo = _clientTreatmentAddressRepository._S_Schedule_GetClientTreatmentAddressInfo(HomeHealthAgenciesInfo.HHA_ID, 0, ClientTreatmentAddressRequiredColumn, ClientTreatmentAddressRequiredFilter).Result;

                                    if (ClientTreatmentAddressInfo != null && ClientTreatmentAddressInfo.ClientTreatmentAddressID > 0 && OldCaregiverTaskDetail.CheckinTreatmentLocation != ClientTreatmentAddressInfo.ClientTreatmentAddressID)
                                    {
                                        UpdateScheduleAdditionalInfoColumns += "CheckinTreatmentLocation=" + ClientTreatmentAddressInfo.ClientTreatmentAddressID + ",";
                                    }

                                }
                            }
                        }

                        //CHECK-OUT TREAMENT LOCATION
                        if ((visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_type == null) || (visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_type != null && visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_type.ToLower() == "homeaddress"))
                        {
                            UpdateScheduleAdditionalInfoColumns += "CheckoutTreatmentLocation=null,";
                        }
                        else if (visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_type != null && visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_type.ToLower() == "treatmentaddress")
                        {
                            if (visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_geo_location.lat != null && visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_geo_location.lng != null)
                            {
                                var checkoutAddressRequiredColumn = "ADDRESS_ID";
                                var checkoutAddressRequiredFilter = _repositoryFilterConditions.EqualFilter("Latitude", visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_geo_location.lat, FilterConditionDataTypeEnums.stringType)
                                                       .EqualFilter("Longitude", visitCheckInOutSubscriberModel.visit_info.checkout?.address?.address_geo_location.lng, FilterConditionDataTypeEnums.stringType)
                                                      .Build();

                                var TreatmentAddressInfo = _addressRepository._S_Schedule_GetTreatmentLocationInfo(HomeHealthAgenciesInfo.HHA_ID, 0, checkoutAddressRequiredColumn, checkoutAddressRequiredFilter).Result;

                                if (TreatmentAddressInfo != null && TreatmentAddressInfo.ADDRESS_ID > 0)
                                {
                                    var ClientTreatmentAddressRequiredColumn = "ClientTreatmentAddressID";
                                    var ClientTreatmentAddressRequiredFilter = _repositoryFilterConditions.EqualFilter("TreatmentAddressID", TreatmentAddressInfo.ADDRESS_ID, FilterConditionDataTypeEnums.integerType)
                                                                                .EqualFilter("ClientID", OldCaregiverTaskDetail.CLIENT_ID, FilterConditionDataTypeEnums.integerType)
                                                                                .Build();
                                    var ClientTreatmentAddressInfo = _clientTreatmentAddressRepository._S_Schedule_GetClientTreatmentAddressInfo(HomeHealthAgenciesInfo.HHA_ID, 0, ClientTreatmentAddressRequiredColumn, ClientTreatmentAddressRequiredFilter).Result;

                                    if (ClientTreatmentAddressInfo != null && ClientTreatmentAddressInfo.ClientTreatmentAddressID > 0 && OldCaregiverTaskDetail.CheckoutTreatmentLocation != ClientTreatmentAddressInfo.ClientTreatmentAddressID)
                                    {
                                        UpdateScheduleAdditionalInfoColumns += "CheckoutTreatmentLocation=" + ClientTreatmentAddressInfo.ClientTreatmentAddressID + ",";
                                    }
                                }
                            }
                        }

                        //SHIFT NAME
                        if (!string.IsNullOrEmpty(visitCheckInOutSubscriberModel.schedule_info.shift_name))
                        {
                            //Shift Info
                            var ShiftScheduleFilter = _repositoryFilterConditions.EqualFilter("SHIFT_NAME", visitCheckInOutSubscriberModel.schedule_info.shift_name, FilterConditionDataTypeEnums.stringType)
                                                      .Build();
                            var shiftScheduleRequiredColumn = "SHIFT_ID,HHA,SHIFT_NAME";
                            var shiftScheduleInfo = _scheduleShiftRepository._HHA_Schedule_GetScheduleShiftsInfo(HomeHealthAgenciesInfo.HHA_ID, 0, shiftScheduleRequiredColumn, ShiftScheduleFilter).Result.FirstOrDefault();

                            if (shiftScheduleInfo != null)
                            {
                                if (OldCaregiverTaskDetail.ShiftID != shiftScheduleInfo.SHIFT_ID)
                                    UpdateScheduleAdditionalInfoColumns += "ShiftID=" + shiftScheduleInfo.SHIFT_ID + ",";
                            }
                        }

                        //EDITED HOURS
                        if (visitCheckInOutSubscriberModel.visit_info.hours_billable_in_mins != null && visitCheckInOutSubscriberModel.visit_info.hours_billable_in_mins > 0)
                        {
                            if (OldCaregiverTaskDetail.EDITED_HOURS == null || (OldCaregiverTaskDetail.EDITED_HOURS != null && OldCaregiverTaskDetail.EDITED_HOURS != Common.ConvertMinsToString(Convert.ToInt32(visitCheckInOutSubscriberModel.visit_info.hours_billable_in_mins))))
                                UpdateScheduleColumns += "EDITED_HOURS='" + Common.ConvertMinsToString(Convert.ToInt32(visitCheckInOutSubscriberModel.visit_info.hours_billable_in_mins)) + "',";
                        }

                        //EDITED HOURS PAYABLE
                        if (visitCheckInOutSubscriberModel.visit_info.hours_payable_in_mins != null && visitCheckInOutSubscriberModel.visit_info.hours_payable_in_mins > 0)
                        {
                            if (OldCaregiverTaskDetail.EDITED_HOURS_PAYABLE == null || OldCaregiverTaskDetail.EDITED_HOURS_PAYABLE != null && OldCaregiverTaskDetail.EDITED_HOURS_PAYABLE != Common.ConvertMinsToString(Convert.ToInt32(visitCheckInOutSubscriberModel.visit_info.hours_payable_in_mins)))
                                UpdateScheduleColumns += "EDITED_HOURS_PAYABLE='" + Common.ConvertMinsToString(Convert.ToInt32(visitCheckInOutSubscriberModel.visit_info.hours_payable_in_mins)) + "',";
                        }

                        //BILLABLE FLAG
                        if (OldCaregiverTaskDetail.CanBill != visitCheckInOutSubscriberModel.visit_info.is_billable)
                        {
                            string returnValue = "";
                            returnValue = visitCheckInOutSubscriberModel.visit_info.is_billable == true ? "1" : "0";
                            UpdateScheduleColumns += "CanBill=" + returnValue + ",";
                        }

                        //MILES
                        if (visitCheckInOutSubscriberModel.visit_info.miles != null && visitCheckInOutSubscriberModel.visit_info.miles > 0)
                        {
                            if (OldCaregiverTaskDetail.MILES == null || (OldCaregiverTaskDetail.MILES != null && OldCaregiverTaskDetail.MILES != visitCheckInOutSubscriberModel.visit_info.miles))
                                UpdateScheduleColumns += "MILES='" + visitCheckInOutSubscriberModel.visit_info.miles + "',";
                        }

                        //TRAVEL TIME
                        if (visitCheckInOutSubscriberModel.visit_info.travel_time_hours_in_mins != null && visitCheckInOutSubscriberModel.visit_info.travel_time_hours_in_mins > 0)
                        {
                            if (OldCaregiverTaskDetail.travelTimeHours == null || (OldCaregiverTaskDetail.travelTimeHours != null && OldCaregiverTaskDetail.travelTimeHours != visitCheckInOutSubscriberModel.visit_info.travel_time_hours_in_mins))
                                UpdateScheduleAdditionalInfoColumns += "travelTimeHours='" + visitCheckInOutSubscriberModel.visit_info.travel_time_hours_in_mins + "',";
                        }

                        //DOCUMENTATION TIME
                        if (visitCheckInOutSubscriberModel.visit_info.doc_time_hours_in_mins != null && visitCheckInOutSubscriberModel.visit_info.doc_time_hours_in_mins > 0)
                        {
                            if (OldCaregiverTaskDetail.documentationTimeMins == null || (OldCaregiverTaskDetail.documentationTimeMins != null && OldCaregiverTaskDetail.documentationTimeMins != visitCheckInOutSubscriberModel.visit_info.doc_time_hours_in_mins))
                                UpdateScheduleAdditionalInfoColumns += "documentationTimeMins='" + visitCheckInOutSubscriberModel.visit_info.doc_time_hours_in_mins + "',";
                        }

                        //EVV FLAG
                        if (visitCheckInOutSubscriberModel.evv.is_evv_schedule && OldCaregiverTaskDetail.isEvvschedule != visitCheckInOutSubscriberModel.evv.is_evv_schedule)
                        {
                            UpdateScheduleAdditionalInfoColumns += "isEvvschedule=1" + ",";
                            UpdateScheduleAdditional2InfoColumns += "IsEVVDirty=1" + ",";
                        }
                        else if (!visitCheckInOutSubscriberModel.evv.is_evv_schedule && OldCaregiverTaskDetail.isEvvschedule != visitCheckInOutSubscriberModel.evv.is_evv_schedule)
                        {
                            UpdateScheduleAdditionalInfoColumns += "isEvvschedule=0" + ",";
                            UpdateScheduleAdditional2InfoColumns += "IsEVVDirty=0" + ",";
                        }

                        //EVV Export Required
                        if (visitCheckInOutSubscriberModel.evv.is_evv_export_required && OldCaregiverTaskDetail.isEvvAggregatorExportRequired != visitCheckInOutSubscriberModel.evv.is_evv_export_required)
                            UpdateScheduleAdditionalInfoColumns += "isEvvAggregatorExportRequired=1" + ",";
                        else if (!visitCheckInOutSubscriberModel.evv.is_evv_export_required && OldCaregiverTaskDetail.isEvvAggregatorExportRequired != visitCheckInOutSubscriberModel.evv.is_evv_export_required)
                            UpdateScheduleAdditionalInfoColumns += "isEvvAggregatorExportRequired=0" + ",";

                        //UNITS
                        if (visitCheckInOutSubscriberModel.schedule_info.units != null && visitCheckInOutSubscriberModel.schedule_info.units > 0)
                        {
                            if (visitCheckInOutSubscriberModel.schedule_info.units != OldCaregiverTaskDetail.Units)
                                UpdateScheduleAdditionalInfoColumns += "Units='" + visitCheckInOutSubscriberModel.schedule_info.units + "',";
                        }
                        else if (visitCheckInOutSubscriberModel.schedule_info.units == 0 || visitCheckInOutSubscriberModel.schedule_info.units == null)
                        {
                            UpdateScheduleAdditionalInfoColumns += "Units=null,";
                        }

                        //ISAUTHORIZED FLAG
                        if (OldCaregiverTaskDetail.IS_AUTH_MANDATORY != null && Convert.ToBoolean(OldCaregiverTaskDetail.IS_AUTH_MANDATORY) && visitCheckInOutSubscriberModel.visit_info.is_billable && visitCheckInOutSubscriberModel.auth.is_authorized)
                        {

                            if (visitCheckInOutSubscriberModel.schedule_info.units > 0 && visitCheckInOutSubscriberModel.schedule_info.units != OldCaregiverTaskDetail.Units)
                            {
                                var result = _paymentSourceAuthorizationRepository.CanModifiedSchedluleWithSameAuth(Convert.ToInt32(OldCaregiverTaskDetail.HHA), 0, OldCaregiverTaskDetail.AUTHORIZATION_ID ?? 0,
                                                    OldCaregiverTaskDetail.CGTASK_ID, Convert.ToString(visitCheckInOutSubscriberModel.schedule_info.planned_date), serviceInfo.SERVICE_CODE_ID,
                                                    (float)visitCheckInOutSubscriberModel.schedule_info.units).Result;

                                if (result == 1)
                                {
                                    if (OldCaregiverTaskDetail.AUTHORIZATION_ID > 0)
                                        UpdateScheduleColumns += "isAuthorized=1,";
                                    else
                                        UpdateScheduleColumns += "isAuthorized=0,";
                                }
                                else
                                    UpdateScheduleColumns += "isAuthorized=0,";
                            }
                            else
                            {
                                UpdateScheduleColumns += "isAuthorized=1,";
                            }
                        }
                        else
                        {
                            if (visitCheckInOutSubscriberModel.auth.is_authorized)
                                UpdateScheduleColumns += "isAuthorized=1,";
                            else
                                UpdateScheduleColumns += "isAuthorized=0,";
                        }


                        if (!string.IsNullOrEmpty(UpdateScheduleColumns) || !string.IsNullOrEmpty(UpdateScheduleAdditionalInfoColumns) || !string.IsNullOrEmpty(UpdateScheduleAdditional2InfoColumns))
                        {
                            if (!string.IsNullOrEmpty(UpdateScheduleColumns))
                                UpdateScheduleColumns = UpdateScheduleColumns.Substring(0, UpdateScheduleColumns.Length - 1);
                            if (!string.IsNullOrEmpty(UpdateScheduleAdditionalInfoColumns))
                                UpdateScheduleAdditionalInfoColumns = UpdateScheduleAdditionalInfoColumns.Substring(0, UpdateScheduleAdditionalInfoColumns.Length - 1);
                            if (!string.IsNullOrEmpty(UpdateScheduleAdditional2InfoColumns))
                                UpdateScheduleAdditional2InfoColumns = UpdateScheduleAdditional2InfoColumns.Substring(0, UpdateScheduleAdditional2InfoColumns.Length - 1);

                            var result = await _caregivertasksRepository.UpdateScheduleInfo_Subscriber(Convert.ToInt32(HomeHealthAgenciesInfo.HHA_ID), 0, OldCaregiverTaskDetail.CGTASK_ID, UpdateScheduleColumns, UpdateScheduleAdditionalInfoColumns, UpdateScheduleAdditional2InfoColumns);

                            if (result == 0)
                                ret = true;
                            else
                                return Response.ValidationError<bool>("Fail to Save due to Exception");
                        }

                    }
                    else
                    {
                        return Response.ValidationError<bool>("Source Visit UID is not correct");
                    }

                }
                else
                {
                    return Response.ValidationError<bool>("Source Agency UID is not correct");
                }

                return Response.Ok<bool>(ret);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, ex.Message);
            }
        }
    }
}

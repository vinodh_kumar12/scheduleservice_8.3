﻿using Newtonsoft.Json.Linq;
using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Subscriber.Caretracker.Command
{
    public class SubscribeMessageCommand : IRequestWrapper<bool>
    {
        public string routing_key { get; set; } //Route Key from CRT
        public JObject message { get; set; } //JSON Data
    }
}

﻿using System;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response; 
using System.Threading.Tasks; 
using System.Threading;
using Scheduleservice.Subscriber.Caretracker.Command;
using AutoMapper;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using Scheduleservice.Core.Interfaces.Services.Subscriber;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;
using System.Linq;

namespace Scheduleservice.Subscriber.Caretracker.CommandHandlers
{
    public class SubscribeMessageCommandHandler : IHandlerWrapper<SubscribeMessageCommand, bool>
    {
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IVisitCheckinSubscriberService _visitCheckinSubscriberService;
        private readonly IVisitCheckInOutSubscriberService _visitCheckinoutSubscriberService;
        private readonly IVisitCheckOutSubscriberService _visitCheckoutSubscriberService;
        private readonly IVisitAdjudicateSubscriberService _visitAdjudicateSubscriberService;
        private readonly IVisitMissedSubscriberService _visitMissedSubscriberService;
        private readonly IVisitUndoCheckInSubscriberService _visitUndoCheckinSubscriberService;
        private readonly IVisitUndoCheckInOutSubscriberService _visitUndoCheckinoutSubscriberService;
        private readonly IVisitUndoCheckOutSubscriberService _visitUndoCheckoutSubscriberService;
        private readonly IVisitUndoAdjudicateSubscriberService _visitUndoAdjudicateSubscriberService;
        private readonly IVisitUndoMissedSubscriberService _visitUndoMissedSubscriberService;
        private readonly IVisitModifiedSubscriberService _visitModifiedSubscriberService;
        private readonly ILogger<SubscribeMessageCommandHandler> _logger;
        private readonly IUAAgencyDatabaseMasterList _uAAgencyDatabaseMasterList;
        public ActionContextDto _actionContext { get; }

        public SubscribeMessageCommandHandler(IMapper mapper,IHttpContextAccessor httpContextAccessor, IVisitCheckinSubscriberService _visitCheckinSubscriberService,
            IVisitCheckInOutSubscriberService _visitCheckinoutSubscriberService, IVisitCheckOutSubscriberService _visitCheckoutSubscriberService,
            IVisitAdjudicateSubscriberService _visitAdjudicateSubscriberService, IVisitMissedSubscriberService _visitMissedSubscriberService,
            IVisitUndoCheckInSubscriberService _visitUndoCheckinSubscriberService, IVisitUndoCheckInOutSubscriberService _visitUndoCheckinoutSubscriberService,
            IVisitUndoCheckOutSubscriberService _visitUndoCheckoutSubscriberService, IVisitUndoAdjudicateSubscriberService _visitUndoAdjudicateSubscriberService,
            IVisitUndoMissedSubscriberService _visitUndoMissedSubscriberService, IVisitModifiedSubscriberService _visitModifiedSubscriberService,
            ILogger<SubscribeMessageCommandHandler> _logger, IUAAgencyDatabaseMasterList _uAAgencyDatabaseMasterList
            )
        {
            this._mapper = mapper;
            this._httpContextAccessor = httpContextAccessor;
            this._visitCheckinSubscriberService = _visitCheckinSubscriberService;
            this._visitCheckinoutSubscriberService = _visitCheckinoutSubscriberService;
            this._visitCheckoutSubscriberService = _visitCheckoutSubscriberService;
            this._visitAdjudicateSubscriberService = _visitAdjudicateSubscriberService;
            this._visitMissedSubscriberService = _visitMissedSubscriberService;
            this._visitUndoCheckinSubscriberService = _visitUndoCheckinSubscriberService;
            this._visitUndoCheckinoutSubscriberService = _visitUndoCheckinoutSubscriberService;
            this._visitUndoCheckoutSubscriberService = _visitUndoCheckoutSubscriberService;
            this._visitUndoAdjudicateSubscriberService = _visitUndoAdjudicateSubscriberService;
            this._visitUndoMissedSubscriberService = _visitUndoMissedSubscriberService;
            this._visitModifiedSubscriberService = _visitModifiedSubscriberService;
            this._logger = _logger;
            this._uAAgencyDatabaseMasterList = _uAAgencyDatabaseMasterList;

            if (_httpContextAccessor.HttpContext != null)
                _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
        }

        public async Task<Response<bool>> Handle(SubscribeMessageCommand request, CancellationToken cancellationToken)
        {
            try
            {
                bool ret = false;
                string _routeKey = request.routing_key;
                JObject _message = request.message;
                Response<bool> _subscriberServiceResponse = null;

                var token = _message["source_app"]["source_agency_uid"];

                var agencyDatabaseMasterListEntities = _uAAgencyDatabaseMasterList.GetAgencyDatabaseList();

                if (token!=null)
                {
                    Guid source_agency_uid = new Guid(token.ToString());

                    var hhaDatabaseDetail = agencyDatabaseMasterListEntities.Where(x => x.Publish_UID == source_agency_uid).FirstOrDefault();

                    if (hhaDatabaseDetail != null)
                    {
                        _actionContext.HHA = hhaDatabaseDetail.HHA_ID;
                        _actionContext.Publish_UID = hhaDatabaseDetail.Publish_UID;
                    }
                    else
                    {
                        return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Source Agency UID is not correct");
                    }
                }
                else
                {
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed,"Source Agency UID is not correct");
                }

                CareTrackerRouteKeyEnums _searchRouteKey = (CareTrackerRouteKeyEnums)System.Enum.Parse(typeof(CareTrackerRouteKeyEnums), _routeKey);

                switch (_searchRouteKey)
                {
                    case CareTrackerRouteKeyEnums.visit_checkedin:
                        _subscriberServiceResponse = await _visitCheckinSubscriberService.VisitCheckinService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_checkedout:
                        _subscriberServiceResponse = await _visitCheckoutSubscriberService.VisitCheckoutService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_checkedinout:
                        _subscriberServiceResponse = await _visitCheckinoutSubscriberService.VisitCheckinoutService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_adjudicated:
                        _subscriberServiceResponse = await _visitAdjudicateSubscriberService.VisitAdjudicateService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_missed:
                        _subscriberServiceResponse = await _visitMissedSubscriberService.VisitMissedService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_undo_checkin:
                        _subscriberServiceResponse = await _visitUndoCheckinSubscriberService.VisitUndoCheckinService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_undo_checkout:
                        _subscriberServiceResponse = await _visitUndoCheckoutSubscriberService.VisitUndoCheckoutService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_undo_checkinout:
                        _subscriberServiceResponse = await _visitUndoCheckinoutSubscriberService.VisitUndoCheckinoutService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_undo_adjudicate:
                        _subscriberServiceResponse = await _visitUndoAdjudicateSubscriberService.VisitUndoAdjudicateService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.visit_undo_missed:
                        _subscriberServiceResponse = await _visitUndoMissedSubscriberService.VisitUndoMissedVisitService(_message);
                        break;
                    case CareTrackerRouteKeyEnums.VisitUpdated:
                    case CareTrackerRouteKeyEnums.update_schedule:
                        _subscriberServiceResponse = await _visitModifiedSubscriberService.VisitModifiedService(_message);
                        break;
                    default:
                        return Response.Fail<bool>(ResponseErrorCodes.InvalidInput, "Failed to Save due to mismatch in route Key");
                }

                if (_subscriberServiceResponse.http_status_code == System.Net.HttpStatusCode.OK)
                    ret = _subscriberServiceResponse.response_payload;
                else
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, _subscriberServiceResponse.error_message);

                return Response.Ok<bool>(ret);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, ex.Message);
            }
        }

    }
}

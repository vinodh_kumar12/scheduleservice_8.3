﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Common.Behaviors;
using Scheduleservice.Core.Interfaces.Services.Subscriber;
using Scheduleservice.Subscriber.Caretracker.Services.VisitSusbscriber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Subscriber.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServicesSubscriberCollection(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddScoped<IVisitCheckinSubscriberService, VisitCheckInSubscriberService>();
            services.AddScoped<IVisitCheckOutSubscriberService, VisitCheckOutSubscriberService>();
            services.AddScoped<IVisitCheckInOutSubscriberService, VisitCheckInOutSubscriberService>();
            services.AddScoped<IVisitMissedSubscriberService, VisitMissedSubscriberService>();
            services.AddScoped<IVisitAdjudicateSubscriberService, VisitAdjudicateSubscriberService>();

            services.AddScoped<IVisitUndoCheckInSubscriberService, VisitUndoCheckInSubscriberService>();
            services.AddScoped<IVisitUndoCheckOutSubscriberService, VisitUndoCheckOutSubscriberService>();
            services.AddScoped<IVisitUndoCheckInOutSubscriberService, VisitUndoCheckInOutSubscriberService>();
            services.AddScoped<IVisitUndoMissedSubscriberService, VisitUndoMissedSubscriberService>();
            services.AddScoped<IVisitUndoAdjudicateSubscriberService, VisitUndoAdjudicateSubscriberService>();

            services.AddScoped<IVisitModifiedSubscriberService, VisitModifiedSubscriberService>();

            return services;
        }
    }
}

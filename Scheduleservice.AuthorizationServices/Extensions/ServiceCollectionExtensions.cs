﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.AuthorizationServices.PaymentsourceAuthorization.Service;
using Scheduleservice.Core.Interfaces.Services.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.AuthorizationServices.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServicesAuthServiceCollection(this IServiceCollection services)
        {

            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddScoped<IAuthorization, Authorization>();


            return services;
        }
    }
}

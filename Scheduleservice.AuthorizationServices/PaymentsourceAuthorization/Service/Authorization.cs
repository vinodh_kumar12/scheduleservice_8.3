﻿namespace Scheduleservice.AuthorizationServices.PaymentsourceAuthorization.Service
{
    using Newtonsoft.Json;
    using Scheduleservice.Core.Common.Extensions;
    using Scheduleservice.Core.DTO.Authorization;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Enums;
    using Scheduleservice.Core.Interfaces.RepositoryContracts;
    using Scheduleservice.Core.Interfaces.Services.Schedule;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.Models.Authorization;
    using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
    using Scheduleservice.Core.SQLQueryBuilder;
    using System;
    using System.Threading.Tasks;

    public class Authorization : IAuthorization
    {
        private readonly IPaymentSourceAuthorizationRepository _paymentSourceAuthorizationRepository;
        private readonly IPaymentsourcesRepository _paymentsourcesRepository;        
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHModificationActivityLog _hModificationActivityLog;
        public Authorization(IPaymentSourceAuthorizationRepository paymentSourceAuthorizationRepository,
            IPaymentsourcesRepository paymentsourcesRepository, 
            IRepositoryFilterConditions repositoryFilterConditions,
            IHModificationActivityLog hModificationActivityLog)
        {
            this._paymentSourceAuthorizationRepository = paymentSourceAuthorizationRepository;
            this._paymentsourcesRepository = paymentsourcesRepository; 
            this._repositoryFilterConditions = repositoryFilterConditions;
            this._hModificationActivityLog = hModificationActivityLog;
        }

        public async Task<AuthInfoDto> ValidateScheduleAuth(int HHA, int UserID, int CgtasKID, EditScheduleProperties caregivertasksEntity, bool confirm_softwarning)
        {
            int result = 0;
            AuthInfoDto authInfoDto = new AuthInfoDto();

            if (caregivertasksEntity.AUTHORIZATION_ID > 0)
            {
                PaymentSourceAuthorizationsEntity newAuthInfo = new PaymentSourceAuthorizationsEntity();
                //get new auth info 
                var newAuthInfoFilters = _repositoryFilterConditions.EqualFilter("AUTHORIZATION_ID", caregivertasksEntity.AUTHORIZATION_ID, FilterConditionDataTypeEnums.integerType).Build();

                newAuthInfo = await _paymentSourceAuthorizationRepository.GetAuthInfo(HHA, UserID, newAuthInfoFilters, "");
                float units = caregivertasksEntity.Units ?? 0;
                if (newAuthInfo.isAuthHourlyBased == true)
                    units = caregivertasksEntity.TotalHours??0;

                //verify whether exisiting Auth is sufficient
                result = await _paymentSourceAuthorizationRepository.CanModifiedSchedluleWithSameAuth(HHA,UserID, caregivertasksEntity.AUTHORIZATION_ID??0,
                        CgtasKID, caregivertasksEntity.PLANNED_DATE.ToDateFormat(), caregivertasksEntity.SERVICECODE_ID??0,
                        units); 
            }

            if (result != 1)
            {
                authInfoDto = await _paymentSourceAuthorizationRepository.GetAvailableAuthID(HHA, UserID,  0, caregivertasksEntity.CLIENT_ID??0,
                        caregivertasksEntity.PAYMENT_SOURCE??0, caregivertasksEntity.SERVICECODE_ID??0, caregivertasksEntity.PLANNED_DATE.ToDateFormat(),
                        caregivertasksEntity.Units ?? 0, false, confirm_softwarning, caregivertasksEntity.TotalHours ?? 0, true);


                if (authInfoDto.iserror)
                {
                    var payerinfo = _paymentsourcesRepository.GetPaymentSourceBasicInfo(HHA, UserID, caregivertasksEntity.PAYMENT_SOURCE??0).Result;

                    if ((payerinfo.Auth_RuleOnNonAuth == 1 )
                       ||
                       (payerinfo.Auth_RuleOnNonAuth == 2 && caregivertasksEntity.STATUS == ScheduleStatusEnums.Approved.ToString())
                       ||
                       (payerinfo.Auth_RuleOnNonAuth == 3 && caregivertasksEntity.IS_BILLED == true)
                       ||
                       (payerinfo.Auth_RuleOnNonAuth == 5 && caregivertasksEntity.STATUS != ScheduleStatusEnums.Planned.ToString())
                      )
                    {
                        authInfoDto.iserror = true;

                        switch (authInfoDto.error_id)
                        { 
                          case -4:
                                if (!payerinfo.Auth_DailyLimit_HardStop)
                                    authInfoDto.is_softwarning = true;
                                break;
                          case -5:
                                if (!payerinfo.Auth_WeeklyLimit_HardStop)
                                    authInfoDto.is_softwarning = true;
                                break;
                          case -6:
                                if (!payerinfo.Auth_MonthlyLimit_HardStop)
                                    authInfoDto.is_softwarning = true;
                                break;
                          default: 
                                break;
                        }  
                    }
                    else
                    {
                        authInfoDto.iserror = false;
                    }
                }                
            }
            else {
                authInfoDto.AuthID = caregivertasksEntity.AUTHORIZATION_ID;
            }

            return authInfoDto;
        }

        public async Task<bool> UpdateAuthorizationInfo(int HHA, int UserID, EditScheduleProperties editScheduleProperties, CaregivertasksEntity oldscheduleInfo=null)
        {
            bool issuccess = true;
            EditAuthorizationProperties editAuthorizationProperties = new EditAuthorizationProperties();

            PaymentSourceAuthorizationsEntity oldAuthInfo = new PaymentSourceAuthorizationsEntity();
            PaymentSourceAuthorizationsEntity newAuthInfo = new PaymentSourceAuthorizationsEntity();
            //var AuthInfoColumns = " AUTHORIZATION_ID,UNUSED,IsAuthVisitBased,isAuthHourlyBased,NUMBER_OF_VISITS ";
            var AuthInfoColumns = "";
            if(oldscheduleInfo==null)
            {
                oldscheduleInfo = new CaregivertasksEntity();
            }
            if (oldscheduleInfo.AUTHORIZATION_ID > 0)
            {
                //get old auth info
                var oldAuthInfoFilters = _repositoryFilterConditions.EqualFilter("AUTHORIZATION_ID", oldscheduleInfo.AUTHORIZATION_ID, FilterConditionDataTypeEnums.integerType).Build(); 
                oldAuthInfo = await _paymentSourceAuthorizationRepository.GetAuthInfo(HHA, UserID, oldAuthInfoFilters, AuthInfoColumns);
            }

            if (editScheduleProperties.AUTHORIZATION_ID > 0 && oldscheduleInfo.AUTHORIZATION_ID != editScheduleProperties.AUTHORIZATION_ID)
            {
                //get new auth info 
                var newAuthInfoFilters = _repositoryFilterConditions.EqualFilter("AUTHORIZATION_ID", editScheduleProperties.AUTHORIZATION_ID, FilterConditionDataTypeEnums.integerType).Build();

                newAuthInfo = await _paymentSourceAuthorizationRepository.GetAuthInfo(HHA, UserID, newAuthInfoFilters, AuthInfoColumns);

            }
            else if (editScheduleProperties.AUTHORIZATION_ID > 0) 
                newAuthInfo = oldAuthInfo; 
            

            //if old auth is available but new Auth is not available, then deallocate existing auth
            if (oldscheduleInfo.AUTHORIZATION_ID > 0 && Convert.ToInt32(editScheduleProperties.AUTHORIZATION_ID) == 0)
            {
                issuccess = await DeallocateAuthorization(HHA, UserID, editAuthorizationProperties, oldAuthInfo, editScheduleProperties, oldscheduleInfo);
            }
            else if (Convert.ToInt32(oldscheduleInfo.AUTHORIZATION_ID) == 0 && editScheduleProperties.AUTHORIZATION_ID > 0)
            {//if old auth is not available and new auth is available, then attach new Auth 
                issuccess = await AllocateAuthorization(HHA, UserID, editAuthorizationProperties, newAuthInfo, editScheduleProperties, oldscheduleInfo);
            }
            else if (oldscheduleInfo.AUTHORIZATION_ID == editScheduleProperties.AUTHORIZATION_ID && editScheduleProperties.AUTHORIZATION_ID > 0)
            {//if old auth and new Auth is same then update unsed count
               
                editAuthorizationProperties.AUTHORIZATION_ID = editScheduleProperties.AUTHORIZATION_ID;
                if (newAuthInfo.isAuthHourlyBased == true)
                {
                    //reduce existing Auth 
                    editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED + (editScheduleProperties.TotalHours - oldscheduleInfo.TotalHours); 
                }
                else if (newAuthInfo.IsAuthVisitBased == true)
                {
                    //reduce existing Auth 
                    editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED;                    
                }
                else
                    editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED + (editScheduleProperties.Units - oldscheduleInfo.Units);


                issuccess = await AllocateAuthorization(HHA, UserID, editAuthorizationProperties, oldAuthInfo, editScheduleProperties, oldscheduleInfo);
                 
            }
            else if (oldscheduleInfo.AUTHORIZATION_ID != editScheduleProperties.AUTHORIZATION_ID && editScheduleProperties.AUTHORIZATION_ID > 0)
            {//if old and new Auth is different then deallocate from old auth and allocate to new Auth

                //deallocate existing Auth
                issuccess = await DeallocateAuthorization(HHA, UserID, editAuthorizationProperties, oldAuthInfo, editScheduleProperties, oldscheduleInfo);

                //allocate new Auth
                if(issuccess)
                    issuccess = await AllocateAuthorization(HHA, UserID, editAuthorizationProperties, oldAuthInfo, editScheduleProperties, oldscheduleInfo);

            }   
            return issuccess;
        }

        public async Task<bool> AllocateAuthorization(int HHA, int UserID, EditAuthorizationProperties editAuthorizationProperties, PaymentSourceAuthorizationsEntity newAuthInfo,
            EditScheduleProperties editScheduleProperties, CaregivertasksEntity oldscheduleInfo)
        {
            bool isallocated = true;

            editScheduleProperties.AUTHORIZATION_ID = newAuthInfo.AUTHORIZATION_ID;
            if (newAuthInfo.isAuthHourlyBased == true)
            {
                editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED - editScheduleProperties.TotalHours;
            }
            else if (newAuthInfo.IsAuthVisitBased == true)
            {
                editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED - 1;                
            }
            else
                editAuthorizationProperties.UNUSED = newAuthInfo.UNUSED - editScheduleProperties.Units;


            var updatedAuthColumns = new BuildUpdateQueryBasedOnEntity<PaymentSourceAuthorizationsEntity>().GetBuildUpdateQueryStatementBasedOnEntity(editAuthorizationProperties, newAuthInfo);

            if (!string.IsNullOrEmpty(updatedAuthColumns.columns))
            {
                var updatefilters = _repositoryFilterConditions.EqualFilter("AUTHORIZATION_ID", editScheduleProperties.AUTHORIZATION_ID, FilterConditionDataTypeEnums.integerType).Build();
                isallocated = await _paymentSourceAuthorizationRepository.UpdateAuthoriaztionInfo(HHA, UserID, updatefilters, updatedAuthColumns.columns);
                 
                string Audit = " Authorization " + newAuthInfo.AUTHORIZATION_NO == "" ? "" : "with Auth#:'" + newAuthInfo.AUTHORIZATION_NO + "' is attached.";
                if (isallocated && !string.IsNullOrEmpty(Audit))
                {
                    isallocated = await AuditAuth(HHA, UserID, oldscheduleInfo.CGTASK_ID, Audit);
                }
            } 
            return isallocated;
        }

        public async Task<bool> DeallocateAuthorization(int HHA, int UserID, EditAuthorizationProperties editAuthorizationProperties, PaymentSourceAuthorizationsEntity oldAuthInfo,
            EditScheduleProperties editScheduleProperties, CaregivertasksEntity oldscheduleInfo)
        {
            bool isDeallocated = true;             

            editAuthorizationProperties.AUTHORIZATION_ID = oldscheduleInfo.AUTHORIZATION_ID;

            //dealocate and update add unsed
            if (oldAuthInfo.isAuthHourlyBased == true)
            {
                editAuthorizationProperties.UNUSED = oldAuthInfo.UNUSED + editScheduleProperties.TotalHours;
            }
            else if (oldAuthInfo.IsAuthVisitBased == true)
            {
                editAuthorizationProperties.UNUSED = oldAuthInfo.UNUSED + 1;                
            }
            else
                editAuthorizationProperties.UNUSED = oldAuthInfo.UNUSED + editScheduleProperties.Units;


            var updatedAuthColumns = new BuildUpdateQueryBasedOnEntity<PaymentSourceAuthorizationsEntity>().GetBuildUpdateQueryStatementBasedOnEntity(editAuthorizationProperties, oldAuthInfo);
            if (!string.IsNullOrEmpty(updatedAuthColumns.columns))
            {
                var updatefilters = _repositoryFilterConditions.EqualFilter("AUTHORIZATION_ID", editAuthorizationProperties.AUTHORIZATION_ID, FilterConditionDataTypeEnums.integerType).Build();
                 isDeallocated = await _paymentSourceAuthorizationRepository.UpdateAuthoriaztionInfo(HHA, UserID, updatefilters, updatedAuthColumns.columns);
                 
                string Audit = " Authorization " + oldAuthInfo.AUTHORIZATION_NO == "" ? "" : "with Auth#: '" + oldAuthInfo.AUTHORIZATION_NO + "' is detached.";
                if (isDeallocated && !string.IsNullOrEmpty(Audit))
                {
                    isDeallocated = await AuditAuth(HHA, UserID, oldscheduleInfo.CGTASK_ID, Audit);
                }
            }
            return isDeallocated;
        }

        public async Task<bool> AuditAuth(int HHA, int UserID, int CgtaskID, string audit)
        {
            bool issucess = true;

            dynamic AuditInfo = new HModificationActivityLogEntity();
            AuditInfo.ActivityName = "Modify Schedule";
            AuditInfo.ActivityType = 6;
            AuditInfo.RecordIdentityName = "CGTASK_ID";
            AuditInfo.RecordIdentityValue = CgtaskID;
            AuditInfo.isSingleActivity = true;
            AuditInfo.Category = "Schedule";
            AuditInfo.Description = "Modified Schedule " + audit;

            AuditInfo.UserDefined2 = "api/schedules/v1/schedules";

            string AuditInfoJSON = JsonConvert.SerializeObject(AuditInfo);
            var result = await _hModificationActivityLog.AddAuditData(HHA, UserID, AuditInfoJSON);

            if (result == 1)
                issucess = false;

            return issucess;
        } 
    }
}

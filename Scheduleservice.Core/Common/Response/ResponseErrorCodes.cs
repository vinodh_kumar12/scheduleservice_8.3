﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Response
{
     
    public static class ResponseErrorCodes
    {
        public static string InvalidInput = "INVAILD_INPUT";
        public static string ValidationFailed = "VALIDATION_FAILED";
        public static string InternalServerFailed = "INTERNAL_SERVER_FAILED";
        public static string SoftWarningError = "VALIDATION_WARNING";
    }
}

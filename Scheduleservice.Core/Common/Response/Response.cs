﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Scheduleservice.Core.Common.Response
{
    public static class Response
    {
        public static Response<T> Ok<T>(T response_payload) => new Response<T>(response_payload, "", "", true, HttpStatusCode.OK, null);
        public static Response<T> NoContent<T>(T response_payload) => new Response<T>(response_payload, "", "", true, HttpStatusCode.NoContent,null);
        public static Response<T> Created<T>(T response_payload) => new Response<T>(response_payload, "", "", true, HttpStatusCode.Created,null);
        public static Response<T> ValidationError<T>( string error_message, T response_payload = default, string error_details = default) => new Response<T>(response_payload, ResponseErrorCodes.ValidationFailed, error_message, false, HttpStatusCode.BadRequest, error_details);
        public static Response<T> Fail<T>(string error_code, string error_message, HttpStatusCode http_status_code = HttpStatusCode.InternalServerError, T response_payload = default) => new Response<T>(response_payload, error_code, error_message, false, http_status_code, null);
        public static Response<T> SoftWarningError<T>(string error_message, string error_details = default, T response_payload = default) => new Response<T>(response_payload, ResponseErrorCodes.SoftWarningError, error_message, false, HttpStatusCode.Continue, error_details);

    }
    public class Response<T>
    {
        public Response(T response_payload, string error_code, string error_message, bool isValid, HttpStatusCode http_status_code, string error_details)
        {
            this.response_payload = response_payload;
            this.error_code = error_code;
            this.error_message = error_message;
            this.isValid = isValid;
            this.http_status_code = http_status_code;
            this.error_details = error_details;
        }
        public T response_payload { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public string error_details { get; set; }
        public bool isValid { get; set; }
        public HttpStatusCode http_status_code { get; set; }
    }
}

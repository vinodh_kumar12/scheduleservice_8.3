﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Request
{
    public class SortbyColumn
    {
        public string sort_field { get; set; }
        public bool is_ascending_filed { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Common.Request
{
    public class BaseRequest
    {
        public int HHA { get; set; }
        public int UserId { get; set; }
        public int PAGEID { get; set; }
        //public string AgencyLoginID { get; set; }
        //public string superAdminLoginID { get; set; } 

    }
}

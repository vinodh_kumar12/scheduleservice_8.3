﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.DTO.Application;
using Serilog.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Behaviors
{
    public class SerilogMiddleware<TIn, TOut> : IPipelineBehavior<TIn, TOut>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SerilogMiddleware(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<TOut> Handle(TIn request, CancellationToken cancellationToken, RequestHandlerDelegate<TOut> next)
        {
            var _actionContext = (ActionContextDto)_httpContextAccessor.HttpContext.Items[ConstantKeys.ActionContext];
            string _instanceCode = _httpContextAccessor.HttpContext.Request.Headers[HttpHeaderKeys.InstanceCode];

            var builder = new StringBuilder(Environment.NewLine);
            foreach (var header in _httpContextAccessor.HttpContext.Request.Headers)
            {
                builder.AppendLine($"{header.Key}: {header.Value}");
            }
            var headersDump = builder.ToString();

            LogContext.PushProperty("UniqueRequestId", _actionContext.UniqueRequestId);
            LogContext.PushProperty("HHA", _actionContext.HHA);
            LogContext.PushProperty("QueryString", _httpContextAccessor.HttpContext.Request.QueryString.ToString());
            LogContext.PushProperty("RequestHeaders", headersDump);
            LogContext.PushProperty("InstanceCode", _instanceCode);
            return await next();
        }
    }
}

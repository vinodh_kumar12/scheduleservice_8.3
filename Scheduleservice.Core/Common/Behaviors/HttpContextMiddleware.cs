﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.DTO.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Behaviors
{
    public class HttpContextMiddleware<TIn, TOut> : IPipelineBehavior<TIn, TOut>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ActionContextDto _actionContext;
        public HttpContextMiddleware(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<TOut> Handle(TIn request, CancellationToken cancellationToken, RequestHandlerDelegate<TOut> next)
        {

            InitiateActionContext();
            SetBasicParams(_httpContextAccessor.HttpContext, request);           
            SetActionContext(_httpContextAccessor.HttpContext);
            AddResponseHeaders(_httpContextAccessor.HttpContext); 
            var result = await next();

            return result;
        }



        private void InitiateActionContext()
        {
            _actionContext = new ActionContextDto();
        }

        private void SetBasicParams(HttpContext context, TIn request)
        {
            int hha = 0;
            int user = 0;
            if (_httpContextAccessor.HttpContext.User.Claims.Any())
            {
                var hhaClaim = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == HttpHeaderKeys.HHA).FirstOrDefault().Value;
                var userIdClaim = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == HttpHeaderKeys.User).FirstOrDefault().Value;
                if (request is BaseRequest br)
                {
                    br.UserId = Convert.ToInt32(userIdClaim);
                    br.HHA = Convert.ToInt32(hhaClaim);
                }

            }
            else
            {
                var hhaHeader = context.Request.Headers[HttpHeaderKeys.HHA];
                var loginUserHeader = context.Request.Headers[HttpHeaderKeys.User];
                var InstanceCodeHeader = context.Request.Headers[HttpHeaderKeys.InstanceCode];

                if (!string.IsNullOrWhiteSpace(hhaHeader))
                {
                    if (!int.TryParse(hhaHeader, out hha))
                        throw new Exception("Invalid HHA");
                }
                else if(context.Request.Path != @"/subscriber/messages/from/crt"
                        && context.Request.Path != @"/schedule/evv/exceptions/auto_generate"
                        && context.Request.Path != @"/schedule/evv-terminal-payer/auto-register"
                       )
                    throw new Exception("HHA should not be empty");

                if (!string.IsNullOrWhiteSpace(loginUserHeader))
                {
                    if (!int.TryParse(loginUserHeader, out user))
                        throw new Exception("Invalid User");
                }
                else if (context.Request.Path != @"/subscriber/messages/from/crt"
                        && context.Request.Path != @"/schedule/evv/exceptions/auto_generate"
                        && context.Request.Path != @"/schedule/evv-terminal-payer/auto-register"
                       )
                    throw new Exception("User should not be empty");

                if (string.IsNullOrWhiteSpace(InstanceCodeHeader))
                    throw new Exception("Instance Code should not be empty");

                if (request is BaseRequest br)
                {
                    if (!String.IsNullOrEmpty(loginUserHeader))
                        br.UserId = Convert.ToInt32(loginUserHeader);
                    if (!String.IsNullOrEmpty(hhaHeader))
                        br.HHA = Convert.ToInt32(hhaHeader);
                }
            }

            _actionContext.HHA = hha;
            _actionContext.User = user;
            _actionContext.UniqueRequestId = Guid.NewGuid().ToString();

            var instanceCoderHeader = _httpContextAccessor.HttpContext.Request.Headers[HttpHeaderKeys.InstanceCode];
              _actionContext.InstanceCode = instanceCoderHeader;
        }

       
        private void SetActionContext(HttpContext context)
        {
            context.Items.Add(ConstantKeys.ActionContext, _actionContext);
        }

        private void AddResponseHeaders(HttpContext context)
        {
            context.Response.OnStarting(() =>
            {
                context.Response.Headers.Add("unique-request-id", _actionContext.UniqueRequestId);
                return Task.FromResult(0);
            });
        }
    }
}
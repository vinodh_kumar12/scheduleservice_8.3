﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Helpers
{
    public static class localTimeZones
    {
        public const string AKST = "Alaskan Standard Time";
        public const string AST = "Atlantic Standard Time";
        public const string CST = "Central Standard Time";
        public const string EST = "Eastern Standard Time";
        public const string HST = "Hawaiian Standard Time";
        public const string MST = "Mountain Standard Time";
        public const string MST2 = "US Mountain Standard Time";
        public const string PST = "Pacific Standard Time";
        public const string IST = "India Standard Time";
    }

    public static class TimeZones
    {
        public const string AKST = "Alaska Standard Time";
        public const string AST = "Atlantic Standard Time";
        public const string CST = "Central Standard Time";
        public const string EST = "Eastern Standard Time";
        public const string HST = "Hawaii-Aleutian Standard Time";
        public const string MST = "Mountain Standard Time";
        public const string MST2 = "Mountain Standard Time";
        public const string PST = "Pacific Standard Time";
        public const string IST = "India Standard Time";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Common.Helpers
{
    public static class ConstantKeys
    {
        public const string ActionContext = "ActionContext";
    }

    public static class HttpHeaderKeys
    {
        public const string HHA = "hha";
        public const string User = "user";
        public const string InstanceCode = "instance_code";
    }
}

﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text; 
using System.Linq.Dynamic.Core;

namespace Scheduleservice.Core.Common.Extensions
{
    public static class SortingList<T>
    {
        public static IQueryable<T>  ApplySort(IQueryable<T> entities, IEnumerable<SortbyColumn> sortbyColumns)
        {
            if (!entities.Any())
                return entities;
            if (!sortbyColumns.Any())
            {
                return entities;
            }

            var propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var orderQueryBuilder = new StringBuilder();
            foreach (var param in sortbyColumns)
            {
                if (string.IsNullOrWhiteSpace(param.sort_field))
                    continue;
                var propertyFromQueryName = param.sort_field;
                var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name.Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));
                if (objectProperty == null)
                    continue;
                var sortingOrder = param.is_ascending_filed ? "ascending" : "descending";
                orderQueryBuilder.Append($"{objectProperty.Name.ToString()} {sortingOrder}, ");
            }
            var orderQuery = orderQueryBuilder.ToString().TrimEnd(',', ' ');
            if (orderQuery == "")
            {
                return entities;
            }
            else
            {
                return entities.OrderBy(orderQuery);
            }
            
        }
    }
}

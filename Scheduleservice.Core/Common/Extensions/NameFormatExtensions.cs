﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Common.Extensions
{
    public static class NameFormatExtensions
    {
        public static string ClientNameFormat(this string[] ClientNamePatientIDInfo)
        {
            StringBuilder FormatedClientName = new StringBuilder();
            if (ClientNamePatientIDInfo.Length > 0 && ClientNamePatientIDInfo[0] != null)
                FormatedClientName.Append(ClientNamePatientIDInfo[0]);
            if (ClientNamePatientIDInfo.Length > 1 && ClientNamePatientIDInfo[1] != null)
                FormatedClientName.Append(", " + ClientNamePatientIDInfo[1]);
            if (ClientNamePatientIDInfo.Length > 2 && ClientNamePatientIDInfo[1] != null)
                FormatedClientName.Append(" (" + ClientNamePatientIDInfo[2] + ")");

            return FormatedClientName.ToString();
        }
        public static string ClinicianNameFormat(this string[] ClinicianInfo)
        {
            StringBuilder FormatedClinicianName =new StringBuilder();
            if (ClinicianInfo.Length > 0 && ClinicianInfo[0] != null)
                FormatedClinicianName.Append(ClinicianInfo[0]);
            if (ClinicianInfo.Length > 1 && ClinicianInfo[1] != null)
                FormatedClinicianName.Append(", " + ClinicianInfo[1]);
            if (ClinicianInfo.Length > 2 && ClinicianInfo[1] != null)
                FormatedClinicianName.Append(" (" + ClinicianInfo[2] + ")"); 
            
            return FormatedClinicianName.ToString();
        }
    }
}

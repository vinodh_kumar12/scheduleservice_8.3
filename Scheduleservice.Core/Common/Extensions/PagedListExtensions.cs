﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Common.Extensions
{
    public class PagedList<T> : List<T>
    {
        public int current_page { get; private set; }
        public int total_pages { get; private set; }
        public int page_size { get; private set; }
        public int total_count { get; private set; }
        public int number_records_in_this_page { get; private set; }

        public bool HasPrevious => current_page > 1;
        public bool HasNext => current_page < total_pages;

        public PagedList(List<T> items, int count, int pageNumber, int pageSize)
        {
            total_count = count;
            page_size = pageSize;
            current_page = pageNumber;
            total_pages = (int)Math.Ceiling(count / (double)pageSize);
            number_records_in_this_page = items.Count;
            AddRange(items);
        }

        public static PagedList<T> ToPagedList(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return new PagedList<T>(items, count, pageNumber, pageSize);
        }
    }
}

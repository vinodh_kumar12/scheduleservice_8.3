﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq; 

namespace Scheduleservice.Core.Common.Extensions
{
    public static class ExpressionExtensions
    { 
        public static IDictionary<string, object> DynamicSelectGenerator(this object a, IEnumerable<string> props)
        {
            if (a == null)
            {
                return null;
            }
            IDictionary<string, object> res = new ExpandoObject();
            var type = a.GetType();
            foreach (var pair in props.Select(n => new {
                Name = n, Property = type.GetProperty(n)
            }))
            {
                res[pair.Name] = pair.Property.GetValue(a, new object[0]);
            }
            return res;
        }

        public static T[] Append<T>(this T[] array, T item)
        {
            if (array == null)
            {
                return new T[] { item };
            }
            T[] result = new T[array.Length + 1];
            array.CopyTo(result, 0);
            result[array.Length] = item;
            return result;
        }

    }
}

﻿using FluentValidation;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Common.Extensions
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilder<T, string> Json_input<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            var options = ruleBuilder.NotNull().Must((x)=>x.ValidateJSON()).WithMessage("Input should be json format");
                
            return options;
        }

        public static bool ValidateJSON(this string data)
        {
            if(String.IsNullOrEmpty(data))
            {
                return false;
            }

            data = data.Trim();
            try
            {
                if (data.StartsWith("{") && data.EndsWith("}"))
                {
                    var token = JToken.Parse(data);
                    if (!((token == null) ||(token.Type == JTokenType.Array && !token.HasValues) ||(token.Type == JTokenType.Object && !token.HasValues) ||(token.Type == JTokenType.String && token.ToString() == String.Empty) ||(token.Type == JTokenType.Null)))
                    {
                        //parse the input into a JObject
                        var jObject = JObject.Parse(data);

                        foreach (var jo in jObject)
                        {
                            string name = jo.Key;
                            JToken value = jo.Value;

                            //if the element has a missing value, it will be Undefined - this is invalid
                            if (value.Type == JTokenType.Undefined)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (data.StartsWith("[") && data.EndsWith("]"))
                {
                    JArray.Parse(data);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

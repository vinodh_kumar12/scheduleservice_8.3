﻿using Scheduleservice.Core.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scheduleservice.Core.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToTimeAMPMFormat(this DateTime? dateTime)
        {
            if (dateTime == null)
                return "";

            return string.Format("{0:hh:mm tt}", dateTime);
        }
        public static string ToTimeAMPMFormat(this DateTime dateTime)
        {
            return string.Format("{0:hh:mm tt}", dateTime);
        }

        public static string ToDateFormat(this DateTime? dateTime)
        {
            if (dateTime == null)
                return "";
            DateTime currentDatetime = Convert.ToDateTime("01/01/1900");
            return (dateTime?? currentDatetime).ToString("MM/dd/yyyy");
        }
        public static string ToDateFormat(this DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy");
        }

        public static DateTime ToConvertUTCTimeZone(this DateTime dateTime, string timeZone)
        {
            var ZoneID = typeof(TimeZones).GetField(timeZone).GetValue(null).ToString();
            TimeZoneInfo time_zone_info;
            var system_time_zones = TimeZoneInfo.GetSystemTimeZones();

            if (system_time_zones.Any(x => x.StandardName == ZoneID))
            {
                ZoneID = system_time_zones.Where(x => x.StandardName == ZoneID).Select(x => x.Id).FirstOrDefault();
                time_zone_info = TimeZoneInfo.FindSystemTimeZoneById(ZoneID);
                dateTime = TimeZoneInfo.ConvertTime(dateTime, time_zone_info);
            }
            else
            {
                ZoneID = typeof(localTimeZones).GetField(timeZone).GetValue(null).ToString();
                if (system_time_zones.Any(x => x.StandardName == ZoneID))
                {
                    ZoneID = system_time_zones.Where(x => x.StandardName == ZoneID).Select(x => x.Id).FirstOrDefault();
                    time_zone_info = TimeZoneInfo.FindSystemTimeZoneById(ZoneID);
                    dateTime = TimeZoneInfo.ConvertTime(dateTime, time_zone_info);
                }
            }            

            return dateTime;
        }

    }
}

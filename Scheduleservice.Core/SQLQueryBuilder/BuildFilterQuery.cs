﻿using System;
using System.Collections.Generic; 
using Newtonsoft.Json; 
using Scheduleservice.Core.Enums;  

namespace Scheduleservice.Core.RepositoryQueryBuilder
{
    public class BuildFilterQuery<T>
    {
        public string GetBuildQueryFilterConditions(string jsonQueryFilter, dynamic NullableColumns = null)
        {
              Dictionary<string, string> QueryFilterProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonQueryFilter);

            var scheduleallFilter = new RepositoryFilterConditions();

            Type t = typeof(T);

            FilterConditionDataTypeEnums filterConditionDataTypeEnums = FilterConditionDataTypeEnums.integerType;
            
            var _nullableColumns = (object)NullableColumns;
            List<string> NegativeparamNames = new List<string>();

            foreach (var column in (string[])_nullableColumns)
                NegativeparamNames.Add(column);
            

            foreach (var FilterConditions in QueryFilterProperties)
            {
                string ColumnName = FilterConditions.Key;
                object ColumnValue = FilterConditions.Value;

                var columnPropertytype = t.GetProperty(ColumnName).PropertyType;

                if (columnPropertytype == typeof(Int32) || columnPropertytype == typeof(Int16) || columnPropertytype == typeof(Int64))
                {
                    filterConditionDataTypeEnums = FilterConditionDataTypeEnums.integerType;
                }
                else if (columnPropertytype == typeof(string))
                {
                    filterConditionDataTypeEnums = FilterConditionDataTypeEnums.stringType;

                }
                else if (columnPropertytype == typeof(DateTime))
                {
                    filterConditionDataTypeEnums = FilterConditionDataTypeEnums.datetimeType;

                }
                else if (columnPropertytype == typeof(Boolean))
                {
                    filterConditionDataTypeEnums = FilterConditionDataTypeEnums.boolType;
                }
                bool isNullable = false;
                if (NegativeparamNames != null)
                {
                    if (NegativeparamNames.Contains(ColumnName))
                        isNullable = true;
                }

                scheduleallFilter.EqualFilter(ColumnName, ColumnValue, filterConditionDataTypeEnums, isNullable);
            }
            return scheduleallFilter.Build();
        }
    }
}

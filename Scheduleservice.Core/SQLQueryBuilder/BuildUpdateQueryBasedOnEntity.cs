﻿using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using Scheduleservice.Services.Schedules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.SQLQueryBuilder
{
    public class BuildUpdateQueryBasedOnEntity<T> where T : class
    {
        private const string appender = " and ";
        public UpdateModel GetBuildUpdateQueryStatementBasedOnEntity(object model, object oldentity)
        {
            //
            UpdateModel scheduleUpdateModel = new UpdateModel();
            var scheduleallUpdateColumn = new RepositoryUpdateColumn();

            StringBuilder Audit = new StringBuilder();
            PropertyInfo[] props = model.GetType().GetProperties();
            Type entity = typeof(T);
            bool IsModified;
            string constructedFieldAudit = "";
            
            foreach (PropertyInfo prp in props)
            {
                object value = prp.GetValue(model, new object[] { });

                if (entity.GetProperty(prp.Name) != null && value != null)
                {
                    var oldvalue = oldentity.GetType().GetProperty(prp.Name).GetValue(oldentity, new object[] { });
                     

                    var type = FilterConditionDataTypeEnums.stringType;
                    if (prp.PropertyType.FullName.Contains("System.Bool"))
                        type = FilterConditionDataTypeEnums.boolType;
                    else if (prp.PropertyType.FullName.Contains("System.Int"))
                        type = FilterConditionDataTypeEnums.integerType;
                    else if (prp.PropertyType.FullName.Contains("System.Single") || prp.PropertyType.FullName.Contains("System.Double"))
                        type = FilterConditionDataTypeEnums.floatType;
                    else if (prp.PropertyType.FullName.Contains("System.DateTime"))
                        type = FilterConditionDataTypeEnums.datetimeType;
                    else if (prp.PropertyType.FullName.Contains("System.Byte"))
                        type = FilterConditionDataTypeEnums.byteType;

                    if (type == FilterConditionDataTypeEnums.stringType && oldvalue == null)
                        oldvalue = "";

                    IsModified = false;
                    if (type == FilterConditionDataTypeEnums.integerType 
                        || 
                        type == FilterConditionDataTypeEnums.floatType
                        ||
                        type == FilterConditionDataTypeEnums.byteType
                       )
                    {
                        if (Convert.ToDouble(oldvalue) != Convert.ToDouble(value) || (oldvalue == null && value != null)) 
                            IsModified = true;  
                    }
                    else if (type == FilterConditionDataTypeEnums.boolType)
                    {
                        if (Convert.ToBoolean(oldvalue) != Convert.ToBoolean(value)) 
                            IsModified = true;  
                    }
                    else if (type == FilterConditionDataTypeEnums.datetimeType)
                    {
                        if (Convert.ToDateTime(oldvalue) != Convert.ToDateTime(value)) 
                            IsModified = true;  
                    }
                    else if (oldvalue.ToString() != value.ToString())
                    {
                        IsModified = true; 
                    }

                    if (IsModified)
                    {
                        scheduleallUpdateColumn.UpdateColumn(prp.Name, value.ToString(), type);
                         
                        //get modified field audit
                        constructedFieldAudit = this.GetAuditInfo(model, value, prp);
                        
                        if (Audit.Length > 0 && constructedFieldAudit.Length > 0)
                            Audit.Append(appender);

                        Audit.Append(constructedFieldAudit);                        
                    }
                }
            }

            scheduleUpdateModel.columns = scheduleallUpdateColumn.Build();
            scheduleUpdateModel.audit = Audit.ToString();

            return scheduleUpdateModel;
        }

        public string GetAuditInfo(object model, object value, PropertyInfo prp)
        {
            string construct;
            if (model.GetType().GetProperty(prp.Name).CustomAttributes.Count() > 0)
            {
                if (value == null)
                {
                    if (prp.PropertyType.FullName.Contains("System.Bool"))
                        construct = model.GetType().GetProperty(prp.Name).CustomAttributes.FirstOrDefault().ConstructorArguments[1].Value.ToString();
                    else
                        construct = model.GetType().GetProperty(prp.Name).CustomAttributes.FirstOrDefault().ConstructorArguments[0].Value.ToString() + " is removed";

                    return "'" + construct + "' ";
                }
                else
                {
                    if (prp.PropertyType.FullName.Contains("System.DateTime")) 
                        value = Convert.ToDateTime(value).ToTimeAMPMFormat(); 

                    if (prp.PropertyType.FullName.Contains("System.Bool"))
                        construct = model.GetType().GetProperty(prp.Name).CustomAttributes.FirstOrDefault().ConstructorArguments[0].Value.ToString();
                    else 
                        construct = model.GetType().GetProperty(prp.Name).CustomAttributes.FirstOrDefault().ConstructorArguments[0].Value.ToString() + " is set to '" + value.ToString() + "'";
                    return construct;
                }
            }
            else
                return "";           
        }
    }
}

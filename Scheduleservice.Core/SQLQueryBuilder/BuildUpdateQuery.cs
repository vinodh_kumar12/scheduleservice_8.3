﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Scheduleservice.Core.Enums;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Scheduleservice.Core.RepositoryQueryBuilder
{
    public class BuildUpdateQuery<T> where T : class
    {
        public string GetBuildUpdateQueryStatement(string jsonQueryUpdate)
        {
            Dictionary<string, string> QueryFilterProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonQueryUpdate);

            var scheduleallUpdateColumn = new RepositoryUpdateColumn();

            Type t = typeof(T);

            FilterConditionDataTypeEnums filterConditionDataTypeEnums = FilterConditionDataTypeEnums.integerType;

            foreach (var FilterConditions in QueryFilterProperties)
            {
                string ColumnName = FilterConditions.Key;
                string ColumnValue = FilterConditions.Value;
                if (t.GetProperty(ColumnName) != null)
                { 
                    var columnPropertytype = t.GetProperty(ColumnName).PropertyType;

                    if (columnPropertytype.FullName.Contains("System.Int"))
                    {
                        filterConditionDataTypeEnums = FilterConditionDataTypeEnums.integerType;
                    }
                    else if (columnPropertytype.FullName.Contains("System.String"))
                    {
                        filterConditionDataTypeEnums = FilterConditionDataTypeEnums.stringType;

                    }
                    else if (columnPropertytype.FullName.Contains("System.DateTime"))
                    {
                        filterConditionDataTypeEnums = FilterConditionDataTypeEnums.datetimeType;

                    }
                    else if (columnPropertytype.FullName.Contains("System.Bool"))
                    {
                        filterConditionDataTypeEnums = FilterConditionDataTypeEnums.boolType;
                    }
                    else if (columnPropertytype.FullName.Contains("System.Single"))
                    {
                        filterConditionDataTypeEnums = FilterConditionDataTypeEnums.floatType;
                    }

                    scheduleallUpdateColumn.UpdateColumn(ColumnName, ColumnValue, filterConditionDataTypeEnums);
                }
            }
            return scheduleallUpdateColumn.Build();
        }
    }
}

﻿using Scheduleservice.Core.Enums;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System.Linq;
using System.Text;

namespace Scheduleservice.Core.RepositoryQueryBuilder
{
    public class RepositoryFilterConditions : IRepositoryFilterConditions
    {
        StringBuilder filters = new StringBuilder();

        public IRepositoryFilterConditions EqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {

            if (ColumnValue != null)
            {
                filters.Append(" And ");


                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " = '" + ColumnValue + "'");

                        break;

                    case FilterConditionDataTypeEnums.boolType:
                           ColumnValue = System.Convert.ToBoolean(ColumnValue) ? "1" : "0";

                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " = " + ColumnValue);
                        break;

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " = Convert(DateTime,'" + ColumnValue + "')");
                        break;
                    case FilterConditionDataTypeEnums.dateType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " = Convert(Date,'" + ColumnValue + "')");
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " = " + ColumnValue);

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions NotEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {

            if (ColumnValue != null)
            {
                filters.Append(" And ");


                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " != '" + ColumnValue + "'");
                        break;

                    case FilterConditionDataTypeEnums.boolType:
                        ColumnValue = (bool)ColumnValue ? "1" : "0";
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " != " + ColumnValue);
                        break;

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " != Convert(DateTime,'" + ColumnValue + "')");
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " != " + ColumnValue);

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions BetweenFilter(string ColumnName, dynamic ColumnFirstValue, dynamic ColumnSecondValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {
            if (ColumnFirstValue != null && ColumnSecondValue != null)
            {
                filters.Append(" And ");


                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " Between '" + ColumnFirstValue + "' And '" + ColumnSecondValue + "'");

                        break;

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " BetWeen Convert(DateTime,'" + ColumnFirstValue + "') And Convert(DateTime,'" + ColumnSecondValue + "')");
                        break;

                    case FilterConditionDataTypeEnums.dateType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " BetWeen " + ColumnFirstValue + " And " + ColumnSecondValue);
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " BetWeen " + ColumnFirstValue+" And "+ ColumnSecondValue);

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions LessThanEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {
            if (ColumnValue != null)
            {
                filters.Append(" And ");


                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " <= '" + ColumnValue + "'");

                        break;

                  

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " <= Convert(DateTime,'" + ColumnValue + "')");
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " <= " + ColumnValue);

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions LessThanFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {
            if (ColumnValue != null)
            {
                filters.Append(" And ");


                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " < '" + ColumnValue + "'");

                        break;
                         

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " < Convert(DateTime,'" + ColumnValue + "')");
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " < " + ColumnValue);

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions GreaterThanEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {
            if (ColumnValue != null)
            {
                filters.Append(" And "); 
                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " >= '" + ColumnValue + "'");

                        break;

                    case FilterConditionDataTypeEnums.boolType:
                        ColumnValue = (bool)ColumnValue ? "1" : "0";
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " >= " + ColumnValue);
                        break;

                    case FilterConditionDataTypeEnums.datetimeType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";

                        filters.Append(ColumnName + " >= Convert(DateTime,'" + ColumnValue + "')");
                        break;

                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " >= " + ColumnValue);

                        break;
                }
            }

            return this;
        }
        public IRepositoryFilterConditions GreaterThanFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {
                if (ColumnValue != null)
                {
                    filters.Append(" And ");


                    switch (DataType)
                    {
                        case FilterConditionDataTypeEnums.stringType:
                            if (isNullable)
                                ColumnName = "isnull(" + ColumnName + ",'')";

                            filters.Append(ColumnName + " > '" + ColumnValue + "'");

                            break;

                        case FilterConditionDataTypeEnums.boolType:
                            ColumnValue = (bool)ColumnValue ? "1" : "0";
                            if (isNullable)
                                ColumnName = "isnull(" + ColumnName + ",0)";

                            filters.Append(ColumnName + " > " + ColumnValue);
                            break;

                        case FilterConditionDataTypeEnums.datetimeType:
                            if (isNullable)
                                ColumnName = "isnull(" + ColumnName + ",'')";

                            filters.Append(ColumnName + " > Convert(DateTime,'" + ColumnValue + "')");
                            break;

                        default:
                            if (isNullable)
                                ColumnName = "isnull(" + ColumnName + ",0)";

                            filters.Append(ColumnName + " > " + ColumnValue);

                            break;
                    }
                }

                return this;
            }

        public IRepositoryFilterConditions InFilter(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable=false)
        {

            if (!string.IsNullOrEmpty(ColumnValue))
            {
                filters.Append(" And ");  
                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";


                        ColumnValue = string.Join("','", ColumnValue.Split(',')
                        .Select(x => x.Trim())
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToArray());

                        filters.Append(ColumnName + " IN ('" + ColumnValue + "')");

                        break;                   

                    
                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " IN (" + ColumnValue + ")");

                        break;
                }
            }

            return this;
        }

        public IRepositoryFilterConditions NotInFilter(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false)
        {

            if (!string.IsNullOrEmpty(ColumnValue))
            {
                filters.Append(" And ");
                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",'')";


                        ColumnValue = string.Join("','", ColumnValue.Split(',')
                        .Select(x => x.Trim())
                        .Where(x => !string.IsNullOrWhiteSpace(x))
                        .ToArray());

                        filters.Append(ColumnName + " NOT IN ('" + ColumnValue + "')");

                        break;


                    default:
                        if (isNullable)
                            ColumnName = "isnull(" + ColumnName + ",0)";

                        filters.Append(ColumnName + " NOT IN (" + ColumnValue + ")");

                        break;
                }
            }

            return this;
        }


        public string Build()
        {
            var Stringfilters = filters.ToString();
            filters = new StringBuilder();
            return Stringfilters;
        }
    }
}

﻿using Scheduleservice.Core.Enums;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.RepositoryQueryBuilder
{
    public class RepositoryUpdateColumn : IRepositoryUpdateColumn
    {
        StringBuilder Columns = new StringBuilder();
        public string Build()
        {
            var StringColumns = Columns.ToString();
            Columns = new StringBuilder();

            return StringColumns;
        }

        public IRepositoryUpdateColumn UpdateColumn(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType)
        {
            if (ColumnValue != null)
            {
                if (Columns.Length>0)
                    Columns.Append(" , ");

                switch (DataType)
                {
                    case FilterConditionDataTypeEnums.stringType:
                        ColumnValue = ColumnValue.Replace("'", "''");
                        Columns.Append(ColumnName + " = '" + ColumnValue + "'");
                        break;
                    case FilterConditionDataTypeEnums.datetimeType:
                        if (ColumnValue == "")
                        {
                            Columns.Append(ColumnName + " = null");
                        }
                        else
                        {
                            Columns.Append(ColumnName + " = '" + ColumnValue + "'");
                        }
                        break;

                    case FilterConditionDataTypeEnums.boolType:
                        ColumnValue = Convert.ToBoolean(ColumnValue) ? "1" : "0";

                        Columns.Append(ColumnName + " = " + ColumnValue + " ");
                        break;
                    case FilterConditionDataTypeEnums.integerType:
                        if (ColumnValue == "")
                        {
                            Columns.Append(ColumnName + " = null");
                        }
                        else
                        {
                            Columns.Append(ColumnName + " = " + ColumnValue + "");
                        }
                        break;
                    default:                        
                        Columns.Append(ColumnName + " = " + ColumnValue );

                        break;
                }
            }

            return this;
        }
    }
}

﻿using Scheduleservice.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.RepositoryQueryBuilder.Contracts
{
    public interface IRepositoryUpdateColumn
    {
        IRepositoryUpdateColumn UpdateColumn(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType);

        string Build();
    }
}

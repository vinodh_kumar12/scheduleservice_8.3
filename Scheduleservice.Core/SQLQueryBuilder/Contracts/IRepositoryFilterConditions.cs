﻿using Scheduleservice.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.RepositoryQueryBuilder.Contracts
{
    public interface IRepositoryFilterConditions
    {
        IRepositoryFilterConditions EqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable=false);
        IRepositoryFilterConditions NotEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions InFilter(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions NotInFilter(string ColumnName, string ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions BetweenFilter(string ColumnName, dynamic ColumnFirstValue1, dynamic ColumnSecondValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions GreaterThanFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions LessThanFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions GreaterThanEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);
        IRepositoryFilterConditions LessThanEqualFilter(string ColumnName, dynamic ColumnValue, FilterConditionDataTypeEnums DataType, bool isNullable = false);

        string Build(); 
    }
}

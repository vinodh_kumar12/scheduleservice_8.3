﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Constants
{
    public static class VisitStatusCRT
    {
        public const string planned = "planned";
        public const string in_progress = "in_progress";
        public const string completed = "completed";
        public const string adjudicated = "adjudicated";
        public const string missed = "missed";
        public const string checkin_due = "checkin_due";
        public const string checkout_due = "checkout_due";
        public const string upcoming_checkin = "upcoming_checkin";
        public const string upcoming_checkout = "upcoming_checkout";
        public const string deleted = "deleted";
    }
}

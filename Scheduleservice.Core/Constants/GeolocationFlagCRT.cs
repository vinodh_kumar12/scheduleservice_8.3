﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Constants
{
    public static class GeolocationFlagCRT
    {
        public const string Inside = "inside";
        public const string Outside = "outside";
        public const string ClientLocationNotAvailable = "client_location_not_available";
        public const string CheckinLocationNotAvailable = "checkin_location_not_available";
    }
}

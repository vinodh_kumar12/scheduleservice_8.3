﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Constants
{
    public static class GeolocationFlagRTB
    {
        public const string WithInLocation = "Y";
        public const string OutsideLocation = "O";
        public const string LocationNotVerified = "N";
    }
}

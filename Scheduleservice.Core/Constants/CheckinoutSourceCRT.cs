﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Constants
{
    public class CheckinoutSourceCRT
    {
        public static string KMobile = "mobile";
        public static string KIce = "kice";
        public static string Online = "online";
        public static string Fob = "fob";
        public static string CDA = "cda";
        public static string Telephone = "telephony";
    }
}

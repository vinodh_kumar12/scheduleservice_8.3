﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Constants
{
    public static class VisitStatusRTB
    {
        public const string Planned = "Planned";
        public const string In_Progress = "In_Progress";
        public const string Completed = "Completed";
        public const string Approved = "Approved";
        public const string MissedVisit = "MissedVisit";
        public const string NotCreated = "NotCreated";
        public const string Deleted = "Deleted";
        public const string ReSchedule = "ReSchedule";
    }
}

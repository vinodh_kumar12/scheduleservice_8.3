﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Interfaces.TaskQueue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Core.TaskQueue
{
    public class QueuedHostedService : BackgroundService
    {
        private readonly ILogger<QueuedHostedService> _logger;

        public ITaskQueue TaskQueue { get; }
        public QueuedHostedService(ILogger<QueuedHostedService> logger, ITaskQueue taskQueue)
        {
            _logger = logger;
            TaskQueue = taskQueue;

        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("BackgroundProcessing -ExecuteAsync");
            await BackgroundProcessing(stoppingToken);

        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Queued Hosted Service is stopping.");

            await base.StopAsync(stoppingToken);
        }

        private async Task BackgroundProcessing(CancellationToken stoppingToken)
        {
            _logger.LogInformation("BackgroundProcessing Stated");
            while (!stoppingToken.IsCancellationRequested)
            {
                var workItem = await TaskQueue.DequeueAsync(stoppingToken);

                try
                {
                    _logger.LogInformation("BackgroundProcessing - workItem Added");

                    await workItem(stoppingToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occurred executing {WorkItem}.", nameof(workItem));
                }
            }
        }
    }
}

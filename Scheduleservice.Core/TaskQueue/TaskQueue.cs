﻿using Scheduleservice.Core.Interfaces.TaskQueue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MediatR;
using System.Threading;
using System.Threading.Tasks;


namespace Scheduleservice.Core.TaskQueue
{
    public class TaskQueue : ITaskQueue
    {
        private ConcurrentQueue<Func<CancellationToken, Task>> _workItems = new ConcurrentQueue<Func<CancellationToken, Task>>();

        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        //private readonly ILogger<TaskQueue> _logger;
        //public PublishTaskQueue(ILogger<TaskQueue> logger)
        //{
        //    _logger = logger;
        //}
        public async Task<Func<CancellationToken, Task>> DequeueAsync(CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            //_logger.LogInformation("DequeueAsync Background Task!");
            _workItems.TryDequeue(out var workItem);

            return workItem;
        }

        public void QueueBackgroundWorkItem(Func<CancellationToken, Task> workItem)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            //_logger.LogInformation("Enqueue Background Task!--Start");

            _workItems.Enqueue(workItem);
            _signal.Release();

            //_logger.LogInformation("Enqueue Background Task!--End");
        }
    }
}

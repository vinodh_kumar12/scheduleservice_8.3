﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Enums
{
    public enum CareTrackerRouteKeyEnums
    {
        visit_checkedin,
        visit_checkedout,
        visit_checkedinout,
        visit_adjudicated,
        visit_missed,
        visit_undo_adjudicate,
        visit_undo_checkout,
        visit_undo_checkin,
        visit_undo_checkinout,
        visit_undo_missed,
        VisitUpdated,
        update_schedule

    }
}

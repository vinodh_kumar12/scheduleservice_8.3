﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Enums
{
   public enum FilterConditionDataTypeEnums
    {
        integerType, stringType,boolType, datetimeType, floatType, dateType, byteType,TimeType
    }
}

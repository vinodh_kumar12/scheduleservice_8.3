﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Enums
{
    public enum ConflictExceptions {
        DOUBLEBOOK,
        DEVIATION,
        SERVICEMAXORMINHOURS,
        CLIENTTNOTPREFERREDTHISCLINICIAN,
        CLINICIANONVACATION,
        CLIENTONVACATION,
        CLINICIANRENEWABLEITEMSEXPIRED,
        CLIENTMAXHOURS,
        CLINICIANMAXHOURS,
        OVERTIME,
        VF,
        AUTHORIZATION,
        GRACEPERIOD,
        NO_ATTESTATION,
        NO_REASONCODE,
        NO_ACTIONCODE,
        UNMATCHEDPHONE,
        OUTSIDEFENCE,
        SIGNATUREEXCEPTION,
        MANUAL_ENTRY,
        TIME_EDITED,
        NO_LOCATION,
        SHORT_VISIT,
        LATE_CHECKIN,
        UNSCHEDULED_VISIT,
        VISIT_NO_SHOW,
        MISSED_CHECKOUT,
        LONG_VISIT,
        CLIENTMAXHOURS_WARNING,
        CLINICIANMAXHOURS_WARNING,
        OVERTIME_WARNING,
        AUTHORIZATION_WARNING
    }
}

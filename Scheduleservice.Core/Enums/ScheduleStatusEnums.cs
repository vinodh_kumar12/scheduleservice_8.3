﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Enums
{
    public enum ScheduleStatusEnums
    {
        Approved,
        Completed,
        Deleted,
        In_Progress,
        MissedVisit,
        NotCreated,
        Planned,
        ReSchedule,
    }
}

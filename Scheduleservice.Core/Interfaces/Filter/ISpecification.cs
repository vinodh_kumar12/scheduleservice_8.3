﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Interfaces.Filter
{
    public interface ISpecification<T>
    {
        bool IsSatisfied(T t);
    }
}

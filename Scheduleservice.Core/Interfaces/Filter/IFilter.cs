﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Interfaces.Filter
{
    public interface IFilter<T>
    {

        IEnumerable<T> filter(IEnumerable<T> items, ISpecification<T> spec);
    }
}

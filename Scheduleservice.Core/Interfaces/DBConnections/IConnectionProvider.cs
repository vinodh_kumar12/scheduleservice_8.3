﻿using Scheduleservice.Core.Interfaces.UnitofWorks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Interfaces.DBConnections
{
    public interface IConnectionProvider
    {
        IUnitOfWork ConnectMaster(bool IsReadOnly = false);

        IUnitOfWork Connect(bool IsReadOnly = false);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.ValildationHandlers
{
    public interface ICustomValidators
    {
        public bool BeAValidProperty(object obj, string propertyName);
        public bool BeAValidSortField(List<string> FieldName, string[] output_fields); 
        public bool BeAUniqueStringList(List<string> FieldName);
        public bool BeAUniqueStringArray(string[] output_fields);
        public bool BeAValidCommaSeparatedString(string CommaSeparatedString);
        public bool BeAValidGUID(string val);
        public bool BeAValidDate(string date);
        public bool BeAValidScheduleStatus(string CommaSeparatedString);
        public bool BeAValidEditedHours(string EditedHours);

    }
}

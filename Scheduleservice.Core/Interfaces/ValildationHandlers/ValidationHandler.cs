﻿using Scheduleservice.Core.Models; 
using System.Collections.Generic; 

namespace Scheduleservice.Core.Interfaces.ValildationHandlers
{
    public abstract class ValidationHandler<T> : IValidationHandler<T> where T : class
    {
        public ValidationHandler()
        {
            _nextValidationHandler = null;

        }
        protected IValidationHandler<T> _nextValidationHandler { get; set; }
        protected List<ValidationErrorInfo> result = new List<ValidationErrorInfo>();
        public virtual List<ValidationErrorInfo> HandleValidation(T request)
        {
            return _nextValidationHandler?.HandleValidation(request);
        }


        public IValidationHandler<T> SetNextValidation(IValidationHandler<T> scheduleValidatorHandler)
        {
            _nextValidationHandler = scheduleValidatorHandler;
            return _nextValidationHandler;
        }
    }


}

﻿using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.ValildationHandlers
{
   public interface IValidationHandler<T> where T : class
    {
        IValidationHandler<T> SetNextValidation(IValidationHandler<T> next);
        List<ValidationErrorInfo> HandleValidation(T request);
    }
}

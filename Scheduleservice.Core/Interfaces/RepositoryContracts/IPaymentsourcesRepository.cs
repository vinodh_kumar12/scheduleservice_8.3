﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IPaymentsourcesRepository
    {
        //GetPaymentSourceBasicInfo
        Task<PaymentsourcesEntity> GetPaymentSourceBasicInfo(int hha, int userid, int Payment_Source_ID);
        Task<PaymentsourcesEntity> GetPaymentSourceAdditionalInfo(int hha, int userid, int Payment_Source_ID, int HHA_BRANCH_ID);
        //List
        Task<IEnumerable<PaymentsourcesEntity>> GetPaymentsources(int hha, int userid, string payerColumns, string payerFilters);
        
    }
}

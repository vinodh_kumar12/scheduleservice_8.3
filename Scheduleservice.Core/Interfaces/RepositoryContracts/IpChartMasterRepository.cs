﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IpChartMasterRepository
    {
        Task<IEnumerable<pChartMasterEntity>> GetPChartMasterList(int HHA, int User, string Columns, string Filters);
    }
}

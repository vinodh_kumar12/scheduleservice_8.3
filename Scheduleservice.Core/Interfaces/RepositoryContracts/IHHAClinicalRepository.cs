﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAClinicalRepository
    {
        Task<HHAClinicalEntity> _S_Schedule_GetHHAClinicalSettings(int HHA, int UserID, string Columns, string Filters);
    }
}

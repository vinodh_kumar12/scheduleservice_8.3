﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
   public interface IHHABranchScheduleConfigurationsRepository
    {
        Task<HHABranchScheduleConfigurationsEntity> GetHHABranchScheduleConfigurations(int HHA, int UserId, int HHABranchID);
        Task<string> _VC_GetCurrentTime(int HHA, int UserId, int HHABranchID, string Context);
    }
}

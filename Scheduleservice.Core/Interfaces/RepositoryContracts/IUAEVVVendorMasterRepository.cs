﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUAEVVVendorMasterRepository
    {
        Task<IEnumerable<UAEvvVendorMasterEntity>> _HHA_Schedule_GetUAEVVVendorMaster(int HHA, int UserID, string SchedulesColumns);
    }
}

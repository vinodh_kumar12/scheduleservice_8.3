﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAEVVScheduleConfigurationGroupRepository
    {
        Task<HHAEVVScheduleConfigurationGroupEntity> GetHHAEVVScheduleConfigurationGroupSettings(int HHA, int UserID, string Columns, string Filters);
    }
}

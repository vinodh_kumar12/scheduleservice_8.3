﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverTasksTerminalPayerRepository
    {
        Task<int> InsertCaregiverTasksTerminalPayer(int HHA, int User, string Columns, string ValuesJson);
        Task<int> InsertCaregiverTaskChildScheduleTerminalPayer(int HHA, int User, string Columns, string ValuesJson);
        Task<IEnumerable<CaregiverTasksTerminalPayerEntity>> GetAllTerminalPayerSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregiverTasksTerminalPayerEntity>> GetTerminalPayerSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity>> GetAllTerminalPayerChildSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity>> GetTerminalPayerChildSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);

        Task<int> DeleteCaregiverTasksTerminalPayer(int HHA, int User, string ValuesJson);
        Task<int> DeleteCaregiverTaskChildScheduleTerminalPayer(int HHA, int User, string ValuesJson);

        Task<int> UpdateTerminalPayerSchedules(int HHA, int UserId, string jsonData);
        Task<int> UpdateTerminalPayerChildSchedules(int HHA, int UserId, string jsonData);
    }
}

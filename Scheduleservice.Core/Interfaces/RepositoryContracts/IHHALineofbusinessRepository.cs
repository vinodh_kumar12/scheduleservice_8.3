﻿
using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHALineofbusinessRepository
    {
        Task<IEnumerable<LineofbusinessEntity>> GetHHALOBs(int HHA, int UserID, string LOBColumns, string LOBfilter);
    }
}

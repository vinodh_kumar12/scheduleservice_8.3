﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUserRolesRepository
    {
        Task<IEnumerable<UserRolesEntity>> GetUserEnabledPermissionList(int HHA, int user, string Columns, string filters);
    }
}

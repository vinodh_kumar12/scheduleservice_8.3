﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUAEvvExceptionEventCodesRepository
    {
        Task<IEnumerable<UAEVVExceptionEventCodesEntity>> GetUAEVVExceptionEventCodes(int UserID, int EvvVendorVersionMasterID, string Filters, string Columns);
    }
}

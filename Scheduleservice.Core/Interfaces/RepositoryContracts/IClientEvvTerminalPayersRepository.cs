﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IClientEvvTerminalPayersRepository
    {
        Task<IEnumerable<ClientEvvTerminalPayersEntity>> GetClientTerminalPayersList(int HHA, int User, string Columns, string Filters);
        Task<IEnumerable<ClientEvvTerminalPayersEntity>> GetAllClientTerminalPayersList(int HHA, int User, string Columns, string Filters);

        Task<int> UpdateClientEvvTerminalPayers(int HHA, int User, string json_data);
    }
}

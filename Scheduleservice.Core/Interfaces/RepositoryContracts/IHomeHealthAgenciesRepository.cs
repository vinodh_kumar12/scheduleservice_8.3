﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHomeHealthAgenciesRepository
    {
        //Get HHA Basic Info
        Task<IEnumerable<HomeHealthAgenciesEntity>> GetHomeHealthAgencieseBasicInfo(int hha, int userid, string Columns, string Filters);
    }
}

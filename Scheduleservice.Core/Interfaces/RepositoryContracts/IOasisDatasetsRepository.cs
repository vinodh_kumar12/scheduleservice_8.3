﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IOasisDatasetsRepository
    {
        Task<IEnumerable<OasisDatasetsEntity>> GetOasisDatasetsList(int HHA, int User, string Columns, string Filters);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUAEvvExceptionEventMasterRepository
    {
        Task<IEnumerable<_UA_EvvExceptionEventMasterEntity>> GetEvvExceptionEventMasterList(int HHA, int UserID);
    }
}

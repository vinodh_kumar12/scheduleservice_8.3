﻿using Scheduleservice.Core.Entities;  
using System.Collections.Generic; 
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IClientsRepository
    {
        //INFO
        Task<ClientsEntity> GetClientsBasicInfo(int HHA, int UserID, int CLIENT_ID);
         

        //Client payer  info
        Task<IEnumerable<ClientPaymentSourcesEntity>> GetClientPaymentSourcesInfo(int HHA, int UserID, int CLIENT_ID, int PAYMENT_SOURCE_ID);

        //NOT preferred clinician lists
        Task<IEnumerable<ClientNonPreferredCliniciansEnity>> GetClientNonPreferredCliniciansList(int HHA, int UserID, int CLIENT_ID);

        //ClientVacationMaster 
        Task<IEnumerable<ClientVacationMasterEntity>> GetClientVacationMasterList(int HHA, int UserID, int CLIENT_ID);

        Task<IEnumerable<ClientsEntity>> GetClientStatus(int hha, int Userid, string ClientIDs);

        //List
        Task<IEnumerable<ClientsEntity>> GetALLClients(int HHA, int UserID, string clientColumns, string clientsFilters);
        //Task<IEnumerable<ClientsEntity>> GetClientsBasicList(int HHA, int UserID, string clientsFilters);
        //Task<IEnumerable<ClientsEntity>> GetClientsAdditionalList(int HHA, int UserID, string clientsFilters);
        //Task<IEnumerable<ClientsEntity>> GetClientsAdditional2List(int HHA, int UserID, string clientsFilters);
        Task<IEnumerable<_CL_NonPreferredCliniciansEntity>> GetNonPreferredClinicians(int HHA, int UserID, string Columns, string Filter);
        Task<IEnumerable<ClientVacationMasterEntity>> GetClientVacationMaster(int HHA, int UserID, string Columns, string Filter);
        Task<IEnumerable<ClientPaymentSourcesEntity>> GetClientPaymentSourcesList(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters, string ClientFilters_json);
        Task<IEnumerable<ClientAdditionalPaymentSourcesEntity>> GetClientAdditionalPaymentSourcesList(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters);
        Task<IEnumerable<DualEligibilePayersEntity>> GetClientDualEligiblePaymentSourceslist(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters);
        Task<IEnumerable<ClientPayerEffectivePeriodsEntity>> GetClientPayerEffectivePeriods(int HHA, int UserID, string Columns, string Filters);
        Task<IEnumerable<ClientInactivePeriodsEntity>> GetClientInactivePeriods(int HHA, int UserID, int ClientID);
    }
}

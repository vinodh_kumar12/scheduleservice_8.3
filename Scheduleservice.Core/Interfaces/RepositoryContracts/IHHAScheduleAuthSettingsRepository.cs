﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAScheduleAuthSettingsRepository
    {
        Task<IEnumerable<HHAScheduleAuthSettingsEntity>> GetHHAScheduleAuthSettingsDetails(int HHA, int UserID);        
        Task<bool> SaveHHAScheduleAuthSettingsDetails(int HHA, int UserID, string configurations);
    }
}
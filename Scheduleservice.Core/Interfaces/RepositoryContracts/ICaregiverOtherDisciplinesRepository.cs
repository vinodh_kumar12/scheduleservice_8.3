﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverOtherDisciplinesRepository
    {
        Task<IEnumerable<CaregiverOtherDisciplinesEntity>> GetCaregiverOtherDiscipines(int HHA, int UserID, string Columns, string Filter);
    }
}

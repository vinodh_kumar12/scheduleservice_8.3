﻿using Scheduleservice.Core.Entities;  
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverTaskEVVExceptionsRepository
    {
        Task<IEnumerable<CaregiverTaskEVVExceptionsEntity>> GetCaregiverTaskEVVExceptionsList(int hha, int userid, string Cgtaskids, string filters);
        Task<bool> AddCaregiverTaskEVVExceptions(int hha, int userid, int cgTaskid, string CaregiverTaskEVVExceptions);
        Task<bool> AddCaregiverTaskEVVExceptions_ValidationErrors(int hha, int userid, string CaregiverTaskEVVExceptions);
        Task<bool> ClearCaregiverTaskEVVExceptions(int hha, int userid, int cgTaskid, string filters);

        Task<IEnumerable<CaregiverTaskEVVExceptionsEntity>> GetCaregiverTaskEVVExceptions(int hha, int userid, string columns, string filters);
        Task<bool> UpdateCaregiverTaskEVVException(int hha, int userid, string CaregiverTaskEvvExceptionIDs, string UpdateColumns);


        //Schedule conflict Exceptions
        Task<IEnumerable<CaregiverTaskValidationErrorsDependentSchedulesEntity>> GetCaregiverTaskValidationErrorsDependentSchedules(int hha, int userid, int cgTaskid);
    }
}

﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAConfigurationsRepository
    {
        Task<_M_ConfigurationsEntity> Get_M_Configurations(int HHA, int UserId, string serviceColumns, string serviceFilter);
        Task<bool> UpdateUAConfigurations(int HHA, int UserId, string UpdateColumns, string UpdateCondition);
    }
}

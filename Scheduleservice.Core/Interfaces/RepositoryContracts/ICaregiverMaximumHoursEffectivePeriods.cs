﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverMaximumHoursEffectivePeriods
    {
         Task<IEnumerable<CaregiverMaximumHoursEffectivePeriodsEntity>> GetCaregiverMaximumHoursEffectivePeriodsEntity_Details(int HHA, int UserID, string Columns, string Filter);
    }
}

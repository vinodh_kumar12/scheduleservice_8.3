﻿using Scheduleservice.Core.Entities; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IPaymentsourcesAdditionalRepository
    {
        Task<IEnumerable<PaymentSourcesAdditionalEntity>> GetHHAPayers(int HHA, int UserID, IEnumerable<int> Payers);

        Task<PaymentSourcesAdditionalEntity> GetPayerAdditionalInfo(int HHA, int UserID, int PayerID, string Columns);
    }
}

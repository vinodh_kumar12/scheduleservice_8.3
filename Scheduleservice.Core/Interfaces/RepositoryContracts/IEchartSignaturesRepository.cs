﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IEchartSignaturesRepository
    {
        Task<EchartSignaturesEntity> GetScheduleSignatureInfo(int HHA, int UserID, string filters, string columns);
    }
}

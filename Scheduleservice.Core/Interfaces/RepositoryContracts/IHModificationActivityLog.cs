﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHModificationActivityLog
    {
        Task<int> AddAuditData(int HHA, int UserId, string AuditInfo);
    }
}

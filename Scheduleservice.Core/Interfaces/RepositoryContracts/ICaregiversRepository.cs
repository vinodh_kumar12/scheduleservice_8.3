﻿using Scheduleservice.Core.Entities; 
using System.Collections.Generic; 
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiversRepository
    {
        //Info
        Task<CaregiversEntity> GetCaregiverBasicInfo(int HHA, int UserID, int Caregiver_ID);

        //additional info
        Task<CaregiversEntity> GetCaregiverAdditionalInfo(int HHA, int UserID, int Caregiver_ID);

        //Lists
        Task<IEnumerable<CaregiversEntity>> GetCaregivers(int HHA, int UserID,string caregiverColumns, string caregiverFilter);

        Task<IEnumerable<CaregiverVacationsEntity>> GetCaregiverVacations(int HHA, int UserID, string Columns, string Filter);
    }
}

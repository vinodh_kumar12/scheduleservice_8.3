﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUAEvvExceptionEventsRepository
    {
        Task<IEnumerable<_UA_EvvExceptionEventsEntity>> GetEVVVendorEvents(int HHA, int userID, int EvvVendorVersionMasterID);
        Task<IEnumerable<_UA_EvvExceptionEventsEntity>> GetFilteredEVVVendorEvents(int HHA, int userID, string Filters, string Columns);
    }
}

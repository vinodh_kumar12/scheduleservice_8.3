﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHConfigurations2Repository
    {
        Task<HConfigurations2Entity> _S_Schedule_GetHConfiguration2Settings(int HHA, int UserID, string Columns, string Filters);
    }
}

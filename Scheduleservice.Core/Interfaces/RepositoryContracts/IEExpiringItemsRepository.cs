﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IEExpiringItemsRepository
    {
        Task<IEnumerable<_E_ExpiringItemsEntity>> GetEExpiringItems(int HHA, int UserID, string Columns, string Filter);
    }
}

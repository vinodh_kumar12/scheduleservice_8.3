﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverTaskEvvMissedVisitsReasonsRepository
    {
       
        Task<IEnumerable<CaregiverTaskEvvMissedVisitsReasonsEntity>> GetMissedVisitReasonCodes(int HHA, int UserId, string Columns, string Filters);
        Task<int> AddMissedVisitReasonCodes(int HHA, int UserId, int CGTASKId, int CaregiverTaskMissedVisitId, string MissedvisitReasonCodeInfo_JSON);
        Task<int> UpdateMissedVisitReasonCodes(int HHA, int UserId,string Filters,string updatecolumns);

    }
}

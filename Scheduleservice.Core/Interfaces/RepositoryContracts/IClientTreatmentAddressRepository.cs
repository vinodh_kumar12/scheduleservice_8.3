﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IClientTreatmentAddressRepository
    {
        Task<ClientTreatmentAddressEntity> _S_Schedule_GetClientTreatmentAddress(int HHA, int UserID, int clientTreatmentLocationID);
        Task<ClientTreatmentAddressEntity> _S_Schedule_GetClientTreatmentAddressInfo(int HHA, int UserID, string columns, string Filters);
    }
}

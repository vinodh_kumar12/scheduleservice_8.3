﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICDSPayerServiceCategoryMasterRepository
    {
        Task <IEnumerable<CDSPayerServiceCategoryMasterEntity>> _S_Schedule_CDSPayerServiceCategoryMaster(int HHA, int UserID, string columns, string Filters);
    }
}

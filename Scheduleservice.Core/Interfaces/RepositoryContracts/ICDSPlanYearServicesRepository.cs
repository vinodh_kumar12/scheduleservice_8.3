﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICDSPlanYearServicesRepository
    {

        Task<IEnumerable<CDSPlanYearServicesEntity>> _S_Schedule_GetAllCDSPlanYearServices(int HHA, int UserID, string columns, string Filters);
        Task<CDSPlanYearServicesEntity> _S_Schedule_GetCDSPlanYearServices(int HHA, int UserID, string columns, string Filters);

        Task<int> UpdateCDSPlanYearService_bulk(int HHA, int UserID, string columns, string Filters);



    }
}

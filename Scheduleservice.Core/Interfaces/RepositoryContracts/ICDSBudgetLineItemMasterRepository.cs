﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICDSBudgetLineItemMasterRepository
    {
        Task<CDSBudgetLineItemMasterEntity> _S_Schedule_CDSBudgetLineItemMaster(int HHA, int UserID, string columns, string Filters);

        Task<IEnumerable<CDSBudgetLineItemMasterEntity>> _S_Schedule_GetCDSBudgetLineItemMaster(int HHA, int UserID, string columns, string Filters);
    }
}

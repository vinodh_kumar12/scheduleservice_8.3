﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAScheduleConflictExceptionsRepository
    {
        Task<IEnumerable<HHAScheduleConflictExceptionsEntity>> GetHHAScheduleConflictExceptions(int HHA, int UserId);
        Task<bool> AddHHAScheduleConflictExceptions(int HHA, int UserID, string Exceptions);
        Task<bool> DeleteHHAScheduleConflictExceptions(int HHA, int UserID, string Exceptions);
    }
}

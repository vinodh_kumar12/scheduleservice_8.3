﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICDSClientBudgetLineItemRepository
    {
        Task<CDSClientBudgetLineItemEntity> _S_Schedule_GetCDSClientBudgetLineItem(int HHA, int UserID, string columns, string Filters);
        Task<IEnumerable<CDSClientBudgetLineItemEntity>> _S_Schedule_CDSClientBudgetLineItems(int HHA, int UserID, string columns, string Filters);
    }
}

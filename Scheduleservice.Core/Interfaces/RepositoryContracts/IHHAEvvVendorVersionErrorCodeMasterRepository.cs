﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface  IHHAEvvVendorVersionErrorCodeMasterRepository
    {
        Task<IEnumerable<HHAEvvVendorVersionErrorCodeMasterEntity>> _HHA_Schedule_GetHHAEVVVendorVersionErrorCodeTypes(int HHA, int UserID, int EVVVendorVersionMasterID);
    }
}

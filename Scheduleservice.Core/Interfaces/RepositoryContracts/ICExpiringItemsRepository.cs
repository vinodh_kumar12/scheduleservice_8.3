﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICExpiringItemsRepository
    {
        Task<IEnumerable<_C_ExpiringItemsEntity>> GetCExpiringItems(int HHA, int UserID, string Columns, string Filter);
    }
}

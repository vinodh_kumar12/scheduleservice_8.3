﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ITimeZonesRepository
    {
        Task<IEnumerable<TimeZonesEntity>> GetTimeZoneName(int HHA, int User, string ZoneID);
    }
}

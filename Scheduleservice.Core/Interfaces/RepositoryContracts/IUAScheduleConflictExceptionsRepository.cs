﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
   public interface IUAScheduleConflictExceptionsRepository
    {
        Task<IEnumerable<_UA_ScheduleConflictExceptionsEntity>> GetScheduleConflictExceptions(int HHA, int UserID);

        Task<IEnumerable<_UA_ScheduleConflictExceptionsEntity>> GetFilteredScheduleConflictExceptions(int HHA, int UserID, string Filters, string Columns);
    }
}

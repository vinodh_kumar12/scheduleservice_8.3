﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHomeHealthAgencies_BillingPayrollSettingsRepository
    {
        Task<IEnumerable<HomeHealthAgencies_BillingPayrollSettingsEntity>> GetBillingPayrollSettingsDetails(int HHA, int UserID,string column,string filters);

    }
}

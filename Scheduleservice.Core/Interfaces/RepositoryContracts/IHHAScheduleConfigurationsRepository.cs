﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAScheduleConfigurationsRepository
    {
        Task<HHAScheduleConfigurationsEntity> GetHHAScheduleConfigurations(int HHA, int UserId);
    }
}

﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAEVVConfigurationsRepository
    {
        Task<bool> _HHA_Schedule_SaveEVVBranchAggregatorSettings(int HHA, int UserID, string EvvConfigurationID = "", string ModifiedData="", string ModifiedData_Audit = "");
        Task<IEnumerable<HHAEVVConfigurationsEntity>> _HHA_Schedule_GetHHAEVVConfigurations(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson);
    }
}

﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUsersRepository
    {
        Task<IEnumerable<UsersEntity>> GetHHAUsers(int HHA, string Columns, string filters);
        Task<IEnumerable<UsersBranchListEntity>> GetUsersBranchList(int HHA, int user, string Columns, string filters);
    }
}

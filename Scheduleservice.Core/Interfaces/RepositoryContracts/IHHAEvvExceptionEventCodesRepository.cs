﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAEvvExceptionEventCodesRepository
    {
        Task<IEnumerable<HHAEVVExceptionEventCodesEntity>> GetHHAEVVExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID, string Filters, string Columns);
        Task<bool> AddEVVExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID, string eventcodes, string exception_code);
        Task<bool> DeleteEVVExceptionEventCodes(int HHA, int UserID, string ExceptionEventCodeIDs);
        Task<bool> UpdateEVVEventCodes(int HHA, int userID, string filters, string columns);
    }
}
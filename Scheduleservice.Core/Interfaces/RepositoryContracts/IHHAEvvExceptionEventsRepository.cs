﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHAEvvExceptionEventsRepository
    {
        Task<IEnumerable<_C_EvvExceptionEventsEntity>> GetHHAEVVEvents(int HHA, int userID, int EvvVendorVersionMasterID);
        Task<bool> UpdateEVVEventInfo(int HHA, int userID, string filters, string columns);
        Task<bool> AddEVVExceptionEvents(int HHA, int UserID, int EvvVendorVersionMasterID, string eventcodes, string exception_code);
        Task<bool> DeleteEVVExceptionEvents(int HHA, int UserID, string EvvExceptionEventIDs);
        Task<IEnumerable<HHAEvvVendorExceptionEventsEntity>> GetHHAEvvVendorExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID);
        Task<IEnumerable<HHAEvvVendorExceptionEventsEntity>> GetHHAEvvVendorExceptionEvents(int HHA, int UserID, int EvvVendorVersionMasterID);
    }
}

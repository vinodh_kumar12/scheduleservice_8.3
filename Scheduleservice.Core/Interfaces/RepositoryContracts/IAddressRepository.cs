﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IAddressRepository
    {
        Task<AddressEntity> _S_Schedule_GetTreatmentLocation(int HHA, int UserID, int AddressID);
        Task<AddressEntity> _S_Schedule_GetTreatmentLocationInfo(int HHA, int UserID, string columns, string Filters);
    }
}

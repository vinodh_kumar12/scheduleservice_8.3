﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities;


namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
   public interface ICaregivertaskMissedVisitsRepository
    {
      Task<IEnumerable<CaregivertaskMissedVisitsEntity>> GetMissedVisitInfo(int hha, int user, string filters, string columns);
 

    }
}

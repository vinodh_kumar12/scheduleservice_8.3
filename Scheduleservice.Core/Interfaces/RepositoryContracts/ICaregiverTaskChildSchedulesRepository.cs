﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverTaskChildSchedulesRepository
    {
        Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetAllChildSchedulesList(int HHA, int UserId, string SchedulesColumns, string scheduleFilters, bool requireallSplits = false);
    }
}

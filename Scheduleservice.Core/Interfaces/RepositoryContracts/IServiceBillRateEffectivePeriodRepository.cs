﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IServiceBillRateEffectivePeriodRepository
    {
        Task<ServiceBillRateEffectivePeriodEntity> GetServiceBillRateEffectivePeriodInfo(int HHA, int UserID, string filters, string columns );

    }
}

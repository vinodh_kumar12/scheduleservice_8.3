﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IHHASelectedEvvMasterRepository
    {
        Task<IEnumerable<HHASelectedEvvMasterEntity>> _HHA_Schedule_GetHHAEVVVendors(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson);
    }
}

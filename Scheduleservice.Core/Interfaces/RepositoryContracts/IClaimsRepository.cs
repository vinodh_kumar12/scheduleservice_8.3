﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IClaimsRepository
    {
        Task<IEnumerable<ClaimsEntity>> GetScheduleClaims(int HHA, int User, string Columns, string Filters);
    }
}

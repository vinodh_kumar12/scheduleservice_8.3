﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IServiceCodesRepository
    {//Infor
        Task<ServiceCodesEntity> GetServiceCodeBasicInfo(int HHA, int UserID, int SERVICE_CODE_ID);
        Task<ServiceCodesEntity> GetServiceCodeAdditionalInfo(int HHA, int UserID, int SERVICE_CODE_ID, int HHA_BRANCH_ID, string EFFECTIVE_DATE="");

        //list
        Task<IEnumerable<ServiceCodesEntity>> GetServiceCodes(int HHA, int UserID,string serviceColumns, string serviceFilter);
        Task<IEnumerable<_UA_FormsEntity>> GetUA_FormsEntities(int HHA, int UserID, string formsColumns, string formsFilter);
    }
}

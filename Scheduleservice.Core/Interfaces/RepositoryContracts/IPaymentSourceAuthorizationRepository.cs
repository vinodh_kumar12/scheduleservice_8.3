﻿using Scheduleservice.Core.DTO.Authorization;
using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IPaymentSourceAuthorizationRepository
    {
        Task<int> CanModifiedSchedluleWithSameAuth(int HHA, int UserID, int OldAuthId,
                    int CgtasID, string NewScheduleDate, int NewService, float NewUnits);

        Task<AuthInfoDto> GetAvailableAuthID(int HHA, int UserID, int OldAuthId, int ClientID,
                int PayerID, int ServiceID, string ScheduleDate, float ScheduleUnits,
                bool IsOfficeAuth, bool ConfirmSoftwarning, float ScheduleHours,
                bool ConsiderBothOfficeAndRealAuth);

        Task<PaymentSourceAuthorizationsEntity> GetAuthInfo(int HHA, int UserID, string filters, string columns);
        Task<bool> UpdateAuthoriaztionInfo(int HHA, int UserID, string Updatefilters, string UpdateColumns);
    }
}

﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks; 

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IPickListValuesRepository
    {
        Task<IEnumerable<PickListValuesEntity>> GetPickListValues(int HHA,int UserID,string PickListName);
        
        Task<int> CreatePickListValues(int HHA, int UserID, string PickListName, string PickListCategory);
        Task<int> UpdatePickListValues(int HHA, int UserID, string PickListName, string PickListCategory, int PicklistItemID);
        Task<int> DeletePickListValues(int HHA, int UserID, int PicklistItemID, string PickListCategory);
    }
}

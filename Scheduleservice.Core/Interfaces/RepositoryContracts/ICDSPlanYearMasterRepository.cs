﻿using System;
using Scheduleservice.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICDSPlanYearMasterRepository
    {
        Task<IEnumerable<CDSPlanYearMasterEntity>> _S_Schedule_GetAllCDSPlanYearMasters(int HHA, int UserID, string columns, string Filters);
    }
}

﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IScheduleProcessingQueueRepository
    {
        Task<bool> InsertScheduleProcessingQueue(int hha, int userid, Guid batch_guid, string task_type, string validation_rule_json, string cgtask_ids);
        Task<IEnumerable<ScheduleProcessingQueueEntity>> GetScheduleProcessingQueue(int HHA, int UserID, string Columns, string Filter);
        Task<bool> DeleteScheduleProcessingQueue(int hha, int userid, string batch_guid);

    }
}

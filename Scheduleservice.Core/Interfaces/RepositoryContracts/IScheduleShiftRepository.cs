﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IScheduleShiftRepository
    {
        Task<IEnumerable<ScheduleShiftEntity>> _HHA_Schedule_GetScheduleShiftsInfo(int HHA, int UserID, string columns, string Filters);
    }
}

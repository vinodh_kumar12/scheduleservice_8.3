﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregivertasksRepository
    {
        //Get Info Details
        Task<CaregivertasksEntity> GetScheduleBasicInfo(int HHA, int UserId,  int CGTask_ID);
        Task<CaregivertasksEntity> GetScheduleAdditionalInfo(int HHA, int UserId, int CGTask_ID, string SchedulesColumns);

        //Get The lists
        Task<IEnumerable<CaregivertasksEntity>> GetAllSchedulesList(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregivertasksEntity>> GetScheduleBasicList(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregivertasksEntity>> GetScheduleAdditionalList(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
        Task<IEnumerable<CaregivertasksEntity>> GetScheduleAdditional2List(int HHA, int UserId, string SchedulesColumns, string scheduleFilters);
       // Task<int> GetScheduleEVVVendorVersionMasterID(int HHA, int UserID, int CgtaskID);
        Task<IEnumerable<CaregivertasksEntity>> GetExceptionEligibleSchedulesList(int HHA, int UserId, string Context, int GraceMinutes);
        //Insert


        //Modify
        Task<int> UpdateScheduleBasic(int HHA, int UserId, int CGTASK_ID, string caregivertaskscolumns);
        Task<int> UpdateScheduleAdditional(int HHA, int UserId, int CGTASK_ID, string caregivertasksAdditionalcolumns);
        Task<int> UpdateScheduleAdditional2(int HHA, int UserId, string CGTASK_IDs, string caregivertasksAdditional2Columns);

        Task<int> UpdateScheduleInfo(int HHA, int UserId, int CGTASK_ID, string caregivertaskcolumns, string Caregivertaskadditionalcolumns,
                   string caregivertaskadditional2columns);

        Task<string> UpdateRecalculateEVVSchedules(int HHA, int UserId, string CGTaskIDsjson, int context);


        //validations
        Task<bool> GetVisitFrequencyValidation(int HHA, int UserId, int CgtaskID, string CheckinDatetime, string CheckoutDatetime);

        Task<int> ReConstructChildSchedules(int HHA, int UserId, string SplitedTaskDetails, bool BlockCheckinValidation);
        Task<int> AddEditDeleteTrivelDocumentTimeMiscVisit(int HHA, int UserId, string ModifyData);

        Task<int> pChartSaveSubmit(int hha, int User, int Cgtaskid, int pChartSubmit, int pChartApproved, int pageId, string checkInTime, string CheckOutTime, string Miles, string EditedHours, int PlaceOfService, int? clinician);

        Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetChildSchedulesList(int HHA, int User, string Columns, string Filters);
        Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetAllChildSchedulesList(int HHA, int User, string Columns, string Filters);
        Task<int> CreateSchedule(int hha, int user, string schedule_json);
        Task<int> CreateChildSchedules(int hha, int user, string schedule_json,bool IsSplitForBilling,bool IsSplitForPayroll);

        Task<int> UpdateScheduleInfo_Subscriber(int HHA, int UserId, int CGTASK_ID, string caregivertaskcolumns, string Caregivertaskadditionalcolumns,
                   string caregivertaskadditional2columns);

        Task<IEnumerable<CaregivertasksEntity>> GetEvvTerminalPayerSchedulesToRegister(int HHA, int User, string Columns, string Filters, string client_payer_json);

        Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetEvvTerminalPayerChildSchedulesToRegister(int HHA, int User, string Columns, string Filters, string client_payer_json);
        Task<bool> GetCreateScheduleVisitFrequencyValidation(int HHA, int UserId, int ClientID, int Payerid, DataTable SchedulesTable, int Context);
    }
}

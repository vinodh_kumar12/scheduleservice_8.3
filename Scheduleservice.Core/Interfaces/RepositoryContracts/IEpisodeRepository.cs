﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IEpisodeRepository
    {
        Task<IEnumerable<EpisodeEntity>> _CL_GetClientEpisodes(int HHA, int UserID, int ClientID);
    }
}

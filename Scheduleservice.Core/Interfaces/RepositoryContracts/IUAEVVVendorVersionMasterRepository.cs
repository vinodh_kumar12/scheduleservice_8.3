﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IUAEVVVendorVersionMasterRepository
    {
        Task<IEnumerable<UAEvvVendorVersionMasterEntity>> _HHA_Schedule_GetUAEVVVendorVersionMaster(int HHA, int UserID, string SchedulesColumns);
    }
}

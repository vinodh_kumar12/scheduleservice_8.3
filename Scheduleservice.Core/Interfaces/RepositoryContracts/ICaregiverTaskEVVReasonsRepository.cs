﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface ICaregiverTaskEVVReasonsRepository
    {
        Task<int> AddCaregiverTaskEVVReasons(int hha, int userid, int cgTaskid, string ReasonCodesJson, string ActionCodeJson);
        Task<string> _S_GetApproveScheduleValidationErrors(int HHA, int cgtaskid, int UserID,
                bool isTimemodified = false, bool? IsEVVSchedule = null, bool isfromChart = false, int Context = 0);

    }
}

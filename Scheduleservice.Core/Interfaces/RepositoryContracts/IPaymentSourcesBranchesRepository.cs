﻿using Scheduleservice.Core.Entities; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IPaymentSourcesBranchesRepository
    {
        Task<IEnumerable<PaymentSourcesBranchesEntity>> GetHHAPayerBranches(int HHA, int UserID, IEnumerable<int> Payers);
        Task<PaymentSourcesBranchesEntity> GetPayerBranchInfo(int HHA, int UserID, int PayerID, int HHABranchID, string columns);
        Task<IEnumerable<PaymentSourcesBranchesEntity>> GetAllPayerBranchesInfo(int HHA, int UserID, string PayerColumns, string PayerFilters);
    }
}

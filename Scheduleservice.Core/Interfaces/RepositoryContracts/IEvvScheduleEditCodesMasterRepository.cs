﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IEvvScheduleEditCodesMasterRepository
    {
        Task<IEnumerable<EvvScheduleEditCodesMasterEntity>> GetHHAEVVCodes(int HHA, int userID, string Context, int EvvVendorVersionID);
        Task<HHAAggregatorEVVCodeResponseEntity> _HHA_Schedule_AddNewEVVCode(int HHA, int UserID, int EVVVendorVersionMasterID, string CodeType, string Code, string Desc, bool EnableNotesPrefill);
        Task<HHAAggregatorEVVCodeResponseEntity> _HHA_Schedule_UpdateEVVCode(int HHA, int UserID, int EVVScheduleEditCodeMasterID, string ModifiedFields);
    }
}

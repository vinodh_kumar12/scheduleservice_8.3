﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.RepositoryContracts
{
    public interface IServiceGroupsRepository
    {
        Task<IEnumerable<ServiceGroupsEntity>> GetServiceGroups(int HHA, int UserID, string Columns, string Filter); 
    }
}

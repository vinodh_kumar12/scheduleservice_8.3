﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Scheduleservice.Core.DTO.EVV;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface IAutoGenerateException
    {
        Task<IEnumerable<CaregivertasksEntity>> GetExceptionEligibleSchedules(int HHA, int UserID, string SystemCode,int GraceMin);

        Task<bool> AutoRaiseExceptionForSchedules(int HHA, int UserID, string Event,List<_UA_EvvExceptionEventsEntity> evvExceptionEventsDto, IEnumerable<CaregivertasksEntity> schedules);
        
        Task<bool> GenerateException(int HHA, int UserId, int CgtaskID, EVVExceptioninfoDto eVVExceptioninfoDto);

        Task<bool> RaiseEVVExceptionsOnCheckinCheckout(int HHA, int User, CaregivertasksEntity oldScheduleDetails, ScheduleEVVInfo scheduleEVVInfo,
           int EVVVendorVersionMasterID, int EvvCheckinReasonID = 0, int EvvCheckoutReasonID = 0, bool is_clincian_checkout = false);
    }
}

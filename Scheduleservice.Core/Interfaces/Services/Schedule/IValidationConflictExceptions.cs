﻿using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
   public interface IValidationConflictExceptions
    { 
        Task<ValidationConflictexceptionsDto> GetValidationConflictexceptions(int HHA, int UserId, string schedulefilters);
        Task<IEnumerable<RecalculateScheduleExceptionsinitDto>> GetValidationConflictRecalculate_init(int HHA, int UserId, string schedulefilters);
        Task<RecalculateScheduleExceptionsBatchDto> GetValidationConflictRecalculate_Batch(int HHA, int UserId, string batch_id);
        
        // create -save
        //REvalidate
        //delete
        // update ValidationConflictexceptions
        // Get show conflicts schedules
        // Get validations rules 

    }
}

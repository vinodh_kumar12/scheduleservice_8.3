﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface IConflictExceptionsValidationRules
    {
        Task<IEnumerable<ScheduleValidationRulesDto>> GetConflictExceptionsValidationRules(int HHA, int UserId, ValidateScheduleRules validateScheduleRules);
        Task<IEnumerable<ScheduleValidationRulesDto>> GetConflictExceptionsValidationRules_old(int HHA, int UserId, int HHA_BRANCH_ID, int CLIENT_ID, int PAYMENT_SOURCE, int SERVICECODE_ID, int? CAREGIVER);
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface IGetScheduleBasicDetail
    {
        Task<ValidateScheduleRules> getScheduleDetails(int hha, int user, CaregivertasksEntity caregiverTaskEntity);
    }
}

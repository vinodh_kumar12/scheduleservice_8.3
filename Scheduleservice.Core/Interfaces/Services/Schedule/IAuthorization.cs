﻿namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    using Scheduleservice.Core.DTO.Authorization;
    using Scheduleservice.Core.Entities;
    using Scheduleservice.Core.Models;
    using Scheduleservice.Core.Models.Authorization;
    using System.Threading.Tasks;
    public interface IAuthorization
    {
        Task<AuthInfoDto> ValidateScheduleAuth(int HHA, int UserID, int CgtasKID, EditScheduleProperties caregivertasksEntity, bool confirm_softwarning);
        Task<bool> UpdateAuthorizationInfo(int HHA, int UserID, EditScheduleProperties editScheduleProperties, CaregivertasksEntity oldscheduleInfo=null);
    }
}

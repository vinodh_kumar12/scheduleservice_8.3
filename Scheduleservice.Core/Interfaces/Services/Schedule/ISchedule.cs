﻿using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface ISchedule
    {
        Task<string> GetSchedulesAllList(int HHA, int UserId, string scheduleFilters, string[] Output_columns);
        Task<string> GetAllChildSchedulesList(int HHA, int UserId, string scheduleFilters, string[] Output_columns);
        Task<IEnumerable<ScheduleBasicListDto>> GetSchedulesBasicList(int HHA, int UserId, string scheduleFilters);

        Task<IEnumerable<ValidationErrorInfo>> ValidateApproveSchedule(int HHA, int UserId, int CGTask_ID);
        Task<int> ApproveSchedule(int HHA, int UserId, int CGTask_ID);       
        
        Task<int> ModifyScheduleAsync(int HHA, int UserId, int CGTask_ID, EditScheduleProperties editScheduleProperties, CaregivertasksEntity oldcaregivertasksEntity, bool BlockCheckinValidation);
         
        Task<IEnumerable<ValidationErrorInfo>> ValidateScheduleCheckInOutAsync(int HHA, int UserId, CaregivertasksEntity caregivertasksEntity, ValidateScheduleRules validateScheduleRules);
        Task<IEnumerable<ValidationErrorInfo>> ValidateCreateScheduleasync(int HHA, int UserId, ValidateScheduleRules validationCreateSchedule);
        Task<int> GetScheduleEVVVendorVersionMasterID(int HHA, int UserId, CaregivertasksEntity caregivertasksEntity, int HHABranchID, bool ConsiderSettings);
        Task<EditScheduleProperties> GetEditSchedulePropertiesModel(int HHA, int User, EditScheduleProperties request, CaregivertasksEntity oldscheduleinfo, ClientsEntity clientsEntity, bool RequireGeoLocationforFOB );
        Task<float> GetVisitUnits(int HHA, int UserID, PaymentSourcesAdditionalEntity paymentSourcesAdditionalEntity, int ServiceID, DateTime Checkindatetime, DateTime CheckOutDatetime, int HHA_BRANCH_ID,
                          int CgTaskID, CDSPlanYearServicesEntity cDSPlanYearServicesEntity =null,int TotalMinutes = 0);
        //Task<float> GetVisitUnitsForCds(int HHA, int UserID, PaymentSourcesAdditionalEntity paymentSourcesAdditionalEntity, int ServiceID, DateTime Checkindatetime, DateTime CheckOutDatetime, int HHA_BRANCH_ID,
        //                  int CgTaskID,int CDSPlanYearServiceid);
        
        Task<IEnumerable<ValidationErrorInfo>> CreateSchedule(int hha, int user,CreateScheduleDTO createScheduleDTO);

        Task<Response<IEnumerable<ValidationErrorInfo>>> ValidateModifyScheduleProperties(int HHA, int User, CaregivertasksEntity oldScheduleData, CaregivertasksEntity newScheduleData);

        Task<Response<bool>> GenerateConflictExceptions(int HHA, int User,IEnumerable<ValidationErrorInfo> validationErrorInfo, int CGTASK_ID);

        Task<IEnumerable<ValidationErrorInfo>> ValidateSchedule(int HHA, int UserId, int CGTask_ID);
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.DTO.Schedule;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface IEVVException
    {
        Task<bool> AutogenerateEVVEventExceptions(int HHA, int UserId);
        Task<bool> ClearScheduleEVVExceptions(int HHA, int UserId, int CGTASKID, string TypeOfOperation, string SystemCodes); 
        

    }
}

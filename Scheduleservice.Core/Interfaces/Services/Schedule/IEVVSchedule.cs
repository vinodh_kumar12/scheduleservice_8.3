﻿using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Schedule
{
    public interface IEVVSchedule
    { 
        Task<IEnumerable<ScheduleEVVBatchListDto>> InsertCaregiverTaskEvvSchedules(int HHA, int UserID, IEnumerable<ScheduleEVVBatchDto> CGTaskIDs );
        Task<bool>RecalculateCaregiverTaskEvvSchedules(int HHA, int UserID, String ScheduleEVVBatchID);

        Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> AutoRegisterEvvTerminalPayerSchedules(int HHA, int User);

        Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> InsertEvvTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> combinedSchedules);
        Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> UpdateEvvTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> combinedSchedules);
        Task<Response<IEnumerable<EvvTerminalPayerSchedulesDto>>> RegisterTerminalPayerSchedules(int HHA, int User, IEnumerable<EvvTerminalPayerSchedules> combinedSchedules);
    }
}

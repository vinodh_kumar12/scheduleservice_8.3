﻿using Scheduleservice.Core.DTO.CDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.CDS
{
    public interface ICDSBudgetLineitemreport
    {
        Task<CDSBudgetLineItemDTO> GetCDSBudgetLineitemReport(int HHA,int UserId, int PageID, string Location, string Payer,
                                               string Service, string BudgetLineItem, int ClientID,  int Purpose, string SortColumn,
                                               bool SortDirection, int pageNo, string ClientStatus, string SearchAuthNo, string AuthServiceStartFromDate,
                                               string AuthServiceStartToDate, string AuthServiceEndFromDate, string AuthServiceEndToDate, bool IncludeOfficeAuth,
                                               string PlanStartFromDate, string PlanStartToDate, string PlanEndFromDate, string PlanEndToDate, int CDSPlanYearServiceId);

        
        // Get CDSBudgetLineItemReport 

    }
}

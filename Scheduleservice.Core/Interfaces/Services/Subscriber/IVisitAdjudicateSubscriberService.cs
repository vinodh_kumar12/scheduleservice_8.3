﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Scheduleservice.Core.Common.Response;

namespace Scheduleservice.Core.Interfaces.Services.Subscriber
{
    public interface IVisitAdjudicateSubscriberService
    {
        Task<Response<bool>> VisitAdjudicateService(JObject IncomingModelMessage);
    }
}

﻿using Newtonsoft.Json.Linq;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Models.Subscriber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Interfaces.Services.Subscriber
{
    public interface IVisitCheckinSubscriberService
    {
        Task<Response<bool>> VisitCheckinService(JObject IncomingModelMessage);
    }
}

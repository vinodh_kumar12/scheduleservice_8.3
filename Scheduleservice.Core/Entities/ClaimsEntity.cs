﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClaimsEntity
    {
        public int CLAIM_ID { get; set; }
        public int HHA { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public int EPISODE { get; set; }
        public string? CLAIM_TYPE { get; set; }
        public string STATUS { get; set; }
        public int? SubEpisodeMasterID { get; set; }
        public bool? APPROVED { get; set; }
        public int CLAIM { get; set; }
        public DateTime SERVICE_DATE { get; set; }
    }
}

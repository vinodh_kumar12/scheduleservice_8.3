﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskEvvMissedVisitsReasonsEntity
    {
		public int CaregiverTaskEvvskMissedvisitsReasonID { get; set; }
		public int CaregiverTaskMissedVisitID { get; set; }
		public int HHA { get; set; }
		public int CGTASKID { get; set; }
		public string Context { get; set; }
		public string ReasonCode { get; set; }
		public string ReasonCodeDescription { get; set; }
		public string ActionCode { get; set; }
		public string ActionCodeDescription { get; set; }
		public string ActionCodeComments { get; set; }
		public string Notes { get; set; }
		public DateTime CreatedOn { get; set; }
		public int CreatedBy { get; set; }
		public int EVVVendorVersionMaserID { get; set; }
		public DateTime ActionAddedOn { get; set; }
		public int ActionAddedBy { get; set; }
		public bool isDeleted { get; set; }
		public DateTime DeletedOn { get; set; }
		public int DeletedBy { get; set; }
	}
}

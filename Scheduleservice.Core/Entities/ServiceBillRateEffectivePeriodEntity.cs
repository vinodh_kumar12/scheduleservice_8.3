﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ServiceBillRateEffectivePeriodEntity
    {
        public int ServiceBillRateEffectivePeriodID { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public bool? isUnitsFixed { get; set; }
        public double? FixedUnits { get; set; }
        public int? MinutesPerUnit { get; set; }
        public double? NumberOfUnits { get; set; } 
    }
}

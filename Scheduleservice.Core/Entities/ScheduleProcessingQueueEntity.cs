﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class ScheduleProcessingQueueEntity
    {
        public int HHA { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Guid batch_guid { get; set; }
        public int cgtask_id { get; set; }
        public string task_type { get; set; }
        public string validation_rule_json { get; set; }
        public DateTime processing_started_on { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class PaymentSourcesAdditionalEntity
    {
        public int PayerSourceId { get; set; }

        public bool IsEnableEVV { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }

        public int ROUNDOFF_Direction { get; set; }
        public int ROUNDOFF_Minutes { get; set; }

        public bool? Round_ZeroUnitScheduleToOne { get; set; }
        public bool DiscardDoubleBooking { get; set; }
        public bool EnableOvernightSplitScheduleToHaveSameDate { get; set; }



    }
}

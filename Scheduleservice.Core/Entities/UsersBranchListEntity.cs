﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UsersBranchListEntity
    {
        public int USER_BRANCH_ID { get; set; }
        public int USER { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public int HHA { get; set; }
    }
}

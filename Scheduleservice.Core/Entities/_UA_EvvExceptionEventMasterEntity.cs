﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _UA_EvvExceptionEventMasterEntity
    {
        public int exception_event_id { get; set; }
        public string event_code { get; set; }
        public string event_description { get; set; }
        public bool is_mandatory_event { get; set; }
        public bool is_evv_event { get; set; }
        public string kantime_default_exception_code { get; set; }
        public string kantime_default_exception_description { get; set; }
        public bool IsQueueEnabled { get; set; }
    }
}

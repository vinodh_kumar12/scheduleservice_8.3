﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class VisitChartsEntity
    {
        public int CHART_ID { get; set; }
        public int? VISIT_PLAN_ENTRY { get; set; }
        public int? CAREGIVER { get; set; }
        public DateTime VISIT_DATE { get; set; }
        public DateTime START_TIME { get; set; }
        public DateTime END_TIME { get; set; }
        public string STATUS { get; set; }
    }
}

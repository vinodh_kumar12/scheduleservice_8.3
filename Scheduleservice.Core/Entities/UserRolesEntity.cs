﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class UserRolesEntity
    {
        public int RolePermissionID { get; set; }
        public int RoleID { get; set; }
        public int PermissionIdx { get; set; }
        public int HHA { get; set; }
        public string PermissionDescription { get; set; }
        public int USER_ID { get; set; }
        public int User_Type { get; set; }
    }
}

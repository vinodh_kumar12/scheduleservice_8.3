﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskEVVExceptionsEntity
    {
        public int CaregiverTaskEvvExceptionID { get; set; }
        public int HHA { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int CgTaskID { get; set; }
        public string Context { get; set; }
        public string ExceptionCode { get; set; }
        public string ExceptionReason { get; set; }
        public string Notes { get; set; }
        public bool isResolved { get; set; }
        public DateTime ResolvedOn { get; set; }
        public int ResolvedBy { get; set; }
        public int CaregiverTaskEvvReasonID { get; set; }
        public string SystemCode { get; set; }
        public bool isSystemAutoAdded { get; set; }
        public int CaregiverTaskChildID { get; set; }
        public int EvvVendorVersionMasterID { get; set; }
        public bool isKanTimeCode { get; set; }
        public string ExceptionSystemType { get; set; }
        public bool isDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public string DeleteReason { get; set; }
        public string Resolution { get; set; }
        public string Resolution_comments { get; set; }
    }
}

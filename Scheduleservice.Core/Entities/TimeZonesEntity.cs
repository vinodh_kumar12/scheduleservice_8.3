﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class TimeZonesEntity
    {
        public string ZONE_ID { get; set; }
        public string NAME { get; set; }
    }
}

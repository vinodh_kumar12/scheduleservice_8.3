﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class ClientPaymentSourcesEntity
    {
        public int CLIENT_PAYMENT_ID { get; set; }
        public int PAYMENT_SOURCE_ID { get; set; }
        public int CLIENT_ID { get; set; }
        public int PAYMENT_PRIORITY { get; set; }
        public bool IS_AUTH_MAND { get; set; }
        public bool ACTIVE { get; set; }
        public int INTAKE_ID { get; set; }
        public bool isPrimaryInsurance { get; set; }
        public string VisitFrequencyDisciplines { get; set; }
        public string SchedulingRule { get; set; }
        public string AuthorizationDiscipline { get; set; }
        public string VisitFrequencyTypeForDisciplines { get; set; }
        public bool overridePayerAuthRequestSettings { get; set; }
        public bool authRequestNotRequired { get; set; }
        public bool IsEnableEVV { get; set; }
        public bool EvvExport_DiscardPayer { get; set; }
        public DateTime EvvExport_ExportEffectiveFrom { get; set; }
    }
}

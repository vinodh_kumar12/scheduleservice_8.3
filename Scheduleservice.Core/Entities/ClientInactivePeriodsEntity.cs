﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClientInactivePeriodsEntity
    {
        public DateTime TrasferDate { get; set; }
        public DateTime? ROCDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHAAggregatorEVVCodeResponseEntity
    {
        public int IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int EVVScheduleEditCodeMasterID { get; set; }
    }
}

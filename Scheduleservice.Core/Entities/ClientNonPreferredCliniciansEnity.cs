﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class ClientNonPreferredCliniciansEnity
    { 
        public int CLINICIAN_ID { get; set; }
        public DateTime? EffectiveFrom { get; set; }
    }
}

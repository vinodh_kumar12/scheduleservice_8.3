﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CDSClientBudgetLineItemEntity
    {
        public int BudgetLineItemMasterID { get; set; }
        public float HoursPerWeek { get; set; }
        public float WeeksPerYear { get; set; }
        public double TotalAmount { get; set; }
        public double Rate { get; set; }
        public string EffectiveStartDate { get; set; }
        public string EffectiveEndDate { get; set; }
        public int ClientBudgetLineItemID { get; set; }
        public int CaregiverID { get; set; }
        public int CDSPlanYearServiceID { get; set; }
        public double Amount { get; set; }
        public double TotalUsedAmount { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Scheduleservice.Core.Entities
{
    public class HHASelectedEvvMasterEntity
    {
        public int EvvVendorMasterID { get; set; }
        public string EvvVendorName { get; set; }
        public string EvvVendorID { get; set; }
        public bool isPrimary { get; set; }
    }
}

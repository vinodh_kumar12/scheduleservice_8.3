﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class ServiceCodesEntity
    {
        public int SERVICE_CODE_ID { get; set; }
        public string SERVICE_CODE { get; set; }
        public string DESCRIPTION { get; set; } 
        public string DISCIPLINE { get; set; } 
        public int? PAYMENT_SOURCE { get; set; }
        public int? SERVICE_PROPERTIES { get; set; }
        public bool? ACTIVE { get; set; }
        public int? PARENT_SERVICE_ID { get; set; }
        public int? ASSESSMENT_FLAG { get; set; }
        public string SERVICE_TYPE { get; set; }
        public int? DISPLAY_ORDER { get; set; } 
        public Double? TotalWorkHours { get; set; }
        public bool? isBillHourly { get; set; }
        public bool? isPayHourly { get; set; }
        public Double? HoursToCount { get; set; }
        public bool? useHoursToCountasActualHours { get; set; }
        public decimal? BillRate { get; set; }
        public decimal? PayRate { get; set; }
        public bool? isUnitsFixed { get; set; }
        public Double? FixedUnits { get; set; }
        public int? MinutesPerUnit { get; set; }
       
        public int? GroupID { get; set; }
        public string REVENUE_CODE { get; set; }
        public bool? IsSkillService { get; set; }
        public bool? IsElectronicChartSubmissionAllowed { get; set; }
        public bool? IsPaperChartSubmissionAllowed { get; set; }
        public int? FormID { get; set; }
        public Double? NumberOfUnits { get; set; }        
        public bool? isOTPayRateApplicable { get; set; }
        public bool? isHolidayPayRateApplicable { get; set; }
        public bool? EnableBaseRate { get; set; }
        public decimal? BaseRate { get; set; }
        public Double? BaseRateHours { get; set; }
        public string SystemServiceType { get; set; }
        public bool? isBillHourlyContractual { get; set; } 
        public DateTime? BillRateEffectiveFrom { get; set; } 
        public bool? enableSplitRevenueCode { get; set; }
        public Double? SplitRevenueCodeHours { get; set; }
        public string SplitRevenueCode { get; set; }
        public bool? IsTerminated { get; set; }
        public DateTime? effectiveEndDate { get; set; }
        public DateTime? TerminatedOn { get; set; }
        public int? TerminatedBy { get; set; }
        public bool? RevSplit_OverrideUnits { get; set; }
        public bool? RevSplit_isUnitsFixed { get; set; }
        public Double? RevSplit_FixedUnits { get; set; }
        public int? RevSplit_MinutesPerUnit { get; set; }
        public Double? RevSplit_NumberofUnits { get; set; }
        public string BillCode { get; set; }
        public int? ServiceCodeID_Live { get; set; }
        public int? ServiceCodeID_training { get; set; }
        public string PayrollExportGLCode { get; set; }
        public string BaseRateBillMode { get; set; }
        //servicegroups.ServiceCategory
        public int ServiceCategory { get; set; }
        public string QBInvoiceItemName { get; set; }
        public string QBInvoiceItemID { get; set; }
        public int? CDSBudgetLineItemMasterID { get; set; }
        public bool? enableWageParity { get; set; }
        public bool? IsIncludeOnlyNonPayableHoursInPayroll { get; set; }
        public bool isEvvEnabled { get; set; }
        public bool isPTO { get; set; }
        public string PTOTypeName { get; set; }
        public string PTOTypeCode { get; set; }
        public bool PTOValidateAgainstClinicianAccruedHours { get; set; }
        public bool isPTONotesMandatory { get; set; }
        public int? PTOTypeID { get; set; }
        public int ServiceCodeID { get; set; }
        public Double? WeightagePoints { get; set; }
        public decimal? BonusAmount { get; set; }
        public bool? Is_Billable { get; set; }
        public decimal? BonusAmount2 { get; set; }
        public decimal? BonusAmount3 { get; set; }
        public int? BonusHours { get; set; } 
        public bool? isUnitBasedBilling { get; set; }
        public decimal? ContractualBillRate { get; set; }
        public int? MinDurationMins { get; set; }
        public int? MaxDurationMins { get; set; }
        public bool? BillFirstHourComplete { get; set; }
        public int? BillFirstHourComplete_Hours { get; set; }
        public bool? DonotAllowClinicianToReschedule { get; set; }
        public bool? isBonusHourly { get; set; }
        public string HCPCSDescription { get; set; }
        public bool? isUnitBasedBillingContractual { get; set; } 
        public int? NumberOfPatientsServed { get; set; }
        public bool? IsPayable { get; set; }
        public bool? DiscardDeviation { get; set; } 
        public byte? PayrollRoundDirection { get; set; }
        public int? PayrollRoundMinutes { get; set; } 
        public bool? enableMaximumUnitsForTheSchedule { get; set; }
        public Double? MaximumUnitsForTheSchedule { get; set; } 
        public byte? RevFirstSplit_UnitRoundOffDirection { get; set; }
        public int? RevFirstSplit_UnitRoundOffMinutes { get; set; }
        public bool? RevSecondSplit_OverrideUnits { get; set; }
        public bool? RevSecondSplit_isUnitsFixed { get; set; }
        public Double? RevSecondSplit_FixedUnits { get; set; }
        public int? RevSecondSplit_MinutesPerUnit { get; set; }
        public Double? RevSecondSplit_NumberofUnits { get; set; }
        public byte? RevSecondSplit_UnitRoundOffDirection { get; set; }
        public int? RevSecondSplit_UnitRoundOffMinutes { get; set; } 
        public Guid? Publish_UID { get; set; }
    }
}

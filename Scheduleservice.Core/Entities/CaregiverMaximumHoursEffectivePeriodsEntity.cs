﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverMaximumHoursEffectivePeriodsEntity
    {
        public int CaregiverMaximumHoursEffectivePeriodID { get; set; }
        public int CaregiverID { get; set; }
        public int HHA { get; set; }
        public DateTime EffectiveStart { get; set; }
        public DateTime EffectiveEnd { get; set; }
        public float CaregiverMaxHoursPerWeek { get; set; }
        public bool CaregiverMaxHoursPerWeek_isHardStop { get; set; }
        public int Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public bool isdeleted { get; set; }
        public int deletedby { get; set; }
        public DateTime deletedon { get; set; }
    }
}

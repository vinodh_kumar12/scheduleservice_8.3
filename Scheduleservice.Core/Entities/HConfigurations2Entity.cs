﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HConfigurations2Entity
    {
        public int WEEK_START_DAY { get; set; }
        public string TIME_ZONE { get; set; }
    }
}

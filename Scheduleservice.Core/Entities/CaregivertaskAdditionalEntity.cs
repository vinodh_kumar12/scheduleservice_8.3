﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregivertaskAdditionalEntity
    {

        //  public int CGTaskID { get; set; }
        public string REVENUE_CODE { get; set; }
        public string GCODE { get; set; }
        public double? TotalCharges { get; set; }
        public float? Units { get; set; }
        public string NotesToClinician { get; set; }
        public bool? TimesheetSubmitted { get; set; }
        public bool? ApprovedForPayroll { get; set; }
        public byte? ApprovedForBonusPayRate { get; set; }
        public double? BonusAmount { get; set; }
        public int? PlaceOfService { get; set; }
        public bool? Has_Child_Schedules { get; set; }
        public string Modifier1 { get; set; }
        public string Modifier2 { get; set; }
        public string Modifier3 { get; set; }
        public string Modifier4 { get; set; }
        public int? ShiftID { get; set; }
        public bool? PRNApproved { get; set; }
        public int? PRNApprovedBy { get; set; }
        public DateTime? PRNApprovedOn { get; set; }
        public bool? IsSplitForBilling { get; set; }
        public bool? IsSplitForPayroll { get; set; }
        public double? BaseRate { get; set; }
        public int? CoSignStaff { get; set; }
        public bool? RequireCoSign { get; set; }
        public float? travelTimeHours { get; set; }
        public int? documentationTimeMins { get; set; }
        public byte? updatedToCM2k { get; set; }
        public int? ServicePrimarySkill { get; set; }
        public string PayerHours { get; set; }
        public string CancellationReason { get; set; }
        public int? OriginalCgTaskID { get; set; }
        public string CancelledNotes { get; set; }
        public bool? AideDailyTaskApproved { get; set; }
        public DateTime? AideDailyTaskApprovedOn { get; set; }
        public int? AideDailyTaskApprovedBy { get; set; }
        public string EVVVendorUniqueScheduleID { get; set; }
        public bool? PayrollExported { get; set; }
        public string CHECK_IN_LOCATION { get; set; }
        public string CHECK_OUT_LOCATION { get; set; }
        public bool? IsBillRateOverridden { get; set; }
        public bool? daylightSavingsAdjusted { get; set; }
        public int? daylightSavingsAdjustedBy { get; set; }
        public DateTime? daylightSavingsAdjustedOn { get; set; }
        public string MissedVisitNote { get; set; }
        public string UnbilledComment { get; set; }
        public string UnbilledCommentDescription { get; set; }
        public bool? IS_BILLRATE_UnitBased { get; set; }
        public double? Contractual_BillRate { get; set; }
        public bool? isBillHourly_Contractual { get; set; }
        public bool? isUnitBasedBilling_Contractual { get; set; }
        public int? CancelledBy { get; set; }
        public DateTime? CancelledOn { get; set; }
        public string NonBillableReason { get; set; }
        public float? CHECK_IN_DISTANCE { get; set; }
        public float? CHECK_OUT_DISTANCE { get; set; }
        public DateTime? BilledByContractAgencyOn { get; set; }
        public int? BilledByContractAgencyBy { get; set; }
        public DateTime? PaidToContractAgencyOn { get; set; }
        public int? PaidToContractAgencyBy { get; set; }
        public bool? GCodeConfirmed { get; set; }
        public int? evvUploadFailedAttempts { get; set; }
        public bool? IsBillModeOverriden { get; set; }
        public bool? IsContractualRateOverriden { get; set; }
        public bool? IsContractualModeOverriden { get; set; }
        public bool? overrideBonusRules { get; set; }
        public string SERVICE_LOCATION { get; set; }
        public bool? isMileageAutoCalculated { get; set; }
        public DateTime? MileageAutoCalculatedOn { get; set; }
        public bool? requireMileageReCalculation { get; set; }
        public float? AutoCalculatedMiles { get; set; }
        public string HyperTrackActionId { get; set; }
        public string HyperTrackingUrl { get; set; }
        public float? ManualMiles { get; set; }
        public string eChartConcurrencyGUID { get; set; }
        public bool? cosignStaffSetbyKanTime { get; set; }
        public bool? IsTravelTimeAutoCalculated { get; set; }
        public float? AutoCalculatedTravelTimeMins { get; set; }
        public float? ManualTravelTimeMins { get; set; }
        public string DeletedContext { get; set; }
        public string EvvTokenCode_Checkin { get; set; }
        public string EvvTokenCode_CheckinOriginal { get; set; }
        public string EvvTokenCode_Checkout { get; set; }
        public string EvvTokenCode_CheckoutOriginal { get; set; }
        public bool? isRescheduledSchedule { get; set; }
        public int? EvvMyUniqueID { get; set; }
        public bool? isLostRevenue { get; set; }
        public DateTime? TravelTimeAutoCalculatedOn { get; set; }
        public bool? isEvvschedule { get; set; }
        public bool? isEvvScheduleDirty { get; set; }
        public char? EvvCheckinLocationVerified { get; set; }
        public char? EvvCheckoutLocationVerified { get; set; }
        public int? CheckinTreatmentLocation { get; set; }
        public int? CheckoutTreatmentLocation { get; set; }
        public byte? NotesToCliniciantype { get; set; }
        public DateTime? Receivable_FollowupDate { get; set; }
        public bool? isAttested { get; set; }
        public int? AttestedBy { get; set; }
        public DateTime? AttestedOn { get; set; }
        public bool? IsGeolocationallowedbyUser { get; set; }
        public bool? isEvvAggregatorExportRequired { get; set; }
        public bool? isVisitVerifiedWithEvvAggregator { get; set; }
       
    }
}

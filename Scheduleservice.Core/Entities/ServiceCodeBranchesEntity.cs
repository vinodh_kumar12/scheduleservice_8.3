﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ServiceCodeBranchesEntity
    {
        public int ServiceCodeBranchID { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public int SERVICE_CODE_ID { get; set; }
        public bool? isUnitsFixed { get; set; }
        public double? FixedUnits { get; set; }
        public int? MinutesPerUnit { get; set; }
        public double? NumberOfUnits { get; set; }
        public bool? enableSplitRevenueCode { get; set; }
        public double? SplitRevenueCodeHours { get; set; }
        public int? RevSplit_FixedUnits { get; set; }
        public bool? RevSplit_isUnitsFixed { get; set; }
        public int? RevSplit_MinutesPerUnit { get; set; }
        public double? RevSplit_NumberofUnits { get; set; }
        public int? RevFirstSplit_UnitRoundOffDirection { get; set; }
        public int? RevFirstSplit_UnitRoundOffMinutes { get; set; }
        public bool? RevSplit_OverrideUnits { get; set; } 
        public bool? RevSecondSplit_OverrideUnits { get; set; }
        public double? RevSecondSplit_FixedUnits { get; set; }
        public bool? RevSecondSplit_isUnitsFixed { get; set; }
        public int? RevSecondSplit_MinutesPerUnit { get; set; }
        public double? RevSecondSplit_NumberofUnits { get; set; }
        public int? RevSecondSplit_UnitRoundOffDirection { get; set; }
        public int? RevSecondSplit_UnitRoundOffMinutes { get; set; }

    }
}

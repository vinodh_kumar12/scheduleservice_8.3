﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskValidationErrorsDependentSchedulesEntity
    {
        public int DependentScheduleID { get; set; }
        public int HHA { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string exception_type { get; set; }
        public int SourceCgTaskID { get; set; }
        public int dependentCgTaskID { get; set; }
    }
}

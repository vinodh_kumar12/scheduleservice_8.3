﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHAEVVExceptionEventCodesEntity
    {
        public int ExceptionEventCodeID { get; set; }
        public int HHA { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int EvvExceptionEventID { get; set; }
        public int EvvVendorversionMasterID { get; set; }
        public string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UAAgencyDatabaseMasterListEntity
    {
        public int HHA_ID { get; set; }
        public string AgencyName { get; set; }
        public int DatabaseID { get; set; }
        public string DatabaseName { get; set; }
        public string Server { get; set; }
        public int EvvDatabaseID { get; set; }
        public Guid? Publish_UID { get; set; }
    }
}

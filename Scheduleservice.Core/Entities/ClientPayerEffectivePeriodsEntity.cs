﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClientPayerEffectivePeriodsEntity
    {
        public int PayerEffectivePeriodID { get; set; }
        public int HHA { get; set; }
        public int ClientId { get; set; }
        public int IntakeId { get; set; }
        public int PayerID { get; set; }
        public int ClientPaymentSourceID { get; set; }
        public char PrimaryPayerType { get; set; }
        public DateTime EffectiveStart { get; set; }
        public DateTime EffectiveEnd { get; set; }
        public int Client_Additional_Payer_ID { get; set; }
        public string Insuranceid { get; set; }
        public string insuranceGroupNo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _UA_FormsEntity
    {
        public int StartOfCareType { get; set; }
        public bool? isPreSOCForm { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _E_ExpiringItemsEntity
    {
        public int CLINICIAN_EXPIRING_ITEM_ID { get; set; }
        public int HHA_EXPIRING_ITEM_ID { get; set; }
        public int HHA { get; set; }
        public int CLINICIAN_ID { get; set; }
        public DateTime? EXPIRY_DATE { get; set; }
        public string Description { get; set; }
        public DateTime? IssuedDate { get; set; }
        public string Licenseno { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string LicenseState { get; set; }
        public bool? IsExpiryNotified { get; set; }
    }
}

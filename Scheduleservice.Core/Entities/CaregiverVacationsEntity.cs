﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverVacationsEntity
    {
        public int VACATIONID { get; set; }
        public int HHA { get; set; }
        public int CAREGIVERID { get; set; }
        public DateTime? START_DATE { get; set; }
        public DateTime? END_DATE { get; set; }
        public DateTime? ADDEDON { get; set; }
        public int ADDEDBY { get; set; }
        public DateTime? LASTMODIFIEDON { get; set; }
        public int? LASTMODIFIEDBY { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

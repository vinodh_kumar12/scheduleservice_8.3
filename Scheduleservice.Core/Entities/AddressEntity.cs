﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class AddressEntity
    {
        public int ADDRESS_ID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ScheduleShiftEntity
    {
        public int SHIFT_ID { get; set; }
        public int HHA { get; set; }
        public string SHIFT_NAME { get; set; }
    }
}

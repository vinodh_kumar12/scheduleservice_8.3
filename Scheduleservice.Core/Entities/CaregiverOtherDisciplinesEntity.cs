﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverOtherDisciplinesEntity
    {
        public int CAREGIVER_DISC_ID { get; set; } 
        public int ServiceCategory { get; set; }

        public int CaregiverID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class EchartSignaturesEntity
    {
        public int SIGNATURE_ID { get; set; }
        public int HHA { get; set; }
        public int? ECHARTMASTERID { get; set; }
        public int? CLIENT_ID { get; set; }
        public string? CONTEXT { get; set; }
        public int? CgTaskID { get; set; }
    }
}

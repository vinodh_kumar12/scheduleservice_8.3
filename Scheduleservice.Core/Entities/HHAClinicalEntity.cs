﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Scheduleservice.Core.Entities
{
    public class HHAClinicalEntity
    {
        public bool ConsiderSplitScheduleToValidateAuthService { get; set; }
        public bool? enableCDS { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHABranchAggregatorEntity
    {
        public int EvvConfigurationID { get; set; }
        public string VendorName { get; set; }
        public bool AttestationMandatoryForAllReasonCodes { get; set; }
        public string AttestationMandatoryEvent { get; set; }
        public bool AttestationHideFromClinician { get; set; }
    }
}

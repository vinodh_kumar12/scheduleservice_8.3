﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class LineofbusinessEntity
    {
        public int LOB_ID { get; set; }
        public string LOB_NAME { get; set; }
        public bool allowOnlyRegisteredServicesForSchedules { get; set; }
        public bool isEpisodeLess { get; set; }
        public bool allowCreateaideScheduleOnlyToOrientationPerformed { get; set; }
        public string LOBType { get; set; }
    }
}

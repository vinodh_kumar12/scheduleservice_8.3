﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHAScheduleAuthSettingsEntity
    {
        public int AutoGenerateNoShowExceptionMin { get; set; }
        public int AutoGenerateMissedCheckoutExceptionMin { get; set; }
        public int AutoGenerateUnscheduledExceptionMin { get; set; }
        public int AutoGenerateShortVisitExceptionMin { get; set; }
        public int AutoGenerateLongVisitExceptionMin { get; set; }
        public int AutoGenerateLateCheckinExceptionMin { get; set; }
        public int EVVOverageExceptionthreshold { get; set; }
        public int EVVOverNightExceptionthreshold { get; set; }
        public bool SplitOvernightSchedulesAtCreationlevel { get; set; }
        public bool AllowToCheckoutifHardStopValidationExists { get; set; }
        public bool AllowToCheckoutifHardStopValidationExistsForEVV { get; set; }
        public bool AllowToCheckoutifHardStopValidationExistsForNonEVV { get; set; }
        public bool EnableAutoSplitWhenActualHrsExceedPlannedHrs { get; set; }
        public int AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForKantimePrimaryEVV { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForOtherPrimaryEVV { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForNonEVV { get; set; }
        public string AutoSplitWhenActualHrsExceedPlannedHrsPayers { get; set; }
        public string AutoSplitWhenActualHrsExceedPlannedHrsDiscplines { get; set; }
        public bool EnableAutoSplitforAdditionalHoursAtStartTimeandEndTime { get; set; }
        public bool EnableAutoSplitforAdditionalHoursAtOnlyEndTime { get; set; }
    }
}

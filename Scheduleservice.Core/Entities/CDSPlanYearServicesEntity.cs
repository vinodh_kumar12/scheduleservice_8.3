﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CDSPlanYearServicesEntity
    {
        public int CDSPlanYearServiceID { get; set; }
        public float WeeklyHours { get; set; }
        public int PayerCDSServiceCategoryID { get; set; }
        public bool enableWeekAuthLimitOnHours { get; set; }
        public int MinutesPerUnit { get; set; }
        public double NumberOfUnits { get; set; }
        public string effectiveStart { get; set; }
        public string effectiveEnd { get; set; }
        public int ClientID { get; set; }
        public int PayerID { get; set; }
        public int CDSPlanYearMasterID { get; set; }
        public string AuthorizationNo { get; set; }
        public int InitialPlanYearServiceID { get; set; }
        public string Status { get; set; }
        public bool IsOfficeAuth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


    }
}

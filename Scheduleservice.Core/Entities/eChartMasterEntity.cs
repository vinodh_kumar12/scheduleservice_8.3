﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class eChartMasterEntity
    {
        public int eChartMasterID { get; set; }
        public int HHA { get; set; }
        public int isPchart { get; set; }
        public int CgTaskId { get; set; }
        public int ClientID { get; set; }
        public DateTime eChartDate { get; set; }
        public int eChartType { get; set; }
        public int ServiceCodeId { get; set; }
        public int POC { get; set; }
        public int FormID { get; set; }
        public int Caregiver { get; set; }
        public bool isSubmitted { get; set; }
        public bool isApproved { get; set; }
        public int TimePoint { get; set; }
        public int? isDeleted { get; set; }
    }
}

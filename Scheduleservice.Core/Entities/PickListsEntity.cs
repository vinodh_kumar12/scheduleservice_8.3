﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
   public class PickListsEntity
    {
        public int PICKLIST_ID;
        public string NAME;
        public bool EXPANDABLE;
        public int HHA;
        public DateTime CreatedOn;
        public DateTime UPDATED_TIMESTAMP_FOR_OFFLINE;
    }
}

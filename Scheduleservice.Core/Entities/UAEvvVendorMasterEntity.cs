﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UAEvvVendorMasterEntity
    {
        public int EvvVendorMasterID { get; set; }
        public string VendorName { get; set; }
        public bool isPrimary { get; set; }

    }
}

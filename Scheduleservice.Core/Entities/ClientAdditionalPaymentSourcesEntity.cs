﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClientAdditionalPaymentSourcesEntity
    {
        public int ADDITIONAL_PAYER_SOURCE_ID { get; set; }
        public int CLIENT_PAYER_SOURCE_ID { get; set; }
        public int PAYER_SOURCE_ID { get; set; }
        public int ADDITIONAL_PAYER_FLAG { get; set; }
        public bool IsEnableEVV { get; set; }
        public bool EvvExport_DiscardPayer { get; set; }
        public DateTime EvvExport_ExportEffectiveFrom { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }
        public DateTime EffectiveStart { get; set; }
        public DateTime EffectiveEnd { get; set; }
    }
}

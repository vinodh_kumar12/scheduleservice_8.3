﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class PaymentSourceAuthorizationsEntity
    {
        public int AUTHORIZATION_ID { get; set; }
        public double? UNUSED { get; set; }
        public bool? IsAuthVisitBased { get; set; }
        public double? TOTAL_UNITS { get; set; }
        public double? DailyLimit { get; set; }
        public double? WeeklyLimit { get; set; }
        public double? MonthlyLimit { get; set; }
        public bool? isAuthHourlyBased { get; set; }
        public int? NUMBER_OF_VISITS { get; set; }
        public string AUTHORIZATION_NO { get; set; }
    }
}

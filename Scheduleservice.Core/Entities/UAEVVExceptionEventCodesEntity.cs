﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UAEVVExceptionEventCodesEntity
    {
        public int ExceptionEventCodeID { get; set; }
        public int ExceptionEventMasterID { get; set; }
        public string Code { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}

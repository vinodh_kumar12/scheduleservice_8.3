﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class HomeHealthAgencies_BillingPayrollSettingsEntity
    {
        public bool splitHolidayOvernightSchedule { get; set; }
        public TimeSpan PayrollBusinessHoursStartTime { get; set; }
        public TimeSpan PayrollBusinessHoursEndTime { get; set; }
        public TimeSpan BillingBusinessHoursStartTime { get; set; }
        public TimeSpan BillingBusinessHoursEndTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class DualEligibilePayersEntity
    {
        public int DualEligibilePayerID { get; set; }
        public int ClientPaymentSourceID { get; set; }
        public int PaymentsourceId { get; set; }
        public bool IsEnableEVV { get; set; }
        public bool EvvExport_DiscardPayer { get; set; }
        public DateTime EvvExport_ExportEffectiveFrom { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }
        public DateTime Effective_Start { get; set; }
        public DateTime Effective_End { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class ClientVacationMasterEntity
    {
        public int ClientVacationMasterID { get; set; }
        public int HHA { get; set; }
        public DateTime? createdon { get; set; }
        public int createdby { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public int? LastModifiedBy { get; set; }
        public int ClientID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ReasonID { get; set; }
        public string Notes { get; set; }
    }
}

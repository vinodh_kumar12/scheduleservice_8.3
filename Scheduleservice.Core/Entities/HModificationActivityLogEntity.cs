﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HModificationActivityLogEntity
    {
        public int ModificationID { get; set; }
        public string ActivityName { get; set; }
        public int ActivityType { get; set; }
        public int HHA { get; set; }
        public int? ClientNo { get; set; }
        public int? ClinicianID { get; set; }
        public int OperatorID { get; set; }
        public DateTime datetime { get; set; }
        public string RecordIdentityName { get; set; }
        public int RecordIdentityValue { get; set; }
        public string UserDefined1 { get; set; }
        public string UserDefined2 { get; set; }
        public string UserDefined3 { get; set; }
        public string UserDefined4 { get; set; }
        public string UserDefined5 { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public int? EpisodeID { get; set; }
        public int? PageID { get; set; }
        public bool? AuditNotShow { get; set; }
        public bool isSingleActivity { get; set; }
        public bool? isGroupActivity { get; set; }
        public int? OasisDatasetID { get; set; }
        public int? eChartMasterID { get; set; }
        public int? OperatorType { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? PermissionIdx { get; set; }
        public bool IsMovetoHDFS { get; set; }
        public bool EncValuesExist { get; set; }
        public string EncVal1 { get; set; }
        public string EncVal2 { get; set; }
        public bool IsArchived { get; set; }
        public bool IsArchiveInProgress { get; set; }
        public string AgencyLoginID { get; set; }
        public string SuperAdminLoginID { get; set; }
        public string AuditField { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}

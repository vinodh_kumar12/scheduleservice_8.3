﻿ using System;
using System.Collections.Generic;
using System.Text; 

namespace Scheduleservice.Core.Entities
{
    public class HHAScheduleConflictExceptionsEntity
    {

        public int hha_exception_id { get; set; }
        public int exception_id { get; set; }
        public int exception_code { get; set; }
        public string exception_type { get; set; }
        public string exception_message { get; set; }
        public bool can_resolve { get; set; }
        public string comments_mandatory { get; set; }
        public string exception_event { get; set; }
    }
}

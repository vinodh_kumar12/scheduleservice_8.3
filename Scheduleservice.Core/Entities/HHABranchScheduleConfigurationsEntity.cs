﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHABranchScheduleConfigurationsEntity
    {
        public bool enableTravelTime { get; set; }
        public bool enableDOcumentationTime { get; set; }
        public bool ScheduleCalendarBasedonWeekStartDay { get; set; }
        public bool CreateCustomEpisode { get; set; }
        public bool ClinicianCreateScheduleAllowOnlyPrimaryPayer { get; set; }
        public bool CAN_CREATE_ADHOC_SCHEDULE { get; set; }
        public bool IsMilitaryTimeFormat { get; set; }
        public byte NotAllowScheduleDoubleBooking { get; set; }
        public byte NotAllowScheduleDeviation { get; set; }
        public bool DonotValdateDoubleBookforThirdPartyClinicians { get; set; }
        public bool ignoreDoubleBookForNonBillable { get; set; }
        public bool IgnoreDeviationForNonBillable { get; set; }
        public bool ignoreDeviationForSupervisorSchedules { get; set; }
        public bool ignoreDeviationForDifferentServiceDiscplines { get; set; }
        public int doubleBookGracePeriodMinutes { get; set; }
        public byte scheduleDeviationGracePeriod { get; set; }
        public bool ignoreacrossLobAdmits { get; set; }
        public bool EnableBillingofMissedVisitTravelTime { get; set; }
        public bool IsActualTimeForCheckin_CheckOutLinks { get; set; }
        public bool EnableClinicianMaxHoursPerWeek { get; set; }
        public bool EnableClientMaxHoursPerWeek { get; set; }
        public float ClientMaxHoursPerWeek { get; set; }
        public bool ClientMaxHoursPerWeek_isHardStop { get; set; }
        public char scheduleOverTimeRule { get; set; }
        public bool EnableVisitFrequency { get; set; }
        public bool doNotAllowSchedulePlannedHoursOutsideServiceDurations { get; set; }
        public bool allowToHaveSchedulesInInactiveRegion_AdultNonMedicare { get; set; }
        public bool allowToHaveSchedulesInInactiveRegion_Pedi { get; set; } 
        public bool isAuthMonthlyBasedOnAuthStartDate { get; set; }
        public bool isAuthWeeklyBasedOnAuthStartDate { get; set; } 
        public bool EnableScheduleAutoSplitBasedOnBusinessHours { get; set; }
        public bool splitHolidayOvernightSchedule { get; set; }
        public TimeSpan BillingBusinessHoursStartTime { get; set; }
        public TimeSpan PayrollBusinessHoursStartTime { get; set; }
        public bool SplitOvernightSchedulesForPayroll { get; set; }
        public bool AllowSchedulesPriortoSOC { get; set; }
        public bool allowSchedulesPriorToSOCDate { get; set; }
        public bool AllowNonBillableSchedulesPriorToSOC { get; set; }
        public bool AllowSinglePreSOCVisit { get; set; }
        public bool AllowMultiplePreSOCVisit { get; set; }
        public int WEEK_START_DAY { get; set; }
        public bool SetTravelTimeRuleasPayrolledHours { get; set; }
        public string autoApprovepChartDisciplines { get; set; }
        public bool EnabledautoApprovepChart { get; set; }
        public int PayrollRule { get; set; }
        public bool ExcludePlannedSchedulesDoubleBookConflictWithCompletedSchedule { get; set; }
        public bool ExcludePlannedSchedulesDeviationConflictWithCompletedSchedule { get; set; }
        public bool AllowToChangeClinicianWheneChartIsFilled { get; set; }
        public bool allowToCreateNonBillableScheduleOnInactiveRegion { get; set; }
        public byte PayrollRoundOffDirection { get; set; }
        public int PayrollRoundOffMinutes { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClientTreatmentAddressEntity
    {
        public int ClientTreatmentAddressID { get; set; }
        public int TreatmentAddressID { get; set; }
    }
}

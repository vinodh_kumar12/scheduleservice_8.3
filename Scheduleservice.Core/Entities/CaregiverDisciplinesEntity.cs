﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverDisciplinesEntity
    {
        public int CAREGIVER_DISC_ID { get; set; }
        public int BASE_DISCIPLINE_ID { get; set; }
        public int ServiceCategory { get; set; }
    }
}

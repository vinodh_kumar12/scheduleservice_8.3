﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UsersEntity
    {
        public int USER_ID { get; }
        public int? LOB_ID { get; set; }
        public int USER_PK { get; set; }
        public int ROLE { get; set; }
        public int? User_Type { get; set; }
        public int? ROLE_TYPE { get; set; }
        public bool? RestrictAccessToOwnRecords { get; set; }
    }
}

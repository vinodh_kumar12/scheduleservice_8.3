﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class PaymentsourcesEntity
    {
        public int PAYMENT_SOURCE_ID { get; set; }
        public string ORG_NAME { get; set; }        
        public bool IS_AUTH_MANDATORY { get; set; }       
        public byte AuthType { get; set; }
        public bool Auth_Enable_DailyLimit { get; set; }
        public bool Auth_Enable_WeeklyLimit { get; set; }
        public bool Auth_Enable_MonthlyLimit { get; set; }
        public bool Auth_DailyLimit_HardStop { get; set; }
        public bool Auth_WeeklyLimit_HardStop { get; set; }
        public bool Auth_MonthlyLimit_HardStop { get; set; }
        public byte Auth_RuleOnNonAuth { get; set; }
        public bool Auth_EnableOfficeAuth { get; set; }        
        public bool SplitOvernightSchedules { get; set; }        
        public bool isAuthHourlyBased { get; set; } 
        public bool SplitOvernightScheduleByWeek { get; set; }
        public bool OverNightScheduleApplicabletoPrimaryEVV { get; set; }
        public bool OverNightScheduleApplicabletoOtherEVV { get; set; }
        public bool OverNightScheduleApplicabletoNonEVV { get; set; }
        public bool RequireAuthNPINo { get; set; }  
        public byte? ROUNDOFF_Direction { get; set; }
        public int? ROUNDOFF_Minutes { get; set; }
        public int EpisodeDuration { get; set; }
        public bool? AuthFormNOTRequiredToRequestAuth { get; set; }
        public string AuthRequest_OrdersRequired { get; set; }        
        public int? AutoAuthNumberOfSchedules { get; set; }        
        public bool? DiscardDoubleBooking { get; set; }        
        public byte? ROUNDOFFEditedHours_Direction { get; set; }
        public int? ROUNDOFFEditedHours_Minutes { get; set; } 
        public string VisitFrequencyDisciplines { get; set; }
        public string SchedulingRule { get; set; }
        public bool? EnableAutoAuth_Disciplines { get; set; }
        public bool? EnableAutoAuth_SN { get; set; }
        public int? EnableAutoAuth_SN_NumberOfVIistsOrHours { get; set; }
        public bool? EnableAutoAuth_Aide { get; set; }
        public int? EnableAutoAuth_Aide_NumberOfVIistsOrHours { get; set; }
        public bool? SplitAuthForOvernightWeekCrossOver { get; set; } 
        public bool? ROUNDOFFEditedHours_ExcludePayableHours { get; set; }
        public bool? Enable837FileAutoTransmisisonToSHP { get; set; }
        
        public bool? EnableOvernightSplitScheduleToHaveSameDate { get; set; }         
        public bool? IsAuthNumberNotReqforAddingAuth { get; set; }        
        public int? TimelyFilingDays { get; set; }         
        public bool? Round_ZeroUnitScheduleToOne { get; set; }         
        public bool? IsEnableEVV { get; set; }
        public bool? enableDollarAmountTrackingOnAuthorization { get; set; }
        public bool? authRequestNotRequired { get; set; }
        public bool? isCDSPayer { get; set; } 
        public bool EnableAuthorizationRestrictionToDayofWeek { get; set; } 
        public bool? splitScheduleForPartialAuthAllocation { get; set; }  
        public string PrimaryEvvVendorID { get; set; }
        public string SecondaryEvvVendorID { get; set; } 
        public int? EvvAggregatorVendorVersionMasterID { get; set; } 
        public bool? IsPrimaryEVV { get; set; } 
        public bool? requireAttestationForScheduleApprovalForNonEvv { get; set; }
        public bool? requireAttestationForScheduleApprovalForEvv { get; set; } 
        public bool? requireEvvReasonCodesResolutionForApproval { get; set; }
        public bool IsEpisodicBilling { get; set; }
        public bool PayerPrimaryType { get; set; }
        public string MasterPayerCode { get; set; }

        public int? evv_configuration_id { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHAEvvVendorExceptionEventsEntity
    {
        public string event_code { get; set; }
        public string event_description { get; set; }
        public bool isVendorConfiguredEvent { get; set; }
        public bool isConfiguredForHHA { get; set; }
        public int EvvExceptionEventID { get; set; }
        public string exception_code { get; set; }
        public int ExceptionEventCodeID { get; set; }

        public string kantime_default_exception_code { get; set; }

        public string kantime_default_exception_description { get; set; }

    }
}

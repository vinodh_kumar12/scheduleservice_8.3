﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class HHAEVVScheduleConfigurationGroupEntity
    {
        public int EvvScheduleConfigurationGroupID { get; set; }
        public bool LocationValidation_isMandatory { get; set; }
        public bool LocationValidation_EnableGeoFencing { get; set; }
        public int LocationValidation_MaxDistance { get; set; }
        public string LocationValidation_NoLocationRule { get; set; }
        public string LocationValidation_OutsideFenceRule { get; set; }
        public string PayerDisciplinesJSON { get; set; }
        public bool isApplicableToPrimaryEvvSchedule { get; set; }
        public bool isApplicableToSecondaryEvvSchedule { get; set; }
    }

    public class RootObjectPayerDisciplines
    {
        public int PayerID { get; set; }
        public int GroupID { get; set; }
    }
}

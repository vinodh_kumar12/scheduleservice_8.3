﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class HHAEVVConfigurationsEntity
    {
        public int EvvConfigurationID { get; set; }
        //public int HHABranchID { get; set; }
        public int EvvVendorVersionMasterID { get; set;}
        public DateTime EffectiveDate { get; set; }
        public bool AttestationMandatoryForAllReasonCodes { get; set; }
        public bool MissedVisitReasonCodeAvailable { get; set; }
        public bool MissedVisitActionCodeAvailable { get; set; }
        public bool ExcepionCodeAvailable { get; set; }
        public bool ReasonCodeAvailable { get; set; }
        public bool ActionCodesAvailable { get; set; }
        public bool captureExceptionCodeOnReasonCode { get; set; }
        public bool requireAllExceptionsToClearForApproval { get; set; }
        public bool isActionCodeMandatoryOnReasonCode { get; set; }
        public bool isExceptionCodeMandatoryOnReasonCode { get; set; }
        public bool isMissedVisitActionCodeMandatoryOnReasonCode { get; set; }
        public bool canGrowMissedVisitReason { get; set; }
        public bool canGrowMissedVisitAction { get; set; }
        public bool canGrowException { get; set; }
        public bool canGrowReasonCode { get; set; }
        public bool canGrowActionCode { get; set; }
        public int HHA { get; set; }
        public string AttestationMandatoryEvent { get; set; }
        public bool AttestationHideFromClinician { get; set; }
        public string configuration_name { get; set; }

        public bool require_latlong_for_fob { get; set; }

    }
}

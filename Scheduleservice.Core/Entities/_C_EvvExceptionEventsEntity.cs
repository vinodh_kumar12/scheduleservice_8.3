﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _C_EvvExceptionEventsEntity
    {
        public int EVVVendorVersionMasterID { get; set; }
        public string Event { get; set; }
      //  public string exception_code { get; set; }
        public bool IsQueueEnabled { get; set; }    
        public DateTime lastQueueExecutedon { get; set; }
        public int EvvExceptionEventID { get; set; }
    }
}

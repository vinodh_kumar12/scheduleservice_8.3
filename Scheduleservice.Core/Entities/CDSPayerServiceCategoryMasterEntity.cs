﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CDSPayerServiceCategoryMasterEntity
    {
        public int PayerCDSServiceCategoryID { get; set; }
        public string WeeklyHourExceedingRule { get;set; }
        public string CategoryName { get; set; }
    }
}

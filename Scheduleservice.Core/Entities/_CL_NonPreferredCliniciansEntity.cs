﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _CL_NonPreferredCliniciansEntity
    {
        public int NON_PREFERRED_CLINICIAN_ID { get; set; }
        public int HHA { get; set; }
        public int CLIENT_ID { get; set; }
        public int CLINICIAN_ID { get; set; }
        public int CREATED_BY { get; set; }
        public DateTime CREATED_ON { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? EffectiveFrom { get; set; }
    }
}

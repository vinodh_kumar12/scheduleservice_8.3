﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _UA_ScheduleConflictExceptionsEntity
    {
        public int exception_id { get; set; }
        public int exception_code { get; set; }
        public string exception_type { get; set; }
        public string exception_message { get; set; }
        public string exception_event { get; set; }
        public bool can_resolve { get; set; }
        public bool comments_mandatory { get; set; }
    }
}

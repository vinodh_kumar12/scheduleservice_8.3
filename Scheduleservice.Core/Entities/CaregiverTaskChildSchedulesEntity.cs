﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskChildSchedulesEntity
    {
        public int HHA { get; set; }
        public int Child_Schedule_Id { get; set; }
        public int PARENT_CGTASK_ID { get; set; }
        public int SERVICECODE_ID { get; set; }

        public int PAYMENT_SOURCE { get; set; }
        public bool isEvvschedule { get; set; }
        public bool isEvvScheduleDirty { get; set; }

        public int EvvAggregatorVendorVersionMasterID { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public int CDSAuthServiceID { get; set; }
        public int? TOTALMINUTES_ACTUAL { get; set; }
        public bool IsRealEVV { get; set; }
        public int CLIENT { get; set; }
        public DateTime SCHEDULE_DATE { get; set; }
        public long? childScheduleUniqueIDKT { get; set; }

        public DateTime ? ACTUAL_STARTTIME { get; set; }

        public DateTime ? ACTUAL_ENDTIME { get; set; }

        public DateTime PLANNED_STARTTIME { get; set; }

        public DateTime PLANNED_ENDTIME { get; set; }

        public bool CANBILL { get; set; }
        public string STATUS { get; set; }
        public string Edited_Hours { get; set; }

        public int POC { get; set; }
        public DateTime ACTUAL_DATE { get; set; }
        public int TOTALMINUTES_PLANNED { get; set; }
        public bool isManualSplit { get; set; }
        public bool IsAuthMandatory { get; set; }
        public float Units { get; set; }
        public CaregiverTaskChildSchedulesEntity()
        {
            IsRealEVV = true;
        }
        public bool enablePayableHours { get; set; }
        public int? TOTALMINUTES_ACTUAL_PAYABLE { get; set; }
        public string CHECK_IN_LOCATION { get; set; }
        public string CHECK_OUT_LOCATION { get; set; }
        public int CHECK_IN_DISTANCE { get; set; }
        public int? CHECK_OUT_DISTANCE { get; set; }
        public byte CheckinSource { get; set; }
        public byte CheckoutSource { get; set; }

    }
}

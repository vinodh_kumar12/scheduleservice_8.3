﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskChildScheduleTerminalPayerEntity
    {
        public int CaregiverChildTaskTerminalPayerID { get; set; }
        public int HHA { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int CgTaskID { get; set; }
        public int ChildScheduleID { get; set; }
        public Int64 KanTimeUniqueID { get; set; }
        public string EvvVendorUniqueID { get; set; }
        public int ClientID { get; set; }
        public int PrimaryPayerID { get; set; }
        public char Hierarchy { get; set; }
        public int TerminalPayerID { get; set; }
       // public int Terminal_aggregator_id { get; set; }
        public int terminal_configuration_ID { get; set; }
        public bool is_exported { get; set; }
        public int? Last_export_status { get; set; }
        public DateTime First_success_exported_on { get; set; }
        public DateTime? Last_success_exported_on { get; set; }
        public DateTime Last_failed_on { get; set; }
        public string Last_failed_error_code { get; set; }
        public string Last_failed_error_message { get; set; }
        public bool isDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public int DeletedBy { get; set; }
        public DateTime SCHEDULE_DATE { get; set; }

        public int SERVICECODE_ID { get; set; }

        public bool IS_BILLED { get; set; }
        public bool IsPayrolled { get; set; }

        public DateTime ? ACTUAL_ENDTIME { get; set; }
        public DateTime ? ACTUAL_STARTTIME { get; set; }

        public DateTime ? PLANNED_STARTTIME { get; set; }
        public DateTime ? PLANNED_ENDTIME { get; set; }
        public int PARENT_CGTASK_ID { get; set; }

        public bool isEvvschedule { get; set; }

        public int PAYMENT_SOURCE { get; set; }

        public bool CANBILL { get; set; }

        public bool Ispayable { get; set; }
        public bool is_dirty { get; set; }

        public string status { get; set; }
        public int? TOTALMINUTES_ACTUAL { get; set; }
        public string Edited_Hours { get; set; }
        public int? evv_configuration_id { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ServiceGroupsEntity
    {
        public int GroupID { get; set; }
        public int ServiceCategory { get; set; }
        public string CareplanDiscipline { get; set; }
        public string GroupDiscplineName { get; set; }
    }
}

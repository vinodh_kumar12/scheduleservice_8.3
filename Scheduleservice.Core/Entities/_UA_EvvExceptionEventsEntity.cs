﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _UA_EvvExceptionEventsEntity
    {
        public int EVVVendorVersionMasterID { get; set; }
        public string Event { get; set; }
        //public string exception_code { get; set; }
       // public string additional_exception_code2 { get; set; }
        //public string additional_exception_code3 { get; set; }
        public int ExceptionEventMasterID { get; set; }
        public bool IsQueueEnabled { get; set; }

    }
}

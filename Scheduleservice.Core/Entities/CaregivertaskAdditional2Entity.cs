﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregivertaskAdditional2Entity
    {
        // public int CgTaskID { get; set; }
        public int? SMS_ScheduleAlertSentStatus { get; set; }
        public DateTime? SMS_ScheduleAlertSentOn { get; set; }
        public DateTime? SMS_ScheduleAlertSentStatusLastUpdatedTime { get; set; }
        public int? SMS_SchedeuleLateCheckinStatus { get; set; }
        public DateTime? SMS_SchedeuleLateCheckinSentOn { get; set; }
        public DateTime? SMS_SchedeuleLateCheckinStatusLastUpdatedTime { get; set; }
        public bool? patientUnableToDigitalSign { get; set; }
        public string patientUnableToDigitalSignReason { get; set; }
        public string patientUnableToDigitalSignNotes { get; set; }
        public bool? SchedulesOver24HrDayReviewed { get; set; }
        public int? SchedulesOver24HrDayReviewedBy { get; set; }
        public DateTime? SchedulesOver24HrDayReviewedOn { get; set; }
        public bool? HomeboundStatus { get; set; }
        public string AdditionalHCPCSAndModifiers { get; set; }
        public bool? isHCPCSModified { get; set; }
        public bool? isRevenueModified { get; set; }
        public DateTime? ClinicianConfirmedOn { get; set; }
        public int? ClinicianConfirmedBy { get; set; }
        public bool? isDirtyCalculatedColumns { get; set; }
        public double? Calculated_BilledAmount { get; set; }
        public double? Calculated_ContractualAmount { get; set; }
        public double? Calculated_PaidAmount { get; set; }
        public double? Calculated_PayrolledAmount { get; set; }
        public double? Calculated_BillRate { get; set; }
        public double? Calculated_ContractualRate { get; set; }
        public double? Calculated_Payrate { get; set; }
        public bool? Calculated_BillModeHourly { get; set; }
        public bool? Calculated_BillModeUnitRate { get; set; }
        public bool? Calculated_ContractualModeHourly { get; set; }
        public bool? Calculated_ContractualModeUnitRate { get; set; }
        public bool? Calculated_PayModeHourly { get; set; }
        public double? Calculated_BaseRate { get; set; }
        public int? Calculated_BaseRateHours { get; set; }
        public bool? Calculated_BaseRateEnabled { get; set; }
        public DateTime? LastsentForCorrectionOn { get; set; }
        public int? LastsentForCorrectionBy { get; set; }
        public bool? OverrideMileageRate { get; set; }
        public double? MileageRate { get; set; }
        public int? SMS_SchedeuleEarlyCheckOutStatus { get; set; }
        public DateTime? SMS_SchedeuleEarlyCheckOutSentOn { get; set; }
        public DateTime? SMS_SchedeuleEarlyCheckOutStatusLastUpdatedTime { get; set; }
        public bool? eChartLanguageInterpreterUsed { get; set; }
        public string eChartLanguageInterpreter_Language { get; set; }
        public int? ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public DateTime? EVVOriginalCheckinTime { get; set; }
        public DateTime? EVVOriginalCheckoutTime { get; set; }
        public string CheckinEditReason { get; set; }
        public string CheckoutEditReason { get; set; }
        public bool? SupervisoryVisitPerformedInThisVisit { get; set; }
        public DateTime? InitialsentforCorrection { get; set; }
        public int? Notifyd_ScheduleAlertSentStatus { get; set; }
        public DateTime? Notifyd_ScheduleAlertSentOn { get; set; }
        public DateTime? Notifyd_ScheduleAlertSentStatusLastUpdatedTime { get; set; }
        public int? Notifyd_SchedeuleLateCheckinStatus { get; set; }
        public DateTime? Notifyd_SchedeuleLateCheckinSentOn { get; set; }
        public DateTime? Notifyd_SchedeuleLateCheckinStatusLastUpdatedTime { get; set; }
        public int? Notifyd_SchedeuleEarlyCheckOutStatus { get; set; }
        public DateTime? Notifyd_SchedeuleEarlyCheckOutSentOn { get; set; }
        public DateTime? Notifyd_SchedeuleEarlyCheckOutStatusLastUpdatedTime { get; set; }
        public int? CdsPlanYear { get; set; }
        public int? CdsPlanYearService { get; set; }
        public int? CDSClientBudgetLineItemID { get; set; }
        public string SplitPurpose { get; set; }
        public bool? IsEVVExported { get; set; }
        public bool? IsEVVDirty { get; set; }
        public string CDSPayRateEditComments { get; set; }
        public string CDSOverrideWeekHoursRulesComments { get; set; }
        public bool? IsCDSTaxBilledUnitMode { get; set; }
        public string CheckinEditCode { get; set; }
        public string CheckoutEditCode { get; set; }
        public string CheckinEditAction { get; set; }
        public string CheckoutEditAction { get; set; }
        public string CheckinEditActionCode { get; set; }
        public string CheckoutEditActionCode { get; set; }
        public bool? Calculated_PayModeHourly_Prev { get; set; }
        public double? Calculated_Payrate_Prev { get; set; }
        public string AssessmentNotes { get; set; }
        public double? Calculated_MileageRate_Prev { get; set; }
        public double? Calculated_MileageRate { get; set; }
        public string EvvOutsideCheckInLocationReason { get; set; }
        public string EvvOutsideCheckOutLocationReason { get; set; }
        public string EvvOutsideCheckInLocationReasonCode { get; set; }
        public string EvvOutsideCheckOutLocationReasonCode { get; set; }
        public string EvvOutsideCheckInLocationNotes { get; set; }
        public string EvvOutsideCheckOutLocationNotes { get; set; }
        public string EvvCheckInVoicePath { get; set; }
        public string EvvCheckOutVoicePath { get; set; }
        public string OverHoursNotes { get; set; }
        public int? CheckinSource_Online { get; set; }
        public int? CheckOutSource_Online { get; set; }
        public DateTime? OriginalCheckinTimeUTC { get; set; }
        public DateTime? OriginalCheckoutTimeUTC { get; set; }
        public string evv_contingency_plan { get; set; }
        public DateTime? LAST_CONFLICT_VALIDATED_ON { get; set; }
        public string fob_checkin_code { get; set; }
        public string fob_checkout_code { get; set; }
        public string fob_checkin_device_id { get; set; }
        public string fob_checkout_device_id { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class ClientEvvTerminalPayersEntity
    {
        public int client_evv_terminal_payer_id { get; set; }
        public int hha { get; set; }
        public DateTime created_on { get; set; }
        public int created_by { get; set; }
        public int client_id { get; set; }
        public int Primary_client_payer_id { get; set; }
        public char hierarchy { get; set; }
        public int Terminal_client_payer_id { get; set; }
        //public int terminal_aggregator_id { get; set; }
        public int terminal_configuration_ID { get; set; }
        public DateTime export_effective_from { get; set; }
        public bool is_deleted { get; set; }
        public DateTime deleted_on { get; set; }
        public int deleted_by { get; set; }

        public int PAYMENT_SOURCE_ID { get; set; }
        public int AdmitNo { get; set; }
        public string PATIENT_ID { get; set; }

        public string FIRST_NAME { get; set; }
        public string MIDDLE_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string VendorName { get; set; }
        public string VendorState { get; set; }
        public string VendorVersionCode { get; set; }
        public DateTime visits_last_registered_on { get; set; }
        public int? evv_configuration_id { get; set; }
    }

    public class EVVClientTerminalPayerListEntity : ClientEvvTerminalPayersEntity
    {
        public int PAYMENT_SOURCE_ID { get; set; }
        public int AdmitNo { get; set; }
        public string PATIENT_ID { get; set; }

        public string FIRST_NAME { get; set; }
        public string MIDDLE_NAME { get; set; }
        public string LAST_NAME { get; set; }
    }
}

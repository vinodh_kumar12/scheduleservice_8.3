﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class EpisodeEntity
    {
        public int EPISODEID { get; set; }
        public int POC { get; set; }
        public DateTime EpStartDate { get; set; }

        public DateTime EpEndDate { get; set; }
        


    }
}

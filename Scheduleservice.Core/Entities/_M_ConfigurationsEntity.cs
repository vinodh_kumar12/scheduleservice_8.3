﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _M_ConfigurationsEntity
    {
        public int schedule_conflict_revalidation_min_interval_mins { get; set; }
        public DateTime Evv_terminal_payer_visits_last_registered_on { get; set; }
        public int EvvDataBaseID { get; set; }
    }
}

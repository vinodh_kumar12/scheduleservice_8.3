﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
	public class CaregiversEntity
	{
		public int CAREGIVER_ID { get; set; }
		public int CAREGIVER_DISC_ID { get; set; }
		public string FIRST_NAME { get; set; }
		public string LAST_NAME { get; set; }
		public string CLINICIAN_ID { get; set; }
		public int DISCIPLINE { get; set; }
		public string DISCIPLINE_NAME { get; set; }
		public string STATUS { get; set; }
		public DateTime HIRED_DATE { get; set; }
		public DateTime TERMINATED_DATE { get; set; }
		public int? supervisingClinician { get; set; }
		public int PAYROLL_BRANCH_ID { get; set; }
		public bool isClinician { get; set; }
		public bool isOfficeStaff { get; set; }
		public bool isIntake { get; set; }
		public bool isDeleted { get; set; }
		public bool isRejected { get; set; }
		public bool isActive { get; set; }
		public string coSignRule { get; set; }
		public bool IsAssistant { get; set; }
		public bool IsEnableEVV { get; set; }
		public int BASE_DISCIPLINE_ID { get; set; }
		public bool isPCA { get; set; }
		public bool IsCaseManager { get; set; }
		public bool IS_QA { get; set; }
		public bool CosignClinician { get; set; }
		public bool IsScheduler { get; set; }
		public bool isSupervisor { get; set; }
		public bool enableTravelTime { get; set; }
		public bool enableDOcumentationTime { get; set; }
		public bool BlockFromFurtherScheduling { get; set; }
		public DateTime BlockSchedulingEffectiveDate { get; set; }
		public bool overrideOnlyRegisteredServicesForClinicianForSchedules { get; set; }
		public bool enableOnlyRegisteredServicesForClinicianForSchedules { get; set; }
        public int? EMPLOYER_VENDOR { get; set; }
		public Guid? Publish_UID { get; set; }
        public bool caregiveradditional_overrideClinicianMaxHoursPerWeek { get; set; }
        public float CaregiverDiscipline_ClinicianMaxHoursPerWeek { get; set; }
        public bool CaregiverDisciplines_ClinicianMaxHoursPerWeek_isHardStop { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class CaregiverTaskValidationErrorEntity
    {
		public int ValidationErrorID { get; set; }
		public int HHA { get; set; }
		public DateTime? CreatedOn { get; set; }
		public int? CreatedBy { get; set; }
		public int CgTaskID { get; set; }
		public string Event { get; set; }
		public string ErrorCode { get; set; }
		public string ErrorText { get; set; }
		public bool? isHardStopError { get; set; }
		public bool? isResolved { get; set; }
		public string Resolution { get; set; }
		public string ResolutionNotes { get; set; }
		public int? ResolvedBy { get; set; }
		public DateTime? ResolvedOn { get; set; }
		public int ErrorID { get; set; }
		public bool isConflictValidation { get; set; }
		public bool Can_resolve { get; set; }
	}
}

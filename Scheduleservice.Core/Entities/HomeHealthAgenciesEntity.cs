﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class HomeHealthAgenciesEntity
    {
        public int HHA_ID { get; set; }
        public Guid? Publish_UID { get; set; }
    }
}

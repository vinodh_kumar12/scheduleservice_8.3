﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class UAEvvVendorVersionMasterEntity
    {
        public int EvvVendorVersionMasterID { get; set; }
        public int EvvVendorMasterID { get; set; }
        public string DisplayName { get; set; }
    }
}

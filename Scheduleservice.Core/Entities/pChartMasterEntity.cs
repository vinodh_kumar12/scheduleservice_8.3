﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class pChartMasterEntity
    {
        public int pChartMasterID { get; set; }
        public int HHA { get; set; }
        public bool? pChartSubmitted { get; set; }
        public DateTime pChartSubmittedOn { get; set; }
        public int pChartSubmittedBy { get; set; }
        public bool? pChartApproved { get; set; }
        public int pChartApprovedBy { get; set; }
        public DateTime pChartApprovedOn { get; set; }
        public int CgTask_ID { get; set; }
        public int TimePoint { get; set; }
        public bool? IsDeleted { get; set; }
    }
}

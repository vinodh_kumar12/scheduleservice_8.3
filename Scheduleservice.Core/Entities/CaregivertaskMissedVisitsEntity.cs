﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
   public  class CaregivertaskMissedVisitsEntity
    {

        public int CaregiverTaskMissedVisitID { get; set; }

        public int? hha { get; set; }

        public int? cgTaskid { get; set; }

        public string MissedVisitEvvActionCode { get; set; }

        public string MissedVisitEvvReasonCode { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? CreatedBy { get; set; }

        public string MissedVisitEvvReason { get; set; }

        public string MissedVisitEvvAction { get; set; }
        public string ActionCodeNotes { get; set; }

    }
}

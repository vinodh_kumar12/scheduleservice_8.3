﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CDSBudgetLineItemMasterEntity
    {
        public string DataCapture { get; set; }
        public bool enableWeeklyHoursScheduleValidation { get; set; }
        public bool enableWeeklyAmountScheduleValidation { get; set; }
        public bool isPayHourly { get; set; }
        public int BudgetLineItemMasterID { get; set; }

        public string ItemName { get; set; }
    }
}

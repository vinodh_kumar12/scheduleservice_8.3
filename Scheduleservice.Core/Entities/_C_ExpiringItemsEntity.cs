﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class _C_ExpiringItemsEntity
    {
        public int EXPIRING_ITEM_ID { get; set; }
        public int HHA { get; set; }
        public string ITEM_NAME { get; set; }
        public string Notes { get; set; }
        public bool Active { get; set; }
        public byte Category { get; set; }
        public bool? isMandatory { get; set; }
        public bool? blockFromschedulingwhenExpires { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? blockFromeChartDocumentationWhenExpires { get; set; }
        public bool? AlertWhenExpireThroughOfficeComm { get; set; }
        public bool? enableSetExipryDate { get; set; }
        public string ExpiresOnDuration { get; set; }
        public int? ExpiresOnValue { get; set; }
        public bool? BlockFromTelephoneChartWhenExpires { get; set; }
        public string PayrollDeficiencyType { get; set; }
    }
}

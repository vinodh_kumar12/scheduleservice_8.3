﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Entities
{
    public class CaregivertaskBasicEntity
    {
       // public int CGTASK_ID { get; set; }
        public int? CAREGIVER { get; set; }
        public DateTime? PLANNED_DATE { get; set; }
        public DateTime? PLANNED_START_TIME { get; set; }
        public DateTime? PLANNED_END_TIME { get; set; }
        public DateTime? ACCTUAL_DATE { get; set; }
        public DateTime? ACCTUAL_START_TIME { get; set; }
        public DateTime? ACCTUAL_END_TIME { get; set; }
        public string STATUS { get; set; }
        public int? PLAN_ENTRY { get; set; }
        public bool? CONFIRMED { get; set; }
        public string EDITED_HOURS { get; set; }
        public float? MILES { get; set; }
        public int? SERVICECODE_ID { get; set; }
        public int? CLIENT_ID { get; set; }
        public int? AUTHORIZATION_ID { get; set; }
        public string EDITED_HOURS_PAYABLE { get; set; }
        public double? PAYRATE { get; set; }
        public bool? IS_PAYRATE_HOURLY { get; set; }
        public bool? IS_PAYABLE { get; set; }
        public bool? IS_BILLABLE { get; set; }
        public int? SCHEDULE_PROPERTIES { get; set; }
        public string ADDITIONAL_NOTE { get; set; }
        public int? MISSED_VISIT_FLAG { get; set; }
        public int? PAYMENT_SOURCE { get; set; }
        public bool? IS_BILLED { get; set; }
        public bool? IS_PAID { get; set; }
        public string MISSED_VISIT_REASON { get; set; }
        public DateTime? WEEK_START { get; set; }
        public float? OFFICE_MILES { get; set; }
        public int? POC { get; set; }
        public int? HHA { get; set; }
        public bool? IS_AUTH_MANDATORY { get; set; }
        public int? RequestId { get; set; }
        public byte? CheckInSource { get; set; }
        public byte? CheckOutSource { get; set; }
        public int? CREATED_BY { get; set; }
        public double? BillRate { get; set; }
        public bool? IS_BILLRATE_HOURLY { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? isPRNVisit { get; set; }
        public int? eChartMasterID { get; set; }
        public bool? CanBill { get; set; }
        public char? Chart_Type { get; set; }
        public bool? enablePayableHours { get; set; }
        public bool? CopayBilled { get; set; }
        public bool? isAcceptedFromFC { get; set; }
        public bool? isChartSubmitted { get; set; }
        public bool? isChartApproved { get; set; }
        public bool? isChartSendForCorrection { get; set; }
        public int? oasisDatasetID { get; set; }
        public int? pChartMasterID { get; set; }
        public int? InsertedIndex { get; set; }
        public float? AuthorizedHours { get; set; }
        public string CodingStatus { get; set; }
        public bool? isBilledByContractAgency { get; set; }
        public bool? isPaidToContractAgency { get; set; }
        public float? AuthGraceUnitsApplied { get; set; }
        public bool? AuthGraceUnitsResolved { get; set; }
        public float? TotalHours { get; set; }
        public bool? isScheduleCreatedFromKMobile { get; set; }
        public float? TotalBreakHours { get; set; }
        public string CreateMethod { get; set; }
        public bool? isAuthorized { get; set; }
        public bool? isChartLockedForEditing { get; set; }
        public int? eChartLastLockedSessionId { get; set; }
        public bool? isInterventionTabLockedForEditing { get; set; }
        public int? isInterventionTabLastLockedSessionId { get; set; }
        public int? CaregiverTaskMissedVisitID { get; set; }
        public bool? isOverridePayrate { get; set; }
        public bool? isOverridePayMode { get; set; }
        public bool? IsPartiallyBilled { get; set; }
        public int? EvvAggregatorStatus { get; set; }
        public DateTime? EvvStatusLastUpdatedTimeStamp { get; set; }
        public string EvvLastExportStatus { get; set; }
        public DateTime? EvvLastExportDate { get; set; }
    }
}

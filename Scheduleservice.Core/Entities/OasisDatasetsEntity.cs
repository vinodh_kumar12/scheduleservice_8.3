﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Entities
{
    public class OasisDatasetsEntity
    {
        public int DATASET_ID { get; set; }
        public int EPISODE { get; set; }
        public string TIME_POINT { get; set; }
        public string DESCRIPTION { get; set; }
        public int CG_TASK_ID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Scheduleservice.Core.Entities
{
    public class HHAEvvVendorVersionErrorCodeMasterEntity
    {
        public int EvvVendorVersionMasterID { get; set; }
        public string EvvVendorVersionErrorCodeMaster { get; set; }
        public bool ReasonCodeAvailable { get; set; }
        public bool CanGrowReasonCode { get; set; }
        public bool ActionCodeAvailable { get; set; }
        public bool CanGrowActionCode { get; set; }
        public bool ExceptionCodeAvailable { get; set; }
        public bool CanGrowExceptionCode { get; set; }
        public bool MissedVisitReasonCodeAvailable { get; set; }
        public bool CanGrowMissedVisitReasonCode { get; set; }
        public bool MissedVisitActionCodeAvailable { get; set; }
        public bool CanGrowMissedVisitActionCode { get; set; }
    }
}

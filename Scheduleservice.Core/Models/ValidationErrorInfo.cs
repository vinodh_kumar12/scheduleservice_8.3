﻿using Scheduleservice.Core.DTO.Schedule;
using System.Collections;
using System.Collections.Generic;

namespace Scheduleservice.Core.Models
{
    public class Scheduleerrorinfo
    {
        public int schedule_id { get; set; }
        public string schedule_uuid { get; set; }
        public List<ScheduleValidationErrorsDto> errors { get; set; } 
    }
     
    public class ValidationErrorInfo
    {
        public string validation_type { get; set; }
        public string validation_error_message { get; set; }
        public string validation_error_details_json { get; set; }
        public bool is_soft_warning { get; set; }
        public string dependent_cgtaskIds_json { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class Terminalpayerschedules
    {

        public int CaregiverTasksTerminalPayerID { get; set; }

        public int HHA { get; set; }
        public int ClientID { get; set; }
        public int CgtaskID { get; set; }
        public int PayerID_terminal { get; set; }
        public int ChildScheduleID { get; set; }

        public int PayerID { get; set; }

        public int TerminalPayerID { get; set; }

        public char Hierarchy { get; set; }

        public bool is_exported { get; set; }

        public DateTime ScheduleDate { get; set; }
        public int ServiceID { get; set; }

        public string Status { get; set; }
        public DateTime? Planned_EndTime { get; set; }
        public DateTime? Planned_StartTime { get; set; }
        public DateTime? CheckinTime { get; set; }
        public DateTime? CheckoutTime { get; set; }
        public string Actual_Hours { get; set; }
        public bool is_Billed { get; set; }
        public bool is_Paid { get; set; }

        public bool is_Evv_Schedule { get; set; }

        public TimeSpan? Planned_Hours { get; set; }

        public bool IsPayable { get; set; }
        public bool IsBillable { get; set; }
        public string failure_message { get; set; }
        public string failure_code { get; set; }
        public DateTime? last_exported_on { get; set; }
        public string last_export_status { get; set; }

    }
}

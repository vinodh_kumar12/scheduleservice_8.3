﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Models
{
    public class EVVScheduleExceptions
    {
    }

    public class UpdateCaregiverTaskEVVException
    { 
       public bool isResolved { get; set; }
       public DateTime ResolvedOn { get; set; }
       public int ResolvedBy { get; set; }
       public string Resolution { get; set; }
       public string Resolution_comments { get; set; }
       public int CaregiverTaskEvvReasonID { get; set; }
       public string SystemCode { get; set; }
       public bool isSystemAutoAdded { get; set; }
       public int CaregiverTaskChildID { get; set; }
       public int EvvVendorVersionMasterID { get; set; }
       public bool isKanTimeCode { get; set; }
       public string ExceptionSystemType { get; set; }
       public bool isDeleted { get; set; }
       public DateTime DeletedOn { get; set; }
       public int DeletedBy { get; set; }
       public string DeleteReason { get; set; }

    }
}

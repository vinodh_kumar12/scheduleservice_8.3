﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitCheckInOutSubscriberModel
    {
        public VisitSourceAppModel source_app { get; set; }
        public ScheduleInfoModel schedule_info { get; set; }
        public VisitInfoModel visit_info { get; set; }
        public VisitCaregiverModel provider_info { get; set; }
        public string payer_name { get; set; }
        public string service_name { get; set; }
        public VisitAuthorizationModel auth { get; set; }
        public VisitEvvModel evv { get; set; }

        public VisitCheckInOutSubscriberModel()
        {
            source_app = new VisitSourceAppModel();
            schedule_info = new ScheduleInfoModel();
            visit_info = new VisitInfoModel();
            provider_info = new VisitCaregiverModel();
            auth = new VisitAuthorizationModel();
            evv = new VisitEvvModel();
        }
    }
}

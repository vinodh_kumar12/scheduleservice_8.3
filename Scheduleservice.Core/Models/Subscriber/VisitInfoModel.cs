﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitInfoModel
    {
        public string visit_status { get; set; }
        public VisitCheckinModel checkin { get; set; }
        public VisitCheckoutModel checkout { get; set; }
        public int? hours_billable_in_mins { get; set; }
        public int? hours_payable_in_mins { get; set; }
        public bool is_billable { get; set; }
        public string place_of_service_code { get; set; }
        public float? miles { get; set; }
        public int? travel_time_hours_in_mins { get; set; }
        public int? doc_time_hours_in_mins { get; set; }

    }
}

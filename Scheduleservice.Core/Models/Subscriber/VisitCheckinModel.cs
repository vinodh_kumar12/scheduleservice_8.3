﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitCheckinModel
    {

        public DateTime? checkin_time { get; set; }
        public DateTime? evv_checkin_time { get; set; }
        public string checkin_source { get; set; }
        public GeoLocationModel checkin_geo_location { get; set; }
        public string checkin_geo_location_bounday_flag { get; set; }
        public int? checkin_distance_meters { get; set; }
        public AddressModel address { get; set; }
    }

}

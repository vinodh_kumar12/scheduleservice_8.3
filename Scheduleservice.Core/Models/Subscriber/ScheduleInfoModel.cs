﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class ScheduleInfoModel
    {
        public DateTime? planned_date { get; set; }
        public DateTime? planned_start_time { get; set; }
        public DateTime? planned_end_time { get; set; }
        public float? units { get; set; }
        public bool is_visit_confirmed { get; set; }
        public string shift_name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitCaregiverModel
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public VisitSecondaryProviderModel secondary_provider { get; set; }
    }
}

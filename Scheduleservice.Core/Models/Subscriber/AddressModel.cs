﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class AddressModel
    {
        public string address_type { get; set; }
        public GeoLocationModel address_geo_location { get; set; }
    }
}

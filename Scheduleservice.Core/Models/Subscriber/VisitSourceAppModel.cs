﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitSourceAppModel
    {
        public string source_application_code { get; set; }
        public string source_agency_uid { get; set; }
        public int? source_agency_id { get; set; }
        public string source_visit_uid { get; set; }
        public string source_caregiver_uid { get; set; }
        public string source_payer_uid { get; set; }
        public string source_service_uid { get; set; }
        public string reference_agency_id { get; set; }
        public string reference_visit_id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitEvvModel
    {
        public bool is_evv_schedule { get; set; }
        public bool is_evv_export_required { get; set; }
    }
}

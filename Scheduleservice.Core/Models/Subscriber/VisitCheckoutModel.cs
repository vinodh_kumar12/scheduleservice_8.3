﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class VisitCheckoutModel
    {
        public DateTime? checkout_time { get; set; }
        public DateTime? evv_checkout_time { get; set; }
        public string checkout_source { get; set; }
        public GeoLocationModel checkout_geo_location { get; set; }
        public string checkout_geo_location_bounday_flag { get; set; }
        public int? checkout_distance_meters { get; set; }
        public AddressModel address { get; set; }
    }
}

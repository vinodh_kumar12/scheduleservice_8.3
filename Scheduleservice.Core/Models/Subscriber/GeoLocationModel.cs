﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models.Subscriber
{
    public class GeoLocationModel
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }
}

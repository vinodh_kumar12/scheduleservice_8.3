﻿using Scheduleservice.Core.Enums;
using System;
 

namespace Scheduleservice.Core.Models
{
    public class EditScheduleProperties 
    {
       
        [FieldDescription("Planned Date")]
        public DateTime? PLANNED_DATE { get; set; }

        [FieldDescription("Planned start time")]        
        public DateTime? PLANNED_START_TIME { get; set; }

        [FieldDescription("Planned end time")]
        public DateTime? PLANNED_END_TIME { get; set; }
        public DateTime? ACCTUAL_DATE { get; set; }
        
        [FieldDescription("Actual start time")]
        public DateTime? ACCTUAL_START_TIME { get; set; }
        
        [FieldDescription("Actual end time")]
        public DateTime? ACCTUAL_END_TIME { get; set; }

        [FieldDescription("Status")]
        public string STATUS { get; set; }

        [FieldDescription("Schedule is Confirmed")]
        public bool? CONFIRMED { get; set; }
        public DateTime? ClinicianConfirmedOn { get; set; }
        public int? ClinicianConfirmedBy { get; set; }
        
        [FieldDescription("Edited hours")]
        public string EDITED_HOURS { get; set; }
        public int? CLIENT_ID { get; set; }
        [FieldDescription("Authorization")]
        public int? AUTHORIZATION_ID { get; set; }
        public bool? IS_PAYABLE { get; set; }

        [FieldDescription("Payable hours")]
        public string EDITED_HOURS_PAYABLE { get; set; }
        public int? POC { get; set; }
        public bool? IS_AUTH_MANDATORY { get; set; }

        public int? PAYMENT_SOURCE { get; set; }
        public int? SERVICECODE_ID { get; set; }
        public byte? CheckInSource { get; set; }
        public byte? CheckOutSource { get; set; }

        [FieldDescription("Marked as Billable", "Marked as non Billable" )]
        public bool? CanBill { get; set; }
        public bool? IS_BILLED { get; set; }
        public float? AuthGraceUnitsApplied { get; set; }
        public bool? AuthGraceUnitsResolved { get; set; }
        public float? TotalHours { get; set; }
        public float? TotalBreakHours { get; set; }
        public bool? isAuthorized { get; set; }

        [FieldDescription("Units")]
        public float? Units { get; set; }
        public bool? Has_Child_Schedules { get; set; }
        public bool? IsSplitForBilling { get; set; }
        public bool? IsSplitForPayroll { get; set; }
        
        [FieldDescription("Check-In Geo location")]
        public string CHECK_IN_LOCATION { get; set; }

        [FieldDescription("Check-Out Geo location")]
        public string CHECK_OUT_LOCATION { get; set; }
        public float? CHECK_IN_DISTANCE { get; set; }
        public float? CHECK_OUT_DISTANCE { get; set; }

        [FieldDescription("Marked as EVV", "Marked as non EVV")]
        public bool? isEvvschedule { get; set; }
        public char? EvvCheckinLocationVerified { get; set; }
        public char? EvvCheckoutLocationVerified { get; set; }
        public int? CheckinTreatmentLocation { get; set; }
        public int? CheckoutTreatmentLocation { get; set; }
        [FieldDescription("is Attested")]
        public bool? isAttested { get; set; }
        public int? AttestedBy { get; set; }
        public DateTime? AttestedOn { get; set; }
        public bool? isEvvAggregatorExportRequired { get; set; }
        public bool? isVisitVerifiedWithEvvAggregator { get; set; }
        public bool? IsEVVDirty { get; set; }
        public DateTime? OriginalCheckinTimeUTC { get; set; }
        public DateTime? OriginalCheckoutTimeUTC { get; set; }
        public DateTime? EVVOriginalCheckinTime { get; set; }
        public DateTime? EVVOriginalCheckoutTime { get; set; }

        [FieldDescription("FOB Check-In code")]
        public string fob_checkin_code { get; set; }

        [FieldDescription("FOB Check-Out code")]
        public string fob_checkout_code { get; set; }
        [FieldDescription("FOB Check-Out DeviceID")]
        public string fob_checkout_device_id { get; set; }
        [FieldDescription("FOB Check-In DeviceID")]
        public string fob_checkin_device_id { get; set; }
        public int CGTASK_ID { get; set; }
        public int? CAREGIVER { get; set; }
        public int? PLAN_ENTRY { get; set; }
        public int? ShiftID { get; set; }
        public int? CoSignStaff { get; set; }
        public int? documentationTimeMins { get; set; }
        public int? CdsPlanYear { get; set; }
        public int? CdsPlanYearService { get; set; }
        public int? CDSClientBudgetLineItemID { get; set; }
        public float? MILES { get; set; }
        public float? AuthorizedHours { get; set; }
        public float? travelTimeHours { get; set; }
        public double? PAYRATE { get; set; }
        public double? BillRate { get; set; }
        public double? BonusAmount { get; set; }
        public bool? IS_PAYRATE_HOURLY { get; set; }
        public bool? IS_BILLRATE_HOURLY { get; set; }
        public bool? enablePayableHours { get; set; }
        public bool? isOverridePayrate { get; set; }
        public bool? isOverridePayMode { get; set; }
        public bool? ApprovedForPayroll { get; set; }
        public bool? ApprovedForBonusPayRate { get; set; }
        public bool? RequireCoSign { get; set; }
        public string? ADDITIONAL_NOTE { get; set; }
        public string? NotesToClinician { get; set; }
        public char? Chart_Type { get; set; }

    }

    internal class FieldDescriptionAttribute : Attribute
    {
        public FieldDescriptionAttribute(string v1, string v2 = "")
        {
            V1 = v1;
            V2 = v2;
        }

        public string V1 { get; }
        public string V2 { get; }
    }

}

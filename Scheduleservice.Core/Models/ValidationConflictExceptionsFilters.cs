﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Models
{
    public class ValidationConflictExceptionsFilters
    {
        public string StartDate { get;set; }
        public string EndDate { get;set; }
        public string ddl_payer { get;set; }
        public int? ddl_clinician { get;set; }
        public string ddl_schedulestatus { get;set; }
        public bool? ddl_billed { get;set; }
        public bool? ddl_Payrolled { get;set; }
        public bool? ddl_Billable { get;set; }
        public bool? ddl_Payable { get;set; }
        public bool? ddl_evvschedule { get;set; }
        public int? ddl_casemanager { get;set; }
        public int? ddl_client { get;set; }
        public string ddl_LOB { get;set; }
        public string ddl_Team { get;set; }
        public string ddl_discipline { get;set; }
        public bool? isResolved { get; set; }
        public string Exceptions { get; set; }
        public int Sort_By { get; set; }
        public bool Sort_Order { get; set; }
        public int PageNo { get; set; }
        public string ddl_location { get; set; }
        public int? ddl_Clientcoordinator1 { get; set; }
        public int? ddl_Clientcoordinator2 { get; set; }
        public int? ddl_Clientcoordinator3 { get; set; }
        public string ddl_evvvendor { get; set; }
        public int? ddl_scheduler { get; set; }
        public int? ddl_Supervisor { get; set; }
        public string ddl_echartstatus { get; set; }
        public bool? ddl_evvexportrequires { get; set; }


    }
}

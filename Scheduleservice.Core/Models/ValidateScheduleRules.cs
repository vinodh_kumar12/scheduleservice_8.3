﻿using Scheduleservice.Core.DTO.Schedule;
using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class ValidateScheduleRules
    {
        public CaregivertasksEntity caregiverTaskEntity { get; set; }
        public PaymentsourcesEntity paymentsourcesEntity { get; set; }
        public ClientsEntity clientsEntity { get; set; }
        public HHAScheduleConflictExceptionsEntity hHAScheduleConflictExceptionsEntity { get; set; }
        public ClientPaymentSourcesEntity clientPaymentSourcesEntity { get; set; }
        public ServiceCodesEntity serviceCodesEntity { get; set; }
        
        public LineofbusinessEntity lineofbusinessEntity { get; set; }
        public IEnumerable<HHAScheduleAuthSettingsEntity> hHAScheduleAuthSettingsEntities { get; set; }
        public HHABranchScheduleConfigurationsEntity hHABranchScheduleConfigurationsEntity { get; set; }
        public IEnumerable<CaregivertasksEntity> clientschedluesdetails { get; set; }
        public IEnumerable<CaregiversEntity> caregivers { get; set; }
        public IEnumerable<ServiceCodesEntity> servicecodes { get; set; }
        public IEnumerable<CaregiverOtherDisciplinesEntity> CaregiverOtherDisciplines { get; set; }
        public IEnumerable<CaregiverDisciplinesEntity> caregiverDisciplines { get; set; }
        public IEnumerable<CaregivertasksEntity> caregiverschedluesdetails { get; set; }
      
        public CDSPlanYearServicesEntity cDSplanyearservices { get; set; }
        public CDSClientBudgetLineItemEntity cDSClientBudgetLineItem { get; set; }

        public IEnumerable<ScheduleValidationRulesDto> scheduleValidationRulesDtos { get; set; }
        public IEnumerable<CaregiverTaskValidationErrorsDependentSchedulesEntity> validationErrorsDependentSchedules { get; set; }

        public bool AllowToCheckoutifHardStopValidationExists { get; set; } = false;

        public bool confirm_softwarning { get; set; } = true;
        public bool RequireGeoLocationforFOB { get; set; } = false;
    }

   
}

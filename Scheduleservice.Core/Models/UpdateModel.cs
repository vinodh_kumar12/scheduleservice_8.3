﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Schedules.Models
{
    public class UpdateModel
    {
        public string columns { get; set; }
        public string audit { get; set; }

    }
}

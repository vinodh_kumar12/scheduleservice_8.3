﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Models
{
    public class ScheduleValidationResult
    {
        public bool isValid { get; set; }
        public List<string> messages = new List<string>();
    }
}

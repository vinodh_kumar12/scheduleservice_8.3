﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class UpdateMissedVisits
    {

    }
    public class MissedVisitReasonCodeinfo
    {
        public string ReasonCode { get; set; }
        public string ReasonCodeDescription { get; set; }
        public string ActionCode { get; set; }
        public string ActionCodeDescription { get; set; }
        public string Notes { get; set; }
        public bool isDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int? DeletedBy { get; set; } 
        public string MasterPayerCodes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class EvvTerminalPayerSchedules
    {
        public int cgtask_id { get; set; }
        public int child_schedule_id { get; set; }
        public string kantime_unique_id { get; set; }
        public int client_id { get; set; }
        public int client_payer_id { get; set; }
        public char hierarchy { get; set; }
        public int client_terminal_payer_id { get; set; }
        public int terminal_payer_aggregator_id { get; set; }
        public DateTime schedule_date { get; set; }
        public bool is_deleted { get; set; }
        public int caregiver_task_terminal_payer_id { get; set; }
        public int caregiver_child_task_terminal_payer_id { get; set; }
        public bool can_delete { get; set; }
        public bool can_activate { get; set; }
        public int? evv_configuration_id { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.Models
{
    public class ScheduleFilter
    {
        public int? CLIENT_ID { get; set; }
        public string status { get; set; }
        public bool? isScheduleEVV { get; set; }
    }
    //Query search end points
    public class ScheduleSearchFilter
    {
       public int Client_ID { get; set; }
        public string LocationIDs { get; set; }
        public string startDate { get; set; }
        public string EndDate { get; set; }   
        public int isEVVExportDirty { get; set; }
        public bool isBillable { get; set; }
        public bool isInvoiced { get; set; }
        public bool isPayrolled { get; set; }
        public string ScheduleStatus { get; set; }
        public bool ExportEvvenbled { get; set; }
        public string PayerIDs { get; set; }
        public int EvvVendorVersionMasterID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class ValidationErrorDetail
    {
        public string error_code { get; set; }
        public string error_message { get; set; }
    }
}

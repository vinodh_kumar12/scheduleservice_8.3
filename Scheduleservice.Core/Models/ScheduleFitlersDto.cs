﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.Models
{
    public class ScheduleFitlersDto
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public bool? is_evv_export_dirty { get; set; }
        public int? is_billable { get; set; }
        public bool? IS_BILLED { get; set; }
        public bool? is_payrolled { get; set; }
        public bool? is_evv_aggregator_export_required { get; set; }
        public string payer_ids { get; set; }
        public bool? IsSplitForBilling { get; set; }
        public bool? is_evv_schedule { get; set; }
        public string hha_branch_ids { get; set; }
        public string context { get; set; }
        public int?[] client_id_list { get; set; }
        public int? aggregator_id { get; set; }
        public string visit_status { get; set; }
        public bool? evv_dirty_flag { get; set; }
    }
}

﻿using GeoCoordinatePortable;
using Newtonsoft.Json;
using Scheduleservice.Core.Common.Extensions;
using Scheduleservice.Core.DTO.EVV;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Models;
using Scheduleservice.Core.RepositoryQueryBuilder;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Scheduleservice.Core.Schedules
{
    public static class Common
    {
        public static string GetVisitEditedHours(DateTime Checkin, DateTime Checkout, int PayerRoundOffDirection, int PayerRoundOffMin)
        {

            string edited_hours = "";

            if (Checkin >= Checkout)
                Checkout = Checkout.AddDays(1);

            TimeSpan interval = Checkout - Checkin;
            edited_hours = interval.ToString(@"hh\:mm"); 
            
            if(edited_hours =="00:00" && Checkout>=Checkin && interval.TotalMinutes==1440)
            {
                edited_hours = "24:00";
            }

            //Apply the round rule based on the payer

            return edited_hours;
        }

        public static float GetVisitActualHours(DateTime Checkin, DateTime Checkout)
        {
            float actual_hours = 0;
            //get time difference between checkin and checkout and convert to hours
            decimal dec = Convert.ToDecimal(Checkout.Subtract(Checkin).TotalHours);
            actual_hours = (float)dec;
            return actual_hours;
        }

        public static float GetVisitUnits(int TotalMinutes, int ROUNDOFFUnits_Direction, int ROUNDOFFUnits_Minutes,
                bool isUnitsFixed, double FixedUnits, int MinutesPerUnit, double NumberOfUnits, bool Round_ZeroUnitScheduleToOne,
                int SplitRevenueCodeHoursInMins,bool RevSplit_isUnitsFixed, double RevSplit_FixedUnits, int RevSplit_RoundDirection,
                int RevSplit_Round_OffMinutes, int RevSplit_MinutesPerUnit, double RevSplit_NumberofUnits,
                bool ServicemaxunitEnabled, double Servicemaxunits, bool RevSecondSplit_OverrideUnits,
                bool RevSplit_isUnitsFixed_secondsplit, double RevSplit_FixedUnits_secondsplit, int RevSplit_MinutesPerUnit_secondsplit,
                double RevSplit_NumberofUnits_secondsplit, int RevSecondSplit_RoundDirection, int RevSecondSplit_Round_OffMinutes)
        {
            float units = 0;
            try
            {
                int MinutesTobeUsed = 0;
                float RevenueUnits = 0;
                int RevSplitMinutes = 0;

                if (SplitRevenueCodeHoursInMins > 0)
                {
                    if (TotalMinutes > SplitRevenueCodeHoursInMins && TotalMinutes > 0)
                    {
                        RevSplitMinutes = SplitRevenueCodeHoursInMins;
                        TotalMinutes = TotalMinutes - RevSplitMinutes;
                    }
                    else
                    {
                        isUnitsFixed = RevSplit_isUnitsFixed;
                        FixedUnits = RevSplit_FixedUnits;
                        MinutesPerUnit = RevSplit_MinutesPerUnit;
                        NumberOfUnits = RevSplit_NumberofUnits;
                    }
                }

                if (RevSecondSplit_OverrideUnits == true && TotalMinutes > SplitRevenueCodeHoursInMins && TotalMinutes > 0)
                {
                    isUnitsFixed = RevSplit_isUnitsFixed_secondsplit;
                    FixedUnits = RevSplit_FixedUnits_secondsplit;
                    MinutesPerUnit = RevSplit_MinutesPerUnit_secondsplit;
                    NumberOfUnits = RevSplit_NumberofUnits_secondsplit;
                    ROUNDOFFUnits_Direction = RevSecondSplit_RoundDirection;
                    ROUNDOFFUnits_Minutes = RevSecondSplit_Round_OffMinutes;
                }


                //if units is fixed , then return the fixed units
                if (isUnitsFixed)
                    units = (float)FixedUnits;
                else
                {
                    //if units is not fixed, check if roundoffdirection value and caluculate
                    if (ROUNDOFFUnits_Minutes == 0)
                        ROUNDOFFUnits_Minutes = 1;

                    //if dual then calucualte whether upper rounding or lower rounding
                    if (ROUNDOFFUnits_Direction == 3)
                    {
                        if (TotalMinutes % ROUNDOFFUnits_Minutes >= Convert.ToDouble(ROUNDOFFUnits_Minutes) / 2)
                            ROUNDOFFUnits_Direction = 1;
                        else
                            ROUNDOFFUnits_Direction = 2;
                    }

                    if (ROUNDOFFUnits_Direction == 1)
                        MinutesTobeUsed = (Convert.ToInt32(TotalMinutes / ROUNDOFFUnits_Minutes) * ROUNDOFFUnits_Minutes) + (TotalMinutes % ROUNDOFFUnits_Minutes == 0 ? 0 : ROUNDOFFUnits_Minutes);
                    else
                        MinutesTobeUsed = Convert.ToInt32(TotalMinutes / ROUNDOFFUnits_Minutes) * ROUNDOFFUnits_Minutes;

                    if (MinutesTobeUsed == 0 && ROUNDOFFUnits_Minutes > 0)
                    {
                        if (ROUNDOFFUnits_Minutes < TotalMinutes)
                            MinutesTobeUsed = ROUNDOFFUnits_Minutes;
                    }

                    if(MinutesPerUnit!=0)
                        units = (float)Math.Round((Convert.ToSingle(MinutesTobeUsed) / MinutesPerUnit) * NumberOfUnits, 2);
                }

                //if units is 0 and payer level setting to Zero Unit Schedule To One
                if (units == 0 && Round_ZeroUnitScheduleToOne)
                    units = 1;


                if (SplitRevenueCodeHoursInMins > 0)
                {
                    //if rev split split units is fixed then retuen fixed units
                    if (RevSplit_isUnitsFixed)
                        RevenueUnits = (float)Math.Round(RevSplit_FixedUnits, 2);
                    else
                    {
                        if (RevSplit_RoundDirection == 0)
                            RevSplit_Round_OffMinutes = RevSplitMinutes;

                        if (RevSplit_Round_OffMinutes == 0)
                            RevSplit_Round_OffMinutes = 1;

                        if (RevSplit_MinutesPerUnit == 0)
                            RevSplit_MinutesPerUnit = 1;

                        //if dual then calucualte whether upper rounding or lower rounding
                        if (RevSplit_RoundDirection == 3)
                        {
                            if (RevSplitMinutes % RevSplit_Round_OffMinutes >= Convert.ToDouble(RevSplit_Round_OffMinutes) / 2)
                                RevSplit_RoundDirection = 1;
                            else
                                RevSplit_RoundDirection = 2;
                        }

                        if (MinutesTobeUsed == 0 && RevSplit_Round_OffMinutes > 0)
                        {
                            if (RevSplit_Round_OffMinutes < RevSplitMinutes)
                                MinutesTobeUsed = RevSplit_Round_OffMinutes;
                        }

                        RevenueUnits = (float)Math.Round((Convert.ToDouble(MinutesTobeUsed) / RevSplit_MinutesPerUnit) * RevSplit_NumberofUnits, 2);
                    }

                    if (RevenueUnits == 0 && Round_ZeroUnitScheduleToOne)
                        RevenueUnits = 1;

                    units += RevenueUnits;
                }

                if (ServicemaxunitEnabled)
                {
                    if (units > Servicemaxunits)
                        units = (float)Servicemaxunits;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return units;
        }

        public static float GetDistanceFromSource(GeoCoordinate Source, GeoCoordinate Destination)
        {
            //Source => Client Location -- lat, long
            //Destination => Caregiver Location -- lat, long            

            float distance = (float)Source.GetDistanceTo(Destination);

            //distance is in meters
            return distance;
        }

        public static char GetLocationVerifiedStatus(float distancefromSource, float maxfence)
        {
            char verifiedStatus = 'N';

            if (maxfence > 0)
            {
                //if distance is greater than maxfence then return O
                if (distancefromSource > maxfence)
                    verifiedStatus = 'O';
                else if (distancefromSource <= maxfence)//if distance is less than or equal to maxfence then return Y
                    verifiedStatus = 'Y';
            }
            return verifiedStatus;
        }

        public static DateTime GetCurrentPSTTime()
        {
            //get utc and convert to pst
            var utctimenow = GetCurrentUTCTime();

            return utctimenow.ToConvertUTCTimeZone("PST");
        }

        public static DateTime GetCurrentUTCTime()
        {
            return DateTime.UtcNow;
        }

        public static DateTime GetWeekStartdate(DateTime date, DayOfWeek WeekStartDay)
        {
            int diff = (7 + (date.DayOfWeek - WeekStartDay)) % 7;
            return date.AddDays(-1 * diff).Date;
        }

        public static int ConvertStringToMins(this string Editedhours)
        {
            int totalminutes = 0;
            try
            {
                if (Editedhours.Contains(":"))
                {
                    var timediff = Editedhours.Split(":");
                    int hours = Convert.ToInt32(Convert.ToInt32(timediff[0]) * 60);
                    int mins = Convert.ToInt32(timediff[1]);
                    totalminutes = hours + mins;
                }
                else
                {
                    totalminutes = Convert.ToInt32(Convert.ToInt32(Editedhours) * 60);
                }
            }
            catch (Exception)
            {

            }
            return totalminutes;
        }
        public static string ConvertMinsToString(this float Mins)
        {
            TimeSpan ts = TimeSpan.FromMinutes(Mins);
            return ts.ToString(@"hh\:mm");
        }

        public static List<EVVExceptioninfoDto> GetEVVExceptionEvents(CaregivertasksEntity oldScheduleDetails, ScheduleEVVInfo NewScheduleDetails,
           HHAScheduleAuthSettingsEntity hHAConfigDetails, int EVVVendorVersionMasterID, bool IsManualEdit = false, bool RaiseExceptionWhenboth_CS_VR_areMissed = true,
           int EvvCheckinReasonID = 0, int EvvCheckoutReasonID = 0, bool IsManualCheckin = false,
           bool IsManualCheckout = false)
        {
            List<EVVExceptioninfoDto> eventlist = new List<EVVExceptioninfoDto>();

            /*
             * IsManualCheckin -- pass from clincian interfaces if checkin is done manually
             * IsManualCheckOut -- pass from clincian interfaces if checkout is done manually.
             * IsManualEdit -- Pass from non clinician interfaces.
              
            Events
               * OUTSIDEFENCE 
               * NO_LOCATION
               * LATE_CHECKIN 
               * SIGNATUREEXCEPTION 
               * MANUAL_ENTRY 
               * TIME_EDITED 		 
               * SHORT_VISIT		
               * LONG_VISIT

               * UNMATCHEDPHONE 
               * UNSCHEDULED_VISIT 
               * VISIT_NO_SHOW 
               * MISSED_CHECKOUT

               Y = Verified
               O = OutsideFence
               N = Not Verified 
           */

            try
            {
                //Checkout
                if (NewScheduleDetails.ACCTUAL_END_TIME != null)
                {
                    if (oldScheduleDetails.ACCTUAL_END_TIME == null)
                    {
                        //----------------------------OUTSIDEFENCE_CHECKOUT------------------------------------
                        if (!IsManualEdit && NewScheduleDetails.HasCheckOutgeoLocation && NewScheduleDetails.EVVCheckOutLocationVerified != 'Y')
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "OUTSIDEFENCE_CHECKOUT", Context = "CheckOut", ResolvedReasoncodeID = EvvCheckoutReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //----------------------------NO_LOCATION_CHECKOUT------------------------------------
                        if (!IsManualEdit && !NewScheduleDetails.HasCheckOutgeoLocation)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "NO_LOCATION_CHECKOUT", Context = "CheckOut", ResolvedReasoncodeID = EvvCheckoutReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //-----------------------MANUAL_ENTRY_CHECKOUT--------------------------
                        if (IsManualEdit || IsManualCheckout)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "MANUAL_ENTRY_CHECKOUT", Context = "CheckOut", ResolvedReasoncodeID = EvvCheckoutReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //-----------------------SIGNATUREEXCEPTION--------------------------
                        if (RaiseExceptionWhenboth_CS_VR_areMissed)
                        {
                            if (!NewScheduleDetails.HasClientSign && !NewScheduleDetails.HasClientVoiceRecording)
                            {
                                eventlist.Add(new EVVExceptioninfoDto { Event = "SIGNATUREEXCEPTION", Context = "Visit", EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                            }
                        }

                        TimeSpan PlannedMin = oldScheduleDetails.PLANNED_END_TIME - oldScheduleDetails.PLANNED_START_TIME;
                        TimeSpan ActualMin = Convert.ToDateTime(NewScheduleDetails.ACCTUAL_END_TIME) - Convert.ToDateTime(NewScheduleDetails.ACCTUAL_START_TIME);

                        //-----------------------SHORT_VISIT--------------------------
                        if (PlannedMin.TotalMinutes - hHAConfigDetails.AutoGenerateShortVisitExceptionMin > ActualMin.TotalMinutes)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "SHORT_VISIT", Context = "Visit", EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //-----------------------LONG_VISIT--------------------------
                        if (PlannedMin.TotalMinutes + hHAConfigDetails.AutoGenerateLongVisitExceptionMin < ActualMin.TotalMinutes)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "LONG_VISIT", Context = "Visit", EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }
                    }
                    else
                    {
                        //-------------------------TIME_EDITED_CHECKOUT----------------------------------------
                        if (Convert.ToDateTime(NewScheduleDetails.ACCTUAL_END_TIME).ToString("hh:mm") != Convert.ToDateTime(oldScheduleDetails.ACCTUAL_END_TIME).ToString("hh:mm"))
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "TIME_EDITED_CHECKOUT", Context = "CheckOut", ResolvedReasoncodeID = EvvCheckoutReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }
                    }
                }

                //Checkin
                if (NewScheduleDetails.ACCTUAL_START_TIME != null)
                {
                    if (oldScheduleDetails.ACCTUAL_START_TIME == null)
                    {
                        //----------------------------OUTSIDEFENCE_CHECKIN------------------------------------
                        if (!IsManualEdit && NewScheduleDetails.EvvCheckinLocationVerified != 'Y' && NewScheduleDetails.HasCheckInGeoLocation)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "OUTSIDEFENCE_CHECKIN", Context = "CheckIn", ResolvedReasoncodeID = EvvCheckinReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //----------------------------NO_LOCATION_CHECKIN------------------------------------
                        if (!IsManualEdit && NewScheduleDetails.HasCheckInGeoLocation == false)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "NO_LOCATION_CHECKIN", Context = "CheckIn", ResolvedReasoncodeID = EvvCheckinReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //---------------------------LATE_CHECKIN-------------------------------------
                        if (oldScheduleDetails.PLANNED_START_TIME.AddMinutes(hHAConfigDetails.AutoGenerateLateCheckinExceptionMin) < NewScheduleDetails.ACCTUAL_START_TIME)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "LATE_CHECKIN", Context = "CheckIn", EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }

                        //-----------------------MANUAL_ENTRY_CHECKIN--------------------------
                        if (IsManualEdit || IsManualCheckin)
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "MANUAL_ENTRY_CHECKIN", Context = "CheckIn", ResolvedReasoncodeID = EvvCheckinReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }
                    }
                    else
                    {
                        //-------------------------TIME_EDITED_CHECKIN----------------------------------------
                        if (Convert.ToDateTime(NewScheduleDetails.ACCTUAL_START_TIME).ToString("hh:mm") != Convert.ToDateTime(oldScheduleDetails.ACCTUAL_START_TIME).ToString("hh:mm"))
                        {
                            eventlist.Add(new EVVExceptioninfoDto { Event = "TIME_EDITED_CHECKIN", Context = "CheckIn", ResolvedReasoncodeID = EvvCheckinReasonID, EVVVendorVersionMasterID = EVVVendorVersionMasterID });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return eventlist;
        }

        public static float GetUnitsForCDS(int TotalMinutes,byte? RoundDirection,int? Round_OffMinutes,int MinutesPerUnit,double NumberOfUnits,bool? RoundZeroUnitScheduleToOne)
        {
            float RetUnits;
            int? MinutesTobeUsed;
            if(Round_OffMinutes == 0)

            {
                RoundDirection = 1;
            }
            RoundDirection = 0;


            Round_OffMinutes = TotalMinutes;

            if (Round_OffMinutes == 0)
                Round_OffMinutes = 1;


            if (MinutesPerUnit == 0)
                 MinutesPerUnit = 1;

            if(RoundDirection==3)
            {
                if (TotalMinutes % Round_OffMinutes >= Convert.ToDouble(Round_OffMinutes) / Convert.ToDouble (2)) 
                          RoundDirection = 1;
                else
                    RoundDirection = 2;
            }
            if(RoundDirection==1)
            { 
                if(@TotalMinutes % Round_OffMinutes==0)
                {
                    MinutesTobeUsed= (TotalMinutes/ Round_OffMinutes) *Round_OffMinutes;
                }
                 
                else
                {
                    MinutesTobeUsed = ((TotalMinutes / Round_OffMinutes) * Round_OffMinutes)+Round_OffMinutes;

                }
            }
            else if(RoundDirection==2)
            {
                MinutesTobeUsed = ((TotalMinutes / Round_OffMinutes) * Round_OffMinutes) + Round_OffMinutes;

            }
            else
                MinutesTobeUsed = ((TotalMinutes / Round_OffMinutes) * Round_OffMinutes) + Round_OffMinutes;

            if(MinutesTobeUsed==0 && Round_OffMinutes>0)
            {
                MinutesTobeUsed = Round_OffMinutes;
            }

            RetUnits = Convert.ToSingle((MinutesTobeUsed / MinutesPerUnit) * NumberOfUnits);

            if(RetUnits==0 && RoundZeroUnitScheduleToOne==true)
            {
                RetUnits = 1;
            }
            return RetUnits;


        }

        //added by anish
        //desc:-to calculate schedule overage hours(ActualHours>PlannedHours+Graceperiod)
        //function should be called when schedule is in completed status and canbill =1
        public static bool ScheduleOverageHoursAsync(HHAScheduleAuthSettingsEntity hHAScheduleAuthSettingsRepository,EditScheduleProperties editSchedulePropertiesmodel,PaymentsourcesEntity paymentsourcedetails, CaregivertasksEntity oldcaregivertasksEntity, int caregiverdiscipline_id)
        {
            bool isoveragerequired = false;
   
            DateTime PlannedStartTime = Convert.ToDateTime(oldcaregivertasksEntity.PLANNED_START_TIME);
            DateTime PlannedEndtime = Convert.ToDateTime(oldcaregivertasksEntity.PLANNED_END_TIME);
           
            DateTime ActualStartTime = Convert.ToDateTime(editSchedulePropertiesmodel.ACCTUAL_START_TIME);
            DateTime ActualEndTime = Convert.ToDateTime(editSchedulePropertiesmodel.ACCTUAL_END_TIME);

            TimeSpan planned_editedhours = PlannedEndtime.Subtract(PlannedStartTime);
            TimeSpan Acctual_editedhours = ActualEndTime.Subtract(ActualStartTime);
            int EarlyCheckinMin = 0;
            int LateCheckoutMin = 0;

            if (hHAScheduleAuthSettingsRepository.EnableAutoSplitWhenActualHrsExceedPlannedHrs)
            {

                bool isevvschedule = oldcaregivertasksEntity.isEvvschedule;
                  
                 if (Convert.ToInt32(Acctual_editedhours.TotalMinutes) > Convert.ToInt32(planned_editedhours.TotalMinutes) + hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes)
                 {
                    if (isevvschedule)
                    {
                        if (hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsForKantimePrimaryEVV)
                        {
                            if (paymentsourcedetails.PrimaryEvvVendorID == "PRM_KANTIME")
                                isoveragerequired = true;
                        }
                        if (hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsForOtherPrimaryEVV && !isoveragerequired)
                        {
                            if (paymentsourcedetails.PrimaryEvvVendorID != "PRM_KANTIME" && !string.IsNullOrEmpty(paymentsourcedetails.PrimaryEvvVendorID))
                                isoveragerequired = true;
                        }
                    }
                    else
                    {
                        isoveragerequired = hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsForNonEVV;
                        
                    }
                     
                 
                 
                     if(!string.IsNullOrEmpty(hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsPayers) && isoveragerequired ==true)
                     {
                         int schedule_payerid = 0;
                         List<int> overhours_payersourcelist = hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsPayers.Split(",").Where(x => x != "").Select(y => Convert.ToInt32(y)).ToList();
                         
                             schedule_payerid = editSchedulePropertiesmodel.PAYMENT_SOURCE??0;
                        
                        if (!overhours_payersourcelist.Where(s => s == schedule_payerid).Any())
                            isoveragerequired = false ;
                         
                 
                     }
                 
                     if(hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsDiscplines != "" && isoveragerequired ==true)
                     {
                         List<int> overhours_disciplineids = hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsDiscplines.Split(",").Where(x => x != "").Select(y => Convert.ToInt32(y)).ToList();
                         if (caregiverdiscipline_id != 0)
                         {
                             
                             if (!overhours_disciplineids.Where(s => s == caregiverdiscipline_id).Any())
                                 isoveragerequired = false;
                         }
                     }

                    if (isoveragerequired && hHAScheduleAuthSettingsRepository.EnableAutoSplitforAdditionalHoursAtStartTimeandEndTime)
                    {

                        if (ActualStartTime < PlannedStartTime && ActualEndTime >= PlannedEndtime) //Early Checkin
                        {
                            TimeSpan EarlycheckinTimeDiff = PlannedStartTime - ActualStartTime;
                            EarlyCheckinMin = EarlycheckinTimeDiff.Minutes;

                            TimeSpan LateCheckoutTimeDiff = ActualEndTime - PlannedEndtime;
                            LateCheckoutMin = LateCheckoutTimeDiff.Minutes;
                            isoveragerequired = false;
                            if (EarlyCheckinMin > 0 || LateCheckoutMin > 0)
                            {
                                if (EarlyCheckinMin > hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes || LateCheckoutMin > hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes)
                                    isoveragerequired = true;
                            }
                        }
                    }
                    else if (isoveragerequired && hHAScheduleAuthSettingsRepository.EnableAutoSplitforAdditionalHoursAtOnlyEndTime)
                    {
                        if (Convert.ToInt32(Acctual_editedhours.TotalMinutes) > Convert.ToInt32(planned_editedhours.TotalMinutes) + hHAScheduleAuthSettingsRepository.AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes)
                            isoveragerequired = true;
                        else
                            isoveragerequired = false;
                    }
                    else
                        isoveragerequired = false;


                }
            }
            

            return isoveragerequired;
        }

        public static string GetScheduleFilters(ScheduleFitlersDto scheduleFitlersDto)
        {
            string constructed_query = "";
            RepositoryFilterConditions _repositoryFilterConditions = new RepositoryFilterConditions();

            if (!string.IsNullOrEmpty(scheduleFitlersDto.start_date))
            {
                if (!string.IsNullOrEmpty(scheduleFitlersDto.end_date))
                    constructed_query += _repositoryFilterConditions.BetweenFilter("PLANNED_DATE", scheduleFitlersDto.start_date, scheduleFitlersDto.end_date, FilterConditionDataTypeEnums.datetimeType).Build();
                else
                    constructed_query += _repositoryFilterConditions.GreaterThanEqualFilter("PLANNED_DATE", scheduleFitlersDto.start_date, FilterConditionDataTypeEnums.datetimeType).Build();
            }

            if (!string.IsNullOrEmpty(scheduleFitlersDto.end_date))
            {
                if (string.IsNullOrEmpty(scheduleFitlersDto.start_date))
                    constructed_query += _repositoryFilterConditions.LessThanEqualFilter("PLANNED_DATE", scheduleFitlersDto.end_date, FilterConditionDataTypeEnums.datetimeType).Build();
            }

            if (scheduleFitlersDto.is_evv_export_dirty != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("IsEVVDirty", scheduleFitlersDto.is_evv_export_dirty, FilterConditionDataTypeEnums.boolType).Build();

            if (scheduleFitlersDto.is_billable != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("CanBill", scheduleFitlersDto.is_billable, FilterConditionDataTypeEnums.integerType).Build();

            if (scheduleFitlersDto.IS_BILLED != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("IS_BILLED", scheduleFitlersDto.IS_BILLED, FilterConditionDataTypeEnums.boolType,true).Build();

            if (scheduleFitlersDto.is_payrolled != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("IS_PAID", scheduleFitlersDto.is_payrolled, FilterConditionDataTypeEnums.boolType,true).Build();

            if(!string.IsNullOrEmpty(scheduleFitlersDto.visit_status))
                constructed_query += _repositoryFilterConditions.InFilter("Status", scheduleFitlersDto.visit_status, FilterConditionDataTypeEnums.stringType).Build();

            if (scheduleFitlersDto.is_evv_aggregator_export_required != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("isEvvAggregatorExportRequired", scheduleFitlersDto.is_evv_aggregator_export_required, FilterConditionDataTypeEnums.boolType).Build();

            if (scheduleFitlersDto.evv_dirty_flag != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("isEvvAggregatorExportRequired", true, FilterConditionDataTypeEnums.boolType).Build();

            if (!string.IsNullOrEmpty(scheduleFitlersDto.payer_ids))
                constructed_query += _repositoryFilterConditions.InFilter("PAYMENT_SOURCE", scheduleFitlersDto.payer_ids, FilterConditionDataTypeEnums.integerType).Build();

            if (scheduleFitlersDto.IsSplitForBilling != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("IsSplitForBilling", scheduleFitlersDto.IsSplitForBilling, FilterConditionDataTypeEnums.boolType, true).Build();
            
            if (scheduleFitlersDto.evv_dirty_flag != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("isEvvschedule", true, FilterConditionDataTypeEnums.boolType, true).Build();
            else if (scheduleFitlersDto.is_evv_schedule != null)
                constructed_query += _repositoryFilterConditions.EqualFilter("isEvvschedule", scheduleFitlersDto.is_evv_schedule, FilterConditionDataTypeEnums.boolType, true).Build();

            if(scheduleFitlersDto.client_id_list!=null && scheduleFitlersDto.client_id_list.Length>0)
            {
                string clientIDList = string.Join(",", scheduleFitlersDto.client_id_list);
                constructed_query += _repositoryFilterConditions.InFilter("CaregiverTasks.CLIENT_ID", clientIDList, FilterConditionDataTypeEnums.integerType).Build();
            }

            if(!string.IsNullOrEmpty(constructed_query))
                constructed_query += _repositoryFilterConditions.NotInFilter("CaregiverTasks.Status", "Deleted, Reschedule", FilterConditionDataTypeEnums.stringType).Build();

            return constructed_query;
        }

        public static int ScheduleEVVVendorVersionMasterID(int ServiceGroupID, PaymentsourcesEntity paymentsourcesEntity, PaymentSourcesBranchesEntity paymentSourcesBranchesEntity, HHAEVVScheduleConfigurationGroupEntity hhaEVVScheduleConfigurationGroupEntity, bool ConsiderSettings)
        {
            int EVVVendorVersionMasterID = 0;
            int VendorType = 0; //1 => Primary , 2 => Secondary


            if (hhaEVVScheduleConfigurationGroupEntity != null)
            {
                if (paymentSourcesBranchesEntity != null)
                {
                    if (paymentSourcesBranchesEntity.EvvAggregatorVendorVersionMasterID > 0 && paymentSourcesBranchesEntity.EnableEvv)
                    {
                        if (paymentSourcesBranchesEntity.IsPrimaryEVV == true)
                            VendorType = 1;
                        else
                            VendorType = 2;

                        EVVVendorVersionMasterID = paymentSourcesBranchesEntity.EvvAggregatorVendorVersionMasterID;
                    }
                }

                //if payer branch is not enabled for evv then get the evv info from payer profile
                if (VendorType == 0)
                {
                    //get payer profile info 
                    if (paymentsourcesEntity != null)
                    {
                        if (paymentsourcesEntity.EvvAggregatorVendorVersionMasterID > 0)
                        {
                            if (paymentsourcesEntity.IsPrimaryEVV == true)
                                VendorType = 1;
                            else
                                VendorType = 2;

                            EVVVendorVersionMasterID = paymentsourcesEntity.EvvAggregatorVendorVersionMasterID ?? 0;
                        }
                    }
                }



                if (ConsiderSettings)
                {

                    if (string.IsNullOrEmpty(hhaEVVScheduleConfigurationGroupEntity.PayerDisciplinesJSON))
                    {
                        if (hhaEVVScheduleConfigurationGroupEntity.isApplicableToSecondaryEvvSchedule == true && hhaEVVScheduleConfigurationGroupEntity.isApplicableToPrimaryEvvSchedule == false && VendorType != 2)
                            EVVVendorVersionMasterID = 0;

                        if (hhaEVVScheduleConfigurationGroupEntity.isApplicableToPrimaryEvvSchedule == true && hhaEVVScheduleConfigurationGroupEntity.isApplicableToSecondaryEvvSchedule == false && VendorType != 1)
                            EVVVendorVersionMasterID = 0;
                    }
                    else
                    {
                        List<RootObjectPayerDisciplines> PayerDiscLists = new();
                        if (!string.IsNullOrEmpty(hhaEVVScheduleConfigurationGroupEntity.PayerDisciplinesJSON))
                        {
                            PayerDiscLists = JsonConvert.DeserializeObject<List<RootObjectPayerDisciplines>>(hhaEVVScheduleConfigurationGroupEntity.PayerDisciplinesJSON);
                        }

                        if (PayerDiscLists.Where(i => (i.PayerID == paymentsourcesEntity.PAYMENT_SOURCE_ID || i.PayerID == 0) && (i.GroupID == ServiceGroupID || i.GroupID == 0)).Count() > 0
                            &&
                            ((hhaEVVScheduleConfigurationGroupEntity.isApplicableToSecondaryEvvSchedule == true && VendorType == 2)
                              ||
                             (hhaEVVScheduleConfigurationGroupEntity.isApplicableToPrimaryEvvSchedule == true && VendorType == 1)
                            )
                           )
                        {
                            EVVVendorVersionMasterID = 0;
                        }
                    }
                }
            }

            return EVVVendorVersionMasterID;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.EVV
{
    public class ScheduleEVVInfo
    {
        public DateTime? ACCTUAL_START_TIME { get; set; }
        public DateTime? ACCTUAL_END_TIME { get; set; }
        public char? EvvCheckinLocationVerified { get; set; }
        public char? EVVCheckOutLocationVerified { get; set; }
        public bool HasCheckOutgeoLocation { get; set; }
        public bool HasClientSign { get; set; }
        public bool HasClientVoiceRecording { get; set; }
        public bool HasCheckInGeoLocation { get; set; }
    }
}

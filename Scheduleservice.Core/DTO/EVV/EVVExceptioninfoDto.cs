﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.EVV
{
    public class EVVExceptioninfoDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Event { get; set; }
        public string Context { get; set; }
        public string Notes { get; set; }
        public int EVVVendorVersionMasterID { get; set; }
        public int ResolvedReasoncodeID { get; set; }
    }
}

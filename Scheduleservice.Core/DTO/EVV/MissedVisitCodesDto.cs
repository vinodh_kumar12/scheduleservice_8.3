﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.EVV
{
    public class MissedVisitCodesDto
    {
        public int CGTask_ID { get; set; }
        public int aggregator_id { get; set; }
        public string reason_code { get; set; }
        public string reason_description { get; set; }
        public string action_code { get; set; }
        public string action_description { get; set; }
        public string action_comments { get; set; }
        public string Notes { get; set; }
        public string Context { get; set; }
        public string MasterPayerCodes { get; set; }
    }
}

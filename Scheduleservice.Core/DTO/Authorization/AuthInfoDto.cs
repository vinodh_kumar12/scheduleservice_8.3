﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Authorization
{
    public class AuthInfoDto
    {
        public int? AuthID { get; set; }
        public bool iserror { get; set; }
        public int error_id { get; set; }
        public bool is_softwarning { get; set; }
    }
}

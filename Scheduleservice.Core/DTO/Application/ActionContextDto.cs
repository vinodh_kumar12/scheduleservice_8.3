﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Application
{
    public class ActionContextDto
    {
        public int HHA { get; set; }
        public int User { get; set; }
        public string DbServer { get; set; }
        public string DbName { get; set; }
        public string UniqueRequestId { get; set; }
        public string InstanceCode { get; set; }
        public Guid? Publish_UID { get; set; }
    }
}

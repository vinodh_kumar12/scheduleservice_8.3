﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.CDS
{
    public class PlanYearServicesDTO
    {
       public int planyear_service_id { get; set; }
public string planyear_service_name{ get; set; }
    public string effective_start_date { get; set; }
public string effective_end_date { get; set; }
public string status { get; set; }
public bool is_office_auth { get; set; }

    }
}

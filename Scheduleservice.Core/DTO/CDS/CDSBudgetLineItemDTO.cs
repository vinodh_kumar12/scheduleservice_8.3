﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.CDS
{
    public class CDSBudgetLineItemDTO
    {
        public int total_records { get; set; }
        public int current_page_no { get; set; }
        public int number_records_in_this_page { get; set; }
        public int total_pages { get; set; }
        public int page_size { get; set; }
        public IEnumerable<CDSBudgetLineitemReportList> CDSBudgetLineitemList { get; set; }
    }
    public class CDSBudgetLineitemReportList
    {
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }
        public  int ClientID { get; set; }
        public string PatientId { get; set; }
        public string Payer { get; set; }
        public int PayerID { get; set; }
        public string Service { get; set; }
        public string AuthorizationNo { get; set; }
        public int CDSPlanYearMasterID { get; set; }
        public int CDSPlanYearServiceID { get; set; }
        public int InitialAuthID { get; set; }
        public string Status { get; set; }
        public bool IsOfficeAuth { get; set; }
        public string AutheffectiveStart { get; set; }
        public string AutheffectiveEnd { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public IEnumerable<LineItemList> LineItemDetails { get; set; }


    }

    public class LineItemList
    {
        public string ItemName { get; set; }
        public double Amount { get; set; }
        public double TotalAmount { get; set; }
        public double TotalUsedAmount { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public int? EmployeeID { get; set; }
    }
}

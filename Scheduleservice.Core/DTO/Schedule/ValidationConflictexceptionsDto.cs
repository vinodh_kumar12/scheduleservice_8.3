﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
   public class ValidationConflictexceptionsDto
    {
        public int TotalRecord { get; set; }
        public int PageNo { get; set; }
        public int RecordPerPage { get; set; }
        public IEnumerable<ConflictReportScheduleList> ScheduleList { get; set; }
    }
    public class ConflictReportScheduleList
    {
        public int CGTASKID { get; set; }
        public int ClientID { get; set; }
        public int? ClinicianID { get; set; }
        public int PayerID { get; set; }
        public int ServiceID { get; set; }
        public string Client { get; set; }
        public string Payer { get; set; }
        public string Service { get; set; }
        public DateTime ScheduleDate { get; set; }
        public string ScheduleStatus { get; set; }
        public string Clinician { get; set; }
        public DateTime PlannedStart { get; set; }
        public DateTime PlannedEnd { get; set; }
        public string PlannedHours { get; set; }
        public DateTime? ActualStart { get; set; }
        public DateTime? AcrualEnd { get; set; }
        public string ActualHours { get; set; }
        public int AdmitNo { get; set; }
        public bool isEvvSchedule { get; set; }
        public IEnumerable<CaregiverTaskEvvException> Exceptions { get; set; }
    }
    public class CaregiverTaskEvvException
    {
        public int CaregiverTaskEvvExceptionID { get; set; }
        public bool isResolved { get; set; }
        public string ExceptionCode { get; set; }
        public string ExceptionReason { get; set; }
        public string Resolution { get; set; }
        public string Resolution_comments { get; set; }
        public bool can_resolve { get; set; }
    }

}

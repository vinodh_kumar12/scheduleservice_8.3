﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvTerminalPayersDto
    {

        public char Hierarchy { get; set; }
        public int TerminalPayerID { get; set; }
        //public int Terminal_aggregator_id { get; set; }
        public int terminal_configuration_ID { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvTerminalPayerSchedulesDto
    {
        public int HHA { get; set; }
        public int CgTaskID { get; set; }
        public int child_schedule_id { get; set; }
    }
}

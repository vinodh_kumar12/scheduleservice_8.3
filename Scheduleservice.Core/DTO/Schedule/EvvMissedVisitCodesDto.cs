﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvMissedVisitCodesDto
    {
		//public int CaregiverTaskMissedVisitID { get; set; }
		public string ReasonCode { get; set; }
		public string ReasonCodeDescription { get; set; }
		public string ActionCode { get; set; }
		public string ActionCodeDescription { get; set; }
		public string Notes { get; set; }
		public int CaregiverTaskEvvskMissedvisitsReasonID { get; set; }
		public string ActionCodeComments { get; set; }
		//public string Notes { get; set; }

	}
}

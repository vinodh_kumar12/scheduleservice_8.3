﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
   public class ScheduleEVVBatchListDto
    {
        public Guid BatchID { get; set; }
    }
}

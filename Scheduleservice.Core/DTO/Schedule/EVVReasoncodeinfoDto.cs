﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EVVReasoncodeinfoDto
    {
        public string ReasonCode { get; set; }
        public string ReasonDescription { get; set; } 
        public string Context { get; set; }
        public string Notes { get; set; }
        public string ActionCode { get; set; }
        public string ActionDescription { get; set; }
        public int EVVVendorVersionMasterID { get; set; }       
    }
}

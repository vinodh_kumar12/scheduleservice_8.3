﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvTerminalPayerSchedulesListDto
    {
        public int CaregiverTasksTerminalPayerID { get; set; }
        public int CGTASK_ID { get; set; }
        public int Client_ID { get; set; }
        public int Payer_ID { get; set; }
        public int Service_ID { get; set; }
        public int Terminal_Payer_ID { get; set; }
       
        public string Client_Name { get; set; }
        public string Payer_Name { get; set; }
        public string Terminal_Payer_Name { get; set; }
        public string Service_Name { get; set; }
        public string Schedule_Date { get; set; }
        public string Schedule_Status { get; set; }
        public string Planned_Start_Time { get; set; }
        public string Planned_End_Time { get; set; }
        public string Planned_Hours { get; set; }
        public string Actual_Start_Time { get; set; }
        public string Actual_End_Time { get; set; }
        public string Actual_Hours { get; set; }
        public int Admit_No { get; set; }
        public bool is_Evv_Schedule { get; set; }
        public bool is_Billable { get; set; }
        public bool is_Payable { get; set; }
        public bool is_Billed { get; set; }
        public bool is_Paid { get; set; }         
        public int ChildScheduleID { get; set; }
        public string failure_message { get; set; }
        public string failure_code { get; set; }

        public string last_exported_on { get; set; }
        public string aggregator_name { get; set; }
        public string lob { get; set; }
        public int lob_ID { get; set; }
        public string service_disc { get; set; }
        public string hierarchy { get; set; }
        public string last_export_status { get; set; }

        public int Terminal_Paymentsource_ID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleExceptionsConflictSchedulesDto
    { 
        public string exception_type { get; set; }
        public int dependentCgTaskID { get; set; }
        public string ScheduleDate { get; set; }
        public int ScheduleClientID { get; set; }
        public string ScheduleClient { get; set; }
        public int ScheduleClincianID { get; set; }
        public string ScheduleClincian { get; set; }
        public string ScheduleService { get; set; }
        public string ScheduleStatus { get; set; }
        public string PlannedTime { get; set; }
        public string ActualStartTime { get; set; }
        public string ActualEndTime { get; set; }
    }
}

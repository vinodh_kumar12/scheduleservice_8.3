﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleBasicListDto
    {
        public int CGTASK_ID { get; set; }
        public int Child_Schedule_Id { get; set; }
    }
}

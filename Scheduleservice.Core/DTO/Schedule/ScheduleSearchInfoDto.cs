﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleSearchInfoDto
    {
        public int total_records { get; set; }
        public int current_page_no { get; set; }
        public int number_records_in_this_page { get; set; }
        public int total_pages { get; set; }
        public int page_size { get; set; }
        public IEnumerable<Dictionary<string, object>> visit_details { get; set; }

        public ScheduleSearchInfoDto()
        {
            visit_details = new List<Dictionary<string, object>>();
        }
    }
}

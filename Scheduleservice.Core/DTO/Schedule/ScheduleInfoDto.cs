﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleInfoDto
    {
        public int CGTASK_ID { get; set; }
        public int CAREGIVER { get; set; }
        public string CAREGIVER_NAME { get; set; }
        public string PLANNED_DATE { get; set; }
        public string ACCTUAL_START_TIME { get; set; }
        public string ACCTUAL_END_TIME { get; set; }
        public string EDITED_HOURS { get; set; }
        public string PLANNED_START_TIME { get; set; }
        public string PLANNED_END_TIME { get; set; }
        public string STATUS { get; set; }
        public int SERVICECODE_ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public int CLIENT_ID { get; set; }
        public string CLIENT_NAME { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public string PAYMENT_SOURCE_NAME { get; set; }
        public bool IS_BILLED { get; set; }
        public bool IS_PAID { get; set; }
        public bool CanBill { get; set; }
        public int? eChartMasterID { get; set; }
        public int? oasisDatasetID { get; set; }
        public int? pChartMasterID { get; set; }
        public bool isEvvschedule { get; set; }
        public char EvvCheckinLocationVerified { get; set; }
        public char EvvCheckoutLocationVerified { get; set; }
        public int? CheckinTreatmentLocation { get; set; }
        public int? CheckoutTreatmentLocation { get; set; }
        public bool isAttested { get; set; }
        public string fob_checkin_code { get; set; }
        public string fob_checkout_code { get; set; }
        public int HHA_Branch_Id { get; set; }
        public Guid client_group_uuid { get; set; }
        public Guid Agency_UUID { get; set; }

    }
}

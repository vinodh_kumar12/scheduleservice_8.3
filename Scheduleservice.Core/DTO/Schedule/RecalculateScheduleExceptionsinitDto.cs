﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class RecalculateScheduleExceptionsinitDto
    {
        public int total_records { get; set; }
        public Guid batch_guid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvExceptionEventsDto
    {
        public int EVVVendorVersionMasterID { get; set; }
        public string Event { get; set; }
        public string exception_code { get; set; }
        public bool IsQueueEnabled { get; set; }
        public DateTime lastQueueExecutedon { get; set; }
        public bool ExcepionCodeAvailable { get; set; }

    }
}

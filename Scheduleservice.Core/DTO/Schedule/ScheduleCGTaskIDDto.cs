﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleCGTaskIDDto
    {
        public int CGTaskID { get; set; }
    }
}

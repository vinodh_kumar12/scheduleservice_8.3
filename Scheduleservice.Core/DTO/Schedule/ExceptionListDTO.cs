﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ExceptionListDTO
    {
        public int exception_id { get; set; }
        public string exception_name { get; set; }
    }
}

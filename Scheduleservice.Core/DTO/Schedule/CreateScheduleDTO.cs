﻿using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
	public class CreateScheduleDTO
    {
		public List<CreateSchedulesingleDTO> Schedule { get; set; }

	}
	public class CreateSchedulesingleDTO
	{
		public string schedule_date { get; set; }

		public string planned_start_time { get; set; }

		public string planned_end_time { get; set; }

		public int client_id { get; set; }

		public int service_code_id { get; set; }

		public int payment_source_id { get; set; }
		public int caregiver_id { get; set; }

		public int? cds_auth_service_id { get; set; }

		public int miles { get; set; }

		public int place_of_service { get; set; }

		public bool? is_billable { get; set; }
		public bool is_auth_mand { get; set; }

		public bool? is_payable { get; set; }

		public string check_in_time { get; set; }

		public string check_in_location { get; set; }

		public int? check_in_treatment_location { get; set; }


		public string evv_check_in_time { get; set; }

		public string check_out_time { get; set; }

		public string check_out_location { get; set; }

		public int? check_out_treatment_location { get; set; }
		public char evv_check_in_location_verified { get; set; }
		public char evv_check_out_location_verified { get; set; }

		public float check_in_distance { get; set; }
		public float check_out_distance { get; set; }


		public string evv_check_out_time { get; set; }

		public string edited_hours { get; set; }
		public bool enable_payable_hours { get; set; }

		public string payable_hours { get; set; }

		public float break_hours { get; set; }

		public float travel_time_min { get; set; }

		public int documentation_time_in_min { get; set; }
		public float? units { get; set; }
		public bool is_evvschedule { get; set; }
		public bool is_evvschedule_dirty { get; set; }
		public int? cds_plan_year { get; set; }
		public bool has_child_schedules { get; set; }
		public float total_hours { get; set; }
		public float authorized_hours { get; set; }
		//public int total_minutes_planned { get; set; }
		//public int total_minutes_actual { get; set; }
		public bool is_approved { get; set; }
		public bool is_approved_for_payroll { get; set; }
		public string status { get; set; }
		public int? CDSBudgetLineItemMasterID { get; set; }
		public int? CDSBudgetLineItemID { get; set; }
		public DateTime? original_UTC_checkin_time { get; set; }
		public DateTime? original_UTC_checkout_time { get; set; }
		public bool? canbill { get; set; }
		public bool SplitOvernightSchedulesForBilling { get; set; }
		public bool AutoSplitBasedOnBusinessHours { get; set; }
		public bool SplitOvernightSchedulesForPayroll { get; set; }
		public dynamic caregiver_taskdetails {get;set;}
		public int? Authorization_ID { get; set; }

	}
}

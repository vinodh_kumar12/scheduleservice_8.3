﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleValidationErrorsDto
    {
        public string validation_type { get; set; }
        public string validation_error_message { get; set; }
        public string validation_error_details_json { get; set; }
        public bool is_soft_warning { get; set; } 
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EVVExceptioninfoDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Event { get; set; }
        public string Context { get; set; }
        public string Notes { get; set; }
        public int EVVVendorVersionMasterID { get; set; }
        public int ResolvedReasoncodeID { get; set; } 
    } 
}

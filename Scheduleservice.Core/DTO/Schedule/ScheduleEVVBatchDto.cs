﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleEVVBatchDto
    {
        public int CGTaskID { get; set; }
        public bool isEvvschedule { get; set; }
        public bool isEvvAggregatorExportRequired { get; set; }
    }
}

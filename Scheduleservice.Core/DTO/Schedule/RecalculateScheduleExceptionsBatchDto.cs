﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class RecalculateScheduleExceptionsBatchDto
    {
        public int error_code { get; set; }
        public string error_message { get; set; }
        public int pending_records { get; set; }
    }
}

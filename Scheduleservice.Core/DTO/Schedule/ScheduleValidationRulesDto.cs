﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleValidationRulesDto
    {
        public string ValidationType { get; set; }
        public bool Validationenabled { get; set; }
        public bool block_schedule_creation { get; set; }
        public bool block_clinician_confirmation { get; set; }
        public bool block_checkin { get; set; }
        public bool block_approval { get; set; }
        public int grace_period_mins { get; set; }
        public bool discard_non_billable_schedules { get; set; }
        public bool discard_third_party_clinicians { get; set; }
        public int Min_Duration_Mins { get; set; }
        public float Max_Duration_Mins { get; set; }
        public bool Soft_Warning_Validation { get; set; }
        public string ValidationError { get; set; }
        public bool ValidateForEVV { get; set; }
        public bool ValidateForNonEVV { get; set; }
    }
}

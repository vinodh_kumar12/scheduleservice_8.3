﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class SchedulesListDto
    {
        public int CGTASK_ID { get; set; }

        public List<int> Child_Schedule_Ids { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class ScheduleShortInfoDto
    {
        public int CGTASK_ID { get; set; }
        public int CAREGIVER { get; set; }
        public string CAREGIVER_NAME { get; set; }
        public string PLANNED_DATE { get; set; }
        public string ACCTUAL_START_TIME { get; set; }
        public string ACCTUAL_END_TIME { get; set; }
        public string EDITED_HOURS { get; set; }
        public string PLANNED_START_TIME { get; set; }
        public string PLANNED_END_TIME { get; set; }
        public string STATUS { get; set; }
        public int SERVICECODE_ID { get; set; }
        public string SERVICE_NAME { get; set; }
        public int CLIENT_ID { get; set; }
        public string CLIENT_NAME { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public string PAYMENT_SOURCE_NAME { get; set; }
        public bool IS_BILLED { get; set; }
        public bool IS_PAID { get; set; }
        public bool CanBill { get; set; }
    }
}

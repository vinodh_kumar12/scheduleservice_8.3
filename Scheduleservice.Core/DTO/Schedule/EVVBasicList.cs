﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EVVBasicList
    {
        public int CGTaskID { get; set; }
        public int ClientID { get; set; }
        public int ServiceID { get; set; }
        public int PayerID { get; set; }
        public int ClinicianDiscID { get; set; }
        public string ScheduleDate { get; set; }
        public int CDSPlanYearServiceID { get; set; }
        public bool isEvvAggregatorExportRequired { get; set; }
    }
}

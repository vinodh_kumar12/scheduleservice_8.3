﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EVVAggregatorDetailsDto
    {
        public int EvvConfigurationID { get; set; }
        public bool AttestationMandatoryForAllReasonCodes { get; set; }
        public string AttestationMandatoryEvent { get; set; }
        public bool AttestationHideFromClinician { get; set; }
    }
}

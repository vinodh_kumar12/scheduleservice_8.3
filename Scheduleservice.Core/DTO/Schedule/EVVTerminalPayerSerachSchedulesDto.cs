﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EVVTerminalPayerSerachSchedulesDto
    {

        public int TotalRecord { get; set; }
        public int PageNo { get; set; }
        public int RecordPerPage { get; set; }
        public int TotalPages { get; set; }
        public List<EvvTerminalPayerSchedulesListDto> evvTerminalPayerSearchSchedulesList { get; set; }
    }
}

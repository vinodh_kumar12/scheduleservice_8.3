﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Core.DTO.Schedule
{
    public class EvvTerminalPayerdashboardDto
    {
        public EvvTerminalPayerdashboardDto()
        {
            this.exported_visits = new exported_visits();
            this.failed_visits = new failed_visits();
            this.export_pending = new export_pending();
        }

        public exported_visits exported_visits { get; set; }
        public failed_visits failed_visits { get; set; }
        public export_pending export_pending { get; set; }
    }

    public class exported_visits
    {
        public int current_week { get; set; }
        public int last_week { get; set; }
        public float current_week_percentage { get; set; }
        public float last_week_percentage { get; set; }
        public int total { get; set; }

    }

    public class failed_visits
    {
        public int current_week { get; set; }
        public int last_week { get; set; }
        public float current_week_percentage { get; set; }
        public float last_week_percentage { get; set; }
        public int total { get; set; }
    }

    public class export_pending
    {
        public int current_week { get; set; }
        public int last_week { get; set; }
        public float current_week_percentage { get; set; }
        public float last_week_percentage { get; set; }
        public int total { get; set; }
    }
}

﻿ 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection; 
using Scheduleservice.Core.Interfaces.DBConnections;  
using Scheduleservice.Data.Repository; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using KanCache.Interfaces; 
using KanCache.Controllers; 
using Scheduleservice.Data.Repository.CacheRepository;
using System.Reflection;

namespace Scheduleservice.Data.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureConnectionProvider<T>(this IServiceCollection services, IConfiguration Configuration)
             where T : class, IConnectionProvider
        {           

            services.AddScoped<IConnectionProvider, T>();

            //master Database 
            services.AddScoped<IUAAgencyDatabaseMasterList, UAAgencyDatabaseMasterList>();
            services.Decorate<IUAAgencyDatabaseMasterList, CacheUAAgencyDatabaseMasterList>();


            return services;
        } 
        public static IServiceCollection AddCacheServices(this IServiceCollection services, IConfiguration configuration)
        {
            var redisConnectionString = configuration.GetConnectionString("RedisConnectionString");

            var msSqlEnvironmentConnection = configuration.GetConnectionString("MsSqlEnvironmentConnection");

            services.AddSingleton<IRedisCacheController>(x => new RedisCacheController(redisConnectionString));

            services.AddScoped<ICacheController>(x => new CacheController(redisConnectionString, msSqlEnvironmentConnection));
          
            return services;
        }

        public static IServiceCollection AddDataServiceCollection(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericMasterRepository<>));

            //DB Level
            services.AddScoped<IUAEvvExceptionEventsRepository,UAEvvExceptionEventsRepository>();
            services.AddScoped<IUAEvvExceptionEventMasterRepository, UAEvvExceptionEventMasterRepository>();
            services.AddScoped<IUAEvvExceptionEventCodesRepository, UAEvvExceptionEventCodesRepository>();

            // Agency level
            services.AddScoped<IHHAEVVConfigurationsRepository, HHAEVVConfigurationsRepository>();
            services.AddScoped<IHHALineofbusinessRepository, HHALineofbusinnessRepository>();
            services.AddScoped<IHHAScheduleConflictExceptionsRepository, HHAScheduleConflictExceptionsRepository>();
            services.AddScoped<IHHAScheduleConfigurationsRepository, HHAScheduleConfigurationsRepository>();
            services.AddScoped<IHHAScheduleAuthSettingsRepository, HHAScheduleAuthSettingsRepository>();
            services.AddScoped<IEvvScheduleEditCodesMasterRepository, EvvScheduleEditCodesMasterRepository>();
            services.AddScoped<IHHAEvvExceptionEventsRepository, HHAEvvExceptionEventsRepository>();
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IHHAEvvExceptionEventCodesRepository, HHAEvvExceptionEventCodesRepository>();
            services.AddScoped<IUserRolesRepository, UserRolesRepository>();
            services.AddScoped<IHomeHealthAgenciesRepository, HomeHealthAgenciesRepository>();
            

            //Agency Branch 
            services.AddScoped<IHHABranchScheduleConfigurationsRepository, HHABranchScheduleConfigurationsRepository>();
            services.AddScoped<ITimeZonesRepository, TimeZonesRepository>();
             
            services.AddScoped<IHHAEVVScheduleConfigurationRepository, HHAEVVScheduleConfigurationRepository>();
            services.AddScoped<IHHAEVVScheduleConfigurationGroupRepository, HHAEVVScheduleConfigurationGroupRepository>();
            services.AddScoped<IHHAClinicalRepository, HHAClinicalRepository>();
            services.AddScoped<IHConfigurations2Repository, HConfigurations2Repository>();            
            services.AddScoped<IHomeHealthAgencies_BillingPayrollSettingsRepository, HomeHealthAgencies_BillingPayrollSettingsRepository>();



            // Schedules
            services.AddScoped<ICaregivertasksRepository, CaregivertaskRepository>(); 
            services.AddScoped<IClientEvvTerminalPayersRepository, ClientEvvTerminalPayersRepository>(); 
            services.AddScoped<ICaregiverTasksTerminalPayerRepository, CaregiverTasksTerminalPayerRepository>(); 
            services.AddScoped<ICaregiverTaskEvvSchedulesRepository, CaregiverTaskEvvSchedulesRepository>();
            services.AddScoped<IScheduleProcessingQueueRepository, ScheduleProcessingQueueRepository>();
            services.AddScoped<ICaregiverTaskChildSchedulesRepository, CaregiverTaskChildSchedulesRepository>();
            services.AddScoped<ICaregivertaskMissedVisitsRepository,CaregivertaskMissedVisitsRepository>();

            services.AddScoped<ICDSPlanYearServicesRepository, CDSPlanYearServicesRepository>();
            services.AddScoped<ICDSPayerServiceCategoryMasterRepository, CDSPayerServiceCategoryMasterRepository>();
            services.AddScoped<ICDSClientBudgetLineItemRepository, CDSClientBudgetLineItemRepository>();
            services.AddScoped<ICDSBudgetLineItemMasterRepository, CDSBudgetLineItemMasterRepository>();
            services.AddScoped<ICDSPlanYearMasterRepository, CDSPlanYearMasterRepository>();

            services.AddScoped<IEchartSignaturesRepository, EchartSignaturesRepository>();
            services.AddScoped<IScheduleShiftRepository, ScheduleShiftRepository>();

            //Authorization
            services.AddScoped<IPaymentSourceAuthorizationRepository, PaymentSourceAuthorizationRepository>();

            // Clients
            services.AddScoped<IClientsRepository, ClientsRepository>();
            services.AddScoped<IClientTreatmentAddressRepository, ClientTreatmentAddressRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();

            // Services
            services.AddScoped<IServiceCodesRepository, ServiceCodesRepository>();
            services.AddScoped<IServiceBillRateEffectivePeriodRepository, ServiceBillRateEffectivePeriodRepository>();
            services.AddScoped<IServiceCodeBranchesRepository, ServiceCodeBranchesRepository>();

            //payers
            services.AddScoped<IPaymentsourcesRepository, PaymentsourcesRepository>();
            services.AddScoped<IPaymentsourcesAdditionalRepository, PaymentSourcesAdditionalRepository>();
            services.AddScoped<IPaymentSourcesBranchesRepository, PaymentSourcesBranchesRepository>();

            //Clinicians
            services.AddScoped<ICaregiversRepository, CaregiversRepository>();
            services.AddScoped<ICaregiverDisciplinesRepository, CaregiverDisciplinesRepository>();
            services.AddScoped<ICaregiverOtherDisciplinesRepository, CaregiverOtherDisciplinesRepository>();
            services.AddScoped<ICaregiverDisciplineHistoryRepository, CaregiverDisciplineHistoryRepository>();
            services.AddScoped<ICaregiverMaximumHoursEffectivePeriods,CaregiverMaximumHoursEffectivePeriodsRepository>();

            //CDS tables
            services.AddScoped<ICDSPlanYearServicesRepository, CDSPlanYearServicesRepository>();

            //pick list
            services.AddScoped<IPickListValuesRepository, PickListValuesRepository>();
            services.AddScoped<ICaregiverTaskEVVExceptionsRepository, CaregiverTaskEvvExceptionsRepository>();
            services.AddScoped<ICaregiverTaskEVVReasonsRepository, CaregiverTaskEvvReasonsRepository>();
            services.AddScoped<ICaregiverTaskEvvMissedVisitsReasonsRepository, CaregiverTaskEvvMissedVisitsReasonsRepository>();

            services.AddScoped<IHHAConfigurationsRepository, HHAConfigurationsRepository>();
            services.AddScoped<ICExpiringItemsRepository, CExpiringItemsRepository>();
            services.AddScoped<IEExpiringItemsRepository, EExpiringItemsRepository>();

            //UA tables
            services.AddTransient<IUAScheduleConflictExceptionsRepository, UAScheduleConflictExceptionsRepository>();
            services.AddTransient<IHHAEvvVendorVersionErrorCodeMasterRepository, HHAEvvVendorVersionErrorCodeMasterRepository>();
            services.AddTransient<IHHASelectedEvvMasterRepository, HHASelectedEvvMasterRepository>();
            services.AddTransient<IUAScheduleConflictExceptionsRepository, UAScheduleConflictExceptionsRepository>();
            services.AddTransient<IUAEVVVendorVersionMasterRepository, UAEVVVendorVersionMasterRepository>();
            services.AddTransient<IUAEVVVendorMasterRepository, UAEVVVendorMasterRepository>();

            //Audit tables 
            services.AddScoped<IHModificationActivityLog, HModificationActivityLog>();


            //Auth Tables
            services.AddScoped<IPaymentSourceAuthorizationRepository, PaymentSourceAuthorizationRepository>();

            //episodes
            services.AddScoped<IEpisodeRepository, EpisodesRepository>();

            //Billing
            services.AddScoped<IClaimsRepository, ClaimsRepository>();

            //Charts
            services.AddScoped<IeChartMasterRepository, eChartMasterRepository>();
            services.AddScoped<IpChartMasterRepository, pChartMasterRepository>();
            services.AddScoped<IOasisDatasetsRepository, OasisDatasetsRepository>();
            services.AddScoped<IVisitChartsRepository, VisitChartsRepository>();

            return services;
        }
    }
}

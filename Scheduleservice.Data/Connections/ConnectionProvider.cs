﻿using Scheduleservice.Data.UnitofWorks;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System; 
using System.Linq;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using KanCache.Interfaces;
using KanCache.Common;
using KanCache.Models;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Common.Helpers;
using Scheduleservice.Core.Interfaces.UnitofWorks;

namespace Scheduleservice.Core.Interfaces.DBConnections
{
    public class ConnectionProvider : IConnectionProvider
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly ICacheController _cacheController;
        public ActionContextDto ActionContext { get; }
        public ConnectionProvider(IServiceProvider serviceProvider, ICacheController cacheController)
        {
            this._cacheController = cacheController;
            _accessor = serviceProvider.GetService<IHttpContextAccessor>();
            if (_accessor.HttpContext != null)
                ActionContext = (ActionContextDto)_accessor.HttpContext.Items[ConstantKeys.ActionContext];
            _cacheController.InstanceCode = ActionContext.InstanceCode;
        }
         
        public IUnitOfWork Connect(bool IsReadOnly = false)
        {
            try
            {
                string connectionString = "";
                List<HHADatabaseDetail> hhaDatabaseDetails = _cacheController.GetAgencyDatabaseList();
                var server = "";
                var instance = "";
                if (hhaDatabaseDetails != null)
                {
                    var hhaDatabaseDetail = hhaDatabaseDetails.Where(x => x.HHA_ID == ActionContext.HHA).FirstOrDefault();
                    if (hhaDatabaseDetail != null)
                    {
                        server = hhaDatabaseDetail.Server;
                        instance = hhaDatabaseDetail.DatabaseName;
                    }
                    else
                    {
                        throw new Exception("InValid HHA ID.");

                    }
                }
                if (IsReadOnly)
                {
                    connectionString = _cacheController.GetConnectionString("Private", ReadWriteMode.Read);
                }
                else
                {
                    connectionString = _cacheController.GetConnectionString("Private", ReadWriteMode.ReadWrite);
                }
                 

                if (!string.IsNullOrEmpty(connectionString)&& !string.IsNullOrEmpty(server) && !string.IsNullOrEmpty(instance))
                {
                    if (connectionString.Contains("$SERVER_NAME$"))
                    {
                      

                        if (server == null || string.IsNullOrEmpty(server) || instance == null || string.IsNullOrEmpty(instance))
                            throw new Exception("Can't create connection as server & instance details are empty");

                        connectionString = connectionString.Replace("$SERVER_NAME$", server).Replace("$DATABASE_NAME$", instance);
                    }
                }

                return new UnitOfWork(connectionString);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Connection Strings are blank")
                    throw new Exception("Invalid Instance Code");
                else
                    throw;
            }
        }

      


        public IUnitOfWork ConnectMaster(bool IsReadOnly = false)
        {
            string connectionString = "";
            if (IsReadOnly)
            {
                connectionString = _cacheController.GetConnectionString("Master", ReadWriteMode.Read);
            }
            else
            {
                connectionString = _cacheController.GetConnectionString("Master", ReadWriteMode.ReadWrite);
            }


            return new UnitOfWork(connectionString);

        }
    }
}

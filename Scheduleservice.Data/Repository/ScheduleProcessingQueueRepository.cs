﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ScheduleProcessingQueueRepository : IScheduleProcessingQueueRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public ScheduleProcessingQueueRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<bool> InsertScheduleProcessingQueue(int hha, int userid, Guid batch_guid, string task_type, string validation_rule_json, string cgtask_ids)
        {
            bool Issuccess = false;
            var procedurename = "_S_Schedule_AddScheduleProcessingQueue";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@user", userid);
            parameter.Add("@batch_guid", batch_guid, System.Data.DbType.Guid);
            parameter.Add("@task_type", task_type);
            parameter.Add("@validation_rule_json", validation_rule_json);
            parameter.Add("@cgtask_ids", cgtask_ids);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    int ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }

        public async Task<IEnumerable<ScheduleProcessingQueueEntity>> GetScheduleProcessingQueue(int HHA, int UserId, string Columns, string Filter)
        {
            IEnumerable<ScheduleProcessingQueueEntity> result = new List<ScheduleProcessingQueueEntity>();
            var procedurename = "_S_Schedule_GetScheduleProcessingQueue";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@Filters", Filter);
            parameter.Add("@Columns", Columns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ScheduleProcessingQueueEntity> _genericRepository = new GenericMasterRepository<ScheduleProcessingQueueEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<bool> DeleteScheduleProcessingQueue(int hha, int userid, string batch_guid)
        {
            bool Issuccess = false;
            var procedurename = "_S_Schedule_DeleteScheduleProcessingQueue";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@user", userid);
            parameter.Add("@batchId", batch_guid);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    if (ret == 0)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }
    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ServiceCodeBranchesRepository : IServiceCodeBranchesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public ServiceCodeBranchesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<ServiceCodeBranchesEntity> GetServiceCodeBranchesInfo(int HHA, int UserID, string filters, string columns)
        {
            ServiceCodeBranchesEntity result = new ServiceCodeBranchesEntity();
            var procedurename = "_S_Schedule_GetServiceBranchinfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@filters", filters);
            parameter.Add("@columns", columns); 

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ServiceCodeBranchesEntity> _genericRepository = new GenericMasterRepository<ServiceCodeBranchesEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

    }
}

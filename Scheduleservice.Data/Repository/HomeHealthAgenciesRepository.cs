﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HomeHealthAgenciesRepository : IHomeHealthAgenciesRepository
    {

        private readonly IConnectionProvider _connectionProvider;
        public HomeHealthAgenciesRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<HomeHealthAgenciesEntity>> GetHomeHealthAgencieseBasicInfo(int hha, int userid, string Columns, string Filters)
        {
            IEnumerable<HomeHealthAgenciesEntity> HomeHealthAgenciesEntityList = new List<HomeHealthAgenciesEntity>();

            var procedurename = "[ReadOnly].[_HHA_Schedule_GetHomeHealthAgenciesBasicInfo]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@User", userid);
            parameter.Add("@filter", Filters);
            parameter.Add("@columns", Columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HomeHealthAgenciesEntity> _genericRepository = new GenericMasterRepository<HomeHealthAgenciesEntity>(uow);
                    HomeHealthAgenciesEntityList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return HomeHealthAgenciesEntityList;
        }
    }
}

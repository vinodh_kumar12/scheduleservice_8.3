﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class pChartMasterRepository: IpChartMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public pChartMasterRepository(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<pChartMasterEntity>> GetPChartMasterList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<pChartMasterEntity> pChartMasterEntities = new List<pChartMasterEntity>();
            string procedureName = "_S_Schedule_GetPchartMasterList";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Fitlers", Filters);

            using (var uow = _connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<pChartMasterEntity> _genericRepository = new GenericMasterRepository<pChartMasterEntity>(uow);
                    pChartMasterEntities = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return pChartMasterEntities;
        }
    }
}

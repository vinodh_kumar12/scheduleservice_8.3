﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class PickListValuesRepository : IPickListValuesRepository
    { 
        private readonly IConnectionProvider _connectionProvider;    
        public PickListValuesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<int> CreatePickListValues(int HHA, int UserID, string PickListName, string PickListCategory)
        {
           
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<PickListValuesEntity> _genericRepository = new GenericMasterRepository<PickListValuesEntity>(uow);

                    var SqlQuery = "_H_SavePickListValues";
                    var parameter = new DynamicParameters();
                    parameter.Add("@HHA", HHA);
                    parameter.Add("@USER_ID", UserID);
                    parameter.Add("@PicklistItemValue", PickListName);
                    parameter.Add("@Picklistcategory", PickListCategory);
                    var PickListResult = await _genericRepository.ExecuteProcedure(SqlQuery, parameter).ConfigureAwait(false);
                    return PickListResult;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
        }

        public async Task<int> DeletePickListValues(int HHA, int UserID, int PicklistItemID, string PickListCategory)
        {
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<PickListValuesEntity> _genericRepository = new GenericMasterRepository<PickListValuesEntity>(uow);

                    var SqlQuery = "DeletePickListValues";
                    var parameter = new DynamicParameters();
                    parameter.Add("@hha", HHA);
                    parameter.Add("@user", UserID);
                    parameter.Add("@picklistvalueID", PicklistItemID);
                     
                    var PickListResult = await _genericRepository.ExecuteProcedure(SqlQuery, parameter).ConfigureAwait(false);
                    return PickListResult;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
        }

        public async  Task<IEnumerable<PickListValuesEntity>> GetPickListValues(int HHA, int UserID, string PickListName)
        { 
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {                     
                    IGenericRepository<PickListValuesEntity> _genericRepository = new GenericMasterRepository<PickListValuesEntity>(uow);

                    var SqlQuery = "[ReadOnly].[_HHA_GetPickListValues]";
                    var parameter = new DynamicParameters();
                    parameter.Add("@HHA", HHA);
                    parameter.Add("@USER", UserID);
                    parameter.Add("@pickListName", PickListName);

                    var PickListResult = await _genericRepository.SelectProcedure(SqlQuery, parameter).ConfigureAwait(false);
                    return PickListResult;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }           
        }

        public async Task<int> UpdatePickListValues(int HHA, int UserID, string PickListName, string PickListCategory, int PicklistItemID)
        {
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<PickListValuesEntity> _genericRepository = new GenericMasterRepository<PickListValuesEntity>(uow);

                    var SqlQuery = "_PL_ModifyPickListItem";
                    var parameter = new DynamicParameters();
                    parameter.Add("@HHA", HHA);
                    parameter.Add("@UserID", UserID);
                    parameter.Add("@PickListName", PickListCategory);
                    parameter.Add("@PickListValueID", PicklistItemID);
                    parameter.Add("@PicklistItemName", PickListName);
                    var PickListResult = await _genericRepository.ExecuteProcedure(SqlQuery, parameter).ConfigureAwait(false);
                    return PickListResult;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
        }
    }
}

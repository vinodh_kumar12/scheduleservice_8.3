﻿using Microsoft.Extensions.Options;
using Scheduleservice.Core.Entities; 
using Scheduleservice.Data.Models;
using Scheduleservice.Data.UnitofWorks;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class AgencyMasterListRepository : IAgencyMasterListRepository
    {
        private readonly IOptions<ConnectionProviderOptions> _connectionProviderOptions;
        string connectionMasterString = "";
        public AgencyMasterListRepository(IOptions<ConnectionProviderOptions> connectionProviderOptions)
        {
            _connectionProviderOptions = connectionProviderOptions;
            connectionMasterString = _connectionProviderOptions.Value.connctionMasterstring;
        }

        public async Task<AgencyListEntity> GetAgency(int HHAID)
        {
            var sql = "select HHA_ID, DatabaseName from [dbo].[AgencyList] With(Nolock) where HHA_ID=@HHAID";
            object parameter = new { HHAID = HHAID };
            AgencyListEntity moidfieddataResult = new AgencyListEntity();
            using (var uow = new UnitOfWork(connectionMasterString))
            {
                try
                {
                    IGenericRepository<AgencyListEntity> _genericRepository = new GenericMasterRepository<AgencyListEntity>(uow);
                    moidfieddataResult = await _genericRepository.SelectSingle(sql, parameter).ConfigureAwait(true);
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return moidfieddataResult;
        }

        public async Task<IEnumerable<AgencyListEntity>> GetAgencyList(string DataBase)
        {
            var sql = "select HHA_ID, DatabaseName from [dbo].[AgencyList] With(Nolock) where  DatabaseName=@DataBase";
            object parameter = new { Status = "Active", DataBase = DataBase };
            IEnumerable<AgencyListEntity> moidfieddataResult = new List<AgencyListEntity>();
            using (var uow = new UnitOfWork(connectionMasterString))
            {
                try
                {
                    IGenericRepository<AgencyListEntity> _genericRepository = new GenericMasterRepository<AgencyListEntity>(uow);
                    moidfieddataResult = await _genericRepository.Select(sql, parameter).ConfigureAwait(true);
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return moidfieddataResult;
        }

        public async Task<IEnumerable<AgencyListEntity>> GetallAgencyList()
        {
            var sql = "select HHA_ID, AgencyName, DatabaseName from [dbo].[AgencyList] With(Nolock)";
            object parameter = new { status = "Active" };
            IEnumerable<AgencyListEntity> AgencyListResult = new List<AgencyListEntity>();

            using (var uow = new UnitOfWork(connectionMasterString))
            {
                try
                {
                    IGenericRepository<AgencyListEntity> _genericRepository = new GenericMasterRepository<AgencyListEntity>(uow);
                    AgencyListResult = await _genericRepository.Select(sql, parameter).ConfigureAwait(true);
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return AgencyListResult;
        }
    }
}

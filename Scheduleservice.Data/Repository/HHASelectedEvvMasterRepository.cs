﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;


namespace Scheduleservice.Data.Repository
{
    public class HHASelectedEvvMasterRepository : IHHASelectedEvvMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHASelectedEvvMasterRepository> _logger;

        public HHASelectedEvvMasterRepository(IConnectionProvider connectionProvider,
            ILogger<HHASelectedEvvMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<HHASelectedEvvMasterEntity>> _HHA_Schedule_GetHHAEVVVendors(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<HHASelectedEvvMasterEntity> HHASelectedEVVMasterList = new List<HHASelectedEvvMasterEntity>();
            string procedurename = "_HHA_Schedule_GetHHAEVVVendors";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHASelectedEvvMasterEntity> _genericRepository = new GenericMasterRepository<HHASelectedEvvMasterEntity>(uow);
                    HHASelectedEVVMasterList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return HHASelectedEVVMasterList;
        }

    }
}

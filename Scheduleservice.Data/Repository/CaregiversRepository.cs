﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;  
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class CaregiversRepository : ICaregiversRepository
    {
      
        private readonly IConnectionProvider _connectionProvider;

        public CaregiversRepository(  IConnectionProvider connectionProvider)
        { 
            this._connectionProvider = connectionProvider;
        } 
         
        public async Task<IEnumerable<CaregiversEntity>> GetCaregivers(int HHA, int UserId,string caregiverColumns, string caregiverFilters)
        {
            IEnumerable<CaregiversEntity> result = new List<CaregiversEntity>();
            var procedurename = "_S_Schedule_GetCaregivers";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@Filters", caregiverFilters);
            parameter.Add("@Columns", caregiverColumns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiversEntity> _genericRepository = new GenericMasterRepository<CaregiversEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<CaregiversEntity> GetCaregiverBasicInfo(int HHA, int UserID, int Caregiver_ID)
        {
            CaregiversEntity result = new CaregiversEntity();
            var procedurename = "_S_Schedule_GetCaregiverBasicInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Caregiver_ID", Caregiver_ID); 


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiversEntity> _genericRepository = new GenericMasterRepository<CaregiversEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<CaregiversEntity> GetCaregiverAdditionalInfo(int HHA, int UserID, int Caregiver_ID)
        {
            CaregiversEntity result = new CaregiversEntity();
            var procedurename = "_S_Schedule_GetCaregiverAdditionalInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Caregiver_ID", Caregiver_ID); 

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiversEntity> _genericRepository = new GenericMasterRepository<CaregiversEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregiverVacationsEntity>> GetCaregiverVacations(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<CaregiverVacationsEntity> result = new List<CaregiverVacationsEntity>();
            var procedurename = "_CL_Get_CaregiverVacations";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filter);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverVacationsEntity> _genericRepository = new GenericMasterRepository<CaregiverVacationsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

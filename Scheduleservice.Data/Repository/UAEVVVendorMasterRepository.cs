﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class UAEVVVendorMasterRepository: IUAEVVVendorMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAEVVVendorMasterRepository> _logger;


        public UAEVVVendorMasterRepository(IConnectionProvider connectionProvider,
                                    ILogger<UAEVVVendorMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;

        }

        public async Task<IEnumerable<UAEvvVendorMasterEntity>> _HHA_Schedule_GetUAEVVVendorMaster(int HHA, int UserID, string SchedulesColumns)
        {
            IEnumerable<UAEvvVendorMasterEntity> EVVVendorMasterList = new List<UAEvvVendorMasterEntity>();
            string procedurename = "_HHA_Schedule_GetUAEVVVendorMaster";
            var parameter = new DynamicParameters();
            parameter.Add("@columns", SchedulesColumns);
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<UAEvvVendorMasterEntity> _genericRepository = new GenericMasterRepository<UAEvvVendorMasterEntity>(uow);
                    EVVVendorMasterList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVVendorMasterList;
        }



    }
}

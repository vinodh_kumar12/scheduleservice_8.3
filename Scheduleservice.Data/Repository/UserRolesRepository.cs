﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class UserRolesRepository : IUserRolesRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public UserRolesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<UserRolesEntity>> GetUserEnabledPermissionList(int HHA, int user, string Columns, string filters)
        {
            IEnumerable<UserRolesEntity> result = new List<UserRolesEntity>();
            var procedurename = "_S_Schedule_GetUserEnabledPermissions";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", user);
            parameter.Add("@Columns", Columns);
            parameter.Add("@filters", filters);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<UserRolesEntity> _genericRepository = new GenericMasterRepository<UserRolesEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }
    }
}

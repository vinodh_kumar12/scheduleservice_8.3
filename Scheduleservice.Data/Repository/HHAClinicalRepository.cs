﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAClinicalRepository : IHHAClinicalRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAClinicalRepository> _logger;


        public HHAClinicalRepository(IConnectionProvider connectionProvider,
           ILogger<HHAClinicalRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<HHAClinicalEntity> _S_Schedule_GetHHAClinicalSettings(int HHA, int UserID, string Columns, string Filters)
        {
            HHAClinicalEntity hHAClinicalDetails = new HHAClinicalEntity();
            string procedurename = "_S_Schedule_GetHHAClinicalSettings";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAClinicalEntity> _genericRepository = new GenericMasterRepository<HHAClinicalEntity>(uow);
                    hHAClinicalDetails = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return hHAClinicalDetails;
        }
    }
}

﻿using Dapper; 
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAScheduleAuthSettingsRepository : IHHAScheduleAuthSettingsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAScheduleAuthSettingsRepository> logger;

        public HHAScheduleAuthSettingsRepository(IConnectionProvider connectionProvider, ILogger<HHAScheduleAuthSettingsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }
        

        public async Task<IEnumerable<HHAScheduleAuthSettingsEntity>> GetHHAScheduleAuthSettingsDetails(int HHA, int UserID)
        {
            IEnumerable<HHAScheduleAuthSettingsEntity> eVVExceptionSettings = null;
            string procedurename = "_HHA_Schedule_GetEVVExceptionSettings";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAScheduleAuthSettingsEntity> _genericRepository = new GenericMasterRepository<HHAScheduleAuthSettingsEntity>(uow);
                   var  _eVVExceptionSettingsList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    eVVExceptionSettings = _eVVExceptionSettingsList.ToList();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return eVVExceptionSettings;
        }

        public async Task<bool> SaveHHAScheduleAuthSettingsDetails(int HHA, int UserID, string configurations)
        {
            bool result = false;
            string procedurename = "_HHA_SaveEVVExceptionSettings";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@configurations", configurations); 

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<bool> _genericRepository = new GenericMasterRepository<bool>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    if (ret == 0)
                        result = true;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;

        } 
         
    }
}
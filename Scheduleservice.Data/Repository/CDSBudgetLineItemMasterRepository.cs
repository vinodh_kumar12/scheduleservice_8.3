﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CDSBudgetLineItemMasterRepository: ICDSBudgetLineItemMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CDSBudgetLineItemMasterRepository> _logger;

        public CDSBudgetLineItemMasterRepository(IConnectionProvider connectionProvider,
           ILogger<CDSBudgetLineItemMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<CDSBudgetLineItemMasterEntity> _S_Schedule_CDSBudgetLineItemMaster(int HHA, int UserID, string columns, string Filters)
        {
            CDSBudgetLineItemMasterEntity cDSBudgetLineItemMaster = new CDSBudgetLineItemMasterEntity();
            string procedurename = "_S_Schedule_CDSBudgetLineItemMaster";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSBudgetLineItemMasterEntity> _genericRepository = new GenericMasterRepository<CDSBudgetLineItemMasterEntity>(uow);
                    cDSBudgetLineItemMaster = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSBudgetLineItemMaster;
        }

        public async Task<IEnumerable<CDSBudgetLineItemMasterEntity>> _S_Schedule_GetCDSBudgetLineItemMaster(int HHA, int UserId, string columns, string Filters)
        {
            IEnumerable<CDSBudgetLineItemMasterEntity> result = new List<CDSBudgetLineItemMasterEntity>();
            var procedurename = "_S_Schedule_CDSBudgetLineItemMaster";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSBudgetLineItemMasterEntity> _genericRepository = new GenericMasterRepository<CDSBudgetLineItemMasterEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

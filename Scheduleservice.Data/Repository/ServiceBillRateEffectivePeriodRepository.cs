﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ServiceBillRateEffectivePeriodRepository : IServiceBillRateEffectivePeriodRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public ServiceBillRateEffectivePeriodRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<ServiceBillRateEffectivePeriodEntity> GetServiceBillRateEffectivePeriodInfo(int HHA, int UserID, string filters, string columns)
        {
            ServiceBillRateEffectivePeriodEntity serviceBillRateEffectivePeriodEntity = new ServiceBillRateEffectivePeriodEntity();

            var procedurename = "_S_Schedule_GetServiceBillRateEffectivePeriodinfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@filters", filters);
            parameter.Add("@columns", columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ServiceBillRateEffectivePeriodEntity> _genericRepository = new GenericMasterRepository<ServiceBillRateEffectivePeriodEntity>(uow);
                    serviceBillRateEffectivePeriodEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
                return serviceBillRateEffectivePeriodEntity;
            }
        }
    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HHABranchScheduleConfigurationsRepository : IHHABranchScheduleConfigurationsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public HHABranchScheduleConfigurationsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<HHABranchScheduleConfigurationsEntity> GetHHABranchScheduleConfigurations(int HHA, int UserId, int HHABranchID)
        {
            HHABranchScheduleConfigurationsEntity hhaBranchScheduleConfigurationsEntity = new HHABranchScheduleConfigurationsEntity();
            string procedurename = "_S_Schedule_GetHHABranchConfiguration";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@HHABranchID", HHABranchID);  

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHABranchScheduleConfigurationsEntity> _genericRepository = new GenericMasterRepository<HHABranchScheduleConfigurationsEntity>(uow);
                    hhaBranchScheduleConfigurationsEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return hhaBranchScheduleConfigurationsEntity;
        }

        public async Task<string> _VC_GetCurrentTime(int HHA, int UserId, int HHABranchID, string Context)
        {
            string GetCurrentTime = null;
            string procedurename = "_VC_GetCurrentTime";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@Location", HHABranchID);
            parameter.Add("@StrContext", Context);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<string> _genericRepository = new GenericMasterRepository<string>(uow);
                    GetCurrentTime = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return GetCurrentTime;
        }
    }
}

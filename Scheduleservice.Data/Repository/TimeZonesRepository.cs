﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class TimeZonesRepository : ITimeZonesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public TimeZonesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<TimeZonesEntity>> GetTimeZoneName(int HHA, int User, string ZoneID)
        {
            IEnumerable<TimeZonesEntity> timeZoneName = new List<TimeZonesEntity>();
            var procedureName = "_S_Schedule_GetTimeZones";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@ZoneID", ZoneID);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<TimeZonesEntity> _genericRepository = new GenericMasterRepository<TimeZonesEntity>(uow);
                    timeZoneName = await _genericRepository.SelectProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }

            return timeZoneName;
        }
    }
}

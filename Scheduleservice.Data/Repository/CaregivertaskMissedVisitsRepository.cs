﻿using CacheManager.Core.Logging;
using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Data.Repository
{
    public class CaregivertaskMissedVisitsRepository : ICaregivertaskMissedVisitsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregivertaskMissedVisitsRepository> _logger;
        public CaregivertaskMissedVisitsRepository(IConnectionProvider connectionProvider, ILogger<CaregivertaskMissedVisitsRepository> logger)
        {
            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<CaregivertaskMissedVisitsEntity>> GetMissedVisitInfo(int hha, int user,string columns, string filters)
        {
            IEnumerable<CaregivertaskMissedVisitsEntity> result = new List<CaregivertaskMissedVisitsEntity>();
            var procedurename = "_S_Schedule_GetMissedVisitInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@User", user);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", filters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertaskMissedVisitsEntity> _genericRepository = new GenericMasterRepository<CaregivertaskMissedVisitsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;


        } 
    }
}


﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class ClientsRepository: IClientsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public ClientsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<ClientsEntity>> GetALLClients(int HHA, int UserID,string clientColumns, string clientsFilters)
        {
            IEnumerable<ClientsEntity> result = new List<ClientsEntity>();
            var procedurename = "_S_Schedule_GetClients";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filters", clientsFilters);
            parameter.Add("@Columns", clientColumns);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientsEntity> _genericRepository = new GenericMasterRepository<ClientsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }

        public async Task<IEnumerable<ClientNonPreferredCliniciansEnity>> GetClientNonPreferredCliniciansList(int HHA, int UserID, int CLIENT_ID)
        {
            
            IEnumerable<ClientNonPreferredCliniciansEnity> result = new List<ClientNonPreferredCliniciansEnity>();
            var procedurename = "_S_Schedule_GetClientNonPreferredCliniciansList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@CLIENT_ID", CLIENT_ID); 
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientNonPreferredCliniciansEnity> _genericRepository = new GenericMasterRepository<ClientNonPreferredCliniciansEnity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

        public async Task<IEnumerable<ClientPaymentSourcesEntity>> GetClientPaymentSourcesInfo(int HHA, int UserID, int CLIENT_ID, int PAYMENT_SOURCE_ID)
        {
            IEnumerable<ClientPaymentSourcesEntity> result = new List<ClientPaymentSourcesEntity>();
            var procedurename = "_S_Schedule_GetClientPaymentSourcesInfo";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@CLIENT_ID", CLIENT_ID);
            parameter.Add("@PAYMENT_SOURCE_ID", PAYMENT_SOURCE_ID);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientPaymentSourcesEntity> _genericRepository = new GenericMasterRepository<ClientPaymentSourcesEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }

        public async Task<ClientsEntity> GetClientsBasicInfo(int HHA, int UserID, int CLIENT_ID)
        {
            ClientsEntity result = new ClientsEntity();
            var procedurename = "_S_Schedule_GetClientInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID); 
            parameter.Add("@CLIENT_ID", CLIENT_ID); 

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientsEntity> _genericRepository = new GenericMasterRepository<ClientsEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }

        public async Task<IEnumerable<ClientVacationMasterEntity>> GetClientVacationMasterList(int HHA, int UserID, int CLIENT_ID)
        {
            IEnumerable<ClientVacationMasterEntity> result = new List<ClientVacationMasterEntity>();
            var procedurename = "_S_Schedule_GetClientVacationMasterList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@CLIENT_ID", CLIENT_ID);
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientVacationMasterEntity> _genericRepository = new GenericMasterRepository<ClientVacationMasterEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

        public async Task<IEnumerable<_CL_NonPreferredCliniciansEntity>> GetNonPreferredClinicians(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<_CL_NonPreferredCliniciansEntity> result = new List<_CL_NonPreferredCliniciansEntity>();
            var procedurename = "_CL_Get_NonPreferredClinicians";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filter);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<_CL_NonPreferredCliniciansEntity> _genericRepository = new GenericMasterRepository<_CL_NonPreferredCliniciansEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<ClientVacationMasterEntity>> GetClientVacationMaster(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<ClientVacationMasterEntity> result = new List<ClientVacationMasterEntity>();
            var procedurename = "_CL_Get_ClientVacationMaster";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filter);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientVacationMasterEntity> _genericRepository = new GenericMasterRepository<ClientVacationMasterEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

      
        public async Task<IEnumerable<ClientPaymentSourcesEntity>> GetClientPaymentSourcesList(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters, string ClientFilters_json)
        {
            IEnumerable<ClientPaymentSourcesEntity> clientPaymentSourcesEntities = new List<ClientPaymentSourcesEntity>();

            var procedurename = "_S_Schedule_GetClientPaymentSources";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Columns", ClientPaymentColumns);
            parameter.Add("@Filters", ClientPaymentFilters);
            parameter.Add("@ClientFilter_json", ClientFilters_json);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientPaymentSourcesEntity> genericRepository = new GenericMasterRepository<ClientPaymentSourcesEntity>(uow);
                    clientPaymentSourcesEntities = await genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return clientPaymentSourcesEntities;
        }

        public async Task<IEnumerable<ClientAdditionalPaymentSourcesEntity>> GetClientAdditionalPaymentSourcesList(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters)
        {
            IEnumerable<ClientAdditionalPaymentSourcesEntity> clientAdditionalPaymentSourcesEntities = new List<ClientAdditionalPaymentSourcesEntity>();

            var procedurename = "_S_Schedule_GetClientAdditionalPaymentSources";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Columns", ClientPaymentColumns);
            parameter.Add("@Filters", ClientPaymentFilters);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientAdditionalPaymentSourcesEntity> genericRepository = new GenericMasterRepository<ClientAdditionalPaymentSourcesEntity>(uow);
                    clientAdditionalPaymentSourcesEntities = await genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return clientAdditionalPaymentSourcesEntities;
        }

        public async Task<IEnumerable<DualEligibilePayersEntity>> GetClientDualEligiblePaymentSourceslist(int HHA, int UserID, string ClientPaymentColumns, string ClientPaymentFilters)
        {
            IEnumerable<DualEligibilePayersEntity> dualEligibilePayersEntities = new List<DualEligibilePayersEntity>();

            var procedurename = "_S_Schedule_GetClientDualEligiblePaymentSources";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Columns", ClientPaymentColumns);
            parameter.Add("@Filters", ClientPaymentFilters);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<DualEligibilePayersEntity> genericRepository = new GenericMasterRepository<DualEligibilePayersEntity>(uow);
                    dualEligibilePayersEntities = await genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return dualEligibilePayersEntities;
        }

        public async Task<IEnumerable<ClientPayerEffectivePeriodsEntity>> GetClientPayerEffectivePeriods(int HHA, int UserID, string Columns, string Filters)
        {
            IEnumerable<ClientPayerEffectivePeriodsEntity> clientPayerEffectivePeriods = new List<ClientPayerEffectivePeriodsEntity>();

            var procedurename = "_S_Schedule_GetClientPayerEffectivePeriods";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Columns", Columns);
            parameter.Add("@Filters", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientPayerEffectivePeriodsEntity> genericRepository = new GenericMasterRepository<ClientPayerEffectivePeriodsEntity>(uow);
                    clientPayerEffectivePeriods = await genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }


            return clientPayerEffectivePeriods;
        }

        public async Task<IEnumerable<ClientInactivePeriodsEntity>> GetClientInactivePeriods(int HHA, int UserID, int ClientID)
        {
            IEnumerable<ClientInactivePeriodsEntity> clientInactivePeriodsEntities = new List<ClientInactivePeriodsEntity>();

            string procedureName = "[ReadOnly].[_S_GetClientInactivePeriods]";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@hha", HHA);
            parameters.Add("@user", UserID);
            parameters.Add("@ClientId", ClientID);

            using (var uow = _connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientInactivePeriodsEntity> genericRepository = new GenericMasterRepository<ClientInactivePeriodsEntity>(uow);
                    clientInactivePeriodsEntities = await genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return clientInactivePeriodsEntities;
        }

        public async Task<IEnumerable<ClientsEntity>> GetClientStatus(int HHA, int UserID, string ClientIDs)
        {
            IEnumerable<ClientsEntity> result = new List<ClientsEntity>();
            var procedurename = "_S_Schedule_GetClientsStatus";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@ClientIDs", ClientIDs); 

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClientsEntity> _genericRepository = new GenericMasterRepository<ClientsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }
    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class UAEvvExceptionEventCodesRepository : IUAEvvExceptionEventCodesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAEvvExceptionEventsRepository> logger;

        public UAEvvExceptionEventCodesRepository(IConnectionProvider connectionProvider, ILogger<UAEvvExceptionEventsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<UAEVVExceptionEventCodesEntity>> GetUAEVVExceptionEventCodes(int UserID, int EvvVendorVersionMasterID, string Filters, string Columns)
        {
            IEnumerable<UAEVVExceptionEventCodesEntity> UAEVVExceptionEventCodesList= new List<UAEVVExceptionEventCodesEntity>();

            var procedurename = "[ReadOnly].[_DB_Schedule_GetUAExceptionEventCodes]";

            var parameter = new DynamicParameters();
            parameter.Add("@User", UserID);
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);
            parameter.Add("@filter", Filters);
            parameter.Add("@columns", Columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<UAEVVExceptionEventCodesEntity> _genericRepository = new GenericMasterRepository<UAEVVExceptionEventCodesEntity>(uow);
                    UAEVVExceptionEventCodesList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return UAEVVExceptionEventCodesList;
        }
    }
}

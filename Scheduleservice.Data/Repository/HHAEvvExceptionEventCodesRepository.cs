﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;


namespace Scheduleservice.Data.Repository
{
    public class HHAEvvExceptionEventCodesRepository: IHHAEvvExceptionEventCodesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAEvvExceptionEventsRepository> logger;

        public HHAEvvExceptionEventCodesRepository(IConnectionProvider connectionProvider, ILogger<UAEvvExceptionEventsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<HHAEVVExceptionEventCodesEntity>> GetHHAEVVExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID, string Filters, string Columns)
        {
            IEnumerable<HHAEVVExceptionEventCodesEntity> HHAEVVExceptionEventCodesList = new List<HHAEVVExceptionEventCodesEntity>();

            var procedurename = "[ReadOnly].[_HHA_Schedule_GetHHAExceptionEventCodes]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);
            parameter.Add("@filter", Filters);
            parameter.Add("@columns", Columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEVVExceptionEventCodesEntity> _genericRepository = new GenericMasterRepository<HHAEVVExceptionEventCodesEntity>(uow);
                    HHAEVVExceptionEventCodesList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return HHAEVVExceptionEventCodesList;
        }


        public async Task<bool> AddEVVExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID, string eventcodes, string exception_code)
        {
            bool Error = true;
            int ret;

            var procedurename = "_HHA_Schedule_AddHHAEventCodes";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);
            parameter.Add("@events", eventcodes);
            parameter.Add("@exception_code", exception_code);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception e)
                {
                    logger.LogError(e,e.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Error;
        }

        public async Task<bool> DeleteEVVExceptionEventCodes(int HHA, int UserID, string ExceptionEventCodeIDs)
        {
            bool Error = true;
            int ret;

            var procedurename = "_HHA_Schedule_DeleteHHAEventCodes";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@ExceptionEventCodeIDs", ExceptionEventCodeIDs);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception e)
                {
                    logger.LogError(e,e.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Error;
        }

        public async Task<bool> UpdateEVVEventCodes(int HHA, int userID, string filters, string columns)
        {
            int ret = 0;
            bool ret_bool = false;

            var procedurename = "_HHA_Schedule_UpdateEVVEventCodes";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", userID);
            parameter.Add("@filters", filters);
            parameter.Add("@columns", columns);
            //  parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    if (ret == 0)
                        ret_bool = true;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret_bool;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;


namespace Scheduleservice.Data.Repository
{
    public class UAEVVVendorVersionMasterRepository : IUAEVVVendorVersionMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAEVVVendorVersionMasterRepository> _logger;

        public UAEVVVendorVersionMasterRepository(IConnectionProvider connectionProvider,
            ILogger<UAEVVVendorVersionMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<UAEvvVendorVersionMasterEntity>> _HHA_Schedule_GetUAEVVVendorVersionMaster(int HHA, int UserID, string SchedulesColumns)
        {
            IEnumerable<UAEvvVendorVersionMasterEntity> EVVVendorVersionMasterList = new List<UAEvvVendorVersionMasterEntity>();
            string procedurename = "_HHA_Schedule_GetUAEVVVendorVersionMaster";
            var parameter = new DynamicParameters();
            parameter.Add("@columns", SchedulesColumns);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<UAEvvVendorVersionMasterEntity> _genericRepository = new GenericMasterRepository<UAEvvVendorVersionMasterEntity>(uow);
                    EVVVendorVersionMasterList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVVendorVersionMasterList;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAEVVConfigurationsRepository : IHHAEVVConfigurationsRepository
    {
         
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAEVVConfigurationsRepository> _logger;

        public HHAEVVConfigurationsRepository(IConnectionProvider connectionProvider,
            ILogger<HHAEVVConfigurationsRepository> logger)
        {
             
            this._connectionProvider = connectionProvider;
            this._logger = logger;
        } 

        public async Task<bool> _HHA_Schedule_SaveEVVBranchAggregatorSettings(int HHA, int UserID, string EvvConfigurationIDs="", string ModifiedData="", string ModifiedData_Audit = "")
        {
            bool result = false;
            string procedurename = "_HHA_Schedule_SaveEVVBranchAggregatorSettings";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@userID", UserID);
            parameter.Add("@EvvConfigurationIDs", EvvConfigurationIDs);
            parameter.Add("@ModifiedData", ModifiedData);
            parameter.Add("@ModifiedData_Audit", ModifiedData_Audit);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<bool> _genericRepository = new GenericMasterRepository<bool>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    if (ret == 0)
                        result = true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

       
        public async Task<IEnumerable<HHAEVVConfigurationsEntity>> _HHA_Schedule_GetHHAEVVConfigurations(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<HHAEVVConfigurationsEntity> EVVConfigurationList = new List<HHAEVVConfigurationsEntity>();
            string procedurename = "_HHA_Schedule_GetHHAEVVConfigurations";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEVVConfigurationsEntity> _genericRepository = new GenericMasterRepository<HHAEVVConfigurationsEntity>(uow);
                    EVVConfigurationList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVConfigurationList;
        }

    }
}

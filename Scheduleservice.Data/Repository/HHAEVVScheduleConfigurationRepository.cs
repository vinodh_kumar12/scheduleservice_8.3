﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAEVVScheduleConfigurationRepository : IHHAEVVScheduleConfigurationRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAEVVScheduleConfigurationRepository> _logger;


        public HHAEVVScheduleConfigurationRepository(IConnectionProvider connectionProvider,
           ILogger<HHAEVVScheduleConfigurationRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }


        public async Task<IEnumerable<HHAEVVScheduleConfigurationEntity>> GetHHAEVVScheduleConfigurationSettings(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<HHAEVVScheduleConfigurationEntity> EVVScheduleConfigurationList = new List<HHAEVVScheduleConfigurationEntity>();
            string procedurename = "_HHA_Schedule_GetHHAEVVScheduleConfigurations";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEVVScheduleConfigurationEntity> _genericRepository = new GenericMasterRepository<HHAEVVScheduleConfigurationEntity>(uow);
                    EVVScheduleConfigurationList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVScheduleConfigurationList;
        }
    }
}

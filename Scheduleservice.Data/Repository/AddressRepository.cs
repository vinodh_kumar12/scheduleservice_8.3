﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class AddressRepository : IAddressRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<AddressRepository> _logger;


        public AddressRepository(IConnectionProvider connectionProvider,
           ILogger<AddressRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<AddressEntity> _S_Schedule_GetTreatmentLocation(int HHA, int UserID, int AddressID)
        {
            AddressEntity Address = new AddressEntity();
            string procedurename = "_S_Schedule_GetTreatmentLocation";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@AddressID", AddressID);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<AddressEntity> _genericRepository = new GenericMasterRepository<AddressEntity>(uow);
                    var Addressinfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    Address = Addressinfo.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Address;
        }


        public async Task<AddressEntity> _S_Schedule_GetTreatmentLocationInfo(int HHA, int UserID, string columns, string Filters)
        {
            AddressEntity Address = new AddressEntity();
            string procedurename = "[ReadOnly].[_S_Schedule_GetTreatmentLocationInfo]";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<AddressEntity> _genericRepository = new GenericMasterRepository<AddressEntity>(uow);
                    var Addressinfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    Address = Addressinfo.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Address;
        }
    }
}

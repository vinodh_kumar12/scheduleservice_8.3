﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;

namespace Scheduleservice.Data.Repository
{
    public class CaregiverDisciplinesRepository : ICaregiverDisciplinesRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public CaregiverDisciplinesRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<CaregiverDisciplinesEntity>> GetCaregiverDiscipines(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<CaregiverDisciplinesEntity> caregiverDisciplines = new List<CaregiverDisciplinesEntity>();
            var procedurename = "_S_Schedule_GetClinicianDisciplines";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filters", Filter);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregiverDisciplinesEntity> _genericRepository = new GenericMasterRepository<CaregiverDisciplinesEntity>(uow);
                    caregiverDisciplines = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return caregiverDisciplines;
        }
    }
}

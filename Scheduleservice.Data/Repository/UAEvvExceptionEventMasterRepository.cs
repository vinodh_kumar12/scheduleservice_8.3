﻿using System;
using System.Collections.Generic; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System.Threading.Tasks;
using Scheduleservice.Core.Entities;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class UAEvvExceptionEventMasterRepository : IUAEvvExceptionEventMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public UAEvvExceptionEventMasterRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<_UA_EvvExceptionEventMasterEntity>> GetEvvExceptionEventMasterList(int HHA, int UserID)
        {
            IEnumerable<_UA_EvvExceptionEventMasterEntity> evvExceptionEventMasterList = new List<_UA_EvvExceptionEventMasterEntity>();

            var procedurename = "[ReadOnly].[_DB_Schedule_GetEventMasterList]";

            var parameter = new DynamicParameters();

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_UA_EvvExceptionEventMasterEntity> _genericRepository = new GenericMasterRepository<_UA_EvvExceptionEventMasterEntity>(uow);
                    evvExceptionEventMasterList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return evvExceptionEventMasterList;
        }
    }
}

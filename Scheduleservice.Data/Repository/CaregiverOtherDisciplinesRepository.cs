﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class CaregiverOtherDisciplinesRepository: ICaregiverOtherDisciplinesRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public CaregiverOtherDisciplinesRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<CaregiverOtherDisciplinesEntity>> GetCaregiverOtherDiscipines(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<CaregiverOtherDisciplinesEntity> caregiverOtherDisciplines = new List<CaregiverOtherDisciplinesEntity>();
            var procedurename = "_S_Schedule_GetClinicianOtherDisciplines";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filters", Filter);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregiverOtherDisciplinesEntity> _genericRepository = new GenericMasterRepository<CaregiverOtherDisciplinesEntity>(uow);
                    caregiverOtherDisciplines = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return caregiverOtherDisciplines;
        }
    }
}

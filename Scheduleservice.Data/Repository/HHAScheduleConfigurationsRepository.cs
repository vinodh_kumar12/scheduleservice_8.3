﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HHAScheduleConfigurationsRepository : IHHAScheduleConfigurationsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public HHAScheduleConfigurationsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<HHAScheduleConfigurationsEntity> GetHHAScheduleConfigurations(int HHA, int UserId)
        {
            HHAScheduleConfigurationsEntity hhaScheduleConfigurationsEntity=new HHAScheduleConfigurationsEntity();
            string procedurename = "_S_Schedule_GetHHAConfiguration";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAScheduleConfigurationsEntity> _genericRepository = new GenericMasterRepository<HHAScheduleConfigurationsEntity>(uow);
                    hhaScheduleConfigurationsEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                     
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return hhaScheduleConfigurationsEntity;
        }
    }
}

﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ClaimsRepository : IClaimsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<ClaimsRepository> _logger;
        public ClaimsRepository(IConnectionProvider connectionProvider, ILogger<ClaimsRepository> logger)
        {
            _connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<IEnumerable<ClaimsEntity>> GetScheduleClaims(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<ClaimsEntity> claimsEntities = new List<ClaimsEntity>();
            var procedureName = "_S_Schedule_GetScheduleClaims";
            var parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Filters", Filters);

            using (var uow = _connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ClaimsEntity> _genericRepository = new GenericMasterRepository<ClaimsEntity>(uow);
                    var res = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);

                    if (res.Any())
                    {
                        claimsEntities = res;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e,e.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return claimsEntities;
        }
    }
}

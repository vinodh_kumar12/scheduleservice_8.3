﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class VisitChartsRepository: IVisitChartsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<VisitChartsRepository> _logger;

        public VisitChartsRepository(IConnectionProvider connectionProvider, ILogger<VisitChartsRepository> logger)
        {
            _connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<IEnumerable<VisitChartsEntity>> GetVisitChartsList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<VisitChartsEntity> visitChartsEntities = new List<VisitChartsEntity>();

            string procedureName = "_S_Schedule_GetVisitChartsList";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Filters", Filters);

            using (var uow = _connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<VisitChartsEntity> _genericRepository = new GenericMasterRepository<VisitChartsEntity>(uow);
                    visitChartsEntities = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    _logger.LogError(e,e.Message);
                    throw;
                }
            }

            return visitChartsEntities;
        }
    }
}

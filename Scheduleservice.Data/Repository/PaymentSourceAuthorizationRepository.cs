﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.DTO.Authorization;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class PaymentSourceAuthorizationRepository : IPaymentSourceAuthorizationRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<AddressRepository> _logger;

        public PaymentSourceAuthorizationRepository(IConnectionProvider connectionProvider,
           ILogger<AddressRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }
        public async Task<int> CanModifiedSchedluleWithSameAuth(int HHA, int UserID, int OldAuthId, 
                    int CgtasID, string NewScheduleDate, int NewService, float NewUnits)
        {
            int ret = 0; //1 => success
            string procedurename = "_S_Auth_CanModifiedSchedluleWithSameAuth";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@OldAuthId", OldAuthId);
            parameter.Add("@CgtaskId", CgtasID);
            parameter.Add("@NewScheduleDate", NewScheduleDate);
            parameter.Add("@NewService", NewService);
            parameter.Add("@NewUnits", NewUnits);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
             
            return ret;
        }

        public async Task<AuthInfoDto> GetAvailableAuthID(int HHA, int UserID, int OldAuthId,int ClientID,
                int PayerID, int ServiceID, string ScheduleDate, float ScheduleUnits,
                bool IsOfficeAuth, bool ConfirmSoftwarning,float ScheduleHours,
                bool ConsiderBothOfficeAndRealAuth)
        {
            int ret = 0; //1 => success

            AuthInfoDto authInfoDto = new AuthInfoDto();

            string procedurename = "[ReadOnly].[_S_GetAvailableAuthorizationId]";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@OldAuthId", OldAuthId);
            parameter.Add("@Client_ID", ClientID);
            parameter.Add("@PayerID", PayerID);
            parameter.Add("@ServiceID", ServiceID);
            parameter.Add("@ScheduleDate", ScheduleDate);
            parameter.Add("@ScheduleUnits", ScheduleUnits);
            parameter.Add("@IsOfficeAuth", IsOfficeAuth);
            parameter.Add("@Confirmsoftwarning", ConfirmSoftwarning);
            parameter.Add("@ScheduleHours", ScheduleHours);
            parameter.Add("@ConsiderBothOfficeAndRealAuth", ConsiderBothOfficeAndRealAuth);
            parameter.Add("@AuthID", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);


            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                    authInfoDto.error_id = ret;
                    if (ret != 1)
                        authInfoDto.iserror = true;
                    else
                    {
                        authInfoDto.AuthID = parameter.Get<int>("@AuthID");
                    }                    
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return authInfoDto;
        }

        public async Task<PaymentSourceAuthorizationsEntity> GetAuthInfo(int HHA, int UserID, string filters, string columns)
        {
            PaymentSourceAuthorizationsEntity result = new PaymentSourceAuthorizationsEntity();

            var procedurename = "_S_Schedule_GetAuthorizationInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filter", filters);
            parameter.Add("@Columns", columns);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<PaymentSourceAuthorizationsEntity> _genericRepository = new GenericMasterRepository<PaymentSourceAuthorizationsEntity>(uow);
                    var AuthInfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                    result = AuthInfo.FirstOrDefault();

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;

        }

        public async Task<bool> UpdateAuthoriaztionInfo(int HHA, int UserID, string Updatefilters, string UpdateColumns)
        {
            bool ret = false;

            var procedurename = "_S_Schedule_UpdateAuthorizationInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filter", Updatefilters);
            parameter.Add("@UpdateColumns", UpdateColumns);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    var result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    if (result == 0)
                    {
                        ret = true;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
                return ret;
            }

        }

    }
}

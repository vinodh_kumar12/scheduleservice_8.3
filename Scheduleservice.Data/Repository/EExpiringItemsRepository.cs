﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Scheduleservice.Data.Repository
{
    class EExpiringItemsRepository : IEExpiringItemsRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public EExpiringItemsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<_E_ExpiringItemsEntity>> GetEExpiringItems(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<_E_ExpiringItemsEntity> result = new List<_E_ExpiringItemsEntity>();
            var procedurename = "_CL_Get_E_ExpiringItems";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filter);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<_E_ExpiringItemsEntity> _genericRepository = new GenericMasterRepository<_E_ExpiringItemsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}
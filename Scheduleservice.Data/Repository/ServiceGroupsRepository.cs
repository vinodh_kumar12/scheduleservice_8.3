﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ServiceGroupsRepository: IServiceGroupsRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public ServiceGroupsRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<ServiceGroupsEntity>> GetServiceGroups(int HHA, int UserID, string LOBColumns, string LOBFilter)
        {
            IEnumerable<ServiceGroupsEntity> serviceGroups = new List<ServiceGroupsEntity>();
            var procedurename = "_S_Schedule_GetServiceGroups";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", LOBColumns);
            parameter.Add("@filter", LOBFilter);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<ServiceGroupsEntity> _genericRepository = new GenericMasterRepository<ServiceGroupsEntity>(uow);
                    serviceGroups = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return serviceGroups;
        }
    }
}

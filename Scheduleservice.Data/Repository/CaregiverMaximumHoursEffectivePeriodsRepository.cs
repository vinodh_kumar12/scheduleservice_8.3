﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;


namespace Scheduleservice.Data.Repository
{
    public class CaregiverMaximumHoursEffectivePeriodsRepository : ICaregiverMaximumHoursEffectivePeriods
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregiverMaximumHoursEffectivePeriodsRepository> logger;

        public CaregiverMaximumHoursEffectivePeriodsRepository(IConnectionProvider connectionProvider, ILogger<CaregiverMaximumHoursEffectivePeriodsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<CaregiverMaximumHoursEffectivePeriodsEntity>> GetCaregiverMaximumHoursEffectivePeriodsEntity_Details(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<CaregiverMaximumHoursEffectivePeriodsEntity> details = new List<CaregiverMaximumHoursEffectivePeriodsEntity>();
            var sqlQuery = "_S_Schedule_GetCaregiverMaximumHoursEffectivePeriods";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@filters", Filter);
            parameter.Add("@columns", Columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverMaximumHoursEffectivePeriodsEntity> _genericRepository = new GenericMasterRepository<CaregiverMaximumHoursEffectivePeriodsEntity>(uow);

                    details = await _genericRepository.SelectProcedure(sqlQuery, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return details;
        }
    }
}

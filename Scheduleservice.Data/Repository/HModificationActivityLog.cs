﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HModificationActivityLog : IHModificationActivityLog
    {
        private readonly IConnectionProvider _connectionProvider;

        public HModificationActivityLog(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<int> AddAuditData(int HHA, int UserId, string AuditInfo)
        {
            int result = 0; //1 => failure
            var procedurename = "_S_Schedule_AddAuditInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserId);
            parameter.Add("@AuditInfoJSON", AuditInfo); 

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregiversEntity> _genericRepository = new GenericMasterRepository<CaregiversEntity>(uow);
                    result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                     
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose(); 
                }
            }

            return result;
        }

    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAScheduleConflictExceptionsRepository : IHHAScheduleConflictExceptionsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAScheduleConflictExceptionsRepository> logger;
        public HHAScheduleConflictExceptionsRepository(IConnectionProvider connectionProvider, ILogger<HHAScheduleConflictExceptionsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }
        public async Task<IEnumerable<HHAScheduleConflictExceptionsEntity>> GetHHAScheduleConflictExceptions(int HHA, int UserId)
        {
            IEnumerable<HHAScheduleConflictExceptionsEntity> hhaScheduleConflictExceptions = new List<HHAScheduleConflictExceptionsEntity>();
            string procedurename = "_S_Schedule_GetHHAScheduleConflictExceptions";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId); 

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAScheduleConflictExceptionsEntity> _genericRepository = new GenericMasterRepository<HHAScheduleConflictExceptionsEntity>(uow);
                    hhaScheduleConflictExceptions = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return hhaScheduleConflictExceptions;
        }
    
        public async Task<bool> AddHHAScheduleConflictExceptions(int HHA, int UserID, string Exceptions)
        {
            bool Error = true;
            int ret;

            string procedurename = "_HHA_Schedule_AddScheduleConflictException";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@Exceptions", Exceptions);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return Error;
        }

        public async Task<bool> DeleteHHAScheduleConflictExceptions(int HHA, int UserID, string Exceptions)
        {
            bool Error = true;
            int ret;

            string procedurename = "_HHA_Schedule_DeleteScheduleConflictException";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@Exceptions", Exceptions);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return Error;
        }
    }
}

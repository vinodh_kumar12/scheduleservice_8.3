﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CDSPlanYearServicesRepository : ICDSPlanYearServicesRepository
    { 
         
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CDSPlanYearServicesRepository> _logger;

        public CDSPlanYearServicesRepository( IConnectionProvider connectionProvider,
           ILogger<CDSPlanYearServicesRepository> logger)
        {
            
            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

         

        public async Task< CDSPlanYearServicesEntity> _S_Schedule_GetCDSPlanYearServices(int HHA, int UserID, string columns, string Filters)
        {
              CDSPlanYearServicesEntity cDSPlanYearServicesEntity = new CDSPlanYearServicesEntity();
            string procedurename = "_S_Schedule_GetCDSPlanYearServices";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository< CDSPlanYearServicesEntity> _genericRepository = new GenericMasterRepository<CDSPlanYearServicesEntity>(uow);
                    cDSPlanYearServicesEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSPlanYearServicesEntity;
        }
        public async Task<IEnumerable<CDSPlanYearServicesEntity>> _S_Schedule_GetAllCDSPlanYearServices(int HHA, int UserID, string columns, string Filters)
        {
            IEnumerable<CDSPlanYearServicesEntity> cDSPlanYearServicesEntity = new List<CDSPlanYearServicesEntity>();
            string procedurename = "_S_Schedule_GetCDSPlanYearServices";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSPlanYearServicesEntity> _genericRepository = new GenericMasterRepository<CDSPlanYearServicesEntity>(uow);
                    cDSPlanYearServicesEntity = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSPlanYearServicesEntity;
        }

        public async Task<int> UpdateCDSPlanYearService_bulk(int HHA, int UserID, string columns, string Filters)
        {
            int ret = 0;
            var procedurename = "_S_Schedule_UpdateCDSPlanYearService_bulk";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@CDS_PlanYear_Service_columns", columns);
            parameter.Add("@CDS_PlanYear_Service_filter", Filters);


            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return ret;
        }

    }
}

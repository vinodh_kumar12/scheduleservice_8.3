﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Data.UnitofWorks;
using System;
using System.Collections.Generic; 

namespace Scheduleservice.Data.Repository
{
    public class UAAgencyDatabaseMasterList : IUAAgencyDatabaseMasterList
    {

        private readonly IConnectionProvider _connectionProvider;

        public UAAgencyDatabaseMasterList(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;

        }
        public IEnumerable<UAAgencyDatabaseMasterListEntity> GetAgencyDatabaseList()
        {
            IEnumerable<UAAgencyDatabaseMasterListEntity> agencyDatabaseMasterListEntities = new List<UAAgencyDatabaseMasterListEntity>();
            var procedurename = "_Master_GetHHADatabaseList"; 
            //using Dapper
            using (var uow =  _connectionProvider.ConnectMaster(false))
            {
                try
                {
                    IGenericRepository<UAAgencyDatabaseMasterListEntity> _genericRepository = new GenericMasterRepository<UAAgencyDatabaseMasterListEntity>(uow);
                    agencyDatabaseMasterListEntities =   _genericRepository.SelectProcedure(procedurename, null).Result;

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }           
            return agencyDatabaseMasterListEntities;
        }
    }
}

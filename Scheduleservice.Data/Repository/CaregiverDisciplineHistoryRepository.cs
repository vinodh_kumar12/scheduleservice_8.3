﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
namespace Scheduleservice.Data.Repository
{
    public class CaregiverDisciplineHistoryRepository : ICaregiverDisciplineHistoryRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public CaregiverDisciplineHistoryRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<CaregiverDisciplineHistoryEntity>> GetCaregiverDiscipineHistory(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<CaregiverDisciplineHistoryEntity> caregiverDisciplines = new List<CaregiverDisciplineHistoryEntity>();
            var procedurename = "_S_Schedule_GetClinicianDisciplineHistory";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filters", Filter);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregiverDisciplineHistoryEntity> _genericRepository = new GenericMasterRepository<CaregiverDisciplineHistoryEntity>(uow);
                    caregiverDisciplines = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return caregiverDisciplines;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class ServiceCodesRepository: IServiceCodesRepository
    {
   
        private readonly IConnectionProvider _connectionProvider;

        public ServiceCodesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<ServiceCodesEntity> GetServiceCodeAdditionalInfo(int HHA, int UserID, int SERVICE_CODE_ID, int HHA_BRANCH_ID, string EFFECTIVE_DATE="")
        {
            ServiceCodesEntity result = new ServiceCodesEntity();
            var procedurename = "_S_Schedule_GetServiceCodeAdditionalInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@SERVICE_CODE_ID", SERVICE_CODE_ID);
            parameter.Add("@HHA_BRANCH_ID", HHA_BRANCH_ID);
            parameter.Add("@EFFECTIVE_DATE", EFFECTIVE_DATE);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ServiceCodesEntity> _genericRepository = new GenericMasterRepository<ServiceCodesEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

        public async Task<ServiceCodesEntity> GetServiceCodeBasicInfo(int HHA, int UserID, int SERVICE_CODE_ID)
        {

            ServiceCodesEntity result = new ServiceCodesEntity();
            var procedurename = "_S_Schedule_GetServiceCodeBasicInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@SERVICE_CODE_ID", SERVICE_CODE_ID);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ServiceCodesEntity> _genericRepository = new GenericMasterRepository<ServiceCodesEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

        public async Task<IEnumerable<ServiceCodesEntity>> GetServiceCodes(int HHA, int UserId,string serviceColumns, string serviceFilter)
        {
            IEnumerable<ServiceCodesEntity> result = new List<ServiceCodesEntity>();
            var procedurename = "_S_Schedule_GetServiceCodes";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@Filters", serviceFilter);
            parameter.Add("@Columns", serviceColumns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ServiceCodesEntity> _genericRepository = new GenericMasterRepository<ServiceCodesEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<_UA_FormsEntity>> GetUA_FormsEntities(int HHA, int UserID, string formsColumns, string formsFilter)
        {
            IEnumerable<_UA_FormsEntity> result = new List<_UA_FormsEntity>();
            var procedurename = "_S_Schedule_Get_UA_Forms";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filters", formsFilter);
            parameter.Add("@Columns", formsColumns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<_UA_FormsEntity> _genericRepository = new GenericMasterRepository<_UA_FormsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

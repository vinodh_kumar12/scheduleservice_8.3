﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class EpisodesRepository : IEpisodeRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public EpisodesRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<EpisodeEntity>> _CL_GetClientEpisodes(int HHA, int UserID, int ClientID)
        {
            IEnumerable<EpisodeEntity> result = new List<EpisodeEntity>();
            var procedurename = "_CL_GetClientEpisodes";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@ClientId", ClientID);
            parameter.Add("@POCId", 0);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<EpisodeEntity> _genericRepository = new GenericMasterRepository<EpisodeEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HHALineofbusinnessRepository : IHHALineofbusinessRepository
    {

        private readonly IConnectionProvider _connectionProvider;

        public HHALineofbusinnessRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }


        //IEnumerable<LineofbusinessEntity> EVVLOBS = new List<LineofbusinessEntity>(); var sqlQuery = "select LOB_ID,LOB_NAME from _C_LineOfBusiness Where  _C_LineOfBusiness.HHA = @HHA ";
        //object parameter = new { HHA = HHA };
        //using (var uow = this._connectionProvider.Connect())
        //{
        //    try
        //    {

        //        IGenericRepository<LineofbusinessEntity> _genericRepository = new GenericMasterRepository<LineofbusinessEntity>(uow);
        //        EVVLOBS = await _genericRepository.Select(sqlQuery, parameter).ConfigureAwait(true);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        uow.Dispose();
        //    }
        //}
        public async Task<IEnumerable<LineofbusinessEntity>> GetHHALOBs(int HHA, int UserID, string LOBColumns, string LOBFilter)
        {
            IEnumerable<LineofbusinessEntity> EVVLOBS = new List<LineofbusinessEntity>();
            var procedurename = "_S_Schedule_GetLOBS";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", LOBColumns);
            parameter.Add("@filter", LOBFilter);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<LineofbusinessEntity> _genericRepository = new GenericMasterRepository<LineofbusinessEntity>(uow);
                    EVVLOBS = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return EVVLOBS;
        }
    }
}

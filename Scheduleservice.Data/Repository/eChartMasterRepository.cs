﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class eChartMasterRepository: IeChartMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public eChartMasterRepository(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<eChartMasterEntity>> GetEChartMasterList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<eChartMasterEntity> eChartMasterEntities = new List<eChartMasterEntity>();
            string procedureName = "_S_Schedule_GetEchartMasterList";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Fitlers", Filters);

            using (var uow = _connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<eChartMasterEntity> _genericRepository = new GenericMasterRepository<eChartMasterEntity>(uow);
                    eChartMasterEntities = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return eChartMasterEntities;
        }
    }
}

﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ScheduleShiftRepository : IScheduleShiftRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<ScheduleShiftRepository> _logger;

        public ScheduleShiftRepository(IConnectionProvider connectionProvider,
          ILogger<ScheduleShiftRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<IEnumerable<ScheduleShiftEntity>> _HHA_Schedule_GetScheduleShiftsInfo(int HHA, int UserID, string columns, string Filters)
        {
            IEnumerable<ScheduleShiftEntity> scheduleShiftEntity = new List<ScheduleShiftEntity>();
            string procedurename = "[ReadOnly].[_HHA_Schedule_GetScheduleShiftsInfo]";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ScheduleShiftEntity> _genericRepository = new GenericMasterRepository<ScheduleShiftEntity>(uow);
                    scheduleShiftEntity = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return scheduleShiftEntity;
        }
    }
}

﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class CaregiverTaskEvvReasonsRepository : ICaregiverTaskEVVReasonsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregiverTaskEvvReasonsRepository> logger;

        public CaregiverTaskEvvReasonsRepository(IConnectionProvider connectionProvider, ILogger<CaregiverTaskEvvReasonsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<int> AddCaregiverTaskEVVReasons(int hha, int userid, int cgTaskid, string ReasonCodesJson, string ActionCodeJson)
        {
            int ret = 0;
             var procedurename = "_S_SaveScheduleEVVReasonCode";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@user", userid);
            parameter.Add("@cgtaskID", cgTaskid);
            parameter.Add("@ReasonCodesJson", ReasonCodesJson);
            parameter.Add("@ActionCodeJson", ActionCodeJson); 

            parameter.Add("@CaregiverTaskEvvReasonID", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);


            // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    ret =  parameter.Get<int>("@CaregiverTaskEvvReasonID");

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return ret;
        }


        public async Task<string> _S_GetApproveScheduleValidationErrors(int HHA, int cgtaskid, int UserID,
                bool isTimemodified = false, bool? IsEVVSchedule = null, bool isfromChart = false, int Context = 0)
        {
            string jsonresult = "";

            string procedurename = "_S_GetApproveScheduleValidationErrors";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@CgtaskID", cgtaskid);
            parameter.Add("@CaregiverTaskValidationErrorsjson", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output,size:-1);
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                    jsonresult = parameter.Get<string>("@CaregiverTaskValidationErrorsjson");

                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return jsonresult;
        }

    }

}

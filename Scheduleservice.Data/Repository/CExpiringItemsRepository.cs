﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Scheduleservice.Data.Repository
{
    class CExpiringItemsRepository : ICExpiringItemsRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public CExpiringItemsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<_C_ExpiringItemsEntity>> GetCExpiringItems(int HHA, int UserID, string Columns, string Filter)
        {
            IEnumerable<_C_ExpiringItemsEntity> result = new List<_C_ExpiringItemsEntity>();
            var procedurename = "_CL_Get_C_ExpiringItems";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filter);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<_C_ExpiringItemsEntity> _genericRepository = new GenericMasterRepository<_C_ExpiringItemsEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

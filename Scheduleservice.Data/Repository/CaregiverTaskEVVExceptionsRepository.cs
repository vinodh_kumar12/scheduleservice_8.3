﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{

    public class CaregiverTaskEvvExceptionsRepository : ICaregiverTaskEVVExceptionsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregiverTaskEvvExceptionsRepository> logger;

        public CaregiverTaskEvvExceptionsRepository(IConnectionProvider connectionProvider, ILogger<CaregiverTaskEvvExceptionsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }
        public async Task<bool> AddCaregiverTaskEVVExceptions(int hha, int userid, int cgTaskid, string CaregiverTaskEVVExceptions)
        {
            bool Issuccess = false;
            var procedurename = "_S_SaveScheduleEVVException";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@user", userid);
            parameter.Add("@cgtaskID", cgTaskid);
            parameter.Add("@ExceptionCodesJSON", CaregiverTaskEVVExceptions); 
           // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                      
                    if (ret == 1)
                        Issuccess = true;
                    else if (ret == -5)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }
        public async Task<IEnumerable<CaregiverTaskEVVExceptionsEntity>> GetCaregiverTaskEVVExceptionsList(int hha, int userid, string Cgtaskids, string filters)
        {
            IEnumerable<CaregiverTaskEVVExceptionsEntity> ExceptionList = new List<CaregiverTaskEVVExceptionsEntity>();

            var sqlQuery = "_S_GetDistinctExceptionList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@USER", userid);
           // parameter.Add("@CGTASKIDS", Cgtaskids);
            //parameter.Add("@IsResolved", IsResolved);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskEVVExceptionsEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskEVVExceptionsEntity>(uow);

                    ExceptionList = await _genericRepository.SelectProcedure(sqlQuery, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            } 
            return ExceptionList;
        }
        
        public async Task<bool> ClearCaregiverTaskEVVExceptions(int hha, int userid, int cgTaskid, string filters)
        {
            bool Result = false;

            var procedurename = "_S_DELETEEVVExceptions";

            var parameter = new DynamicParameters();

            parameter.Add("@HHA", hha);
            parameter.Add("@USER", userid);
            parameter.Add("@PAGEID", 0);
            parameter.Add("@CGTASKID", cgTaskid);
            parameter.Add("@CODEIDS", filters);
            parameter.Add("@SystemCode", "");
            //parameter.Add("@AgencyLoginID", clearEVVExceptionsCommand.AgencyLoginID);
            //parameter.Add("@superAdminLoginID", clearEVVExceptionsCommand.SuperAdminLoginID);

           // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                     
                    if (ret == 1)
                        Result = true;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return Result;
        }

        public async Task<IEnumerable<CaregiverTaskEVVExceptionsEntity>> GetCaregiverTaskEVVExceptions(int hha, int userid, string columns, string filters)
        {
            IEnumerable<CaregiverTaskEVVExceptionsEntity> ExceptionList = new List<CaregiverTaskEVVExceptionsEntity>();

            var sqlQuery = "_S_Schedule_GetCaregiverTaskEVVExceptionsList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@USER", userid);
            parameter.Add("@FILTERS", filters);
            parameter.Add("@Columns", columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskEVVExceptionsEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskEVVExceptionsEntity>(uow);

                    ExceptionList = await _genericRepository.SelectProcedure(sqlQuery, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return ExceptionList;
        }

        public async Task<bool> UpdateCaregiverTaskEVVException(int hha, int userid,string CaregiverTaskEvvExceptionIDs, string UpdateColumns="")
        {
            bool Result = false;

            var procedurename = "_S_Schedule_UpdateCaregiverTaskEVVException";

            var parameter = new DynamicParameters();

            parameter.Add("@HHA", hha);
            parameter.Add("@USER", userid);
            parameter.Add("@CaregiverTaskEvvExceptionIDs", CaregiverTaskEvvExceptionIDs);           
            parameter.Add("@UpdateColumns", UpdateColumns);

           // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int ret =  await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                     
                    if (ret == 1)
                        Result = true;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return Result;
        }

        public async Task<IEnumerable<CaregiverTaskValidationErrorsDependentSchedulesEntity>> GetCaregiverTaskValidationErrorsDependentSchedules(int hha, int userid, int cgTaskid)
        {
            IEnumerable<CaregiverTaskValidationErrorsDependentSchedulesEntity> ExceptionList = new List<CaregiverTaskValidationErrorsDependentSchedulesEntity>();

            var sqlQuery = "_S_Schedule_GetCaregiverTaskValidationErrorsDependentSchedules";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@User", userid);
            parameter.Add("@CGTaskID", cgTaskid);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskValidationErrorsDependentSchedulesEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskValidationErrorsDependentSchedulesEntity>(uow);

                    ExceptionList = await _genericRepository.SelectProcedure(sqlQuery, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return ExceptionList;
        }

        public async Task<bool> AddCaregiverTaskEVVExceptions_ValidationErrors(int hha, int userid, string CaregiverTaskEVVExceptions)
        {
            bool Issuccess = false;
            var procedurename = "_S_InsertScheduleValidationErrors";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@user", userid);
            parameter.Add("@ErrorDetails", CaregiverTaskEVVExceptions);
            // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    if (ret == 1)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }
    }   

}

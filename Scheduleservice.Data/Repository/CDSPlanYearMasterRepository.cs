﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CDSPlanYearMasterRepository : ICDSPlanYearMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CDSPlanYearMasterRepository> _logger;

        public CDSPlanYearMasterRepository(IConnectionProvider connectionProvider,
           ILogger<CDSPlanYearMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<IEnumerable<CDSPlanYearMasterEntity>> _S_Schedule_GetAllCDSPlanYearMasters(int HHA, int UserID, string columns, string Filters)
        {
            IEnumerable<CDSPlanYearMasterEntity> cDSPlanYearMasterEntity = new List<CDSPlanYearMasterEntity>();
            string procedurename = "_S_Schedule_GetCDSPlanYearMaster";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSPlanYearMasterEntity> _genericRepository = new GenericMasterRepository<CDSPlanYearMasterEntity>(uow);
                    cDSPlanYearMasterEntity = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSPlanYearMasterEntity;
        }
    }
}

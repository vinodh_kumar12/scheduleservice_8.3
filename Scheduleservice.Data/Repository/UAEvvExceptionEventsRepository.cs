﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class UAEvvExceptionEventsRepository : IUAEvvExceptionEventsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAEvvExceptionEventsRepository> logger;

        public UAEvvExceptionEventsRepository(IConnectionProvider connectionProvider, ILogger<UAEvvExceptionEventsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<_UA_EvvExceptionEventsEntity>> GetEVVVendorEvents(int HHA, int userID, int EvvVendorVersionMasterID)
        {
            IEnumerable<_UA_EvvExceptionEventsEntity> evvVendotEventList = new List<_UA_EvvExceptionEventsEntity>();

            var procedurename = "[ReadOnly]._DB_Schedule_GetEVVVendorEvents";

            var parameter = new DynamicParameters(); 
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_UA_EvvExceptionEventsEntity> _genericRepository = new GenericMasterRepository<_UA_EvvExceptionEventsEntity>(uow);
                    evvVendotEventList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return evvVendotEventList;
        }

        public async Task<IEnumerable<_UA_EvvExceptionEventsEntity>> GetFilteredEVVVendorEvents(int HHA, int userID, string Filters, string Columns)
        {
            IEnumerable<_UA_EvvExceptionEventsEntity> evvVendotEventList = new List<_UA_EvvExceptionEventsEntity>();

            var procedurename = "[ReadOnly].[_DB_Schedule_GetFilteredEVVVendorEvents]";

            var parameter = new DynamicParameters();
            parameter.Add("@Filters", Filters);
            parameter.Add("@Columns", Columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_UA_EvvExceptionEventsEntity> _genericRepository = new GenericMasterRepository<_UA_EvvExceptionEventsEntity>(uow);
                    evvVendotEventList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return evvVendotEventList;
        }
    }
}

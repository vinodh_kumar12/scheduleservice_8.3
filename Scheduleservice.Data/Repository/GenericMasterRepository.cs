﻿using Dapper; 
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic; 
using System.Data; 
using System.Threading.Tasks;
using Scheduleservice.Core.Interfaces.UnitofWorks; 

namespace Scheduleservice.Data.Repository
{
    public class GenericMasterRepository<T> : IGenericRepository<T> 
    {      

        private IUnitOfWork _uow;
        IDbConnection Connection = null;
        IDbTransaction Transaction = null;

        public GenericMasterRepository(IUnitOfWork uow)
        {
            this._uow = uow;
            Connection = this._uow.Connection;
            Transaction = this._uow.Transaction;
        }


        public async Task<int> Execute(string sql, object paramenters)
        {
            return await Connection.ExecuteAsync(sql, paramenters).ConfigureAwait(false);
        }

        public async Task<T> Get(string sql, object paramenters)
        {
            var result = await Connection.QuerySingleAsync<T>(sql, paramenters).ConfigureAwait(false);
            return result;
        }
        public async Task<T> SelectSingle(string sql, object paramenters)
        {
            try
            {
                var result = await Connection.QueryFirstAsync<T>(sql, paramenters).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
         
        public async Task<IEnumerable<T>> Select(string sql, object paramenters)
        {
            try
            {
                var result = await Connection.QueryAsync<T>(sql, paramenters).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<IEnumerable<T>> SelectAll(string sql)
        {
            try
            {
                var result = await Connection.QueryAsync<T>(sql).ConfigureAwait(false);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IEnumerable<T>> SelectProcedure(string procedurename, object paramenters)
        {
            try
            {
                var result = await Connection.QueryAsync<T>(procedurename, paramenters, commandType: CommandType.StoredProcedure).ConfigureAwait(false);
                return result;

            }
            catch (Exception e)
            {

                throw;
            }
        }
        public async Task<T> SelectFirstOrDefaultProcedure(string procedurename, object paramenters)
        {
            try
            {
                var result = await Connection.QueryFirstOrDefaultAsync<T>(procedurename, paramenters, commandType: CommandType.StoredProcedure).ConfigureAwait(false);
                return result;

            }
            catch (Exception e)
            {

                throw;
            }
        }
        public async Task<int> ExecuteProcedure(string procedurename, DynamicParameters parameters)
        {
            try
            {    

                parameters.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);
                
                await Connection.ExecuteAsync(procedurename, parameters, this.Transaction, commandType: CommandType.StoredProcedure).ConfigureAwait(false);

                var result = parameters.Get<int>("@ret"); 

                return result;                
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<T>SelectProcedureSingle(string procedurename, object paramenters)
        {
            try
            {
                var result = await Connection.QueryFirstAsync<T>(procedurename, paramenters, commandType: CommandType.StoredProcedure).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HConfigurations2Repository : IHConfigurations2Repository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HConfigurations2Repository> _logger;

        public HConfigurations2Repository(IConnectionProvider connectionProvider,
           ILogger<HConfigurations2Repository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<HConfigurations2Entity> _S_Schedule_GetHConfiguration2Settings(int HHA, int UserID, string Columns, string Filters)
        {
            HConfigurations2Entity Hconfiguration2Details = new HConfigurations2Entity();
            string procedurename = "_S_Schedule_GetHConfiguration2Settings";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HConfigurations2Entity> _genericRepository = new GenericMasterRepository<HConfigurations2Entity>(uow);
                    Hconfiguration2Details = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Hconfiguration2Details;
        }
    }
}

﻿using CacheManager.Core.Logging;
using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Scheduleservice.Data.Repository
{
    public class CaregiverTaskEvvMissedVisitsReasonsRepository : ICaregiverTaskEvvMissedVisitsReasonsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregiverTaskEvvMissedVisitsReasonsRepository> _logger;
        public CaregiverTaskEvvMissedVisitsReasonsRepository(IConnectionProvider connectionProvider, ILogger<CaregiverTaskEvvMissedVisitsReasonsRepository> logger)
        {
            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<CaregiverTaskEvvMissedVisitsReasonsEntity>> GetMissedVisitReasonCodes(int HHA, int UserId, string Columns, string Filters)
        {

            IEnumerable<CaregiverTaskEvvMissedVisitsReasonsEntity> obj_result = new List<CaregiverTaskEvvMissedVisitsReasonsEntity>();
            var procedurename = "_S_Schedule_GetMissedVisitEVVCodes";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filters);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskEvvMissedVisitsReasonsEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskEvvMissedVisitsReasonsEntity>(uow);
                    obj_result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);


                }
                catch (Exception ex)
                {
                    _logger.LogInformation(ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
                return obj_result;

            }
        }
        public async Task<int> AddMissedVisitReasonCodes(int HHA, int UserId, int CGTASKId, int CaregiverTaskMissedVisitId,string MissedvisitReasonCodeInfo_JSON)
            {

           
            int obj_result = 0;
            var procedurename = "_S_Schedule_AddMissedVisitReasonCodes";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@CgtaskId", CGTASKId);
            parameter.Add("@CaregivertaskMissedVisitID", CaregiverTaskMissedVisitId);
            parameter.Add("@MissedVisitReasonCodeInfo_JSON", MissedvisitReasonCodeInfo_JSON);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    obj_result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                    

                }
                catch (Exception ex)
                {
                    _logger.LogInformation(ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
                return obj_result;

            }
        }
        public async Task<int> UpdateMissedVisitReasonCodes(int HHA, int UserId,string filter,string updatecolumns)
            {

                int obj_result = 0;
                var procedurename = "_S_Schedule_UpdateMissedVisitReasonInfo";
                var parameter = new DynamicParameters();
                parameter.Add("@HHA", HHA);
                parameter.Add("@user", UserId); 
                parameter.Add("@filter", filter);
                parameter.Add("@UpdateColumns", updatecolumns);

                using (var uow = this._connectionProvider.Connect())
                {
                    try
                    {
                        IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    obj_result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);
                        

                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation(ex.Message);
                        throw;
                    }
                    finally
                    {
                        uow.Dispose();

                    }
                    return obj_result;


                }
            }
        }
   }
    


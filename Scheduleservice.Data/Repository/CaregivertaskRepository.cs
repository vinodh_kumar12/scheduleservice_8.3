﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections; 
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using System;
using System.Collections.Generic; 
using System.Threading.Tasks;
using System.Linq;
using System.Data;

namespace Scheduleservice.Data.Repository
{
    public class CaregivertaskRepository : ICaregivertasksRepository
    {

        private readonly IConnectionProvider _connectionProvider;

        public CaregivertaskRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetAllSchedulesList(int HHA, int UserId, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<CaregivertasksEntity> result = new List<CaregivertasksEntity>();
            var procedurename = "_S_Schedule_GetAllSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetScheduleBasicList(int HHA, int UserID, string SchedulesColumns, string scheduleFilters)
        {
            IEnumerable<CaregivertasksEntity> result = new List<CaregivertasksEntity>();
            var procedurename = "_S_Schedule_GetBasicSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFilters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetScheduleAdditionalList(int HHA, int UserId, string SchedulesColumns, string scheduleFilters)
        {
            IEnumerable<CaregivertasksEntity> result = new List<CaregivertasksEntity>();
            var procedurename = "_S_Schedule_GetAdditionalSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFilters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetScheduleAdditional2List(int HHA, int UserId, string SchedulesColumns, string scheduleFilters)
        {
            IEnumerable<CaregivertasksEntity> result = new List<CaregivertasksEntity>();
            var procedurename = "_S_Schedule_GetAdditional2SchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFilters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<int> UpdateScheduleBasic(int HHA, int UserId, int CGTASK_ID, string caregivertaskscolumns)
        {
            int ret = 0;
            var procedurename = "_S_Schedule_ModifyBasicScheduleInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@CGTASK_ID", CGTASK_ID);
            parameter.Add("@columns", caregivertaskscolumns);
            

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                     
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose(); 
                }
            }
            return ret;

        }

        public async Task<int> UpdateScheduleAdditional(int HHA, int UserId, int CGTASK_ID, string caregivertasksAdditionalColumns)
        {
            int ret = 0;

            var procedurename = "_S_Schedule_ModifyAdditionalScheduleInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@CGTASK_ID", CGTASK_ID);
            parameter.Add("@columns", caregivertasksAdditionalColumns);
          //  parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                    
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return ret;
        }

        public async Task<int> UpdateScheduleAdditional2(int HHA, int UserId, string CGTASK_IDs, string caregivertasksAdditional2Columns)
        {
            int ret = 0;
            var procedurename = "_S_Schedule_ModifyAdditional2ScheduleInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@CGTASK_IDs", CGTASK_IDs);
            parameter.Add("@columns", caregivertasksAdditional2Columns);
           // parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);


            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                    //ret = parameter.Get<int>("@ret");
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return ret;
        }

        //Recalculate EVV schedules
        public async Task<string> UpdateRecalculateEVVSchedules(int HHA, int UserId, string CGTaskIDsjson, int context)
        {
            int result = 0;
            var procedurename = "_S_RecalcualteEvvScheduleFlag";
            string EvvCgtaskids = "";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@Cgtaskids", CGTaskIDsjson);
            parameter.Add("@Context", context);
            parameter.Add( "@EvvCgtaskids",value: EvvCgtaskids, direction:System.Data.ParameterDirection.Output,size:8000);
            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                    EvvCgtaskids = parameter.Get<string>("EvvCgtaskids");
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            } 
            return EvvCgtaskids; 
        }


        public async Task<CaregivertasksEntity> GetScheduleBasicInfo(int HHA, int UserId, int CGTask_ID)
        {
            CaregivertasksEntity result = new CaregivertasksEntity();
            var procedurename = "[dbo].[_S_Schedule_ScheduleBasicInfo]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@CGTask_ID", CGTask_ID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;
        }

        public Task<CaregivertasksEntity> GetScheduleAdditionalInfo(int HHA, int UserId, int CGTask_ID, string SchedulesColumns)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetExceptionEligibleSchedulesList(int HHA, int UserId, string Context, int GraceMinutes)
        {
            IEnumerable<CaregivertasksEntity> result = new List<CaregivertasksEntity>();
            var procedurename = "_HHA_Schedule_GetSchedulesToRaiseException";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@UserID", UserId);
            parameter.Add("@Context", Context);
            //parameter.Add("@lastQueueExecutedon", lastQueueExecutedon);
            parameter.Add("@GraceMinutes", GraceMinutes); 

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<bool> GetVisitFrequencyValidation(int HHA, int UserId, int CgtaskID, string CheckinDatetime, string CheckoutDatetime)
        {
            bool Issuccess = false;
            var procedurename = "_S_ValidateVisitFrequency_Schedule";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@userid", UserId);
            parameter.Add("@CgtaskId", CgtaskID);
            parameter.Add("@pageId", 0);
            parameter.Add("@CheckinDateTime", CheckinDatetime);
            parameter.Add("@CheckoutDateTime", CheckoutDatetime);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    int ret = parameter.Get<int>("@ret");

                    if (ret == 1|| ret == -1)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }

        public async Task<int> ReConstructChildSchedules(int HHA, int UserId, string SplitedTaskDetails, bool BlockCheckinValidation)
        {
            int result = 1;
            var procedurename = "_S_ReConstructChildSchedules";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@ModifiedTasks", SplitedTaskDetails);
            parameter.Add("@BlockCheckinValidation", BlockCheckinValidation);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;
        }

        public async Task<int> AddEditDeleteTrivelDocumentTimeMiscVisit(int HHA, int UserId, string ModifyData)
        {
            int result = 1;
            var procedurename = "_S_AddEditDeleteTrivelDocumentTimeMiscVisit";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@ModifyData", ModifyData);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;
        }

        public async Task<int> pChartSaveSubmit(int hha, int User, int Cgtaskid, int pChartSubmit, int pChartApproved, int pageId, string checkInTime, string CheckOutTime, string Miles, string EditedHours, int PlaceOfService, int? clinician)
        {
            int result = 1;
            var procedurename = "_VC_pChartSaveSubmit";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@User", User);
            parameter.Add("@Cgtaskid", Cgtaskid);
            parameter.Add("@pageId", pageId);
            parameter.Add("@pChartSubmit", pChartSubmit);
            parameter.Add("@pChartApproved", pChartApproved);
            parameter.Add("@CheckInTime", checkInTime);
            parameter.Add("@CheckOutTime", CheckOutTime);
            parameter.Add("@Miles", Miles);
            parameter.Add("@EditedHours", EditedHours);
            parameter.Add("@PlaceOfService", PlaceOfService);
            parameter.Add("@clinician", clinician);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    result = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;
        }

        public async Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetChildSchedulesList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<CaregiverTaskChildSchedulesEntity> caregiverTaskChildSchedulesEntity = new List<CaregiverTaskChildSchedulesEntity>();

            var procedureName = "_S_Schedule_GetChildSchedulesList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@Columns", Columns);
            parameter.Add("@Filters", Filters);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskChildSchedulesEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildSchedulesEntity>(uow);
                    var result = await _genericRepository.SelectProcedure(procedureName, parameter).ConfigureAwait(false);

                    if (result.Any())
                    {
                        caregiverTaskChildSchedulesEntity = result.ToList();
                    }

                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return caregiverTaskChildSchedulesEntity;
        }

        //public async Task<int> GetScheduleEVVVendorVersionMasterID(int HHA, int UserID, int CgtaskID)
        //{
        //    int EVVVendorVersionMasterID;
        //    string ScheduleDetailsJson = "[{'CgtaskID':" + CgtaskID + "}]";

        //    var procedurename = "_S_GetVendorversionMasterID";

        //    var parameter = new DynamicParameters();
        //    parameter.Add("@hha", HHA);
        //    parameter.Add("@User", UserID);
        //    parameter.Add("@ScheduleDetailsJson", ScheduleDetailsJson);

        //    //using Dapper
        //    using (var uow = this._connectionProvider.Connect())
        //    {
        //        try
        //        {
        //            IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
        //            EVVVendorVersionMasterID = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

        //        }
        //        catch (Exception ex)
        //        {
        //            throw;
        //        }
        //        finally
        //        {
        //            uow.Dispose();

        //        }
        //    }

        //    return EVVVendorVersionMasterID;

        //}

        public async Task<int> UpdateScheduleInfo(int HHA, int UserId, int CGTASK_ID, string caregivertaskcolumns, string Caregivertaskadditionalcolumns,
                   string caregivertaskadditional2columns)
        {
            int ret = 0;
            string CGTaskID = string.Join(",",CGTASK_ID);
            var procedurename = "_S_Schedule_ModifyBasicScheduleInfo";

            var dictionary = new Dictionary<string, object>
                                {
                                    { "@hha", HHA },
                                    { "@User", UserId },
                                    { "@CGTASK_ID", CGTASK_ID }
                                };

            var dictionaryAdditionalInfo2 = new Dictionary<string, object>
                                {
                                    { "@hha", HHA },
                                    { "@User", UserId },
                                    { "@CGTASK_IDs", CGTaskID }
                                };

            //Caregivertask input params
            var Caregivertaskparams = new DynamicParameters(dictionary);
            Caregivertaskparams.Add("@columns", caregivertaskcolumns);

            //Caregivertaskadditional input params
            var Caregivertaskadditionalparams = new DynamicParameters(dictionary);
            Caregivertaskadditionalparams.Add("@columns", Caregivertaskadditionalcolumns);

            //Caregivertaskadditional2 input params
            var Caregivertaskadditional2params = new DynamicParameters(dictionaryAdditionalInfo2);
            Caregivertaskadditional2params.Add("@columns", caregivertaskadditional2columns);

            using (var uow = this._connectionProvider.Connect())
            {

                try
                {
                    uow.Begin();
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    if (!string.IsNullOrEmpty(caregivertaskcolumns))
                        ret = await _genericRepository.ExecuteProcedure(procedurename, Caregivertaskparams).ConfigureAwait(false);

                    if (ret == 0 && !string.IsNullOrEmpty(Caregivertaskadditionalcolumns))
                    {
                        procedurename = "_S_Schedule_ModifyAdditionalScheduleInfo";
                        ret = await _genericRepository.ExecuteProcedure(procedurename, Caregivertaskadditionalparams).ConfigureAwait(false);
                    }
                     
                    if (ret == 0 && !string.IsNullOrEmpty(caregivertaskadditional2columns))
                    {
                        procedurename = "_S_Schedule_ModifyAdditional2ScheduleInfo";
                        ret = await _genericRepository.ExecuteProcedure(procedurename, Caregivertaskadditional2params).ConfigureAwait(false);
                    }

                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
                finally
                {
                    if (ret != 0)
                        uow.Rollback();
                    else
                        uow.Commit();
                }
            }
            return ret;
        }

        public async Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetAllChildSchedulesList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<CaregiverTaskChildSchedulesEntity> caregiverTaskChildSchedulesEntities = new List<CaregiverTaskChildSchedulesEntity>();

            var procedureName = "_S_Schedule_GetAllChildSchedulesList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", User);
            parameter.Add("@columns", Columns);
            parameter.Add("@filter", Filters);

            using(var uow = this._connectionProvider.Connect(true)){
                try
                {
                    IGenericRepository<CaregiverTaskChildSchedulesEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildSchedulesEntity>(uow);
                    caregiverTaskChildSchedulesEntities = await _genericRepository.SelectProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }

            return caregiverTaskChildSchedulesEntities;
        }

        public async Task<int> CreateSchedule(int hha, int user, string schedule_json)
        {
            int ret=0;
            int cgtaskid_out = 0;
            var procedureName = "_s_schedule_createschedules";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@userid", user);
            parameter.Add("@schedule_json", schedule_json);
            parameter.Add("@CGTASK_ID", value: cgtaskid_out, direction: System.Data.ParameterDirection.Output);
            using (var uow = this._connectionProvider.Connect(false))
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);
                    cgtaskid_out = parameter.Get<int>("CGTASK_ID");

                }
                catch (Exception e)
                {
                    throw;
                }
            }

            return cgtaskid_out;
        }

        public async Task<int> CreateChildSchedules(int hha, int user, string schedule_json,bool IsSplitForBilling,bool IsSplitForPayroll)
        {
            int ret = 0;
            var procedureName = "_s_schedule_createchildschedules";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", hha);
            parameter.Add("@userid", user);
            parameter.Add("@schedule_json", schedule_json);
            parameter.Add("@IsSplitForBilling", IsSplitForBilling);
            parameter.Add("@IsSplitForPayroll", IsSplitForPayroll);

            using (var uow = this._connectionProvider.Connect(false))
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return ret;
        }

        public async Task<int> UpdateScheduleInfo_Subscriber(int HHA, int UserId, int CGTASK_ID, string caregivertaskcolumns, string Caregivertaskadditionalcolumns,
                   string caregivertaskadditional2columns)
        {
            int ret = 0;
            var procedureName = "_S_SubscribedRecords_Modify";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@cgtaskid", CGTASK_ID);
            parameter.Add("@caregivertaskcolumns", caregivertaskcolumns);
            parameter.Add("@caregivertaskadditionalcolumns", Caregivertaskadditionalcolumns);
            parameter.Add("@caregivertaskadditional2columns", caregivertaskadditional2columns);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return ret;
        }

        public async Task<IEnumerable<CaregivertasksEntity>> GetEvvTerminalPayerSchedulesToRegister(int HHA, int User, string Columns, string Filters, string client_payer_json)
        {
            IEnumerable<CaregivertasksEntity> caregivertasksEntity = new List<CaregivertasksEntity>();

            string procedureName = "_S_Schedule_GetEvvTerminalPayerSchedulesToRegister";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Filters", Filters);
            parameters.Add("@client_payer_json", client_payer_json);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregivertasksEntity> _genericRepository = new GenericMasterRepository<CaregivertasksEntity>(uow);
                    var res = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);

                    if (res.Any())
                    {
                        caregivertasksEntity = res.ToList();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return caregivertasksEntity;
        }

        public async Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetEvvTerminalPayerChildSchedulesToRegister(int HHA, int User, string Columns, string Filters, string client_payer_json)
        {
            IEnumerable<CaregiverTaskChildSchedulesEntity> caregiverTaskChildSchedulesEntity = new List<CaregiverTaskChildSchedulesEntity>();

            string procedureName = "_S_Schedule_GetEvvTerminalPayerChildSchedulesToRegister";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@Columns", Columns);
            parameters.Add("@Filters", Filters);
            parameters.Add("@client_payer_json", client_payer_json);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<CaregiverTaskChildSchedulesEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildSchedulesEntity>(uow);
                    var res = await _genericRepository.SelectProcedure(procedureName, parameters).ConfigureAwait(false);

                    if (res.Any())
                    {
                        caregiverTaskChildSchedulesEntity = res.ToList();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return caregiverTaskChildSchedulesEntity;
        }
        public async Task<bool> GetCreateScheduleVisitFrequencyValidation(int HHA, int UserId, int ClientID, int Payerid, DataTable SchedulesTable,int Context)
        {
            bool Issuccess = false;
            var procedurename = "[ReadOnly].[_S_ValidateVisitFrequency_CreateSchedule]";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@Client", ClientID);
            parameter.Add("@Payer", Payerid);

            parameter.Add("@pageId", 0);
            parameter.Add("@Context", Context);
            parameter.Add("@newSchedulesTable", SchedulesTable.AsTableValuedParameter("[dbo].[tempVisitFrequencyschedules]"));

            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(true);

                    int ret = parameter.Get<int>("@ret");

                    if (ret == 1)
                        Issuccess = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return Issuccess;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class ClientTreatmentAddressRepository : IClientTreatmentAddressRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<ClientTreatmentAddressRepository> _logger;


        public ClientTreatmentAddressRepository(IConnectionProvider connectionProvider,
           ILogger<ClientTreatmentAddressRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<ClientTreatmentAddressEntity> _S_Schedule_GetClientTreatmentAddress(int HHA, int UserID, int clientTreatmentLocationID)
        {
            ClientTreatmentAddressEntity ClientTreatmentAdrress = new ClientTreatmentAddressEntity();
            string procedurename = "_S_Schedule_GetClientTreatmentAddress";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@ClientTreatmentAddressID", clientTreatmentLocationID);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientTreatmentAddressEntity> _genericRepository = new GenericMasterRepository<ClientTreatmentAddressEntity>(uow);
                    var ClientTreatmentAdrressinfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    ClientTreatmentAdrress = ClientTreatmentAdrressinfo.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ClientTreatmentAdrress;
        }


        public async Task<ClientTreatmentAddressEntity> _S_Schedule_GetClientTreatmentAddressInfo(int HHA, int UserID, string columns, string Filters)
        {
            ClientTreatmentAddressEntity ClientTreatmentAdrress = new ClientTreatmentAddressEntity();
            string procedurename = "[ReadOnly].[_S_Schedule_GetClientTreatmentAddressInfo]";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientTreatmentAddressEntity> _genericRepository = new GenericMasterRepository<ClientTreatmentAddressEntity>(uow);
                    var ClientTreatmentAdrressinfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    ClientTreatmentAdrress = ClientTreatmentAdrressinfo.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ClientTreatmentAdrress;
        }
    }
}

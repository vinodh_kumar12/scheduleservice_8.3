﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class CaregiverTasksTerminalPayerRepository: ICaregiverTasksTerminalPayerRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public CaregiverTasksTerminalPayerRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<int> InsertCaregiverTasksTerminalPayer(int HHA, int User, string Columns, string ValuesJson)
        {
            int ret;
            var procedureName = "_S_InsertCaregiverTasksTerminalPayer";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@InsertColumns", Columns);
            parameter.Add("@InsertValuesJson", ValuesJson);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);

                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }

        public async Task<int> InsertCaregiverTaskChildScheduleTerminalPayer(int HHA, int User, string Columns, string ValuesJson)
        {
            int ret;
            var procedureName = "_S_InsertCaregiverTaskChildScheduleTerminalPayer";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@InsertColumns", Columns);
            parameter.Add("@InsertValuesJson", ValuesJson);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);

                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }

        public async Task<IEnumerable<CaregiverTasksTerminalPayerEntity>> GetAllTerminalPayerSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<CaregiverTasksTerminalPayerEntity> result = new List<CaregiverTasksTerminalPayerEntity>();
            var procedurename = "_S_Schedule_GetAllTerminalPayerSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTasksTerminalPayerEntity> _genericRepository = new GenericMasterRepository<CaregiverTasksTerminalPayerEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregiverTasksTerminalPayerEntity>> GetTerminalPayerSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<CaregiverTasksTerminalPayerEntity> result = new List<CaregiverTasksTerminalPayerEntity>();
            var procedurename = "_S_Schedule_GetTerminalPayerSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTasksTerminalPayerEntity> _genericRepository = new GenericMasterRepository<CaregiverTasksTerminalPayerEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity>> GetAllTerminalPayerChildSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity> result = new List<CaregiverTaskChildScheduleTerminalPayerEntity>();
            var procedurename = "_S_Schedule_GetAllTerminalPayerChildSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskChildScheduleTerminalPayerEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildScheduleTerminalPayerEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity>> GetTerminalPayerChildSchedules(int HHA, int UserId, string SchedulesColumns, string scheduleFiltersJson)
        {
            IEnumerable<CaregiverTaskChildScheduleTerminalPayerEntity> result = new List<CaregiverTaskChildScheduleTerminalPayerEntity>();
            var procedurename = "_S_Schedule_GetTerminalPayerChildSchedulesList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskChildScheduleTerminalPayerEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildScheduleTerminalPayerEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<int> DeleteCaregiverTasksTerminalPayer(int HHA, int User, string ValuesJson)
        {
            int ret;
            var procedureName = "_S_Schedule_DeleteCaregiverTasksTerminalPayer";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User); 
            parameter.Add("@Filters", ValuesJson);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);

                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }

        public async Task<int> DeleteCaregiverTaskChildScheduleTerminalPayer(int HHA, int User, string ValuesJson)
        {
            int ret;
            var procedureName = "_S_DeleteCaregiverTaskChildScheduleTerminalPayer";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User); 
            parameter.Add("@Filters", ValuesJson);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);

                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }

        public async Task<int> UpdateTerminalPayerSchedules(int HHA, int UserId, string jsonData)
        {
            int ret;
            var procedureaName = "_S_Schedule_UpdateTerminalPayerSchedules";
            var paramerter = new DynamicParameters();
            paramerter.Add("@HHA", HHA);
            paramerter.Add("@User", UserId);
            paramerter.Add("@jsonData", jsonData);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureaName, paramerter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

                return ret;
        }

        public async Task<int> UpdateTerminalPayerChildSchedules(int HHA, int UserId, string jsonData)
        {
            int ret;
            var procedureaName = "_S_Schedule_UpdateTerminalPayerChildSchedules";
            var paramerter = new DynamicParameters();
            paramerter.Add("@HHA", HHA);
            paramerter.Add("@User", UserId);
            paramerter.Add("@jsonData", jsonData);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureaName, paramerter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }
    }
}

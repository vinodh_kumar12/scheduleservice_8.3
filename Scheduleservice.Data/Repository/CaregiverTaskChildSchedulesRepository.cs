﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CaregiverTaskChildSchedulesRepository : ICaregiverTaskChildSchedulesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CaregiverTaskChildSchedulesRepository> _logger;

        public CaregiverTaskChildSchedulesRepository(IConnectionProvider connectionProvider,
           ILogger<CaregiverTaskChildSchedulesRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<CaregiverTaskChildSchedulesEntity>> GetAllChildSchedulesList(int HHA, int UserID, string SchedulesColumns, string scheduleFilters, bool requireallSplits)
        {
            IEnumerable<CaregiverTaskChildSchedulesEntity> ChildScheduleList = new List<CaregiverTaskChildSchedulesEntity>();
            if (!requireallSplits)
            {
                scheduleFilters += "AND isnull(CaregiverTaskChildSchedules.IsDeleted,0)=0";
            }

            string procedurename = "_S_Schedule_GetAllChildSchedulesList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFilters);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CaregiverTaskChildSchedulesEntity> _genericRepository = new GenericMasterRepository<CaregiverTaskChildSchedulesEntity>(uow);
                    ChildScheduleList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ChildScheduleList;
        }
    }
}

﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CDSPayerServiceCategoryMasterRepository : ICDSPayerServiceCategoryMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CDSPayerServiceCategoryMasterRepository> _logger;

        public CDSPayerServiceCategoryMasterRepository(IConnectionProvider connectionProvider,
           ILogger<CDSPayerServiceCategoryMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<IEnumerable< CDSPayerServiceCategoryMasterEntity>> _S_Schedule_CDSPayerServiceCategoryMaster(int HHA, int UserID, string columns, string Filters)
        {
            IEnumerable < CDSPayerServiceCategoryMasterEntity> cDSPayerServiceCategory=
                        new List< CDSPayerServiceCategoryMasterEntity>() ;
            string procedurename = "_S_Schedule_CDSPayerServiceCategoryMaster";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSPayerServiceCategoryMasterEntity> _genericRepository = 
                        new GenericMasterRepository<CDSPayerServiceCategoryMasterEntity>(uow);
                    cDSPayerServiceCategory = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSPayerServiceCategory;
        }
    }
}

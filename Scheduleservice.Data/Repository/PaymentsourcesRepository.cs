﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class PaymentsourcesRepository : IPaymentsourcesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public PaymentsourcesRepository(IConnectionProvider connectionProvider)
        {

            this._connectionProvider = connectionProvider;
        }

        public async Task<PaymentsourcesEntity> GetPaymentSourceAdditionalInfo(int hha, int userid, int Payment_Source_ID, int HHA_BRANCH_ID)
        { 
            PaymentsourcesEntity result = new PaymentsourcesEntity();
            var procedurename = "_S_Schedule_GetPaymentSourceAdditionalInfo";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@User", userid);
            parameter.Add("@Payment_Source_ID", Payment_Source_ID);
            parameter.Add("@HHA_BRANCH_ID", HHA_BRANCH_ID);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<PaymentsourcesEntity> _genericRepository = new GenericMasterRepository<PaymentsourcesEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }

        public async Task<PaymentsourcesEntity> GetPaymentSourceBasicInfo(int hha, int userid, int Payment_Source_ID)
        {

            PaymentsourcesEntity result = new PaymentsourcesEntity();
            var procedurename = "_S_Schedule_GetPaymentSourceBasicInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@User", userid);
            parameter.Add("@Payment_Source_ID", Payment_Source_ID); 


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<PaymentsourcesEntity> _genericRepository = new GenericMasterRepository<PaymentsourcesEntity>(uow);
                    result = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }

        public async Task<IEnumerable<PaymentsourcesEntity>> GetPaymentsources(int hha, int userid, string payerColumns, string payerFilters)
        {
            IEnumerable<PaymentsourcesEntity> result = new List<PaymentsourcesEntity>();
            var procedurename = "_S_Schedule_GetPaymentSources";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", hha);
            parameter.Add("@User", userid);
            parameter.Add("@Filters", payerFilters);
            parameter.Add("@Columns", payerColumns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<PaymentsourcesEntity> _genericRepository = new GenericMasterRepository<PaymentsourcesEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }
            return result;
        }

    }
}

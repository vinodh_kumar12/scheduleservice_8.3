﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class HHAConfigurationsRepository : IHHAConfigurationsRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public HHAConfigurationsRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<_M_ConfigurationsEntity> Get_M_Configurations(int HHA, int UserId, string serviceColumns, string serviceFilter)
        {
            _M_ConfigurationsEntity result = new _M_ConfigurationsEntity();
            var procedurename = "_UA_GetMasterConfigurations";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@Filters", serviceFilter);
            parameter.Add("@Columns", serviceColumns);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_M_ConfigurationsEntity> _genericRepository = new GenericMasterRepository<_M_ConfigurationsEntity>(uow);
                    var resultList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                    if(resultList.Any())
                    {
                        result = resultList.ToList()[0];
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

        public async Task<bool> UpdateUAConfigurations(int HHA, int UserId, string UpdateColumns, string UpdateCondition)
        {
            bool ret = false;

            var procedureName = "_UA_UpdateMasterConfigurations";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserId);
            parameter.Add("@UpdateColumns", UpdateColumns);
            parameter.Add("@UpdateCondition", UpdateCondition);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    int returnCode = await _genericRepository.ExecuteProcedure(procedureName, parameter).ConfigureAwait(false);

                    if(returnCode == 1)
                    {
                        ret = true;
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return ret;
        }
    }
}

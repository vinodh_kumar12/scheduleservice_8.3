﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class UsersRepository : IUsersRepository
    {

        private readonly IConnectionProvider _connectionProvider;

        public UsersRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<UsersEntity>> GetHHAUsers(int HHA, string Columns, string filters)
        {
            IEnumerable<UsersEntity> result = new List<UsersEntity>();
            var procedurename = "_HHA_Schedule_GetUsers";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@Columns", Columns);
            parameter.Add("@filters", filters);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<UsersEntity> _genericRepository = new GenericMasterRepository<UsersEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose(); 
                }
            }

            return result;
        }
        
        public async Task<IEnumerable<UsersBranchListEntity>> GetUsersBranchList(int HHA, int user, string Columns, string filters)
        {
            IEnumerable<UsersBranchListEntity> result = new List<UsersBranchListEntity>();
            var procedurename = "_HHA_Schedule_GetUsersBranchList";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user", user);
            parameter.Add("@Columns", Columns);
            parameter.Add("@filters", filters);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<UsersBranchListEntity> _genericRepository = new GenericMasterRepository<UsersBranchListEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return result;
        }

    }
}

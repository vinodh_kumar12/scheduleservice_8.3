﻿using Dapper;
using Newtonsoft.Json;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class EvvScheduleEditCodesMasterRepository : IEvvScheduleEditCodesMasterRepository
    {

        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<EvvScheduleEditCodesMasterRepository> _logger;

        public EvvScheduleEditCodesMasterRepository(IConnectionProvider connectionProvider,
            ILogger<EvvScheduleEditCodesMasterRepository> logger)
        { 
            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<EvvScheduleEditCodesMasterEntity>> GetHHAEVVCodes(int HHA, int userID,string Context, int EvvVendorVersionID)
        {
            IEnumerable<EvvScheduleEditCodesMasterEntity> evvCodeList = new List<EvvScheduleEditCodesMasterEntity>();

            var procedurename = "_S_GetEvvReasonCodeorActionCodeLists";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", userID);
            parameter.Add("@Context", Context);
            parameter.Add("@EvvVendorVersionID", EvvVendorVersionID);
            parameter.Add("@CodeList", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size:-1);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<EvvScheduleEditCodesMasterEntity> _genericRepository = new GenericMasterRepository<EvvScheduleEditCodesMasterEntity>(uow);
                    await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);
                    string Codes = parameter.Get<string>("@CodeList");

                    if(!string.IsNullOrEmpty(Codes))
                        evvCodeList= JsonConvert.DeserializeObject<IEnumerable<EvvScheduleEditCodesMasterEntity>>(Codes);

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            } 

            return evvCodeList; 
        }

        public async Task<HHAAggregatorEVVCodeResponseEntity> _HHA_Schedule_AddNewEVVCode(int HHA, int UserID, int EVVVendorVersionMasterID, string CodeType, string Code, string Desc, bool EnableNotesPrefill)
        {
            HHAAggregatorEVVCodeResponseEntity addNewEVVCodeEntity = new HHAAggregatorEVVCodeResponseEntity();
            string procedurename = "_HHA_Schedule_AddNewEVVCode";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@EVVVendorVersionMasterID", EVVVendorVersionMasterID);
            parameter.Add("@CodeType", CodeType);
            parameter.Add("@Code", Code);
            parameter.Add("@Desc", Desc);
            parameter.Add("@IsEnableNotesPrefill", EnableNotesPrefill);

            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<HHAAggregatorEVVCodeResponseEntity> _genericRepository = new GenericMasterRepository<HHAAggregatorEVVCodeResponseEntity>(uow);
                    addNewEVVCodeEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);

                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return addNewEVVCodeEntity;
        }

        public async Task<HHAAggregatorEVVCodeResponseEntity> _HHA_Schedule_UpdateEVVCode(int HHA, int UserID, int EVVScheduleEditCodeMasterID, string ModifiedFields)
        {
            HHAAggregatorEVVCodeResponseEntity addNewEVVCodeEntity = new HHAAggregatorEVVCodeResponseEntity();
            string procedurename = "_HHA_Schedule_UpdateEVVCode";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@userID", UserID);
            parameter.Add("@EVVScheduleEditCodeMasterID", EVVScheduleEditCodeMasterID);
            parameter.Add("@ModifiedData", ModifiedFields);
            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<HHAAggregatorEVVCodeResponseEntity> _genericRepository = new GenericMasterRepository<HHAAggregatorEVVCodeResponseEntity>(uow);
                    addNewEVVCodeEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return addNewEVVCodeEntity;
        }

    }
}

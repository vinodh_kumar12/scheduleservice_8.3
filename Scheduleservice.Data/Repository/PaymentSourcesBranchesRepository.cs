﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections; 
using Scheduleservice.Data.Repository;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Scheduleservice.Data.Repository
{
    public class PaymentSourcesBranchesRepository : IPaymentSourcesBranchesRepository
    {
         private readonly IConnectionProvider _connectionProvider;

        public PaymentSourcesBranchesRepository(IConnectionProvider connectionProvider)
        { 
            this._connectionProvider = connectionProvider;
        }

        public async Task<IEnumerable<PaymentSourcesBranchesEntity>> GetHHAPayerBranches(int HHA, int UserID, IEnumerable<int> Payers)
        {
            IEnumerable<PaymentSourcesBranchesEntity> EVVPayerBranches = new List<PaymentSourcesBranchesEntity>();

            try
            {
                var sqlQuery = "";

                if (Payers.Count() > 2100)
                {
                    sqlQuery = "select HHA, Branch_ID, PaymentSource_ID,EvvAggregatorVendorVersionMasterID, isnull(EnableEvv, 0) EnableEvv  from PaymentSourcesBranches With(Nolock) " +
                                        "Where PaymentSourcesBranches.HHA = @HHA ";
                }
                else
                {
                    sqlQuery = "select HHA, Branch_ID, PaymentSource_ID,EvvAggregatorVendorVersionMasterID, isnull(EnableEvv, 0) EnableEvv  from PaymentSourcesBranches With(Nolock) " +
                                        "Where PaymentSourcesBranches.HHA = @HHA and PaymentSource_ID IN @Payers ";
                }

                object parameter = new { HHA = HHA, Payers = Payers};
                

                //using Dapper
                using (var uow = this._connectionProvider.Connect())
                {
                    IGenericRepository<PaymentSourcesBranchesEntity> _genericRepository = new GenericMasterRepository<PaymentSourcesBranchesEntity>(uow);
                    EVVPayerBranches = await _genericRepository.Select(sqlQuery, parameter).ConfigureAwait(true);
                } 
            }
            catch (Exception ex)
            { 
                throw;
            }

            return EVVPayerBranches;
        }

        public async Task<PaymentSourcesBranchesEntity> GetPayerBranchInfo(int HHA, int UserID, int PayerID, int HHABranchID, string columns)
        {
            PaymentSourcesBranchesEntity paymentSourcesBranchesEntity = new PaymentSourcesBranchesEntity();

            var procedurename = "_S_Schedule_GetPaymentBranchInfo";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Payment_Source_ID", PayerID);
            parameter.Add("@HHA_BRANCH_ID", HHABranchID);
            parameter.Add("@columns", columns);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<PaymentSourcesBranchesEntity> _genericRepository = new GenericMasterRepository<PaymentSourcesBranchesEntity>(uow);
                    paymentSourcesBranchesEntity = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return paymentSourcesBranchesEntity;

        }

        public async Task<IEnumerable<PaymentSourcesBranchesEntity>> GetAllPayerBranchesInfo(int HHA, int UserID, string PayerColumns, string PayerFilters)
        {
            IEnumerable<PaymentSourcesBranchesEntity> result = new List<PaymentSourcesBranchesEntity>();
            var procedurename = "_S_Schedule_GetAllPayerBranchesInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filters", PayerFilters);
            parameter.Add("@Columns", PayerColumns);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<PaymentSourcesBranchesEntity> _genericRepository = new GenericMasterRepository<PaymentSourcesBranchesEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return result;
        }
    }
}

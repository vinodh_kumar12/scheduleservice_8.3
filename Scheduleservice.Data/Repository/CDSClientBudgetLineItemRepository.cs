﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class CDSClientBudgetLineItemRepository : ICDSClientBudgetLineItemRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<CDSClientBudgetLineItemRepository> _logger;

        public CDSClientBudgetLineItemRepository(IConnectionProvider connectionProvider,
           ILogger<CDSClientBudgetLineItemRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            _logger = logger;
        }

        public async Task<CDSClientBudgetLineItemEntity> _S_Schedule_GetCDSClientBudgetLineItem(int HHA, int UserID, string columns, string Filters)
        {
            CDSClientBudgetLineItemEntity cDSClientBudgetLineItem = new CDSClientBudgetLineItemEntity();
            string procedurename = "_S_Schedule_GetCDSClientBudgetLineItem";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSClientBudgetLineItemEntity> _genericRepository = new GenericMasterRepository<CDSClientBudgetLineItemEntity>(uow);
                     cDSClientBudgetLineItem = await _genericRepository.SelectFirstOrDefaultProcedure(procedurename, parameter).ConfigureAwait(true);
                
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return cDSClientBudgetLineItem;
        }

        public async Task<IEnumerable<CDSClientBudgetLineItemEntity>> _S_Schedule_CDSClientBudgetLineItems(int HHA, int UserId, string columns, string Filters)
        {
            IEnumerable<CDSClientBudgetLineItemEntity> result = new List<CDSClientBudgetLineItemEntity>();
            var procedurename = "_S_Schedule_GetCDSClientBudgetLineItem";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserId);
            parameter.Add("@columns", columns);
            parameter.Add("@filter", Filters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<CDSClientBudgetLineItemEntity> _genericRepository = new GenericMasterRepository<CDSClientBudgetLineItemEntity>(uow);
                    result = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return result;
        }
    }
}

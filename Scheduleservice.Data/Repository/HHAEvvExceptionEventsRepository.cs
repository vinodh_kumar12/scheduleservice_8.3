﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAEvvExceptionEventsRepository : IHHAEvvExceptionEventsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAEvvExceptionEventsRepository> logger;

        public HHAEvvExceptionEventsRepository(IConnectionProvider connectionProvider, ILogger<HHAEvvExceptionEventsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<_C_EvvExceptionEventsEntity>> GetHHAEVVEvents(int HHA, int userID, int EvvVendorVersionMasterID)
        {
            IEnumerable<_C_EvvExceptionEventsEntity> evvEventList = new List<_C_EvvExceptionEventsEntity>();

            var procedurename = "[ReadOnly]._HHA_Schedule_GetHHAEVVVendorEvents";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", userID);
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_C_EvvExceptionEventsEntity> _genericRepository = new GenericMasterRepository<_C_EvvExceptionEventsEntity>(uow);
                    evvEventList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose(); 
                }
            }

            return evvEventList;
        }

        public async Task<bool> UpdateEVVEventInfo(int HHA, int userID, string filters,string columns)
        {
            int  ret = 0;
            bool ret_bool = false;

            var procedurename = "_HHA_Schedule_UpdateEVVeventInfo";

            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", userID);
            parameter.Add("@filters", filters);
            parameter.Add("@columns", columns);
          //  parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);


            //using Dapper
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);
                     
                    if (ret == 0)
                        ret_bool = true; 
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret_bool;
        }

        public async Task<bool> AddEVVExceptionEvents(int HHA, int UserID, int EvvVendorVersionMasterID, string eventcodes, string exception_code)
        {
            bool Error = true;
            int ret;

            var procedurename = "_HHA_Schedule_AddHHAEventinfo";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@EvvVendorVersionMasterID", EvvVendorVersionMasterID);
            parameter.Add("@events", eventcodes);
            parameter.Add("@exception_code", exception_code);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception e)
                {
                    logger.LogError(e,e.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Error;
        }

        public async Task<bool> DeleteEVVExceptionEvents(int HHA, int UserID, string EvvExceptionEventIDs)
        {
            bool Error = true;
            int ret;

            var procedurename = "_HHA_Schedule_DeleteHHAEventinfo";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@EvvExceptionEventIDs", EvvExceptionEventIDs);
            parameter.Add("@ret", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.ReturnValue);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> _genericRepository = new GenericMasterRepository<int>(uow);
                    await _genericRepository.ExecuteProcedure(procedurename, parameter).ConfigureAwait(false);

                    ret = parameter.Get<int>("@ret");

                    if (ret == 0)
                        Error = false;
                }
                catch (Exception e)
                {
                    logger.LogError(e,e.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return Error;
        }

        public async Task<IEnumerable<HHAEvvVendorExceptionEventsEntity>> GetHHAEvvVendorExceptionEventCodes(int HHA, int UserID, int EvvVendorVersionMasterID)
        {
            IEnumerable<HHAEvvVendorExceptionEventsEntity> evvEventList = new List<HHAEvvVendorExceptionEventsEntity>();

            var procedurename = "[ReadOnly].[_HHA_Schedule_GetHHAEvvVendorExceptionEventCodes]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@AggregatorID", EvvVendorVersionMasterID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEvvVendorExceptionEventsEntity> _genericRepository = new GenericMasterRepository<HHAEvvVendorExceptionEventsEntity>(uow);
                    evvEventList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return evvEventList;

        }

        public async Task<IEnumerable<HHAEvvVendorExceptionEventsEntity>> GetHHAEvvVendorExceptionEvents(int HHA, int UserID, int EvvVendorVersionMasterID)
        {
            IEnumerable<HHAEvvVendorExceptionEventsEntity> evvEventList = new List<HHAEvvVendorExceptionEventsEntity>();

            var procedurename = "[ReadOnly].[_HHA_Schedule_GetHHAEvvVendorExceptionEvents]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@AggregatorID", EvvVendorVersionMasterID);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEvvVendorExceptionEventsEntity> _genericRepository = new GenericMasterRepository<HHAEvvVendorExceptionEventsEntity>(uow);
                    evvEventList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return evvEventList;

        }
    }
}

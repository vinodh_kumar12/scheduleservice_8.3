﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class ClientEvvTerminalPayersRepository: IClientEvvTerminalPayersRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        public ClientEvvTerminalPayersRepository(IConnectionProvider connectionProvider)
        {
            this._connectionProvider = connectionProvider;
        }
        public async Task<IEnumerable<ClientEvvTerminalPayersEntity>> GetClientTerminalPayersList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<ClientEvvTerminalPayersEntity> clientEvvTerminalPayersEntities = new List<ClientEvvTerminalPayersEntity>();

            var procedureName = "_S_Schedule_GetClientTerminalPayersList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@Columns", Columns);
            parameter.Add("@Filters", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientEvvTerminalPayersEntity> _genericRepository = new GenericMasterRepository<ClientEvvTerminalPayersEntity>(uow);
                    clientEvvTerminalPayersEntities = await _genericRepository.SelectProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }

            return clientEvvTerminalPayersEntities;
        }


        public async Task<IEnumerable<ClientEvvTerminalPayersEntity>> GetAllClientTerminalPayersList(int HHA, int User, string Columns, string Filters)
        {
            IEnumerable<ClientEvvTerminalPayersEntity> clientEvvTerminalPayersEntities = new List<ClientEvvTerminalPayersEntity>();

            var procedureName = "_S_Schedule_GetAllClientTerminalPayersList";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", User);
            parameter.Add("@Columns", Columns);
            parameter.Add("@Filters", Filters);

            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<ClientEvvTerminalPayersEntity> _genericRepository = new GenericMasterRepository<ClientEvvTerminalPayersEntity>(uow);
                    clientEvvTerminalPayersEntities = await _genericRepository.SelectProcedure(procedureName, parameter).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
            }

            return clientEvvTerminalPayersEntities;
        }

        public async Task<int> UpdateClientEvvTerminalPayers(int HHA, int User, string json_data)
        {
            int ret;
            string procedureName = "_S_Schedule_UpdateClientEVVTerminalPayers";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@HHA", HHA);
            parameters.Add("@User", User);
            parameters.Add("@json_data", json_data);

            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<int> genericRepository = new GenericMasterRepository<int>(uow);
                    ret = await genericRepository.ExecuteProcedure(procedureName, parameters).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return ret;
        }
    }
}

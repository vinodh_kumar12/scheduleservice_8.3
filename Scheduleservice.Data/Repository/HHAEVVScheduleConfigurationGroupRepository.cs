﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAEVVScheduleConfigurationGroupRepository : IHHAEVVScheduleConfigurationGroupRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAEVVScheduleConfigurationGroupRepository> _logger;


        public HHAEVVScheduleConfigurationGroupRepository(IConnectionProvider connectionProvider,
           ILogger<HHAEVVScheduleConfigurationGroupRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }


        public async Task<HHAEVVScheduleConfigurationGroupEntity> GetHHAEVVScheduleConfigurationGroupSettings(int HHA, int UserID, string SchedulesColumns, string scheduleFiltersJson)
        {
            HHAEVVScheduleConfigurationGroupEntity EVVScheduleConfigurationGroup = new HHAEVVScheduleConfigurationGroupEntity();
            string procedurename = "_HHA_Schedule_GetHHAEVVScheduleConfigurationGroupSettings";
            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@user", UserID);
            parameter.Add("@columns", SchedulesColumns);
            parameter.Add("@filter", scheduleFiltersJson);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEVVScheduleConfigurationGroupEntity> _genericRepository = new GenericMasterRepository<HHAEVVScheduleConfigurationGroupEntity>(uow);
                    EVVScheduleConfigurationGroup = await _genericRepository.SelectProcedureSingle(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVScheduleConfigurationGroup;
        }
    }
}

﻿using Dapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Interfaces.DBConnections;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class UAScheduleConflictExceptionsRepository : IUAScheduleConflictExceptionsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<UAScheduleConflictExceptionsRepository> logger;

        public UAScheduleConflictExceptionsRepository(IConnectionProvider connectionProvider, ILogger<UAScheduleConflictExceptionsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<_UA_ScheduleConflictExceptionsEntity>> GetScheduleConflictExceptions(int HHA, int UserID)
        {
            IEnumerable<_UA_ScheduleConflictExceptionsEntity> scheduleConflictExceptionsEntity = new List<_UA_ScheduleConflictExceptionsEntity>();

            var procedurename = "[ReadOnly]._S_Schedule_GetScheduleConflictExceptionsMasterList";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@User", UserID);
            using (var uow = this._connectionProvider.Connect())
            {
                try
                {
                    IGenericRepository<_UA_ScheduleConflictExceptionsEntity> _genericRepository = new GenericMasterRepository<_UA_ScheduleConflictExceptionsEntity>(uow);
                    scheduleConflictExceptionsEntity = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }
            return scheduleConflictExceptionsEntity;
        }

        public async Task<IEnumerable<_UA_ScheduleConflictExceptionsEntity>> GetFilteredScheduleConflictExceptions(int HHA, int UserID, string Filters, string Columns)
        {
            IEnumerable<_UA_ScheduleConflictExceptionsEntity> scheduleConflictExceptionsEntity = new List<_UA_ScheduleConflictExceptionsEntity>();

            var procedurename = "[ReadOnly].[_DB_Schedule_GetScheduleConflictExceptions]";

            var parameter = new DynamicParameters();
            parameter.Add("@HHA", HHA);
            parameter.Add("@UserID", UserID);
            parameter.Add("@Filters", Filters);
            parameter.Add("@Columns", Columns);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<_UA_ScheduleConflictExceptionsEntity> _genericRepository = new GenericMasterRepository<_UA_ScheduleConflictExceptionsEntity>(uow);
                    scheduleConflictExceptionsEntity = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(false);

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return scheduleConflictExceptionsEntity;
        }
    }
}

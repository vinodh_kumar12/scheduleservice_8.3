﻿using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Data.Repository
{
    public class HHAEvvVendorVersionErrorCodeMasterRepository: IHHAEvvVendorVersionErrorCodeMasterRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HHAEvvVendorVersionErrorCodeMasterRepository> _logger;

        public HHAEvvVendorVersionErrorCodeMasterRepository(IConnectionProvider connectionProvider,
            ILogger<HHAEvvVendorVersionErrorCodeMasterRepository> logger)
        {

            this._connectionProvider = connectionProvider;
            this._logger = logger;
        }

        public async Task<IEnumerable<HHAEvvVendorVersionErrorCodeMasterEntity>> _HHA_Schedule_GetHHAEVVVendorVersionErrorCodeTypes(int HHA, int UserID, int EVVVendorVersionMasterID)
        {
            IEnumerable<HHAEvvVendorVersionErrorCodeMasterEntity> EVVVendorVersionErrorCodeTypeList = new List<HHAEvvVendorVersionErrorCodeMasterEntity>();
            string procedurename = "_HHA_Schedule_GetHHAEVVVendorVersionErrorCodeTypes";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@user",UserID);
            parameter.Add("@EVVVendorVersionMasterID", EVVVendorVersionMasterID);
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HHAEvvVendorVersionErrorCodeMasterEntity> _genericRepository = new GenericMasterRepository<HHAEvvVendorVersionErrorCodeMasterEntity>(uow);
                    EVVVendorVersionErrorCodeTypeList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return EVVVendorVersionErrorCodeTypeList;
        }
    }
}

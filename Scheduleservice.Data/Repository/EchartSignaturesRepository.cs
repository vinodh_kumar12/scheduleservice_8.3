﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class EchartSignaturesRepository : IEchartSignaturesRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<EchartSignaturesEntity> logger;

        public EchartSignaturesRepository(IConnectionProvider connectionProvider, ILogger<EchartSignaturesEntity> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<EchartSignaturesEntity> GetScheduleSignatureInfo(int HHA, int UserID, string filters, string columns)
        {
            EchartSignaturesEntity echartSignaturesEntity = new EchartSignaturesEntity();

            string procedurename = "_S_Schedule_GeteChartSignatureInfo";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@Filters", filters);
            parameter.Add("@columns", columns);

            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<EchartSignaturesEntity> _genericRepository = new GenericMasterRepository<EchartSignaturesEntity>(uow);
                    var Signaturesinfo = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    echartSignaturesEntity = Signaturesinfo.FirstOrDefault();

                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                    throw;
                }
                finally
                {
                    uow.Dispose();
                }
            }

            return echartSignaturesEntity;
        }
    }
}

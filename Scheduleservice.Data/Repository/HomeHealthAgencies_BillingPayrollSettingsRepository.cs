﻿using Dapper;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.DBConnections;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository
{
    public class HomeHealthAgencies_BillingPayrollSettingsRepository : IHomeHealthAgencies_BillingPayrollSettingsRepository
    {
        private readonly IConnectionProvider _connectionProvider;
        private readonly ILogger<HomeHealthAgencies_BillingPayrollSettingsRepository> logger;

        public HomeHealthAgencies_BillingPayrollSettingsRepository(IConnectionProvider connectionProvider, ILogger<HomeHealthAgencies_BillingPayrollSettingsRepository> _logger)
        {
            this._connectionProvider = connectionProvider;
            this.logger = _logger;
        }

        public async Task<IEnumerable<HomeHealthAgencies_BillingPayrollSettingsEntity>> GetBillingPayrollSettingsDetails(int HHA, int UserID, string column, string filters)
        {
            IEnumerable<HomeHealthAgencies_BillingPayrollSettingsEntity> eVVExceptionSettings = null;
            string procedurename = "_HHA_Get_HHABillingPayrollSetting";
            var parameter = new DynamicParameters();
            parameter.Add("@hha", HHA);
            parameter.Add("@User", UserID);
            parameter.Add("@columns", column);
            parameter.Add("@filters", filters);


            //using Dapper
            using (var uow = this._connectionProvider.Connect(true))
            {
                try
                {
                    IGenericRepository<HomeHealthAgencies_BillingPayrollSettingsEntity> _genericRepository = new GenericMasterRepository<HomeHealthAgencies_BillingPayrollSettingsEntity>(uow);
                    var _eVVExceptionSettingsList = await _genericRepository.SelectProcedure(procedurename, parameter).ConfigureAwait(true);
                    eVVExceptionSettings = _eVVExceptionSettingsList.ToList();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex,ex.Message);
                    throw ex;
                }
                finally
                {
                    uow.Dispose();

                }
            }

            return eVVExceptionSettings;
        }
    }
}

﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Scheduleservice.Core.Entities;
using Scheduleservice.Data.Models;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyCaching.Core;

namespace Scheduleservice.Data.Repository.CacheRepository
{
    public class CachedAgencyMasterListRepository : IAgencyMasterListRepository
    {
        private readonly IAgencyMasterListRepository _agencyMasterListRepository;
        private IEasyCachingProvider _easyCachingProvider;
        private IEasyCachingProviderFactory _easyCachingProviderFactory;
        private readonly IOptions<RedisConnectionDetails> _redisConnectionDetailsoptions;
        public CachedAgencyMasterListRepository(IEasyCachingProviderFactory easyCachingProviderFactory, IAgencyMasterListRepository agencyMasterListRepository, IOptions<RedisConnectionDetails> redisConnectionDetailsoptions)
        {
            _agencyMasterListRepository = agencyMasterListRepository;
            _easyCachingProviderFactory = easyCachingProviderFactory;
            _redisConnectionDetailsoptions = redisConnectionDetailsoptions;
            _easyCachingProvider = _easyCachingProviderFactory.GetCachingProvider(_redisConnectionDetailsoptions.Value.Name);
        }

        public async Task<AgencyListEntity> GetAgency(int HHAID)
        {
            var agency = new AgencyListEntity();
            try
            {
                var agencyMasterList = await GetallAgencyList();

                agency = agencyMasterList.Where(x => x.HHA_ID == HHAID).FirstOrDefault();

            }
            catch (Exception ex)
            {

                throw;
            }

            return agency;
        }

        

        public async Task<IEnumerable<AgencyListEntity>> GetallAgencyList()
        {
            var AgencyList = new List<AgencyListEntity>();
            try
            {
                if (_easyCachingProvider.Get<string>(_redisConnectionDetailsoptions.Value.Name + "_AgencyMaster").Value != null)
                {
                    var AgencyMasterArrayObject = _easyCachingProvider.Get<string>(_redisConnectionDetailsoptions.Value.Name + "_AgencyMaster").Value;

                    AgencyList = JsonConvert.DeserializeObject<List<AgencyListEntity>>(AgencyMasterArrayObject);
                }

                if (AgencyList != null && AgencyList.Count > 0)
                {
                    return AgencyList;
                }
                var AgencyListResult = await _agencyMasterListRepository.GetallAgencyList();
                AgencyList = AgencyListResult.ToList();
                this._easyCachingProvider.Set(_redisConnectionDetailsoptions.Value.Name + "_AgencyMaster", JsonConvert.SerializeObject(AgencyList), TimeSpan.FromDays(1));
            }
            catch (Exception ex)
            {

                throw;
            }



            return AgencyList;
        }
    }
}

﻿using EasyCaching.Core;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Scheduleservice.Core.Entities;
using Scheduleservice.Data.Models;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Data.Repository.CacheRepository
{
    public class CachedDatabaselistRepository : IDatabaselistRepository
    {
        private IEasyCachingProvider _easyCachingProvider;
        private IEasyCachingProviderFactory _easyCachingProviderFactory;
        private IDatabaselistRepository _databaselistRepository;
        private readonly IOptions<RedisConnectionDetails> _redisConnectionDetailsoptions;
        public CachedDatabaselistRepository(IEasyCachingProviderFactory easyCachingProviderFactory, IDatabaselistRepository databaselistRepository, IOptions<RedisConnectionDetails> redisConnectionDetailsoptions)
        {
            _databaselistRepository = databaselistRepository;
            _easyCachingProviderFactory = easyCachingProviderFactory;
            _redisConnectionDetailsoptions = redisConnectionDetailsoptions;
            _easyCachingProvider = _easyCachingProviderFactory.GetCachingProvider(_redisConnectionDetailsoptions.Value.Name);
        }

        public async Task<IEnumerable<DatabaselistEntity>> GetAllDataBaseList()
        {
            var DatabaseList = new List<DatabaselistEntity>();
            try
            {
                if (_easyCachingProvider.Get<string>(_redisConnectionDetailsoptions.Value.Name + "_DataBaseMasterList").Value != null)
                {
                    var AgencyMasterArrayObject = _easyCachingProvider.Get<string>(_redisConnectionDetailsoptions.Value.Name + "_DataBaseMasterList").Value;

                    DatabaseList = JsonConvert.DeserializeObject<List<DatabaselistEntity>>(AgencyMasterArrayObject);
                }

                if (DatabaseList != null && DatabaseList.Count > 0)
                {
                    return DatabaseList;
                }
                var AgencyListResult = await _databaselistRepository.GetAllDataBaseList();
                DatabaseList = AgencyListResult.ToList();
                this._easyCachingProvider.Set(_redisConnectionDetailsoptions.Value.Name + "_DataBaseMasterList", JsonConvert.SerializeObject(DatabaseList), TimeSpan.FromDays(1));
            }
            catch (Exception ex)
            {

                throw;
            }
            return DatabaseList;
        }
    }
}

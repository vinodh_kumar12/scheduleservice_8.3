﻿using AutoMapper;
using KanCache.Interfaces;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Scheduleservice.Core.DTO.Application;
using Scheduleservice.Core.Common.Helpers;

namespace Scheduleservice.Data.Repository.CacheRepository
{
    public class CacheUAAgencyDatabaseMasterList : IUAAgencyDatabaseMasterList
    {
        private readonly ICacheController  _cacheController;
        private readonly IUAAgencyDatabaseMasterList _agencyDatabaseMasterList;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _accessor;
        public ActionContextDto ActionContext { get; }
        public CacheUAAgencyDatabaseMasterList(IServiceProvider serviceProvider, ICacheController cacheController, IMapper mapper, IUAAgencyDatabaseMasterList agencyDatabaseMasterList)
        {
            _cacheController = cacheController;
            _mapper = mapper;
            _agencyDatabaseMasterList = agencyDatabaseMasterList;
            _accessor = serviceProvider.GetService<IHttpContextAccessor>();
            if (_accessor.HttpContext != null)
                ActionContext = (ActionContextDto)_accessor.HttpContext.Items[ConstantKeys.ActionContext];

            _cacheController.InstanceCode = ActionContext.InstanceCode;
        }

      

        public IEnumerable<UAAgencyDatabaseMasterListEntity> GetAgencyDatabaseList()
        {
            List<UAAgencyDatabaseMasterListEntity> agencyDatabaseMasterListEntities = new List<UAAgencyDatabaseMasterListEntity>();
           
            //var cacheAgencyDatabaseList = _cacheController.GetAgencyDatabaseList();
            //if (cacheAgencyDatabaseList != null && cacheAgencyDatabaseList.Count() > 0)
            //{
            //    agencyDatabaseMasterListEntities = _mapper.Map<List<UAAgencyDatabaseMasterListEntity>>(cacheAgencyDatabaseList);

            //    return agencyDatabaseMasterListEntities;
            //}
            agencyDatabaseMasterListEntities =   _agencyDatabaseMasterList.GetAgencyDatabaseList().ToList();
            return agencyDatabaseMasterListEntities;
        }
    }
}

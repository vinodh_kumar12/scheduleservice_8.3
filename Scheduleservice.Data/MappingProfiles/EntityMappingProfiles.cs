﻿using AutoMapper;
using KanCache.Models;
using Scheduleservice.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Data.MappingProfiles
{
    public class EntityMappingProfiles:Profile
    {
        public EntityMappingProfiles()
        {
            CreateMap<HHADatabaseDetail, UAAgencyDatabaseMasterListEntity>();
        }
    }
}

﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Services.Configuration.MasterLibrary.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.MasterLibrary.MappingProfiles
{
    public class MasterLibraryMappingProfiles:Profile
    {
        public MasterLibraryMappingProfiles()
        {
            CreateMap<_UA_ScheduleConflictExceptionsEntity, ExceptionsMasterlistDto>();
        }
    }
}

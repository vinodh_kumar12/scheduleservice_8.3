﻿using AutoMapper;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.MasterLibrary.DTO;
using Scheduleservice.Services.Configuration.MasterLibrary.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.MasterLibrary.QueryHandler
{
    public class ExceptionsMasterlistQueryHandler : IHandlerWrapper<ExceptionsMasterlistQuery, IEnumerable<ExceptionsMasterlistDto>>
    {
        private readonly IUAScheduleConflictExceptionsRepository _uA_ScheduleConflictExceptionsRepository;
        private readonly IMapper _mapper;
        public ExceptionsMasterlistQueryHandler(IUAScheduleConflictExceptionsRepository uA_ScheduleConflictExceptionsRepository, IMapper mapper)
        {
            _mapper = mapper;
            _uA_ScheduleConflictExceptionsRepository = uA_ScheduleConflictExceptionsRepository;
        }

        public async Task<Response<IEnumerable<ExceptionsMasterlistDto>>> Handle(ExceptionsMasterlistQuery request, CancellationToken cancellationToken)
        {
            var ExceptionmasterlistEntiy = await _uA_ScheduleConflictExceptionsRepository.GetScheduleConflictExceptions(request.HHA, request.UserId);
            var exceptionsMasterlistDto = _mapper.Map<IEnumerable<ExceptionsMasterlistDto>>(ExceptionmasterlistEntiy);
            return Response.Ok<IEnumerable<ExceptionsMasterlistDto>>(exceptionsMasterlistDto); ;

        } 
    }
}

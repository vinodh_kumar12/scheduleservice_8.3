﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.MasterLibrary.DTO
{
    public class ExceptionsMasterlistDto
    {
        public int exception_id { get; set; }
        public int exception_code { get; set; }
        public string exception_type { get; set; }
        public string exception_message { get; set; }
        public string events { get; set; }
        public bool can_resolve { get; set; }
        public bool comments_mandatory { get; set; }
    }
}

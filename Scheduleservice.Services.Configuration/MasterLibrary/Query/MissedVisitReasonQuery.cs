﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.MasterLibrary.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.MasterLibrary.Query
{
   public class MissedVisitReasonQuery:BaseRequest, IRequestWrapper<IEnumerable<MissedVisitReasonDto>>
    {
    }
}

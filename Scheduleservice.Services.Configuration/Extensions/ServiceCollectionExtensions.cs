﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using Scheduleservice.Core.Common.Behaviors;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;


namespace Scheduleservice.Services.Configuration.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServicesConfigServiceCollection(this IServiceCollection services)
        {
         
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddScoped<IEvvConfigurations, EvvConfigurations>();
            return services;
        }
    }
}

﻿using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts
{
    public interface IScheduleConfigurations
    {
        Task<AgencyScheduleConfigurationsDto> GetScheduleAgencyConfigurations(int HHA, int UserId, int PAGEID);
        Task<AgencyBranchScheduleConfigurationsDto> GetScheduleAgencyBranchConfigurations(int HHA, int UserId, int PAGEID, int HHA_BRANCH_ID);
    }
}

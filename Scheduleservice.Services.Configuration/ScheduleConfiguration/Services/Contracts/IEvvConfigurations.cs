﻿using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts
{
    public interface IEvvConfigurations
    {
        Task<IEnumerable<EvvConfigurationDto>> GetEvvConfigurations(int HHA, int UserId);
        Task<bool> SaveEvvConfigurations(int HHA, int UserId, string EvvConfigurationProperties);
        Task<IEnumerable<EvvExceptionEventsDto>> GetEvvExceptionEventCodes(int HHA, int UserId, int AggregatorID);
        Task<bool> AddExceptionEvent(int HHA, int UserId, int AggregatorID, string Events, string exception_code);
        Task<bool> UpdateExceptionEvent(int HHA, int UserId, int AggregatorID, string EvvExceptionEventIDs, string exception_code);
        Task<bool> DeleteExceptionEvents(int HHA, int UserId, string EvvExceptionEvents);
        Task<IEnumerable<EvvExceptionEventsDto>> GetEvvExceptionEvents(int HHA, int UserId, int AggregatorID);
    }
}

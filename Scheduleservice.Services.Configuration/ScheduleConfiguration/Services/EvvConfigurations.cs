﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Services
{
    public class EvvConfigurations : IEvvConfigurations
    {
        private readonly IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository;
        private readonly IRepositoryUpdateColumn repositoryUpdateColumn;
        private readonly IMapper mapper;
        private readonly ILogger<EvvConfigurations> logger;
        private readonly IHHAEvvExceptionEventsRepository hHAEvvExceptionEventsRepository;
        private readonly IUAScheduleConflictExceptionsRepository uAScheduleConflictExceptionsRepository;
        private readonly IHHAScheduleConflictExceptionsRepository hHAScheduleConflictExceptionsRepository;
        private readonly IRepositoryFilterConditions repositoryFilterConditions;
        private readonly IUAEvvExceptionEventsRepository uAEvvExceptionEventsRepository;
        private readonly IHHAEvvExceptionEventCodesRepository hHAEvvExceptionEventCodesRepository;

        public EvvConfigurations(IHHAScheduleAuthSettingsRepository hHAScheduleAuthSettingsRepository, IRepositoryUpdateColumn _repositoryUpdateColumn, IMapper _mapper,
            ILogger<EvvConfigurations> _logger, IHHAEvvExceptionEventsRepository _hHAEvvExceptionEventsRepository,
            IUAScheduleConflictExceptionsRepository _uAScheduleConflictExceptionsRepository,
            IHHAScheduleConflictExceptionsRepository _hHAScheduleConflictExceptionsRepository, IRepositoryFilterConditions _repositoryFilterConditions,
            IUAEvvExceptionEventsRepository _uAEvvExceptionEventsRepository, IHHAEvvExceptionEventCodesRepository _hHAEvvExceptionEventCodesRepository)
        {
            this.hHAScheduleAuthSettingsRepository = hHAScheduleAuthSettingsRepository;
            this.repositoryUpdateColumn = _repositoryUpdateColumn;
            this.mapper = _mapper;
            this.logger = _logger;
            this.hHAEvvExceptionEventsRepository = _hHAEvvExceptionEventsRepository;
            this.uAScheduleConflictExceptionsRepository = _uAScheduleConflictExceptionsRepository;
            this.hHAScheduleConflictExceptionsRepository = _hHAScheduleConflictExceptionsRepository;
            this.repositoryFilterConditions = _repositoryFilterConditions;
            this.uAEvvExceptionEventsRepository = _uAEvvExceptionEventsRepository;
            this.hHAEvvExceptionEventCodesRepository = _hHAEvvExceptionEventCodesRepository;
        }
        public async Task<IEnumerable<EvvConfigurationDto>> GetEvvConfigurations(int HHA, int UserId)
        {
            IEnumerable<EvvConfigurationDto> evvconfiginfo = new List<EvvConfigurationDto>();
            try
            {
                var hHAScheduleAuthSettingsEntity = await hHAScheduleAuthSettingsRepository.GetHHAScheduleAuthSettingsDetails(HHA, UserId);

                evvconfiginfo = mapper.Map<IEnumerable<EvvConfigurationDto>>(hHAScheduleAuthSettingsEntity);
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
                throw e;
            }

            return evvconfiginfo;
        }

        public async Task<bool> SaveEvvConfigurations(int HHA, int UserId, string EvvConfigurationProperties)
        {
            var Error = true;
            try
            {
                var evvConfigurationDto = JsonConvert.DeserializeObject<Dictionary<string, string>>(EvvConfigurationProperties);
                var Columns = "";

                foreach (var item in evvConfigurationDto)
                {
                   Columns = repositoryUpdateColumn.UpdateColumn(item.Key, item.Value, FilterConditionDataTypeEnums.integerType).Build();
                }

                Error = !(await hHAScheduleAuthSettingsRepository.SaveHHAScheduleAuthSettingsDetails(HHA, UserId, Columns));
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
            }
            return Error;
        }
        public async Task<IEnumerable<EvvExceptionEventsDto>> GetEvvExceptionEventCodes(int HHA, int UserId, int AggregatorID)
        {
            IEnumerable<EvvExceptionEventsDto> evvExceptionEventsDto = new List<EvvExceptionEventsDto>();
            try
            {
                var hhaEvvVendorExceptionEventsEntity = await hHAEvvExceptionEventsRepository.GetHHAEvvVendorExceptionEventCodes(HHA, UserId, AggregatorID);
                evvExceptionEventsDto = mapper.Map<IEnumerable<EvvExceptionEventsDto>>(hhaEvvVendorExceptionEventsEntity);
            }
            catch (Exception e)
            {
                throw e;
            }

            return evvExceptionEventsDto;
        }
        public async Task<bool> AddExceptionEvent(int HHA, int UserId, int AggregatorID, string Events, string exception_code)
        {
            bool Error = true;
            string Message = "";
            try
            {
                var hhaEvvExceptionEventsEntity = await hHAEvvExceptionEventsRepository.GetHHAEVVEvents(HHA, UserId, AggregatorID);
                List<EvvExceptionEventsDto> eventsToInsert = JsonConvert.DeserializeObject<List<EvvExceptionEventsDto>>(Events);

                var finalEvents =
                    JsonConvert.SerializeObject(
                        from x in eventsToInsert
                        //where !hhaEvvExceptionEventsEntity.Any(y => y.Event == x.event_code)
                        select new { event_code = x.event_code }
                    );

                if (finalEvents.Length >= 1)
                {
                    Error = await hHAEvvExceptionEventsRepository.AddEVVExceptionEvents(HHA, UserId, AggregatorID, finalEvents, exception_code);

                    if(!Error)
                    {
                        Error = await hHAEvvExceptionEventCodesRepository.AddEVVExceptionEventCodes(HHA, UserId, AggregatorID, finalEvents, exception_code);
                    }

                    if (!Error)
                    {
                        var Filters = "  WHERE exception_type IN (SELECT event_code FROM OPENJSON('" + finalEvents + "') WITH(event_code VARCHAR(30) '$.event_code'))";
                        var Columns = " exception_id, exception_type, exception_message, can_resolve, comments_mandatory ";
                        var ScheduleConflictExceptions = await uAScheduleConflictExceptionsRepository.GetFilteredScheduleConflictExceptions(HHA, UserId, Filters, Columns);

                        var Exceptions = JsonConvert.SerializeObject(ScheduleConflictExceptions);

                        Error = await hHAScheduleConflictExceptionsRepository.AddHHAScheduleConflictExceptions(HHA, UserId, Exceptions);

                        if (Error)
                            Message = "Failed to add schedule conflict exception";
                    }
                }
                else
                    Error = false;

                if (Error)
                {
                    Message = "Failed to add EVV exception event";
                }
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
            }

            return Error;
        }
        public async Task<bool> UpdateExceptionEvent(int HHA, int UserId, int AggregatorID, string EvvExceptionEventIDs, string exception_code)
        {
            var Error = true;
            try
            {
                List<EvvExceptionEventsDto> eventIds = JsonConvert.DeserializeObject<List<EvvExceptionEventsDto>>(EvvExceptionEventIDs);
                var CommaSeparatedIDs = string.Join(',', eventIds.Select(x => x.EvvExceptionEventID).ToArray());

                var Filters =
                    repositoryFilterConditions.InFilter("EvvExceptionEventID", CommaSeparatedIDs, FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("EVVVendorVersionMasterID", AggregatorID, FilterConditionDataTypeEnums.integerType)
                        .EqualFilter("Code", eventIds[0].exception_code, FilterConditionDataTypeEnums.stringType)
                        .Build();

                var Columns =
                    repositoryUpdateColumn.UpdateColumn("Code", exception_code, FilterConditionDataTypeEnums.stringType)
                        .Build();

                Error = !(await hHAEvvExceptionEventsRepository.UpdateEVVEventInfo(HHA, UserId, Filters, Columns));
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
            }

            return Error;
        }
        public async Task<bool> DeleteExceptionEvents(int HHA, int UserId, string EvvExceptionEvents)
        {
            var Error = true;
            var Message = "";
            try
            {
                List<EvvExceptionEventsDto> eventIds = JsonConvert.DeserializeObject<List<EvvExceptionEventsDto>>(EvvExceptionEvents);
                var ExceptionEventCodeIDs = JsonConvert.SerializeObject(from x in eventIds select new { ExceptionEventCodeID = x.ExceptionEventCodeID });
                if (!string.IsNullOrEmpty(ExceptionEventCodeIDs))
                {
                    Error = await hHAEvvExceptionEventCodesRepository.DeleteEVVExceptionEventCodes(HHA, UserId, ExceptionEventCodeIDs);
                }

                if (!Error)
                {
                    var EvvExceptionEventIDs = JsonConvert.SerializeObject(from x in eventIds select new { EvvExceptionEventID = x.EvvExceptionEventID });
                    if (!string.IsNullOrEmpty(EvvExceptionEventIDs))
                    {
                        Error = await hHAEvvExceptionEventsRepository.DeleteEVVExceptionEvents(HHA, UserId, EvvExceptionEventIDs);
                    }
                }

                if (Error)
                {
                    Message = "Failed to delete EVV exception event";
                }
                else
                {
                    string Filters = "";
                    string Columns = "";
                    // Get Events which are unmapped and not mapped to for any other aggragator exception and not mapped to any aggregator
                    Filters = " WHERE event IN (SELECT event_code FROM OPENJSON('" + EvvExceptionEvents + "') WITH(event_code VARCHAR(30) '$.event_code'))";
                    Columns = " TOP 1 event ";
                    var vendorEvvExceptionEvents = await uAEvvExceptionEventsRepository.GetFilteredEVVVendorEvents(HHA, UserId, Filters, Columns);

                    var DeletedEvvExceptionEvents = JsonConvert.DeserializeObject<List<EvvExceptionEventsDto>>(EvvExceptionEvents);
                    var hhaEvvExceptionEvents = await hHAEvvExceptionEventsRepository.GetHHAEVVEvents(HHA, UserId, 0);
                    var EventsToDelete = JsonConvert.SerializeObject(from x in DeletedEvvExceptionEvents
                                                                     where !hhaEvvExceptionEvents.Any(s => (s.Event == x.event_code))
                                                                         && !vendorEvvExceptionEvents.Any(s => (s.Event == x.event_code))
                                                                     select new { event_code = x.event_code }
                                        );
                    if (EventsToDelete != "[]")
                    {
                        //Get exception_id for all the exception_types from master table which need to delete from _C_ScheduleConflictExceptions
                        Filters = "  WHERE exception_type IN (SELECT event_code FROM OPENJSON('" + EventsToDelete + "') WITH(event_code VARCHAR(30) '$.event_code'))";
                        Columns = " exception_id ";
                        var ScheduleConflictExceptions = await uAScheduleConflictExceptionsRepository.GetFilteredScheduleConflictExceptions(HHA, UserId, Filters, Columns);

                        // Get hha_exception_id for all exception_ids from _C_ScheduleConflictExceptions table
                        var HHAScheduleConflictExceptions = await hHAScheduleConflictExceptionsRepository.GetHHAScheduleConflictExceptions(HHA, UserId);
                        var Exceptions = JsonConvert.SerializeObject(from x in HHAScheduleConflictExceptions
                                                                     where ScheduleConflictExceptions.Any(s => (s.exception_id == x.exception_id))
                                                                     select new { hha_exception_id = x.hha_exception_id });

                        // Delete exceptions using hha_exception_id
                        Error = await hHAScheduleConflictExceptionsRepository.DeleteHHAScheduleConflictExceptions(HHA, UserId, Exceptions);

                        if (Error)
                            Message = "Failed to delete schedule conflict exception";
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
            }

            return Error;
        }

        public async Task<IEnumerable<EvvExceptionEventsDto>> GetEvvExceptionEvents(int HHA, int UserId, int AggregatorID)
        {
            IEnumerable<EvvExceptionEventsDto> evvExceptionEventsDto = new List<EvvExceptionEventsDto>();
            try
            {
                var hhaEvvVendorExceptionEventsEntity = await hHAEvvExceptionEventsRepository.GetHHAEvvVendorExceptionEvents(HHA, UserId, AggregatorID);
                evvExceptionEventsDto = mapper.Map<IEnumerable<EvvExceptionEventsDto>>(hhaEvvVendorExceptionEventsEntity);
            }
            catch (Exception e)
            {
                throw e;
            }

            return evvExceptionEventsDto;
        }
    }
}

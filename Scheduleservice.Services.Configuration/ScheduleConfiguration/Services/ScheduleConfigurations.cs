﻿using AutoMapper;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Services
{
    public class ScheduleConfigurations : IScheduleConfigurations
    {
        private readonly IHHAScheduleConfigurationsRepository _hhhaScheduleConfigurationsRepository;
        private readonly IHHABranchScheduleConfigurationsRepository _hhhaBranchScheduleConfigurationsRepository;
        private readonly IMapper _mapper;
        public ScheduleConfigurations(IHHAScheduleConfigurationsRepository hhhaScheduleConfigurationsRepository,
            IHHABranchScheduleConfigurationsRepository hhhaBranchScheduleConfigurationsRepository,
            IMapper mapper)
        {
            _hhhaScheduleConfigurationsRepository = hhhaScheduleConfigurationsRepository;
            _hhhaBranchScheduleConfigurationsRepository = hhhaBranchScheduleConfigurationsRepository;
            _mapper = mapper;
        }

        public async Task<AgencyBranchScheduleConfigurationsDto> GetScheduleAgencyBranchConfigurations(int HHA, int UserId, int PAGEID, int HHA_BRANCH_ID)
        {
            var _hhhaBranchScheduleConfigurations = await _hhhaBranchScheduleConfigurationsRepository.GetHHABranchScheduleConfigurations(HHA, UserId, HHA_BRANCH_ID);
            var agencyBranchScheduleConfigurationsDto = _mapper.Map<AgencyBranchScheduleConfigurationsDto>(_hhhaBranchScheduleConfigurations);
            return agencyBranchScheduleConfigurationsDto;
        }
         
        public async Task<AgencyScheduleConfigurationsDto> GetScheduleAgencyConfigurations(int HHA, int UserId, int PAGEID)
        {
            var _hhhaScheduleConfigurations = await _hhhaScheduleConfigurationsRepository.GetHHAScheduleConfigurations(HHA, UserId);
            var gencyScheduleConfigurationsDto = _mapper.Map<AgencyScheduleConfigurationsDto>(_hhhaScheduleConfigurations);
            return gencyScheduleConfigurationsDto;
        }
         
    }
}

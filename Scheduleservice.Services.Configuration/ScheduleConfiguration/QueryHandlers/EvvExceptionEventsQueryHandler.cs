﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class EvvExceptionEventsQueryHandler: IHandlerWrapper<EvvExceptionEventsQuery, IEnumerable<EvvExceptionEventsDto>>
    {
        private readonly IEvvConfigurations evvConfigurations;
        private readonly ILogger<EvvExceptionEventsQueryHandler> logger;

        public EvvExceptionEventsQueryHandler(IEvvConfigurations _evvConfigurations, ILogger<EvvExceptionEventsQueryHandler> _logger)
        {
            this.evvConfigurations = _evvConfigurations;
            this.logger = _logger;
        }

        public async Task<Response<IEnumerable<EvvExceptionEventsDto>>> Handle(EvvExceptionEventsQuery request, CancellationToken cancellationToken)
        {
            
            try
            {
                var evvExceptionEventsDto = await evvConfigurations.GetEvvExceptionEvents(request.HHA, request.UserId, request.Aggregator_ID);
                return Response.Ok<IEnumerable<EvvExceptionEventsDto>>(evvExceptionEventsDto);
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvExceptionEventsDto>>(ResponseErrorCodes.InternalServerFailed, "Failed");
            }
        }
    }
}

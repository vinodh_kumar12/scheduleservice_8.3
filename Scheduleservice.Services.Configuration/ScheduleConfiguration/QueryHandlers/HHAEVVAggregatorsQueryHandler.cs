﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class HHAEVVAggregatorsQueryHandler : IHandlerWrapper<HHAEVVAggregatorsQuery, IEnumerable<HHAEVVAgenciesConfigurationDto>>
    {
        private IHHASelectedEvvMasterRepository _HHASelectedEvvMasterRepository;
        private IUAEVVVendorVersionMasterRepository _UAEVVVendorVersionMasterRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IHHAEVVConfigurationsRepository _hHAEVVConfigurationsRepository;
        private IMapper mapper;
        private readonly ILogger<HHAEVVAggregatorsQueryHandler> _logger;

        public HHAEVVAggregatorsQueryHandler( IHHASelectedEvvMasterRepository _HHASelectedEvvMasterRepository, IMapper _mapper, 
            IRepositoryFilterConditions _repositoryFilterConditions, IUAEVVVendorVersionMasterRepository _UAEVVVendorVersionMasterRepository,
            IHHAEVVConfigurationsRepository _hHAEVVConfigurationsRepository,
            ILogger<HHAEVVAggregatorsQueryHandler> logger)
        {
            this._HHASelectedEvvMasterRepository = _HHASelectedEvvMasterRepository;
            this.mapper = _mapper;
            this._repositoryFilterConditions = _repositoryFilterConditions;
            this._UAEVVVendorVersionMasterRepository = _UAEVVVendorVersionMasterRepository;
            this._hHAEVVConfigurationsRepository = _hHAEVVConfigurationsRepository;
            this._logger = logger;
        }

        public async Task<Response<IEnumerable<HHAEVVAgenciesConfigurationDto>>> Handle(HHAEVVAggregatorsQuery request, CancellationToken cancellationToken)
        {
            try
            {                
                var EVVVendorMasterList = await _HHASelectedEvvMasterRepository._HHA_Schedule_GetHHAEVVVendors(request.HHA, request.UserId, "EvvVendorMasterID, EvvVendorName,EvvVendorID,isPrimary", "");
                var EVVVendorVersionList = await _UAEVVVendorVersionMasterRepository._HHA_Schedule_GetUAEVVVendorVersionMaster(request.HHA, request.UserId, "EvvVendorVersionMasterID,EvvVendorMasterID,DisplayName");

                var EVVConfigurationFilter = _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                var EVVConfigurationList = await _hHAEVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(request.HHA, request.UserId, "", EVVConfigurationFilter);

                EVVConfigurationList.ToList().ForEach(x => { x.MissedVisitReasonCodeAvailable = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.MissedVisitReasonCodeAvailable); });
                EVVConfigurationList.ToList().ForEach(x => { x.MissedVisitActionCodeAvailable = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.MissedVisitActionCodeAvailable); });
                EVVConfigurationList.ToList().ForEach(x => { x.ExcepionCodeAvailable = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.ExcepionCodeAvailable); });
                EVVConfigurationList.ToList().ForEach(x => { x.ReasonCodeAvailable = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.ReasonCodeAvailable); });
                EVVConfigurationList.ToList().ForEach(x => { x.ActionCodesAvailable = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.ActionCodesAvailable); });
                EVVConfigurationList.ToList().ForEach(x => { x.captureExceptionCodeOnReasonCode = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.captureExceptionCodeOnReasonCode); });
                EVVConfigurationList.ToList().ForEach(x => { x.isExceptionCodeMandatoryOnReasonCode = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.isExceptionCodeMandatoryOnReasonCode); });
                EVVConfigurationList.ToList().ForEach(x => { x.isMissedVisitActionCodeMandatoryOnReasonCode = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.isMissedVisitActionCodeMandatoryOnReasonCode); });
                EVVConfigurationList.ToList().ForEach(x => { x.canGrowMissedVisitReason = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.canGrowMissedVisitReason); });
                EVVConfigurationList.ToList().ForEach(x => { x.canGrowMissedVisitAction = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.canGrowMissedVisitAction); });
                EVVConfigurationList.ToList().ForEach(x => { x.canGrowException = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.canGrowException); });
                EVVConfigurationList.ToList().ForEach(x => { x.canGrowReasonCode = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.canGrowReasonCode); });
                EVVConfigurationList.ToList().ForEach(x => { x.canGrowActionCode = EVVConfigurationList.Where(y => y.EvvVendorVersionMasterID == x.EvvVendorVersionMasterID).Max(z => z.canGrowActionCode); });

               
                var EVVvendorlist = (from item  in EVVConfigurationList
                                     group item by item.EvvVendorVersionMasterID into groupedlist
                                      select  groupedlist.Max(x=> x.EvvConfigurationID)                                      
                                     );

                EVVConfigurationList = EVVConfigurationList.Where(x => x.EvvConfigurationID == EVVvendorlist.Where(y => y == x.EvvConfigurationID).FirstOrDefault());

                IEnumerable<HHAEVVAgenciesConfigurationDto> hhaproviderfinallist = new List<HHAEVVAgenciesConfigurationDto>();

                hhaproviderfinallist = from vendorMaster in EVVVendorMasterList
                                       join vendorversionMaster in EVVVendorVersionList
                                       on vendorMaster.EvvVendorMasterID equals vendorversionMaster.EvvVendorMasterID
                                       join evvconfiguration in EVVConfigurationList
                                       on vendorversionMaster.EvvVendorVersionMasterID equals evvconfiguration.EvvVendorVersionMasterID
                                       orderby vendorMaster.EvvVendorName, vendorMaster.isPrimary
                                       select new HHAEVVAgenciesConfigurationDto
                                       {
                                           DisplayName = vendorversionMaster.DisplayName,
                                           VendorName = vendorMaster.EvvVendorName,
                                           isPrimary = vendorMaster.isPrimary,
                                           EvvVendorVersionMasterID = vendorversionMaster.EvvVendorVersionMasterID,
                                           MissedVisitReasonCodeAvailable= evvconfiguration.MissedVisitReasonCodeAvailable,

                                           MissedVisitActionCodeAvailable= evvconfiguration.MissedVisitActionCodeAvailable,
                                           ExcepionCodeAvailable=evvconfiguration.ExcepionCodeAvailable,
                                           ReasonCodeAvailable=evvconfiguration.ReasonCodeAvailable,
                                           ActionCodeAvailable=evvconfiguration.ActionCodesAvailable,
                                           captureExceptionCodeOnReasonCode=evvconfiguration.captureExceptionCodeOnReasonCode,
                                           isActionCodeMandatoryOnReasonCode=evvconfiguration.isActionCodeMandatoryOnReasonCode,
                                           isExceptionCodeMandatoryOnReasonCode=evvconfiguration.isExceptionCodeMandatoryOnReasonCode,
                                           isMissedVisitActionCodeMandatoryOnReasonCode=evvconfiguration.isMissedVisitActionCodeMandatoryOnReasonCode,
                                           canGrowMissedVisitReason=evvconfiguration.canGrowMissedVisitReason,
                                           canGrowMissedVisitAction=evvconfiguration.canGrowMissedVisitAction,
                                           canGrowException=evvconfiguration.canGrowException,
                                           canGrowReasonCode=evvconfiguration.canGrowReasonCode,
                                           canGrowActionCode=evvconfiguration.canGrowActionCode


                                       };

                return Response.Ok<IEnumerable<HHAEVVAgenciesConfigurationDto>>(hhaproviderfinallist);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<HHAEVVAgenciesConfigurationDto>>(ResponseErrorCodes.InternalServerFailed, "Failed");
            }
        }
    }
}

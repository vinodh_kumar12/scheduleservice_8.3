﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class AgencyScheduleConfigurationsQueryHandler : IHandlerWrapper<AgencyScheduleConfigurationsQuery, IEnumerable<AgencyScheduleConfigurationsDto>>
    {

        public Task<Response<IEnumerable<AgencyScheduleConfigurationsDto>>> Handle(AgencyScheduleConfigurationsQuery request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}

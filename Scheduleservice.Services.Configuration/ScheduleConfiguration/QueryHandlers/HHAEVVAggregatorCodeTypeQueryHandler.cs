﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class HHAEVVAggregatorCodeTypeQueryHandler : IHandlerWrapper<HHAEVVAggregatorCodeTypeQuery, IEnumerable<HHAAggregatorCodeTypeDto>>
    {
        private IHHAEvvVendorVersionErrorCodeMasterRepository _HHAEvvVendorVersionErrorCodeMasterRepository;
        private IMapper mapper;
        private readonly ILogger<HHAEVVAggregatorCodeTypeQueryHandler> _logger;

        public HHAEVVAggregatorCodeTypeQueryHandler(IHHAEvvVendorVersionErrorCodeMasterRepository _HHAEvvVendorVersionErrorCodeMasterRepository, IMapper _mapper,
           ILogger<HHAEVVAggregatorCodeTypeQueryHandler> logger)
        {
            this._HHAEvvVendorVersionErrorCodeMasterRepository = _HHAEvvVendorVersionErrorCodeMasterRepository;
            this.mapper = _mapper;
            this._logger = logger;
        }
        public async Task<Response<IEnumerable<HHAAggregatorCodeTypeDto>>> Handle(HHAEVVAggregatorCodeTypeQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var hHAEvvVendorVersionErrorCodeMasterEntity = await _HHAEvvVendorVersionErrorCodeMasterRepository._HHA_Schedule_GetHHAEVVVendorVersionErrorCodeTypes(request.HHA, request.UserId, request.EVVVVendorVersionMasterID);

                IEnumerable<HHAAggregatorCodeTypeDto> HHACodeTypeList = mapper.Map<IEnumerable<HHAAggregatorCodeTypeDto>>(hHAEvvVendorVersionErrorCodeMasterEntity);
                return Response.Ok<IEnumerable<HHAAggregatorCodeTypeDto>>(HHACodeTypeList);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<HHAAggregatorCodeTypeDto>>(ResponseErrorCodes.InternalServerFailed, "Failed"); ;
            }
        }
    }
}

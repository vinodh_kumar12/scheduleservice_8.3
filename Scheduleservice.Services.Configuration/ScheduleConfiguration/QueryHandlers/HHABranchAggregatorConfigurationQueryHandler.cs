﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class HHABranchAggregatorConfigurationQueryHandler : IHandlerWrapper<HHABranchAggregatorConfigurationQuery, IEnumerable<HHABranchAggregatorConfigurationDto>>
    {
        private IHHAEVVConfigurationsRepository _C_EVVConfigurationsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private IUAEVVVendorVersionMasterRepository _UAEVVVendorVersionMasterRepository;
        private IUAEVVVendorMasterRepository _UAEVVVendorMasterRepository;
        private IMapper mapper;
        private readonly ILogger<HHABranchAggregatorConfigurationQueryHandler> _logger;

        public HHABranchAggregatorConfigurationQueryHandler(IHHAEVVConfigurationsRepository _C_EVVConfigurationsRepository, 
            IMapper _mapper, IRepositoryFilterConditions _repositoryFilterConditions,
            IUAEVVVendorVersionMasterRepository _UAEVVVendorVersionMasterRepository,
            IUAEVVVendorMasterRepository _UAEVVVendorMasterRepository,
            ILogger<HHABranchAggregatorConfigurationQueryHandler> logger)
        {
            this._C_EVVConfigurationsRepository = _C_EVVConfigurationsRepository;
            this.mapper = _mapper;
            this._repositoryFilterConditions = _repositoryFilterConditions;
            this._UAEVVVendorVersionMasterRepository = _UAEVVVendorVersionMasterRepository;
            this._UAEVVVendorMasterRepository = _UAEVVVendorMasterRepository;
            this._logger = logger;
        }

        public async Task<Response<IEnumerable<HHABranchAggregatorConfigurationDto>>> Handle(HHABranchAggregatorConfigurationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                //var EVVVendorMasterList = await _UAEVVVendorMasterRepository._HHA_Schedule_GetUAEVVVendorMaster(request.HHA, request.UserId, "EvvVendorMasterID,VendorName,isPrimary");
                //var EVVVendorVersionList = await _UAEVVVendorVersionMasterRepository._HHA_Schedule_GetUAEVVVendorVersionMaster(request.HHA, request.UserId, "EvvVendorVersionMasterID,EvvVendorMasterID,DisplayName");

                var EVVConfigurationFilter =  _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .Build(); 

                var EVVConfigurationList = await _C_EVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(request.HHA, request.UserId, "EvvConfigurationID,HHA,EvvVendorVersionMasterID,AttestationMandatoryForAllReasonCodes,AttestationMandatoryEvent,AttestationHideFromClinician,configuration_name", EVVConfigurationFilter);


                IEnumerable<HHABranchAggregatorConfigurationDto> hhabranchAggregatorsettingslists = new List<HHABranchAggregatorConfigurationDto>();

                hhabranchAggregatorsettingslists = from evvconfiguration in EVVConfigurationList
                                                   select new HHABranchAggregatorConfigurationDto
                                                   {
                                                       EvvConfigurationID = evvconfiguration.EvvConfigurationID,
                                                       ConfigurationName = evvconfiguration.configuration_name,
                                                       AttestationMandatoryForAllReasonCodes = evvconfiguration.AttestationMandatoryForAllReasonCodes,
                                                       AttestationMandatoryEvent = evvconfiguration.AttestationMandatoryEvent,
                                                       AttestationHideFromClinician = evvconfiguration.AttestationHideFromClinician
                                                   };


                return Response.Ok<IEnumerable<HHABranchAggregatorConfigurationDto>>(hhabranchAggregatorsettingslists);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<HHABranchAggregatorConfigurationDto>>(ResponseErrorCodes.InternalServerFailed, "Failed"); ;
            }
        }
    }
}

﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class EvvConfigurationQueryHandler : IHandlerWrapper<EvvConfigurationQuery, IEnumerable<EvvConfigurationDto>>
    {
        private readonly IEvvConfigurations evvConfigurations;
        private readonly ILogger<EvvConfigurationQueryHandler> logger;
        public EvvConfigurationQueryHandler(IEvvConfigurations _evvConfigurations, ILogger<EvvConfigurationQueryHandler> _logger)
        {
            this.evvConfigurations = _evvConfigurations;
            this.logger = _logger;
        }
        public async Task<Response<IEnumerable<EvvConfigurationDto>>> Handle(EvvConfigurationQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var evvconfiginfo = await evvConfigurations.GetEvvConfigurations(request.HHA, request.UserId);
                return Response.Ok<IEnumerable<EvvConfigurationDto>>(evvconfiginfo);
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EvvConfigurationDto>>(ResponseErrorCodes.InternalServerFailed, "Failed");
            }
        }
    }
}

﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.QueryHandlers
{
    public class HHAEVVVendorVersionCodesQueryHandler : IHandlerWrapper<HHAEVVVendorVersionCodesQuery, IEnumerable<EVVScheduleEditCodeMasterDto>>
    {
        private IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository;
        private IMapper mapper;
        private readonly ILogger<HHAEVVVendorVersionCodesQueryHandler> _logger;

        public HHAEVVVendorVersionCodesQueryHandler(IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository, IMapper _mapper,
            ILogger<HHAEVVVendorVersionCodesQueryHandler> logger)
        {
            this._UAEvvScheduleEditCodesMasterRepository = _UAEvvScheduleEditCodesMasterRepository;
            this.mapper = _mapper;
            this._logger = logger;
        }

        public async Task<Response<IEnumerable<EVVScheduleEditCodeMasterDto>>> Handle(HHAEVVVendorVersionCodesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var EVVCodeList = await _UAEvvScheduleEditCodesMasterRepository.GetHHAEVVCodes(request.HHA, request.UserId, "", request.EVVVVendorVersionMasterID);

                IEnumerable<EVVScheduleEditCodeMasterDto> EVVVenorCodeList = mapper.Map<IEnumerable<EVVScheduleEditCodeMasterDto>>(EVVCodeList);

                return Response.Ok<IEnumerable<EVVScheduleEditCodeMasterDto>>(EVVVenorCodeList);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<IEnumerable<EVVScheduleEditCodeMasterDto>>(ResponseErrorCodes.InternalServerFailed, "Failed");
            }

        }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Query
{
    public class EvvBranchConfigurationQuery: BaseRequest, IRequestWrapper<IEnumerable<EvvBranchConfigurationDto>>
    {
        public int HHA_Brach_ID { get; set; }
    }
}

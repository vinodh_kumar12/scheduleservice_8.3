﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Query
{
    public class EvvExceptionEventCodesQuery : BaseRequest, IRequestWrapper<IEnumerable<EvvExceptionEventsDto>>
    {
        public int Aggregator_ID { get; set; }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Query
{
    public class HHABranchAggregatorConfigurationQuery : BaseRequest, IRequestWrapper<IEnumerable<HHABranchAggregatorConfigurationDto>>
    {
        public int HHA_Branch_ID { get; set; }
    }
}

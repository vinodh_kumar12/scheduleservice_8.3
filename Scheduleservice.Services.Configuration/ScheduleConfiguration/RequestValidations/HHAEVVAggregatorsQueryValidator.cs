﻿using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class HHAEVVAggregatorsQueryValidator : AbstractValidator<HHAEVVAggregatorsQuery>
    {
        public HHAEVVAggregatorsQueryValidator(){
            RuleFor(e=>e.HHA).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
        }
    }
}

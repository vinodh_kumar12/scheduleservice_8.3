﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using Newtonsoft.Json.Linq;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class EvvConfigurationCommandValidator : AbstractValidator<EvvConfigurationCommand>
    {
        public EvvConfigurationCommandValidator()
        {
            RuleFor(x => x.EvvConfigurationProperties).Must(ValidateJSON);
        }

        private bool ValidateJSON(string data)
        {
            if (String.IsNullOrEmpty(data))
            {
                return false;
            }

            data = data.Trim();
            try
            {
                if (data.StartsWith("{") && data.EndsWith("}"))
                {
                    JToken.Parse(data);
                }
                else if (data.StartsWith("[") && data.EndsWith("]"))
                {
                    JArray.Parse(data);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

﻿using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class EVVAggregatorConfigurationCommandValidator : AbstractValidator<EVVAggregatorConfigurationCommand>
    {
        public EVVAggregatorConfigurationCommandValidator()
        { 
            RuleFor(e=>e.HHA).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e => e.EvvConfigurationID).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e => e.EVVAggregatorConfiguration).Must(ValidJSON);
        }

        public bool ValidJSON(string EVVAggrgeatorconfigurationJSON)
        {
            if (string.IsNullOrWhiteSpace(EVVAggrgeatorconfigurationJSON)) { return false; }
            EVVAggrgeatorconfigurationJSON = EVVAggrgeatorconfigurationJSON.Trim();
            if ((EVVAggrgeatorconfigurationJSON.StartsWith("{") && EVVAggrgeatorconfigurationJSON.EndsWith("}")) || //For object
                (EVVAggrgeatorconfigurationJSON.StartsWith("[") && EVVAggrgeatorconfigurationJSON.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(EVVAggrgeatorconfigurationJSON);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}

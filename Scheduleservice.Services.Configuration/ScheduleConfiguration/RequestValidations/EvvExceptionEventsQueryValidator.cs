﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class EvvExceptionEventsQueryValidator: AbstractValidator<EvvExceptionEventsQuery>
    {
        public EvvExceptionEventsQueryValidator()
        {
            RuleFor(x => x.Aggregator_ID).NotNull().NotEqual(0);
        }
    }
}

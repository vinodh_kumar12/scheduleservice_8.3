﻿using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class HHAEVVVendorVersionCodesQueryValidator : AbstractValidator<HHAEVVVendorVersionCodesQuery>
    {
        public HHAEVVVendorVersionCodesQueryValidator() {

            RuleFor(e => e.HHA).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e => e.EVVVVendorVersionMasterID).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
        }
    }
}

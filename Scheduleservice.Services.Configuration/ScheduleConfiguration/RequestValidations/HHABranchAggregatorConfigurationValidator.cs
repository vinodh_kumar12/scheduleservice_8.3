﻿using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class HHABranchAggregatorConfigurationValidator : AbstractValidator<HHABranchAggregatorConfigurationQuery>
    {
        public HHABranchAggregatorConfigurationValidator()
        {
            RuleFor(e => e.HHA).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e=>e.HHA_Branch_ID).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
        }
    }
}

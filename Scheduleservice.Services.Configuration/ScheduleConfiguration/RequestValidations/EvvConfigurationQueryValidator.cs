﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Query;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class EvvConfigurationQueryValidator: AbstractValidator<EvvConfigurationQuery>
    {
        public EvvConfigurationQueryValidator()
        {
            RuleFor(e => e.HHA).NotNull().NotEqual(0);
        }
    }
}

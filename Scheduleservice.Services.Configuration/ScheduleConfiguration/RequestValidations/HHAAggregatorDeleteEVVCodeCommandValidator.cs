﻿using FluentValidation;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.RequestValidations
{
    public class HHAAggregatorDeleteEVVCodeCommandValidator : AbstractValidator<HHAAggregatorDeleteEVVCodeCommand>
    {
        public HHAAggregatorDeleteEVVCodeCommandValidator()
        {
            RuleFor(e => e.HHA).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e => e.Code).NotEmpty().NotNull();
            RuleFor(e => e.CodeType).NotEmpty().NotNull();
            RuleFor(e => e.EVVVendorVersionMasterID).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
            RuleFor(e => e.EVVScheduleEditCodeMasterID).NotEmpty().NotNull().NotEqual(0).GreaterThan(0);
        }
    }
}

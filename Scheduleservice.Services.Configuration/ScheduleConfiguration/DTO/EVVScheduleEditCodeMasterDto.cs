﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class EVVScheduleEditCodeMasterDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool UseThisCodeForScheduleSplit { get; set; }
        public string SystemCode { get; set; }
        public bool NotesMandatory { get; set; }
        public string CodeType { get; set; }
        public int EvvVendorVersionMasterID { get; set; }
        public bool isKanTimeCode { get; set; }
        public string ExceptionSystemType { get; set; }
        public int EvvScheduleEditCodesMasterID { get; set; }
        public bool EnableNotesPrefill { get; set; }
        public string MasterPayerCodes { get; set; }

    }
}

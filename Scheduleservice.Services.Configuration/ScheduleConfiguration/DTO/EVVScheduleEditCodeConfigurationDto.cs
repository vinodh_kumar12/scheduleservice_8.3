﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class EVVScheduleEditCodeConfigurationDto
    {
        public int IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int EVVScheduleEditCodeMasterID { get; set; }
    }
}

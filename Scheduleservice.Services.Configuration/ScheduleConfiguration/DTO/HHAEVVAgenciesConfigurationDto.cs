﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class HHAEVVAgenciesConfigurationDto
    {
        public string DisplayName { get; set; }
        public string VendorName { get; set; }
        public bool isPrimary { get; set; }
        public int EvvVendorVersionMasterID { get; set; }

        public bool MissedVisitReasonCodeAvailable { get; set; }
        public bool MissedVisitActionCodeAvailable { get; set; }
        public bool ExcepionCodeAvailable { get; set; }
        public bool ReasonCodeAvailable { get; set; }
        public bool ActionCodeAvailable { get; set; }
        public bool captureExceptionCodeOnReasonCode { get; set; }
        public bool isActionCodeMandatoryOnReasonCode { get; set; }

        public bool isExceptionCodeMandatoryOnReasonCode { get; set; }
        public bool isMissedVisitActionCodeMandatoryOnReasonCode { get; set; }
        public bool canGrowMissedVisitReason { get; set; }
        public bool canGrowMissedVisitAction { get; set; }
        public bool canGrowException { get; set; }
        public bool  canGrowReasonCode { get; set; }
        public bool  canGrowActionCode { get; set; }



    }
}

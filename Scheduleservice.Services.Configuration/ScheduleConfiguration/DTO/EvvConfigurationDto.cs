﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class EvvConfigurationDto
    {
        public int AutoGenerateNoShowExceptionMin { get; set; }
        public int AutoGenerateMissedCheckoutExceptionMin { get; set; }
        public int AutoGenerateUnscheduledExceptionMin { get; set; }
        public int AutoGenerateShortVisitExceptionMin { get; set; }
        public int AutoGenerateLongVisitExceptionMin { get; set; }
        public int AutoGenerateLateCheckinExceptionMin { get; set; }
        public int EVVOverageExceptionthreshold { get; set; }
        public int EVVOverNightExceptionthreshold { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class HHABranchAggregatorConfigurationDto
    {
        public int EvvConfigurationID { get; set; }
        public string ConfigurationName { get; set; }
        public bool? AttestationMandatoryForAllReasonCodes { get; set; }
        public string AttestationMandatoryEvent { get; set; }
        public bool? AttestationHideFromClinician { get; set; }
    }
}

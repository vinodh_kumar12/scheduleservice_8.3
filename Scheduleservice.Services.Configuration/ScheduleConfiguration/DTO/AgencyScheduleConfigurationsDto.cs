﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class AgencyScheduleConfigurationsDto
    {
        public bool EnableAutoSplitWhenActualHrsExceedPlannedHrs { get; set; }
        public int AutoSplitWhenActualHrsExceedPlannedHrsGraceMinutes { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForKantimePrimaryEVV { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForOtherPrimaryEVV { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsForNonEVV { get; set; }
        public string AutoSplitWhenActualHrsExceedPlannedHrsPayers { get; set; }
        public string AutoSplitWhenActualHrsExceedPlannedHrsDiscplines { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsPayerRoundingRulesApplyFirstSplit { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsPayerRoundingRulesApplySecondSplit { get; set; }
        public bool AutoSplitWhenActualHrsExceedPlannedHrsMarkNonBillableAdditionalHrsSplit { get; set; }
        public string AutoSplitWhenActualHrsExceedPlannedHrsNonBillableReason { get; set; }
        public string DashboardAlertwhenAdditionalHrsexceedsPayers { get; set; }
        public string DashboardAlertwhenAdditionalHrsexceedsCaregiverDisciplines { get; set; }
        public int EVVOverageExceptionthreshold { get; set; }
        public bool AllowToCheckoutifHardStopValidationExistsForEVV { get; set; }
        public bool AllowToCheckoutifHardStopValidationExistsForNonEVV { get; set; }
        public bool AllowToCheckoutifHardStopValidationExists { get; set; }
        public bool BlockApprovalIfUnresolvedConflictsExists { get; set; }
        public bool BlockCheckInCheckoutForPastVisits { get; set; }
        public int BlockCheckInCheckoutForPastVisitsDays { get; set; }
        public bool BlockCheckInCheckoutForPastVisitsForEVV { get; set; }
        public bool BlockCheckInCheckoutForPastVisitsForNonEVV { get; set; }
        public string AttestationCaption { get; set; }
        public int AutoGenerateNoShowExceptionMin { get; set; }
        public int AutoGenerateMissedCheckoutExceptionMin { get; set; }
        public int AutoGenerateUnscheduledExceptionMin { get; set; }
        public int AutoGenerateShortVisitExceptionMin { get; set; }
        public int AutoGenerateLongVisitExceptionMin { get; set; }
        public int AutoGenerateLateCheckinExceptionMin { get; set; }
    }
}

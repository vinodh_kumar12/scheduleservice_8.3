﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO
{
    public class AgencyBranchScheduleConfigurationsDto
    {
        public bool enableTravelTime { get; set; }
        public bool enableDOcumentationTime { get; set; }
        public bool ScheduleCalendarBasedonWeekStartDay { get; set; }
        public bool CreateCustomEpisode { get; set; }
        public bool ClinicianCreateScheduleAllowOnlyPrimaryPayer { get; set; }
        public bool CAN_CREATE_ADHOC_SCHEDULE { get; set; }
        public bool IsMilitaryTimeFormat { get; set; }
        public byte NotAllowScheduleDoubleBooking { get; set; }
        public byte NotAllowScheduleDeviation { get; set; }
        public bool DonotValdateDoubleBookforThirdPartyClinicians { get; set; }
        public bool ignoreDoubleBookForNonBillable { get; set; }
        public bool IgnoreDeviationForNonBillable { get; set; }
        public bool ignoreDeviationForSupervisorSchedules { get; set; }
        public bool ignoreDeviationForDifferentServiceDiscplines { get; set; }
        public int doubleBookGracePeriodMinutes { get; set; }
        public byte scheduleDeviationGracePeriod { get; set; }
        public bool ignoreacrossLobAdmits { get; set; }
        public bool EnableBillingofMissedVisitTravelTime { get; set; }
        public bool IsActualTimeForCheckin_CheckOutLinks { get; set; }

    }
}

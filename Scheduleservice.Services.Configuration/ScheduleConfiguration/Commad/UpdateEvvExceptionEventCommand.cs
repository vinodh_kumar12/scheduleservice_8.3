﻿using System;
using System.Collections.Generic;
using System.Text;
using Scheduleservice.Core.Common.Request;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class UpdateEvvExceptionEventCommand : BaseRequest, IRequestWrapper<bool>
    {
        public int Aggregator_ID { get; set; }
        public string EvvExceptionEventIDs { get; set; }
        public string exception_code { get; set; }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class HHAAggregatorDeleteEVVCodeCommand : BaseRequest, IRequestWrapper<EVVScheduleEditCodeConfigurationDto>
    {
  
        public int EVVScheduleEditCodeMasterID { get; set; }
        public string CodeType { get; set; }
        public int EVVVendorVersionMasterID { get; set; }
        public string Code { get; set; }
    }
}

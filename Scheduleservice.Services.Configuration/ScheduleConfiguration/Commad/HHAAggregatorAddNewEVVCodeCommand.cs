﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class HHAAggregatorAddNewEVVCodeCommand : BaseRequest, IRequestWrapper<EVVScheduleEditCodeConfigurationDto>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string CodeType { get; set; }
        public int EVVVendorVersionMasterID { get; set; }
        public bool EnableNotesPrefill { get; set; }
    }
}

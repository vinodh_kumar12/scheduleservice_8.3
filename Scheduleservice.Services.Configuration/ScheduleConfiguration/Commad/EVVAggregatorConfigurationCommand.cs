﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
   public class EVVAggregatorConfigurationCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string EVVAggregatorConfiguration { get; set; }
        public int EvvConfigurationID { get; set; }
    }
}

﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class EvvConfigurationCommand:BaseRequest, IRequestWrapper<bool>
    {
        public int HHA_Branch_ID { get; set; }
        public string EvvConfigurationProperties { get; set; }
    }
}

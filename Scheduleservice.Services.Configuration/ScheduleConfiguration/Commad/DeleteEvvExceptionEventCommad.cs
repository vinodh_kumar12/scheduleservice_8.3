﻿using System;
using System.Collections.Generic;
using System.Text;
using Scheduleservice.Core.Common.Request;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class DeleteEvvExceptionEventCommad : BaseRequest, IRequestWrapper<bool>
    {
        public int Aggregator_ID { get; set; }
        public string EvvExceptionEvents { get; set; }
    }
}

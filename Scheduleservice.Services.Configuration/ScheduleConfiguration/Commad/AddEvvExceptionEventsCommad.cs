﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad
{
    public class AddEvvExceptionEventsCommad: BaseRequest, IRequestWrapper<bool>
    {
        public int Aggregator_ID { get; set; }
        public string Events { get; set; }
        public string exception_code { get; set; }
    }
}

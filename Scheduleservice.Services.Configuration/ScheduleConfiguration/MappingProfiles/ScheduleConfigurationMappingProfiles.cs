﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.MappingProfiles
{
    public class ScheduleConfigurationMappingProfiles : Profile
    {
        public ScheduleConfigurationMappingProfiles()
        {
            CreateMap<HHAScheduleAuthSettingsEntity, EvvConfigurationDto>();
            CreateMap<HHABranchScheduleConfigurationsEntity, AgencyBranchScheduleConfigurationsDto>();
            CreateMap<HHAScheduleConfigurationsEntity, AgencyScheduleConfigurationsDto>();
            CreateMap<_UA_EvvExceptionEventMasterEntity, EvvExceptionEventsDto>();
            CreateMap<HHAEvvVendorVersionErrorCodeMasterEntity, HHAAggregatorCodeTypeDto>();
            CreateMap<EvvScheduleEditCodesMasterEntity, EVVScheduleEditCodeMasterDto>();
            CreateMap<HHAEvvVendorExceptionEventsEntity, EvvExceptionEventsDto>();
        }
    }
}

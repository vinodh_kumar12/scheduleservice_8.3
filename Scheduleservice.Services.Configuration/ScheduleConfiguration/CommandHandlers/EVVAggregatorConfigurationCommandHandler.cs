﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Microsoft.Extensions.Logging;


namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class EVVAggregatorConfigurationCommandHandler : IHandlerWrapper<EVVAggregatorConfigurationCommand, bool>
    {
        private IHHAEVVConfigurationsRepository _C_EVVConfigurationsRepository;
        private readonly ILogger<EVVAggregatorConfigurationCommandHandler> _logger;

        public EVVAggregatorConfigurationCommandHandler(IHHAEVVConfigurationsRepository _C_EVVConfigurationsRepository,
           ILogger<EVVAggregatorConfigurationCommandHandler> logger)
        {
            this._C_EVVConfigurationsRepository = _C_EVVConfigurationsRepository;
            this._logger = logger;
        }
        public async Task<Response<bool>> Handle(EVVAggregatorConfigurationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string AuditData = "";
                string ModifiedData = "";

                StringBuilder sbModifiedData = new StringBuilder();
                StringBuilder sbModifiedAudit = new StringBuilder();

                if (request.EVVAggregatorConfiguration != "")
                {
                    var newdata = JsonConvert.DeserializeObject<HHABranchAggregatorConfigurationDto>(request.EVVAggregatorConfiguration);

                    sbModifiedAudit.AppendFormat("Modified {0}", newdata.ConfigurationName);


                    if (newdata.AttestationMandatoryForAllReasonCodes != null)
                    {
                        if (Convert.ToBoolean(newdata.AttestationMandatoryForAllReasonCodes))
                        {
                            sbModifiedData.Append(" AttestationMandatoryForAllReasonCodes=1,");
                            sbModifiedAudit.Append(" Attestation has been enabled,");
                        }
                        else
                        {
                            sbModifiedData.Append(" AttestationMandatoryForAllReasonCodes=0,");
                            sbModifiedAudit.Append(" Attestation has been disabled,");
                        }
                    }

                    if (!string.IsNullOrEmpty(newdata.AttestationMandatoryEvent))
                    {
                        if (newdata.AttestationMandatoryEvent == "AT_APPROVAL")
                        {
                            sbModifiedData.Append(" AttestationMandatoryEvent='AT_APPROVAL',");
                            sbModifiedAudit.Append(" Attestation Event is modifed from 'Visit Edit' to 'Visit Approval',");
                        }
                        else
                        {
                            sbModifiedData.Append(" AttestationMandatoryEvent='AT_EDIT',");
                            sbModifiedAudit.Append(" Attestation Event is modifed from 'Visit Approval' to 'Visit Edit',");
                        }
                    }

                    if (newdata.AttestationHideFromClinician != null)
                    {
                        if (Convert.ToBoolean(newdata.AttestationHideFromClinician))
                        {
                            sbModifiedData.Append(" AttestationHideFromClinician=1,");
                            sbModifiedAudit.Append(" Attestation by Clinician has been enabled,");
                        }
                        else
                        {
                            sbModifiedData.Append(" AttestationHideFromClinician=0,");
                            sbModifiedAudit.Append(" Attestation by Clinician has been disabled,");
                        }
                    }
                }

                ModifiedData = sbModifiedData.ToString();
                AuditData = sbModifiedAudit.ToString();

                ModifiedData = ModifiedData.Substring(0, ModifiedData.Length - 1);
                AuditData = AuditData.Substring(0, AuditData.Length - 1);

                var ret = await _C_EVVConfigurationsRepository._HHA_Schedule_SaveEVVBranchAggregatorSettings(request.HHA, request.UserId, request.EvvConfigurationID.ToString(), ModifiedData, AuditData);

                if (ret)
                    return Response.Ok<bool>(ret);
                else
                    return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            }
        }
    }
}

﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class AddEvvExceptionEventsCommandHandler: IHandlerWrapper<AddEvvExceptionEventsCommad, bool>
    {
        private readonly IEvvConfigurations evvConfigurations;
        private readonly ILogger<AddEvvExceptionEventsCommandHandler> logger;
        public AddEvvExceptionEventsCommandHandler(IEvvConfigurations _evvConfigurations, ILogger<AddEvvExceptionEventsCommandHandler> _logger)
        {
            this.evvConfigurations = _evvConfigurations;
            this.logger = _logger;
        }

        public async Task<Response<bool>> Handle(AddEvvExceptionEventsCommad request, CancellationToken cancellationToken)
        {
            bool Error = true;
            string Message = "";
            try
            {
                Error = await evvConfigurations.AddExceptionEvent(request.HHA, request.UserId, request.Aggregator_ID, request.Events, request.exception_code);
            }
            catch (Exception e)
            {
                Error = true;
                Message = "Some unknown exception occurred";
                logger.LogError(e,e.Message);
            }

            if(Error)
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed,Message);
            else
                return Response.Ok<bool>(Error);
        }
    }
}

﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class EvvConfigurationCommandHandler : IHandlerWrapper<EvvConfigurationCommand, bool>
    {
        private readonly ILogger<EvvConfigurationCommandHandler> logger;
        private readonly IEvvConfigurations evvConfigurations;

        public EvvConfigurationCommandHandler(ILogger<EvvConfigurationCommandHandler> _logger, IEvvConfigurations _evvConfigurations)
        {
            this.logger = _logger;
            this.evvConfigurations = _evvConfigurations;
        }

        public async Task<Response<bool>> Handle(EvvConfigurationCommand request, CancellationToken cancellationToken)
        {
            var Error = true;
            try
            {
                
                Error = await evvConfigurations.SaveEvvConfigurations(request.HHA, request.UserId, request.EvvConfigurationProperties);
                Error = false;
            }
            catch (Exception e)
            {
                logger.LogError(e,e.Message);
            }
            if (Error)
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            else
                return Response.Ok<bool>(true);
        }
    }
}

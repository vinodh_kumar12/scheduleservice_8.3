﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System.Linq;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.Enums;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class HHAAggregatorDeleteEVVCodeCommandHandler : IHandlerWrapper<HHAAggregatorDeleteEVVCodeCommand, EVVScheduleEditCodeConfigurationDto>
    {
        private IHHAEVVConfigurationsRepository _HHAEVVConfigurationsRepository;
        private IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository;
        private IHHAEvvExceptionEventsRepository _hHAEvvExceptionEventsRepository;
        private readonly ILogger<HHAAggregatorDeleteEVVCodeCommandHandler> _logger;
        private readonly IHHAEvvExceptionEventCodesRepository hHAEvvExceptionEventCodesRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;

        public HHAAggregatorDeleteEVVCodeCommandHandler(IHHAEVVConfigurationsRepository _HHAEVVConfigurationsRepository, IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository,
                                IHHAEvvExceptionEventsRepository _hHAEvvExceptionEventsRepository,
                                ILogger<HHAAggregatorDeleteEVVCodeCommandHandler> logger,
                                IHHAEvvExceptionEventCodesRepository _hHAEvvExceptionEventCodesRepository,
                                IRepositoryFilterConditions _repositoryFilterConditions)
        {
            this._HHAEVVConfigurationsRepository = _HHAEVVConfigurationsRepository;
            this._UAEvvScheduleEditCodesMasterRepository = _UAEvvScheduleEditCodesMasterRepository;
            this._hHAEvvExceptionEventsRepository = _hHAEvvExceptionEventsRepository;
            this._logger = logger;
            this.hHAEvvExceptionEventCodesRepository = _hHAEvvExceptionEventCodesRepository;
            this._repositoryFilterConditions = _repositoryFilterConditions;
        }

        public async Task<Response<EVVScheduleEditCodeConfigurationDto>> Handle(HHAAggregatorDeleteEVVCodeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                string ModifiedData = "";
                string ModifiedEVVConfigData = "";
                string EVVConfigurationIDWithCommaSep = "";
                string ExceptionIDJSON = "";
                string ExceptionEventCodeIDJSON = "";

                StringBuilder sbModifiedData = new StringBuilder();

                EVVScheduleEditCodeConfigurationDto eVVScheduleEditCodeConfigurationDto = new EVVScheduleEditCodeConfigurationDto();

                var EVVCodeList = await _UAEvvScheduleEditCodesMasterRepository.GetHHAEVVCodes(request.HHA, request.UserId, request.CodeType, request.EVVVendorVersionMasterID);

                sbModifiedData.Append(" isDeleted = 1,");
                sbModifiedData.AppendFormat(" DeletedBy = {0},", request.UserId);
                sbModifiedData.Append(" DeletedOn = GETDATE(),");

                ModifiedData = sbModifiedData.ToString();
                ModifiedData = ModifiedData.Substring(0, ModifiedData.Length - 1);

                var result = await _UAEvvScheduleEditCodesMasterRepository._HHA_Schedule_UpdateEVVCode(request.HHA, request.UserId, request.EVVScheduleEditCodeMasterID, ModifiedData);

                eVVScheduleEditCodeConfigurationDto.IsSuccess = result.IsSuccess;
                eVVScheduleEditCodeConfigurationDto.ErrorMessage = result.ErrorMessage;
                eVVScheduleEditCodeConfigurationDto.EVVScheduleEditCodeMasterID = result.EVVScheduleEditCodeMasterID;

               var EVVExceptionList =  await _hHAEvvExceptionEventsRepository.GetHHAEvvVendorExceptionEventCodes(request.HHA, request.UserId, request.EVVVendorVersionMasterID);

                if (EVVExceptionList.Count() > 0) 
                { 

                    var ExceptionIDsList = EVVExceptionList.Where(y=>y.exception_code==request.Code).Select(x => new { EvvExceptionEventID = x.EvvExceptionEventID, event_code = x.event_code, ExceptionEventCodeID = x.ExceptionEventCodeID }).ToList();

                    if (ExceptionIDsList.Count > 0)
                    {
                        ExceptionEventCodeIDJSON = JsonConvert.SerializeObject(from x in ExceptionIDsList select new { ExceptionEventCodeID = x.ExceptionEventCodeID });

                        if (!string.IsNullOrEmpty(ExceptionEventCodeIDJSON))
                        {

                            bool ret = await hHAEvvExceptionEventCodesRepository.DeleteEVVExceptionEventCodes(request.HHA, request.UserId, ExceptionEventCodeIDJSON);
                        }

                        ExceptionIDJSON = JsonConvert.SerializeObject(from x in ExceptionIDsList select new { EvvExceptionEventID = x.EvvExceptionEventID });

                        if (!string.IsNullOrEmpty(ExceptionIDJSON))
                        {

                            bool ret = await _hHAEvvExceptionEventsRepository.DeleteEVVExceptionEvents(request.HHA, request.UserId, ExceptionIDJSON);
                        }
                    }
                }

                var EVVConfigurationFilter = _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                var EVVBranchConfiguarationList = await _HHAEVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(request.HHA, request.UserId, "EvvConfigurationID,EvvVendorVersionMasterID, EffectiveDate,AttestationMandatoryForAllReasonCodes,MissedVisitReasonCodeAvailable,MissedVisitActionCodeAvailable,ExcepionCodeAvailable,ReasonCodeAvailable,ActionCodesAvailable,captureExceptionCodeOnReasonCode,requireAllExceptionsToClearForApproval,isActionCodeMandatoryOnReasonCode,isExceptionCodeMandatoryOnReasonCode,isMissedVisitActionCodeMandatoryOnReasonCode,canGrowMissedVisitReason,canGrowMissedVisitAction,canGrowException,canGrowReasonCode,canGrowActionCode", EVVConfigurationFilter);

                if (EVVCodeList.Count() == 1)
                {
                    switch (request.CodeType)
                    {
                        case "Exception":
                            var filterExceptionconfigdetails = EVVBranchConfiguarationList.Where(x => x.ExcepionCodeAvailable == true && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                            ModifiedEVVConfigData = "ExcepionCodeAvailable=0,AutoGenerateException=0,";
                            if (filterExceptionconfigdetails.Count > 0)
                                EVVConfigurationIDWithCommaSep = string.Join(",", filterExceptionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                            break;
                        case "ReasonCode":
                            var filterReasonconfigdetails = EVVBranchConfiguarationList.Where(x => x.ReasonCodeAvailable == true && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                            ModifiedEVVConfigData = "ReasonCodeAvailable=0,";
                            if (filterReasonconfigdetails.Count > 0)
                                EVVConfigurationIDWithCommaSep = string.Join(",", filterReasonconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                            break;
                        case "ActionCode":
                            var filterActionconfigdetails = EVVBranchConfiguarationList.Where(x => x.ActionCodesAvailable == true && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                            ModifiedEVVConfigData = "ActionCodesAvailable=0,";
                            if (filterActionconfigdetails.Count > 0)
                                EVVConfigurationIDWithCommaSep = string.Join(",", filterActionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                            break;
                        case "MissedVisitReasonCode":
                            var filterMissedVisitReasonconfigdetails = EVVBranchConfiguarationList.Where(x => x.MissedVisitReasonCodeAvailable == true && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                            ModifiedEVVConfigData = "MissedVisitReasonCodeAvailable=0,";
                            if (filterMissedVisitReasonconfigdetails.Count > 0)
                                EVVConfigurationIDWithCommaSep = string.Join(",", filterMissedVisitReasonconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                            break;
                        case "MissedVisitActionCode":
                            var filterMissedVisitActionconfigdetails = EVVBranchConfiguarationList.Where(x => x.MissedVisitActionCodeAvailable == true && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                            ModifiedEVVConfigData = "MissedVisitActionCodeAvailable=0,";
                            if (filterMissedVisitActionconfigdetails.Count > 0)
                                EVVConfigurationIDWithCommaSep = string.Join(",", filterMissedVisitActionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                            break;
                        default:
                            break;
                    }

                    if (!string.IsNullOrEmpty(ModifiedEVVConfigData))
                    {

                        ModifiedEVVConfigData = ModifiedEVVConfigData.Substring(0, ModifiedEVVConfigData.Length - 1);

                        if (!string.IsNullOrEmpty(EVVConfigurationIDWithCommaSep))
                        {
                            var response = await _HHAEVVConfigurationsRepository._HHA_Schedule_SaveEVVBranchAggregatorSettings(request.HHA, request.UserId, EVVConfigurationIDWithCommaSep.ToString(), ModifiedEVVConfigData, "");
                        }
                    }
                }

                return Response.Ok<EVVScheduleEditCodeConfigurationDto>(eVVScheduleEditCodeConfigurationDto);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<EVVScheduleEditCodeConfigurationDto>(ResponseErrorCodes.InternalServerFailed,"Failed to save, Please contact KanTime support!");
            }

        }
    }
}

﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System.Linq;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Microsoft.Extensions.Logging;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class HHAAggrgegatorUpdateEVVCodeCommandHandler : IHandlerWrapper<HHAAggregatorUpdateEVVCodeCommand, EVVScheduleEditCodeConfigurationDto>
    {
        private IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository;
        private readonly IHHAEvvExceptionEventsRepository _hHAEvvExceptionEventsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly IRepositoryUpdateColumn _repositoryUpdateColumn;
        private readonly ILogger<HHAAggrgegatorUpdateEVVCodeCommandHandler> _logger;
        private readonly IHHAEvvExceptionEventCodesRepository hHAEvvExceptionEventCodesRepository;

        public HHAAggrgegatorUpdateEVVCodeCommandHandler(IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository,
                                                        IHHAEvvExceptionEventsRepository _hHAEvvExceptionEventsRepository, IRepositoryFilterConditions _repositoryFilterConditions,
                                                        IRepositoryUpdateColumn _repositoryUpdateColumn, ILogger<HHAAggrgegatorUpdateEVVCodeCommandHandler> logger,
                                                        IHHAEvvExceptionEventCodesRepository _hHAEvvExceptionEventCodesRepository)
        {
            this._UAEvvScheduleEditCodesMasterRepository = _UAEvvScheduleEditCodesMasterRepository;
            this._hHAEvvExceptionEventsRepository = _hHAEvvExceptionEventsRepository;
            this._repositoryFilterConditions = _repositoryFilterConditions;
            this._repositoryUpdateColumn = _repositoryUpdateColumn;
            this._logger = logger;
            this.hHAEvvExceptionEventCodesRepository = _hHAEvvExceptionEventCodesRepository;
        }

        public async Task<Response<EVVScheduleEditCodeConfigurationDto>> Handle(HHAAggregatorUpdateEVVCodeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                bool ret = false;
                string ModifiedData = "";
                string EVVExceptionIDCommaSep = "";

                StringBuilder sbModifiedData = new StringBuilder();

                EVVScheduleEditCodeConfigurationDto eVVScheduleEditCodeConfigurationDto = new EVVScheduleEditCodeConfigurationDto();

                var EVVCodeList = await _UAEvvScheduleEditCodesMasterRepository.GetHHAEVVCodes(request.HHA, request.UserId, request.CodeType, request.EVVVendorVersionMasterID);

                if (EVVCodeList.Count() > 0)
                {
                    var OldData = EVVCodeList.Where(x => x.EvvScheduleEditCodesMasterID == request.EVVScheduleEditCodeMasterID).ToList();
                    var OldCode = OldData.Select(x => x.Code).FirstOrDefault();

                    if (!(OldCode == request.Code))
                    {
                        if (EVVCodeList.Where(x => x.Code == request.Code).Count() > 0)
                        {
                            eVVScheduleEditCodeConfigurationDto.IsSuccess = -1;
                            eVVScheduleEditCodeConfigurationDto.ErrorMessage = "Modified code already exists for this vendor version";
                            ret = true;
                        }
                    }
                }

                if (!ret)
                {

                    sbModifiedData.AppendFormat(" Code = '{0}',", request.Code.Replace("'", "''"));
                    sbModifiedData.AppendFormat(" Description = '{0}',", request.Description.Replace("'","''"));
                    if (request.CodeType == "Exception")
                    {
                        if (request.EnableNotesPrefill)
                        {
                            sbModifiedData.AppendFormat(" NotePrefillType = 'CLIENTADDRESS',");
                            sbModifiedData.AppendFormat(" EnableNotesPrefill = 1,");
                        }
                        else
                        {
                            sbModifiedData.AppendFormat(" NotePrefillType = '',");
                            sbModifiedData.AppendFormat(" EnableNotesPrefill = 0,");
                        }
                    }


                    ModifiedData = sbModifiedData.ToString();
                    ModifiedData = ModifiedData.Substring(0, ModifiedData.Length - 1);

                    var result = await _UAEvvScheduleEditCodesMasterRepository._HHA_Schedule_UpdateEVVCode(request.HHA, request.UserId, request.EVVScheduleEditCodeMasterID, ModifiedData);

                    eVVScheduleEditCodeConfigurationDto.IsSuccess = result.IsSuccess;
                    eVVScheduleEditCodeConfigurationDto.ErrorMessage = result.ErrorMessage;
                    eVVScheduleEditCodeConfigurationDto.EVVScheduleEditCodeMasterID = result.EVVScheduleEditCodeMasterID;

                    var EVVExceptionList = await _hHAEvvExceptionEventsRepository.GetHHAEvvVendorExceptionEventCodes(request.HHA, request.UserId, request.EVVVendorVersionMasterID);

                    if (EVVExceptionList.Count() > 0 && EVVCodeList.Count() > 0)
                    {

                        var OldData = EVVCodeList.Where(x => x.EvvScheduleEditCodesMasterID == request.EVVScheduleEditCodeMasterID).ToList();
                        var OldCode = OldData.Select(x => x.Code).FirstOrDefault();

                        var ExceptionIDsList = EVVExceptionList.Where(y => y.exception_code == OldCode).Select(x => new { EvvExceptionEventID = x.EvvExceptionEventID, Code=x.exception_code }).ToList();

                        if (ExceptionIDsList.Count > 0)
                        {
                            EVVExceptionIDCommaSep = string.Join(",", ExceptionIDsList.Select(x => x.EvvExceptionEventID.ToString()));

                            if (!string.IsNullOrEmpty(EVVExceptionIDCommaSep))
                            {

                                var Filters = _repositoryFilterConditions.InFilter("EvvExceptionEventID", EVVExceptionIDCommaSep, FilterConditionDataTypeEnums.integerType)
                                                   .EqualFilter("EVVVendorVersionMasterID", request.EVVVendorVersionMasterID, FilterConditionDataTypeEnums.integerType)
                                                   .EqualFilter("Code", ExceptionIDsList[0].Code, FilterConditionDataTypeEnums.stringType)
                                                    .Build();

                                var Columns = _repositoryUpdateColumn.UpdateColumn("Code", request.Code, FilterConditionDataTypeEnums.stringType)
                                               .Build();

                                bool Error = await hHAEvvExceptionEventCodesRepository.UpdateEVVEventCodes(request.HHA, request.UserId, Filters, Columns);
                            }
                        }
                    }
                }

                return Response.Ok<EVVScheduleEditCodeConfigurationDto>(eVVScheduleEditCodeConfigurationDto);
            }
            catch(Exception e) {
                _logger.LogError(e,e.Message);
                return Response.Fail<EVVScheduleEditCodeConfigurationDto>(ResponseErrorCodes.InternalServerFailed,"Failed to save, Please contact KanTime support!");
            }
          
        }
    }
}

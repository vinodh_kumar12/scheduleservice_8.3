﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class DeleteEvvExceptionEventCommadHandler : IHandlerWrapper<DeleteEvvExceptionEventCommad, bool>
    {
        private readonly IEvvConfigurations evvConfigurations;
        private readonly ILogger<DeleteEvvExceptionEventCommadHandler> logger;
        public DeleteEvvExceptionEventCommadHandler(IEvvConfigurations _evvConfigurations, ILogger<DeleteEvvExceptionEventCommadHandler> _logger)
        {
            this.evvConfigurations = _evvConfigurations;
            this.logger = _logger;
        }
        public async Task<Response<bool>> Handle(DeleteEvvExceptionEventCommad request, CancellationToken cancellationToken)
        {
            bool Error = true;
            string Message = "";
            try
            {
                Error = await evvConfigurations.DeleteExceptionEvents(request.HHA, request.UserId, request.EvvExceptionEvents);
            }
            catch (Exception e)
            {
                Error = true;
                Message = "Some unknown exception occurred";
                logger.LogError(e,e.Message);
            }

            if (Error)
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, Message);
            else
                return Response.Ok<bool>(Error);
        }
    }
}

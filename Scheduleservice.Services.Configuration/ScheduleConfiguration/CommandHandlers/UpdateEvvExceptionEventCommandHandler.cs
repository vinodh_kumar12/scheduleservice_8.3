﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;
using Microsoft.Extensions.Logging;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Services.Contracts;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class UpdateEvvExceptionEventCommandHandler : IHandlerWrapper<UpdateEvvExceptionEventCommand, bool>
    {
        private readonly IEvvConfigurations evvConfigurations;
        private readonly ILogger<UpdateEvvExceptionEventCommandHandler> logger;
        public UpdateEvvExceptionEventCommandHandler(IEvvConfigurations _evvConfigurations, ILogger<UpdateEvvExceptionEventCommandHandler> _logger)
        {
            this.evvConfigurations = _evvConfigurations;
            this.logger = _logger;
        }
        public async Task<Response<bool>> Handle(UpdateEvvExceptionEventCommand request, CancellationToken cancellationToken)
        {
            bool Error = true;
            try
            {
                Error = await evvConfigurations.UpdateExceptionEvent(request.HHA, request.UserId, request.Aggregator_ID, request.EvvExceptionEventIDs, request.exception_code);
            }
            catch (Exception e)
            {
                Error = true;
                logger.LogError(e,e.Message);
            }
            if (Error)
            {
                return Response.Fail<bool>(ResponseErrorCodes.InternalServerFailed, "Failed");
            }
            else
            {
                return Response.Ok<bool>(Error);
            }
        }
    }
}

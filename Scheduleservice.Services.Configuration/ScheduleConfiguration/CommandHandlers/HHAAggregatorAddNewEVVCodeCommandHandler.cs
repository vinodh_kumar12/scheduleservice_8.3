﻿using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.Commad;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Scheduleservice.Services.Configuration.ScheduleConfiguration.DTO;
using System.Linq;
using Microsoft.Extensions.Logging;
using Scheduleservice.Core.RepositoryQueryBuilder.Contracts;
using Scheduleservice.Core.Enums;

namespace Scheduleservice.Services.Configuration.ScheduleConfiguration.CommandHandlers
{
    public class HHAAggregatorAddNewEVVCodeCommandHandler : IHandlerWrapper<HHAAggregatorAddNewEVVCodeCommand, EVVScheduleEditCodeConfigurationDto>
    {
        private IEvvScheduleEditCodesMasterRepository _UAEvvScheduleEditCodesMasterRepository;
        private IHHAEVVConfigurationsRepository _HHAEVVConfigurationsRepository;
        private readonly IRepositoryFilterConditions _repositoryFilterConditions;
        private readonly ILogger<HHAAggregatorAddNewEVVCodeCommandHandler> _logger;

        public HHAAggregatorAddNewEVVCodeCommandHandler(IEvvScheduleEditCodesMasterRepository UAEvvScheduleEditCodesMasterRepository, IHHAEVVConfigurationsRepository _HHAEVVConfigurationsRepository,
            ILogger<HHAAggregatorAddNewEVVCodeCommandHandler> logger, IRepositoryFilterConditions _repositoryFilterConditions)
        {
            this._UAEvvScheduleEditCodesMasterRepository = UAEvvScheduleEditCodesMasterRepository;
            this._HHAEVVConfigurationsRepository = _HHAEVVConfigurationsRepository;
            this._logger = logger;
            this._repositoryFilterConditions = _repositoryFilterConditions;
        }

        public async Task<Response<EVVScheduleEditCodeConfigurationDto>> Handle(HHAAggregatorAddNewEVVCodeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                bool ret = false;
                string ModifiedData = "";
                string EVVConfigurationIDWithCommaSep = "";

                EVVScheduleEditCodeConfigurationDto eVVScheduleEditCodeConfigurationDto = new EVVScheduleEditCodeConfigurationDto();

                var EVVCodeList = await _UAEvvScheduleEditCodesMasterRepository.GetHHAEVVCodes(request.HHA, request.UserId, request.CodeType, request.EVVVendorVersionMasterID);

                if (EVVCodeList.Count() > 0)
                { 
                    if(EVVCodeList.Where(x=>x.Code==request.Code).Count()>0)
                    {
                        eVVScheduleEditCodeConfigurationDto.IsSuccess = -1;
                        eVVScheduleEditCodeConfigurationDto.ErrorMessage = "Added code already exists for this vendor version";
                        ret = true;
                    }
                }

                if (!ret)
                {
                    var result = await _UAEvvScheduleEditCodesMasterRepository._HHA_Schedule_AddNewEVVCode(request.HHA, request.UserId, request.EVVVendorVersionMasterID, request.CodeType, request.Code, request.Description,request.EnableNotesPrefill);

                    eVVScheduleEditCodeConfigurationDto.IsSuccess = result.IsSuccess;
                    eVVScheduleEditCodeConfigurationDto.ErrorMessage = result.ErrorMessage;
                    eVVScheduleEditCodeConfigurationDto.EVVScheduleEditCodeMasterID = result.EVVScheduleEditCodeMasterID;

                    var EVVConfigurationFilter = _repositoryFilterConditions.EqualFilter("is_deleted", false, FilterConditionDataTypeEnums.boolType)
                                                .Build();

                    var EVVBranchConfiguarationList = await _HHAEVVConfigurationsRepository._HHA_Schedule_GetHHAEVVConfigurations(request.HHA, request.UserId, "EvvConfigurationID,EvvVendorVersionMasterID, EffectiveDate,AttestationMandatoryForAllReasonCodes,MissedVisitReasonCodeAvailable,MissedVisitActionCodeAvailable,ExcepionCodeAvailable,ReasonCodeAvailable,ActionCodesAvailable,captureExceptionCodeOnReasonCode,requireAllExceptionsToClearForApproval,isActionCodeMandatoryOnReasonCode,isExceptionCodeMandatoryOnReasonCode,isMissedVisitActionCodeMandatoryOnReasonCode,canGrowMissedVisitReason,canGrowMissedVisitAction,canGrowException,canGrowReasonCode,canGrowActionCode", EVVConfigurationFilter);

                    if (EVVCodeList.Count() == 0)
                    {
                        switch (request.CodeType)
                        {
                            case "Exception":
                                var filterExceptionconfigdetails = EVVBranchConfiguarationList.Where(x => x.ExcepionCodeAvailable == false && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                                ModifiedData = "ExcepionCodeAvailable=1,AutoGenerateException=1,";
                                if (filterExceptionconfigdetails.Count > 0)
                                    EVVConfigurationIDWithCommaSep = string.Join(",", filterExceptionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                                break;
                            case "ReasonCode":
                                var filterReasonconfigdetails = EVVBranchConfiguarationList.Where(x => x.ReasonCodeAvailable == false && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                                ModifiedData = "ReasonCodeAvailable=1,";
                                if (filterReasonconfigdetails.Count > 0)
                                    EVVConfigurationIDWithCommaSep = string.Join(",", filterReasonconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                                break;
                            case "ActionCode":
                                 var filterActionconfigdetails = EVVBranchConfiguarationList.Where(x => x.ActionCodesAvailable == false && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                                ModifiedData = "ActionCodesAvailable=1,";
                                if (filterActionconfigdetails.Count > 0)
                                    EVVConfigurationIDWithCommaSep = string.Join(",", filterActionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                                break;
                            case "MissedVisitReasonCode":
                                var filterMissedVisitReasonconfigdetails = EVVBranchConfiguarationList.Where(x => x.MissedVisitReasonCodeAvailable == false && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                                ModifiedData = "MissedVisitReasonCodeAvailable=1,";
                                if (filterMissedVisitReasonconfigdetails.Count > 0)
                                    EVVConfigurationIDWithCommaSep = string.Join(",", filterMissedVisitReasonconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                                break;
                            case "MissedVisitActionCode":
                                var filterMissedVisitActionconfigdetails = EVVBranchConfiguarationList.Where(x => x.MissedVisitActionCodeAvailable == false && x.EvvVendorVersionMasterID == request.EVVVendorVersionMasterID).ToList();
                                ModifiedData = "MissedVisitActionCodeAvailable=1,";
                                if (filterMissedVisitActionconfigdetails.Count > 0)
                                    EVVConfigurationIDWithCommaSep = string.Join(",", filterMissedVisitActionconfigdetails.Select(x => x.EvvConfigurationID.ToString()));
                                break;
                            default:
                                break;
                        }

                        if (!string.IsNullOrEmpty(ModifiedData))
                        {
                            ModifiedData = ModifiedData.Substring(0, ModifiedData.Length - 1);

                            if (!string.IsNullOrEmpty(EVVConfigurationIDWithCommaSep))
                            {
                                var response = await _HHAEVVConfigurationsRepository._HHA_Schedule_SaveEVVBranchAggregatorSettings(request.HHA, request.UserId, EVVConfigurationIDWithCommaSep.ToString(), ModifiedData, "");
                            }
                        }
                    }
                }

                return Response.Ok<EVVScheduleEditCodeConfigurationDto>(eVVScheduleEditCodeConfigurationDto);

            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return Response.Fail<EVVScheduleEditCodeConfigurationDto>(ResponseErrorCodes.InternalServerFailed, "Failed to save, Please contact KanTime support!");
            }
        }
    }
}

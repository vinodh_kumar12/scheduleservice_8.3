﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.PickList.Command
{
    public class DeletePickListCommand : BaseRequest, IRequestWrapper<bool>
    {
        public int PicklistItemID { get; set; }
        public string PickListCategory { get; set; }
    }
}

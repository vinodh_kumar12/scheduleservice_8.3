﻿using Scheduleservice.Core.Common.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.PickList.Command
{
    public class CreatePickListCommand : BaseRequest, IRequestWrapper<bool>
    {
        public string PickListName { get; set; }
        public string PickListCategory { get; set; }
    }
}

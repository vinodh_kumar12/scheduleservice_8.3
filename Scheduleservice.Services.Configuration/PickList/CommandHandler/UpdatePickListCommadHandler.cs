﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Configuration.PickList.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.PickList.CommandHandler
{
    public class UpdatePickListCommadHandler : IHandlerWrapper<UpdatePickListCommad, bool>
    {
        private readonly IPickListValuesRepository _pickListValuesRepository;
        public UpdatePickListCommadHandler(IPickListValuesRepository pickListValuesRepository)
        {
            _pickListValuesRepository = pickListValuesRepository;
        }
        public async Task<Response<bool>> Handle(UpdatePickListCommad request, CancellationToken cancellationToken)
        {
            int result = await _pickListValuesRepository.UpdatePickListValues(request.HHA, request.UserId, request.PickListName, request.PickListCategory, request.PicklistItemID);

            return Response.Ok<bool>(true);
        }
    }
}

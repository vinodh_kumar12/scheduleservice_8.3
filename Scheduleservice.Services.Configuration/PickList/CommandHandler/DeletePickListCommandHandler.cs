﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts;
using Scheduleservice.Services.Configuration.PickList.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Scheduleservice.Services.Configuration.PickList.CommandHandler
{
   public class DeletePickListCommandHandler : IHandlerWrapper<DeletePickListCommand, bool>
    {
        private readonly IPickListValuesRepository _pickListValuesRepository;
        public DeletePickListCommandHandler(IPickListValuesRepository pickListValuesRepository)
        {
            _pickListValuesRepository = pickListValuesRepository;
        }

        public async Task<Response<bool>> Handle(DeletePickListCommand request, CancellationToken cancellationToken)
        {
            int result = await _pickListValuesRepository.DeletePickListValues(request.HHA, request.UserId, request.PicklistItemID, request.PickListCategory);

            return Response.Ok<bool>(true);
        }
    }
}

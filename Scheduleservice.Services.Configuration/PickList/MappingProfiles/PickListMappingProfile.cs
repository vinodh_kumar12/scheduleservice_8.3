﻿using AutoMapper;
using Scheduleservice.Core.Entities;
using Scheduleservice.Services.Configuration.PickList.DTO; 

namespace Scheduleservice.Services.Configuration.PickList.MappingProfiles
{
    public class PickListMappingProfile:Profile
    {
        public PickListMappingProfile()
        {
            CreateMap<PickListValuesEntity, PickListDTO>(); 
        }
    }
}

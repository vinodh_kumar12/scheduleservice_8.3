﻿using AutoMapper;
using Scheduleservice.Core.Common.Request;
using Scheduleservice.Core.Common.Response;
using Scheduleservice.Core.Interfaces.RepositoryContracts; 
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Scheduleservice.Services.Configuration.PickList.Query;
using Scheduleservice.Services.Configuration.PickList.DTO;

namespace Scheduleservice.Services.Configuration.PickList.QueryHandlers
{
    public class PickListQueryHandler : IHandlerWrapper<PickListQuery, IEnumerable<PickListDTO>>
    { 
        private readonly IPickListValuesRepository _pickListValuesRepository;
     
        private readonly IMapper _mapper;
        public PickListQueryHandler( IPickListValuesRepository pickListValuesRepository, IMapper mapper)
        {
            this._mapper = mapper; 
            this._pickListValuesRepository = pickListValuesRepository; 
        }
        public async Task<Response<IEnumerable<PickListDTO>>> Handle(PickListQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<PickListDTO> pageFilterFieldsDto=new List<PickListDTO>();
            var _pageFilterFieldsentity= await _pickListValuesRepository.GetPickListValues(request.HHA,request.UserId, request.Context);
            pageFilterFieldsDto = this._mapper.Map<IEnumerable<PickListDTO>>(_pageFilterFieldsentity);
            return Response.Ok<IEnumerable<PickListDTO>>(pageFilterFieldsDto);
        }
       

    }
    }


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.PickList.DTO
{
    public class PickListDTO
    {
        public int PLValueIndex { get; set; }
        public string PLValue { get; set; }
        public string PLLabel { get; set; }
        public int PL_Value_ID { get; set; }

    }
}

﻿using Scheduleservice.Core.Common.Request;
using Scheduleservice.Services.Configuration.PickList.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Scheduleservice.Services.Configuration.PickList.Query
{
    public class PickListQuery : BaseRequest, IRequestWrapper<IEnumerable<PickListDTO>>
    {
      public string Context { get; set; }
    }
}

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY . .
RUN dotnet restore "Scheduleservice.API/Scheduleservice.API.csproj"  --configfile ./NuGet/NuGet.Config  -s https://api.nuget.org/v3/index.json -s http://192.168.2.15/KantimeNuget/nuget -verbosity:normal
WORKDIR "/src/Scheduleservice.API"
RUN dotnet build "Scheduleservice.API.csproj" -c Release -o /app
RUN dotnet publish "Scheduleservice.API.csproj" -c Release -o /app
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "Scheduleservice.API.dll"]
